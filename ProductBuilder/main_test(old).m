//
//  main.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Customer.h"
#import "ItemCharge.h"
#import "Receipt.h"
#import "USAdditions.h"
#import "SyncManager.h"
#import "CCSwipeParser.h"
#import "CCMethods.h"
#import "NSString+Utils.h"

@interface SyncOp : NSOperation{
    BOOL isExecuting;
    BOOL isFinished;
    NSString* name;
    BOOL sleep;
    NSUInteger loop;
    NSOperationQueue* parentQueue;
}
@property (nonatomic, retain)NSString* name;
@property (nonatomic, assign)NSOperationQueue* parentQueue;
-(void)finish;
-(id)initWithName:(NSString*)aName sleep:(BOOL)sleep;
@end

@interface SyncOp (Private)
//-(void)setIsFinished:(BOOL)value;
///-(void)setIsExecuting:(BOOL)value;
@end

@implementation SyncOp

@synthesize name, parentQueue;

-(id)initWithName:(NSString*)aName sleep:(BOOL)aSleep{
    if ((self=[super init])){
        self.name = aName;
        sleep = aSleep;
    }
    return self;
}

-(void)dealloc{
    [name release];
    [super dealloc];
}
/*
+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key{
    return TRUE;
}
*/

-(void)start{
    NSLog(@"operation %@ started. Thread = %@", self.name, [NSThread currentThread]);
    
    [self willChangeValueForKey:@"isExecuting"];
    isExecuting = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    //self.isExecuting = YES;
    if (sleep)
        [NSThread sleepForTimeInterval:2.0];
    [self finish];
}

-(BOOL)isExecuting{
    return isExecuting;
}
/*
-(void)setIsFinished:(BOOL)value{
    isFinished =value;
}

-(void)setIsExecuting:(BOOL)value{
    isExecuting =value;
}
 */
-(BOOL)isFinished{
    return isFinished;   
}

-(BOOL)isConcurrent{
    return FALSE;
}

- (void)finish
{
    NSLog(@"operation %@ finished. Thread = %@", self.name, [NSThread currentThread]);

    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    isExecuting = NO;
    isFinished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];

    //self.isExecuting = NO;
    //self.isFinished = YES;
    if (parentQueue){
        NSLog(@"notify parent queue here");
    }
}
@end

@interface SyncQueue : NSOperationQueue

@end

@implementation SyncQueue

-(id)init{
    if ((self = [super init])){
        [self addObserver:self forKeyPath:@"operationCount" options:NSKeyValueObservingOptionNew context:nil];
        [self setMaxConcurrentOperationCount:1];
    }
    return self;
}

-(void)addOperation:(NSOperation *)op{
    if ([[op class]isSubclassOfClass:[SyncOp class]]){
        ((SyncOp*)op).parentQueue = self;
    }
    [super addOperation:op];
}

-(void)addOperations:(NSArray *)ops waitUntilFinished:(BOOL)wait{
    [ops enumerateObjectsUsingBlock: ^(id op, NSUInteger idx, BOOL *stop)
    {
        if ([[op class]isSubclassOfClass:[SyncOp class]]){
            ((SyncOp*)op).parentQueue = self;
        }
    }];
    [super addOperations:ops waitUntilFinished:wait];

}

-(void)setMaxConcurrentOperationCount:(NSInteger)cnt{
    if (cnt!=1) return;
    [super setMaxConcurrentOperationCount:cnt];
}

-(void)dealloc{
    [self removeObserver:self forKeyPath:@"operationCount"];
    [super dealloc];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object 
                         change:(NSDictionary *)change context:(void *)context{
    
    if (object == self && [keyPath isEqualToString:@"operationCount"]) {
        if (self.operationCount == 0) {
            NSLog(@"queue has completed");
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object 
                               change:change context:context];
    }
}
@end


@interface MyObserver:NSObject{
	NSMutableDictionary *objs;
}
-(void)observeOp:(SyncOp*)op;
@end

@implementation MyObserver

-(id)init{
	if ((self = [super init])){
		objs = [[NSMutableDictionary alloc]init];
	}
	return self;
}

-(void)dealloc{
	for (NSString* key in [objs allKeys]){
		NSObject* o = [objs objectForKey:key];
		[o removeObserver:self forKeyPath:key];
	}
	[objs release];
	[super dealloc];
}

-(void)observeOp:(SyncOp*)d{
	[d addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
	[objs setObject:d forKey:@"isFinished"];
	
	[d addObserver:self forKeyPath:@"isExecuting" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
	[objs setObject:d forKey:@"isExecuting"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([[change objectForKey:NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeSetting) {
		id oldValue = [change objectForKey:NSKeyValueChangeOldKey];
		id newValue = [change objectForKey:NSKeyValueChangeNewKey];
		if ((!oldValue && newValue) || (oldValue && ![oldValue isEqual:newValue])){
			NSLog(@"oldValue for %@ = %@, new value = %@", keyPath, oldValue, newValue);
		}
	}
}

@end

@interface A : CROEntity
@property (nonatomic, assign)NSDecimal d;
@property (nonatomic, assign)NSInteger i;
@property (nonatomic, readonly)NSString* s;
@property (nonatomic, readonly)NSNumber* n;
@property (nonatomic, readonly)NSMutableArray* arr;
@property (nonatomic, assign)NSInteger count;
@end

@interface A(Private)
@property (nonatomic, retain)NSString* s;
@property (nonatomic, retain)NSNumber* n;
@property (nonatomic, retain)NSMutableArray* arr;
@end
    
@implementation A
@synthesize d,i,s, n, count, arr;
-(id)init{
    if ((self=[super init])){
        self.s=@"hi";
    }
    return self;
}
-(void)dealloc{
    self.n = nil;
    self.s = nil;
    self.arr = nil;
    [super dealloc];
}
@end


@interface ArrayObserver:NSObject{
    NSObject* object;
    NSString* objKeyPath;
}
-(id)initWithObject:(NSObject*)anObject keyPath:(NSString*)keyPath;
@end

@implementation ArrayObserver


-(id)initWithObject:(NSObject*)anObject keyPath:(NSString*)keyPath{
    if ((self=[super init])){
        object = [anObject retain];
        objKeyPath = [keyPath retain];
        [object addObserver:self forKeyPath:objKeyPath options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
        if ([object isKindOfClass:[NSArray class]]){
            for(id obj in (NSArray*)object){
                //[obj addObserver:self forKeyPath:<#(NSString *)#> options:<#(NSKeyValueObservingOptions)#> context:<#(void *)#>
            }
        }
    }
    return self;
}
-(void)dealloc{
    [self removeObserver:object forKeyPath:@"arr"];
    [object release];
    [objKeyPath release];
    [super dealloc];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
	NSObject* objKind = [change objectForKey:NSKeyValueChangeKindKey];
    if (objKind && [objKind isKindOfClass:[NSNumber class]]){
        NSInteger kind  =[(NSNumber*)objKind intValue];
        if (kind == NSKeyValueChangeSetting) {
            id oldValue = [change objectForKey:NSKeyValueChangeOldKey];
            id newValue = [change objectForKey:NSKeyValueChangeNewKey];
            NSLog(@"oldValue for %@ = %@, new value = %@", keyPath, oldValue, newValue);
        }
        else if (kind == NSKeyValueChangeInsertion || kind == NSKeyValueChangeRemoval || kind== NSKeyValueChangeReplacement){
            id indexes = [change objectForKey:NSKeyValueChangeIndexesKey];
            if ([indexes isKindOfClass:[NSIndexSet class]]){
                NSIndexSet* idxSet = (NSIndexSet*)indexes;
                [idxSet enumerateIndexesUsingBlock:^(NSUInteger idx,BOOL* stop){
                    NSLog(@"changed index = %i", idx);
                }];
                
            }
            id oldValue = [change objectForKey:NSKeyValueChangeOldKey];
            id newValue = [change objectForKey:NSKeyValueChangeNewKey];
            if ([newValue isKindOfClass:[NSArray class]]){
                NSArray* newValues = (NSArray*)newValue;
                for(id obj in newValues){
                    NSLog(@"%@", obj);
                }
            }
            if ([oldValue isKindOfClass:[NSArray class]]){
                NSArray* oldValues = (NSArray*)oldValue;
                for(id obj in oldValues){
                    NSLog(@"%@", obj);
                }
            }

        }
    }
}

@end
/*
typedef struct {
    unsigned int _length;     // length == 0 && isNegative -> NaN
    unsigned short _mantissa[NSDecimalMaxSize];
} NSDecimalFake;

@interface NSDecimalFakeObject :NSObject{
    NSDecimalFake df;
    NSDecimal d;
}
@end

@implementation NSDecimalFakeObject

@end

typedef struct {
    signed   int _exponent:8;
    unsigned int _length:4;     // length == 0 && isNegative -> NaN
    unsigned int _isNegative:1;
    unsigned int _isCompact:1;
    unsigned int _reserved:18;
    unsigned short _mantissa[NSDecimalMaxSize];
} NSDecimalOrg;
*/

typedef struct{
    NSDecimal total1;    
    NSDecimal total2;
    //NSDecimal total3;
}EditableValues;

typedef struct{
    NSUInteger i1;    
    NSUInteger i2;
}NSDummy;

typedef struct{
    NSDummy d1;    
    NSDummy d2;
}NSDummyTotals;

@interface TestEntity : CEntity {
@private
    NSUInteger    i;
    NSDummyTotals dummyTotals;
    EditableValues lineValues;
    NSString* name;
    NSInteger count;
}
@property (nonatomic,retain) NSString* name;
-(void)setValues:(EditableValues)aValues;
-(EditableValues)values;
@end

@implementation TestEntity

@synthesize name;

-(void)setValues:(EditableValues)aValues{
    lineValues = aValues;
}
-(EditableValues)values{
    return lineValues;
}
-(void)dealloc{
    [name release];
    [super dealloc];
}

@end

@interface DummyConnection : NSObject {
@private
    NSLock* lock;
}
-(NSLock*)lock;
-(void)execute:(NSUInteger)trancount command:(NSString*)command;
@property(nonatomic, assign)BOOL inTransaction;
@end

@implementation DummyConnection
@synthesize inTransaction;
-(id)init{
    if ((self=[super init])){
        lock = [[NSLock alloc]init];
    }
    return self;
}

-(NSLock*)lock{
    return lock;
}

-(BOOL)inTransaction{
    NSNumber* value = [[[NSThread currentThread]threadDictionary]objectForKey:@"inTransaction"];
    return [value boolValue];
}

-(void)setInTransaction:(BOOL)value{
    [[[NSThread currentThread]threadDictionary]setObject:@(value)forKey:@"inTransaction"];
}

-(void)execute:(NSUInteger)trancount command:(NSString*)command{
    @try{
        NSLog(@"%@ executing %@", [NSThread currentThread], command);
        if (!self.inTransaction){
            NSLog(@"%@ accuring lock",[NSThread currentThread]);
            [lock lock];
            NSLog(@"%@ accured lock", [NSThread currentThread]);
        }
        else{
            NSLog(@"%@ running withing transaction", [NSThread currentThread]);
        }

        NSLog(@"%@ settting tran to %i", [NSThread currentThread],trancount);
        if (trancount){
            self.inTransaction = TRUE;
        }
        else{
            self.inTransaction = FALSE;
        }
        NSLog(@"%@ current tran %i", [NSThread currentThread],self.inTransaction);
        
        NSLog(@"%@ start executing. in transaction = %i", [NSThread currentThread],self.inTransaction);
        [NSThread sleepForTimeInterval:2];
        NSLog(@"%@ end executing. in transactcion = %i", [NSThread currentThread], self.inTransaction);
    }
    @finally{
        if (!self.inTransaction){
             NSLog(@"%@ releasing lock", [NSThread currentThread]);
            [lock unlock];
            NSLog(@"%@ released lock", [NSThread currentThread]);
        }
    }
}

-(void)dealloc{
    [lock release];
    [super dealloc];
}

@end


int main(int argc, char *argv[]){
{
        @autoreleasepool {
//            BPUUID* emptyGuid = [BPUUID UUIDWithString:@"00000000-0000-0000-0000-000000000001"];
//            NSLog(@"%d",[emptyGuid isEmpty]);
        CCSwipeParser* parser = [[[CCSwipeParser alloc]init]autorelease];
        NSError* error = nil;
        NSString* s = [[[NSString stringWithString:@"%B4736904206379782^THOMAS J\n\
                         GODFREY^1207101007000705680900654000000?;4736904206379782=12071010000000654000?"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByReplacingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] withString:@" "];
        NSLog(@"%@",s);
        if ([parser parse:@"%B4736904206379782^THOMAS J\n\
             GODFREY^1207101007000705680900654000000?;4736904206379782=12071010000000654000?" error:&error]){
            
            NSLog (@"%i",[CCMethods is_validChecksum:[parser account]]); 
        }
        }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        EGODatabase* db = [[DataManager instance]currentDatabase];
        dispatch_queue_t queue1 = dispatch_queue_create("com.cloudworks.backgroundTask1",NULL);
        dispatch_queue_t queue2 = dispatch_queue_create("com.cloudworks.backgroundTask2",NULL);
        [db execute:@"create table if not exists t_dummy(id int primary key, txt text)"];
        [db execute:@"delete from t_dummy"];
        dispatch_async(queue1, ^{
            NSLog(@"thread=%@ inTransaction =%i",[NSThread currentThread], [db inTransaction]);
            [db beginTransaction];
            [db executeUpdate:@"insert into t_dummy values(1,'hi')"];
            [db executeUpdate:@"insert into t_dummy values(2,'by')"];
            NSLog(@"thread=%@ inTransaction =%i",[NSThread currentThread], [db inTransaction]);
            [db commit];
            NSLog(@"thread=%@ inTransaction =%i",[NSThread currentThread], [db inTransaction]);
        });
        dispatch_async(queue2, ^{
            NSLog(@"thread=%@ inTransaction =%i",[NSThread currentThread], [db inTransaction]);
            EGODatabaseResult* result =[db executeQuery:@"select count(*) from t_dummy"];
            if ([result count]){
                NSInteger count = [[result.rows objectAtIndex:0]intForColumnIndex:0];
                NSLog(@"count =%i",count);
            }
        });
        dispatch_sync(queue1, ^{
        });
        dispatch_sync(queue2, ^{
        });
        dispatch_release(queue1);
        dispatch_release(queue2);
        [pool release];
        return 0;
    }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        DummyConnection* c = [[DummyConnection alloc]init];
        dispatch_queue_t queue1 = dispatch_queue_create("com.cloudworks.backgroundTask1",NULL);
        dispatch_queue_t queue2 = dispatch_queue_create("com.cloudworks.backgroundTask2",NULL);
        dispatch_async(queue1, ^{
            [c execute:TRUE command:@"begin tran"];
            [c execute:FALSE command:@"commit tran"];
        });
        [NSThread sleepForTimeInterval:1];
        dispatch_async(queue2, ^{
            [c execute:FALSE command:@"dummy"];
        });
        dispatch_sync(queue1, ^{
        });
        dispatch_sync(queue2, ^{
        });
        dispatch_release(queue1);
        dispatch_release(queue2);
        [c release];
        [pool release];
        return 0;
    }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        xmlDocPtr doc;
        NSData* faultData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TestFault" ofType:@"xml"]];
        doc = xmlParseMemory([faultData bytes], [faultData length]);
        SOAPFault *bodyObject = [SOAPFault deserializeNode:doc->children];
        NSLog(@"%@",bodyObject);
        xmlFreeDoc(doc);
        [pool release];
        return 0;
    }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        ReceiptItem* ri = [[[ReceiptItem alloc]init]autorelease];
        NSMutableString* priceLevelCode =[[[NSMutableString alloc]initWithString:@""]autorelease];
        ri.priceLevelCode = priceLevelCode;
        //[ri beginEdit];
        //[ri setQty:CPDecimalFromInt(10)];
        //NSMutableString* priceLevelCode2 =[[[NSMutableString alloc]initWithString:@"MSRP"]autorelease];
        // ri.priceLevelCode = priceLevelCode2;
        //[ri cancelEdit];
        //NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:ri];
        //ReceiptItem* ri2 = [NSKeyedUnarchiver unarchiveObjectWithData:archivedData];
        [pool release];
        return 0;
    }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        TestEntity* te = [[TestEntity alloc]init];
        
        /*
        NSDummyTotals dt;
        NSDummy d1 = {99,49};
        NSDummy d2 = {55,31};
        dt.d1 = d1;
        dt.d2 = d2;
        NSData* data = [NSData dataWithBytes:&values length:sizeof(EditableValues)];
        NSValue* v= [NSValue valueWithBytes:&dt objCType:@encode(NSDummyTotals)];
        [te setValue:v forKey:@"dummyTotals"];
         */
        
        //NSUInteger i = 99;
        //object_setInstanceVariable(te, [[NSString stringWithString:@"i"]UTF8String],  *(char**)&i);
        //NSValue* vi = [NSValue valueWithBytes:&i objCType:@encode(NSUInteger)];
        //object_setInstanceVariable(te, [[NSString stringWithString:@"i"]UTF8String],  vi);
        
        
        EditableValues values;
        values.total1 = CPDecimalFromFloat(10.1);
        values.total2 = CPDecimalFromFloat(19.99);
        /*
        
        void *value;
        value = *(void**)&values;
        Ivar var;
        void **varIndex;
        if ((var = class_getInstanceVariable([te class], [[NSString stringWithString:@"lineValues"]UTF8String]))){
            varIndex = (void **)((char *)te + ivar_getOffset(var));
            *varIndex = value;
        }

        //NSString* s = @"hi";
        //object_setIvar(te, class_getInstanceVariable([TestEntity class], [[NSString stringWithString:@"name"]UTF8String]), s );
        //NSValue* v = [NSValue valueWithBytes:&dt objCType:@encode(NSDummyTotals)];
        //object_setIvar(te, class_getInstanceVariable([TestEntity class], [[NSString stringWithString:@"dummyTotals"]UTF8String]), v );
        //[te setValue:v forKey:@"dummyTotals"];
        //object_setInstanceVariable(te, [[NSString stringWithString:@"dummyTotals"]UTF8String], v);
        //object_setInstanceVariable(te, [[NSString stringWithString:@"dummyTotals"]UTF8String],  (void*)&dt);
         */
        te.name = @"test";
        
        [te setValues:values];
        //char* key = [[NSString stringWithString:@"lineValues"]UTF8String];
        //Ivar ivar = class_getInstanceVariable([TestEntity class], key);
        //object_setInstanceVariable(te, key,  *(void **)&values);
        
        NSLog(@"total1=%@, total2=%@",[NSCurrencyFormatter formatCurrency:te.values.total1],[NSCurrencyFormatter formatCurrency:te.values.total2]);
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:te];
        TestEntity* te2 = [NSKeyedUnarchiver unarchiveObjectWithData:archivedData];
        NSLog(@"total1=%@, total2=%@",[NSCurrencyFormatter formatCurrency:te2.values.total1],[NSCurrencyFormatter formatCurrency:te2.values.total2]);
        [te release];
        [pool release];
        return 0;
    }
    
    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        
        unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        //calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        calendar.timeZone = [NSTimeZone localTimeZone];
        NSDate* d = [NSDate date];
        NSDateComponents* components = [calendar components:flags fromDate:d];
        NSDate* dateOnly = [calendar dateFromComponents:components];
        NSLog(@"%@\n%@",d, dateOnly);
        
        Receipt* receipt = [[[Receipt alloc]init]autorelease];
        CEntityList* list = [[CEntityList alloc]init];
        ReceiptItem* receiptItem = [[[ReceiptItem alloc]initWithinReceipt:receipt error:nil]autorelease];
        //[receipt insertObject:receiptItem inItemsAtIndex:[receipt countOfItems]];
        [list addObject:receiptItem];
        //NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:list];
        //CEntityList* list2 = [[NSKeyedUnarchiver unarchiveObjectWithData: archivedData]retain];
        //[list2 release];
        [receiptItem markOld];
        [list removeObject:receiptItem];
        for (ReceiptItem* ri in list.deletedList){
            NSLog(@"%@",ri);
        }
        [list release];
        [pool release];
        return 0;
    }

    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        Receipt* receipt = [[[Receipt alloc]init]autorelease];
        ArrayObserver* obs = [[ArrayObserver alloc]initWithObject:receipt keyPath:@"items"];
        ReceiptItem* receiptItem = [[[ReceiptItem alloc]initWithinReceipt:receipt error:nil]autorelease];
        [receipt insertObject:receiptItem inItemsAtIndex:[receipt countOfItems]];
        //[a willChangeValueForKey:@"arr"];
        //[[a mutableArrayValueForKey:@"arr"] addObject:@"test"];
        //[a didChangeValueForKey:@"arr"];
        [obs release];
        [pool release];
        return 0;
    }

    {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        A* a = [[[A alloc]init]autorelease];
        NSLog(@"%@",[NSCurrencyFormatter formatCurrencyFromObject:[a valueForKey:@"d"]]);
        NSLog(@"%@",[NSCurrencyFormatter formatCurrencyFromObject:[a valueForKey:@"i"]]);
        NSLog(@"%@",[NSCurrencyFormatter formatCurrencyFromObject:[a valueForKey:@"s"]]);        
        NSLog(@"%@",[NSCurrencyFormatter formatCurrencyFromObject:[a valueForKey:@"n"]]);    
        [pool release];
        return 0;
    }
    
    {
        if(getenv("NSZombieEnabled"))
            NSLog(@"NSZombieEnabled enabled!");
        if(getenv("NSAutoreleaseFreedObjectCheckEnabled"))
            NSLog(@"NSAutoreleaseFreedObjectCheckEnabled enabled!");
        NSObject* o = [[NSObject alloc]init];
        [o release];
        [o release];

    }
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
        NSMutableArray* guids = [[[NSMutableArray alloc]init]autorelease];
        for (int i=0;i<12;i++){
            [guids addObject:[NSNumber numberWithInt:i]];
        }
        NSInteger blockAmount = 10;
        NSUInteger offset = 0;
        NSUInteger blockLen = 0;
        /*
        for(NSUInteger currentLoop = 0;(offset + blockLen + 1) <= guids.count;currentLoop++){
            offset = currentLoop * blockAmount;
            blockLen = (offset + blockAmount) > guids.count ? guids.count - offset : blockAmount;
            NSRange range = NSMakeRange(offset, blockLen);
            NSArray* ids = [guids subarrayWithRange:range];
            for(NSNumber* n in ids){
                NSLog(@"%@", n);
            }
        }
         */
        do{
            blockLen = (offset + blockAmount) > guids.count ? guids.count - offset : blockAmount;
            NSRange range = NSMakeRange(offset, blockLen);
            NSArray* ids = [guids subarrayWithRange:range];
            for(NSNumber* n in ids){
                NSLog(@"%@", n);
            }
            if ((offset + blockLen + 1) >= guids.count) break;
            offset += blockLen;
        }while(true);
        [pool release];
        
    }
    return 0;
    
    {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        SyncQueue * queue = [[[SyncQueue alloc] init] autorelease];
        SyncOp* op1 = [[[SyncOp alloc]initWithName:@"task 1" sleep:TRUE] autorelease];
        SyncOp* op2 = [[[SyncOp alloc]initWithName:@"task 2" sleep:FALSE] autorelease];
        MyObserver* observer = [[[MyObserver alloc]init] autorelease];
        [observer observeOp:op1];
         NSLog(@"main thread = %@", [NSThread currentThread]);
        [queue setMaxConcurrentOperationCount:1];
        [queue setSuspended:TRUE];
        [op2 addDependency:op1];
        //[queue addOperation:op1];
        //[queue addOperation:op2];
        NSArray* operations = [NSArray arrayWithObjects:op1, op2, nil];
        [queue addOperations:operations waitUntilFinished:FALSE];
        NSLog(@"queue ops count = %i",[[queue operations]count]);
        [queue setSuspended:FALSE];
        [queue waitUntilAllOperationsAreFinished];
        NSLog(@"queue ops count = %i",[[queue operations]count]);
        [pool release];
    }
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    xmlDocPtr doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
	xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar*)"Envelope", NULL);
	xmlDocSetRootElement(doc, root);
	xmlNodePtr bodyNode = xmlNewDocNode(doc, nil, (const xmlChar*)"Body", NULL);
    xmlAddChild(root, bodyNode);
    NSString* body = [NSString stringWithString:@"<'<None>'>"];
	xmlAddChild(bodyNode, [body xmlNodeForDoc:doc elementName:@"key" elementNSPrefix:nil]);
	
	xmlChar *buf;
	int size;
	xmlDocDumpFormatMemory(doc, &buf, &size, 1);
	
	NSString *serializedForm = [NSString stringWithCString:(const char*)buf encoding:NSUTF8StringEncoding];
	xmlFree(buf);
	
	xmlFreeDoc(doc);	
    NSLog(@"%@", serializedForm);
    
    NSLog(@"%@", [NSDate date]);
    NSLog(@"%@", [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle]);
    
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSLog(@"%f", interval);
    
    //NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];    
    NSDate* currentDate = [NSDate date];
    NSLog(@"%@",currentDate);
    NSLog(@"Local Time Zone %@",[[NSTimeZone localTimeZone] name]);
    NSLog(@"System Time Zone %@",[[NSTimeZone systemTimeZone] name]);
    NSLog(@"Default Time Zone %@",[[NSTimeZone defaultTimeZone] name]);
    NSInteger currentGMTOffset = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate* currentGmtDate = [currentDate dateByAddingTimeInterval:currentGMTOffset];
    NSLog(@"%@",currentGmtDate);
    /*
    NSString* dateStr = @"2011-06-24T10:06:23.197";
    NSDate* date = [NSDate dateWithISO8601String:dateStr];
    
    NSDateFormatter * f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    f.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    f.calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] autorelease];
    f.locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
    //NSDate * date = [f dateFromString:dateStr];
    //NSDate* date2 = [date dateByAddingTimeInterval:1.0f/1000.0f];
    NSString *str = [f stringFromDate:date];
    NSLog(@"%@",date);
    NSLog(@"%@",str);
     */
/*
    Receipt* receipt = [[Receipt alloc]init];
    CustomerList* customers = [[CustomerList alloc]init];
    Customer* customer = [customers objectAtIndex:0];
    receipt.sellToCustomer = customer;
    [customers release];
    [receipt save];
    [receipt release];
 */
    /*
    NSArray* itemChargeList = [ItemCharge getItemChargeList];
    for(ItemCharge* itemCharge in itemChargeList){
        NSLog(@"%@",itemCharge.description);
        for(ItemChargePriceRange* priceRange in itemCharge.priceRangeList){
            NSLog(@"price range from %@ to %@",[NSDecimalNumber decimalNumberWithDecimal:priceRange.priceFrom], [NSDecimalNumber decimalNumberWithDecimal:priceRange.priceTo]);
        }
    }
    */
    /*
    Customer* customer = [[Customer alloc]init];
    customer.nameFirst = @"Billy";
    customer.nameSecond = @"Rubin";
    //[customer save];
    [customer release];
     */
    [pool release];
    return 0;
}
