#!/bin/bash
DIR=$(cd $(dirname "$0"); pwd) 
echo $DIR
find $DIR -type f -name "*.xib" | while read FILENAME;
do
ibtool --errors --warnings --notices --export-strings-file $FILENAME.strings $FILENAME
# echo $FILENAME
done