INSERT INTO InvenStyleFTS(InvenStyleFTS) VALUES('rebuild');

DROP TRIGGER IF EXISTS InvenStyle_bu;
CREATE TRIGGER InvenStyle_bu BEFORE UPDATE ON InvenStyle BEGIN
DELETE FROM InvenStyleFTS WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS InvenStyle_bd;
CREATE TRIGGER InvenStyle_bd BEFORE DELETE ON InvenStyle BEGIN
DELETE FROM InvenStyleFTS WHERE docid = old.rowid;
END;

DROP TRIGGER IF EXISTS InvenStyle_au;
CREATE TRIGGER InvenStyle_au AFTER UPDATE ON InvenStyle BEGIN
INSERT INTO InvenStyleFTS(docid, Description, Description2, Description3, Description4) VALUES (new.rowid, new.Description, new.Description2, new.Description3, new.Description4);
END;

DROP TRIGGER IF EXISTS InvenStyle_ai;
CREATE TRIGGER InvenStyle_ai AFTER INSERT ON InvenStyle BEGIN
INSERT INTO InvenStyleFTS(docid, Description, Description2, Description3, Description4) VALUES (new.rowid, new.Description, new.Description2, new.Description3, new.Description4);
END;
