//
//  main.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    if(getenv("NSZombieEnabled"))
        NSLog(@"NSZombieEnabled enabled!");
    if(getenv("NSAutoreleaseFreedObjectCheckEnabled"))
        NSLog(@"NSAutoreleaseFreedObjectCheckEnabled enabled!");

    int retVal = UIApplicationMain(argc, argv, nil, @"ProductBuilderAppDelegate");
    [pool release];
    return retVal;
}
