//
//  TableInfo.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/30/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

@interface TableInfo: NSObject {
    NSString* name;
    int count;
    bool completed;
    bool canceled;
    NSTimeInterval loadTime;
    NSString*       errorMessage;
}

@property (nonatomic, assign) int count;
@property (nonatomic, assign) bool completed;
@property (nonatomic, assign) bool canceled;
@property (nonatomic, assign) NSTimeInterval loadTime;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* errorMessage;

@end
