//
//  BaseWindow.m
//  ProductBuilder
//
//  Created by DSM on 8/8/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "BaseWindow.h"
#import "AutoLogOutManager.h"

@implementation BaseWindow


- (void)sendEvent:(UIEvent *)event {
    
    [super sendEvent: event];
    
    if (event.type == UIEventTypeTouches)
        [[AutoLogOutManager instance] userActivityEvent];
}

@end
