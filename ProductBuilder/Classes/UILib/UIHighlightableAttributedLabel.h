//
//  UIHighlightableAttributedLabel.h
//  ProductBuilder
//
//  Created by valera on 5/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICopyLabel.h"

@interface UIHighlightableAttributedLabel : UICopyLabel

@end
