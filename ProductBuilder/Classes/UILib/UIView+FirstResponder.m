//
//  UIView+GetFirstResponder.m
//  StockCount
//
//  Created by Valera on 12/2/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "UIView+FirstResponder.h"

@implementation UIView (FirstResponder)

- (UIView*)getFirstResponder
{
    if (self.isFirstResponder) {
        return self;     
    }
    for (UIView *subView in self.subviews) {
        UIView* responderView = [subView getFirstResponder];
        if (responderView) return responderView;
    }
    return nil;
}

- (UIView*)topSuperView{
    UIView* currentSuperview = self.superview;
    UIView* nextSuperview = currentSuperview.topSuperView;
    return nextSuperview ?  nextSuperview : currentSuperview;
}

@end


@implementation UIView (FindUIViewController)
- (UIViewController *) firstAvailableUIViewController {
    // convenience function for casting and to "mask" the recursive function
    return (UIViewController *)[self traverseResponderChainForUIViewController];
}

- (id) traverseResponderChainForUIViewController {
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else if ([nextResponder isKindOfClass:[UIView class]]) {
        return [nextResponder traverseResponderChainForUIViewController];
    } else {
        return nil;
    }
}
@end
