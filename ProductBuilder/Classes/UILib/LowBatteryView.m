//
//  AGWindowView.m
//  VG
//
//  Created by Håvard Fossli on 23.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "LowBatteryView.h"
#import <QuartzCore/QuartzCore.h>

@implementation LowBatteryView

+(LowBatteryView *)instance {

    static dispatch_once_t pred;
    static LowBatteryView *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[LowBatteryView alloc] init];
    });
    return instance;
}

-(BOOL)visible {
    
    return _visible;
}

-(void)setVisible:(BOOL)visible {
    
    _visible = visible;
    [self batteryChanged:nil];
}

#pragma mark - Construct, destruct and setup

- (id)initWithCoder:(NSCoder *)aDecoder{
    if((self = [super initWithCoder:aDecoder])){
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    if((self = [super initWithFrame:frame])){
        [self setup];
    }
    return self;
}

- (void)setup{
    _visible = YES;
    self.autoresizesSubviews = TRUE;
    imageView = [[UIImageView alloc]initWithFrame:self.bounds];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:@"lowBattery.png"];
    //imageView.alpha = 0.6f;
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 10.0f;
    [self addSubview:imageView];
    
    ligtningImageView = [[UIImageView alloc]initWithFrame:self.bounds];
    ligtningImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    ligtningImageView.contentMode = UIViewContentModeScaleAspectFit;
    ligtningImageView.image = [UIImage imageNamed:@"power-lightning.png"];
    [self addSubview:ligtningImageView];
    
    UIDevice *device = [UIDevice currentDevice];
    device.batteryMonitoringEnabled = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryChanged:) name:UIDeviceBatteryLevelDidChangeNotification object:device];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryChanged:) name:UIDeviceBatteryStateDidChangeNotification object:device];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryChanged:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self batteryChanged:nil];
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [imageView release];
    [ligtningImageView release];
    [super dealloc];
    
}


- (void)batteryChanged:(NSNotification *)notification {
    
    UIDevice *device = [UIDevice currentDevice];
    BOOL hidden = TRUE;
    BOOL lightningHidden = TRUE;
    float batteryLevel = device.batteryLevel;
    UIDeviceBatteryState batteryState = device.batteryState;
    
    if (batteryState != UIDeviceBatteryStateFull && batteryState != UIDeviceBatteryStateUnknown && batteryLevel < 0.25) {
        
        hidden = FALSE;
        if (batteryState == UIDeviceBatteryStateCharging)
            lightningHidden = FALSE;
    }
    
    hidden = hidden | (!_visible);
    if (self.hidden != hidden){
        
        self.hidden = hidden;
        [self.layer addAnimation:self.fadeAnimation forKey:nil];
    }
    
    if (ligtningImageView.hidden != lightningHidden){
        
        ligtningImageView.hidden = lightningHidden;
        [ligtningImageView.layer addAnimation:self.fadeAnimation forKey:nil];
    }
}

-(CATransition*)fadeAnimation{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3f;
    transition.timingFunction = UIViewAnimationCurveEaseInOut;
    transition.type = kCATransitionFade;
    return transition;
}

@end