//
//  RTTextView.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 8/5/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "RTTextView.h"
#import "KeyboardHelper.h"

@implementation RTTextView

- (void)initDefaults {
    [super setDelegate:self];
    self.autocorrectionType = UITextAutocapitalizationTypeNone;
    if ([self respondsToSelector:@selector(inputAssistantItem)]) {
        UITextInputAssistantItem *inputAssistantItem = [self inputAssistantItem];
        inputAssistantItem.leadingBarButtonGroups = @[];
        inputAssistantItem.trailingBarButtonGroups = @[];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaults];
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initDefaults];
}

- (void)setDelegate:(id<UITextViewDelegate>)delegate {
    _internalDelegate = delegate;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
        return [_internalDelegate textView:textView shouldChangeTextInRange:range replacementText:text];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [_internalDelegate textViewDidBeginEditing:textView];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewDidChange:)]) {
        [_internalDelegate textViewDidChange:textView];
    }
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewDidChangeSelection:)]) {
        [_internalDelegate textViewDidChangeSelection:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewDidEndEditing:)]) {
        [_internalDelegate textViewDidEndEditing:textView];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    BOOL result = YES;
    
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewShouldBeginEditing:)]) {
        result = [_internalDelegate textViewShouldBeginEditing:textView];
    }
    
    return result;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    if (_internalDelegate && [_internalDelegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        return [_internalDelegate textViewShouldEndEditing:textView];
    }
    return YES;
}

@end
