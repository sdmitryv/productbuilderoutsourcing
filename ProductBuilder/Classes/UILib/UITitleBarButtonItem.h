//
//  UITitleBarButtonItem.m
//  Created by Guillaume Cerquant - MacMation on 09/08/12.
//

/*
 * A UIBarButtonItem that does not show any highlight on the touch
 * Drag and drop a normal UIBarButtonItem in your xib and set its subclass to UITitleBarButtonItem
 */
@interface UITitleBarButtonItem : UIBarButtonItem

- (void)disable;
@end
