//
//  RTTableView.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/16/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTUITableView : UITableView
@property (nonatomic, assign) CGFloat additionalOffset;
@end


@interface RTUITableCellBackView : UIView
@property (nonatomic, retain) UIColor *backgroundColor;
@property (nonatomic, retain) UIColor *separatorColor;
@property (nonatomic, assign) UIEdgeInsets insets;
+(CGFloat)RTUITableViewCellStandardInset;
@end
