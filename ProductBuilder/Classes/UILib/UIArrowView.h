//
//  UIArrowView.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 4/4/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UIArrowViewType) {UIArrowViewTypeLeft = 0, UIArrowViewTypeRight = 1, UIArrowViewTypeTop = 2, UIArrowViewTypeBottom =3 };

@interface UIArrowView : UIView

@property(nonatomic, assign) UIArrowViewType type;
@property(nonatomic, retain) UIColor* arrowColor;

@end
