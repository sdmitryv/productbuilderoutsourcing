//
//  SDCAlertViewButton.h
//  SDCAlertView
//
//  Created by valery on 2/13/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDCAlertViewButton : UIButton{
}
@property (nonatomic, readonly)UIEdgeInsets insets;
@property (nonatomic, assign)NSUInteger index;
@property (nonatomic, assign)BOOL isDefault;
-(void)initializeDefaults;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
@end
