//
//  SDCAlertViewCell.m
//  SDCAlertView
//
//  Created by valery on 2/13/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCAlertViewCell.h"
#import "SDCAlertViewButton.h"

@interface SDCAlertViewCell(){
    NSMutableArray* buttons;
}
@end

@implementation SDCAlertViewCell

-(id)initWithNumberOfButtons:(NSUInteger)numberOfButtons defaultButtonIndex:(NSUInteger)defaultButtonIndex reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        buttons = [[NSMutableArray alloc]initWithCapacity:numberOfButtons];
        SDCAlertViewButton *previousButton = nil;
        for (NSUInteger i = 0; i < numberOfButtons; i++){
            SDCAlertViewButton* button = [self initializeButton];
            if (i==defaultButtonIndex) button.isDefault = TRUE;
            button.translatesAutoresizingMaskIntoConstraints = NO;
            
            [buttons addObject:button];
            [self.contentView addSubview:button];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:button.insets.top]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-button.insets.bottom]];

            if(previousButton) {
                [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:previousButton attribute:NSLayoutAttributeRight multiplier:1.0f constant:button.insets.left + previousButton.insets.right]];
                [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:previousButton attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f]];
            } else {
                [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:button.insets.left]];

            }
            previousButton = button;
        }
        if (previousButton){
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:previousButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-previousButton.insets.right]];
        }

    }
    return self;
}

- (UIEdgeInsets)layoutMargins{
    return UIEdgeInsetsZero;
}

-(NSArray*)buttons{
    return buttons;
}

-(SDCAlertViewButton*)initializeButton{
    return [SDCAlertViewButton buttonWithType:UIButtonTypeCustom];
}

@end
