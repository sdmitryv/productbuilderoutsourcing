//
//  SDCAlertViewCell.h
//  SDCAlertView
//
//  Created by valery on 2/13/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDCAlertViewButton;

@interface SDCAlertViewCell : UITableViewCell

@property (nonatomic, readonly)NSArray* buttons;
-(id)initWithNumberOfButtons:(NSUInteger)numberOfButtons defaultButtonIndex:(NSUInteger)defaultButtonIndex reuseIdentifier:(NSString *)reuseIdentifier;


@end
