//
//  SDCBlackAlertView.m
//  SDCAlertView
//
//  Created by valery on 2/14/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCBlackAlertView.h"
#import "SDCBlackAlertViewContentView.h"
#import "SDCBlackAlertViewBackgroundView.h"

@interface SDCBlackAlertView(Private)

+(CGFloat)SDCAlertViewCornerRadius;

@end

@implementation SDCBlackAlertView

-(void)initLayer{
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.2f;
    self.layer.shadowOffset = CGSizeMake(10, 10);
    self.layer.contentsScale = [UIScreen mainScreen].scale;
    self.layer.allowsEdgeAntialiasing = TRUE;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.class.SDCAlertViewCornerRadius].CGPath;
}

-(SDCAlertViewBackgroundView *)initializeAlertBackgroundView{
    return [[SDCBlackAlertViewBackgroundView alloc] init];
}

-(SDCAlertViewContentView *)initializeAlertContentView{
    return [[SDCBlackAlertViewContentView alloc] initAlertView:self];
}

//-(UIEdgeInsets) SDCAlertViewPadding{
//    return (UIEdgeInsets){3, 2, 3, 2};
//}

@end
