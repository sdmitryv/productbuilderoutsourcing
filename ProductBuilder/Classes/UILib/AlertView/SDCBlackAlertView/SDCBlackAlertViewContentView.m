//
//  SDCAlertViewBlackContentView.m
//  SDCAlertView
//
//  Created by valery on 2/14/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCBlackAlertViewContentView.h"
#import "SDCBlackAlertViewCell.h"
#import "SDCBlackAlertView.h"

@implementation SDCBlackAlertViewContentView



+ (UIColor *)sdc_alertButtonTextColor {
    return [UIColor whiteColor];
}

+ (UIColor *)sdc_alertMessageColor {
    //return [UIColor whiteColor];
    return [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
}

+ (UIColor *)sdc_alertTitleColor {
    return [UIColor whiteColor];
}

+ (UIFont *)sdc_suggestedButtonFont {
	return [UIFont boldSystemFontOfSize:17];
}

+ (UIFont *)sdc_normalButtonFont {
	return [self.class sdc_suggestedButtonFont];
}

+(UIEdgeInsets)SDCAlertViewTextFieldBackgroundViewPadding{
    return (UIEdgeInsets){20, 5, 0, 5};
}

-(BOOL)useSeparator{
    return TRUE;
}

-(SDCAlertViewCell*)initializeAlertViewCellWithNumberOfButtons:(NSUInteger)buttonsCount defaultButtonIndex:(NSUInteger)defaultButtonIndex{
    return [[SDCBlackAlertViewCell alloc] initWithNumberOfButtons:buttonsCount defaultButtonIndex:defaultButtonIndex reuseIdentifier:nil];
}


+(CGFloat)heightForButtonView:(BOOL)isDefault{
    //button height + top and bottom insets
    //43 + 3 * 2
    return isDefault ? 63.0f : 49.0f;
}

-(void)initLayer{
    self.layer.masksToBounds = YES;
    self.layer.allowsEdgeAntialiasing = TRUE;
    self.layer.cornerRadius = SDCBlackAlertView.SDCAlertViewCornerRadius;
}

@end
