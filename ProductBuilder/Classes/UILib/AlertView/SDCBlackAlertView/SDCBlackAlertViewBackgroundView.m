//
//  SDCAlertViewBlackBackgroundView.m
//  SDCAlertView
//
//  Created by valery on 2/14/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCBlackAlertViewBackgroundView.h"
#import "SDCBlackAlertView.h"

@implementation SDCBlackAlertViewBackgroundView

//- (void)drawRect:(CGRect)rect
//{
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextClearRect(context, rect);
//    CGContextSetShouldAntialias(context, true);
//    CGContextSetAllowsAntialiasing(context, true);
//    CGContextSetLineWidth(context, 0.0);
//    CGContextSetAlpha(context, 0.8);
//    CGFloat backOffset = 2.0f;
//    //border
//    CGContextSetLineWidth(context, backOffset);
//    CGContextSetStrokeColorWithColor(context, [[UIColor colorWithWhite:0.12f alpha:1.0f] CGColor]);
//    CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
//    
//    // Draw background
//    CGRect backRect = CGRectMake(rect.origin.x + backOffset,
//                                 rect.origin.y + backOffset,
//                                 rect.size.width - backOffset*2,
//                                 rect.size.height - backOffset*2);
//    
//    [self.class drawRoundedRect:backRect inContext:context withRadius:7];
//    CGContextDrawPath(context, kCGPathFillStroke);
//    
//    // Clip Context
//    CGRect clipRect = CGRectMake(backRect.origin.x + backOffset-1,
//                                 backRect.origin.y + backOffset-1,
//                                 backRect.size.width - (backOffset-1)*2,
//                                 backRect.size.height - (backOffset-1)*2);
//    
//    [self.class drawRoundedRect:clipRect inContext:context withRadius:7];
//    CGContextClip (context);
//    
//    //Draw highlight
//    CGGradientRef glossGradient;
//    CGColorSpaceRef rgbColorspace;
//    size_t num_locations = 2;
//    CGFloat locations[2] = { 0.0, 1.0 };
//    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35, 1.0, 1.0, 1.0, 0.06 };
//    rgbColorspace = CGColorSpaceCreateDeviceRGB();
//    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace,
//                                                        components, locations, num_locations);
//    
//    CGRect ovalRect = CGRectMake(-130, -115, (rect.size.width*2),
//                                 rect.size.width/2);
//    
//    CGPoint start = CGPointMake(rect.origin.x, rect.origin.y);
//    CGPoint end = CGPointMake(rect.origin.x, rect.size.height/5);
//    
//    CGContextSetAlpha(context, 1.0);
//    CGContextAddEllipseInRect(context, ovalRect);
//    CGContextClip (context);
//    
//    CGContextDrawLinearGradient(context, glossGradient, start, end, 0);
//    
//    CGGradientRelease(glossGradient);
//    CGColorSpaceRelease(rgbColorspace);
//}

+ (void) drawRoundedRect:(CGRect) rrect inContext:(CGContextRef) context
			  withRadius:(CGFloat) radius
{
    CGPathRef path = [UIBezierPath bezierPathWithRoundedRect:rrect cornerRadius:radius].CGPath;
    CGContextAddPath(context, path);
}


- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Make sure the context is cleared
    CGContextClearRect(context, rect);
    CGContextSetShouldAntialias(context, true);
    // Set the closest matching blend mode for the context. It's really
    // difficult (if possible at all) to discern what Apple is using for this
    // but it produces a nearly identical effect, which serves our needs.
    CGContextSetBlendMode(context, kCGBlendModeOverlay);
    
    // Compensate for whatever the other layer Apple has in the hierachy (or perhaps a color-fill somewhere?) that's darkening the alert:
    //
    // [<CALayer: 0x155faec0> backgroundColor]: <CGColor 0x17551e00> [<CGColorSpace 0x175af850> (kCGColorSpaceDeviceGray)] ( 0.5 0.5 )
    //
    // Darken the alert background prior to filling in the actual background color
    //
    // NOTE: Had to manually tweak this value to compensate for subtle brightness differences. % Grayscale is -0.6f from
    //       Apple's apparent value.
    CGContextSetGrayFillColor(context, 0.05, 0.8);
    //CGContextFillRect(context, rect);
    [self.class drawRoundedRect:rect inContext:context withRadius:SDCBlackAlertView.SDCAlertViewCornerRadius];
    CGContextDrawPath(context, kCGPathFill);
    
    
    // Background color components:
    //
    // [<CALayer: 0x155faec0> backgroundColor]: <CGColor 0x15541550> [<CGColorSpace 0x1558f4c0> (kCGColorSpaceDeviceGray)] ( 0.97 0.96 )
    //
    // NOTE: Had to manually tweak this value to compensate for almost-negligible transparency discrepancies.
    //       Alpha value is now +0.1f from Apple's apparent value.
    CGContextSetGrayFillColor(context, 0.27f, 0.97f);
//    //CGContextFillRect(context, rect);
    [self.class drawRoundedRect:rect inContext:context withRadius:SDCBlackAlertView.SDCAlertViewCornerRadius];
    CGContextClip (context);
    
}

@end
