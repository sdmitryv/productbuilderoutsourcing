//
//  SDCBlackAlertViewCell.m
//  SDCAlertView
//
//  Created by valery on 2/14/14.
//  Copyright (c) 2014 Scotty Doesn't Code. All rights reserved.
//

#import "SDCBlackAlertViewCell.h"
#import "SDCBlackAlertViewButton.h"

@implementation SDCBlackAlertViewCell

-(SDCAlertViewButton*)initializeButton{
    return [SDCBlackAlertViewButton buttonWithType:UIButtonTypeCustom];
}

@end
