//
//  SDCAlertView.m
//  SDCAlertView
//
//  Created by Scott Berrevoets on 9/20/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//
#import "SDCAlertView.h"
#import "SDCAlertViewCoordinator.h"
#import "SDCAlertViewBackgroundView.h"
#import "SDCAlertViewContentView.h"

#import "UIView+SDCAutoLayout.h"

typedef struct{
    BOOL clickedButtonAtIndex;
    BOOL cancel;
    BOOL willPresent;
    BOOL didPresent;
    BOOL willDismissWithButtonIndex;
    BOOL didDismissWithButtonIndex;
    BOOL shouldEnableFirstOtherButton;
} SDCAlertViewDelegateImplementedMethods;

#pragma mark - SDCAlertView

@interface SDCAlertView () {
    SDCAlertViewDelegateImplementedMethods delegateMethods;
}
@property (nonatomic, strong) SDCAlertViewBackgroundView *alertBackgroundView;
@property (nonatomic, strong) SDCAlertViewContentView *alertContentView;

@property (nonatomic, strong) NSMutableArray *buttonTitles;
@property (nonatomic, strong) NSLayoutConstraint *bottomSpaceConstraint;
@property (nonatomic) NSInteger firstOtherButtonIndex;
@end

@implementation SDCAlertView

#pragma mark geometry


-(CGFloat)SDCAlertViewWidth{
    return 270;
};

-(UIEdgeInsets) SDCAlertViewPadding{
    return (UIEdgeInsets){0, 0, 0, 0};
}

+(CGFloat)SDCAlertViewCornerRadius{
    return 7;
};

+(UIOffset)SDCAlertViewParallaxSlideMagnitude{
    return (UIOffset){15.75, 15.75};
}

+(NSInteger) SDCAlertViewUnspecifiedButtonIndex {
    return -1;
};
+(NSInteger)SDCAlertViewDefaultFirstButtonIndex{
    return 0;
};

#pragma  mark -

-(void)setDelegate:(id)aDelegate{
    _delegate = aDelegate;
    delegateMethods.clickedButtonAtIndex = [_delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)];
    delegateMethods.cancel = [_delegate respondsToSelector:@selector(alertViewCancel:)];
    delegateMethods.willPresent = [_delegate respondsToSelector:@selector(willPresentAlertView:)];
    delegateMethods.didPresent = [_delegate respondsToSelector:@selector(didPresentAlertView:)];
    delegateMethods.willDismissWithButtonIndex = [_delegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)];
    delegateMethods.didDismissWithButtonIndex = [_delegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)];
    delegateMethods.shouldEnableFirstOtherButton = [_delegate respondsToSelector:@selector(alertViewShouldEnableFirstOtherButton:)];
}

#pragma mark - Getters

-(SDCAlertViewBackgroundView *)initializeAlertBackgroundView{
    return [[SDCAlertViewBackgroundView alloc] init];
}

- (SDCAlertViewBackgroundView *)alertBackgroundView {
	if (!_alertBackgroundView) {
		_alertBackgroundView = [self initializeAlertBackgroundView];
		[_alertBackgroundView setTranslatesAutoresizingMaskIntoConstraints:NO];
	}
	
	return _alertBackgroundView;
}

-(SDCAlertViewContentView *)initializeAlertContentView{
    return [[SDCAlertViewContentView alloc] initAlertView:self];
}

- (SDCAlertViewContentView *)alertContentView {
	if (!_alertContentView) {
		_alertContentView = [self initializeAlertContentView];
		[_alertContentView setTranslatesAutoresizingMaskIntoConstraints:NO];
	}
	
	return _alertContentView;
}

#pragma mark - Initialization

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                     delegate:(id)delegate
            cancelButtonTitle:(NSString *)cancelButtonTitle
             otherButtonTitle:(NSString *)otherButtonTitle {
    
    if (otherButtonTitle)
        return [self initWithTitle:title message:message delegate:delegate cancelButtonTitle: cancelButtonTitle otherButtonTitles: otherButtonTitle, nil];
    
    return [self initWithTitle:title message:message delegate:delegate cancelButtonTitle: cancelButtonTitle otherButtonTitles:nil];
}

- (instancetype)initWithTitle:(NSString *)title
					  message:(NSString *)message
					 delegate:(id)delegate
			cancelButtonTitle:(NSString *)cancelButtonTitle
			otherButtonTitles:(NSString *)otherButtonTitles, ... {
	self = [super init];
	
	if (self) {
		_title = title;
		_message = message;
		self.delegate = delegate;
		
		_cancelButtonIndex = self.class.SDCAlertViewUnspecifiedButtonIndex;
		_firstOtherButtonIndex = self.class.SDCAlertViewUnspecifiedButtonIndex;
		
		_buttonTitles = [NSMutableArray array];
		
		if (cancelButtonTitle) {
			_buttonTitles[0] = cancelButtonTitle;
			_cancelButtonIndex = self.class.SDCAlertViewDefaultFirstButtonIndex;
		}
		
		va_list argumentList;
		va_start(argumentList, otherButtonTitles);
		for (NSString *buttonTitle = otherButtonTitles; buttonTitle != nil; buttonTitle = va_arg(argumentList, NSString *))
			[self addButtonWithTitle:buttonTitle];
		
		[self setTranslatesAutoresizingMaskIntoConstraints:NO];
		[self initLayer];
	}
	
	return self;
}

-(void)initLayer{
    self.layer.masksToBounds = YES;
    self.layer.allowsEdgeAntialiasing = TRUE;
    self.layer.cornerRadius = self.class.SDCAlertViewCornerRadius;
}

#pragma mark - Visibility

- (BOOL)isVisible {
	return [[SDCAlertViewCoordinator sharedCoordinator] visibleAlert] == self;
}

#pragma mark - Presenting

- (void)show {
	[self configureForShowing];
	[[SDCAlertViewCoordinator sharedCoordinator] presentAlert:self];
}

- (void)showWithDismissHandler:(void (^)(NSInteger))dismissHandler {
	self.didDismissHandler = dismissHandler;
	[self show];
}

- (void)configureForShowing {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
        UIInterpolatingMotionEffect *horizontalParallax;
        UIInterpolatingMotionEffect *verticalParallax;
        
        horizontalParallax = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        horizontalParallax.minimumRelativeValue = @(-self.class.SDCAlertViewParallaxSlideMagnitude.horizontal);
        horizontalParallax.maximumRelativeValue = @(self.class.SDCAlertViewParallaxSlideMagnitude.horizontal);
        
        verticalParallax = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        verticalParallax.minimumRelativeValue = @(-self.class.SDCAlertViewParallaxSlideMagnitude.vertical);
        verticalParallax.maximumRelativeValue = @(self.class.SDCAlertViewParallaxSlideMagnitude.vertical);
        
        UIMotionEffectGroup *groupMotionEffect = [[UIMotionEffectGroup alloc] init];
        groupMotionEffect.motionEffects = @[horizontalParallax, verticalParallax];
        [self.alertContentView addMotionEffect:groupMotionEffect];
	}
	[self insertSubview:self.alertBackgroundView atIndex:0];
	
	[self configureContent];
	[self addSubview:self.alertContentView];
}

- (void)willBePresented {
	if (self.delegate && delegateMethods.willPresent)
		[self.delegate willPresentAlertView:(id)self];
}

- (void)wasPresented {
	if (self.delegate && delegateMethods.didPresent)
		[self.delegate didPresentAlertView:(id)self];
}

#pragma mark - Dismissing

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated {
	[[SDCAlertViewCoordinator sharedCoordinator] dismissAlert:self withButtonIndex:buttonIndex animated:animated];
}

- (void)willBeDismissedWithButtonIndex:(NSInteger)buttonIndex {
	if (delegateMethods.willDismissWithButtonIndex)
		[self.delegate alertView:(id)self willDismissWithButtonIndex:buttonIndex];
    
    if (self.willDismissHandler)
        self.willDismissHandler(buttonIndex);
}

- (void)wasDismissedWithButtonIndex:(NSInteger)buttonIndex {
	if (delegateMethods.didDismissWithButtonIndex)
		[self.delegate alertView:(id)self didDismissWithButtonIndex:buttonIndex];
	
	if (self.didDismissHandler)
		self.didDismissHandler(buttonIndex);
}

#pragma mark - First Responder

-(BOOL)isFirstResponder{
    return [self.alertContentView isFirstResponder];
}

-(BOOL)canBecomeFirstResponder{
    return [self.alertContentView canBecomeFirstResponder];
}

-(BOOL)canResignFirstResponder{
    return [self.alertContentView canResignFirstResponder];
}

- (BOOL)becomeFirstResponder {
    return [self.alertContentView becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    [super resignFirstResponder];
    return [self.alertContentView resignFirstResponder];
}

#pragma mark - Content

- (void)setTitle:(NSString *)title {
	_title = title;
	self.alertContentView.title = title;
}

- (void)setMessage:(NSString *)message {
	_message = message;
	self.alertContentView.message = message;
}

- (UIView *)contentView {
	return self.alertContentView.customContentView;
}

- (void)setNumberOfRows:(NSInteger)numberOfRows
{
    self.alertContentView.numberOfRows = numberOfRows;
}

-(NSInteger)numberOfRows{
    return self.alertContentView.numberOfRows;
}


- (void)setCancelButtonIndex:(NSInteger)cancelButtonIndex {
	_cancelButtonIndex = cancelButtonIndex;
	if (cancelButtonIndex != self.class.SDCAlertViewDefaultFirstButtonIndex)
		self.firstOtherButtonIndex = self.class.SDCAlertViewDefaultFirstButtonIndex;
}

- (void)configureContent {
	self.alertContentView.title = self.title;
	self.alertContentView.message = self.message;
	
	switch (self.alertViewStyle) {
		case UIAlertViewStyleDefault:
            self.alertContentView.numberOfTextFields = 0;
            break;
		case UIAlertViewStylePlainTextInput:
		case UIAlertViewStyleSecureTextInput:
            self.alertContentView.numberOfTextFields = 1;
            break;
		case UIAlertViewStyleLoginAndPasswordInput:
            self.alertContentView.numberOfTextFields = 2;
            break;
	}
}

#pragma mark - SDCAlertViewContentViewDelegate

- (BOOL)alertContentViewShouldUseSecureEntryForPrimaryTextField:(SDCAlertViewContentView *)sender {
	return self.alertViewStyle == UIAlertViewStyleSecureTextInput;
}

- (CGFloat)maximumHeightForAlertContentView:(SDCAlertViewContentView *)sender {
	return CGRectGetHeight(self.superview.bounds) - self.SDCAlertViewPadding.top - self.SDCAlertViewPadding.bottom;
}

- (void)alertContentView:(SDCAlertViewContentView *)sender didTapButtonAtIndex:(NSUInteger)index {
	[self tappedButtonAtIndex:index];
}

- (BOOL)alertContentView:(SDCAlertViewContentView *)sender shouldEnableButtonAtIndex:(NSUInteger)index {
    
	if (index == self.firstOtherButtonIndex && delegateMethods.shouldEnableFirstOtherButton)
		return [self.delegate alertViewShouldEnableFirstOtherButton:(id)self];
	
	return YES;
}

#pragma mark - Buttons & Text Fields

- (void)tappedButtonAtIndex:(NSInteger)index {
    [self resignFirstResponder];
	[self dismissWithClickedButtonIndex:index animated:YES];
    if (self.delegate && delegateMethods.clickedButtonAtIndex)
        [self.delegate alertView:(id)self clickedButtonAtIndex:index];
    
    if (self.clickedButtonHandler)
        self.clickedButtonHandler(index);
}

- (NSInteger)addButtonWithTitle:(NSString *)title {
	[self.buttonTitles addObject:title];
	
	if (self.firstOtherButtonIndex == self.class.SDCAlertViewUnspecifiedButtonIndex) {
		if (self.cancelButtonIndex == self.class.SDCAlertViewUnspecifiedButtonIndex)
			self.firstOtherButtonIndex = self.class.SDCAlertViewDefaultFirstButtonIndex;
		else
			self.firstOtherButtonIndex = self.cancelButtonIndex + 1;
	}
	
	return [self.buttonTitles indexOfObject:title];
}

- (NSInteger)numberOfButtons {
	return [self.buttonTitles count];
}

- (NSString *)buttonTitleAtIndex:(NSInteger)index {
	return self.buttonTitles[index];
}

- (UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex {
	return self.alertContentView.textFields[textFieldIndex];
}

#pragma mark - Layout

- (void)updateConstraints {
    [self removeConstraints:[self constraints]];
    
    [self.alertBackgroundView sdc_centerInSuperview];
    [self.alertBackgroundView sdc_pinWidthToWidthOfView:self];
    [self.alertBackgroundView sdc_pinHeightToHeightOfView:self];
    
    [self.alertContentView sdc_alignEdges:UIRectEdgeAll withView:self insets:(UIEdgeInsets){.left= self.SDCAlertViewPadding.left, .right = -self.SDCAlertViewPadding.right, .top = self.SDCAlertViewPadding.top,.bottom = -self.SDCAlertViewPadding.bottom}];
    
    for(NSInteger i =  self.superview.constraints.count - 1; i >= 0; i--){
        NSLayoutConstraint* constraint = self.superview.constraints[i];
        if (constraint.firstItem==self){
            [self.superview removeConstraint:constraint];
        }
    }
    [self sdc_pinWidth:self.SDCAlertViewWidth];
//    [self sdc_centerInSuperview];

    [self sdc_horizontallyCenterInSuperviewWithOffset:UIOffsetZero.horizontal];
    NSLayoutConstraint *verticalCenterConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:UIOffsetZero.vertical];
    verticalCenterConstraint.priority = 999;
    [self.superview addConstraint:verticalCenterConstraint];

    self.bottomSpaceConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.superview attribute:NSLayoutAttributeBottom multiplier:1 constant:-self.bottomSpacing];
    [self.superview addConstraint:self.bottomSpaceConstraint];
    
	[super updateConstraints];
}

-(void)setBottomSpacing:(CGFloat)bottomSpacing{
    _bottomSpacing = bottomSpacing;
    self.bottomSpaceConstraint.constant = -_bottomSpacing;
}

- (void)positionSelf {
    
}

#pragma mark - Cleanup

- (void)dealloc {
	
}

@end
