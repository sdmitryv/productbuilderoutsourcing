//
//  UIView+RequireSign.h
//  ProductBuilder
//
//  Created by Artem Nikulchenko on 2/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SignPostion) {SignPostionRight = 0, SignPostionLeft = 1 };

@interface UIView (RequireSign)
@property (nonatomic, readonly)UILabel* signLabel;
@property (nonatomic, retain)NSString* sign;
@property (nonatomic, assign)SignPostion signPosition;
-(void)hideSign:(BOOL)hidden;

@end
