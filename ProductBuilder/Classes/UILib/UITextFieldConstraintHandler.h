//
//  UITextFieldConstraintHandler.h
//  ProductBuilder
//
//  Created by valera on 10/31/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITextFieldConstraintHandler : NSObject<UITextFieldDelegate>{

	NSMutableCharacterSet *nonAplhaNumericSet;
	UITextField *inputTextField;
    id<UITextFieldDelegate> delegate;
    NSInteger maxTextLength;
}

@property (nonatomic, assign)UITextField *inputTextField;
@property (nonatomic, assign)id<UITextFieldDelegate> delegate;
@property (nonatomic, assign) NSInteger maxTextLength;

@end
