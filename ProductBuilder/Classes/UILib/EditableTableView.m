//
//  EditableTableView.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "EditableTableView.h"
#import "QuartzCore/QuartzCore.h"

#define MAX_ITEMS_DEFAULT	6

@interface EditableTableView(Private)
-(void)initDefaults;
@end


@implementation EditableTableView

@synthesize itemTableView;
@synthesize itemList;
@synthesize dataDelegate;
@synthesize maxItems;

-(void)initDefaults{
	self.backgroundColor = [UIColor clearColor];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	backButton.frame = self.bounds;
	backButton.backgroundColor = [UIColor clearColor];
//	backButton.enabled = NO;
	backButton.clipsToBounds = YES;
	[self addSubview:backButton];
	itemTableView = [[UITableView alloc] initWithFrame:backButton.bounds style:UITableViewStylePlain];
	itemTableView.backgroundView = nil;
	itemTableView.backgroundColor = [UIColor clearColor];
	itemTableView.delegate = self;
	itemTableView.dataSource = self;
	itemTableView.sectionHeaderHeight = 0.0;
	itemTableView.rowHeight = 30;
	self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | 
		UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	itemTableView.layer.masksToBounds = YES;
	itemTableView.layer.cornerRadius = 12.0;
	[backButton addSubview:itemTableView];
	maxItems = MAX_ITEMS_DEFAULT;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
	if ((self = [super initWithCoder:aDecoder])){
		[self initDefaults];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self != nil) {
		[self initDefaults];
	}
	return self;
}

- (void)dealloc {
	[itemTableView release];
	[itemList release];
    [super dealloc];
}

- (void)setItemList: (NSMutableArray*)items {
	if (itemList != items) {
		[itemList release];
		itemList = [items retain];
		[itemList sortUsingComparator:^(id obj1, id obj2) {
			return [[obj1 description] localizedCaseInsensitiveCompare:[obj2 description]];
		}];
		[itemTableView reloadData];
	}
}

-(NSInteger)rowIndexForTextField:(UITextField *)textField {
	NSArray *visibleIndexPaths = [itemTableView indexPathsForVisibleRows];
	for (NSIndexPath *indexPath in visibleIndexPaths) {
		UITableViewCell *cell = [itemTableView cellForRowAtIndexPath:indexPath];
		UITextField *tf = [cell.contentView viewWithTag:1];
		if (tf == textField) {
			return indexPath.row;
		}
	}
	return -1;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	UITextField * textField = [cell.contentView viewWithTag:1];
	textField.enabled = YES;
	[textField becomeFirstResponder];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	UITextField * textField = [cell.contentView viewWithTag:1];
	textField.enabled = NO;
	return indexPath;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger count = [itemList count];
    return count < maxItems ? count + 1 : maxItems;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"RowCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	UITextField *textField;

    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 2, self.frame.size.width - 20, 20)];
		textField.tag = 1;
		textField.borderStyle = UITextBorderStyleNone;
		textField.font = [UIFont systemFontOfSize:16.0];
		textField.textAlignment = NSTextAlignmentLeft;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		textField.delegate = self;
//		textField.enabled = NO;
		[cell.contentView addSubview:textField];
		[textField release];			
    }
	else {
		textField = [cell.contentView viewWithTag:1];
	}

	NSIndexPath* selectedIndexPath = [tableView indexPathForSelectedRow];
	if ((selectedIndexPath != nil) && (indexPath.row == selectedIndexPath.row)) {
		textField.enabled = dataDelegate != nil;
	}

    if (indexPath.row < itemList.count) {
		textField.textColor = [UIColor blackColor];
		textField.text = [itemList[indexPath.row] description];
	}
	else {
		textField.textColor = [UIColor lightGrayColor];
		textField.text = @"Click to Add";
	}

	
	return cell;
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	textField.textColor = [UIColor blackColor];
	NSInteger row = [self rowIndexForTextField:textField];
	if (row >= itemList.count) {
		textField.text = @"";
	}
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
	textField.enabled = NO;
	NSInteger row = [self rowIndexForTextField:textField];
	if (row < itemList.count) {
		if ([textField.text isEqual:@""]) {
			[dataDelegate editableTableView:self deleteItemAtIndex:row];
		}
		else {
			[dataDelegate editableTableView:self renameItemAtIndex:row newName:textField.text];
		}
	}
	else {
		if (![textField.text isEqual:@""]) {
			[dataDelegate editableTableView:self addItem:textField.text];
		}
	}
	
	[textField resignFirstResponder];
	[itemTableView reloadData];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
 	if (textField.text.length - range.length + string.length > 10) {
		return NO;
	}
	return YES;
}

@end
