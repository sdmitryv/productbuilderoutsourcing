//
//  RTTabView.h
//  iPadPOS
//
//  Created by valera on 3/21/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOButton.h"

@class RTTabView;

@interface RTTabButton : MOButton
{
	NSObject*	object;
}

@property(nonatomic,readonly) NSObject* object;

+(id)buttonWithObject:(NSObject*)value;

@end

@protocol RTTabViewDelegate

-(void)rtTabView:(RTTabView*)tabView didSelectTab:(RTTabButton*)tab;

@end



@interface RTTabView : UIView {
	NSArray*				dataSource;
	CGFloat					tabSpace;
	CGFloat					tabWidth;
	NSMutableArray*			tabs;
	NSInteger				currentTabIndex;
	UIColor*				tabTextColor;
	UIColor*				tabBackground;
	UIColor*				tabSelectedTextColor;
	UIColor*				tabSelectedBackground;
	id<RTTabViewDelegate>	delegate;
	
}

@property (nonatomic, retain)NSArray* dataSource;
@property (nonatomic, assign)CGFloat tabSpace;
@property (nonatomic, assign)CGFloat tabWidth;
@property (nonatomic, assign)NSInteger currentTabIndex;
@property (nonatomic, assign)RTTabButton* currentTab;
@property (nonatomic, retain)UIColor* tabTextColor;
@property (nonatomic, retain)UIColor* tabBackground;
@property (nonatomic, retain)UIColor* tabSelectedTextColor;
@property (nonatomic, retain)UIColor* tabSelectedBackground;
@property (nonatomic, assign)id<RTTabViewDelegate> delegate;
@end
