//
//  RTPopoverController.m
//  ProductBuilder
//
//  Created by valery on 12/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "RTPopoverController.h"
#import "UIView+FirstResponder.h"
#import "NSObject+MethodExchange.h"
#import "Global.h"

@implementation UIViewController (PopoverPresentationController)

+(void)load{
    [[self class] exchangeMethod:@selector(popoverPresentationController) withNewMethod:@selector(popoverPresentationControllerImpl)];
}

-(UIPopoverPresentationController*)popoverPresentationControllerImpl{
    UIPopoverPresentationController*  popoverPresentationController = [self popoverPresentationControllerImpl];
    return popoverPresentationController;
}

@end


typedef struct{
    BOOL shouldDismissPopover;
    BOOL didDismissPopover;
    BOOL willRepositionPopoverToRect;
    
} DelegateImplementedMethods;

@interface RTPopoverController(){
    DelegateImplementedMethods _delegateMethods;
}

@end

@implementation RTPopoverController

-(instancetype)initWithContentViewController:(UIViewController*)controller{
    if ((self = [super init])){
        _contentViewController = [controller retain];
    }
    return self;
}

-(void)dealloc{
    [_contentViewController release];
    [super dealloc];
}

-(void)setDelegate:(id<RTPopoverControllerDelegate>)aDelegate{
    _delegate = aDelegate;
    _delegateMethods.shouldDismissPopover = [_delegate respondsToSelector:@selector(popoverControllerShouldDismissPopover:)];
    _delegateMethods.didDismissPopover = [_delegate respondsToSelector:@selector(popoverControllerDidDismissPopover:)];
    _delegateMethods.willRepositionPopoverToRect = [_delegate respondsToSelector:@selector(popoverPresentationController:willRepositionPopoverToRect:inView:)];
}

- (void)presentPopoverFromBarButtonItem:(UIBarButtonItem *)item permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated{
    _contentViewController.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController* popoverPresentationController = _contentViewController.popoverPresentationController;
    popoverPresentationController.permittedArrowDirections = arrowDirections;
    popoverPresentationController.delegate = self;
    popoverPresentationController.barButtonItem = item;
    UIViewController* parentViewController = [[UIApplication sharedApplication].windows[0] rootViewController];
    [parentViewController presentViewController:_contentViewController animated:animated completion:nil];
}

-(void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated{
    
    if (self.isPopoverVisible) {
        return;
    }
    
    _contentViewController.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController* popoverPresentationController = _contentViewController.popoverPresentationController;
    popoverPresentationController.permittedArrowDirections = arrowDirections;
    popoverPresentationController.delegate = self;
    popoverPresentationController.sourceView = view;
    popoverPresentationController.sourceRect = rect;
    UIViewController* parentViewController = [view firstAvailableUIViewController];
    [parentViewController presentViewController:_contentViewController animated:animated completion:nil];
}

- (void)dismissPopoverAnimated:(BOOL)animated{
    [_contentViewController dismissViewControllerAnimated:animated completion:nil];
}

- (void)setPopoverContentSize:(CGSize)size{
    _contentViewController.preferredContentSize = size;
}

- (CGSize)popoverContentSize:(CGSize)size{
    return _contentViewController.preferredContentSize;
}


-(void)setPassthroughViews:(NSArray<__kindof UIView *> *)passthroughViews{
    _contentViewController.popoverPresentationController.passthroughViews = passthroughViews;
}


-(NSArray*)passthroughViews{
    return _contentViewController.popoverPresentationController.passthroughViews;
}

-(BOOL)isPopoverVisible{
    return _contentViewController.presentingViewController != nil;
}

#pragma mark delegate methods

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    if (_delegate && _delegateMethods.shouldDismissPopover){
        return [_delegate popoverControllerShouldDismissPopover:self];
    }
    return YES;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    if (_delegate && _delegateMethods.didDismissPopover){
        return [_delegate popoverControllerDidDismissPopover:self];
    }
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView  * __nonnull * __nonnull)view{
    if (_delegate && _delegateMethods.willRepositionPopoverToRect){
        [_delegate popoverController:self willRepositionPopoverToRect:rect inView:view];
    }
}


@end
