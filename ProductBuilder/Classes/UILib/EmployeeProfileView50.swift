//
//  EmployeeProfileTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 11/17/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class EmployeeProfileView50: UICollectionViewCell {
    
    
    @IBOutlet weak var photoView: EmployeePhotoView!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let nibName = "EmployeeProfileView50"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var employee: Employee? {
        didSet {
            photoView.employee = employee
            nameLabel.text = employee?.shortName
        }
    }

    class func instanceFromNib() -> UICollectionViewCell {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UICollectionViewCell
    }
}
