//
//  MyClass.h
//  CloudworksPOS
//
//  Created by valera on 8/8/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef NS_OPTIONS(NSUInteger, UIViewRoundedCornerMask) {
    UIViewRoundedCornerNone = 0,
    UIViewRoundedCornerUpperLeft = 1 << 0,
    UIViewRoundedCornerUpperRight = 1 << 1,
    UIViewRoundedCornerLowerLeft = 1 << 2,
    UIViewRoundedCornerLowerRight = 1 << 3,
    UIViewRoundedCornerAll = (1 << 4) - 1,
};


@interface UIRoundedCornersView : UIView {
    BOOL customRoundedCorners;
    UIViewRoundedCornerMask corners;
    CGFloat radius;
    CGSize lastLayoutedSize;
    BOOL defaultsInitited;
}
@property(nonatomic, readonly)UIViewRoundedCornerMask corners;
@property(nonatomic, readonly)CGFloat radius;
-(void)setRoundedCorners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius;
+(void)drawRoundedCorners:(UIView*)view corners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius;
+(void)drawLayerRoundedCorners:(CALayer*)layer corners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius;
+(void)drawRoundedCorners:(UIView*)view radius:(CGFloat)radius;
-(CAShapeLayer*)borderLayer;
@end


@interface UIView(RoundedCorners)
-(void)drawRoundedCorners:(UIViewRoundedCornerMask)corners radius:(CGFloat)radius;
-(void)drawRoundedCornersWithRadius:(CGFloat)radius;

@end