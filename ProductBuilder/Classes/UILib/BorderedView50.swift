//
//  BorderedView.swift
//  RPlusTeamwork
//
//  Created by Alexander Martyshko on 7/7/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class BorderedView: UIView {
    
    static let BorderNone = 0
    static let BorderTop = 1 << 0
    static let BorderRight = 1 << 1
    static let BorderBottom = 1 << 2
    static let BorderLeft = 1 << 3
    static let BorderAll = (1 << 4) - 1
    
    var borderMask = 0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var borderSize = 1 / UIScreen.main.scale
    var borderColor = ColorUtils.ColorWithRGB(red: 200.0, green: 199.0, blue: 204.0)
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()
        context?.setShouldAntialias(false )
        
        let lineHeight = CGFloat(borderSize)
        context?.setStrokeColor(borderColor.cgColor);
        context?.setLineWidth(lineHeight);
        
        backLeft.constant = 0
        backTop.constant = 0
        backRight.constant = 0
        backBottom.constant = 0
        
        blurLeft.constant = 0
        blurTop.constant = 0
        blurRight.constant = 0
        blurBottom.constant = 0
        
        if (borderMask & BorderedView.BorderTop) != 0 {
            context?.beginPath();
            context?.move(to: CGPoint(x: 0, y: lineHeight));
            context?.addLine(to: CGPoint(x: rect.size.width, y: lineHeight));
            context?.strokePath();
            
            backTop.constant = borderSize
            blurTop.constant = borderSize
        }
        
        if (borderMask & BorderedView.BorderRight) != 0 {
            context?.beginPath();
            context?.move(to: CGPoint(x: rect.size.width-lineHeight, y: 0));
            context?.addLine(to: CGPoint(x: rect.size.width-lineHeight, y: rect.size.height));
            context?.strokePath();
            
            backRight.constant = -borderSize
            blurRight.constant = -borderSize
        }
        
        if (borderMask & BorderedView.BorderBottom) != 0 {
            context?.beginPath();
            context?.move(to: CGPoint(x: 0, y: rect.size.height));
            context?.addLine(to: CGPoint(x: rect.size.width, y: rect.size.height));
            context?.strokePath();
            
            backBottom.constant = -borderSize
            blurBottom.constant = -borderSize
        }
        
        if (borderMask & BorderedView.BorderLeft) != 0 {
            context?.beginPath();
            context?.move(to: CGPoint(x: lineHeight, y: 0));
            context?.addLine(to: CGPoint(x: lineHeight, y: rect.size.height));
            context?.strokePath();
            
            backLeft.constant = borderSize
            blurLeft.constant = borderSize
        }
    }
    
    fileprivate var blurView: UIVisualEffectView!
    fileprivate var vibrancyView: UIVisualEffectView!
    fileprivate var backView: UIView!
    
    fileprivate var backLeft: NSLayoutConstraint!
    fileprivate var backTop: NSLayoutConstraint!
    fileprivate var backRight: NSLayoutConstraint!
    fileprivate var backBottom: NSLayoutConstraint!
    
    fileprivate var blurLeft: NSLayoutConstraint!
    fileprivate var blurTop: NSLayoutConstraint!
    fileprivate var blurRight: NSLayoutConstraint!
    fileprivate var blurBottom: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        
        self.backgroundColor = UIColor.clear
        
        backView = UIView()
        backView.backgroundColor = UIColor.white
        backView.alpha = 0.5
        backView.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(backView, at: 0)
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(blurView, at: 1)
        
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        //vibrancyView.contentView.backgroundColor = UIColor.white
        blurView.contentView.addSubview(vibrancyView)
        
        backLeft = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        backTop = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        backRight = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        backBottom = NSLayoutConstraint(item: backView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        
        self.addConstraints([backLeft, backTop, backRight, backBottom])
//        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(borderSize)-[backView]-\(borderSize)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["backView" : backView]))
//        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[backView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["backView" : backView]))
        
        blurLeft = NSLayoutConstraint(item: blurView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        blurTop = NSLayoutConstraint(item: blurView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        blurRight = NSLayoutConstraint(item: blurView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        blurBottom = NSLayoutConstraint(item: blurView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        
        self.addConstraints([blurLeft, blurTop, blurRight, blurBottom])
//        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(borderSize)-[blurView]-\(borderSize)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["blurView" : blurView]))
//        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[blurView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["blurView" : blurView]))
        
        blurView.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[vibrancyView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["vibrancyView" : vibrancyView]))
        blurView.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[vibrancyView]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["vibrancyView" : vibrancyView]))
    }
    
    var contentView: UIView {
        return vibrancyView.contentView
    }
}
