//
//  RTAnimationBaseController.h
//  ModalView.Test
//
//  Created by valery on 11/5/13.
//  Copyright (c) 2013 valera. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef __IPHONE_7_0

@protocol RTAnimationBaseController <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval presentationDuration;

@property (nonatomic, assign) NSTimeInterval dismissalDuration;

@property (nonatomic, assign) BOOL isPresenting;

@property (nonatomic, assign)UIViewController* parentViewController;

@end
#endif