//
//  Buttons.swift
//  ProductBuilder
//
//  Created by valery on 12/21/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class MOButtonBlueTransparent : MOButton{
    
    override func setupLayers() {
        UIRoundedCornersView.drawRoundedCorners(self, radius: 5.0)
        super.setupLayers()
        self.layer.borderWidth = 1
        self.normalBorderColor = UIColor(r:0, g: 139, b: 232)
        self.highlightedBorderColor = self.normalBorderColor.darken(0.2)
        self.selectedBorderColor = self.highlightedBorderColor
        self.disabledBorderColor = UIColor(r:125, g: 129, b: 135)
        self.disabledBackgroundColor = UIColor.clear
        self.setTitleColor(UIColor(r:70, g: 152, b: 211), for: .normal)
        self.setTitleColor(self.selectedBorderColor, for: .selected)
        self.setTitleColor(self.highlightedBorderColor, for: .highlighted)
        self.setTitleColor(UIColor(r:125, g: 129, b: 135), for: .disabled)
        self.handleState()
    }
}

class MOButtonBlue : MOButton{
    
    override func setupLayers() {
        UIRoundedCornersView.drawRoundedCorners(self, radius: 5.0)
        super.setupLayers()
        self.normalBackgroundColor = UIColor(r:0, g: 139, b: 232)
        self.highlightedBackgroundColor = self.normalBackgroundColor.lighten(0.2)
        self.selectedBackgroundColor = self.highlightedBorderColor
        self.disabledBackgroundColor = UIColor(170)
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(.white, for: .selected)
        self.setTitleColor(.white, for: .highlighted)
        self.setTitleColor(.white, for: .disabled)
        self.handleState()
    }
}

class MOButtonRed : MOButton{
    
    override func setupLayers() {
        UIRoundedCornersView.drawRoundedCorners(self, radius: 5.0)
        super.setupLayers()
        self.layer.borderWidth = 1
        self.normalBackgroundColor = UIColor(r:228, g: 30, b: 44)
        self.setTitleColor(.white, for: .normal)
        self.handleState()
    }
}

class MOButtonAppleGreen : MOButton{
    
    override func setupLayers() {
        UIRoundedCornersView.drawRoundedCorners(self, radius: 5.0)
        super.setupLayers()
        self.layer.borderWidth = 1
        self.normalBackgroundColor = UIColor(r:126, g: 211, b: 33)
        self.disabledBackgroundColor = UIColor(170)
        self.setTitleColor(.white, for: .normal)
        self.handleState()
    }
}

class MOButtonBlueWithImage : MOButton{
    
    override func setupLayers() {
        UIRoundedCornersView.drawRoundedCorners(self, radius: 5.0)
        super.setupLayers()
        self.forceLayerWithImage = true
        self.normalBackgroundColor = UIColor(r:0, g: 139, b: 232)
        self.highlightedBackgroundColor = self.normalBackgroundColor.darken(0.02)
        self.selectedBackgroundColor = self.highlightedBackgroundColor
        self.disabledBackgroundColor = UIColor(r:185, g: 194, b: 203)
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(UIColor(r:49, g: 172, b: 234), for: .selected)
        self.setTitleColor(UIColor(r:49, g: 172, b: 234), for: .highlighted)
        self.setTitleColor(UIColor(r:233, g: 233, b: 233), for: .disabled)
        let normalImage = self.image(for: .normal)
        if (normalImage != nil){
            self.setImage(normalImage?.mask(with: UIColor(r:200, g: 200, b: 200)), for: .highlighted)
        }
        self.tintColor = UIColor(r:49, g: 172, b: 234)
        self.handleState()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        centerVertically(withPadding: 8)
    }
    
    func centerVertically(withPadding padding: CGFloat) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + padding
        self.imageEdgeInsets = UIEdgeInsetsMake(-(totalHeight - imageSize.height), 0.0, 0.0, -titleSize.width)
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(totalHeight - titleSize.height), 0.0)
    }

}

extension UIImage{
    func mask(with color: UIColor) -> UIImage {
        let maskImage = self.cgImage!
        let width: CGFloat = self.size.width
        let height: CGFloat = self.size.height
        let bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapContext = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)
        bitmapContext?.clip(to: bounds, mask: maskImage)
        bitmapContext?.setFillColor(color.cgColor)
        bitmapContext?.fill(bounds)
        let cImage = bitmapContext?.makeImage()
        let coloredImage = UIImage(cgImage: cImage!)
        return coloredImage
    }
}

public class UIButtonProxy:UIButton{
    
    var target:UIButton?
    
    public override init(frame: CGRect){
        super.init(frame:frame)
        initDefaults();
    }
    
    public required init?(coder aDecoder: NSCoder){
        super.init(coder:aDecoder)
        initDefaults()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        initDefaults()
    }
    
    private func initDefaults(){
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        addTarget(self, action: #selector(touchDown), for: .touchDown)
        addTarget(self, action: #selector(touchUpOutside), for: .touchUpOutside)
        addTarget(self, action: #selector(touchDragInside), for: .touchDragInside)
        addTarget(self, action: #selector(touchDragOutside), for: .touchDragOutside)
        addTarget(self, action: #selector(touchDragEnter), for: .touchDragEnter)
        addTarget(self, action: #selector(touchDragExit), for: .touchDragExit)
    }
    
    func touchDragEnter(){
        target?.sendActions(for: .touchDragEnter)
    }
    
    func touchDragExit(){
        target?.sendActions(for: .touchDragExit)
    }
    
    func touchDragOutside(){
        target?.isHighlighted = false
        target?.sendActions(for: .touchDragOutside)
    }
    
    func touchDragInside(){
        target?.isHighlighted = touchedDown
        target?.sendActions(for: .touchDragInside)
    }
    
    func touchUpInside(){
        touchedDown = false
        target?.isHighlighted = false
        target?.sendActions(for: .touchUpInside)
    }
    var touchedDown = false
    
    func touchDown(){
        touchedDown = true
        target?.isHighlighted = true
        target?.sendActions(for: .touchDown)
    }
    
    func touchUpOutside(){
        touchedDown = false
        target?.isHighlighted = false
        target?.sendActions(for: .touchUpOutside)
    }
    
//    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        target?.touchesBegan(touches, with: event)
//    }
//    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        target?.touchesMoved(touches, with: event)
//    }
//    
//    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        target?.touchesEnded(touches, with: event)
//    }
//    
//    override public func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        target?.touchesCancelled(touches, with: event)
//    }

//    override public func sendAction(_ action: Selector, to target: Any?, for event: UIEvent?){
//        self.target?.sendAction(action, to :target, for:event)
//    }
//    
//    override public func sendActions(for controlEvents: UIControlEvents){
//        self.target?.sendActions(for:controlEvents)
//    }
}
