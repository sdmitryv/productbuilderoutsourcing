//
//  UITextFieldAlphanumericHandler.m
//  ProductBuilder
//
//  Created by valera on 10/31/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UITextFieldAlphanumericHandler.h"

@implementation UITextFieldAlphanumericHandler

-(id)init{

	if ((self=[super init])){

        NSCharacterSet* anSet = [NSCharacterSet alphanumericCharacterSet];
        nonAplhaNumericSet = [(NSMutableCharacterSet*)[anSet invertedSet] retain];
    }
    return self;
}

@end
