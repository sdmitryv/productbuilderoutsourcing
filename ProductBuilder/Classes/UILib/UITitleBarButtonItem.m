#import "UITitleBarButtonItem.h"

@implementation UITitleBarButtonItem

// Only caring about UITitleBarButtonItem set up in Interface Builder. Update this class if you need to instantiate it from code

- (void) awakeFromNib {

    [super awakeFromNib];

    UIFont *font = IS_IPHONE_6_OR_MORE ? [UIFont systemFontOfSize:18] : [UIFont boldSystemFontOfSize:16];
    [self setTitleTextAttributes:@{NSFontAttributeName: font} forState:UIControlStateNormal];
    [self disable];
}

- (void)disable {
    UIView *theView = [self valueForKey:@"view"];
    
    if ([theView respondsToSelector:@selector(setUserInteractionEnabled:)]) {
        theView.userInteractionEnabled = NO;
    }
}

- (void)setTitle:(NSString *)title {
    [super setTitle:title];
    UIFont *font = IS_IPHONE_6_OR_MORE ? [UIFont systemFontOfSize:18] : [UIFont boldSystemFontOfSize:16];
    [self setTitleTextAttributes:@{NSFontAttributeName: font} forState:UIControlStateNormal];
    [self disable];
}

@end
