//
//  UITextFieldNumeric.h
//  ProductBuilder
//
//  Created by Valera on 11/16/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UITextFieldHandler.h"
@class UITextFieldNumeric;

@protocol UITextFieldNumericDelegate
@optional
-(void)textFieldNumericValueChanged:(UITextFieldNumeric*)textFieldNumeric;
-(BOOL)textFieldNumeric:(UITextFieldNumeric*)textFieldNumeric shouldChangeValue:(NSDecimal*)value;
@end

@interface UITextFieldNumeric : UITextField<UITextFieldHandlerDelegate>{
	UITextFieldHandler* _textHandler;
    BOOL initializing;
    BOOL flushingEditValue;
    BOOL insideTextFieldNumericValueChanged;
}
@property (nonatomic,assign) NSTextFormatterStyle style;
@property (nonatomic,retain)NSString* percentSymbol;
@property (nonatomic, retain)NSNumber* maxValue;
@property (nonatomic, retain)NSNumber* minValue;
@property (nonatomic, retain)NSNumber* customFracationDigits;
@property (nonatomic, assign)NSUInteger formatWidth;
@property (nonatomic,assign) double doubleValue;
@property (nonatomic,assign) NSInteger intValue;
@property (nonatomic,assign) NSDecimal decimalValue;
@property (nonatomic, retain) NSDecimalNumber* decimalNumberValue;
@property (nonatomic,retain) NSNumber* numberValue;
@property (nonatomic,assign)BOOL enableNil;
@property (nonatomic,assign) id owner;
@property (nonatomic, readonly)BOOL isDecimalValueActual;
@property (nonatomic, readonly) UITextFieldHandler* textHandler;
@property (nonatomic, assign) BOOL clearZero;
@property (nonatomic,readonly) id<UITextFieldDelegate> internalDelegate;
@property (nonatomic, assign) NSUInteger maxTextLength;
@property (nonatomic, assign) BOOL useVirtualNumericKeyboard;
@property (nonatomic, assign) BOOL useSmartAmount;

//- (void)textFieldDidEndEditingNumber:(NSDecimalNumber*)decimalNumber;
-(void)flushEditValue;

@end
