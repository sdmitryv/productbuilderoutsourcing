//
//  BaseSearchTableViewCell.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+Utils.h"

@interface BaseSearchTableViewCell : UITableViewCell
- (void)initDefaults;
-(void)displayObject:(NSObject *)object currentSearchText:(NSString *)currentSearchText highlightColor:(UIColor *)highlightColor;
- (void)assignText:(NSString *)str forLabel:(UILabel *)label searchText:(NSString *)searchText color:(UIColor *)color;
@end
