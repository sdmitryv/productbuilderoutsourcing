//
//  RTKeyboardSwitchButton.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOGlassButton.h"
#import "UITextField+Input.h"

@interface RTKeyboardSwitchButton : MOGlassButtonBlack {
    NSString* _valueKey;
    UISearchBar* _sBar;
}

-(void)boundSearchBar:(UISearchBar*)sbar selected:(BOOL)selected;
-(void)handleKeyboardButton;
@end
