#import "RTSelectorButton.h"
#import "Global.h"
#import "RTPopoverController.h"
#import "UIView+FirstResponder.h"
#import "DataUtils.h"
#import "NSAttributedString+Attributes.h"

@implementation RTUnderlinedSelectorButton

-(id)init{
    if (self = [super init]) {
        self.enabledColor = global_blue_color;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        self.enabledColor = global_blue_color;
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.enabledColor = global_blue_color;
    }
    return self;
}


-(void) handleState{
    [super handleState];
    if (self.highlighted){
        self.titleLabel.transform = CGAffineTransformMakeScale(1.0,0.9);
    }
    else{
        self.titleLabel.transform = CGAffineTransformIdentity;
    }
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state {
    
    NSString* label = @"";
    if (title .length > 0) {
        label = title;
    }
    
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:label];
    
    [str setTextAlignment:self.titleLabel.textAlignment lineBreakMode:NSLineBreakByTruncatingTail];
    
    if (self.enabled || !self.noLineForReadOnly) {
        [str setTextIsUnderlined:YES];
    }
    
    [str setFont:self.titleLabel.font range:NSMakeRange(0, str.length)];
    
//    CGSize possibleSize = str.size;
//    possibleSize.width = ceilf(possibleSize.width + 5);
//    
//    if (self.maxWidth > 0){
//        possibleSize.width = MIN(possibleSize.width, self.maxWidth);
//
//        if (self.titleLabel.textAlignment == NSTextAlignmentRight) {
//            [self setFrame:CGRectMake(self.frame.origin.x + self.frame.size.width - possibleSize.width, self.frame.origin.y , possibleSize.width, self.frame.size.height)];
//        }
//        else {
//            [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y , possibleSize.width, self.frame.size.height)];
//        }
//    }
//    CGRect stringRect = [str boundingRectWithSize:CGSizeMake(self.bounds.size.width, CGFLOAT_MAX) options:0 context:nil];
//    if (stringRect.size.height > self.bounds.size.height || stringRect.size.width > self.bounds.size.width){
//        [str setTextAlignment:self.titleLabel.textAlignment lineBreakMode:NSLineBreakByTruncatingTail];
//    }
    
    
    [super setAttributedTitle:str forState:UIControlStateNormal];
    [super setAttributedTitle:str forState:UIControlStateDisabled];

    if (!self.enabled)
        [str setTextColor:[UIColor blackColor]];
    else
        [str setTextColor: _enabledColor];

    [str release];
}


-(void)setEnabled:(BOOL)value{
    if (value==self.enabled) return;
    [super setEnabled:value];
    _isReadOnly = !value;
    [self setTitle:self.titleLabel.attributedText.string forState:UIControlStateNormal];
}

-(void)setupLayers {
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateDisabled];
    
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor colorWithRed:14/255.0 green:62/255.0 blue:158/255.0 alpha:1] forState:UIControlStateSelected];
}

- (void)setIsReadOnly:(BOOL)isReadOnly {
    self.enabled = !isReadOnly;
}

-(void)dealloc {
    
    [_enabledColor release];
    [super dealloc];
}

@end




@implementation RTSelectorButton50
-(void)setupLayers {
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateDisabled];
    
    
    [self setTitleColor:self.defaultTitleColor forState:UIControlStateNormal];
    [self setTitleColor:MO_RGBCOLOR(170, 170, 170) forState:UIControlStateDisabled];
    [self setTitleColor:MO_RGBCOLOR(20, 145, 242) forState:UIControlStateHighlighted];
    [self setTitleColor:MO_RGBCOLOR(20, 145, 242) forState:UIControlStateSelected];
}

-(UIColor *)defaultTitleColor {
    return MO_RGBCOLOR(0, 139, 232);
}

-(SimpleTableListViewController*)itemsListViewController{
    SimpleTableListViewController* list = super.itemsListViewController;
    [list view]; // force load view
    list.cancelButton.hidden = YES;
    return list;
}

-(void)simpleTableListViewController:(SimpleTableListViewController *)controller viewWillAppear:(BOOL)animated {
    if (_fitPopoverSize) {
//        NSArray* displayedDataSorce = _dataSource;
//        if (self.dataSourceDisplayPredicate){
//            displayedDataSorce = [_dataSource filteredArrayUsingPredicate:self.dataSourceDisplayPredicate];
//        }
        NSUInteger itemsCount = ((NSArray *)controller.items).count;
        NSUInteger visibleItemscount = MIN(itemsCount, 10);
        CGFloat titleHeight = controller.plainTableView.frame.origin.y;
        CGFloat rowHeight = controller.plainTableView.rowHeight;
        CGSize popoverSize = CGSizeMake(300.0, titleHeight + rowHeight * visibleItemscount);
        self.popoverContentSize = popoverSize;
        controller.plainTableView.tableFooterView = [[[UIView alloc] init] autorelease];
        if (visibleItemscount == itemsCount) {
            controller.plainTableView.bounces = false;
            controller.plainTableView.scrollEnabled = false;
        }
    }
    
}

@end
