//
//  RTBarcodeButton.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 4/15/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "RTBarcodeButton.h"
#import "SystemVer.h"

@implementation RTBarcodeButton

- (void)applyDefaultProperties {
    [super applyDefaultProperties];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        forceLayerWithImage = TRUE;
        [self setImage:[UIImage imageNamed:@"barcode.png"] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:@"barcode.png"] forState:UIControlStateHighlighted];
        [self setImage:[UIImage imageNamed:@"barcode.png"] forState:UIControlStateSelected];
        [self setImage:[UIImage imageNamed:@"barcode.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
    }
    else {
        if (IS_IPHONE_6_OR_MORE) {
            forceLayerWithImage = TRUE;
            [self setImage:[UIImage imageNamed:@"barcode_iphone.png"] forState:UIControlStateNormal];
            [self setImage:[UIImage imageNamed:@"barcode_iphone.png"] forState:UIControlStateHighlighted];
            [self setImage:[UIImage imageNamed:@"barcode_iphone.png"] forState:UIControlStateSelected];
            [self setImage:[UIImage imageNamed:@"barcode_iphone.png"] forState:UIControlStateSelected | UIControlStateHighlighted];
        }
    }
}

-(void)setupLayers{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE){
        [super setupLayers];
    }
}

@end
