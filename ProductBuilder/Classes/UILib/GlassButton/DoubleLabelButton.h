//
//  DoubleLabelButton.h
//  DoubleLabelButton
//
//  Created by Sergey Lugovoy on 10/14/14.
//  Copyright (c) 2014 Sergey Lugovoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOButton.h"

@interface DoubleLabelButton : UIView

@property (nonatomic)               Class       buttonClass;
@property (nonatomic, readonly)     MOButton    *button;
@property (nonatomic, strong)       UIFont      *firstLineFont;
@property (nonatomic, strong)       UIFont      *secondLineFont;
@property (nonatomic, strong)       UIColor     *firstLineColor;
@property (nonatomic, strong)       UIColor     *secondLineColor;
@property (nonatomic, strong)       UIColor     *firstLineShadowColor;
@property (nonatomic, strong)       UIColor     *secondLineShadowColor;
@property (nonatomic, strong)       NSString    *firstLineText;
@property (nonatomic, strong)       NSString    *secondLineText;
@property (nonatomic, strong)       UIImage     *firstLineImage;

@end
