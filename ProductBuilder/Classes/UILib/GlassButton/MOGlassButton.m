//
//  MOGlassButton.m
//  Licensed under the terms of the BSD License, as specified below.
//
//  Created by Hwee-Boon Yar on Jan/31/2010.
//
/*
 Copyright 2010 Yar Hwee Boon. All rights reserved.
 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 * Neither the name of MotionObj nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#define text_color_for_disabled MO_RGBCOLOR(125, 129, 135)

#import "MOGlassButton.h"

#import "Global.h"
#import "UIRoundedCornersView.h"

@implementation MOGlassButton

-(void)initializeGradientLayer{
    if (_style!=MOGlassButtonStyleDefault) return;
    if (_gradientLayerInitialized) return;
    _gradientLayerInitialized = YES;
    gradientLayer1 = [[CAGradientLayer alloc]init];
    [self initializeGradientLayerProperties];
    [self.layer insertSublayer:gradientLayer1 atIndex:0];
}

-(void)initializeGradientLayerProperties{
    gradientLayer1.colors = @[(id)[UIColor colorWithWhite:0.4f alpha:0.15f].CGColor,
                              (id)[UIColor colorWithWhite:0.5f alpha:0.25f].CGColor,
                              (id)[UIColor colorWithWhite:0.55f alpha:0.25f].CGColor,
                              (id)[UIColor colorWithWhite:0.55f alpha:0.25f].CGColor];
    gradientLayer1.locations = @[@0.0f,
                                 @0.15f,
                                 @0.75f,
                                 @1.0f];
}

- (void)applyDefaultProperties {
    _style = MOGlassButtonStyleFlat;
    [super applyDefaultProperties];
    cornerRadius = 5.0f;
}

-(void)setStyle:(MOGlassButtonStyle)style{
    if (_style==style) return;
    _style = style;
    if (_style==MOGlassButtonStyleFlat){
        [gradientLayer1 removeFromSuperlayer];
        [gradientLayer1 release];
        gradientLayer1 = nil;
        _gradientLayerInitialized = FALSE;
    }
    [self setupLayers];
}

-(void)setCornerRadius:(CGFloat)value{
    if (cornerRadius!=value){
        cornerRadius = value;
        [self setNeedsLayout];
    }
}

-(CGFloat)cornerRadius{
    return cornerRadius;
}

- (void)setupLayers {
    [self setNeedsLayout];
    if (![self imageForState:UIControlStateNormal] && !forceLayerWithImage) {
        self.layer.borderWidth = 1.0f;
    }
    self.titleLabel.shadowOffset = CGSizeMake(0, 0);
    [self setBackgroundColor:[self.class backgroundColorForStyle:_style state:UIControlStateNormal] forState:UIControlStateNormal];
    [self setBackgroundColor:[self.class backgroundColorForStyle:_style state:UIControlStateHighlighted] forState:UIControlStateHighlighted];
    [self setBackgroundColor:[self.class backgroundColorForStyle:_style state:UIControlStateSelected] forState:UIControlStateSelected];
    [self setBackgroundColor:[self.class backgroundColorForStyle:_style state:UIControlStateDisabled] forState:UIControlStateDisabled];
    
    [super setupLayers];
}


- (void)dealloc {
    [gradientLayer1 release];
    [super dealloc];
}


- (void)layoutSubviews {
    [super layoutSubviews];
    if (_style==MOGlassButtonStyleDefault){
        [self initializeGradientLayer];
        gradientLayer1.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height/2);
    }
    [UIRoundedCornersView drawRoundedCorners:self radius:[self imageForState:UIControlStateNormal] && !forceLayerWithImage ? 0.0f : cornerRadius];
}

-(void)setImage:(UIImage *)image forState:(UIControlState)state {
    [super setImage:image forState:state];
    [self setupLayers];
    if (image && !forceLayerWithImage) {
        gradientLayer1.hidden = TRUE;
        self.backgroundColor = [UIColor clearColor];
        self.layer.backgroundColor = self.layer.borderColor = [[UIColor clearColor] CGColor];
        self.layer.borderWidth = 0;
    }
    else {
        gradientLayer1.hidden = FALSE;
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
}


+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    return nil;
}

@end

@implementation MOGlassButtonBlack

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(42, 42, 42);
        case UIControlStateDisabled:
            return MO_RGBCOLOR(227, 226, 225);
        default:
            return nil;
    }
}

- (void)setupLayers{
	[super setupLayers];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end


@implementation MOGlassButtonGraySquare

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(21, 21, 21);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    self.cornerRadius = 0;
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self setTitleColor:MO_RGBCOLOR(205, 212, 220) forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonYellow

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(193, 193, 0);
        case UIControlStateDisabled:
            return MO_RGBCOLOR(227, 226, 225);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonGreen

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        switch(state){
            case UIControlStateNormal:
                    return MO_RGBCOLOR(46, 178, 45);
            case UIControlStateHighlighted:
            case UIControlStateSelected:
                    return MO_RGBCOLOR(105, 189, 104);
            case UIControlStateDisabled:
                return MO_RGBCOLOR(227, 226, 225);
            default:
                return nil;
        }
    }
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(18, 74, 45);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];

}

@end

@implementation MOGlassButtonBlue


+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(0, 0, 255);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:MO_RGBCOLOR(205, 212, 220) forState:UIControlStateDisabled];
}

@end


@implementation MOGlassButtonMarine

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        switch(state){
            case UIControlStateNormal:
                   return MO_RGBCOLOR(0, 139, 232);
            case UIControlStateHighlighted:
            case UIControlStateSelected:
                   return MO_RGBCOLOR(145, 185, 227);
            case UIControlStateDisabled:
                return MO_RGBCOLOR(227, 226, 225);
            default:
                return nil;
        }
    }
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(62, 82, 112);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end


@implementation MOGlassButtonGrayRC

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(204, 204, 204);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    self.cornerRadius = 0.0f;
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:MO_RGBCOLOR(205, 212, 220) forState:UIControlStateDisabled];
}

@end


@implementation MOGlassButtonBlackRC

- (void)setupLayers{
    [super setupLayers];
    self.cornerRadius = 0.0f;
}

@end

@implementation MOGlassButtonGray

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        switch(state){
            case UIControlStateNormal:
                return MO_RGBCOLOR(96, 96, 96);
            default:
                return nil;
        }
    }
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(51, 51, 51);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonLightGray

-(void)initializeGradientLayerProperties{
    gradientLayer1.colors = @[(id)[UIColor colorWithWhite:0.85f alpha:0.15f].CGColor,
                              (id)[UIColor colorWithWhite:0.87f alpha:0.25f].CGColor,
                              (id)[UIColor colorWithWhite:0.88f alpha:0.25f].CGColor,
                              (id)[UIColor colorWithWhite:0.9f alpha:0.25f].CGColor];
    gradientLayer1.locations = @[@0.0f,
                                 @0.15f,
                                 @0.75f,
                                 @1.0f];
}


+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateDisabled:
            return MO_RGBCOLOR(227, 226, 225);
        case UIControlStateNormal:
            return MO_RGBCOLOR(205, 203, 203);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:MO_RGBCOLOR(64, 64, 64) forState:UIControlStateSelected];
    [self setTitleColor:MO_RGBCOLOR(64, 64, 64) forState:UIControlStateHighlighted];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonDarkGray

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(51, 51, 51);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonGreenRounded

- (void)setupLayers{
    [super setupLayers];
    cornerRadius = 15.0f;
}
@end

@implementation MOGlassButtonRed

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        switch(state){
            case UIControlStateNormal:
                return MO_RGBCOLOR(201, 55, 39);
            case UIControlStateSelected:
            case UIControlStateHighlighted:
                return MO_RGBCOLOR(227, 226, 225);
            case UIControlStateDisabled:
                return MO_RGBCOLOR(227, 226, 225);
            default:
                return nil;
        }
    }
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(112, 31, 31);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateHighlighted];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
  
}

@end


@implementation MOGlassButtonBrightRed

-(void)initializeGradientLayerProperties{
    gradientLayer1.colors = @[(id)[UIColor colorWithWhite:0.75f alpha:0.1f].CGColor,
                              (id)[UIColor colorWithWhite:0.77f alpha:0.17f].CGColor,
                              (id)[UIColor colorWithWhite:0.85f alpha:0.17f].CGColor,
                              (id)[UIColor colorWithWhite:0.9f alpha:0.17f].CGColor];
    gradientLayer1.locations = @[@0.0f,
                                 @0.15f,
                                 @0.75f,
                                 @1.0f];
}

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        return [MOGlassButtonRed backgroundColorForStyle:style state:state];
    }
    
    switch(state){
        case UIControlStateDisabled:
            return MO_RGBCOLOR(227, 226, 225);
        case UIControlStateNormal:
            return MO_RGBCOLOR(248, 32, 27);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonDarkGrayWithGradient

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        switch(state){
            case UIControlStateSelected:
                return [MOGlassButtonGreen backgroundColorForStyle:style state:UIControlStateNormal];
            case UIControlStateHighlighted:
                return [MOGlassButtonGreen backgroundColorForStyle:style state:UIControlStateHighlighted];
            case UIControlStateNormal:
                return MO_RGBCOLOR(42, 42, 42);
            default:
                return nil;
        }
    }
    
    switch(state){
        case UIControlStateSelected:
        case UIControlStateHighlighted:
            return MO_RGBCOLOR(14, 78, 35);
        case UIControlStateNormal:
            return MO_RGBCOLOR(21, 21, 21);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}

@end

@implementation MOGlassButtonMarineWithGradient


+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
    if (style == MOGlassButtonStyleFlat){
        return [MOGlassButtonMarine backgroundColorForStyle:style state:state];
    }
    
    switch(state){
        case UIControlStateNormal:
            return MO_RGBCOLOR(62, 82, 112);
        default:
            return nil;
    }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
}
@end
@implementation MOGlassButtonTransparent

+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state{
   
        switch(state){
            case UIControlStateNormal:
                return [UIColor clearColor];
            case UIControlStateSelected:
            case UIControlStateHighlighted:
                return [UIColor clearColor];
            case UIControlStateDisabled:
                return [UIColor clearColor];
            default:
                return nil;
        }
}

- (void)setupLayers{
    [super setupLayers];
    [self setTitleColor:MO_RGBCOLOR(70, 152, 211) forState:UIControlStateNormal];
    [self setTitleColor:text_color_for_disabled forState:UIControlStateDisabled];
    
}

@end

@implementation RTSearchOptionsButton

- (void)applyDefaultProperties {
    [super applyDefaultProperties];
    
    forceLayerWithImage = TRUE;
    
    if (IS_IPHONE_6_OR_MORE)
        [self setImage: [UIImage imageNamed:@"list_iphone.png"] forState:UIControlStateNormal];
    else {
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self setContentEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
        [self setImage: [UIImage imageNamed:@"list.png"] forState:UIControlStateNormal];
    }
}

-(void)setupLayers{
    [super setupLayers];
    
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    if (!IS_IPHONE_6_OR_MORE) {
        [self setImageEdgeInsets:UIEdgeInsetsMake(0, self.bounds.size.width - self.imageView.bounds.size.width - 16, 0, 0)];
        [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.imageView.image.size.width, 0, self.imageView.bounds.size.width + 16)];
    }
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    if (!IS_IPHONE_6_OR_MORE)
        [super setTitle:title forState:state];
}

@end
