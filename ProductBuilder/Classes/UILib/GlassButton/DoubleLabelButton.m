//
//  DoubleLabelButton.m
//  DoubleLabelButton
//
//  Created by Sergey Lugovoy on 10/14/14.
//  Copyright (c) 2014 Sergey Lugovoy. All rights reserved.
//

#import "DoubleLabelButton.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

@interface DoubleLabelButton () {
    Class       _baseButtonClass;
    MOButton    *_baseButton;
    UILabel     *_firstLineLabel;
    UILabel     *_secondLineLabel;
    NSString    *_firstLineText;
    UIFont      *_firstLineFont;
    UIColor     *_firstLineColor;
    UIColor     *_firstLineShadowColor;
    NSString    *_secondLineText;
    UIFont      *_secondLineFont;
    UIColor     *_secondLineColor;
    UIColor     *_secondLineShadowColor;
    UIImageView *_firstLineImageView;
    UIImage     *_firstLineImage;
}

@end

@implementation DoubleLabelButton

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])){
        
        self.backgroundColor = [UIColor clearColor];
        
        [self setButtonClass:[MOButton class]];
        
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    _baseButton.frame = self.bounds;
    [self updateViews];
}

- (Class) buttonClass {
    return _baseButtonClass;
}

- (void) setButtonClass:(Class)buttonClass {
    if (![buttonClass isSubclassOfClass:[MOButton class]]) {
        return;
    }
    
    if (_baseButton) {
        [_baseButton removeFromSuperview];
    }
    
    _baseButtonClass = buttonClass;
    _baseButton = [_baseButtonClass new];
    _baseButton.frame = self.bounds;
    
    [self addSubview:_baseButton];
    [self sendSubviewToBack:_baseButton];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    [self addViews];
    [self updateViews];
}

- (MOButton *) button {return _baseButton;}

- (UIFont *) firstLineFont {return _firstLineFont;}

- (void) setFirstLineFont:(UIFont *)firstLineFont {
    _firstLineFont = firstLineFont;
    [self updateViews];
}

- (UIColor *) firstLineColor {return _firstLineColor;}

- (void) setFirstLineColor:(UIColor *)firstLineColor {
    _firstLineColor = firstLineColor;
    _firstLineLabel.textColor = _firstLineColor;
}

- (UIColor *) firstLineShadowColor {return _firstLineShadowColor;}

- (void) setFirstLineShadowColor:(UIColor *)firstLineShadowColor {
    _firstLineShadowColor = firstLineShadowColor;
    _firstLineLabel.shadowColor = _firstLineShadowColor;
}

- (NSString *) firstLineText {return _firstLineText;}

- (void) setFirstLineText:(NSString *)firstLineText{
    _firstLineText = firstLineText;
    _firstLineLabel.text = _firstLineText;
    
    _firstLineLabel.hidden = _firstLineText == nil || _firstLineText.length == 0;
    if (_firstLineText && _firstLineText.length > 0)
        self.firstLineImage = nil;
}

- (UIImage *) firstLineImage { return _firstLineImage;}

- (void) setFirstLineImage:(UIImage *)firstLineImage {
    _firstLineImage = firstLineImage;
    _firstLineImageView.image = _firstLineImage;
    
    _firstLineImageView.hidden = _firstLineImage == nil;
    if (_firstLineImage)
        self.firstLineText = nil;
}

- (UIFont *) secondLineFont {return _secondLineFont;}

- (void) setSecondLineFont:(UIFont *)secondLineFont {
    _secondLineFont = secondLineFont;
    _secondLineLabel.font = _secondLineFont;
    [self updateViews];
}

- (UIColor *) secondLineColor {return _secondLineLabel.textColor;}

- (void) setSecondLineColor:(UIColor *)secondLineColor {
    _secondLineColor = secondLineColor;
    _secondLineLabel.textColor = _secondLineColor;
}

- (UIColor *) secondLineShadowColor {return _secondLineLabel.shadowColor;}

- (void) setSecondLineShadowColor:(UIColor *)secondLineShadowColor {
    _secondLineShadowColor = secondLineShadowColor;
    _secondLineLabel.shadowColor = _secondLineShadowColor;
}

- (NSString *) secondLineText {return _secondLineText;}

- (void) setSecondLineText:(NSString *)secondLineText {
    _secondLineText = secondLineText;
    _secondLineLabel.text = _secondLineText;
}

- (void) addViews {
    if (!_baseButton) {
        return;
    }
    
    if (!_firstLineLabel) {
        _firstLineLabel = [UILabel new];
        if (!_firstLineFont) _firstLineFont = _baseButton.titleLabel.font;
        _firstLineLabel.font = _firstLineFont;
        if (!_firstLineColor) _firstLineColor = _baseButton.currentTitleColor;
        _firstLineLabel.textColor = _firstLineColor;
        if (!_firstLineShadowColor) _firstLineShadowColor = _baseButton.currentTitleShadowColor;
        _firstLineLabel.shadowColor = _firstLineShadowColor;
        _firstLineLabel.textAlignment = NSTextAlignmentCenter;
        _firstLineLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _firstLineLabel.numberOfLines = 1;
        _firstLineLabel.text = _firstLineText;
        [self addSubview:_firstLineLabel];
    }
    
    if (!_firstLineImageView) {
        _firstLineImageView = [UIImageView new];
        _firstLineImageView.contentMode = UIViewContentModeScaleAspectFit;
        if (_firstLineImage) {
            _firstLineImageView.image = _firstLineImage;
        }
        [self addSubview:_firstLineImageView];
    }
    
    if (!_secondLineLabel) {
        _secondLineLabel = [UILabel new];
        if (!_secondLineFont) _secondLineFont = _baseButton.titleLabel.font;
        _secondLineLabel.font = _secondLineFont;
        if (!_secondLineColor) _secondLineColor = _baseButton.currentTitleColor;
        _secondLineLabel.textColor = _secondLineColor;
        if (!_secondLineShadowColor) _secondLineShadowColor = _baseButton.currentTitleShadowColor;
        _secondLineLabel.shadowColor = _secondLineShadowColor;
        _secondLineLabel.textAlignment = NSTextAlignmentCenter;
        _secondLineLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _secondLineLabel.numberOfLines = 1;
        _secondLineLabel.text = _secondLineText;
        [self addSubview:_secondLineLabel];
    }
}

- (void) updateViews {
    
    CGFloat distance = 0;
    CGFloat padding = (self.bounds.size.height - _firstLineLabel.font.pointSize - distance - _secondLineLabel.font.pointSize) / 2;
    
    _firstLineLabel.frame = CGRectMake(0, padding, self.bounds.size.width, _firstLineLabel.font.pointSize);
    
    _firstLineImageView.frame = CGRectMake(0, padding, self.bounds.size.width, _firstLineLabel.font.pointSize);
    
    _secondLineLabel.frame = CGRectMake(0, _firstLineLabel.frame.origin.y + _firstLineLabel.frame.size.height + distance, self.bounds.size.width, _secondLineLabel.font.pointSize);
    
}

@end
