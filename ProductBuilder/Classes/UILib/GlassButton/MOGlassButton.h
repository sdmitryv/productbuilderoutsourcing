//
//  MOGlassButton.h
//  Licensed under the terms of the BSD License, as specified below.
//
//  Created by Hwee-Boon Yar on Jan/31/2010.
//
/*
 Copyright 2010 Yar Hwee Boon. All rights reserved.
 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 * Neither the name of MotionObj nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#import <Foundation/Foundation.h>

#import "MOButton.h"

typedef NS_ENUM(NSUInteger,MOGlassButtonStyle) {
    MOGlassButtonStyleDefault = 0,
    MOGlassButtonStyleFlat = 1
};

@interface MOGlassButton : MOButton {
	CAGradientLayer* gradientLayer1;
    BOOL _gradientLayerInitialized;
	//CALayer* outlineLayer;
	//CAGradientLayer* shineLayer;
    CGFloat cornerRadius;
    MOGlassButtonStyle _style;
}
+(UIColor*)backgroundColorForStyle:(MOGlassButtonStyle)style state:(UIControlState)state;
@property (nonatomic,assign)CGFloat cornerRadius;
@property (nonatomic,assign)MOGlassButtonStyle style UI_APPEARANCE_SELECTOR;
@end

@interface MOGlassButtonYellow : MOGlassButton
@end

@interface MOGlassButtonGreen : MOGlassButton
@end

@interface MOGlassButtonBlack : MOGlassButton
@end

@interface MOGlassButtonMarine : MOGlassButton
@end

@interface MOGlassButtonBlue : MOGlassButton
@end


@interface MOGlassButtonGrayRC : MOGlassButton
@end

@interface MOGlassButtonBlackRC : MOGlassButtonBlack
@end

@interface MOGlassButtonLightGray : MOGlassButton
@end

@interface MOGlassButtonGray : MOGlassButton
@end

@interface MOGlassButtonDarkGray : MOGlassButton
@end

@interface MOGlassButtonGraySquare : MOGlassButton
@end

@interface MOGlassButtonGreenRounded : MOGlassButtonGreen
@end


@interface MOGlassButtonRed : MOGlassButton
@end

@interface MOGlassButtonBrightRed : MOGlassButton
@end

@interface MOGlassButtonDarkGrayWithGradient : MOGlassButton
@end

@interface MOGlassButtonMarineWithGradient : MOGlassButton
@end

@interface RTSearchOptionsButton : MOGlassButtonBlack

@end


@interface MOGlassButtonTransparent : MOGlassButton
@end


