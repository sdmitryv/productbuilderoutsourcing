//
//  SVIndefiniteAnimatedView.swift
//  AnimatedPaths
//
//  Created by valery on 2/6/17.
//
//

import Foundation
import UIKit

open class RTIndefiniteAnimatedView:UIView{
    
    
    var strokeThickness: CGFloat = 5.0
    var strokeColor: UIColor!
    var errorColor: UIColor!
    
    private var _indefiniteAnimatedLayer: CALayer?
    private var _permanentCircleLayer: CAShapeLayer?
    private var _checkmarkLayer: CAShapeLayer?
    private var _crossLayer: CAShapeLayer?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder);
        self.initDefaults()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initDefaults()
    }
    
    private func initDefaults() {
        self.strokeColor = UIColor(red: CGFloat(119.0 / 255.0), green: CGFloat(212.0 / 255.0), blue: CGFloat(48.0 / 255.0), alpha: CGFloat(1))
        self.errorColor = .red
    }
    
    open func reset() {
        self.layoutAnimatedLayer()
    }
    
    private func removeAllLayers(){
        _indefiniteAnimatedLayer?.removeFromSuperlayer()
        _indefiniteAnimatedLayer = nil
        _permanentCircleLayer?.removeFromSuperlayer()
        _permanentCircleLayer = nil
        _checkmarkLayer?.removeFromSuperlayer()
        _checkmarkLayer = nil
        _crossLayer?.removeFromSuperlayer()
        _crossLayer = nil
    }
    
    private func setupPermanentCircleLayer(color:UIColor){
        //        if _permanentCircleLayer != nil {
        //            _permanentCircleLayer?.removeFromSuperlayer()
        //            _permanentCircleLayer = nil
        //        }
        _permanentCircleLayer = createCircleLayer(forStartAngle: -(CGFloat)(Double.pi/2), endAngle: .pi * 3.0/2.0, color:color)
        if _permanentCircleLayer != nil{
            self.layer.addSublayer(_permanentCircleLayer!)
        }
    }
    
    open func stop(withSuccess:Bool) {
        let centerLayer:CALayer?
        var oldCenterLayer:CALayer? = nil
        let oldPermanentCircleLayer = _permanentCircleLayer
        if withSuccess {
            setupCheckmarkLayer()
            centerLayer = _checkmarkLayer
            if _crossLayer?.superlayer != nil{
                oldCenterLayer = _crossLayer
            }
        }
        else{
            setupCrossLayer()
            centerLayer = _crossLayer
            if _checkmarkLayer?.superlayer != nil{
                oldCenterLayer = _checkmarkLayer
            }
        }
        let animationDuration: CGFloat = 0.4
        setupPermanentCircleLayer(color:withSuccess ? strokeColor : errorColor)
        CATransaction.begin()
        CATransaction.setAnimationDuration(CFTimeInterval(animationDuration))
        CATransaction.setCompletionBlock({
            self._indefiniteAnimatedLayer?.removeAllAnimations()
            self._indefiniteAnimatedLayer?.removeFromSuperlayer()
            oldCenterLayer?.removeFromSuperlayer()
            oldPermanentCircleLayer?.removeFromSuperlayer()
        })
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = (0)
        opacityAnimation.toValue = (1)
        opacityAnimation.isRemovedOnCompletion = false
        _permanentCircleLayer?.add(opacityAnimation, forKey: "opacity")
        opacityAnimation.fromValue = (1)
        opacityAnimation.toValue = (0)
        self.indefiniteAnimatedLayer().add(opacityAnimation, forKey: "opacity")
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.fromValue = Int(0.0)
        pathAnimation.toValue = Int(1.0)
        centerLayer?.add(pathAnimation, forKey: "strokeEnd")
        oldCenterLayer?.opacity = 0
        oldPermanentCircleLayer?.opacity = 0
        CATransaction.commit()
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        if (newSuperview != nil) {
            self.layoutAnimatedLayer()
        }
        else {
            _indefiniteAnimatedLayer?.removeFromSuperlayer()
            _indefiniteAnimatedLayer = nil
        }
    }
    
    private func layoutAnimatedLayer() {
        removeAllLayers()
        self.layer.addSublayer(self.indefiniteAnimatedLayer())
    }
    
    private func createCircleLayer(forStartAngle startAngle: CGFloat, endAngle: CGFloat, color:UIColor) -> CAShapeLayer {
        let arcCenter = CGPoint(x: CGFloat(self.bounds.size.width / 2.0), y: CGFloat(self.bounds.size.height / 2.0))
        let radius: CGFloat = min(arcCenter.x, arcCenter.y) - self.strokeThickness
        let smoothedPath = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        let circleLayer = CAShapeLayer()
        circleLayer.contentsScale = UIScreen.main.scale
        circleLayer.frame = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: arcCenter.x * 2.0, height: arcCenter.y * 2.0)
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = color.cgColor
        circleLayer.lineWidth = self.strokeThickness
        circleLayer.lineCap = kCALineCapRound
        circleLayer.lineJoin = kCALineJoinRound
        circleLayer.path = smoothedPath.cgPath
        let widthDiff: CGFloat = self.bounds.width - circleLayer.bounds.width
        let heightDiff: CGFloat = self.bounds.height - circleLayer.bounds.height
        circleLayer.position = CGPoint(x: self.bounds.width - circleLayer.bounds.width / 2.0 - widthDiff / 2.0, y: self.bounds.height - circleLayer.bounds.height / 2.0 - heightDiff / 2.0)
        return circleLayer
    }
    
    private func indefiniteAnimatedLayer() -> CALayer {
        if _indefiniteAnimatedLayer == nil {
            let gradient = RTConicalGradientLayer()
            gradient.frame = self.bounds
            gradient.colors = [UIColor.white, self.strokeColor]
            gradient.startAngle = -.pi/4.0
            gradient.endAngle = .pi * 3.0 / 2.0
            gradient.mask = self.createCircleLayer(forStartAngle: -(CGFloat)(Double.pi/4.0), endAngle: .pi * 3.0 / 2.0 - .pi / 32.0, color:strokeColor)
            _indefiniteAnimatedLayer = gradient
            
            let animationDuration: TimeInterval = 1
            let linearCurve = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.fromValue = 0
            rotationAnimation.toValue = .pi * 2.0
            rotationAnimation.duration = animationDuration
            rotationAnimation.timingFunction = linearCurve
            rotationAnimation.isRemovedOnCompletion = false
            rotationAnimation.repeatCount = .infinity
            rotationAnimation.fillMode = kCAFillModeForwards
            rotationAnimation.autoreverses = false
            _indefiniteAnimatedLayer?.add(rotationAnimation, forKey: "rotate")
        }
        return _indefiniteAnimatedLayer!
    }
    
    private func setupCheckmarkLayer() {
        if _checkmarkLayer != nil {
            _checkmarkLayer?.removeFromSuperlayer()
            _checkmarkLayer = nil
        }
        var pathRect: CGRect = self.indefiniteAnimatedLayer().bounds
        //make it square
        let sideSize: CGFloat = min(pathRect.size.width, pathRect.size.height)
        pathRect.size.height = sideSize * 0.4
        pathRect.size.width = pathRect.size.height
        //center it
        pathRect.origin.x = self.layer.bounds.size.width / 2.0 - pathRect.size.width / 2.0
        pathRect.origin.y = self.layer.bounds.size.height / 2.0 - pathRect.size.height / 2.0
        let left = CGPoint(x: pathRect.minX, y: pathRect.minY + pathRect.height * 0.55)
        let bottom = CGPoint(x: pathRect.minX + pathRect.width * 0.42, y: pathRect.maxY)
        let right = CGPoint(x: pathRect.maxX, y: pathRect.minY + pathRect.height * 0.05)
        let path = UIBezierPath()
        path.move(to: left)
        path.addLine(to: bottom)
        path.addLine(to: right)
        let pathLayer = CAShapeLayer()
        pathLayer.frame = self.layer.bounds
        pathLayer.bounds = pathRect
        pathLayer.isGeometryFlipped = false
        pathLayer.path = path.cgPath
        pathLayer.strokeColor = strokeColor.cgColor
        pathLayer.fillColor = nil
        pathLayer.lineWidth = self.strokeThickness
        pathLayer.lineJoin = kCALineJoinRound
        pathLayer.lineCap = kCALineCapRound
        pathLayer.allowsEdgeAntialiasing = true
        self.layer.addSublayer(pathLayer)
        _checkmarkLayer = pathLayer
    }
    
    private func setupCrossLayer() {
        if _crossLayer != nil {
            _crossLayer?.removeFromSuperlayer()
            _crossLayer = nil
        }
        var pathRect: CGRect = indefiniteAnimatedLayer().bounds
        //make it square
        let sideSize: CGFloat = min(pathRect.size.width, pathRect.size.height)
        pathRect.size.height = sideSize * 0.4
        pathRect.size.width = pathRect.size.height
        //center it
        pathRect.origin.x = self.layer.bounds.size.width / 2.0 - pathRect.size.width / 2.0
        pathRect.origin.y = self.layer.bounds.size.height / 2.0 - pathRect.size.height / 2.0
        let topLeft = CGPoint(x: pathRect.minX, y: pathRect.minY)
        let topRight = CGPoint(x: pathRect.maxX, y: pathRect.minY)
        let bottomLeft = CGPoint(x: pathRect.minX, y: pathRect.maxY)
        let bottomRight = CGPoint(x: pathRect.maxX, y: pathRect.maxY)
        let path = UIBezierPath()
        path.move(to: topLeft)
        path.addLine(to: bottomRight)
        path.move(to: topRight)
        path.addLine(to: bottomLeft)
        let pathLayer = CAShapeLayer()
        pathLayer.frame = self.layer.bounds
        pathLayer.bounds = pathRect
        pathLayer.isGeometryFlipped = false
        pathLayer.path = path.cgPath
        pathLayer.strokeColor = errorColor.cgColor
        pathLayer.fillColor = nil
        pathLayer.lineWidth = self.strokeThickness
        pathLayer.lineJoin = kCALineJoinRound
        pathLayer.lineCap = kCALineCapRound
        pathLayer.allowsEdgeAntialiasing = true
        self.layer.addSublayer(pathLayer)
        _crossLayer = pathLayer
    }
    
    override open var frame: CGRect{
        get{
            return super.frame
        }
        set{
            if frame != newValue {
                super.frame = newValue
                if self.superview != nil {
                    self.layoutAnimatedLayer()
                }
            }
        }
    }
}
