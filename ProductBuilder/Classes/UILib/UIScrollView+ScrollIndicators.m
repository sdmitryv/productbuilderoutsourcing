//
//  UIScrollView+UIScrollView_ScrollIndicators.m
//  ProductBuilder
//
//  Created by valery on 2/4/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UIScrollView+ScrollIndicators.h"

#import <objc/runtime.h>
#import <objc/message.h>

char* const originalClassNameKey = "originalClassNameKeyTag";
char* const ignoreAlphaZeroKey = "ignoreAlphaZeroKeyTag";
char* const horizontalScrollIndicatorAlwaysVisibleKey = "horizontalScrollIndicatorAlwaysVisibleKeyTag";
const char* verticalScrollIndicatorAlwaysVisibleKey = "verticalScrollIndicatorAlwaysVisibleKeyTag";
//const char* verticalUIImageViewKey = "verticalUIImageViewKeyTag";
//const char* horizontalUIImageViewKey = "horizontalUIImageViewKeyTag";

@implementation UIScrollView(ScrollIndicators)

-(BOOL)horizontalScrollIndicatorAlwaysVisible{
    NSNumber* value = objc_getAssociatedObject(self, horizontalScrollIndicatorAlwaysVisibleKey);
    return value ? [value boolValue] : FALSE;
}

-(void)setHorizontalScrollIndicatorAlwaysVisible:(BOOL)value{
    if (value==self.horizontalScrollIndicatorAlwaysVisible) return;
    objc_setAssociatedObject(self, horizontalScrollIndicatorAlwaysVisibleKey, @(value), OBJC_ASSOCIATION_RETAIN);
    if (self.horizontalScrollIndicatorAlwaysVisible){
        [self replaceClassToCustom];
    }
    else{
        UIImageView* horizontalUIImageView = [self horizontalUIImageView];
        if (horizontalUIImageView){
            [[self class] revertObjectClassToOriginal:horizontalUIImageView];
        }
        if (!self.verticalScrollIndicatorAlwaysVisible){
            [[self class] revertObjectClassToOriginal:self];
        }
    }
    adjustScrollIndicators(self);
}

-(BOOL)verticalScrollIndicatorAlwaysVisible{
    NSNumber* value = objc_getAssociatedObject(self, verticalScrollIndicatorAlwaysVisibleKey);
    return value ? [value boolValue] : FALSE;
}

-(void)setVerticalScrollIndicatorAlwaysVisible:(BOOL)value{
    objc_setAssociatedObject(self, verticalScrollIndicatorAlwaysVisibleKey, @(value), OBJC_ASSOCIATION_RETAIN);
    if (self.verticalScrollIndicatorAlwaysVisible){
        [self replaceClassToCustom];
    }
    else{
        UIImageView* verticalUIImageView = [self verticalUIImageView];
        if (verticalUIImageView){
            [[self class] revertObjectClassToOriginal:verticalUIImageView];
        }
        if (!self.horizontalScrollIndicatorAlwaysVisible){
            [[self class] revertObjectClassToOriginal:self];
        }
    }
    adjustScrollIndicators(self);
    
}

-(UIImageView*)verticalUIImageView{
    //UIImageView* imageView = objc_getAssociatedObject(self, verticalUIImageViewKey);
    //if (imageView) return imageView;
    for (UIView* view in self.subviews){
        if ([view isKindOfClass:[UIImageView class]] && view.frame.size.width < 10 && view.frame.size.height >= view.frame.size.width && view.autoresizingMask == UIViewAutoresizingFlexibleLeftMargin){
            //objc_setAssociatedObject(self, verticalUIImageViewKey, view, OBJC_ASSOCIATION_ASSIGN);
            [[self class]replaceImageViewToCustom:(UIImageView*)view];
            return (UIImageView*)view;
        }
    }
    return nil;
}

-(UIImageView*)horizontalUIImageView{
    //UIImageView* imageView = objc_getAssociatedObject(self, horizontalUIImageViewKey);
    //if (imageView) return imageView;
    for (UIView* view in self.subviews){
        if ([view isKindOfClass:[UIImageView class]] && view.frame.size.height < 10 && view.frame.size.height <= view.frame.size.width && view.autoresizingMask == UIViewAutoresizingFlexibleTopMargin){
            //objc_setAssociatedObject(self, horizontalUIImageViewKey, view, OBJC_ASSOCIATION_ASSIGN);
            [[self class]replaceImageViewToCustom:(UIImageView*)view];
            return (UIImageView*)view;
        }
    }
    return nil;
}

+(void)revertObjectClassToOriginal:(id) object{
    if (!object) return;
    NSString* originalClassName = objc_getAssociatedObject(object, originalClassNameKey);
    if (!originalClassName) return;
    Class c = NSClassFromString(originalClassName);
    object_setClass(object, c);
    objc_setAssociatedObject(object, originalClassNameKey, nil, OBJC_ASSOCIATION_RETAIN);
}

+(void)replaceImageViewToCustom:(UIImageView*)imageView{
    if (!imageView) return;
    if (objc_getAssociatedObject(imageView, originalClassNameKey)) return;
    Class objectClass = object_getClass(imageView);
    NSString *newClassName = [NSString stringWithFormat:@"%@CustomAlpha", NSStringFromClass(objectClass)];
    Class c = NSClassFromString(newClassName);
    SEL selectorToOverride = @selector(setAlpha:);
    if (!c) {
        c = objc_allocateClassPair(objectClass, [newClassName UTF8String], 0);
        Method m = class_getInstanceMethod(objectClass, selectorToOverride);
        class_addMethod(c, selectorToOverride, (IMP)customSetAlpha, method_getTypeEncoding(m));
        objc_registerClassPair(c);
    }
    // change the class of the object
    objc_setAssociatedObject(imageView, originalClassNameKey, NSStringFromClass(objectClass), OBJC_ASSOCIATION_RETAIN);
    object_setClass(imageView, c);
}

-(void)replaceClassToCustom{
    if (objc_getAssociatedObject(self, originalClassNameKey)) return;
    Class objectClass = object_getClass(self);
    NSString *newClassName = [NSString stringWithFormat:@"%@CustomContentSize", NSStringFromClass(objectClass)];
    Class newClass = NSClassFromString(newClassName);
    if (!newClass) {
        newClass = objc_allocateClassPair(objectClass, [newClassName UTF8String], 0);
        SEL selectorToOverride = @selector(setContentSize:);
        Method m = class_getInstanceMethod(objectClass, selectorToOverride);
        class_addMethod(newClass, selectorToOverride, (IMP)customSetContentSize, method_getTypeEncoding(m));
        
        selectorToOverride = @selector(setContentInset:);
        m = class_getInstanceMethod(objectClass, selectorToOverride);
        class_addMethod(newClass, selectorToOverride, (IMP)customSetContentInset, method_getTypeEncoding(m));

        selectorToOverride = @selector(layoutSubviews);
        m = class_getInstanceMethod(objectClass, selectorToOverride);
        class_addMethod(newClass, selectorToOverride, (IMP)customLayoutSubviews, method_getTypeEncoding(m));

        objc_registerClassPair(newClass);
    }
    // change the class of the object
    objc_setAssociatedObject(self, originalClassNameKey, NSStringFromClass(objectClass), OBJC_ASSOCIATION_RETAIN);
    object_setClass(self, newClass);
}

void customSetAlpha(id self, SEL _cmd, CGFloat alpha) {
    NSNumber* value = objc_getAssociatedObject(self, ignoreAlphaZeroKey);
    if ([self alpha]==alpha || (alpha==0.0f && value && [value boolValue])) return;
    
    struct objc_super superData = {
        .receiver = self,
        .super_class = class_getSuperclass(object_getClass(self))
    };
    void (*super_setAlpha)(struct objc_super *, SEL, CGFloat) = (void(*)(struct objc_super *, SEL, CGFloat))&objc_msgSendSuper;
    (*super_setAlpha)(&superData, _cmd, alpha);
}

void customSetContentSize(id self, SEL _cmd, CGSize contentSize) {
    
    struct objc_super superData = {
        .receiver = self,
        .super_class = class_getSuperclass(object_getClass(self))
    };
    void (*super_setContentSize)(struct objc_super *, SEL, CGSize) = (void(*)(struct objc_super *, SEL, CGSize))&objc_msgSendSuper;
    (*super_setContentSize)(&superData, _cmd, contentSize);
    
    
    adjustScrollIndicators(self);
}

void customSetContentInset(id self, SEL _cmd, UIEdgeInsets edgeInsets) {
    
    struct objc_super superData = {
        .receiver = self,
        .super_class = class_getSuperclass(object_getClass(self))
    };
    void (*super_setContentInset)(struct objc_super *, SEL, UIEdgeInsets) = (void(*)(struct objc_super *, SEL, UIEdgeInsets))&objc_msgSendSuper;
    (*super_setContentInset)(&superData, _cmd, edgeInsets);
    
    
    adjustScrollIndicators(self);
}

void customLayoutSubviews(id self, SEL _cmd){
    struct objc_super superData = {
        .receiver = self,
        .super_class = class_getSuperclass(object_getClass(self))
    };
    void (*super_m)(struct objc_super *, SEL) = (void(*)(struct objc_super *, SEL))&objc_msgSendSuper;
    (*super_m)(&superData, _cmd);
    adjustScrollIndicators(self);
}

void adjustScrollIndicators(UIScrollView* sc){
    if (!sc.superview) return;
    if (sc.verticalScrollIndicatorAlwaysVisible && sc.showsVerticalScrollIndicator) {
        UIImageView* imageView = [sc verticalUIImageView];
        if (imageView){
            if (sc.frame.size.height < (sc.contentSize.height + sc.contentInset.top +  sc.contentInset.bottom)){
                imageView.alpha = 1.0f;
                objc_setAssociatedObject(imageView, ignoreAlphaZeroKey,@(TRUE) , OBJC_ASSOCIATION_RETAIN);
                [sc performSelector:@selector(flashScrollIndicators)];
            }
            else{
                objc_setAssociatedObject(imageView, ignoreAlphaZeroKey,nil , OBJC_ASSOCIATION_RETAIN);
                imageView.alpha = 0.0f;
            }
        }
        
    }
    if (sc.horizontalScrollIndicatorAlwaysVisible && sc.showsVerticalScrollIndicator) {
        UIImageView* imageView = [sc horizontalUIImageView];
        if (imageView){
            if (sc.frame.size.width < (sc.contentSize.width  + sc.contentInset.left +  sc.contentInset.right)){
                imageView.alpha = 1.0f;
                objc_setAssociatedObject(imageView, ignoreAlphaZeroKey,@(TRUE) , OBJC_ASSOCIATION_RETAIN);
                [sc performSelector:@selector(flashScrollIndicators)];
            }
            else{
                objc_setAssociatedObject(imageView, ignoreAlphaZeroKey ,nil , OBJC_ASSOCIATION_RETAIN);
                imageView.alpha = 0.0f;
            }
        }
    }
}

@end

