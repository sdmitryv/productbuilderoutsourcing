//
//  UISearchBarTransparent.h
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UISearchBarBig : UISearchBar {
	
    UITextField* textField;
}

- (UITextField*)textField;
	
@end
