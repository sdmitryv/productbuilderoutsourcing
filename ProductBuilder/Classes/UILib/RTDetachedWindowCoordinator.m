//
//  RTDetachedWindowCoordinator.m
//  ProductBuilder
//
//  Created by valery on 3/21/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "RTDetachedWindowCoordinator.h"
#import "UIView+FirstResponder.h"

@interface RTDetachedWindowCoordinator(){
    BOOL showChildController;
}

@property (nonatomic, retain)UIWindow* window;
@property (nonatomic, retain)UIWindow* userWindow;
@property (nonatomic, retain)UIViewController* childViewController;
@property (nonatomic, copy) void(^completion)(void);
@end

@implementation RTDetachedWindowCoordinator

-(id)init{
    if ((self=[super init])){
        UIWindow* window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window = window;
        [window release];
    }
    return self;
}

-(void)loadView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    self.view = view;
    [view release];
}

-(void)dealloc{
    [_completion release];
    [_window release];
    [_userWindow release];
    [_childViewController release];
    [super dealloc];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.childViewController){
        if (showChildController){
            showChildController = FALSE;
            [self presentViewController:self.childViewController animated:TRUE completion:nil];
        }
        else{
            if (self.userWindow){
                self.childViewController = nil;
                [self.userWindow makeKeyAndVisible];
                self.window.rootViewController = nil;
                if (self.completion){
                    self.completion();
                    self.completion = nil;
                }
            }
        }
    }
}

-(void)presentViewControllerInDetachedWindow:(UIViewController*)childController completion:(void (^)(void))completion{
    UIView * firstResponder = [[UIApplication sharedApplication].windows[0] getFirstResponder];
    if (firstResponder != nil) {
        [firstResponder resignFirstResponder];
    }
    self.childViewController = childController;
    self.window.rootViewController = self;
    self.completion = completion;
    self.userWindow = [UIApplication sharedApplication].windows[0];
    showChildController = TRUE;
    [self.window makeKeyAndVisible];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}


+(void)presentViewControllerInDetachedWindow:(UIViewController*)childController completion:(void (^)(void))completion{
    RTDetachedWindowCoordinator* coodinator = [[RTDetachedWindowCoordinator alloc]init];
    [coodinator presentViewControllerInDetachedWindow:childController completion:^{
        //retain coordinator here to avoid its dealloc
        [coodinator view];
        if (completion){
            completion();
        }
    }];
    [coodinator release];
}

@end
