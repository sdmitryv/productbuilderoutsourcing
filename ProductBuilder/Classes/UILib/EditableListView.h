//
//  EditableListView.h
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/4/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EditableListView;

@protocol EditableListViewProtocol

- (id)editableListView:(EditableListView*)listView addItem:(NSString *)name;
- (BOOL)editableListView:(EditableListView*)listView deleteItem:(id)item;
@optional
- (void)editableListView:(EditableListView*)listView didSelectItem:(id)anItem;

@end

@interface EditableListView : UIView <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	UITextField	* itemTextField;
	UIButton	* addButton;
	UITableView	* listTableView;
	UIButton	* deleteButton;
	
	NSMutableArray	* itemList;
	NSObject<EditableListViewProtocol>*	delegate;
	BOOL			enabled;
    NSUInteger      maxItemLength;
}

@property (nonatomic, retain) UITextField	* itemTextField;
@property (nonatomic, retain) UIButton		* addButton;
@property (nonatomic, retain) UITableView	* listTableView;
@property (nonatomic, retain) NSMutableArray	* itemList;
@property (nonatomic, assign) NSObject<EditableListViewProtocol>*	delegate;
@property (nonatomic, assign) BOOL	enabled;
@property (nonatomic, assign) NSUInteger	maxItemLength;

-(NSInteger)selectedRowIndex;
-(id)selectedObject;

@end
