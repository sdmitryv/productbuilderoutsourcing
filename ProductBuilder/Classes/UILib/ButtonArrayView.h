//
//  ButtonArrayView.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 8/9/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableListViewController.h"

@class ButtonArrayView;

@protocol ButtonArrayViewDelegate

- (void)buttonArrayView:(ButtonArrayView*)controller didSelectItemAtIndex:(NSInteger)index button:(UIButton*)button;
@optional
- (void)buttonArrayView:(ButtonArrayView*)controller didSelectItem:(id)item;
- (BOOL)buttonArrayView:(ButtonArrayView*)controller enableItemButton:(NSObject*)item;
- (BOOL)buttonArrayView:(ButtonArrayView*)controller shouldShowMoreWithButton:(UIButton*)button;

@end

@interface ButtonArrayView : UITableView<UITableViewDelegate, UITableViewDataSource,SimpleTableListViewControllerDelegate> {
    NSUInteger columnCount;
	NSUInteger buttonHeight;
	NSUInteger buttonWidth;
	id buttonArrayDelegate;
	NSUInteger rowHorizontalMargin;
	NSUInteger rowVerticalMargin;
    id list;
    BOOL enabled;
    BOOL showMore;
    BOOL moreVisible;
    NSUInteger rowCount;
    BOOL internalLayout;
}

@property (nonatomic, retain) id list;
@property (nonatomic, assign) NSUInteger columnCount;
@property (nonatomic, assign) NSUInteger buttonHeight;
@property (nonatomic, assign) NSUInteger buttonWidth;
@property (nonatomic, assign) NSUInteger rowHorizontalMargin;
@property (nonatomic, assign) NSUInteger rowVerticalMargin;
@property (nonatomic, assign) id<ButtonArrayViewDelegate> buttonArrayDelegate;
@property(nonatomic,assign)BOOL enabled;
@property(nonatomic,assign)BOOL showMore;
@property(nonatomic,retain)NSString* moreTitle;
@property(nonatomic,retain)NSString* moreButtonTitle;
-(UIButton*) buttonAtIndex:(NSUInteger)index;
-(UIButton*) buttonForValue:(NSObject*)value;
-(void) updateButtonsState;
@end
