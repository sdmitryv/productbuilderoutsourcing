//
//  UILoopProgressView.h
//  ProductBuilder
//
//  Created by valera on 10/17/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOGlassButton.h"

@interface UILoopProgressView : UIProgressView{
    //UIProgressView* progressView;
    //MOGlassButtonBlack* progressView;
    BOOL shouldAnimate;
    BOOL canAnimate;
    BOOL animating;
    NSTimer* timer;
}

-(void)startAnimation;
-(void)stopAnimation;
///-(CGFloat)progress;
///-(void)setProgress:(CGFloat)value;
@end
