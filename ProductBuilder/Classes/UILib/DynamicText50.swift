//
//  RPDynamicFontLabel.swift
//  RPlus
//
//  Created by Alexander Martyshko on 11/28/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

@objc class RPDynamicHelper: NSObject {
    class func dynamicFont(forOriginalFont: UIFont) -> UIFont? {
        
        // if original font is already a dynamic font - do nothing
        if let textStyleAttr = forOriginalFont.fontDescriptor.fontAttributes[UIFontDescriptorTextStyleAttribute] as? String,
            textStyleAttr == UIFontTextStyle.caption1.rawValue ||
                textStyleAttr == UIFontTextStyle.caption1.rawValue ||
                textStyleAttr == UIFontTextStyle.footnote.rawValue ||
                textStyleAttr == UIFontTextStyle.subheadline.rawValue ||
                textStyleAttr == UIFontTextStyle.body.rawValue ||
                textStyleAttr == UIFontTextStyle.headline.rawValue ||
                textStyleAttr == UIFontTextStyle.title3.rawValue ||
                textStyleAttr == UIFontTextStyle.title2.rawValue {
            return nil
        }
        
        var dynamicFont: UIFont? = nil
        
        switch forOriginalFont.pointSize {
        case 12:
            dynamicFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        case 13:
            dynamicFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        case 15:
            dynamicFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
        case 17:
            dynamicFont = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad ? UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline) : UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
        case 20:
            dynamicFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title3)
        case 22:
            dynamicFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title2)
        default: break
            
        }
        
        return dynamicFont
    }
}

class RPDynamicFontLabel: UILabel {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    class func dynamicTextHeight(withFontPointSize: CGFloat) -> CGFloat {
        let label = RPDynamicFontLabel()
        label.font = UIFont.systemFont(ofSize: withFontPointSize)
        label.text = "OK"
        label.sizeToFit()
        return label.bounds.size.height
    }
    
    fileprivate var originalFont: UIFont?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }

    fileprivate func initDefaults() {
        NotificationCenter.default.addObserver(self, selector: #selector(RPDynamicFontLabel.didChangePreferredContentSize), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        adjustFont()
    }
    
    @objc fileprivate func didChangePreferredContentSize() {
        adjustFont()
    }
    
    fileprivate var isSettingFontInside = false
    fileprivate func adjustFont() {
        
        if originalFont == nil {
            originalFont = self.font
        }
        
        guard let _originalFont = originalFont else {
            return
        }
        
        if let dynamicFont = RPDynamicHelper.dynamicFont(forOriginalFont: _originalFont) {
            isSettingFontInside = true
            self.font = UIFont.init(descriptor: _originalFont.fontDescriptor, size: dynamicFont.pointSize)
            isSettingFontInside = false
        }
    }
    
    override var font: UIFont! {
        get {
            return super.font
        }
        set {
            super.font = newValue
            if !isSettingFontInside {
                originalFont = nil
                adjustFont()
            }
        }
    }
}


class RPDynamicFontButton: UIButton {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate var originalFont: UIFont?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        NotificationCenter.default.addObserver(self, selector: #selector(RPDynamicFontLabel.didChangePreferredContentSize), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        adjustFont()
    }
    
    @objc fileprivate func didChangePreferredContentSize() {
        adjustFont()
    }
    
    fileprivate var isSettingFontInside = false
    fileprivate func adjustFont() {
        
        if originalFont == nil {
            originalFont = self.titleLabel?.font
        }
        
        guard let _originalFont = originalFont else {
            return
        }
        
        if let dynamicFont = RPDynamicHelper.dynamicFont(forOriginalFont: _originalFont) {
            isSettingFontInside = true
            self.titleLabel?.font = UIFont.init(descriptor: _originalFont.fontDescriptor, size: dynamicFont.pointSize)
            isSettingFontInside = false
        }
    }
    
    var titleFont: UIFont? {
        get {
            return titleLabel?.font
        }
        set {
            titleLabel?.font = newValue
            if !isSettingFontInside {
                originalFont = nil
                adjustFont()
            }
        }
    }
}

class RPDynamicFontTextField: UITextField {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate var originalFont: UIFont?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        NotificationCenter.default.addObserver(self, selector: #selector(RPDynamicFontLabel.didChangePreferredContentSize), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        adjustFont()
    }
    
    @objc fileprivate func didChangePreferredContentSize() {
        adjustFont()
    }
    
    fileprivate var isSettingFontInside = false
    fileprivate func adjustFont() {
        
        if originalFont == nil {
            originalFont = self.font
        }
        
        guard let _originalFont = originalFont else {
            return
        }
        
        if let dynamicFont = RPDynamicHelper.dynamicFont(forOriginalFont: _originalFont) {
            isSettingFontInside = true
            self.font = UIFont.init(descriptor: _originalFont.fontDescriptor, size: dynamicFont.pointSize)
            isSettingFontInside = false
        }
    }
    
    override var font: UIFont! {
        get {
            return super.font
        }
        set {
            super.font = newValue
            if !isSettingFontInside {
                originalFont = nil
                adjustFont()
            }
        }
    }
}

@objc class RPDynamicFontTextView: UITextView {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate var originalFont: UIFont?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        initDefaults()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        NotificationCenter.default.addObserver(self, selector: #selector(RPDynamicFontLabel.didChangePreferredContentSize), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
        adjustFont()
    }
    
    @objc fileprivate func didChangePreferredContentSize() {
        adjustFont()
        invalidateIntrinsicContentSize()
        setNeedsDisplay()
    }
    
    fileprivate var isSettingFontInside = false
    fileprivate func adjustFont() {
        
        if originalFont == nil {
            originalFont = self.font
        }
        
        guard let _originalFont = originalFont else {
            return
        }
        
        if let dynamicFont = RPDynamicHelper.dynamicFont(forOriginalFont: _originalFont) {
            isSettingFontInside = true
            self.font = UIFont.init(descriptor: _originalFont.fontDescriptor, size: dynamicFont.pointSize)
            isSettingFontInside = false
        }
    }
    
    override var font: UIFont! {
        get {
            return super.font
        }
        set {
            super.font = newValue
            if !isSettingFontInside {
                originalFont = nil
                adjustFont()
            }
        }
    }
}
