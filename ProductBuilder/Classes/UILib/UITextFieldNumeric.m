//
//  UITextFieldNumeric.m
//  ProductBuilder
//
//  Created by Valera on 11/16/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "UITextFieldNumeric.h"
#import "DecimalHelper.h"
#import "UITextField+Input.h"
#import "Product_Builder-Swift.h"

typedef struct{
    BOOL textFieldNumericValueChanged;
    BOOL textFieldNumericShouldChangeValue;
} UITextFieldNumericDelegateFlags;

@interface UITextFieldNumeric(){
    UITextFieldNumericDelegateFlags ownerFlags;
    BOOL _initializedDefalts;
    
    UIFont *_originalFont;
    BOOL _isSettingFontInside;
}
-(void)initDefaults;
-(NSString*)stringValue:(NSDecimalNumber*)value;
@property (nonatomic, retain)NSString* lastParsedText;
@end

@implementation UITextFieldNumeric

-(void)initDefaults{
    if (_initializedDefalts) return;
    _initializedDefalts = YES;
    _textHandler = [[UITextFieldHandler alloc]initWithTextField:self];
    _textHandler.textFieldHandlerDelegate = self;
    //self.decimalNumberValue = nil;
    //	_textHandler.inputTextField = self;
    _textHandler.shouldProcessDidEndEditing = TRUE;
    [super setDelegate:_textHandler];
    self.useVirtualNumericKeyboard = FALSE;
    self.keyboardType = UIKeyboardTypeNumberPad;
    //self.decimalValue = CPDecimalFromInt(0);
    self.decimalNumberValue = nil;
    if (SYSTEM_VERSION_GREATER_THAN(@"9"))
        if ([self respondsToSelector:@selector(inputAssistantItem)]) {
#ifdef __IPHONE_9_0
            UITextInputAssistantItem *inputAssistantItem = [self inputAssistantItem];
            inputAssistantItem.leadingBarButtonGroups = @[];
            inputAssistantItem.trailingBarButtonGroups = @[];
#else
            NSObject* inputAssistantItem = [self performSelector:@selector(inputAssistantItem) withObject:nil];
            if ([inputAssistantItem respondsToSelector:@selector(setLeadingBarButtonGroups:)]){
                [inputAssistantItem performSelector:@selector(setLeadingBarButtonGroups:) withObject:@[]];
            }
            if ([inputAssistantItem respondsToSelector:@selector(setTrailingBarButtonGroups:)]){
                [inputAssistantItem performSelector:@selector(setTrailingBarButtonGroups:) withObject:@[]];
            }
#endif
        }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangePreferredContentSize) name:UIContentSizeCategoryDidChangeNotification object:nil];
    [self adjustFont];
}

-(void)didChangePreferredContentSize {
    [self adjustFont];
}

-(void)adjustFont {
    if (!_originalFont) {
        _originalFont = [self.font retain];
    }
    
    UIFont *dynamicFont = [RPDynamicHelper dynamicFontForOriginalFont:_originalFont];
    if (dynamicFont) {
        _isSettingFontInside = YES;
        self.font = [UIFont fontWithDescriptor:_originalFont.fontDescriptor size:dynamicFont.pointSize];
        _isSettingFontInside = NO;
    }
}

-(void)setFont:(UIFont *)font {
    [super setFont:font];
    if (!_isSettingFontInside) {
        [_originalFont release];
        _originalFont = nil;
        [self adjustFont];
    }
}

-(NSString*)stringValue:(NSDecimalNumber*)value{
	return [_textHandler stringValue:value];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
		[self initDefaults];
	}
    return self;
}

- (id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		[self initDefaults];
	}
	return self;
}

-(void)dealloc{
    [_lastParsedText release];
	[_textHandler release];
    [_decimalNumberValue release];
    [super dealloc];
}

-(void)setUseVirtualNumericKeyboard:(BOOL)value{
    if (_useVirtualNumericKeyboard==value)return;
    _useVirtualNumericKeyboard = value;
    [self setKeyboardVisible:!_useVirtualNumericKeyboard];
}

-(void)setStyle:(NSTextFormatterStyle)value{
	_textHandler.style = value;
	super.text = [self stringValue:_decimalNumberValue];
}

-(BOOL)useSmartAmount {
    return _textHandler.useSmartAmount;
}

-(void)setUseSmartAmount:(BOOL)useSmartAmount {
    _textHandler.useSmartAmount = useSmartAmount;
}

-(NSTextFormatterStyle)style{
	return _textHandler.style;
}

-(NSUInteger) maxTextLength {
    return _textHandler.maxTextLength;
}

-(void)setMaxTextLength:(NSUInteger)aMaxTextLength {
    _textHandler.maxTextLength = aMaxTextLength;
}

-(NSString*)percentSymbol{
    return _textHandler.percentSymbol;
}

-(void)setPercentSymbol:(NSString*)value{
    _textHandler.percentSymbol = value;
}

-(BOOL)enableNil{
    return _textHandler.enableNil;
}

-(void)setEnableNil:(BOOL)value{
    _textHandler.enableNil = value;
}

-(NSNumber*)maxValue{
	return _textHandler.maxValue;
}

-(void)setMaxValue:(NSNumber*)value{
	_textHandler.maxValue = value;
}

-(NSNumber*)minValue{
	return _textHandler.minValue;
}

-(void)setMinValue:(NSNumber*)value{
	_textHandler.minValue = value;
}


-(void)setFormatWidth:(NSUInteger)value{
    _textHandler.formatWidth = value;
}

-(NSUInteger)formatWidth{
    return _textHandler.formatWidth;
}

-(NSNumber*)customFracationDigits{
    return _textHandler.customFracationDigits;
}

-(void)setCustomFracationDigits:(NSNumber *)customFracationDigits{
    _textHandler.customFracationDigits = customFracationDigits;
}

-(double)doubleValue{
	return [_decimalNumberValue doubleValue];
}

-(void)setDoubleValue:(double)value{
    NSDecimalNumber* dn = [[NSDecimalNumber alloc]initWithDouble:value];
	self.decimalNumberValue = dn;
    [dn release];
}

-(BOOL)clearZero{
    
	return _textHandler.clearZero;
}

-(void)setClearZero:(BOOL)value{

    _textHandler.clearZero = value;
}


-(NSInteger)intValue{
	return [_decimalNumberValue integerValue];
}

-(void)setIntValue:(NSInteger)value{
    NSDecimalNumber* dn = [[NSDecimalNumber alloc]initWithInteger:value];
	self.decimalNumberValue = dn;
    [dn release];
}

-(NSDecimal)decimalValue{
	return [_decimalNumberValue decimalValue];
}

-(void)setDecimalValue:(NSDecimal)value{
	self.decimalNumberValue = [NSDecimalNumber decimalNumberWithDecimal:value];
}

-(void)setDecimalNumberValue:(NSDecimalNumber*)value{
    BOOL valueChanged = TRUE;
    if ((!_decimalNumberValue && !value) || (_decimalNumberValue && value && [_decimalNumberValue compare:value]==NSOrderedSame)){
        valueChanged = FALSE;
    }
    else{
        [_decimalNumberValue release];
        _decimalNumberValue = [value retain];
    }
    if (!flushingEditValue){
        NSString* text = [self stringValue:_decimalNumberValue];
        if (!self.isFirstResponder || valueChanged || ![text isEqualToString:super.text]){
            super.text = text;
        }
    }
    self.lastParsedText = self.text;
	if (valueChanged && !insideTextFieldNumericValueChanged && _owner) {
		if (ownerFlags.textFieldNumericValueChanged) {
            insideTextFieldNumericValueChanged = TRUE;
            BOOL oldFlushingEditValue = flushingEditValue;
            flushingEditValue = FALSE;
            [_owner textFieldNumericValueChanged:self];
            flushingEditValue = oldFlushingEditValue;
            insideTextFieldNumericValueChanged = FALSE;
		}
	}
}

-(BOOL)isDecimalValueActual{
    return [self.lastParsedText isEqualToString:self.text];
}

-(NSNumber*)numberValue{
	return _decimalNumberValue;
}

-(void)setNumberValue:(NSNumber*)value{
	self.decimalNumberValue = value ? [NSDecimalNumber decimalNumberWithString:[value descriptionWithLocale:nil] locale:nil] : nil;
}

-(void)parseTextIntoDecimal:(NSString*)text{
    NSDecimal d = D0;
    BOOL result = [_textHandler parseString:text result:&d];
    if (result || !_textHandler.enableNil){
        self.decimalValue = d;
    }
    else
        self.decimalNumberValue = nil;
}

-(void)flushEditValue{
    //parse current edit value
    if (!self.isFirstResponder) return;
    flushingEditValue = TRUE;
    [self parseTextIntoDecimal:self.text];
    flushingEditValue = FALSE;
}

-(void)setText:(NSString *)text{
    if (![self isFirstResponder] && [super.text isEqualToString:text]){
        return;   
    }
    if (_textHandler.internalTextAssign || self.style == NSTextFormatterNumbersStyle){
        [super setText:text];
        if (_textHandler.internalTextAssign)
            return;
    }
    
    [self parseTextIntoDecimal:text];
}

-(id<UITextFieldDelegate>)delegate {
    return _textHandler.delegate;
}

-(void)setDelegate:(id<UITextFieldDelegate>)delegate {
    _textHandler.delegate = delegate;
}

-(id<UITextFieldDelegate>)internalDelegate{
    return _textHandler;
}

-(void)setOwner:(id)value{
    _owner = value;
    ownerFlags.textFieldNumericShouldChangeValue = [_owner respondsToSelector:@selector(textFieldNumeric:shouldChangeValue:)];
    ownerFlags.textFieldNumericValueChanged = [_owner respondsToSelector:@selector(textFieldNumericValueChanged:)];
}

#pragma mark UITextFieldHandlerDelegate

-(BOOL)textFieldHandler:(UITextFieldHandler*)textFieldHandler shouldChangeValue:(NSDecimal*)value{
    if (_owner && ownerFlags.textFieldNumericShouldChangeValue){
        return [_owner textFieldNumeric:self shouldChangeValue:value];
    }
    return TRUE;
}

@end
