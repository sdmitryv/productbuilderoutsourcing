//
//  UIAppColor.m
//  iPadPOS
//
//  Created by valera on 3/25/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "UIColor+AppColors.h"
#import "Global.h"

static UIColor *_blueGrayColor = nil;

@implementation UIColor (AppColors)

+(UIColor*)blueGrayColor{
	if (!_blueGrayColor){
		_blueGrayColor = [MO_RGBCOLOR(103, 113, 139) retain];
	}
	return _blueGrayColor;
}

@end
