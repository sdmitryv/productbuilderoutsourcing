//
//  ProductTableViewCell.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 8/31/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    var product : Product?
    
    @IBOutlet var cluLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!

    @IBAction func copyButtonClick(_ sender: UIButton) {
        if product != nil {
            let pasteboard = UIPasteboard.general
            pasteboard.string = product!.clu
            sender.setTitle("Copied", for: UIControlState.normal)
            self.perform(#selector(ProductTableViewCell.updateTitle), with: sender, afterDelay: 1)
        }
    }
    
    func updateTitle(_ sender: UIButton) {
        sender.setTitle("Copy", for: UIControlState.normal)
    }
    
    func updateWithProduct(_ product : Product) {
        self.product = product
        dateLabel.text = DataUtils.readableDateTimeString(from: product.recCreated, format: "MM/dd HH:mm")
        cluLabel.text = product.clu
        descriptionLabel.text = product.description
        priceLabel.text = NSCurrencyFormatter.formatCurrency(from: product.price)
    }
}
