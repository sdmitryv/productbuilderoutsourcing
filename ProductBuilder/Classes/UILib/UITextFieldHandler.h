//
//  UITextFieldHandler.h
//  ProductBuilder
//
//  Created by Valera on 11/5/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NumpadKeyboardController.h"
#import "RTPopoverController.h"

typedef NS_ENUM(NSUInteger, NSTextFormatterStyle) {
    NSTextFormatterNoneStyle,
    NSTextFormatterCurrencyStyle,
    NSTextFormatterPercentStyle,
	NSTextFormatterQtyStyle,
    NSTextFormatterNumbersStyle,
    NSTextFormatterDecimalStyle,
    NSTextFormatterWeightStyle
};

@class UITextFieldNumeric;
@class VirtualNumberpadViewController;
@protocol UITextFieldNumericDelegate;
@class UITextFieldHandler;

@protocol UITextFieldHandlerDelegate
@optional
-(BOOL)textFieldHandler:(UITextFieldHandler*)textFieldHandler shouldChangeValue:(NSDecimal*)value;
@end

@interface UITextFieldHandler : NSObject<UITextFieldDelegate, RTPopoverControllerDelegate> {
	NSNumberFormatter* _formatter;
	NSTextFormatterStyle style;
	NSMutableCharacterSet *nonNumberSet;
	UITextField *inputTextField;
	BOOL forceSetStyle;
    NSNumber* maxValue;
    NSNumber* minValue;
    BOOL enableNil;
    NSString* percentSymbol;
    BOOL shouldProcessDidEndEditing;
    BOOL internalTextAssign;
    id<UITextFieldDelegate> delegate;
    BOOL internalDidBeginEditing;
    NSNumber* customFracationDigits;
    NSInteger maxTextLength;
    BOOL clearZero;
    UIResponder* prevResponder;
}

@property (nonatomic, assign) NSTextFormatterStyle style;
@property (nonatomic, readonly)NSNumberFormatter* formatter;
@property (nonatomic, retain)NSNumber* maxValue;
@property (nonatomic, retain)NSNumber* minValue;
@property (nonatomic, retain)NSNumber* customFracationDigits;
@property (nonatomic, assign)NSUInteger formatWidth;
@property (nonatomic, assign)BOOL enableNil;
@property (nonatomic, retain)NSString* percentSymbol;
@property (nonatomic, assign)UITextField *inputTextField;
@property (nonatomic, assign) BOOL shouldProcessDidEndEditing;
@property (nonatomic, readonly) BOOL internalTextAssign;
@property (nonatomic, assign)id<UITextFieldDelegate> delegate;
@property (nonatomic, assign) NSInteger maxTextLength;
@property (nonatomic, assign)BOOL clearZero;
@property (nonatomic, assign)id<UITextFieldHandlerDelegate> textFieldHandlerDelegate;
@property (nonatomic, assign) BOOL useSmartAmount;

- (id)initWithTextField:(UITextField *)textField;
- (id)initWithTextField:(UITextField *)textField style:(NSTextFormatterStyle)value;
-(BOOL)parseString:(NSString*)candidateString result:(NSDecimal*)decimalValue;
-(NSString*)stringValue:(NSDecimalNumber*)value;
+ (void)setNumberFormatterHandler:(BOOL(^)(NSNumberFormatter* formatter, NSTextFormatterStyle style))numberFormatterHandler;
+ (BOOL(^)(NSNumberFormatter* formatter, NSTextFormatterStyle style))numberFormatterHandler;
@end
