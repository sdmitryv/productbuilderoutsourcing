//
//  CustomCheckboxControl.swift
//  RPlus
//
//  Created by Alexander Martyshko on 10/24/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

protocol CustomCheckboxControlDelegate: AnyObject {
    func valueChanged(inControl: CustomCheckboxControl)
}

class CustomCheckboxControl: UIView {
    
    weak var delegate: CustomCheckboxControlDelegate?

    fileprivate var button: UIButton!
    
    fileprivate var _isOn: Bool = false
    var isOn: Bool {
        get {
            return _isOn
        }
        set {
            _isOn = newValue
            button.isSelected = _isOn
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return button.intrinsicContentSize
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initDefaults()
    }
    
    fileprivate func initDefaults() {
        
        button = UIButton()
        button.backgroundColor = UIColor.clear
        button.setImage(UIImage(named: "icon_multiselecte_selection_indicator"), for: UIControlState.normal)
        button.setImage(UIImage(named: "icon_multiselecte_checked"), for: UIControlState.selected)
        button.addTarget(self, action: #selector(CustomCheckboxControl.buttoTouch(sender:)), for: UIControlEvents.touchUpInside)
        self.addSubview(button)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false

        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[button]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button" : button]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[button]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["button" : button]))
    }
    
    @objc fileprivate func buttoTouch(sender: UIButton) {
        isOn = !isOn
        delegate?.valueChanged(inControl: self)
    }
    
    var contentHorizontalAlignment: UIControlContentHorizontalAlignment {
        get {
            return button.contentHorizontalAlignment
        }
        set {
            button.contentHorizontalAlignment = newValue
        }
    }
    
    var contentVerticalAlignment: UIControlContentVerticalAlignment {
        get {
            return button.contentVerticalAlignment
        }
        set {
            button.contentVerticalAlignment = newValue
        }
    }
}
