//
//  RTBaseListViewController.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/17/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIRoundedCornersView.h"
#import "GridTable.h"

@class RTBaseListView;
@protocol RTBaseListViewDelegate <NSObject>
-(void)baseList:(RTBaseListView *)listView sortWithColumn:(GridTableColumnDescription *)column order:(SortingMode)order;
@end


@interface RTBaseListView : UIView

@property (nonatomic, readonly) UITableView *tableView;
@property (nonatomic, readonly) UIRoundedCornersView *headerView;
@property (nonatomic, readonly) UILabel *countLabel;
@property (nonatomic, retain) NSArray *sortableColumns;
@property (nonatomic, assign) id<UITableViewDataSource, UITableViewDelegate> tableViewDelegateAndDataSource;
@property (nonatomic, assign) NSInteger headerHeight;
@property (nonatomic, assign) id<RTBaseListViewDelegate> delegate;
@property (nonatomic, readonly) GridTableColumnDescription *currentSortDescription;
@property (nonatomic, readonly) SortingMode currentSortOrder;
@property (nonatomic, assign) CGRect customTableFrame;

- (void)setCurrentSortDescription:(GridTableColumnDescription *)columnDescription order:(SortingMode)order;
- (void)setCountAndSortLabelsHidden:(BOOL)hidden;

@end
