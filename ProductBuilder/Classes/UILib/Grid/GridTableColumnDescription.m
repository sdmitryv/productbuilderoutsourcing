//
//  ColumnDescription.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 4/25/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableColumnDescription.h"

#define DEFAULT_CELL_WIDTH 50

@implementation GridTableColumnDescription

@synthesize type = _type,
name = _name,
title = _title,
width = _width,
sortable = _sortable,
editable = _editable,
horizontalAlignment,
format = _format,
formatString = _formatString,
multiline = _multiline,
hideSeparator,
visible,imageHeight, imageWidth, heightToFit, customCellClass, isImageRight = _isImageRight;

@synthesize clearZero, decimalTextHandler, dateFormater;

- (id)copyWithZone: (NSZone *) zone {
    GridTableColumnDescription *newCol = [[GridTableColumnDescription allocWithZone:zone] init];
    newCol.id = _id;
    [newCol setType:_type];
    [newCol setName:_name];
    newCol->_title = [_title retain];
    newCol->_titleOriginal = [_titleOriginal retain];
    [newCol setWidth:_width];
    [newCol setSortable:_sortable];
    [newCol setEditable:_editable];
    [newCol setFormat:_format];
    [newCol setFormatString:_formatString];
    [newCol setHorizontalAlignment:horizontalAlignment];
    [newCol setHideSeparator:hideSeparator];
    [newCol setVisible:visible];
    [newCol setImageWidth:imageWidth];
    [newCol setImageHeight:imageHeight];
    return newCol;
}

- (NSString *)description{
    return _title;
}

-(BOOL)isEqual:(id)object{
    if (object == self)
        return YES;
    if (!object || ![object isKindOfClass:[self class]])
        return NO;
    return [_id isEqual:((GridTableColumnDescription*)object)->_id];
}

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep multilene:(BOOL)isMultiline {
    
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:formatString hideSeparator:hidesep])) {
        self.multiline = isMultiline;
    }
    return self;
}

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment {
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment hideSeparator:NO])) {
        
    }
    
    return self;
}
- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format {
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format hideSeparator:NO])) {
    }
    
    return self;
}

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString {
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:formatString hideSeparator:NO])) {
        
    }
    
    return self;
}


- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment hideSeparator:(BOOL)hidesep {
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:GridColumnFormatNone hideSeparator:hidesep])) {
    }
    return self;
}

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format hideSeparator:(BOOL)hidesep {
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:nil hideSeparator:hidesep])) {
    }
    return self;
}

- (id)initDateColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                  alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle{
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:FALSE alignment:alignment format:GridColumnFormatDate formatString:nil hideSeparator:FALSE])) {
        self.dateFormatterStyle = dateFormatStyle;
    }
    return self;
}

- (id)initDateTimeColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                      alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle timeFormatStyle:(NSDateFormatterStyle)timeFormatStyle;{
    if ((self = [self initTextColumnWithName:name title:title width:width sortable:sortable editable:FALSE alignment:alignment format:GridColumnFormatDateTime formatString:nil hideSeparator:FALSE])) {
        self.dateFormatterStyle = dateFormatStyle;
        self.timeFormatterStyle = timeFormatStyle;
    }
    return self;
}

- (id)initTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                    editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep {
    if ((self = [self init])) {
        if (editable) {
            self.type = TextColumn;
        }
        else {
            self.type = ColoredTextColumn;
        }
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = sortable;
        self.editable = editable;
        self.horizontalAlignment = alignment;
        self.format = format;
        self.formatString = formatString;
        self.hideSeparator = hidesep;
        self.visible = YES;
        self.multiline = NO;
    }
    return self;
}

- (id)initBoolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable {
    
    if ((self = [self initBoolColumnWithName:name title:title width:width sortable:sortable editable:editable hideSeparator:NO alignment:NSTextAlignmentLeft])) {
        
    }
    
    return self;
}

- (id)initBoolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable hideSeparator:(BOOL)hidesep alignment:(NSTextAlignment)alignment{
    if ((self = [self init])) {
        self.type = BoolColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = sortable;
        self.editable = editable;
        self.hideSeparator = hidesep;
        self.horizontalAlignment = alignment;
        self.visible = YES;
    }
    return self;
}

- (id)initButtonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width {
    if ((self = [self initButtonColumnWithName:name title:title width:width hideSeparator:NO alignment:NSTextAlignmentCenter])) {
        
    }
    
    return self;
}

- (id)initButtonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width hideSeparator:(BOOL)hidesep alignment:(NSTextAlignment)alignment{
    if ((self = [self init])) {
        self.type = ButtonColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.hideSeparator = hidesep;
        self.visible = YES;
        self.horizontalAlignment = alignment;
    }
    return self;
}

-(id)initImageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight {
    if ((self = [self init])) {
        self.type = ImageWithTextColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = TRUE;
        self.visible = TRUE;
        self.imageWidth = imgWidth;
        self.imageHeight = imgHeight;
    }
    
    return self;
}

- (id)initCustomColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable alignment:(NSTextAlignment)alignment cellClass:(Class)cellClass{
    if ((self = [self init])) {
        self.type = CustomColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = sortable;
        self.visible = TRUE;
        self.horizontalAlignment = alignment;
        self->customCellClass = cellClass;
    }
    return self;
}
-(id)initImageColumnWithName:(NSString*)name title:(NSString*)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight{
    if ((self = [self init])) {
        self.type = ImageColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = TRUE;
        self.visible = TRUE;
        self.imageWidth = imgWidth;
        self.imageHeight = imgHeight;
    }
    
    return self;
}
-(id)initImageColumnWithName:(NSString*)name title:(NSString*)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight sortable:(BOOL)sortable {
    if ((self = [self init])) {
        self.type = ImageColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = sortable;
        self.visible = TRUE;
        self.imageWidth = imgWidth;
        self.imageHeight = imgHeight;
    }
    
    return self;
}
- (id)initCustomColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable cellClass:(Class)cellClass{
    return [self initCustomColumnWithName:name title:title width:width sortable:sortable alignment:NSTextAlignmentCenter cellClass:cellClass];
}

- (id)initListColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width {
    if ((self = [self init])) {
        self.type = ListColumn;
        self.name = name;
        self.title = title;
        self.width = width;
        self.sortable = NO;
        self.visible = YES;
    }
    return self;
}

-(id)initImageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat) imgHeight isImageRight:(BOOL)isImageRight{
    
    if (self = [self initImageTextColumnWithName:name title:title width:width imageWidth:imgWidth imageHeight:imgHeight]) {
        
        self.isImageRight = isImageRight;
    }
    
    return self;
}

- (id)init {
    if ((self = [super init])) {
        _id = [[BPUUID UUID] retain];
        _name = nil;
        _title = nil;
        _width = DEFAULT_CELL_WIDTH;
        _sortable = YES;
        _editable = NO;
        horizontalAlignment = NSTextAlignmentLeft;
        _format = GridColumnFormatNone;
        visible = YES;
    }
    return self;
}

// Called when unserialized
- (id)initWithCoder: (NSCoder *)coder {
    if (self = [self init]) {
        self.type = (ColumnType)[coder decodeObjectForKey: @"type"];
        self.name = [coder decodeObjectForKey: @"name"];
        self.title = [coder decodeObjectForKey: @"title"];
        self.width = [coder decodeFloatForKey: @"width"];
        self.sortable = [coder decodeBoolForKey: @"sortable"];
        self.editable = [coder decodeBoolForKey: @"editable"];
        self.format = (GridColumnFormat)[coder decodeObjectForKey: @"format"];
        self.formatString = [coder decodeObjectForKey: @"formatString"];
        self.horizontalAlignment = (NSTextAlignment)[coder decodeObjectForKey: @"horizontalAlignment"];
        self.hideSeparator = [coder decodeBoolForKey: @"hideSeparator"];
        self.visible = [coder decodeBoolForKey: @"visible"];
    }
    return self;
}

// Called when serialized
- (void)encodeWithCoder: (NSCoder *)coder {
    [coder encodeInt: _type forKey:@"type"];
    [coder encodeObject: _name forKey:@"name"];
    [coder encodeObject: _title forKey:@"title"];
    [coder encodeFloat: _width forKey:@"width"];
    [coder encodeBool: _sortable forKey:@"sortable"];
    [coder encodeBool: _editable forKey:@"editable"];
    [coder encodeInt: _format forKey:@"format"];
    [coder encodeObject: _formatString forKey:@"formatString"];
    [coder encodeInt: horizontalAlignment forKey:@"horizontalAlignment"];
    [coder encodeBool: hideSeparator forKey:@"hideSeparator"];
    [coder encodeBool: visible forKey:@"visible"];
}

-(void)assignProperties:(NSDictionary*)dict{
    NSString* title = dict[@"title"];
    if (title){
        self.title = title;
    }
    NSNumber* widthNumber = dict[@"width"];
    if ([widthNumber isKindOfClass:[NSNumber class]]){
        _width = widthNumber.floatValue;
    }
    NSNumber* horizontalAlignmentNumber = dict[@"horizontalAlignment"];
    if ([horizontalAlignmentNumber isKindOfClass:[NSNumber class]]){
        horizontalAlignment = (NSTextAlignment)horizontalAlignmentNumber.integerValue;
    }
    NSNumber* visibleNumber = dict[@"visible"];
    if ([visibleNumber isKindOfClass:[NSNumber class]]){
        visible = visibleNumber.boolValue;
    }
}

-(NSDictionary*)getProperties:(GridTableColumnDescription*)defaultColumnDescription{
    NSMutableDictionary* dict = [[[NSMutableDictionary alloc]init]autorelease];
    dict[@"name"] = self.name;
    dict[@"visible"] = @(self.visible);
    
    if (![self.title isEqualToString:defaultColumnDescription.title]){
        dict[@"title"] = self.title;
    }
    if (_width!=defaultColumnDescription.width){
        dict[@"width"] = @(_width);
    }
    if (horizontalAlignment!=defaultColumnDescription.horizontalAlignment){
        dict[@"horizontalAlignment"] = @(horizontalAlignment);
    }
    return dict;
}

- (void)dealloc {
    [_id release];
    [_name release];
    [_title release];
    [_titleOriginal release];
    [_formatString release];
    [decimalTextHandler release];
    [dateFormater release];
    
    [super dealloc];
}

-(void)setTitle:(NSString *)value{
    [value retain];
    [_title release];
    _title = value;
    if (!self.titleOriginal){
        self.titleOriginal = _title;
    }
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment];
    return [columnDescription autorelease];
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format {
    
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format];
    
    return [columnDescription autorelease];
}


+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format clearZero:(BOOL)clearZero{
    
    GridTableColumnDescription * columnDescription = [GridTableColumnDescription textColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format];
    
    columnDescription.clearZero = clearZero;
    
    return columnDescription;
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString *)formatString{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:formatString];
    return [columnDescription autorelease];
}

+ (id)dateColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
              alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc]initDateColumnWithName:name title:title width:width sortable:sortable alignment:alignment dateFormatStyle:dateFormatStyle];
    columnDescription.dateFormatterStyle = dateFormatStyle;
    return [columnDescription autorelease];
}

+ (id)dateTimeColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                  alignment :(NSTextAlignment)alignment dateFormatStyle:(NSDateFormatterStyle)dateFormatStyle timeFormatStyle:(NSDateFormatterStyle)timeFormatStyle{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc]initDateTimeColumnWithName:name title:title width:width sortable:sortable alignment:alignment dateFormatStyle:dateFormatStyle timeFormatStyle:timeFormatStyle];
    columnDescription.dateFormatterStyle = dateFormatStyle;
    columnDescription.timeFormatterStyle = timeFormatStyle;
    return [columnDescription autorelease];
}

+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initBoolColumnWithName:name title:title width:width sortable:sortable editable:editable];
    return [columnDescription autorelease];
}


+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable alignment :(NSTextAlignment)alignment{
    
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initBoolColumnWithName:name title:title width:width sortable:sortable editable:editable hideSeparator:NO alignment:NSTextAlignmentCenter];
    return [columnDescription autorelease];
}

+ (id)boolColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable editable: (BOOL)editable hideSeparator:(BOOL)hidesep{
    
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initBoolColumnWithName:name title:title width:width sortable:sortable editable:editable hideSeparator:hidesep alignment:NSTextAlignmentCenter];
    
    return [columnDescription autorelease];
}


+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment hideSeparator:(BOOL)hidesep {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment hideSeparator:hidesep];
    return [columnDescription autorelease];
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format hideSeparator:(BOOL)hidesep {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format hideSeparator:hidesep];
    return [columnDescription autorelease];
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString *)formatString hideSeparator:(BOOL)hidesep{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:formatString hideSeparator:hidesep];
    return [columnDescription autorelease];
}

+ (id)textColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable
                editable: (BOOL)editable alignment :(NSTextAlignment)alignment format:(GridColumnFormat)format formatString:(NSString*)formatString hideSeparator:(BOOL)hidesep multiline:(BOOL)multiline {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initTextColumnWithName:name title:title width:width sortable:sortable editable:editable alignment:alignment format:format formatString:formatString hideSeparator:hidesep multilene:multiline];
    return [columnDescription autorelease];
}

+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initButtonColumnWithName:name title:title width:width];
    return [columnDescription autorelease];
}

+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width alignment:(NSTextAlignment)alignment{
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initButtonColumnWithName:name title:title width:width hideSeparator:FALSE alignment:alignment];
    return [columnDescription autorelease];
}

+ (id)buttonColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width hideSeparator:(BOOL)hidesep {
    GridTableColumnDescription * columnDescription = [[GridTableColumnDescription alloc] initButtonColumnWithName:name title:title width:width hideSeparator:hidesep alignment:NSTextAlignmentCenter];
    return [columnDescription autorelease];
}

+(id)imageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight {
    return [[[GridTableColumnDescription alloc] initImageTextColumnWithName:name title:title width:width imageWidth:imgWidth imageHeight:imgHeight] autorelease];
}

+ (id)customColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable cellClass:(Class)cellClass{
    return [[[GridTableColumnDescription alloc] initCustomColumnWithName:name title:title width:width sortable:sortable cellClass:cellClass] autorelease];
}

+ (id)customColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width sortable:(BOOL)sortable alignment:(NSTextAlignment)alignment cellClass:(Class)cellClass{
    return [[[GridTableColumnDescription alloc] initCustomColumnWithName:name title:title width:width sortable:sortable alignment:alignment cellClass:cellClass] autorelease];
}


+ (id)listColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width {
    return [[[GridTableColumnDescription alloc] initListColumnWithName:name title:title width:width] autorelease];
}

+(id)imageTextColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight isImageRight:(BOOL)isImageRight {
    return [[[GridTableColumnDescription alloc] initImageTextColumnWithName:name title:title width:width imageWidth:imgWidth imageHeight:imgHeight isImageRight:isImageRight] autorelease];
}
+(id)imageColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight {
    return [[[GridTableColumnDescription alloc] initImageColumnWithName:name title:title width:width imageWidth:imgWidth imageHeight:imgHeight] autorelease];
}

+(id)imageColumnWithName:(NSString *)name title:(NSString *)title width:(CGFloat)width imageWidth:(CGFloat)imgWidth imageHeight:(CGFloat)imgHeight sortable:(BOOL)sortable {
    return [[[GridTableColumnDescription alloc] initImageColumnWithName:name title:title width:width imageWidth:imgWidth imageHeight:imgHeight sortable:sortable] autorelease];
}

@end
