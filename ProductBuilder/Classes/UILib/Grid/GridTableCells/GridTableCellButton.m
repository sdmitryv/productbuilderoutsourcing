//
//  GridTableCellButton.m
//  ProductBuilder
//
//  Created by valera on 9/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellButton.h"
#import "MOGlassButton.h"
#import "GridTable.h"
#import "Global.h"

@implementation GridTableCellButton

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        button = [[[self class] createButton] retain];
        button.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        button.enabled = YES;
        button.contentHorizontalAlignment = [GridTable convertTextAlignmentToContentHorizontalAlignment:columnDescription.horizontalAlignment];
        //button.contentEdgeInsets = UIEdgeInsetsMake(5, 3, 5, 12);
        //button.frame = CGRectMake(3, 5, frame.size.width - 6, frame.size.height - 10);
        button.frame = CGRectMake(8, 3, frame.size.width - 16, frame.size.height - 6);
        button.titleLabel.font = [UIFont systemFontOfSize:15];//self.gridTable.cellFont;
        [button setTitle:NSLocalizedString(@"SALES_RECEPT_PROCESS_BUTTON_TITLE", nil) forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonTouchUp:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addSubview:button];
    }
    return self;
}

+(UIButton*)createButton{
    return [MOGlassButtonBlack buttonWithType:UIButtonTypeCustom];
}

-(void)dealloc{
    [button release];
    [super dealloc];
}

-(void)applyDefaultProperties:(CGRect)dataFrame{
    
}

- (void)buttonTouchDown:(id)sender{
    UIButton *btn = (UIButton *)sender;
	CGPoint point = btn.center;
	point = [btn convertPoint:point toView:self.gridTable.tableView];
	NSIndexPath* indexPath = [self.gridTable.tableView indexPathForRowAtPoint:point];
    [self.gridTable selectRow:indexPath.row animated:TRUE];
}

- (void)buttonTouchUp:(id)sender {
	UIButton *btn = (UIButton *)sender;
	CGPoint point = btn.center;
	point = [btn convertPoint:point toView:self.gridTable.tableView];
	NSIndexPath* indexPath = [self.gridTable.tableView indexPathForRowAtPoint:point];
    if (self.gridTable.delegate != nil && [self.gridTable.delegate respondsToSelector:@selector(gridTable:cellButtonClickedForRow:column:button:)]) {
		[self.gridTable.delegate gridTable:self.gridTable cellButtonClickedForRow:[indexPath row] column:self.column button:btn];
    }
}

@end

@interface MOButtonLightGray : MOGlassButton

@property(nonatomic, assign)GridTableCellButton* cell;
@end

@implementation MOButtonLightGray

-(void)dealloc{
    self.cell = nil;

    [super dealloc];
}

- (void)initializeGradientLayerProperties {
	gradientLayer1.colors = @[(id)[UIColor colorWithWhite:0.85f alpha:0.15f].CGColor,
                             (id)[UIColor colorWithWhite:0.87f alpha:0.25f].CGColor,
                             (id)[UIColor colorWithWhite:0.88f alpha:0.25f].CGColor,
                             (id)[UIColor colorWithWhite:0.9f alpha:0.25f].CGColor];
    gradientLayer1.locations = @[@0.0f,
                                @0.15f,
                                @0.75f,
                                @1.0f];
}


- (void)setupLayers{
	[super setupLayers];
  	[self setBackgroundColor:MO_RGBCOLOR(205, 203, 203) forState:UIControlStateNormal];
	[self setBackgroundColor:MO_RGBCOLOR(191, 190, 190) forState:UIControlStateHighlighted];
    [self setBackgroundColor:MO_RGBCOLOR(102, 102, 102) forState:UIControlStateSelected];
	[self setBackgroundColor:MO_RGBCOLOR(228, 227, 227) forState:UIControlStateDisabled];
	
	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:MO_RGBCOLOR(150, 150, 150) forState:UIControlStateDisabled];
	self.titleLabel.shadowOffset = CGSizeMake(0, 0);
	//self.titleLabel.shadowColor = MO_RGBCOLOR(192, 73, 84);
	
}

/*-(void)handleState{

    [super handleState];
    self.layer.borderWidth = 2.0f;
    
    self.layer.borderColor = MO_RGBCOLOR(227, 227, 227).CGColor;
}

-(void)setHighlighted:(BOOL)highlighted{

    if (self.cell.gridTableRow.changingSelection) return;
    [super setHighlighted:highlighted];
}*/

@end

@implementation GridTableCellGrayButton

+(UIButton*)createButton{
    
    //MOGlassButtonLightGray
    return [MOButtonLightGray buttonWithType:UIButtonTypeCustom];
}

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription{
    if ((self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription])){
        ((MOButtonLightGray*)button).cell = self;
        button.frame = CGRectMake(5, (int)frame.size.height*0.25, frame.size.width - 10, frame.size.height - ((int)frame.size.height*0.25)*2);
        button.contentEdgeInsets = UIEdgeInsetsMake(5, 8, 5, 12);
        button.titleLabel.font = self.gridTable.cellFont;
        
    }
    return self;
}
@end
