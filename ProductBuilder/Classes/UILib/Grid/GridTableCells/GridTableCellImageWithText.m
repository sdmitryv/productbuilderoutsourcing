//
//  GridTableCellImageWithText.m
//  ProductBuilder
//
//  Created by valera on 9/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellImageWithText.h"
#import "GridTable.h"

@implementation GridTableCellImageWithText

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription {
    
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    
    if (self) {
        
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(dataFrame.origin.x, dataFrame.origin.y + (dataFrame.size.height - column.imageHeight)/2,
                                                                column.imageWidth, column.imageHeight)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:imgView];
        
        fontLabel = [[UILabel alloc]initWithFrame:CGRectMake(dataFrame.origin.x + imgView.bounds.size.width + 3, dataFrame.origin.y, dataFrame.size.width - imgView.bounds.size.width, dataFrame.size.height)];
        //make label first view
        [self insertSubview:fontLabel atIndex:0];
        [self applyDefaultProperties:dataFrame];
    }
    return self;
}


-(void)dealloc{
    
    [fontLabel release];
    [imgView release];
    [super dealloc];
}


-(void)applyDefaultProperties:(CGRect)dataFrame{
    
    fontLabel.font = self.gridTable.cellFont;
    fontLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    fontLabel.textAlignment = column.horizontalAlignment;
    fontLabel.textColor = self.gridTable.rowTextColor;
    fontLabel.highlightedTextColor = self.gridTable.rowTextColorSelected;
    fontLabel.text = @"";
}


-(void)fillWithDataForRow:(NSInteger)row dataFrame:(CGRect)dataFrame{
    
    if (self.gridTable.delegate)  {
        
        UIImage* img = [self.gridTable.delegate gridTable:self.gridTable getImageForRow:row column:column];
        imgView.image = img;
        imgView.hidden = img == nil;
    }
    
    [super fillWithDataForRow:row dataFrame:dataFrame];
    
    [self updateLayout:dataFrame];
    
}


-(void)updateLayout:(CGRect)dataFrame{
        if (!imgView.hidden) {
            
            if (column.isImageRight){
                CGSize labelSize = CGSizeMake(dataFrame.size.width, dataFrame.size.height);
                labelSize = [[[[NSAttributedString alloc]initWithString:fontLabel.text attributes:@{NSFontAttributeName:fontLabel.font}] autorelease] boundingRectWithSize:labelSize options:NSStringDrawingUsesLineFragmentOrigin | fontLabel.lineBreakMode context:nil].size;
                
                if (labelSize.width + 5 + imgView.bounds.size.width > dataFrame.size.width)
                    labelSize = CGSizeMake(dataFrame.size.width - 5 - imgView.bounds.size.width, labelSize.height);
                
                fontLabel.frame = CGRectMake(dataFrame.origin.x, dataFrame.origin.y, labelSize.width, dataFrame.size.height);
                imgView.frame = CGRectMake(fontLabel.frame.origin.x + fontLabel.frame.size.width + 5,
                                           dataFrame.origin.y + (dataFrame.size.height - column.imageHeight)/2,
                                           imgView.bounds.size.width,
                                           imgView.bounds.size.height);
            }
            else{
                imgView.frame = CGRectMake(dataFrame.origin.x, dataFrame.origin.y + (dataFrame.size.height - column.imageHeight)/2,
                                                                        column.imageWidth, column.imageHeight);
                fontLabel.frame = CGRectMake(dataFrame.origin.x + imgView.bounds.size.width + 3, dataFrame.origin.y, dataFrame.size.width - imgView.bounds.size.width, dataFrame.size.height);
            }
        }
        else{
            fontLabel.frame = CGRectMake(dataFrame.origin.x, dataFrame.origin.y, dataFrame.size.width, dataFrame.size.height);
        }
}

@end
