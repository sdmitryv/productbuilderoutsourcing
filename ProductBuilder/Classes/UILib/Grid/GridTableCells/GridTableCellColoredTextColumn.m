//
//  GridTableCellColoredTextColumn.m
//  ProductBuilder
//
//  Created by valera on 9/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCellColoredTextColumn.h"
#import "GridTable.h"
#import "UIHighlightableAttributedLabel.h"

@implementation GridTableCellColoredTextColumn

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription
{
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    if (self) {
        fontLabel = [[UIHighlightableAttributedLabel alloc]initWithFrame:dataFrame];
        fontLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:fontLabel];
    }
    return self;
}

-(void)dealloc{
    [fontLabel release];
    [super dealloc];
}

-(void)applyDefaultProperties:(CGRect)dataFrame{
    fontLabel.frame = dataFrame;
    fontLabel.opaque = YES;
    fontLabel.hidden = NO;
    fontLabel.font = self.gridTable.cellFont;
    fontLabel.textAlignment = self.column.horizontalAlignment;
    fontLabel.highlightedTextColor  = self.gridTable.rowTextColorSelected;
    fontLabel.textColor = self.gridTable.rowTextColor;
    fontLabel.text = @"";
    if (self.column.multiline) {
        fontLabel.numberOfLines = 0;
    }
}

@end
