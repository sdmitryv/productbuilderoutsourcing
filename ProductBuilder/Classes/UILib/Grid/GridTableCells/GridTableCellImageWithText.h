//
//  GridTableCellImageWithText.h
//  ProductBuilder
//
//  Created by valera on 9/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCell.h"

@interface GridTableCellImageWithText : GridTableCell{
    UILabel* fontLabel;
    UIImageView* imgView;
}

@end
