//
//  GridTableCellImage.h
//  ProductBuilder
//
//  Created by Julia Korevo on 4/22/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "GridTableCell.h"

@interface GridTableCellImage : GridTableCell{
    UIImageView* imgView;
}

@end
