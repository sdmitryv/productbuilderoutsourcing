//
//  GridTableCellList.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/17/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "GridTableCell.h"

@interface GridTableCellList : GridTableCell <UITableViewDataSource, UITableViewDelegate> {
    UITableView * listTableView;
    NSArray * dataArray;
}

@property (nonatomic, retain) NSArray * dataArray;

@end


@interface ListCell : UITableViewCell {
    UILabel *_titleLabel;
}
@property (nonatomic, readonly) UILabel *titleLabel;
@end

