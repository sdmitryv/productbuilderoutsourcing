//
//  GridTableCellImage.m
//  ProductBuilder
//
//  Created by Julia Korevo on 4/22/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "GridTableCellImage.h"
#import "GridTable.h"

@implementation GridTableCellImage

-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect)dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription {
    
    self = [super initWithFrame:frame dataFrame:dataFrame row:row column:columnDescription];
    
    if (self) {
        
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(dataFrame.origin.x, dataFrame.origin.y + (dataFrame.size.height - column.imageHeight)/2,
                                                            column.imageWidth, column.imageHeight)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:imgView];
    }
    return self;
}


-(void)dealloc{
    
    [imgView release];
    [super dealloc];
}

-(void)fillWithDataForRow:(NSInteger)row dataFrame:(CGRect)dataFrame{
    
    if (self.gridTable.delegate)  {
        
        UIImage* img = [self.gridTable.delegate gridTable:self.gridTable getImageForRow:row column:column];
        imgView.image = img;
        imgView.hidden = img == nil;
    }
    
    [super fillWithDataForRow:row dataFrame:dataFrame];
    [self updateLayout:dataFrame];
    
}

-(void)updateLayout:(CGRect)dataFrame{
    if (!imgView.hidden) {
        
        imgView.frame = CGRectMake(self.frame.size.width/2 -column.imageWidth/2,  dataFrame.origin.y + (dataFrame.size.height - column.imageHeight)/2,
                                       imgView.bounds.size.width,
                                       imgView.bounds.size.height);
    }
}
@end
