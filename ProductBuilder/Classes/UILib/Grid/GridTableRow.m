//
//  GridTableCell.m
//  CloudStockCount
//
//  Created by Lulakov Viacheslav on 6/6/11.
//  Copyright 2011 CloudWorks. All rights reserved.
//

#import "GridTableRow.h"
#import "UIRoundedCornersView.h"
#import "GridTable.h"
#import "GridTableColumnHeader.h"

@class  GridTableRow;
CGFloat const dividerColumnWidth = 0.25;
CGFloat const dividerRowWidth = 0.25;

@interface UIViewCellSelection :UIView{
    GridTableRow* row;
}

-(id)initWithFrame:(CGRect)frame row:(GridTableRow*)row;
@property (nonatomic, retain)UIColor* lineColor;
@property(nonatomic,assign)CGRect lastDrawnFrame;
@end

@implementation UIViewCellSelection

-(id)initWithFrame:(CGRect)frame row:(GridTableRow*)aRow{
    if ((self = [super initWithFrame:frame])){
        row = aRow;
        self.lineColor = [UIColor whiteColor];
    }
    return self;
}

-(void)dealloc{
    [_lineColor release];
    [super dealloc];
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    NSArray* columns = [row.gridTable getVisibleColumns];
    if (!columns.count) return;
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    
    NSInteger radius = 5;
    CGContextSetLineWidth(context, dividerRowWidth);
    //draw separator
    CGContextMoveToPoint(context, radius, rect.size.height);
    CGContextAddLineToPoint(context, rect.size.width-radius, rect.size.height);
    CGContextMoveToPoint(context, radius, 0);
    CGContextAddLineToPoint(context, rect.size.width-radius, 0);
    
    //NSInteger highlightingBorderColumnIndex = row.gridTable.highlightingBorderColumnIndex;
    if (dividerRowWidth!=dividerColumnWidth){
        CGContextStrokePath(context);
    }
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, dividerColumnWidth);
    double x = 0.0;
    
    CGContextMoveToPoint(context, radius, 0);
    CGContextAddArcToPoint(context, 0, 0, 0, radius, radius);
    CGContextAddLineToPoint(context, 0, rect.size.height-radius);
    CGContextAddArcToPoint(context, 0, rect.size.height, radius, rect.size.height, radius);
    
    for (NSInteger i = 0; i < columns.count - 1; i++) {
        GridTableColumnDescription* columnDescription = row.columns[i];
        GridTableColumnHeader* columnHeader = [row.gridTable getColumnHeaderByColumnDescription:columnDescription];
        if (columnHeader){
            x += columnHeader.frame.size.width + row.gridTable.spaceBetweenColumns*0.5;
            // Add the vertical lines
            if (!columnDescription.hideSeparator) {
                CGContextMoveToPoint(context, x, 0);
                CGContextAddLineToPoint(context, x, rect.size.height);
            }
            x+=row.gridTable.spaceBetweenColumns*0.5;
        }
    }
    
    CGContextMoveToPoint(context, rect.size.width-radius, 0);
    CGContextAddArcToPoint(context, rect.size.width, 0, rect.size.width, radius, radius);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height-radius);
    CGContextAddArcToPoint(context, rect.size.width, rect.size.height, rect.size.width - radius, rect.size.height, radius);
    
    // Draw the lines
	CGContextStrokePath(context);
    _lastDrawnFrame = rect;
}

@end

@interface GridTableRow(Private)
-(void)layoutCellForColumn:(GridTableColumnDescription*)columnDescription;
@end

@implementation GridTableRow

@synthesize delegate, gridTable=_gridTable, changingSelection;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier gridTable:(GridTable *)gridTable
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _gridTable = gridTable;
        UIView *selectedBackgroundView = [[UIViewCellSelection alloc] initWithFrame:self.bounds row:self];
        selectedBackgroundView.backgroundColor = gridTable.selectedRowBackgroundColor;
        self.selectedBackgroundView = selectedBackgroundView;
        [selectedBackgroundView release];
        
        UIViewCellSelection *backgroundView = [[UIViewCellSelection alloc] initWithFrame:self.bounds row:self];
        backgroundView.backgroundColor = [UIColor clearColor];
        backgroundView.lineColor = [UIColor blackColor];
        self.backgroundView = backgroundView;
        [backgroundView release];
        
        self.autoresizesSubviews = TRUE;
        self.contentView.clipsToBounds = TRUE;
        self.contentView.autoresizesSubviews = TRUE;
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gridTableColumnHeaderFrameChanged:) name:gridTableColumnHeaderFrameChangedNotification object:gridTable];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    BOOL oldSelected = self.selected;
    changingSelection = TRUE;
    [super setSelected:selected animated:animated];
    if (oldSelected!=selected && delegate){
        [delegate gridTableRow:self didChangeSelectedState:selected];
    }
    changingSelection = FALSE;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [super dealloc];
}

- (void)setFrame:(CGRect)newRect{
    if (_gridTable.spaceBetweenRows){
        newRect = CGRectMake(newRect.origin.x, newRect.origin.y, newRect.size.width, newRect.size.height - _gridTable.spaceBetweenRows);
    }
    [super setFrame:newRect];
}

-(void)didDequeue{
}

-(NSArray*)columns{
    return [_gridTable getVisibleColumns];
}

-(void)gridTableColumnHeaderFrameChanged:(NSNotification*)notification{
    //GridTableColumnDescription* columnDescription = (GridTableColumnDescription*)[[notification userInfo]objectForKey:gridTableColumnDescriptionKey];
    //if (!columnDescription) return;
    NSArray* columns = [_gridTable getVisibleColumns];
    for (GridTableColumnDescription* c in columns){
        [self layoutCellForColumn:c];
    }
    [self setNeedsDisplay];
}

-(void)layoutCellForColumn:(GridTableColumnDescription*)columnDescription{
    GridTableCell* cell = [self getCellViewByColumn:columnDescription];
    CGRect rect = CGRectNull, cellRect = CGRectNull;
    //adjust position and width only. the height remains the same
    [_gridTable getRectsForColumn:columnDescription cellRect:&cellRect contentRect:&rect rowHeight:cell.superview.frame.size.height];
    cell.frame = CGRectMake(cellRect.origin.x, cellRect.origin.y, cellRect.size.width, cell.frame.size.height);
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self.backgroundView setNeedsDisplay];
    [self.selectedBackgroundView setNeedsDisplay];
    [self drawRoundedCornersWithRadius:5.0];
}

-(CGRect)lastDrawnFrame{
    return ((UIViewCellSelection*)self.backgroundView).lastDrawnFrame;
}

- (GridTableCell *)getCellViewByColumn:(GridTableColumnDescription *) column {
    for (UIView *view in [self contentView].subviews) {
        if (![view isKindOfClass:[GridTableCell class]]) continue;
        GridTableCell *cell = (GridTableCell*)view;
        if ([cell.column isEqual:column]) {
            return cell;
        }
    }
    return nil;
}

- (GridTableCell *)getCellViewByColumnName:(NSString *) columnName {
    for (UIView *view in [self contentView].subviews) {
        if (![view isKindOfClass:[GridTableCell class]]) continue;
        GridTableCell *cell = (GridTableCell*)view;
        if ([cell.column.name isEqualToString:columnName]) {
            return cell;
        }
    }
    return nil;
}

- (UIView *)getCellSubViewByColumn:(GridTableColumnDescription *) column {
    UIView *cell = [self getCellViewByColumn:column];
    if (cell && cell.subviews.count) {
        return (cell.subviews)[0];
    }
    return nil;
}

-(NSArray*)getCellSubViewsByColumn:(GridTableColumnDescription*) column {
    UIView *cell = [self getCellViewByColumn:column];
    return cell.subviews;
}

-(NSIndexPath*)indexPath{
    return [self.gridTable.tableView indexPathForCell:self];
}

@end
