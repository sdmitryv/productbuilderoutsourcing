//
//  GridTable+GridTable_Serialization.m
//  GridTable.Serialized
//
//  Created by valery on 6/4/15.
//  Copyright (c) 2015 valera. All rights reserved.
//

#import "GridTable+Serialization.h"

@implementation GridTable (Serialization)


- (NSArray*)serializeColumns{
    NSArray* visibleColumns = [self columns];
    NSMutableArray* visibleColumnsProps = [[NSMutableArray alloc]init];
    for (GridTableColumnDescription* columnDescription in visibleColumns){
        GridTableColumnDescription* defaultColumnDescription = nil;
        for (GridTableColumnDescription* cd in _defaultColumns){
            if ([cd.name isEqualToString:columnDescription.name]){
                defaultColumnDescription = cd;
                break;
            }
        }
        [visibleColumnsProps addObject:[columnDescription getProperties:defaultColumnDescription]];
    }
    return [visibleColumnsProps autorelease];
}

- (void)deserializeColumns:(NSArray*)columns {
    if (!columns) {
        return;
    }
    [_columns release];
    _columns = [[NSMutableArray alloc]init];
    NSMutableSet* columnsNeedsToBeAdded = [[NSMutableSet alloc]initWithArray:_defaultColumns];
    for (NSDictionary* dict in columns){
        if ([dict isKindOfClass:[NSDictionary class]]){
            NSString* columnName = dict[@"name"];
            //check this column does not exist in _columns
            BOOL columnIsAreadyAdded = FALSE;
            for (GridTableColumnDescription* cd in _columns){
                if ([cd.name isEqualToString:columnName]){
                    columnIsAreadyAdded = YES;
                    break;
                }
            }
            if (columnIsAreadyAdded){
                continue;
            }
            for (GridTableColumnDescription* cd in _defaultColumns){
                if ([cd.name isEqualToString:columnName]){
                    GridTableColumnDescription* columnDescription = [cd copy];
                    [columnDescription assignProperties:dict];
                    [_columns addObject:columnDescription];
                    [columnsNeedsToBeAdded removeObject:cd];
                    [columnDescription release];
                    break;
                }
            }
        }
    }
    if (!_columns.count && _defaultColumns.count){
        [self resetToDefaults];
    }
    else{
        //add columns don't exists in serialized set
        //these columns were added (hardcoded) to default columns after serialization occured
        for (GridTableColumnDescription* cd in columnsNeedsToBeAdded){
            GridTableColumnDescription* columnDescription = [cd copy];
            columnDescription.visible = NO;
            NSInteger candidateIndex = [_defaultColumns indexOfObject:cd];
            if (candidateIndex > _columns.count - 1){
                candidateIndex = _columns.count - 1;
            }
            [_columns insertObject:columnDescription atIndex:candidateIndex];
            [columnDescription release];
        }
    }
    [columnsNeedsToBeAdded release];
    [self rebuildGrid];
}

-(NSData*)serialize:(NSError*)error{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]init];
    dict[@"pagingEnabled"] = @(self.scrollView.pagingEnabled);
    dict[@"horizontalScrollEnabled"] = @(self.horizontalScrollEnabled);
    dict[@"columns"] = [self serializeColumns];
    NSData* data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    [dict release];
    return data;
}

-(BOOL)deserialize:(NSData*)data error:(NSError*)error{
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (!dict || error){
        return FALSE;
    }
    NSArray* columns = dict[@"columns"];
    if ([columns isKindOfClass:[NSArray class]]){
        [self deserializeColumns:columns];
    }
    NSNumber* horizontalScrollEnabled = dict[@"horizontalScrollEnabled"];
    if ([horizontalScrollEnabled isKindOfClass:[NSNumber class]]){
        self.horizontalScrollEnabled = horizontalScrollEnabled.boolValue;
    }
    NSNumber* pagingEnabled = dict[@"pagingEnabled"];
    if ([pagingEnabled isKindOfClass:[NSNumber class]]){
        self.scrollView.pagingEnabled = pagingEnabled.boolValue;
    }
    return TRUE;
}

@end
