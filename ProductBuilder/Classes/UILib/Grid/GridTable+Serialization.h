//
//  GridTable+GridTable_Serialization.h
//  GridTable.Serialized
//
//  Created by valery on 6/4/15.
//  Copyright (c) 2015 valera. All rights reserved.
//

#import "GridTable.h"

@interface GridTable (Serialization)

-(NSData*)serialize:(NSError*)error;
-(BOOL)deserialize:(NSData*)data error:(NSError*)error;

@end
