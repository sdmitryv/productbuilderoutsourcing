//
//  ColumnHeader.m
//  ProductBuilder
//
//  Created by Roman on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GridTableColumnHeader.h"
#import "GridTable.h"
#import <QuartzCore/QuartzCore.h>
#import "UIArrowView.h"

@interface GridTableColumnHeader()
@end

@implementation GridTableColumnHeader

- (id)init {
	if ((self = [super init])) {
        _highlightedSide = GridTableColumnHeaderHighlightedSideNone;
        _highlightedBorderView = nil;
        _touchableBorderWidth = 20.0;
	}
	return self;
}

- (NSString *)description {
    return [_columnDescription description];
}


- (void)setHighlightedSide:(GridTableColumnHeaderHighlightedSide)value {
    if (_highlightedSide == value) return;
    
    _highlightedSide = value;
    
    if (!_highlightedBorderView && value == GridTableColumnHeaderHighlightedSideNone) return;
    CGFloat const arrowViewDelta = 3.0f;
    CGFloat const highlightBorderWidth = 10.0f + arrowViewDelta;
    CGRect borderFrame = self.frame;
    borderFrame.origin.y = self.bounds.size.height/4;
    borderFrame.size.height = self.bounds.size.height/2;
    borderFrame.size.width = highlightBorderWidth;
    if (_highlightedSide == GridTableColumnHeaderHighlightedSideLeft) {
        borderFrame.origin.x = 0;
    } else if (_highlightedSide == GridTableColumnHeaderHighlightedSideRight) {
            borderFrame.origin.x = self.frame.size.width - highlightBorderWidth;
    } else {
            [[[_highlightedBorderView subviews][0] layer] removeAllAnimations];
            [_highlightedBorderView removeFromSuperview];
            [_highlightedBorderView release];
            _highlightedBorderView = nil;
            return;
    }
    if (!_highlightedBorderView) {
        _highlightedBorderView = [[UIView alloc] initWithFrame:borderFrame];
        _highlightedBorderView.userInteractionEnabled = FALSE;
        CGRect highlightBorderArrowFrame = _highlightedBorderView.bounds;
        highlightBorderArrowFrame.size.width-=arrowViewDelta;
        if (_highlightedSide == GridTableColumnHeaderHighlightedSideLeft){
            highlightBorderArrowFrame.origin.x += arrowViewDelta;
        }
        UIArrowView* highlightBorderArrowView = [[UIArrowView alloc] initWithFrame:highlightBorderArrowFrame];
        highlightBorderArrowView.layer.shadowOffset = CGSizeMake(3, 3);
        highlightBorderArrowView.layer.shadowOpacity = 0.5f;
        highlightBorderArrowView.autoresizingMask = _highlightedSide == GridTableColumnHeaderHighlightedSideLeft? UIViewAutoresizingFlexibleRightMargin : UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        highlightBorderArrowView.userInteractionEnabled = FALSE;
        highlightBorderArrowView.type = (value== GridTableColumnHeaderHighlightedSideLeft ? UIArrowViewTypeLeft : UIArrowViewTypeRight);
        highlightBorderArrowView.arrowColor = [UIColor blackColor];
        _highlightedBorderView.backgroundColor = highlightBorderArrowView.backgroundColor = [UIColor clearColor];
        [_highlightedBorderView addSubview:highlightBorderArrowView];
        [highlightBorderArrowView release];
        [self addSubview:_highlightedBorderView];
        _highlightedBorderView.autoresizingMask = _highlightedSide == GridTableColumnHeaderHighlightedSideLeft? UIViewAutoresizingFlexibleRightMargin : UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [self sendSubviewToBack:_highlightedBorderView];
    }
    else{
        _highlightedBorderView.frame = borderFrame;
    }
    [self animateHighlightBorder:arrowViewDelta];
}

-(void)animateHighlightBorder:(CGFloat) deltaX{
    if (!_highlightedBorderView || _highlightedSide==GridTableColumnHeaderHighlightedSideNone) return;
    UIView* arrowView = [_highlightedBorderView subviews][0];
    
    [UIView animateWithDuration:0.3f delay:0.0f options: UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionAllowUserInteraction animations:^{
        CGRect frame = arrowView.frame;
        if (_highlightedSide==GridTableColumnHeaderHighlightedSideLeft){
            frame.origin.x+=deltaX;
        }
        else if (_highlightedSide==GridTableColumnHeaderHighlightedSideRight){
            frame.origin.x-=deltaX;
        }
        arrowView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}


-(UIImage *)createDruggingImage
{
    //UIGraphicsBeginImageContext(self.bounds.size);
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    UIColor *origBackroundColor = [self.backgroundColor retain];
    UIColor *origTextColor = [self.titleLabel.textColor retain];
    self.titleLabel.textColor = [UIColor redColor];
    self.backgroundColor = [UIColor colorWithRed:0.35 green:0.35 blue:0.35 alpha:1.0];
    [self.layer renderInContext: currentContext];
    self.backgroundColor = origBackroundColor;
    self.titleLabel.textColor = origTextColor;
    [origBackroundColor release];
    [origTextColor release];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)dealloc {
	[_columnDescription release];
    [_highlightedBorderView release];
    [super dealloc];
}

@end
