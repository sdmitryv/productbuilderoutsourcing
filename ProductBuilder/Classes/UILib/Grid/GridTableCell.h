//
//  MyCell.h
//  ProductBuilder
//
//  Created by Roman on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GridTableColumnDescription;
@class GridTable;
@class GridTableRow;

@interface GridTableCell : UIView{
@protected
    GridTableColumnDescription* column;
}
-(id)initWithFrame:(CGRect)frame dataFrame:(CGRect) dataFrame row:(GridTableRow*)row column:(GridTableColumnDescription*)columnDescription;
@property (nonatomic, readonly, retain) GridTableColumnDescription* column;
@property (nonatomic, readonly, assign) GridTableRow* gridTableRow;
@property (nonatomic, readonly) GridTable* gridTable;

-(UIView *) getSubView;
-(void)applyDefaultProperties:(CGRect)dataFrame;
-(void)fillWithDataForRow:(NSInteger)row dataFrame:(CGRect)dataFrame;
@end
