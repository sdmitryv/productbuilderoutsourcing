//
//  GridColumnsEditorViewController.m
//  ProductBuilder
//
//  Created by Roman on 12/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GridColumnsEditorViewController.h"
#import "MOGlassButton.h"
#import "UIView+FirstResponder.h"
#import "UITextFieldNumeric.h"
#import "UICustomSwitchYesNo.h"

@implementation GridColumnsEditorViewController

-(id)initWithGridTable:(GridTable*)gridTable{
    if ((self=[super init])){
        _gridTable = gridTable;
        _columns = [[NSMutableArray alloc] initWithArray:[_gridTable columns] copyItems:YES];
        GridTableColumnDescription* widthColumn = nil;
        NSMutableArray *displayColumns =  [NSMutableArray arrayWithObjects:
                                           [GridTableColumnDescription boolColumnWithName:@"visible" title:NSLocalizedString(@"GRID_TABLE_SELECT_TITLE", nil)  width:80 sortable:NO editable:YES alignment:NSTextAlignmentCenter],
                                           [GridTableColumnDescription textColumnWithName:@"titleOriginal" title:NSLocalizedString(@"GRID_COLUMNS_EDITOR_ORIGINAL_TITLE_TITLE", nil) width:150 sortable:NO editable:NO alignment:NSTextAlignmentLeft],
                                           [GridTableColumnDescription textColumnWithName:@"title" title:NSLocalizedString(@"GRID_COLUMNS_EDITOR_TITLE_TITLE", nil) width:167 sortable:NO editable:YES alignment:NSTextAlignmentLeft format:GridColumnFormatNone],
                                           widthColumn = [GridTableColumnDescription textColumnWithName:@"width" title:NSLocalizedString(@"GRID_COLUMNS_EDITOR_WIDTH_TITLE", nil) width:103 sortable:NO editable:YES alignment:NSTextAlignmentLeft format:GridColumnFormatDecimal],
                                           nil];
        self.items = _columns;
        self.columns = displayColumns;
        self.titleText = NSLocalizedString(@"GRID_TABLE_TABLECOLUMNS_TITLE", nil);
        CGFloat contentHeight = 80 + (_gridTable.headerView.hidden ? 0 : _gridTable.headerView.bounds.size.height) + (_gridTable.rowHeight + 2)*_gridTable.defaultColumns.count;
        CGFloat maxHeight = [[UIScreen mainScreen] bounds].size.height;
        if (contentHeight > maxHeight) contentHeight = maxHeight;
        self.preferredContentSize = CGSizeMake(500.0, contentHeight);
        _gridTable.scrollView.scrollEnabled = NO;
        ignoreRowSelection = TRUE;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect frame = _titleView.frame;
    frame.size.height = 80;
    _titleView.frame = frame;
    
    frame = _tableView.frame;
    frame.origin.y = _titleView.frame.origin.y + _titleView.frame.size.height;
    frame.size.height = self.view .bounds.size.height - frame.origin.y;
    _tableView.frame = frame;
    
    
    CGSize applyButtonSize = CGSizeMake(80, 50);
    _applyButton = [[MOGlassButtonTransparent alloc]initWithFrame:CGRectMake(0, 0, applyButtonSize.width, applyButtonSize.height)];
    //_applyButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    [_applyButton addTarget:self action: @selector(apply:) forControlEvents: UIControlEventTouchUpInside];
    [_applyButton setTitle:NSLocalizedString(@"APPLYBTN_TITLE", nil) forState:UIControlStateNormal];
    _applyButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    
    _titleLabel.frame = CGRectMake(applyButtonSize.width + _applyButton.frame.origin.x, 0, _titleView.bounds.size.width - _bCancel.bounds.size.width - applyButtonSize.width, 50);
    _titleLabel.text = NSLocalizedString(@"GRID_TABLE_VIEW_TITLE", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_titleView addSubview:_applyButton];
    
    _useHorizontalScrollingSwitch = [[UICustomSwitchYesNo alloc]initWithFrame:CGRectMake(10, _applyButton.frame.origin.y + _applyButton.frame.size.height, 50, 30)];
    _useHorizontalScrollingSwitch.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    _useHorizontalScrollingSwitch.value = _gridTable.horizontalScrollEnabled;
    
    _useHorizontalScrollingTitle = [[UILabel alloc]initWithFrame:CGRectMake(_useHorizontalScrollingSwitch.frame.origin.x + _useHorizontalScrollingSwitch.frame.size.width + 10, 50, 200, 30)];
    _useHorizontalScrollingTitle.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    _useHorizontalScrollingTitle.text = NSLocalizedString(@"GRID_CUSTOMIZATION_HORIZONTAL_SCROLLING", nil);
    _useHorizontalScrollingTitle.font = [UIFont systemFontOfSize:14];
    _useHorizontalScrollingTitle.textColor = [UIColor darkGrayColor];
    [_titleView addSubview:_useHorizontalScrollingSwitch];
    [_titleView addSubview:_useHorizontalScrollingTitle];
    
    [_bCancel removeTarget:self action:@selector(cancel:) forControlEvents: UIControlEventTouchUpInside];
    [_bCancel addTarget:self action: @selector(discardChanges:) forControlEvents: UIControlEventTouchUpInside];
    
    [_tableView setAllowMoveRows:YES];
    [_tableView setYesImage:[UIImage imageNamed:@"Green_Check.png"]];
    _tableView.headerView.hidden = NO;
    [_tableView setNoImage:nil];
}

- (void)apply:(id)sender {
    [[self.view getFirstResponder] resignFirstResponder];
    [_gridTable setColumns:_columns];
    _gridTable.defaultHorizontalScrollEnabled = _gridTable.defaultHorizontalScrollEnabled;
    _gridTable.horizontalScrollEnabled = _useHorizontalScrollingSwitch.value;
    [_gridTable rebuildGrid];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)discardChanges:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)gridTable:(GridTable*)gridTable reorderRowAtIndexPath:(NSIndexPath *)fromIndex toIndexPath:(NSIndexPath *)toIndex {
    if (_gridTable.sortingMode != NotSorted && _gridTable) {
        if (_gridTable.sortingColumnIndex == fromIndex.row) _gridTable.sortingColumnIndex = toIndex.row;
        else
            if (_gridTable.sortingColumnIndex == toIndex.row) _gridTable.sortingColumnIndex = fromIndex.row;
    }
    
    id temp = [[self items][fromIndex.row] retain];
    [self.items removeObject:temp];
    [self.items insertObject:temp atIndex:toIndex.row];
    [temp release];
    [_tableView reloadData];
}

- (BOOL)gridTable:(GridTable*)gridTable formatCellForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content selected:(BOOL)selected initial:(BOOL)initital {
    
    if ([content isKindOfClass:[UITextField class]] && [column.name isEqualToString:@"width"]) {
        
        CGRect fr = ((UITextField *)content).frame;
        ((UITextField *)content).frame = CGRectMake(fr.origin.x, fr.origin.y, column.width - 45, fr.size.height);
    }
    
    return YES;
}

- (BOOL)gridTable:(GridTable*)gridTable cellContentBeginChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value{
    if ([column.name isEqualToString:@"width"]){
        if ([content isKindOfClass:UITextFieldNumeric.class]){
            UITextFieldNumeric* textFieldNumeric = (UITextFieldNumeric*)content;
            if (textFieldNumeric.minValue.floatValue!=1){
                textFieldNumeric.minValue = @(1);
                textFieldNumeric.maxValue = @(300);
            }
        }
    }
    return TRUE;
}

- (void)gridTable:(GridTable*)gridTable cellContentDidEndChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value{
    if ([column.name isEqualToString:@"width"]){
        GridTableColumnDescription* columnDescription = _columns[row];
        NSNumber* numberValue = nil;
        if ([value isKindOfClass:[NSString class]]){
            NSScanner *theScanner = [[NSScanner alloc] initWithString:value];
            NSDecimal decimal;
            if ([theScanner scanDecimal:&decimal]){
                numberValue = [NSDecimalNumber decimalNumberWithDecimal:decimal];
            }
            [theScanner release];
        }
        else if ([value isKindOfClass:[NSNumber class]]){
            numberValue = (NSNumber*)value;
        }
        if (numberValue){
            columnDescription.width = numberValue.floatValue;
        }
    }
    if ([column.name isEqualToString:@"title"]){
        GridTableColumnDescription* columnDescription = _columns[row];
        if ([value isKindOfClass:[NSString class]]){
            columnDescription.title = value;
        }
    }
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverController {
    return NO;
}

- (void)dealloc {
    [_useHorizontalScrollingTitle release];
    [_useHorizontalScrollingSwitch release];
    [_applyButton release];
    [_columns release];
    [super dealloc];
}

@end
