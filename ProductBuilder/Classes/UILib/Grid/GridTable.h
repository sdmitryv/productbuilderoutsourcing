//
//  GridTable.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 11/10/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GridTableRow.h"
#import "GridTableColumnDescription.h"
#import "UIRoundedCornersView.h"
#import "UITextFieldHandler.h"

extern NSString* const gridTableColumnHeaderFrameChangedNotification;
extern NSString* const gridTableColumnHeaderHighLightBorderNotification;
extern NSString* const gridTableColumnDescriptionKey;

//#import "SimpleTableListViewController.h"
@class GridTableColumnHeader;

typedef NS_ENUM(NSUInteger, SortingMode) {
    NotSorted = 0, AscendingSorted = 1, DescendingSorted = 2
};

struct CompareContext {
    __unsafe_unretained NSString * column;
    SortingMode order;
};

@class GridTable;

@protocol GridTableDelegate<NSObject>

- (void)gridTable:(GridTable*)gridTable setContentForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content;
- (NSInteger)gridTableNumberOfRows:(GridTable*)gridTable;
- (void)gridTable:(GridTable*)gridTable sortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order;

@optional
- (void)gridTable:(GridTable*)gridTable willSortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order;
- (void)gridTable:(GridTable*)gridTable didSortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order;
- (void)gridTable:(GridTable *)gridTable cellContentDidBeginEditingRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content;
- (BOOL)gridTable:(GridTable*)gridTable cellContentBeginChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value;
- (BOOL)gridTable:(GridTable*)gridTable cellContentChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content;
- (BOOL)gridTable:(GridTable*)gridTable cellContentShouldEndChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value;
- (void)gridTable:(GridTable*)gridTable cellContentDidEndChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value;
- (void)gridTable:(GridTable*)gridTable cellContentDidChangeForRow: (NSInteger)row column:(GridTableColumnDescription *)column content:(id)content value:(id)value;
- (BOOL)gridTable:(GridTable*)gridTable cellContentShouldClearForRow: (NSInteger)row column:(GridTableColumnDescription *)column;

- (BOOL)gridTable:(GridTable*)gridTable willDeselectRow:(NSInteger)row;
- (BOOL)gridTable:(GridTable*)gridTable willSelectRow:(NSInteger)row;
- (void)gridTable:(GridTable*)gridTable didSelectRow:(NSInteger)row;
- (void)gridTable:(GridTable*)gridTable didDeselectRow:(NSInteger)row;
- (BOOL)gridTable:(GridTable*)gridTable getBoolForRow:(NSInteger)row column:(GridTableColumnDescription *)column;
- (void)gridTable:(GridTable*)gridTable changeBoolForRow:(NSInteger)row column:(GridTableColumnDescription *)column cell:(GridTableCell*)cell;
- (BOOL)gridTable:(GridTable*)gridTable formatCellForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content selected:(BOOL)selected initial:(BOOL)initital;
- (void)gridTable:(GridTable*)gridTable reorderRowAtIndexPath:(NSIndexPath *)fromIndex toIndexPath:(NSIndexPath *)toIndex;
- (void)gridTable:(GridTable*)gridTable cellButtonClickedForRow:(NSInteger)row column:(GridTableColumnDescription *)column button:(UIButton *)button;
- (UIImage*)gridTable:(GridTable*)gridTable getImageForRow:(NSInteger)row column:(GridTableColumnDescription *)column;
- (CGFloat)gridTable:(GridTable *)gridTable heightForRow:(NSInteger)row;
- (void)gridTableLayoutSubViews:(GridTable *)gridTable;
- (void)gridTable:(GridTable *)gridTable willDisplayRow:(NSInteger)row gridTableRow:(GridTableRow*)gridTableRow;
@end

@class RTTableView;

@interface GridTable : UIView  <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GridTableRowDelegate, UIScrollViewDelegate> {
    UIView			* _headerView;
    UITableView		* _tableView;
    UIRoundedCornersView  *_footerView;
    UIImageView		* _upArrow;
    UIImageView		* _downArrow;
    UIImage			* _backgroundImage;
    UIImage			* _yesImage;
    UIImage			* _noImage;
    SortingMode  _sortingMode;
    NSString		* _sortingColumnName;
    NSMutableArray	* _columns;
    NSArray         * _defaultColumns;
    NSNumber         * _defaultHorizontalScrollEnabled;
    id<GridTableDelegate> _delegate;
    UITextField		* _activeField;
    UIScrollView    * _scrollView;
    BOOL            allowCustomization;
    
    BOOL _footerViewHiden;
    NSString *_rowIdentifier;
    NSUInteger spaceBetweenRows;
    UIColor* selectedRowBackgroundColor;
    UIColor * rowTextColorSelected;
    UIColor * rowTextColor;
    UIColor * rowBackgroundColorOdd;
    UIColor * rowBackgroundColorEven;
    UIFont * headerFont;
    UIFont*  cellFont;
    BOOL     firstLoadProcessed;
    BOOL     firstReloadDataWithDelegateProcessed;
    NSUInteger defferedSelectedRow;
    
    NSInteger updateCounter;
}
@property (nonatomic, readonly) CGFloat	spaceBetweenColumns;
@property (nonatomic, retain)   NSArray	* columns;
@property (nonatomic, retain) NSArray	* defaultColumns;
@property (nonatomic, assign) BOOL	defaultHorizontalScrollEnabled;
@property (nonatomic, assign)   id<GridTableDelegate> delegate;
@property (nonatomic, assign)   NSUInteger spaceBetweenRows;
@property (nonatomic, readonly) NSInteger rowCount;
@property (nonatomic, readonly) UITableView* tableView;
@property (nonatomic, readonly) UIView* headerView;
@property (nonatomic, readonly) UIRoundedCornersView* footerView;
@property (nonatomic, retain)   UIFont* cellFont;
@property (nonatomic, retain)   UIFont* headerFont;
@property (nonatomic, retain)UIColor* rowTextColorSelected;
@property (nonatomic, retain)UIColor* rowTextColor;
@property (nonatomic, retain)UIColor* rowBackgroundColorOdd;
@property (nonatomic, retain)UIColor* rowBackgroundColorEven;

@property (nonatomic, readonly) UIScrollView* scrollView;
@property (nonatomic, assign)   BOOL horizontalScrollEnabled;
@property (nonatomic, assign)   BOOL allowCustomization;
@property (nonatomic, assign)   BOOL allowMoveRows;
@property (nonatomic, assign)   NSInteger sortingColumnIndex;
@property (nonatomic, retain)   NSString *sortingColumnName;

@property (nonatomic, retain)   UIImage	* yesImage;
@property (nonatomic, retain)   UIImage	* noImage;
@property (nonatomic, assign)   BOOL	selectRowOnReload;
@property (nonatomic, assign) UITableViewScrollPosition scrollPosition;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, retain) UIColor* selectedRowBackgroundColor;

@property (nonatomic, assign)  BOOL footerViewHiden;
@property (nonatomic, assign)  CGFloat minimumColumnWidthWhileResizing;

@property (nonatomic, assign) BOOL allowsMultipleSelection;
@property (nonatomic, readonly) NSIndexSet * selectedRows;
@property (nonatomic, readonly)BOOL endingLayoutSubviews;
@property (nonatomic, readonly)NSInteger highlightingBorderColumnIndex;
-(void)clearDefaults;
-(void)reloadData;
-(void)reloadRow:(NSInteger)row;
-(BOOL)isRowVisible:(NSInteger)row;
-(void)selectRow:(NSInteger)row;
-(void)selectRow:(NSInteger)row animated:(BOOL)animated;
-(void)deselectRow:(NSInteger)row;
-(void)deselectRow:(NSInteger)row animated:(BOOL)animated;
- (void)removeSelection:(BOOL)animated;
-(NSInteger)selectedRowIndex;
-(GridTableColumnDescription*)sortingColumn;
-(SortingMode)sortingMode;
- (void)sortByColumn:(NSInteger)columnIndex order:(SortingMode)order;
- (GridTableRow *)cellForRow:(NSInteger)row;
- (UIView *)contentForRow:(NSInteger)row column:(NSString *)columnName;
- (void)setContentForRowDefault:(NSInteger)row columnView:(UIView*)columnView column:(GridTableColumnDescription *)column value:(NSObject*)rowValue;
- (NSArray *)getVisibleColumns;
- (GridTableColumnHeader *)getColumnHeaderByColumnDescription:(GridTableColumnDescription*) column;
- (NSArray*)getColumnHeadersByColumnName:(NSString*) name;
- (void)rebuildGrid;
- (void)resetToDefaults;
- (void)endEditing;
-(CGRect)getDefaultContentRectForColumn:(GridTableColumnDescription*)column;
- (void)resetScroll;
//just for internal usage
-(void)getRectsForColumn:(GridTableColumnDescription*)column cellRect:(CGRect*)cRect contentRect:(CGRect*)contentRect rowHeight:(CGFloat)rowHeight;
+ (UIControlContentHorizontalAlignment)convertTextAlignmentToContentHorizontalAlignment:(NSTextAlignment)alignment;

-(void)beginUpdate;
-(void)endUpdate;
-(void)startBlinking;
-(BOOL)isBlinking;
@end
