//
//  EditableImageView.h
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTPopoverController.h"

@interface EditableImageView : UIImageView <UIImagePickerControllerDelegate, UINavigationControllerDelegate, RTPopoverControllerDelegate> {
	//UIImageView	* imageView;
	UIButton	* addImageButton;
	UIButton	* changeImageButton;
	UIButton	* deleteImageButton;
	RTPopoverController *popover;
}

@property (nonatomic, retain) IBOutlet UIButton		* addImageButton;
@property (nonatomic, retain) IBOutlet UIButton		* changeImageButton;
@property (nonatomic, retain) IBOutlet UIButton		* deleteImageButton;
//@property (nonatomic, retain) IBOutlet UIImageView	* imageView;
//@property (nonatomic, assign) UIImage				* image;
@property (retain, readwrite) RTPopoverController	* popover;

@end
