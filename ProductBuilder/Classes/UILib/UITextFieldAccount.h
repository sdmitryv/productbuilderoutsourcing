//
//  UITextFieldAccount.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 3/27/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UISearchBarTransparent.h"

@interface UITextFieldAccount : UITextField

@end

@interface UISearchBarTransparentAccount : UISearchBarTransparent

@end
