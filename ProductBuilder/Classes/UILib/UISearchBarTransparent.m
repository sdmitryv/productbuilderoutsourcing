//
//  UISearchBarTransparent.m
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "UISearchBarTransparent.h"
#import "UITextField+Input.h"
#import "SystemVer.h"

@implementation UISearchBarTransparent

-(void)removeSearchIcon{
	UITextField *tField=self.textField;
	if(tField)
		tField.leftView = nil;
}

-(UITextField*)textField{
	
    if (!textField){
        NSArray* subviews = self.subviews;
        
        #ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
            if (!subviews.count) return nil;
            subviews = [(self.subviews)[0] subviews];
        }
        #endif
        for(UIView* view in subviews) {
			if([view isKindOfClass:[UITextField class]]) {
				textField = (UITextField*)view;
                textField.textColor = [UIColor blackColor];
				break;
			}
		}
	}
	return textField;
}

- (void)setInputView:(UIView*)view {
    self.textField.inputView = view;
}

-(void)insertText:(NSString*)text{
	UITextField* tfield =  self.textField;
	if ([tfield respondsToSelector:@selector(insertText:)])
		[tfield performSelector:@selector(insertText:) withObject:text];
}
    
    - (void)drawRect:(CGRect)rect {
        // do nothing in here
    }
    
- (void) applyTranslucentBackground
{
    BOOL needToUpdateBackground = TRUE;
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
        needToUpdateBackground = FALSE;
        if (self.subviews.count){
            UIView* view = self.subviews[0];
            if (view.subviews.count){
                if ([[view.subviews[0] description] hasPrefix:@"<UISearchBarBackground"]){
                    [view.subviews[0] removeFromSuperview];
                }
            }
        }
        SEL centerSelector = NSSelectorFromString([NSString stringWithFormat:@"%@%@", @"setCenter", @"Placeholder:"]);
        if ([self respondsToSelector:centerSelector])
        {
            BOOL hasCentredPlaceholder = FALSE;
            NSMethodSignature *signature = [[UISearchBar class] instanceMethodSignatureForSelector:centerSelector];
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
            [invocation setTarget:self];
            [invocation setSelector:centerSelector];
            [invocation setArgument:&hasCentredPlaceholder atIndex:2];
            [invocation invoke];
        }
    }
#endif
    if (needToUpdateBackground){
        [self setBackgroundColor:[UIColor clearColor]];
        for(NSInteger i = self.subviews.count- 1; i >=0; i--){
            UIView* view = (self.subviews)[i];
            if ([view.description hasPrefix:@"<UISearchBarBackground"]){
                [view removeFromSuperview];
            }
            if ([view.description hasPrefix:@"<UILabel"]){
                [view removeFromSuperview];
            }
            if ([view.description hasPrefix:@"<UISearchBarTextField"]){
                view.backgroundColor = [UIColor clearColor];
            }
        }
    }
}

// Override init.
- (id) init
{
	self = [super init];
	[self applyTranslucentBackground];
	return self;
}

// Override initWithFrame.
- (id) initWithFrame:(CGRect) frame
{
	if ((self = [super initWithFrame:frame])){
		[self applyTranslucentBackground];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super initWithCoder:decoder])) {
		[self applyTranslucentBackground];
	}
    return self;
}

@end



@implementation UISearchBarTransparentBig

- (void) applyTranslucentBackground {
    
	[super applyTranslucentBackground];
    
    UIImage *searchFieldImage = [UIImage imageNamed:@"searchBar.png"];
    [self setSearchFieldBackgroundImage:searchFieldImage forState:UIControlStateNormal];
    [self textField].font = [UIFont systemFontOfSize:17];
    [self textField].textColor = [UIColor blackColor];
    if ([self.textField.leftView isKindOfClass:[UIImageView class]]){
        self.textField.leftView.contentMode = UIViewContentModeLeft;
        CGRect frame = self.textField.leftView.frame;
        CGFloat delta = 3;
#ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")){
            delta = 8;
        }
#endif
        frame.size.width+=delta;
        self.textField.leftView.frame = frame;
        
    }
}


-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect r = self.textField.frame;
    r.size.height = IS_IPHONE_6_OR_MORE ? 38 : 44;
    r.origin.y = (self.frame.size.height - r.size.height)/2;
    self.textField.frame = r;
}

@end
