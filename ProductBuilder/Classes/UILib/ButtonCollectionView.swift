//
//  ButtonCollectionView.swift
//  ProductBuilder
//
//  Created by PavelGurkovskii on 1/12/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation

protocol ButtonCollectionViewDelegate: AnyObject {
    func buttonCollection(_:ButtonCollectionView, didSelectItemAt index:IndexPath, item:AnyObject)
}

class ButtonCollectionView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private var buttonsCollectionView:UICollectionView?
    var buttons:Array<AnyObject>?
    var numberOfLinesColumns = 1
    var direction:UICollectionViewScrollDirection = .horizontal
    var delegate:ButtonCollectionViewDelegate?
    var isEnabled:Bool = false {
        didSet {
            buttonsCollectionView?.reloadData()
        }
    }
    
    var collectionView: UICollectionView? {
        return buttonsCollectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colLayout = UICollectionViewFlowLayout()
        colLayout.itemSize = CGSize(width: 206.0, height: 44.0)
        colLayout.minimumLineSpacing = 30.0
        colLayout.minimumInteritemSpacing = 22.0
        colLayout.scrollDirection = direction
        buttonsCollectionView = UICollectionView(frame: view.bounds, collectionViewLayout: colLayout)
        buttonsCollectionView?.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        view.addSubview(buttonsCollectionView!)
        buttonsCollectionView?.register(UINib(nibName:"ButtonCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "ButtonCollectionViewCell")
        buttonsCollectionView?.delegate = self
        buttonsCollectionView?.dataSource = self
        buttonsCollectionView?.backgroundColor = UIColor.clear
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let b = buttons {
            if direction == .horizontal {
                let f:Float = Float(b.count)/Float(numberOfLinesColumns)
                let i:Int = Int(f)
                return i + (f - Float(i) > 0 ? 1 : 0)
            } else {
                return numberOfLinesColumns
            }
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let b = buttons {
            let n = numberOfSections(in: collectionView) - 1
            if direction == .horizontal {
                if section < n {
                    return numberOfLinesColumns
                } else {
                    return b.count - numberOfLinesColumns * n
                }
            } else {
                if section < n {
                    return b.count / (n + 1)
                } else {
                    return b.count - n * b.count / (n + 1)
                }
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ButtonCollectionViewCell", for: indexPath) as! ButtonCollectionViewCell
        cell.button.isEnabled = isEnabled
        cell.selectBlock = {[weak self] in
            if let d = self?.delegate, let item = self?.getSelectedElement(indexPath:indexPath) {
                d.buttonCollection(self!, didSelectItemAt: indexPath, item: item)
            }
        }
        
        if let b = getSelectedElement(indexPath:indexPath), b.description != nil {
            cell.button.setTitle(b.description, for: .normal)
        }
        return cell
    }
    
    func getSelectedElement(indexPath:IndexPath) -> AnyObject? {
        if let b = buttons {
            if direction == .horizontal {
                return b[indexPath.section * numberOfLinesColumns + indexPath.row]
            } else {
                let rows = Int(b.count / numberOfLinesColumns)
                return b[rows * indexPath.section + indexPath.row]
            }
        } else {
            return nil
        }
    }
    
}
