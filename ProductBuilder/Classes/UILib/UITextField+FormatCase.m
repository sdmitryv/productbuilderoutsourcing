//
//  UITextField+FormatCase.m
//  ProductBuilder
//
//  Created by Roman on 3/16/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UITextField+FormatCase.h"
#import <objc/runtime.h>

@implementation UITextField (FormatCase)

const char* textCaseTypeKey = "TextCaseTypeKey";

//ConvertEntryType _textCaseType = ConvertEntryTypeDefault;

-(void)setTextCaseType:(ConvertEntry)convertEntry {
    NSNumber* currentValue = (NSNumber*)objc_getAssociatedObject(self, textCaseTypeKey);
    if (!currentValue) {
        objc_setAssociatedObject(self, textCaseTypeKey, @(convertEntry), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        [currentValue release];
    }
}

-(ConvertEntry)textCaseType {
    NSNumber* currentValue = (NSNumber*)objc_getAssociatedObject(self, textCaseTypeKey);
    if (!currentValue) {
        return ConvertEntryDefault;
    }
    return currentValue.intValue;
}

@end
