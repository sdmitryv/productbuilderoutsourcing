//
//  UIXLabelView.m
//  GlassButtonTest
//
//  Created by valera on 10/31/11.
//  Copyright (c) 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "UIXLabelView.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIXLabelView

-(void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:[UIColor clearColor]];
    [labelColor release];
     labelColor = [backgroundColor retain];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if (self.layer.backgroundColor!=[UIColor clearColor].CGColor){
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, labelColor.CGColor);
    CGContextSetLineCap(contextRef, kCGLineCapSquare);
    CGFloat width = 4.0f;
    CGFloat delta = width/2 + 1;
    CGContextSetLineWidth(contextRef, width);
    
    //left bottom to right top
    CGContextMoveToPoint(contextRef, self.bounds.origin.x +delta, self.bounds.origin.y + self.bounds.size.height);
    CGContextAddLineToPoint(contextRef, self.bounds.origin.x + self.bounds.size.width - delta, self.bounds.origin.y);
    
    //left top to right bottom
    CGContextMoveToPoint(contextRef, self.bounds.origin.x + delta, self.bounds.origin.y);
    CGContextAddLineToPoint(contextRef, self.bounds.origin.x + self.bounds.size.width - delta, self.bounds.origin.y + self.bounds.size.height);
    
    CGContextClosePath(contextRef);
    CGContextDrawPath(contextRef, kCGPathStroke);
}

-(void)dealloc{
    [labelColor release];
    [super dealloc];
}


@end
