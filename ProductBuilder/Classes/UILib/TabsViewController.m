//
//  TabsViewController.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/11/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "TabsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MOGlassButton.h"
#import "Global.h"
#import "UIView+FirstResponder.h"
#import "UIImage+Color.h"
#import "SystemVer.h"

#define TAB_BUTTON_WIDTH 90
#define TAB_BUTTON_HEIGHT 50

@implementation TabViewDescription

@synthesize title, image, controller;

+(id)tabViewDescriptionWithTitle:(NSString*)aTitle contoller:(UIViewController*)aController{
    return [[[[self class]alloc]initWithTitle:aTitle contoller:aController]autorelease];
}

+(id)tabViewDescriptionWithTitle:(NSString*)aTitle image:(UIImage*)anImage contoller:(UIViewController*)aController{
    return [[[[self class]alloc]initWithTitle:aTitle image:anImage contoller:aController]autorelease];
}

-(id)initWithTitle:(NSString*)aTitle contoller:(UIViewController*)aController{
    return [self initWithTitle:aTitle image:nil contoller:aController];
}

-(id)initWithTitle:(NSString*)aTitle image:(UIImage*)anImage contoller:(UIViewController*)aController {
    if ((self=[super init])){
        self.title = aTitle;
        self.image = anImage;
        self.controller = aController;
    }
    return self;
}

-(void)dealloc {
    [title release];
    [image release];
    [controller release];
    
    [super dealloc];
}

@end

@interface UITabButton:UIButton{
}

-(id)initWithTabViewController:(TabsViewController*)tabViewController
                         frame:(CGRect)frame
              normalStateImage:(UIImage*)normalStateImage highlightedStateImage:(UIImage*)highlightedStateImage;
@end

@implementation UITabButton

-(id)initWithTabViewController:(TabsViewController*)tvController
                         frame:(CGRect)frame
              normalStateImage:(UIImage*)nStateImage highlightedStateImage:(UIImage*)hStateImage{
    if ((self = [super initWithFrame:frame])){
        UIImage* normalImage =nil;
        
        normalImage =  [UIImage imageWithColor:MO_RGBCOLOR(81, 82, 85)];
        
        [self setBackgroundImage:normalImage forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        UIImage* selectedImage =nil;
        selectedImage =  [UIImage imageWithColor:[UIColor whiteColor]];
        [self setBackgroundImage:selectedImage forState:UIControlStateSelected];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self setSelected:FALSE];
    }
    return self;
}
-(void)dealloc{
    [super dealloc];
}

@end

@implementation TabsViewController

@synthesize delegate;

- (id)initWithTabs:(NSArray *)aTabs{
    
    self = [super init];
    
    if (self) {
      
        _tabs = [aTabs retain];
        currentTabNumber = 0;
        normalStateImage = [[UIImage imageNamed:@"tab_button.png"]retain];
        highlightedStateImage = [[UIImage imageNamed:@"tab_button_highlighted.png"]retain];
        _tabButtonHeight = _tabButtonWidth = 0;
    }
    
    return self;
}


- (void)dealloc {
    [_selectedTextColor release];
    [_normalTextColor release];
    [_selectedButtonColor release];
    [_normalButtonColor release];
    [_tabs release];
    [buttonsBarView release];
    [contentView release];
    [normalStateImage release];
    [highlightedStateImage release];
    
    [super dealloc];
}


#pragma mark - View lifecycle

-(UIColor*)selectedTextColor{
    return _selectedTextColor ?:[UIColor whiteColor];
}

-(UIColor*)normalTextColor{
    return _normalTextColor ?:[UIColor blackColor];
}

-(UIColor*)normalButtonColor{
    return _normalButtonColor;
}

-(UIColor*)selectedButtonColor{
    return _selectedButtonColor;
}

- (void)activateControllerAtIndex:(NSInteger) index {
    UIViewController *viewController = ((TabViewDescription*)_tabs[index]).controller;
    if (viewController) {
        viewController.view.frame = contentView.bounds;
        if (viewController.view.superview!=contentView){
            [contentView addSubview:viewController.view];
            [self addChildViewController:viewController];
        }
    }
    
    currentTabNumber = index + 1;
    
    if ([viewController respondsToSelector:@selector(didActivateTabView)])
        [viewController performSelector:@selector(didActivateTabView)];
    if (delegate)
        [delegate tabSelected:currentTabNumber];
}

-(UIViewController*)selectedTabViewController{
    return currentTabNumber && currentTabNumber <= _tabs.count? ((TabViewDescription*)_tabs[currentTabNumber - 1]).controller : nil;
}

- (void) switchTabForButton:(UIButton *)button {
    NSInteger tabNumber = button.tag;
    if (tabNumber< 1 || _tabs.count < tabNumber || tabNumber == currentTabNumber) return;

    if (currentTabNumber) {
        if (delegate) {
            if (![delegate tabShouldDeselected:currentTabNumber])
                return;
        }
    }

    UIResponder* responder = [self.view getFirstResponder];
    if (responder && ![responder resignFirstResponder]){
        return;
    }
    
    if (delegate) {
        if (![delegate tabShouldSelected:tabNumber]) {
            if (responder != nil) {
                [responder becomeFirstResponder];
            }
            return;
        }
    }
  
    if (currentTabNumber) {
        UIButton *currentTabButton = [self.view viewWithTag:currentTabNumber];
        [currentTabButton setSelected:FALSE];
        UIViewController *currentViewController = ((TabViewDescription*)_tabs[currentTabNumber - 1]).controller;
        [currentViewController.view removeFromSuperview];
        [currentViewController removeFromParentViewController];
    }
    
    [button setSelected:TRUE];
    
    [self activateControllerAtIndex:tabNumber - 1];
}

- (void) switchTabForBarButton:(UITabBarItem *)button {
    
    NSInteger tabNumber = button.tag;
    if (tabNumber< 1 || _tabs.count < tabNumber || tabNumber == currentTabNumber) return;
    
    if (currentTabNumber) {
        if (delegate) {
            if (![delegate tabShouldDeselected:currentTabNumber])
                return;
        }
    }
    
    UIResponder* responder = [self.view getFirstResponder];
    if (responder && ![responder resignFirstResponder]){
        return;
    }
    
    if (delegate)
        if (![delegate tabShouldSelected:tabNumber])
            return;
    
    if (tabNumber != currentTabNumber) {
        if (currentTabNumber != 0) {
            UIViewController *currentViewController = ((TabViewDescription*)_tabs[currentTabNumber - 1]).controller;
            [currentViewController.view removeFromSuperview];
            [currentViewController removeFromParentViewController];
        }

        
        ((UITabBar *)buttonsBarView).selectedItem = button;

        [self activateControllerAtIndex:tabNumber - 1];
    }
}

- (void) touchUpInsideButton:(UIButton *)button {

    [self switchTabForButton:button];
}


-(UIView*) buttonsBarView{
    
    if (!self.isViewLoaded)
        [self loadView];

    return buttonsBarView;
}


-(CGFloat)tabButtonWidth{

    return _tabButtonWidth > 0 ? _tabButtonWidth : TAB_BUTTON_WIDTH;
}


-(CGFloat)tabButtonHeight{
    
    return _tabButtonHeight > 0 ? _tabButtonHeight : TAB_BUTTON_HEIGHT;
}


- (UIButton*)createTabButtonWithFrame:(CGRect)frame{
    
    UITabButton * button = [[[UITabButton alloc]initWithTabViewController:self frame:frame normalStateImage:normalStateImage highlightedStateImage:highlightedStateImage]autorelease];
            
    button.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    return button;
}


- (void)loadView {
    
    UIView * tabView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1006, 616)];
    tabView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    tabView.autoresizesSubviews = TRUE;
    CGFloat tabButtonWidth = [self tabButtonWidth];
    CGFloat tabButtonHeight = [self tabButtonHeight];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE) {
        buttonsBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tabs.count * (tabButtonWidth + 1) + 1, tabButtonHeight + 4)];
        
        buttonsBarView.backgroundColor = [UIColor whiteColor];

        buttonsBarView.layer.masksToBounds = TRUE;
        buttonsBarView.layer.cornerRadius = 5.0f;
        buttonsBarView.userInteractionEnabled = YES;
        CGRect buttonRect = CGRectMake(0, 0, tabButtonWidth, tabButtonHeight);
        
        for (NSInteger i = 0; i < _tabs.count; i++) {
            
            UIButton * button = [self createTabButtonWithFrame:buttonRect];
            [button setTitle:((TabViewDescription*)_tabs[i]).title forState:UIControlStateNormal];
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            button.tag = i + 1;
            [button addTarget:self action:@selector(touchUpInsideButton:) forControlEvents:UIControlEventTouchUpInside];
            [buttonsBarView addSubview:button];
            buttonRect.origin.x += [self tabButtonWidth] + 1;
        }
    } else {
        UITabBar * tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 0, tabView.bounds.size.width, tabButtonHeight)];
        tabBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        #ifdef __IPHONE_7_0
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            tabBar.barStyle = UIBarStyleBlack;
            tabBar.translucent = NO;
        }
        #endif
        tabBar.delegate = self;
        NSMutableArray * buttonItems = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < _tabs.count; i++) {
            TabViewDescription * tabDescription = _tabs[i];
            UITabBarItem * buttonItem = [[UITabBarItem alloc] initWithTitle:tabDescription.title image:tabDescription.image tag:i + 1];
            [buttonItems addObject:buttonItem];
            [buttonItem autorelease];
        }
        tabBar.items = buttonItems;
        [buttonItems release];
        buttonsBarView = tabBar;
    }

    [tabView addSubview:buttonsBarView];
    
    contentView = [[UIRoundedCornersView alloc] initWithFrame:CGRectMake(0, tabButtonHeight, tabView.bounds.size.width, tabView.bounds.size.height - tabButtonHeight)];
    if (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ) && [self isKindOfClass:[TabsViewControllerBlack class]])
        [contentView setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight  radius:5.0f];
    
    else
        [contentView setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight | UIViewRoundedCornerUpperRight radius:5.0f];
    
    contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    contentView.autoresizesSubviews = TRUE;
    contentView.clipsToBounds = TRUE;
    [tabView addSubview:contentView];
    self.view = tabView;
    [tabView release];
    [self switchToTab:0];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad || IS_IPHONE_6_OR_MORE) && ![self isKindOfClass:[TabsViewControllerBlack class]]) {
        
        CGFloat tabButtonWidth = [self tabButtonWidth];
        CGFloat tabButtonHeight = [self tabButtonHeight];
        
        if (_tabsShouldFillAllWidth) {
            tabButtonWidth = (self.view.bounds.size.width - _tabs.count + 1) / (_tabs.count > 0 ? self.tabs.count : 1);
        }
        
        buttonsBarView.frame = CGRectMake(0, 0, _tabs.count * tabButtonWidth +_tabs.count-1, tabButtonHeight + 4);

        CGRect buttonRect = CGRectMake(0, 0, tabButtonWidth, tabButtonHeight);
        
        for (UIButton *button in [buttonsBarView subviews]) {
            button.frame = buttonRect;
            buttonRect.origin.x += tabButtonWidth + 1;
        }
        
        contentView.frame = CGRectMake(0, tabButtonHeight, self.view.bounds.size.width, self.view.bounds.size.height - tabButtonHeight);
        
        if (IS_IPHONE_6_OR_MORE) {
            [contentView setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight radius:5.0f];
        }
    }
    if ([self isKindOfClass:[TabsViewControllerBlack class]]) {
        
        
        CGFloat tabButtonWidth = [self tabButtonWidth];
        CGFloat tabButtonHeight = [self tabButtonHeight];
        
        if ((_tabs.count * tabButtonWidth +_tabs.count - 1) > self.view.bounds.size.width){
            tabButtonWidth = (self.view.bounds.size.width - _tabs.count +1) / (_tabs.count > 0 ? self.tabs.count : 1);
        }
        
        CGRect buttonRect = CGRectMake(0, 0, tabButtonWidth, tabButtonHeight);
        
        int idx = -1;
        for (UIButton *button in [buttonsBarView subviews]) {
            
            idx++;
            
            if (idx == 0)
                continue;
            
            button.frame = buttonRect;
            buttonRect.origin.x += tabButtonWidth + 1;
        }

        contentView.frame = CGRectMake(1, tabButtonHeight - 4, self.view.bounds.size.width - 2, self.view.bounds.size.height - tabButtonHeight + 4);
        
        UIView* lastButton = [[buttonsBarView subviews]lastObject];
        UIButton* backgroudButton = buttonsBarView.subviews[0];
        backgroudButton.frame =  CGRectMake(lastButton.frame.origin.x + lastButton.frame.size.width + 1, 0, buttonsBarView.bounds.size.width - (lastButton.frame.origin.x + lastButton.frame.size.width + 1), tabButtonHeight);
    }
}

- (void)switchToTab:(NSUInteger)tabIndex {

    if (_tabs.count <= tabIndex) return;
    
    if ([buttonsBarView isKindOfClass:[UITabBar class]]) {
        [self switchTabForBarButton:(((UITabBar *)buttonsBarView).items)[tabIndex]];
    }
    else {
        [self switchTabForButton:[buttonsBarView viewWithTag:tabIndex + 1]];
    }
}

-(UIViewController*)getTabControllerByClass:(Class)aClass{
    for (TabViewDescription* tabDescription in self.tabs){
        if ([tabDescription.controller isKindOfClass:aClass]){
            return tabDescription.controller;
        }
    }
    return nil;
}

- (NSUInteger)indexOfTab:(NSString*)tabName {

    for (TabViewDescription* d in _tabs)
        if ([d.title isEqualToString:tabName])
            return [_tabs indexOfObject:d];

    return NSNotFound;
}

- (NSUInteger)indexOfTabByControllerClass:(Class)aClass{
    
    for (TabViewDescription* d in _tabs)
        if ([d.controller isKindOfClass:aClass])
            return [_tabs indexOfObject:d];
    
    return NSNotFound;
}


- (TabViewDescription*)getTabByTitle:(NSString*)tabName{

    for (TabViewDescription* d in _tabs)
        if ([d.title isEqualToString:tabName])
            return d;

    return nil;
}


- (void)setEnabled:(BOOL)enabled forTab:(NSUInteger)tabIndex {
    
    UIButton *tabButton = [buttonsBarView viewWithTag:tabIndex + 1];
    tabButton.enabled = enabled;
}


-(NSInteger)selectedTab{

    return currentTabNumber-1;
}

#pragma mark - UITabBarDelegate Methods

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [self switchToTab:item.tag - 1];
    if (currentTabNumber != item.tag) {
        for (UITabBarItem * i in tabBar.items) {
            if (i.tag == currentTabNumber) {
                tabBar.selectedItem = i;
                break;
            }
        }
    }
}

@end

@implementation TabsViewControllerBlack

-(CGFloat)tabButtonWidth{
    return 120;
}

- (UIButton*)createTabButtonWithFrame:(CGRect)frame{
    MOGlassButton * button = [[[MOGlassButton alloc]initWithFrame:frame]autorelease];
    
    button.cornerRadius = 0.0f;
    [button setBackgroundColor:MO_RGBCOLOR(81, 82, 85) forState:UIControlStateNormal];
    [button setBackgroundColor:MO_RGBCOLOR(0, 0, 0) forState:UIControlStateHighlighted];
    [button setBackgroundColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button setBackgroundColor:MO_RGBCOLOR(209, 216, 232) forState:UIControlStateDisabled];
    
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateSelected | UIControlStateHighlighted];
    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateDisabled];
    button.titleLabel.font = [UIFont systemFontOfSize:14.0];
    return button;
}

- (void)loadView
{
    [super loadView];
    UIView* buttonsBarView = [self buttonsBarView];
    buttonsBarView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    buttonsBarView.frame = CGRectMake(buttonsBarView.frame.origin.x, buttonsBarView.frame.origin.y, self.view.bounds.size.width, buttonsBarView.bounds.size.height - 4);
    buttonsBarView.backgroundColor = [UIColor grayColor];
    
    UIView* lastButton = [[buttonsBarView subviews]lastObject];
    
    UIButton* backgroudButton = [self createTabButtonWithFrame:CGRectMake(lastButton.frame.origin.x + lastButton.frame.size.width + 1, 0, buttonsBarView.bounds.size.width-(lastButton.frame.origin.x + lastButton.frame.size.width + 1), [self tabButtonHeight])];
    backgroudButton.userInteractionEnabled = FALSE;
    [buttonsBarView insertSubview:backgroudButton atIndex:0];
}


@end
