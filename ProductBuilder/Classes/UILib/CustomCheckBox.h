//
//  CustomCheckBox.h
//  ProductBuilder
//
//  Created by User on 4/23/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomCheckBoxDelegate;
@interface CustomCheckBox : UIButton{
    BOOL buttonStateIsChecked;
    UIImage *yesImage;
    UIImage *noImage;
    id<CustomCheckBoxDelegate> delegate;
}
@property(nonatomic,readonly) BOOL buttonStateIsChecked;
@property (nonatomic, assign) id<CustomCheckBoxDelegate> delegate;
@end

@protocol CustomCheckBoxDelegate
@optional
-(void)stateChangedInCheckbox:(CustomCheckBox*)checkBox; 
@end
