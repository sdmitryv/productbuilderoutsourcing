//
//  RTWScrollView.h
//  iPadPOS
//
//  Created by valera on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RTScrollView : UIScrollView {
    BOOL _scrollViewIsAdjustedToKeyboard;
    CGRect lastKeyboardRect;
}
-(void)scrollToFirstResponder;
@property (nonatomic, assign)CGFloat cornerRadius;
@property (nonatomic, assign)UIView * ownerView;
@end
