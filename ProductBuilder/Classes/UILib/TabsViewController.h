//
//  TabsViewController.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/11/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIRoundedCornersView.h"

@interface TabViewDescription : NSObject{
    
    NSString * title;
    UIImage * image;
    UIViewController * controller;
}

+(id)tabViewDescriptionWithTitle:(NSString*)aTitle contoller:(UIViewController*)aController;
+(id)tabViewDescriptionWithTitle:(NSString*)aTitle image:(UIImage*)anImage contoller:(UIViewController*)aController;
-(id)initWithTitle:(NSString*)title contoller:(UIViewController*)controller;
-(id)initWithTitle:(NSString*)title image:(UIImage*)anImage contoller:(UIViewController*)controller;

@property(nonatomic, retain)NSString * title;
@property(nonatomic, retain)UIImage * image;
@property(nonatomic, retain)UIViewController * controller;

@end



@protocol TabsViewControllerDelegate

-(BOOL)tabShouldSelected:(NSInteger)index;
-(void)tabSelected:(NSInteger)index;
-(BOOL)tabShouldDeselected:(NSInteger)index;

@end



@interface TabsViewController : UIViewController <UITabBarDelegate> {

@private
    UIView  * buttonsBarView;
    UIRoundedCornersView  * contentView;
    NSInteger currentTabNumber;
    UIImage * normalStateImage;
    UIImage * highlightedStateImage;

    id<TabsViewControllerDelegate> delegate;
}

-(id)initWithTabs:(NSArray *)tabs;

-(void)switchToTab:(NSUInteger)tabIndex;
-(UIViewController*)getTabControllerByClass:(Class)aClass;
-(NSInteger)selectedTab;
-(NSUInteger)indexOfTab:(NSString*)tabName;
-(NSUInteger)indexOfTabByControllerClass:(Class)aClass;
-(TabViewDescription*)getTabByTitle:(NSString*)tabName;
-(void)setEnabled:(BOOL)enabled forTab:(NSUInteger)tabIndex;
-(UIViewController*)selectedTabViewController;
@property (nonatomic, readonly) UIView *buttonsBarView;
@property (nonatomic, assign) id<TabsViewControllerDelegate> delegate;

//these methods are intended just for overrdide
-(UIButton*)createTabButtonWithFrame:(CGRect)frame;

@property(nonatomic, retain)UIColor* normalTextColor;
@property(nonatomic, retain)UIColor* selectedTextColor;
@property(nonatomic, retain)UIColor* normalButtonColor;
@property(nonatomic, retain)UIColor* selectedButtonColor;
@property(nonatomic, retain)NSArray* tabs;
@property(nonatomic, assign) CGFloat tabButtonWidth;
@property(nonatomic, assign) CGFloat tabButtonHeight;
@property(nonatomic, assign) BOOL tabsShouldFillAllWidth;

@end


@interface TabsViewControllerBlack: TabsViewController

@end
