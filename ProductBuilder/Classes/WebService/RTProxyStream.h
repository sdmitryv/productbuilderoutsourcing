//
//  RTProxyStream.h
//  ProductBuilder
//
//  Created by valera on 6/4/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RTProxyStream : NSInputStream

-(void)append:(const char*)data length:(size_t)length;
@end
