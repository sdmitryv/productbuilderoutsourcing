//
//  StoredValuesService.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/14/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"
#import "StoredValuesServiceRequest.h"
#import "SettingManager.h"

@class ReceiptPayment;
@class Customer;
@class Receipt;
@class ReceiptItem;

typedef NS_ENUM(NSUInteger, SVSLineType) {
    SVSLineTypeNone = 0,
    SVSLineTypeItem = 1,
    SVSLineTypePayment = 2,
    SVSLineTypeCharge = 3,
    SVSLineTypeAdjustment = 4,
    SVSLineTypeRedemption = 5,
    SVSLineTypeGiftCard = 6,
    SVSLineTypeReceipt = 7,
};

typedef NS_ENUM(NSUInteger, SVSTransactionLineType) {
    SVSTransactionLineTypeNone = 0,
    SVSTransactionLineTypeCredit = 1,
    SVSTransactionLineTypePayment = 2,
    SVSTransactionLineTypeReturn = 3
};


@interface StoredValuesService : NSObject{
    NSURL * loyaltyPointsServiceURL;
    NSURL * newloyaltyPointsServiceURL;
    NSURL * houseChargeServiceURL;
    //NSURL * frequentBuyerServiceURL;
    
    NSURL * _devicesAreaURL;
    NSURL * _customersAreaURL;
    NSURL * _giftCardsAreaURL;
    NSURL * _loyaltyAreaURL;
    NSURL * _settingsAreaURL;
    NSURL * _udsAreaURL;
    NSURL * _couponsAreaURL;
    NSURL * _storeCreditAreaURL;
    NSURL * _tokenAreaURL;
    NSURL * _receiptAreaURL;
    NSURL * _receiptOnlineAreaURL;
    NSURL * _fbAreaURL;
    NSURL * _customerCardOnFileURL;
}

-(id)initWithUsername:(NSString *)name password:(NSString *)password;
-(id)initWithForGetUrl;

-(StoredValuesServiceRequest *)validateEmail:(NSString *)email;
-(StoredValuesServiceRequest *)checkPassword:(NSString *)password forCustomer:(BPUUID *)customerId;

-(StoredValuesServiceRequest *)getCustomerFrequentBuyerPrograms:(BPUUID *)customerID;

-(StoredValuesServiceRequest *)registerDevice:(NSString *)regToken;
-(StoredValuesServiceRequest *)searchServerUrl:(NSString *)serverCode;


-(StoredValuesServiceRequest *)getSettings;

-(StoredValuesServiceRequest *)getStoreCreditBalance:(BPUUID *)customerId;
-(StoredValuesServiceRequest *)discardStoreCreditTransaction:(NSString *)transactionId;


+ (StoredValuesService *)sharedInstance;
+(void)reset;

@end
