//
//  GoogleCloudStorageManager.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/11/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GoogleCloudStorageManager;

@protocol GoogleCloudStorageManagerDelegate

-(void)manager:(GoogleCloudStorageManager *)manager uploadProgress:(float)progress;
-(void)manager:(GoogleCloudStorageManager *)manager completedUploadWithError:(NSError *)error;

@end

@interface GoogleCloudStorageManager : NSObject

@property(nonatomic, assign) id<GoogleCloudStorageManagerDelegate> delegate;
@property(nonatomic, assign) BOOL logEnable;

- (id)initWithDelegate:(id<GoogleCloudStorageManagerDelegate>)delegate;
- (void)uploadFile:(NSString *)filePath;
- (void)cancelUpload;

- (NSString *)createJSONWebToken;
- (NSURLRequest *)getAccessTokenRequest:(NSString *)jwt;

@end
