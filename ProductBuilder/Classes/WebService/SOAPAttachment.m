//
//  SOAPAttachment.m
//  ProductBuilder
//
//  Created by valera on 12/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SOAPAttachment.h"
#import "USAdditions.h"

@implementation SOAPAttachment
@synthesize contentId, path, name;

-(void)dealloc{
    self.contentId = nil;
    self.path = nil;
    self.name = nil;
    [super dealloc];
}

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix
{
    xmlNodePtr node = xmlNewDocNode(doc, NULL, [makeElementName(elName,elNSPrefix) xmlString], NULL);
    xmlNodePtr nodeInclude = xmlNewNode(NULL, (const xmlChar*)[makeElementName(@"Include", @"xop") UTF8String]);
    xmlNewNs(nodeInclude, (const xmlChar*)"http://www.w3.org/2004/08/xop/include", (const xmlChar*)"xop");
    xmlSetProp(nodeInclude, (const xmlChar*)"href", (const xmlChar*)[[NSString stringWithFormat:@"cid:%@", contentId] UTF8String]);
    xmlAddChild(node, nodeInclude);
    //exclude white spaces
    xmlNodePtr nodeText = xmlNewText(NULL);
    xmlAddChild(node, nodeText);
    //xmlNodePtr node = xmlNewDocNode(doc, NULL, [makeElementName(elName,elNSPrefix) xmlString], [makeElementName(contentId,@"cid") xmlString]);
    
	return node;
}

@end
