//
//  WebServiceResponse.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/20/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import "EGODatabaseResult.h"
#import "RTProxyStream.h"

@class XmlElement;

@interface XmlValue : NSObject {
@private
    NSString* name;
    NSMutableString* value;
    XmlElement* parent;
}
-(id)initWithName:(NSString*)aName value:(NSString*)aValue;
-(id)initWithXmlNodePtr:(xmlNodePtr)node;
-(id)initWithxmlAttrPtr:(xmlAttrPtr)attr;
@property (nonatomic,retain)NSString* name;
@property (nonatomic,retain)NSMutableString* value;
@property(nonatomic, assign)XmlElement* parent;
@end

@interface XmlElement : XmlValue {
@private
    NSMutableArray* children;
    NSMutableDictionary* attributes;
}
@property (nonatomic,retain)NSArray*children;
@property (nonatomic,retain)NSArray*attributes;
-(void)addChild:(XmlElement*)element;
-(void)addAttribute:(XmlValue*)attribute;
//returns first children by path
-(XmlElement*)childWithNameByPath:(NSString*)path;
-(XmlElement*)childWithName:(NSString*)name;
-(NSArray*)childrenWithNameByPath:(NSString*)name;
-(NSArray*)childrenWithName:(NSString*)name;
-(XmlElement*)attributeWithName:(NSString*)name;
-(EGODatabaseRow*)childrenToEGODatabaseRow;
-(EGODatabaseResult*)childrenToEGODatabaseResult:(NSString*)name;
-(EGODatabaseResult*)childrenToEGODatabaseResultByPath:(NSString*)path;
-(NSDictionary*)childrenToDictionary;
-(NSDictionary*)childrenToNoRetainedDictionary;
-(NSArray*)childrenValuesToArray;

@end

@class WebServiceOperation;
@class WebServiceResponse;

@protocol WebServiceOperationDelegate <NSObject>

- (void) operation:(WebServiceOperation *)operation completedWithResponse:(WebServiceResponse *)response;
@optional 
-(void)operation:(WebServiceOperation *)operation didReceiveRecord:(id)object;
-(void)operation:(WebServiceOperation *)operation didReceiveBinaryStream:(NSInputStream*)stream;
-(void)operation:(WebServiceOperation *)operation completedUploadWithProgress:(float)progress;
@end

@interface WebServiceResponse : NSObject {
	NSArray *headers;
	NSArray *bodyParts;
	NSError *error;
	NSObject *result;
}

@property (nonatomic,retain) NSArray *headers;
@property (nonatomic,retain) NSArray *bodyParts;
@property (nonatomic,retain) NSError *error;
@property (nonatomic,retain) NSObject *result;

@end

