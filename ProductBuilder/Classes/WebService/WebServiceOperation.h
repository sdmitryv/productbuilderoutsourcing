//
//  WebServiceOperation.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/17/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceResponse.h"
#import <libxml/parser.h>
#import <libxml/xmlstring.h>
#import "SOAPResponseParser.h"
#import "XopXmlResponseParser.h"

extern NSInteger const kSOAPFaultError;

@class WebServiceParameters;

@interface WebServiceOperation : NSObject<SOAPResponseParserDelegate, XopXmlResponseParserDelegate, NSURLSessionDataDelegate> {
	WebServiceResponse *response;
	id<WebServiceOperationDelegate> delegate;
	NSMutableData *responseData;
	NSString *name;
	WebServiceParameters *parameters;
    WebServiceParameters *headerParameters;
    NSString* resultName;
    NSString* responseName;
    BOOL responseReceived;
    Class returnClass;
    
    BOOL useSaxParser;
    SOAPResponseParser* soapParser;
    XopXmlResponseParser* xopParser;
    NSString* saxParserPathToRecord;
    
    NSURL *address;
	NSTimeInterval defaultTimeout;
	NSMutableArray *cookies;
	BOOL logXMLInOut;
	NSString *authUsername;
	NSString *authPassword;
    
	NSString *nameSpace;
	NSString *urlAddress;
	NSString *methodName;
	NSString *soapActionURL;
    BOOL    responseIsXop;
    NSURLSessionTask * _sessionTask;
    NSString *_requestBody;
}

@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, retain) NSMutableArray *cookies;
@property (nonatomic, retain) NSString *authUsername;
@property (nonatomic, retain) NSString *authPassword;

@property(nonatomic, copy) NSString *nameSpace;
@property(nonatomic, copy) NSString *methodName;
@property(nonatomic, copy) NSString *soapActionURL;

@property (readonly) WebServiceResponse *response;
@property (nonatomic, assign) id<WebServiceOperationDelegate> delegate;
@property (atomic, readonly) NSMutableData *responseData;
@property (atomic, readonly) NSURLSessionTask * sessionTask;
@property (atomic, retain) NSString *name;
@property (retain) WebServiceParameters * parameters;
@property (retain) WebServiceParameters * headerParameters;
@property (nonatomic, readonly) BOOL responseReceived;
@property (nonatomic, assign) Class returnClass;
@property (nonatomic, assign) BOOL useSaxParser;
@property (nonatomic, retain)NSString* saxParserPathToRecord;
@property (nonatomic, assign)BOOL useMTOM;
@property (nonatomic, assign) BOOL sendErrorLog;

- (id)initWithDelegate:(id<WebServiceOperationDelegate>)aDelegate parameters:(WebServiceParameters *)aParameters;
- (id)initWithDelegate:(id<WebServiceOperationDelegate>)aDelegate parameters:(WebServiceParameters *)aParameters headerParameters:(WebServiceParameters *)aHeaderParameters;
-(void)start;
-(void)cancel;
- (void)addCookie:(NSHTTPCookie *)toAdd;


-(NSString*)getRequestString;

@end
