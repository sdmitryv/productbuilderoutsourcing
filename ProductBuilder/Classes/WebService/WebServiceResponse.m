//
//  WebServiceResponse.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/20/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "WebServiceResponse.h"
#import <libxml/tree.h>
#import "EGODatabaseRow.h"
#import "DBAdditions.h"
#import "NSDate+ISO8601Parsing.h"
#import "XmlConvert.h"

@interface EGODatabaseRowXml : EGODatabaseRow{
    NSMutableArray* columns;
    NSMutableOrderedSet* columnsCI;
}
-(void)addXmlElement:(XmlElement*)element;
@end

@implementation EGODatabaseRowXml

-(id)initWithDatabaseResult:(EGODatabaseResult *)aResult{
    if (self = [super initWithDatabaseResult:aResult]){
        columns = [[NSMutableArray alloc]init];
        columnsCI = [[NSMutableOrderedSet alloc]init];
    }
    return self;
}

- (NSInteger)columnIndexForName:(NSString*)columnName {
	//return [columns indexOfCaseInsensitiveString:columnName];
    return [columnsCI indexOfObject:[columnName lowercaseString]];
}

- (NSDate*)dateForColumnIndex:(NSInteger)columnIndex {
	id value = (self.columnData)[columnIndex];
	if (ISNULL(value)) return nil;
    NSTimeInterval timeInterval;
    NSDate* result = nil;
    if ([NSDate parseISO8601String:value inTimeIntervalSince1970:&timeInterval]){
        result = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    }
    return result;
}

- (NSData*)dataForColumnIndex:(NSInteger)columnIndex {
	id value = (self.columnData)[columnIndex];
	return ISNULL(value) ? nil : [NSData dataWithBase64EncodedString:[value description]];
}

-(void)addXmlElement:(XmlElement*)element{
    NSUInteger idx = [columns indexOfObject:element.name];
    if (idx!=NSNotFound){
        [self.columnData removeObjectAtIndex:idx];
        [self.columnData insertObject:element.name atIndex:idx];
    }
    else{
        [columns addObject:element.name];
        [columnsCI addObject:[element.name lowercaseString]];
        [self.columnData addObjectNilSafe:element.value];
    }
}

-(void)dealloc{
    [columns release];
    [columnsCI release];
    [super dealloc];
}

-(NSString*)description{
    NSMutableString* descr = [NSMutableString string];
    for (NSInteger i = 0; i < columns.count; i++){
        id value = (self.columnData)[i];
        [descr appendFormat:@"%@ = %@\n", columns[i], value];
    }
    return descr;
}
@end


@implementation XmlValue

@synthesize name,value, parent;

-(id)initWithName:(NSString*)aName value:(NSString*)aValue{
    if ((self=[super init])){
        self.name = aName;
        //self.value = aValue;
        [value release];
        if (aValue){
            value = [[NSMutableString alloc]initWithString:aValue];
        }
    }
    return self;
}

-(id)initWithXmlNodePtr:(xmlNodePtr)node{
    //NSString* aName = [[NSString alloc]initWithUTF8String: (char *)node->name];
    NSString* aName = [XmlConvert decodeNameFromUTF8String:(const char*)node->name];
    if ((self=[self initWithName:aName value:[NSString deserializeNode:node]])){
        
    }
    //[aName release];
    return self;
}

-(id)initWithxmlAttrPtr:(xmlAttrPtr)attr{
    NSString* aName = [[NSString alloc]initWithUTF8String: (char *)attr->name];
    if ((self=[self initWithName:aName value:[NSString deserializeAttribute:attr]])){
        
    }
    [aName release];
    return self;
}

-(void)dealloc{
    [name release];
    [value release];
    [super dealloc];
}

-(NSString*)description{
    return [NSString stringWithFormat:@"'%@'='%@'",name,value];
}

@end

@implementation XmlElement

-(id)initWithXmlNodePtr:(xmlNodePtr)node {
    
    NSString* aName = [[NSString alloc]initWithUTF8String: (char *)node->name];
    if ((self=[self initWithName:aName value:[NSString deserializeNode:node]])){
        
        for( xmlAttr* attribute = node->properties; attribute && attribute->children; attribute = attribute->next ){
            XmlValue* attrValue = [[XmlValue alloc]initWithxmlAttrPtr:attribute];
            [self addAttribute:attrValue];
            [attrValue release];
        }
        for( xmlNodePtr nodeChild = node->children ; nodeChild != NULL ; nodeChild = nodeChild->next ) {
            if (!nodeChild || nodeChild->type != XML_ELEMENT_NODE){
                continue;
            }
            XmlElement* childElement = [[XmlElement alloc]initWithXmlNodePtr:nodeChild];
            [self addChild:childElement];
            [childElement release];
        }
    }
    [aName release];
    return self;
}

-(void)dealloc{
    [children release];
    [attributes release];
    [super dealloc];
}

-(NSArray*)attributes{
    return [attributes allValues];
}

-(NSArray*)children{
    return children;
}

-(void)setChildren:(NSArray *)aChildren{
    [children release];
    children = nil;
    for(XmlElement* element in aChildren){
        [self addChild:element];
    }
}

-(void)setAttributes:(NSArray *)aAttributes{
    [attributes release];
    attributes = nil;
    for(XmlElement* element in attributes){
        [self addAttribute:element];
    }
}

-(void)addChild:(XmlElement *)element{
    if (!children){
        children = [[NSMutableArray alloc]init];
    }
    element.parent = self;
    [children addObject:element];
}

-(void)addAttribute:(XmlValue *)attribute{
    if (!attributes){
        attributes = [[NSMutableDictionary alloc]init];
    }
    attribute.parent = self;
    attributes[attribute.name] = attribute;
}

-(XmlElement*)childWithName:(NSString*)name{
    if (!name || !children.count) return nil;
    for (XmlElement* element in children){
        if ([element.name isEqualToString:name]){
            return element;
        }
    }
    return nil;
}

-(XmlElement*)childWithNameByPath:(NSString*)path{
    if (!path || !self.children.count) return nil;
    NSArray* arrayChild = [path componentsSeparatedByString:@"\\"];
    if (!arrayChild.count){
        return [self childWithName:path];
    }
    XmlElement* current = self;
    for (NSString* name in arrayChild){
        current = [current childWithName:name];
        if (!current) return nil;
    }
    return current;
}

-(NSArray*)childrenWithName:(NSString*)name{
    if (!name || !children.count) return nil;
    NSMutableArray* childrenList = nil;
    for (XmlElement* element in children){
        if ([element.name isEqualToString:name]){
            if (!childrenList){
                childrenList = [[[NSMutableArray alloc]init]autorelease];
            }
            [childrenList addObject:element];
        }
    }
    return childrenList;
}

-(NSArray*)childrenWithNameByPath:(NSString*)path{
    if (!path || !self.children.count) return nil;
    NSArray* arrayChild = [path componentsSeparatedByString:@"\\"];
    if (!arrayChild.count){
        return [self childrenWithName:path];
    }
    NSArray* currentChildrenList = nil;
    XmlElement* current = self;
    for (NSString* name in arrayChild){
        currentChildrenList = [current childrenWithName:name];
        if (!currentChildrenList || !currentChildrenList.count) return nil;
        current = currentChildrenList[0];
    }
    return currentChildrenList;
}


-(XmlElement*)attributeWithName:(NSString*)name{
    return attributes[name];
}

-(NSDictionary*)childrenToDictionary{
    if (!self.children || !self.children.count) return nil;
    CFMutableDictionaryRef dict = CFDictionaryCreateMutable(NULL, self.children.count, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    for (XmlElement* element in self.children){
        if (element.children.count || !element.name || !element.value) continue;
        CFDictionarySetValue(dict, element.name, element.value);
    }
    return [(NSDictionary*)dict autorelease];
}

-(NSDictionary*)childrenToNoRetainedDictionary{
    if (!self.children || !self.children.count) return nil;
    CFMutableDictionaryRef dict = CFDictionaryCreateMutable(NULL, self.children.count, &kCFTypeDictionaryKeyCallBacks, NULL);
    for (XmlElement* element in self.children){
        if (element.children.count || !element.name || !element.value) continue;
        CFDictionarySetValue(dict, element.name, element.value);
    }
    return [(NSDictionary*)dict autorelease];
}

-(NSArray*)childrenValuesToArray{
    if (!self.children || !self.children.count) return nil;
    NSMutableArray* arr = [[[NSMutableArray alloc]init]autorelease];
    for (XmlElement* element in self.children){
        if (element.children.count) continue;
        [arr addObjectNilSafe:element.value];
    }
    return arr;
}

-(EGODatabaseRow*)childrenToEGODatabaseRow{
    EGODatabaseRowXml* row = [[[EGODatabaseRowXml alloc]initWithDatabaseResult:nil]autorelease];
    for (XmlElement* element in self.children){
        if (element.children.count) continue;
        [row addXmlElement:element];
    }
    return row;
}

-(EGODatabaseResult*)childrenToEGODatabaseResult:(NSString*)name{
    EGODatabaseResult* result = [[[EGODatabaseResult alloc]init]autorelease];
    for (XmlElement* element in [self childrenWithName:name]){
        //columns names and column values should have the same order
        EGODatabaseRowXml* row = [[EGODatabaseRowXml alloc]initWithDatabaseResult:result];
        for (XmlElement* child in element.children){
            if (child.children.count) continue;
            [row addXmlElement:child];
        }
        [result addRow:row];
        [row release];
    }
    return result;
}

-(EGODatabaseResult*)childrenToEGODatabaseResultByPath:(NSString*)path{
    if (!path || !self.children.count) return nil;
    NSArray* arrayChild = [path componentsSeparatedByString:@"\\"];
    if (!arrayChild.count){
        return [self childrenToEGODatabaseResult:path];
    }
    XmlElement* current = self;
    for(NSUInteger i = 0; i < arrayChild.count - 1; i++){
        NSString* name = arrayChild[i];
        NSArray* currentChildrenList = [current childrenWithName:name];
        if (!currentChildrenList || !currentChildrenList.count) return nil;
        current = currentChildrenList[0];
    }
    return [current childrenToEGODatabaseResult:[arrayChild lastObject]];
}

@end

@implementation WebServiceResponse

@synthesize headers;
@synthesize bodyParts;
@synthesize error;
@synthesize result;

- (id)init
{
	if((self = [super init])) {
		headers = nil;
		bodyParts = nil;
		error = nil;
		result = nil;
	}
	
	return self;
}

-(void)dealloc {
    self.headers = nil;
    self.bodyParts = nil;
    self.error = nil;
	self.result = nil;
    [super dealloc];
}

@end
