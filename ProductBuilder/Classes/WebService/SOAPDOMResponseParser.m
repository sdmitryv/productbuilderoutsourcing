//
//  SOAPDOMResponseParser.m
//  ProductBuilder
//
//  Created by valera on 6/4/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SOAPDOMResponseParser.h"
#import "USAdditions.h"

static const xmlChar *kBodyElementName = (xmlChar *)"Body";
static uint kBodyElementNameLength = 4;
static const xmlChar *kFaultElementName = (xmlChar *)"Fault";
static uint kFaultElementNameLength = 5;

@interface SOAPDOMResponseParser()

@property (nonatomic, retain)NSString* responseName;
@property (nonatomic, assign) Class returnClass;
@property (nonatomic, retain)NSString* resultName;
@end

@implementation SOAPDOMResponseParser

-(id)initWithResponseName:(NSString*)responseName resultName:(NSString*)resultName returnClass:(Class) returnClass{
    if ((self=[super init])){
        self.responseName = responseName;
        self.resultName = resultName;
        self.returnClass = returnClass;
    }
    return self;
}

-(void)dealloc{
    self.responseName = nil;
    self.resultName = nil;
    [super dealloc];
}

-(BOOL)parse:(NSData*)responseData response:(WebServiceResponse*)response{
    BOOL result = FALSE;
    xmlInitParser();
    xmlDocPtr doc = xmlParseMemory([responseData bytes], (int)[responseData length]);
    
    if (!doc) {
        NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:@"Errors while parsing returned XML",NSLocalizedDescriptionKey, nil];
        response.error = [NSError errorWithDomain:@"iPadServiceResponse" code:1 userInfo:userInfo];
        [userInfo release];
        return FALSE;
    }
    else {
        xmlNodePtr cur = xmlDocGetRootElement(doc);
        cur = cur->children;
        
        for( ; cur != NULL ; cur = cur->next) {
            if(cur->type == XML_ELEMENT_NODE) {
                
                if(!xmlStrncmp(cur->name, kBodyElementName, kBodyElementNameLength)) {
                    //if(xmlStrEqual(cur->name, kBodyElementName)) {
                    NSMutableArray *aResponseBodyParts = [[NSMutableArray alloc]init];
                    
                    xmlNodePtr bodyNode;
                    for(bodyNode=cur->children ; bodyNode != NULL ; bodyNode = bodyNode->next) {
                        if(cur->type == XML_ELEMENT_NODE) {
                            if(xmlStrEqual(bodyNode->name, (const xmlChar *)[self.responseName UTF8String])) {
                                [self deserializeElementsFromNode:bodyNode response:response];
                            }
                            else if (xmlStrEqual(bodyNode->ns->prefix, cur->ns->prefix) &&
                                     !xmlStrncmp(bodyNode->name, kFaultElementName, kFaultElementNameLength)){
                                //xmlStrEqual(bodyNode->name, kFaultElementName)){
                                SOAPFault *bodyObject = [SOAPFault deserializeNode:bodyNode];
                                //NSAssert1(bodyObject != nil, @"Errors while parsing body %s", bodyNode->name);
                                if (bodyObject != nil) [aResponseBodyParts addObject:bodyObject];
                                result = TRUE;
                            }
                        }
                    }
                    response.bodyParts = aResponseBodyParts;
                    [aResponseBodyParts release];
                    break;
                }
            }
        }
        xmlFreeDoc(doc);
    }
    return result;
}

- (void)deserializeElementsFromNode:(xmlNodePtr)node response:(WebServiceResponse*)response {
    @autoreleasepool {
        for( node = node->children ; node != NULL ; node = node->next ) {
            if(node->type == XML_ELEMENT_NODE) {
                if(!self.resultName || xmlStrEqual(node->name, (const xmlChar *)[self.resultName UTF8String])) {
                    if (self.returnClass != nil) {
                        response.result = [self.returnClass deserializeNode:node];
                    }
                    else {
                        XmlElement* root = [[XmlElement alloc]initWithXmlNodePtr:node];
                        response.result = root;
                        [root release];
                    }
                    break;
                }
            }
        }
    }
}

@end
