//
//  SOAPAttachment.h
//  ProductBuilder
//
//  Created by valera on 12/26/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <libxml/tree.h>

@interface SOAPAttachment : NSObject

@property (nonatomic, retain)NSString* contentId;
@property (nonatomic,retain)NSString* path;
@property (nonatomic,retain)NSString* name;
- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix;
@end
