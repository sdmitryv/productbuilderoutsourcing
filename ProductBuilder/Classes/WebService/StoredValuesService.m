//
//  StoredValuesService.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/14/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "StoredValuesService.h"
#import "NSDate+System.h"
#import "Workstation.h"
#import "Location.h"
#import "DecimalHelper.h"
#import "NSDictionary+CaseInsensitive.h"
#import "AppSettingManager.h"
#import "UrlDictionaryServiceRequest.h"
#import "SecurityManager.h"
#import "NSDate+ISO8601Unparsing.h"
#import "AppParameterManager.h"

// Common parameters

NSString *const TRANSACTIONAMOUNT = @"amount";
NSString *const TRANSACTIONOVERDRAFT = @"allowOverdraft";

// Gift card API function parameters

NSString *const GCTRANSACTIONID = @"transactionID";
NSString *const GIFTCARDID = @"giftCardNumber";
NSString *const LOCATIONNAME = @"locationName";
NSString *const LOCATIONID = @"locationID";
NSString *const LOCATIONEXTERNALID = @"locationExternalID";
NSString *const WORKSTATIONID = @"workstationId";
NSString *const POSID = @"POSID";
NSString *const REFERENCENUM = @"referenceNum";
NSString *const RECEIPTID = @"receiptID";
NSString *const TRANSACTIONTYPE = @"transactionType";
NSString *const TRANSACTIONSOURCE = @"transactionSource";
NSString *const FETCHNUMBER = @"pageSize";
NSString *const EMPLOYEEID = @"employeeID";
NSString *const EMPLOYEELOGINNAME = @"employeeLoginName";
NSString *const DEVICEID = @"deviceId";
NSString *const DEVICEGUID = @"deviceGUID";
NSString *const DTRNUMBER = @"dTrNumber";
NSString *const RECEIPTPAYMENTID = @"receiptLineId";
NSString *const RECEIPTLINETYPECODE = @"lineType";
NSString *const SVTRANSACTIONID = @"svTransactionId";
NSString *const OFFSET = @"offset";
NSString *const LIMIT = @"limit";
NSString *const PAGESIZE = @"pageSize";
NSString *const OVERALLAMOUNTREQUIREDROPERTY = @"overallAmountRequired";
NSString *const SVPROGRAMTYPE = @"svProgramType";
//NSString * const SVPROGRAMGUID = @"svProgramGuid";
NSString *const CUSTOMERID = @"customerID";
NSString *const CUSTOMEREMAIL = @"customerEmail";
NSString *const PROGRAMTYPEVALUE = @"housecharge";
//NSString * const PROGRAMTOKENTYPEVALUE = @"tokens";
//NSString * const CUSTOMDATA1 = @"customData1";
NSString *const CUSTOMDATA2 = @"customData2";
NSString *const CUSTOMDATA3 = @"customData3";
NSString *const CUSTOMDATA4 = @"customData4";
NSString *const CUSTOMDATA5 = @"customData5";
NSString *const LOCALTRANSACTIONDATE = @"localTransactionDate";
NSString *const LOCALTRANSACTIONTIME = @"localTransactionTime";
NSString *const TRANSACTIONLINETYPE = @"transactionLineType";
NSString *const LPREWARDTRANSACTIONID = @"lpRewardTransactionID";


static StoredValuesService *storedValuesService;

@implementation StoredValuesService

- (id)init {
    SettingManager *settingManager = [SettingManager instance];
    return [self initWithUsername:[settingManager svsAccountUserName] password:[settingManager svsAccountPassword]];
}

- (id)initWithUsername:(NSString *)name password:(NSString *)password {
    self = [super init];
    if (self != nil) {
        NSURL *svsServiceURL = [NSURL URLWithString:[[SettingManager instance] svs2ServiceUrl]];
//        NSURL * svsServiceURL = [NSURL URLWithString:@"https://fbposapi-dot-toi-proving-ground.appspot.com"];

        loyaltyPointsServiceURL = [[NSURL alloc] initWithString:@"/lrppoints/loyaltypoints" relativeToURL:svsServiceURL];
        newloyaltyPointsServiceURL = [[NSURL alloc] initWithString:@"/lrpoints" relativeToURL:svsServiceURL];
        houseChargeServiceURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/houseaccounts"] retain];
        //frequentBuyerServiceURL = [[NSURL alloc] initWithScheme:[svsServiceURL scheme] host:[svsServiceURL host] path:@"/frequentbuyers"];

        _devicesAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/devices"] retain];
        _customersAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/customers"] retain];
        _giftCardsAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/giftcards"] retain];
        _loyaltyAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/loyalty"] retain];
        _settingsAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/settings"] retain];
        _couponsAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/coupons"] retain];
        _storeCreditAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/storecredits"] retain];
        _fbAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/loyalty"] retain];
        _receiptAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/receipts"] retain];
        _receiptOnlineAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/receipts_online"] retain];
        _tokenAreaURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/customertoken"] retain];
        _customerCardOnFileURL = [[svsServiceURL URLByAppendingPathComponent:@"twrposapi/customercardonfile"] retain];
    }
    return self;
}


- (id)initWithForGetUrl {
    self = [super init];
    if (self != nil) {

        _udsAreaURL = [[NSURL alloc] initWithString:@"https://url-dictionary-service.appspot.com/api"];
    }
    return self;
}

- (void)dealloc {
    [loyaltyPointsServiceURL release];
    [houseChargeServiceURL release];
    //[frequentBuyerServiceURL release];
    [newloyaltyPointsServiceURL release];

    [_devicesAreaURL release];
    [_customersAreaURL release];
    [_giftCardsAreaURL release];
    [_loyaltyAreaURL release];
    [_settingsAreaURL release];
    [_udsAreaURL release];
    [_couponsAreaURL release];
    [_storeCreditAreaURL release];
    [_tokenAreaURL release];
    [_fbAreaURL release];
    [_receiptAreaURL release];
    [_receiptOnlineAreaURL release];
    [super dealloc];
}

+ (instancetype)sharedInstance {
    if (storedValuesService == nil) {
        storedValuesService = [[self.class alloc] init];
    }
    return storedValuesService;
}

+ (void)reset {
    if (storedValuesService != nil) {
        [storedValuesService release];
        storedValuesService = nil;
    }
}

#pragma mark - Gift Cards functions



+ (NSString *)getLocalTransactionDatePresentation:(NSDate *)date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    NSString *dateResult = [formatter stringFromDate:[date toUTCTimeZone]];
    [formatter release];

    return dateResult;
}

#pragma mark - Customer methods

- (StoredValuesServiceRequest *)validateEmail:(NSString *)email {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"email"] = email;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:VALIDATEEMAIL url:[_customersAreaURL URLByAppendingPathComponent:@"validateemail"] params:params] autorelease];
    [params release];
    return request;
}

- (StoredValuesServiceRequest *)checkPassword:(NSString *)password forCustomer:(BPUUID *)customerId {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"customerPassword"] = password;
    params[@"customerID"] = customerId;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:CHECKCUSTOMERPASSWORD url:[_customersAreaURL URLByAppendingPathComponent:@"checkpass"] params:params] autorelease];
    [params release];
    return request;
}


#pragma mark - Frequen Buyer Program requests

+ (NSString *)timezoneOffset {
    
    NSDateFormatter *localTimeZoneFormatter = [[NSDateFormatter alloc] init];
    localTimeZoneFormatter.timeZone = [NSTimeZone localTimeZone];
    localTimeZoneFormatter.dateFormat = @"Z";
    NSString *timeZoneOffset = [localTimeZoneFormatter stringFromDate:[NSDate date]];
    [localTimeZoneFormatter release];
    return timeZoneOffset;
}

+ (NSDictionary *)svsContext {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"employeeID"] = SecurityManager.currentEmployee.id;
    params[@"employeeName"] = SecurityManager.currentEmployee.loginName;
    params[@"timezoneOffset"] = self.class.timezoneOffset;
    params[@"POSID"] = [AppSettingManager instance].iPadId;
    params[@"locationID"] = (Location.localLocation.id);
    return [params autorelease];
}

- (StoredValuesServiceRequest *)getCustomerFrequentBuyerPrograms:(BPUUID *)customerID {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[CUSTOMERID] = customerID;
    params[@"POSContext"] = self.class.svsContext;
    params[DEVICEID] = [AppSettingManager instance].iPadId;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:GETCUSTOMERLOYALTYPROGRAMS url:[_customersAreaURL URLByAppendingPathComponent:@"getloyaltyprograms"] params:params] autorelease];
    [params release];
    return request;
}

#pragma mark - Devices Area Methods

- (StoredValuesServiceRequest *)registerDevice:(NSString *)regToken {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"regToken"] = regToken;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:REGISTERDEVICE
                                                                                        url:[_devicesAreaURL URLByAppendingPathComponent:@"register"]
                                                                                     params:params] autorelease];
    [params release];
    return request;
}

- (StoredValuesServiceRequest *)searchServerUrl:(NSString *)serverCode {

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"label"] = serverCode;
    UrlDictionaryServiceRequest *request = [[[UrlDictionaryServiceRequest alloc] initWithType:0
                                                                                          url:[_udsAreaURL URLByAppendingPathComponent:@"get_url"]
                                                                                       params:params] autorelease];
    [params release];
    return request;
}

#pragma mark coupons

- (StoredValuesServiceRequest *)fetchActiveCouponsNumberForCustomer:(NSString *)customerId {
    
    if (!customerId) return nil;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"customerID"] = customerId;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:COUPONSACTIVECUSTOMERCOUNT
                                                                                        url:[_couponsAreaURL URLByAppendingPathComponent:@"countActiveCustomerCoupons"]
                                                                                     params:params] autorelease];
    [params release];
    return request;
}

#pragma mark - Settings requests

- (StoredValuesServiceRequest *)getSettings {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:GETSETTINGS url:[_settingsAreaURL URLByAppendingPathComponent:@"get"] params:params] autorelease];
    [params release];
    return request;
}


#pragma mark - StoreCredit requests

- (StoredValuesServiceRequest *)getStoreCreditBalance:(BPUUID *)customerId {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"customerID"] = customerId;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:GETSCBALANCE url:[_storeCreditAreaURL URLByAppendingPathComponent:@"getbalance"] params:params] autorelease];
    [params release];
    return request;
}

- (StoredValuesServiceRequest *)fetchStoreCreditTransactions:(BPUUID *)customerId {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"customerID"] = customerId;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:FETCHSCTRSANSACTION url:[_storeCreditAreaURL URLByAppendingPathComponent:@"listtxns"] params:params] autorelease];
    [params release];
    return request;
}

- (StoredValuesServiceRequest *)discardStoreCreditTransaction:(NSString *)transactionId {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"transactionKey"] = transactionId;
    params[@"POSContext"] = self.class.svsContext;
    StoredValuesServiceRequest *request = [[[StoredValuesServiceRequest alloc] initWithType:DISCARDSCTRANSACTION url:AppParameterManager.instance.isSvsDisabled ? nil : [_storeCreditAreaURL URLByAppendingPathComponent:@"discard"] params:params] autorelease];
    [params release];
    return request;
}
@end
