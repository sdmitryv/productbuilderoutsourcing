//
//  BackupManager.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/25/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import "BackupManager.h"
#import "SyncOperation.h"
#import "BackupDbOperation.h"
#import "ModalAlert.h"
#import "ActionsHelper.h"

static BackupManager * backupManager;

@interface BackupManager() {
    SyncOperationQueue * _backupQueue;
    BackupDbOperation * _backupDbOperation;
}

@end

@implementation BackupManager

#pragma mark - Life Cycle

- (id)init {
    
    self = [super init];
    if(self != nil)
    {
        _backupQueue = [[SyncOperationQueue alloc] init];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncQueueDidErrorOccur:)
                                                     name:SyncQueueDidErrorOccurNotification
                                                   object:_backupQueue];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncQueueDidStartOperation:)
                                                     name:SyncQueueDidStartOperationNotification
                                                   object:_backupQueue];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncQueueInProgress:)
                                                     name:SyncQueueInProgressNotification
                                                   object:_backupQueue];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncQueueDidChangeOperationTitle:)
                                                     name:SyncQueueDidChangeOperationTitleNotification
                                                   object:_backupQueue];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncQueueDidComplete:)
                                                     name:SyncQueueDidCompleteNotification
                                                   object:_backupQueue];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SyncQueueDidErrorOccurNotification           object:_backupQueue];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SyncQueueDidStartOperationNotification       object:_backupQueue];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SyncQueueInProgressNotification              object:_backupQueue];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SyncQueueDidChangeOperationTitleNotification object:_backupQueue];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SyncQueueDidCompleteNotification             object:_backupQueue];
    
    [_backupDbOperation release];
    [_backupQueue release];
    [_message release];
    [super dealloc];
}

+ (instancetype)sharedInstance {
    if (backupManager == nil) {
        backupManager = [[self.class alloc] init];
    }
    return backupManager;
}

#pragma mark - Public Methods

- (void)backupDatabase {
    _backupDbOperation = [[BackupDbOperation alloc] init];
    [_backupQueue addOperation:_backupDbOperation];
}

- (void)cancel {
    [_backupQueue cancelAllOperations];
    [_backupQueue waitUntilAllOperationsAreFinished];
}

#pragma mark - Private Methods

-(void)postNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:SyncQueueBackupOperationStateChanged object:self userInfo:nil];
}

#pragma mark - Properties

-(BOOL)isBusy {
    return _backupQueue.operationCount > 0;
}

#pragma mark - Backup queue events

-(void)syncQueueDidChangeOperationTitle:(NSNotification *)notification {
    SyncOperationQueue* queue = [notification object];
    if (!queue) return;
    NSDictionary* userInfo = [notification userInfo];
    SyncOperation* operation = userInfo[SyncQueueCurrentOperationKey];
    if (!operation) return;
    self.message = operation.title;
    [self postNotification];
}

- (void) syncQueueDidErrorOccur:(NSNotification *) notification {
    NSDictionary* userInfo = [notification userInfo];
    SyncOperation* operation = userInfo[SyncQueueCurrentOperationKey];
    NSError* error = userInfo[SyncQueueErrorKey];
    if (error != nil) {
        // 404 - not found; 503 - service unavailable; -1009 - NSURLErrorNotConnectedToInternet
        if (error.code == NSURLErrorTimedOut || error.code == 404 || error.code == 503 || error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotFindHost) {
            [ModalAlert show:NSLocalizedString(@"BACKUP_UNABLE_TO_SEND_TITLE", nil) message:NSLocalizedString(@"BACKUP_UNABLE_TO_SEND_TEXT", nil)];
        }
        else if (error.code != -999 && ![ActionsHelper checkSyncOperationErrorForLocationMismatch:operation]) {
            [ModalAlert showError:error];
        }
    }
    else{
        NSString* errorMessage = userInfo[SyncQueueErrorDescriptionKey];
        [ModalAlert showErrorMessage:errorMessage];
    }
    
    if (operation == _backupDbOperation) {
        [_backupDbOperation release];
        _backupDbOperation = nil;
    }
    
    [self postNotification];
}

- (void) syncQueueDidStartOperation:(NSNotification *) notification {
    SyncOperationQueue* queue = [notification object];
    if (!queue) return;
    NSDictionary* userInfo = [notification userInfo];
    SyncOperation* operation = userInfo[SyncQueueCurrentOperationKey];
    if (!operation) return;
    self.progress = 0.0f;
    [self postNotification];
}

- (void) syncQueueDidEndOperation:(NSNotification *) notification {
    SyncOperationQueue* queue = [notification object];
    if (!queue) return;
    NSDictionary* userInfo = [notification userInfo];
    SyncOperation* operation = userInfo[SyncQueueCurrentOperationKey];
    
    self.progress = 100.0;

    if (operation == _backupDbOperation) {
        [_backupDbOperation release];
        _backupDbOperation = nil;
    }

    [self postNotification];
}

- (void) syncQueueInProgress:(NSNotification *) notification {
    SyncOperationQueue* queue = [notification object];
    if (!queue) return;
    NSDictionary* userInfo = [notification userInfo];
    SyncOperation* operation = userInfo[SyncQueueCurrentOperationKey];
    if (!operation) return;
    self.progress = operation.progress * 100.0f;
    [self postNotification];
}

-(void)syncQueueDidComplete:(NSNotification *) notification {
    [self postNotification];
}

-(void)syncQueueDidCancel:(NSNotification *) notification {
    [self postNotification];
}

@end
