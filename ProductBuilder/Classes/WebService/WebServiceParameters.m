//
//  WebServiceParameters.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/20/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <libxml/parser.h>
#import "WebServiceParameters.h"
#import "USAdditions.h"

@implementation WebServiceParameters

- (id)init {
	if((self = [super init])) {
		parameters = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

- (void)dealloc {
	[parameters release];
	[super dealloc];
}

- (NSString *)nsPrefix {
	return @"tem";
}

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix {
    
	NSString *nodeName = nil;
	if(elNSPrefix && [elNSPrefix length]){
		nodeName = [NSString stringWithFormat:@"%@:%@", elNSPrefix, elName];
	}
	else{
		nodeName = [NSString stringWithFormat:@"%@:%@", self.nsPrefix, elName];
	}
	
	xmlNodePtr node = xmlNewDocNode(doc, NULL, [nodeName xmlString], NULL);
	
	[self addAttributesToNode:node];
	
	
	return node;
}

- (void)addAttributesToNode:(xmlNodePtr)node {
	NSString *key;
	for(key in parameters) {
		xmlAddChild(node, [parameters[key] xmlNodeForDoc:node->doc elementName:key elementNSPrefix:self.nsPrefix]);
	}
}

- (void)addElementsToNode:(xmlNodePtr)node {
}

- (void)addParameter:(NSString *)name value:(NSObject *)value {
    if (value && name)
        parameters[name] = value;
}

//- (NSDictionary *)attributes {
//	NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
//	return attributes;
//}

- (NSDictionary *)parameters {
	return parameters;
}


+ (WebServiceParameters *)deserializeNode:(xmlNodePtr)cur {
	WebServiceParameters *newObject = [[WebServiceParameters new] autorelease];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)deserializeAttributesFromNode:(xmlNodePtr)cur {
}

- (void)deserializeElementsFromNode:(xmlNodePtr)cur {
}

@end
