//
//  StoredValuesServiceParser.m
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "StoredValuesServiceParser.h"
#import "DataUtils.h"
#import "DecimalHelper.h"
#import "SettingManager.h"
#import "EmailValidation.h"
#import "NSDate+System.h"

@implementation StoredValuesServiceParser

+(NSDate *)dateFromString:(NSString *)string {
	NSDate* date = [DataUtils dateFromString:string];
    if (date == nil) {
        static dispatch_once_t pred;
        static NSDateFormatter * dateFormatter = nil;
        dispatch_once(&pred, ^{
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"MMM d, yyyy HH:mm:ss aaa";
        });
        date = [dateFormatter dateFromString:string];
    }
    return date;
}

+(BOOL)parseBoolValue:(id)value {
    BOOL result = NO;
    if (![value isKindOfClass:[NSNull class]]) {
        if ([value isKindOfClass:[NSNumber class]] ) {
            result = [value boolValue];
        }
        else {
            NSString *stringValue = [value lowercaseString];
            if ([stringValue hasPrefix:@"true"]) {
                result = YES;
            }
            else if ([stringValue hasPrefix:@"false"]) {
                result = NO;
            }
        }
    }
    return result;
}

+(NSError *)catchManualExceptions:(NSString *)text {
    NSError *error = nil;
    if ([text hasPrefix:@"Exception:"]) {
        NSArray * components = [text componentsSeparatedByString:@";"];
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: components[1]};
        int errorCode = [[components[0] stringByReplacingOccurrencesOfString:@"Exception:" withString:@""] intValue];
        error = [NSError errorWithDomain:@"StoredValuesService" code:errorCode userInfo:userInfo];
    }
    return error;
}


+(EmailValidation*)parseValidateEmailResponse:(NSDictionary*)dictionary {
    EmailValidation * result = [[[EmailValidation alloc] init] autorelease];
    result.isValid = [self parseBoolValue:dictionary[@"isValid"]];
    result.email = dictionary[@"email"];
    if (!result.isValid) {
        result.reason = dictionary[@"reason"];
        result.reasonCode = dictionary[@"reasonCode"];
        result.suggestion = dictionary[@"suggestion"];
        NSString * key = [NSString stringWithFormat:@"SVS_VALIDATE_EMAIL_REASON_CODE_%@", result.reasonCode];
        result.localizedDescription = NSLocalizedStringWithDefaultValue(key, nil, [NSBundle mainBundle], NSLocalizedString(@"VALIDATE_EMAIL_GENERIC_MESSAGE", nil), nil);
    }
    return result;
}




+(ReceiptSVSBalancesResponse*)parseReceiptBalances:(NSDictionary *)dictionary{
    
    if (![dictionary isKindOfClass:NSDictionary.class]){
        return nil;
    }
    
    ReceiptSVSBalancesResponse *receiptBalances = [[ReceiptSVSBalancesResponse alloc]init];
    EGODatabaseRowDictionary* row = [[EGODatabaseRowDictionary alloc]initWithDictionary:dictionary];
    receiptBalances.receiptId = [row UuidForColumn:@"salesReceiptID"];
    BPUUID* customerCandidate = nil;
    NSArray* lrp1Balances = dictionary[@"lrp1Balances"];
    NSArray* lrp2Balances = dictionary[@"lrp2Balances"];
    NSArray* fbBalances = dictionary[@"fbBalances"];
    NSArray* tokenBalances = dictionary[@"tokenBalances"];
    NSArray* haBalances = dictionary[@"haBalances"];
    if ([lrp1Balances isKindOfClass:NSArray.class] && lrp1Balances.count){
        NSDictionary* lrp1BalanceDict = lrp1Balances[0];
        if ([lrp1BalanceDict isKindOfClass:NSDictionary.class]){
            EGODatabaseRowDictionary* lrp1Balance = [[EGODatabaseRowDictionary alloc]initWithDictionary:lrp1BalanceDict];
            BPUUID* customer = [lrp1Balance UuidForColumn:@"customerID"];
            if (!customerCandidate){
                customerCandidate = customer;
            }
            else if (![customerCandidate isEqual:customer]){
                customer = nil;
            }
            if (customer){
                receiptBalances.lrp1Balance = [lrp1Balance decimalNumberForColumn:@"balance"];
                receiptBalances.lrp1EarnedPoints = [lrp1Balance decimalNumberForColumn:@"totalEarned"];
                receiptBalances.lrp1Threshold = [lrp1Balance decimalNumberForColumn:@"threshold"];
            }
            [lrp1Balance release];
        }
    }
    if ([lrp2Balances isKindOfClass:NSArray.class] && lrp2Balances.count){
        NSDictionary* lrp2BalanceDict = lrp2Balances[0];
        if ([lrp2BalanceDict isKindOfClass:NSDictionary.class]){
            EGODatabaseRowDictionary* lrp2Balance = [[EGODatabaseRowDictionary alloc]initWithDictionary:lrp2BalanceDict];
            BPUUID* customer = [lrp2Balance UuidForColumn:@"customerID"];
            if (!customerCandidate){
                customerCandidate = customer;
            }
            else if (![customerCandidate isEqual:customer]){
                customer = nil;
            }
            if (customer){
                receiptBalances.lrp2Balance = [lrp2Balance decimalNumberForColumn:@"balance"];
                receiptBalances.lrp2EarnedPoints = [lrp2Balance decimalNumberForColumn:@"totalEarned"];
                receiptBalances.lrp2Threshold = [lrp2Balance decimalNumberForColumn:@"threshold"];
            }
            [lrp2Balance release];
        }
    }
    if ([tokenBalances isKindOfClass:NSArray.class] && tokenBalances.count){
        NSDictionary* tokenBalanceDict = tokenBalances[0];
        if ([tokenBalanceDict isKindOfClass:NSDictionary.class]){
            EGODatabaseRowDictionary* tokenBalance = [[EGODatabaseRowDictionary alloc]initWithDictionary:tokenBalanceDict];
            BPUUID* customer = [tokenBalance UuidForColumn:@"customerID"];
            if (!customerCandidate){
                customerCandidate = customer;
            }
            else if (![customerCandidate isEqual:customer]){
                customer = nil;
            }
            if (customer){
                receiptBalances.tokenBalance = [tokenBalance decimalNumberForColumn:@"balance"];
                receiptBalances.tokenEarnedPoints = [tokenBalance decimalNumberForColumn:@"totalEarned"];
            }
            [tokenBalance release];
        }
    }
    if ([fbBalances isKindOfClass:NSArray.class] && fbBalances.count){
        NSMutableArray* receiptFbBalances = [[NSMutableArray alloc]init];
        for (NSDictionary* fbBalanceDict in fbBalances){
            if ([fbBalanceDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* fbBalance = [[EGODatabaseRowDictionary alloc]initWithDictionary:fbBalanceDict];
                BPUUID* customer = [fbBalance UuidForColumn:@"customerID"];
                if (!customerCandidate){
                    customerCandidate = customer;
                }
                else if (![customerCandidate isEqual:customer]){
                    customer = nil;
                }
                if (customer){
                    BPUUID* programId = [fbBalance UuidForColumn:@"programID"];
                    NSDecimalNumber* balanceAmount = [fbBalance decimalNumberForColumn:@"balance"];
                    if (programId && balanceAmount){
                        ReceiptSVSFrequentByuerBalancesResponse* receiptSVSFrequentByuerBalancesResponse = [[ReceiptSVSFrequentByuerBalancesResponse alloc]init];
                        receiptSVSFrequentByuerBalancesResponse.programId = programId;
                        receiptSVSFrequentByuerBalancesResponse.balanceAmount = balanceAmount;
                        receiptSVSFrequentByuerBalancesResponse.rate = [fbBalance decimalNumberForColumn:@"creditsForAward"];
                        receiptSVSFrequentByuerBalancesResponse.earnedAmount = [fbBalance decimalNumberForColumn:@"totalEarned"];
                        [receiptFbBalances addObject:receiptSVSFrequentByuerBalancesResponse];
                        [receiptSVSFrequentByuerBalancesResponse release];
                    }
                }
                [fbBalance release];
            }
        }
        receiptBalances.fbBalances = receiptFbBalances;
        [receiptFbBalances release];
    }
    
    if ([haBalances isKindOfClass:NSArray.class] && haBalances.count){
        NSMutableArray* receiptHABalances = [[NSMutableArray alloc]init];
        for (NSDictionary* haBalanceDict in haBalances){
            if ([haBalanceDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* haBalance = [[EGODatabaseRowDictionary alloc]initWithDictionary:haBalanceDict];
                BPUUID* customer = [haBalance UuidForColumn:@"customerID"];
                if (!customerCandidate){
                    customerCandidate = customer;
                }
                else if (![customerCandidate isEqual:customer]){
                    customer = nil;
                }
                if (customer){
                    NSString* invoiceId = [haBalance stringForColumn:@"invoiceId"];
                    NSDecimalNumber* balanceAmount = [haBalance decimalNumberForColumn:@"balance"];
                    if (invoiceId && balanceAmount){
                        ReceiptSVSHABalancesResponse* receiptSVSHABalancesResponse = [[ReceiptSVSHABalancesResponse alloc]init];
                        receiptSVSHABalancesResponse.invoiceId = invoiceId;
                        receiptSVSHABalancesResponse.balanceAmount = balanceAmount;
                        receiptSVSHABalancesResponse.earnedAmount = [haBalance decimalNumberForColumn:@"totalEarned"];
                        [receiptHABalances addObject:receiptSVSHABalancesResponse];
                        [receiptSVSHABalancesResponse release];
                    }
                }
                [haBalance release];
            }   
        }
        receiptBalances.haBalances = receiptHABalances;
        [receiptHABalances release];
    }
    
    [row release];
    return [receiptBalances autorelease];
    
}

+(NSString*)localizedErrorMessageByErrorCode:(SVSErrorCode)code{
    switch (code){
        case SVSErrorCodeInternalError:
            return NSLocalizedString(@"SVS_ERROR_CODE_INTERNAL_ERROR", nil);
        case SVSErrorCodeBadRegistrationToken:
            return NSLocalizedString(@"SVS_ERROR_CODE_BAD_REGISTRATION_TOKEN", nil);
        case SVSErrorCodeBadNamespace:
            return NSLocalizedString(@"SVS_ERROR_CODE_BAD_NAMESPACE", nil);
        case SVSErrorCodeBadInputParameters:
            return NSLocalizedString(@"SVS_ERROR_CODE_BAD_INPUT_PARAMETERS", nil);
        case SVSErrorCodeAccessDenied:
            return NSLocalizedString(@"SVS_ERROR_CODE_ACCESS_DENIED", nil);
        case SVSErrorCodeBadAccessToken:
            return NSLocalizedString(@"SVS_ERROR_CODE_BAD_ACCESS_TOKEN", nil);
        case SVSErrorCodeDuplicateEmail:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_EMAIL", nil);
        case SVSErrorCodeDuplicatePhone:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_PHONE", nil);
        case SVSErrorCodeBadCredentials:
            return NSLocalizedString(@"SVS_ERROR_CODE_BAD_CREDENTIALS", nil);
        case SVSErrorCodeDuplicateGiftcard:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_GIFTCARD", nil);
        case SVSErrorCodeUnknownGiftcard:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_GIFTCARD", nil);
        case SVSErrorCodeNotEnoughBalance:
            return NSLocalizedString(@"SVS_ERROR_CODE_NOT_ENOUGH_BALANCE", nil);
        case SVSErrorCodeUnknownTransaction:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_TRANSACTION", nil);
        case SVSErrorCodeIllegalState:
            return NSLocalizedString(@"SVS_ERROR_CODE_ILLEGAL_STATE", nil);
        case SVSErrorCodeNotAuthenticated:
            return NSLocalizedString(@"SVS_ERROR_CODE_NOT_AUTHENTICATED", nil);
        case SVSErrorCodeCorruptedData:
            return NSLocalizedString(@"SVS_ERROR_CODE_CORRUPTED_DATA", nil);
        case SVSErrorCodeUnknownCustomer:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_CUSTOMER", nil);
        case SVSErrorCodeUnknownDevice:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_DEVICE", nil);
        case SVSErrorCodeUnknownMessage:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_MESSAGE", nil);
        case SVSErrorCodeAuthorizedTransaction:
            return NSLocalizedString(@"SVS_ERROR_CODE_AUTHORIZED_TRANSACTION", nil);
        case SVSErrorCodeDiscardedTransaction:
            return NSLocalizedString(@"SVS_ERROR_CODE_DISCARDED_TRANSACTION", nil);
        case SVSErrorCodeCapturedTransaction:
            return NSLocalizedString(@"SVS_ERROR_CODE_CAPTURED_TRANSACTION", nil);
        case SVSErrorCodeStatsServiceError:
            return NSLocalizedString(@"SVS_ERROR_CODE_STATS_SERVICE_ERROR", nil);
        case SVSErrorCodeSearchServiceError:
            return NSLocalizedString(@"SVS_ERROR_CODE_SEARCH_SERVICE_ERROR", nil);
        case SVSErrorCodeUnknownProgram:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_PROGRAM", nil);
        case SVSErrorCodeDuplicateProgram:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_PROGRAM", nil);
        case SVSErrorCodeDuplicateCoupon:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_COUPON", nil);
        case SVSErrorCodeDeadlineExceededError:
            return NSLocalizedString(@"SVS_ERROR_CODE_DEADLINE_EXCEEDED_ERROR", nil);
        case SVSErrorCodeUnknownCoupon:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWN_COUPON", nil);
        case SVSErrorCodeDuplicateMembercode:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_MEMBERCODE", nil);
        case SVSErrorCodeDuplicateCustomer:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATE_CUSTOMER", nil);
        case SVSErrorCodeInsecureProtocol:
            return NSLocalizedString(@"SVS_ERROR_CODE_INSECURE_PROTOCOL", nil);
        case SVSErrorCodeExpiredCoupon:
            return NSLocalizedString(@"SVS_ERROR_CODE_EXPIRED_COUPON", nil);
        case SVSErrorCodeInactiveGiftcard:
            return NSLocalizedString(@"SVS_ERROR_CODE_INACTIVE_GIFTCARD", nil);
        case SVSErrorCodeMemberOnly:
            return NSLocalizedString(@"SVS_ERROR_CODE_MEMBER_ONLY", nil);
        case SVSErrorCodeRelateNotResponding:
            return NSLocalizedString(@"SVS_ERROR_CODE_RELATE_NOT_RESPONDING", nil);
        case SVSErrorCodeRelateResponseError:
            return NSLocalizedString(@"SVS_ERROR_CODE_RELATE_RESPONSE_ERROR", nil);
        case SVSErrorCodeTryOperationLater:
            return NSLocalizedString(@"SVS_ERROR_CODE_TRY_OPERATION_LATER", nil);
        case SVSErrorCodeRetryOperation:
            return NSLocalizedString(@"SVS_ERROR_CODE_RETRY_OPERATION", nil);
        case SVSErrorCodeUnknownImport:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNIMPORT", nil);
        case SVSErrorCodePostMarkServiceError:
            return NSLocalizedString(@"SVS_ERROR_CODE_POSTMARKSERVICEERROR", nil);
        case SVSErrorCodeDuplicateExternalMailingSetting:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEEXTERNALMAILINGSETTING", nil);
        case SVSErrorCodeUnknownExternalMailingSetting:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNEXTERNALMAILINGSETTING", nil);
        case SVSErrorCodeUnknownMessageAttachment:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNMESSAGEATTACHMENT", nil);
        case SVSErrorCodeBadTransactionType:
            return NSLocalizedString(@"SVS_ERROR_CODE_BADTRANSACTIONTYPE", nil);
        case SVSErrorCodeMandrillServiceError:
            return NSLocalizedString(@"SVS_ERROR_CODE_MANDRILLSERVICEERROR", nil);
        case SVSErrorCodeUnknownEvent:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNEVENT", nil);
        case SVSErrorCodeUnknownClient:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNCLIENT", nil);
        case SVSErrorCodeUnknownApplication:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNAPPLICATION", nil);
        case SVSErrorCodeUnknownRight:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNRIGHT", nil);
        case SVSErrorCodeUnknownRole:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNROLE", nil);
        case SVSErrorCodeDuplicateApplication:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEAPPLICATION", nil);
        case SVSErrorCodeDuplicateClient:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATECLIENT", nil);
        case SVSErrorCodeDuplicateFBProgram:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEFBPROGRAM", nil);
        case SVSErrorCodeUnknownFBProgram:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNFBPROGRAM", nil);
        case SVSErrorCodeUnknownSalesReceipt:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNSALESRECEIPT", nil);
        case SVSErrorCodeUnknownSalesReceiptItem:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNSALESRECEIPTITEM", nil);
        case SVSErrorCodeHouseChargesNotEnabled:
            return NSLocalizedString(@"SVS_ERROR_CODE_HOUSECHARGESNOTENABLED", nil);
        case SVSErrorCodeUnknownHCAccount:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNHCACCOUNT", nil);
        case SVSErrorCodeHCSettingsCanNotBeChanged:
            return NSLocalizedString(@"SVS_ERROR_CODE_HCSETTINGSCANNOTBECHANGED", nil);
        case SVSErrorCodeDuplicateHCInvoice:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEHCINVOICE", nil);
        case SVSErrorCodeUnknownSalesReceiptPayment:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNSALESRECEIPTPAYMENT", nil);
        case SVSErrorCodeUnknownSalesReceiptCharge:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNSALESRECEIPTCHARGE", nil);
        case SVSErrorCodeUnknownHCInvoice:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNHCINVOICE", nil);
        case SVSErrorCodeWrongHouseChargesType:
            return NSLocalizedString(@"SVS_ERROR_CODE_WRONGHOUSECHARGESTYPE", nil);
        case SVSErrorCodeDuplicateMailchimpList:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEMAILCHIMPLIST", nil);
        case SVSErrorCodeUnknownMailchimpList:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNMAILCHIMPLIST", nil);
        case SVSErrorCodeMailchimpServiceError:
            return NSLocalizedString(@"SVS_ERROR_CODE_MAILCHIMPSERVICEERROR", nil);
        case SVSErrorCodeMailchimpNotAvailableSvsError:
            return NSLocalizedString(@"SVS_ERROR_CODE_MAILCHIMPNOTAVAILABLESVSERROR", nil);
        case SVSErrorCodeDuplicateMailchimpMarketingCampaign:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEMAILCHIMPMARKETINGCAMPAIGN", nil);
        case SVSErrorCodeUnknownMailchimpMarketingCampaign:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNMAILCHIMPMARKETINGCAMPAIGN", nil);
        case SVSErrorCodeMailchimpDailySyncIsAlreadyInProgress:
            return NSLocalizedString(@"SVS_ERROR_CODE_MAILCHIMPDAILYSYNCISALREADYINPROGRESS", nil);
        case SVSErrorCodeSVSErrorCodeUnknownStandardInternalMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNSTANDARDINTERNALMAILINGTYPE", nil);
        case SVSErrorCodeReservedAsStandardInternalMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODE_RESERVEDASSTANDARDINTERNALMAILINGTYPE", nil);
        case SVSErrorCodeUnknownInternalMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNINTERNALMAILINGTYPE", nil);
        case SVSErrorCodeDuplicateInternalMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODE_DUPLICATEINTERNALMAILINGTYPE", nil);
        case SVSErrorCodeDisabledInternalMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODE_DISABLEDINTERNALMAILINGTYPE", nil);
        case SVSErrorCodeTokensNotEnabled:
            return NSLocalizedString(@"SVS_ERROR_CODE_TOKENSNOTENABLED", nil);
        case SVSErrorCodeUnknownCustomerTodo:
            return NSLocalizedString(@"SVS_ERROR_CODE_UNKNOWNCUSTOMERTODO", nil);
        case SVSErrorCodeUnknownCustomerActivity:
            return NSLocalizedString(@"SVS_ERROR_CODE_TRYOPERATIONLATER", nil);
        case SVSErrorCodeNamespaceNotScheduledForPurge:
            return NSLocalizedString(@"SVS_ERROR_CODE_NAMESPACENOTSCHEDULEDFORPURGE", nil);
        case SVSErrorCodeNamespaceScheduledForPurge:
            return NSLocalizedString(@"SVS_ERROR_CODENAMESPACESCHEDULEDFORPURGE", nil);
        case SVSErrorCodeUnknownLRPPromotionalPeriod:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNLRPPROMOTIONALPERIOD", nil);
        case SVSErrorCodeUnknownMemberLevel:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNMEMBERLEVEL", nil);
        case SVSErrorCodeDuplicateCustomerNo:
            return NSLocalizedString(@"SVS_ERROR_CODEDUPLICATECUSTOMERNO", nil);
        case SVSErrorCodeDuplicateChatMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODEDUPLICATECHATMAILINGTYPE", nil);
        case SVSErrorCodeUnknownChatMailingType:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNCHATMAILINGTYPE", nil);
        case SVSErrorCodeUnknownChatMailingTypePreSetMessage:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNCHATMAILINGTYPEPRESETMESSAGE", nil);
        case SVSErrorCodeUnknownWebhook:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNWEBHOOK", nil);
        case SVSErrorCodeUnknownDeviceCustomCommand:
            return NSLocalizedString(@"SVS_ERROR_CODEUNKNOWNDEVICECUSTOMCOMMAND", nil);
        case SVSErrorCodeCouponNotEnabled:
            return NSLocalizedString(@"SVS_ERROR_CODE_COUPONNOTENABLED", nil);
        default:
            return NSLocalizedString(@"UNKNOWNERROR_TEXT", nil);
    }
}

@end

@implementation ReceiptSVSFrequentByuerBalancesResponse

-(void)dealloc{
    [_programId release];
    [_balanceAmount release];
    [_rate release];
    [_earnedAmount release];
    
    [super dealloc];
}

@end

@implementation ReceiptSVSHABalancesResponse

-(void)dealloc{
    [_invoiceId release];
    [_balanceAmount release];
    [_earnedAmount release];
    [super dealloc];
}

@end

@implementation ReceiptSVSAuthorizeEntityResponse

-(void)dealloc{
    [_transactionId release];
    [_balanceAmount release];
    [super dealloc];
}

@end

@implementation ReceiptSVSBalancesResponse

-(void)dealloc{
    [_receiptId release];
    [_customerId release];
    [_lrp1Balance release];
    [_lrp1EarnedPoints release];
    [_lrp1Threshold release];
    [_lrp2Balance release];
    [_lrp2EarnedPoints release];
    [_lrp2Threshold release];
    [_fbBalances release];
    [_haBalances release];
    [_tokenBalance release];
    [_tokenEarnedPoints release];
    [super dealloc];
}

@end
