//
//  UrlDictionaryServiceParser.m
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UrlDictionaryServiceParser.h"

@implementation UrlDictionaryServiceParser

+(NSString*)localizedErrorMessageByErrorCode:(UDSErrorCode)code{
    switch (code){
        case UDSInternalError:
            return NSLocalizedString(@"UDS_ERROR_CODE_INTERNAL_ERROR", nil);
        case UDSBadInputParameters:
            return NSLocalizedString(@"UDS_ERROR_CODE_BAD_INPUT_PARAMETERS", nil);
        case UDSURLNotFound:
            return NSLocalizedString(@"UDS_ERROR_CODE_URL_NOT_FOUND", nil);
        default:
            return NSLocalizedString(@"UNKNOWNERROR_TEXT", nil);
    }
}

@end
