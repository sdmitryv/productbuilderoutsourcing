//
//  SVSJsonHelper.h
//  ProductBuilder
//
//  Created by valery on 11/18/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVSJsonHelper : NSObject

+(NSDictionary*)fixJSonTypesInDictionary:(NSDictionary*)dict;

@end
