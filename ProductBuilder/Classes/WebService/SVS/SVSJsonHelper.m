//
//  SVSJsonHelper.m
//  ProductBuilder
//
//  Created by valery on 11/18/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import "SVSJsonHelper.h"
#import "BPUUID.h"
#import "NSNamedMutableArray.h"

@implementation SVSJsonHelper


+(NSDictionary*)fixJSonTypesInDictionary:(NSDictionary*)dict{
    NSMutableDictionary* newDict = nil;
    for (NSString* key in dict.allKeys){
        id obj = dict[key];
        id newObj = [[self class] fixObjectType:obj];
        if (newObj!=obj){
            if (!newDict){
                newDict = [[[NSMutableDictionary alloc]initWithDictionary:dict]autorelease];
            }
            newDict[key] = newObj;
        }
    }
    return newDict?:dict;
}

+(NSArray*)fixJSonTypesInArray:(NSArray*)array{
    NSMutableArray* newArray = nil;
    for (NSInteger index = 0; index < array.count; index++){
        id obj = array[index];
        id newObj = [[self class] fixObjectType:obj];
        if (newObj!=obj){
            if (!newArray){
                newArray = [[[NSMutableArray alloc]initWithArray:array]autorelease];
            }
            newArray[index] = newObj;
        }
    }
    return newArray?:array;
}

+(NSObject*)fixObjectType:(NSObject*)object{
    if ([object isKindOfClass:[BPUUID class]]){
        return [[object description]lowercaseString];
    }
    else if ([object isKindOfClass:[NSDate class]]){
        return @((long long)([(NSDate*)object timeIntervalSince1970]*1000LL));
    }
    else if ([object isKindOfClass:[NSArray class]]){
        return [[self class ]fixJSonTypesInArray:(NSArray*)object];
    }
    else if ([object isKindOfClass:[NSNamedMutableArray class]]){
        return [[self class ]fixJSonTypesInArray:((NSNamedMutableArray *)object).data];
    }
    else if ([object isKindOfClass:[NSDictionary class]]){
        return [[self class ]fixJSonTypesInDictionary:(NSDictionary*)object];
    }
    else if ([object isKindOfClass:[NSDecimalNumber class]]){
        return [((NSDecimalNumber*)object) stringValue];
    }
    return object;
}

@end
