//
//  StoredValuesServiceLoaderHelper.h
//  ProductBuilder
//
//  Created by valery on 9/7/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoredValuesService.h"

extern NSString* const SVSLazyValueDidChangeState;

typedef NS_ENUM(NSUInteger, SVSLazyValueState) {SVSLazyValueStateNone = 0, SVSLazyValueStateInProgress = 1, SVSLazyValueStateReady = 2, SVSLazyValueStateFailed = 3, SVSLazyValueStateCanceled = 4};

@interface SVSLazyValue : NSObject<StoredValuesServiceDelegate>{
    SVSLazyValueState _state;
    StoredValuesServiceRequest* _request;
    NSObject* _value;
}

-(instancetype)initWithRequest:(StoredValuesServiceRequest*)request;
@property(atomic, readonly) StoredValuesServiceRequest* request;
@property(atomic, readonly) SVSLazyValueState state;
@property(atomic, readonly) NSError* error;
@property(atomic, readonly) NSObject* value;
@property(atomic, readonly) BOOL isErrorConnectionRelated;
@property(atomic, readonly) BOOL postingNotification;
-(void)refresh;
-(void)waitUntiInProgress;
@end
