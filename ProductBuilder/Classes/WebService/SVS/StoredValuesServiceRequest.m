//
//  StoredValuesServiceRequest.m
//  ProductBuilder
//
//  Created by valera on 3/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "StoredValuesServiceRequest.h"
#import "USAdditions.h"
#import "StoredValuesServiceParser.h"
#import "SettingManager.h"
#import "EmailValidation.h"
#import "NSThread+BlockAdditions.h"
#import "SVSJsonHelper.h"
#import "NSURLSession+Extentions.h"
#import "AppSettingManager.h"
#import "AppParameterManager.h"

#define DEFAULT_TIMEOUT     30

// Common parameters
NSString * const REQUEST_IDENTIFIER = @"requesttype";

@interface StoredValuesServiceRequest(){
    NSRunLoop* _waitLoop;
}
@property (nonatomic, assign) BOOL isCompleted;
@property (nonatomic, assign) BOOL isSucceed;
@end

@implementation StoredValuesServiceRequest

@synthesize requestType;
@synthesize parameters;
@synthesize returnClass;
@synthesize delegate;
@synthesize error;
@synthesize result;
@synthesize state;
//@synthesize authState;
@synthesize sessionTask;
@synthesize serviceURL;
@synthesize isTokenResult;
@synthesize logEnable;
@synthesize cookies;
@synthesize defaultTimeout;
@synthesize responseData;
@synthesize authValue;
@synthesize authUsername;
@synthesize authPassword;
@synthesize urlResponse;

-(id)initWithType:(StoredValuesServiceRequestType)type url:(NSURL *)url params:(NSDictionary *)params{
    self = [super init];
    if (self != nil) {
        defaultTimeout = DEFAULT_TIMEOUT;
        logEnable = [AppParameterManager instance].enableLogs;
        //authState = StoredValuesServiceRequestAuthStateNotAuthorized;
        SettingManager * settingManager = [SettingManager instance];
        self.authUsername = [settingManager svsAccountUserName];
        self.authPassword = [settingManager svsAccountPassword];
        self.requestType = type;
        self.parameters = params;
        self.state = StoredValuesServiceRequestPending;
        self.serviceURL = url;
        waitForRequestSemaphore = dispatch_semaphore_create(0);
        _delegateQueue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

-(id)initWithType:(StoredValuesServiceRequestType)type url:(NSURL *)url params:(NSDictionary *)params delegate:(id<StoredValuesServiceDelegate>) aDelegate{
    self = [self initWithType:type url:url params:params];
    if (self != nil) {
        self.delegate = aDelegate;
    }
    return self;
}

-(void)dealloc {
    self.delegate = nil;
    [self cancel];
    [serviceURL release];
    [authPassword release];
    [authUsername release];
    [urlResponse release];
    [authValue release];
    [responseData release];
    [cookies release];
	[parameters release];
    [error release];
    [result release];
    [sessionTask cancel];
    [sessionTask release];
    [_context release];
    if (waitForRequestSemaphore)
        dispatch_release(waitForRequestSemaphore);
    [_delegateQueue release];
	[super dealloc];
}

-(void)setDelegate:(id<StoredValuesServiceDelegate>)aDelegate{
    delegate = aDelegate;
    delegateMethods.didCancel = [self.delegate respondsToSelector:@selector(storedValuesServiceRequestDidCancel:)];
    delegateMethods.didCompleteWithError = [self.delegate respondsToSelector:@selector(storedValuesServiceRequestDidComplete:withError:)];
    delegateMethods.didCompleteWithResult = [self.delegate respondsToSelector:@selector(storedValuesServiceRequestDidComplete:withResult:)];
}

-(NSURLSessionTask *)execURLRequest:(NSURL *)url withBody:(NSData *)body handleResults:(BOOL)handleResults{
    
    self.responseData = nil;
    
    if (!url.host || url.host.length == 0) {
        
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:NSLocalizedString(@"SALE_TOKEN_URL_EMPTY", nil) forKey:NSLocalizedDescriptionKey];
        [self handleError:[NSError errorWithDomain:@"StoredValuesService" code:5 userInfo:errorDetail]];
        return nil;
    }
    
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
														   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
													   timeoutInterval:defaultTimeout];
    
	if(cookies != nil) {
		[request setAllHTTPHeaderFields:cookies];
	}
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:url.host forHTTPHeaderField:@"Host"];
    //CR 50306:POS - Fetch CRM localized messages
    NSString * language = [AppSettingManager instance].deviceLanguage;
    if (language == nil) {
        NSArray *languages = [NSBundle mainBundle].preferredLocalizations;
        language = languages.count > 0 ? languages[0] : @"en";
    }
    [request setValue:language forHTTPHeaderField:@"X-SVS-Language"];
    //
    NSString * accessToken = [AppSettingManager instance].SVSAccessToken;
    if ([accessToken length] > 0) {
        [request setValue:accessToken forHTTPHeaderField:@"Access-Token"];
    }
	[request setHTTPMethod: @"POST"];
	[request setHTTPBody: body];
	if(self.logEnable) {
		NSLog(@"\nOutputPath:\n%@", [request.URL absoluteString]);
		NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
        NSString * bodyText = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
		NSLog(@"OutputBody:\n%@", bodyText);
        [bodyText release];
	}
    
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = self.defaultTimeout;

    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:_delegateQueue];
 
    __block __typeof(self) selfCopy = self;
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable anError) {
        if (anError.code==NSURLErrorCancelled){
            [session invalidateAndCancel];
            return;
        }
        [session finishTasksAndInvalidate];
        selfCopy.responseData = data;
        selfCopy.urlResponse = response;
        
        if(logEnable) {
            NSLog(@"\nResponsePath: %@\n", [response.URL absoluteString]);
            if ([response isKindOfClass:NSHTTPURLResponse.class]){
                NSLog(@"ResponseStatus: %ld\n", (long)((NSHTTPURLResponse*)response).statusCode);
                NSLog(@"ResponseHeaders:\n%@", ((NSHTTPURLResponse*)response).allHeaderFields);
            }
        }
        if (!anError){
            if ([response isKindOfClass:NSHTTPURLResponse.class] && ((NSHTTPURLResponse*)response).statusCode >= 400) {
               anError = [NSError errorWithDomain:@"StoredValuesService" code:((NSHTTPURLResponse*)response).statusCode userInfo:@{NSLocalizedDescriptionKey: [NSHTTPURLResponse localizedStringForStatusCode:((NSHTTPURLResponse*)response).statusCode]}];
            }
        }
        selfCopy.error = anError;
        if (handleResults){
            [selfCopy handleResults];
        }
        else{
            dispatch_semaphore_signal(waitForRequestSemaphore);
        }
    }];
    
    [task resume];
    return task;
}

-(void)execWithHandlingResults:(BOOL)handleResults {
    NSDictionary* paramsHandled = [SVSJsonHelper fixJSonTypesInDictionary:parameters];
	NSData *bodyData = [NSJSONSerialization dataWithJSONObject:paramsHandled options:0 error:nil];
    self.error = nil;
    self.isCompleted = self.isSucceed = NO;
    self.state = StoredValuesServiceRequestStarted;
    self.sessionTask = [self execURLRequest:self.serviceURL withBody:bodyData handleResults:handleResults];
}

-(void)handleResults{
    if (self.state != StoredValuesServiceRequestCanceled) {
        if (self.error){
            [self handleError:self.error];
        }
        else{
            if (logEnable && self.responseData) {
                NSLog(@"ResponseBody:\n%@", [[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding] autorelease]);
            }
            [self answerToRequest];
            
        }
    }
    dispatch_semaphore_signal(waitForRequestSemaphore);
}

-(void)exec{
    [self execWithHandlingResults:YES];
}

-(void)execSync{
    [self execWithHandlingResults:NO];
    dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_FOREVER);
    [self handleResults];
}

-(void)cancel{
    if (!delegateMethods.didCancel){
        self.delegate = nil;
    }
    self.state = StoredValuesServiceRequestCanceled;
    [self.sessionTask cancel];
    [_delegateQueue cancelAllOperations];
    self.isCompleted = YES;
    if (delegate && delegateMethods.didCancel) {
        if ([delegate isKindOfClass:UIViewController.class]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [delegate storedValuesServiceRequestDidCancel:self];
            });
        }
        else{
            [delegate storedValuesServiceRequestDidCancel:self];
        }
    }
}

-(void)waitUntilDelegateQueueCompleted{
    [_delegateQueue waitUntilAllOperationsAreFinished];
}

-(void)setIsCompleted:(BOOL)isCompleted{
    _isCompleted = isCompleted;
    if (isCompleted){
        dispatch_semaphore_signal(waitForRequestSemaphore);
    }
    else{
        while (!dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_NOW));
    }
}

#pragma mark -

-(void)reportRequestWithResult:(NSObject*)aResult{
    self.result = aResult;
    self.isSucceed = TRUE;
    self.isCompleted = TRUE;
    if (delegate  && delegateMethods.didCompleteWithResult) {
        if ([delegate isKindOfClass:UIViewController.class]){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (delegate && self.state == StoredValuesServiceRequestFinished){
                    [delegate storedValuesServiceRequestDidComplete:self withResult:aResult];
                }
            });
        }
        else{
            [delegate storedValuesServiceRequestDidComplete:self withResult:aResult];
        }
    }
}

-(void)reportRequestWithError:(NSError*)anError{
    self.error = anError;
    self.isSucceed = FALSE;
    self.isCompleted = TRUE;
    if (delegate && delegateMethods.didCompleteWithError) {
        if ([delegate isKindOfClass:UIViewController.class]){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (delegate && self.state == StoredValuesServiceRequestFinished){
                    [delegate storedValuesServiceRequestDidComplete:self withError:anError];
                }
            });
        }
        else{
            [delegate storedValuesServiceRequestDidComplete:self withError:anError];
        }
    }
}

#pragma mark - Private method

-(void)answerToRequest{
    if (self.state == StoredValuesServiceRequestFinished) {
        return;
    }
    self.state = StoredValuesServiceRequestFinished;
    NSObject * aResult = nil;
    NSError * anError = nil;
    if (responseData != nil) {
        NSString * dirtyText = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSString * text = [dirtyText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n\r ;"]];
        [dirtyText release];
        anError = [StoredValuesServiceParser catchManualExceptions:text];
        if (anError) {
            [self reportRequestWithError:anError];
            return;
        }
        if (self.returnClass == [USBoolean class]) {
            BOOL value = NO;
            if ([text hasPrefix:@"true"]) {
                value = YES;
            }
            else if ([text hasPrefix:@"false"]) {
                value = NO;
            }
            aResult = @(value);
        }
        else if (self.returnClass == [NSString class]) {
            aResult = [text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n\r ;"]];
        }
        else if (self.returnClass == [NSDecimalNumber class]) {
            aResult = [NSDecimalNumber decimalNumberWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n\r ;"]]];
        }
        else {
            id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&anError];
            if (!anError) {
                if ([object isKindOfClass:[NSNull class]]) {
                    aResult = nil;
                }
                else if (![object isKindOfClass:[NSDictionary class]]){
                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Unexpected response type"};
                    anError = [NSError errorWithDomain:@"StoredValuesService" code:100 userInfo:userInfo];
                }
                else {
                    NSDictionary * responseDictionary = (NSDictionary*)object;
                    NSNumber * errorCode = responseDictionary[@"errorCode"];
                    //actually here is string code error
                    NSString * errorReason = responseDictionary[@"error"];
                    if ([errorCode integerValue] || errorReason) {
                        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
                        id data = responseDictionary[@"data"];
                        if (data){
                            userInfo[NSURLErrorKey] = data;
                        }
                        id message = responseDictionary[@"message"];
                        if (message){
                            userInfo[NSLocalizedFailureReasonErrorKey] = message;
                        }
                        
                        NSString* localizedError = [self localizedErrorByCode:[errorCode integerValue] details:message];
                        if (localizedError){
                            userInfo[NSLocalizedDescriptionKey]= localizedError;
                        }
//                        if (errorReason){
//                            userInfo[NSHelpAnchorErrorKey] = errorReason;
//                        }
                        [self reportRequestWithError:[NSError errorWithDomain:@"com.svs.response" code:[errorCode integerValue] userInfo:userInfo]];
                        return;
                    }
                    switch(self.requestType){
                        case CHECKCUSTOMEREMAIL:
                        case CHECKCUSTOMERPHONE:
                            aResult = responseDictionary[@"customerID"];
                            break;
                        case GETCUSTOMER:
                            break;
                        case UPDATECUSTOMER:
                            break;
                        case REGISTERCUSTOMER:
                            break;
                        case FINDCUSTOMER:
                            break;
                        case VALIDATEEMAIL:
                            aResult = [StoredValuesServiceParser parseValidateEmailResponse:responseDictionary];
                            break;
                        case FETCHLATESTGCTRANSACTION:
                            break;
                        case GIFTCARDEXISTS:
                            break;
                        case GIFTCARDEXISTSMULTI:
                            break;
                        case GIFTCARDASSIGNCUSTOMER:
                            break;
                        case GIFTCARDCREATE:
                        case COMMITGCTRANSACTION:
                        case REVERTGCTRANSACTION:
                            aResult = responseDictionary[@"giftCardBalance"];
                            break;
                        case GIFTCARDSFORCUSTOMER:
                            break;
                        case AUTHORIZERECEIPTTRANSACTION:
                        case COMMITRECEIPTTRANSACTION:
                            aResult = [StoredValuesServiceParser parseReceiptBalances:responseDictionary];
                            break;
                        case AUTHORIZEGCTRANSACTION:{
                            ReceiptSVSAuthorizeEntityResponse* receiptSVSAuthorizeEntityResponse = [[[ReceiptSVSAuthorizeEntityResponse alloc]init]autorelease];
                            receiptSVSAuthorizeEntityResponse.transactionId = responseDictionary[@"transactionID"];
                            NSDictionary* giftCardBalance = responseDictionary[@"giftCard"];
                            if ([giftCardBalance isKindOfClass:NSDictionary.class]){
                                receiptSVSAuthorizeEntityResponse.balanceAmount = giftCardBalance[@"giftCardBalance"];
                            }
                            aResult = receiptSVSAuthorizeEntityResponse;
                       }
                            break;
                        case COUPONSAUTHORIZE:
                            aResult = responseDictionary[@"transactionID"];
                            break;
                        case FETCHLATESTHCTRANSACTION:
                            break;
                        case FETCHLATESLRPTRANSACTIONS:
                            break;
                        case FETCHLATESLRPTRANSACTIONS2:
                            break;
                        case GETLRPBALANCE:
                            break;
                        case ASSIGNLRPTOCUSTOMER:
                            break;
                        case REGISTERDEVICE:
                            aResult = responseDictionary[@"accessToken"];
                            break;
                        case GETHCBALANCE:
                        case UPDATEBALANCE:
                            break;
                        case COMMITTRANSACTIONLP:
                        case GET_TOKEN_BALANCE:
                            aResult = [NSDecimalNumber decimalNumberWithString:[responseDictionary[@"balance"] description]];
                            break;
                        case FETCHSCTRSANSACTION:
                            break;
                        case FETCH_TOKEN_TRSANSACTION:
                            break;
                        case AUTHORIZESCTRANSACTION:{
                            ReceiptSVSAuthorizeEntityResponse* receiptSVSAuthorizeEntityResponse = [[[ReceiptSVSAuthorizeEntityResponse alloc]init]autorelease];
                            receiptSVSAuthorizeEntityResponse.transactionId = responseDictionary[@"transactionKey"];
                            receiptSVSAuthorizeEntityResponse.balanceAmount = responseDictionary[@"expectedSCBalance"];
                            aResult = receiptSVSAuthorizeEntityResponse;
                        }
                            break;
                        case COMMITSCTRANSACTION:
                        case DISCARDSCTRANSACTION:
                        case GETSCBALANCE:
                            aResult = [NSDecimalNumber decimalNumberWithString:[responseDictionary[@"SCBalance"] description]];
                            break;
                        case GETSETTINGS:
                            aResult = responseDictionary;
                            break;
                        case COUPONSCHECK:
                            aResult = responseDictionary;
                            break;
                        case COUPONSLIST:
                        case COUPONSACTIVECUSTOMER:
                            aResult = responseDictionary[@"coupons"];
                            break;
                        case COUPONSACTIVECUSTOMERCOUNT:
                            aResult = responseDictionary[@"count"];
                            break;
                        case GETCUSTOMERLOYALTYPROGRAMS:
                            break;
                        case CARDONFILEGETLIST:
                            aResult = responseDictionary[@"entries"];
                            break;
                        default:
                            aResult = [self tryGetResultFromRequest:responseDictionary requestType:self.requestType];
                            break;
                    }
                }
            }
            if (anError) {
                
                if (logEnable) {
                    NSLog(@"JSon Parser Error:\n%@\n", anError);
                }
                [self reportRequestWithError:anError];
            }
        }
    }
    [self reportRequestWithResult:aResult];
}

-(NSString *)localizedErrorByCode:(NSInteger)errorCode {
    
    return [StoredValuesServiceParser localizedErrorMessageByErrorCode:errorCode];
}

-(NSString *)localizedErrorByCode:(NSInteger)errorCode details:(NSString *)message {

    NSString * localizedMessage = [StoredValuesServiceParser localizedErrorMessageByErrorCode:errorCode];
    if (errorCode == SVSErrorCodeRelateResponseError) {
        return [NSString stringWithFormat:@"%@\n%@", localizedMessage, message];
    }
    return localizedMessage;
}

-(NSObject *)tryGetResultFromRequest:(NSDictionary *)transactionsDictionary requestType:(int)requestType {
    
    return nil;
}

#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * __nullable credential))completionHandler{

    [session defaultHandlerForDidReceiveChallenge:challenge completionHandler:completionHandler userName:self.authUsername password:self.authPassword errorHandler:^(NSError* authError){ [self handleError:authError]; }];
}

-(void)handleError:(NSError*)anError{
    if (anError.code==NSURLErrorCancelled) return;
    if (logEnable) {
        NSLog(@"ResponseError:\n%@\n", anError);
    }
    if (((self.state == StoredValuesServiceRequestPending) || (self.state == StoredValuesServiceRequestStarted))) {
        self.state = StoredValuesServiceRequestFinished;
        [self reportRequestWithError:anError];
    }

}

@end
