//
//  SyncManager.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 12/15/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceResponse.h"
#import "NSNamedMutableArray.h"
#import "WebServiceOperation.h"

@class SyncManager;

@protocol SyncManagerDelegate

-(void)syncManager:(SyncManager*)syncManager didCompleteOperation:(NSString *)name withResult:(NSObject *)result;
-(void)syncManager:(SyncManager*)syncManager didOccurError:(NSString *)errorText error:(NSError*)error;
@optional
-(void)syncManager:(SyncManager*)syncManager didReceiveRecord:(id)object;
-(void)syncManager:(SyncManager*)syncManager didReceiveBinaryStream:(NSInputStream*)stream;
-(void)syncManager:(SyncManager*)syncManager completedUploadWithProgress:(float)progress;
@end

@class iPadServiceSoapBinding;
@class Item;
@class BPUUID;

@interface SyncManager : NSObject <WebServiceOperationDelegate> {
	id<SyncManagerDelegate> syncDelegate;
    WebServiceOperation* _currentOperation;
    //BOOL responseReceived;
    NSString* opsAddress;
    BOOL logXML;
    BOOL isOpsAddressValid;
    NSTimeInterval defaultTimeout;
    NSDateFormatter *_localDateFormatter;
}

//+ (SyncManager *)sharedInstance;

@property(nonatomic, assign) id<SyncManagerDelegate> syncDelegate;
@property(nonatomic, assign) BOOL logXML;
@property(nonatomic, readonly) NSString* address;
-(void)readSettings;

- (void)getIDs:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp;
- (void)getTableCount:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp;
- (void)getTableCount:(NSString *)tableName ids:(NSArray*) ids modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp;
- (void)getTable:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp;
- (void)getTable:(NSString *)tableName modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp;
- (void)getTableSAX:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate timestamp:(NSData *)timestamp;
- (void)getTableAsBinary:(NSString *)tableName ids:(NSArray *)ids modifiedDate:(NSDate *)modifiedDate whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams mode:(NSUInteger)syncMode timestamp:(NSData *)timestamp retryCount:(NSUInteger)retryCount;
- (void)updateReceipt:(NSDictionary *)receipt items:(NSNamedMutableArray *)items itemsMembership:(NSNamedMutableArray*)itemsMembership payments:(NSNamedMutableArray *)payments charges:(NSNamedMutableArray *)charges globalCharges:(NSNamedMutableArray *)globalCharges
          itemCharges:(NSNamedMutableArray *)itemCharges taxes:(NSNamedMutableArray *)taxes creditAccounts:(NSNamedMutableArray *)creditAccounts discountCoupons:(NSNamedMutableArray*)discountCoupons onlineOMSTransaction:(NSDictionary *)onlineOMSTransaction receiptTaxExemptInfo:(NSNamedMutableArray*)receiptTaxExemptInfo taxJurisdictions:(NSNamedMutableArray*)taxJurisdictions persons:(NSNamedMutableArray*)persons receiptAccounts:(NSNamedMutableArray*)receiptAccounts itemPromos:(NSNamedMutableArray*)promos receiptPromos:(NSNamedMutableArray*)receiptPromos;
- (void)updateCustomer:(NSDictionary *)customer;
- (void)updateEmployeeNotification:(NSDictionary *)employeeNotification;
- (void)updateEmailTask:(NSNamedMutableArray *)emailTask;

- (void)updateReceiptRemotePayment:(NSNamedMutableArray *)receiptRemotePayments;
- (void)updateActionsTracking:(NSNamedMutableArray*)actionTrackings;
- (void)uploadQueryResults:(NSNamedMutableArray *)results;
- (void)saveCCLog:(NSString*)ccString;
- (void)waitForResponse;
- (void)test:(id<WebServiceOperationDelegate>)delegate;
- (void)getVersion;
- (void)uploadBackup:(NSString*)filePath;
- (void)updateReceiptTaxFree:(NSDictionary *)receiptTaxFree;

- (void)getOriginalReceipt:(int)receiptNum locationId:(BPUUID*)locationId;
- (void)getCustomerHistory:(BPUUID*)customerId locationId:(BPUUID*)locationId fromDate:(NSDate*)fromDate;
- (void)findReceiptForCode:(NSString*)receiptCode withMaxDays:(NSInteger)maxDays useLocationSearch:(BOOL)useLocationSearch useLocalSearch:(BOOL)useLocalSearch;
- (void)getItemsAvailableQty:(BPUUID*)receiptId;
- (void)getCurrentLocation;
- (void)getAppointments:(BPUUID*)customerId;
- (void)rejectAppointmentWithId:(BPUUID *)appointmentId;
- (void)getCustomersByOrderNum:(NSString*)orderNum;
- (void)unholdReceipt:(BPUUID *)receiptId;
- (void)discardAllHeldReceipts;

-(void)processDataSets:(NSArray *)datasets withMetaData:(NSDictionary *)metaData;

- (void)updateSecurityLogs:(NSNamedMutableArray *)logs;
- (void)updateContacts:(NSNamedMutableArray *)contacts;
- (void)updateShipToAddresses:(NSNamedMutableArray *)contacts;
- (void)updateSVSBalances:(NSNamedMutableArray *)balances;
- (void)updateSVSTransactions:(NSNamedMutableArray *)transactions;
- (void)commitReturnItems:(NSArray*)returnReceiptItemIds;

-(void)checkServiceAvailability;
-(void)initProcessor:(int)paymentProcessingType;

- (void)checkDeviceWithName:(NSString *)name;
- (void)updateDevice:(NSDictionary *)device;
- (void)updateDeviceStats:(NSDictionary *)deviceStats;
- (void)authorizeWithLogin:(NSString *)login password:(NSString *)password;
- (void)uploadExceptionLog:(NSDictionary *)log;
- (void)uploadCustomUILayouts:(NSNamedMutableArray *)transactions;


-(void)getMWCardDetailsByCardToken:(NSString*)cardToken;

-(void)updateSalesOrder:(NSDictionary *)salesOrder items:(NSNamedMutableArray *)items globalCharges:(NSNamedMutableArray *)globalCharges itemCharges:(NSNamedMutableArray *)itemCharges taxes:(NSNamedMutableArray *)taxes customers:(NSNamedMutableArray *)customers instruction:(NSDictionary *)instruction instructionKey:(NSString *)instructionKey  orderTaxExemptInfo:(NSNamedMutableArray *)salesOrderTaxExemptInfo taxJurisdictions:(NSNamedMutableArray*)taxJurisdictions persons:(NSNamedMutableArray*)persons;
-(void)updateShipMemo:(NSDictionary *)shipMemo items:(NSNamedMutableArray *)items cartons:(NSNamedMutableArray *)cartons cartonItems:(NSNamedMutableArray *)cartonItems sendPickUpReadyEmail:(BOOL)sendPickUpReadyEmail;

-(void)getLocalDateTime;
-(void)getEmployeeNotificationForEmployeeId:(BPUUID*)employeeId priorNotificationId:(BPUUID*)notificationId;
-(void)checkPendingShipMemos;
-(void)updateInvenPreSetCategories:(NSDictionary*) category;
-(void)updateInvenPreSetCategoryItems:(NSDictionary*) item;

// PayPal Method
-(void)getAccessToken;

// CHQ Payments
-(void)getCCPaymentsBySalesOrder:(BPUUID*)salesOrderId;

//Direct Printers
- (void)updateDirectPrinter:(NSDictionary *)directPrinter;
- (void)updateDirectPrinters:(NSNamedMutableArray *)directPrinters;
- (void)updateDirectPrinterOption:(NSDictionary *)directPrinterOption;
- (void)updateDirectPrinterOptions:(NSNamedMutableArray *)directPrinterOptions;

@property (assign) NSTimeInterval defaultTimeout;
@property (atomic, readonly)WebServiceOperation* currentOperation;
@end
