//
//  RTProtoDataReader.h
//  ProtoBufTableReader
//
//  Created by valera on 5/27/13.
//  Copyright (c) 2013 valera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CodedInputStream.h"

@interface NSUTF8StringContainer : NSObject
@property (nonatomic, retain) NSData* data;
+(id)utf8StringContainerWithData:(NSData*)aData;
@end

typedef NS_ENUM(NSUInteger, ProtoDataType){
    ProtoDataTypeString = 1,
    ProtoDataTypeDateTime = 2,
    ProtoDataTypeInt = 3,
    ProtoDataTypeLong = 4,
    ProtoDataTypeShort = 5,
    ProtoDataTypeBool = 6,
    ProtoDataTypeByte = 7,
    ProtoDataTypeFloat = 8,
    ProtoDataTypeDouble = 9,
    ProtoDataTypeGuid = 10,
    ProtoDataTypeChar = 11,
    ProtoDataTypeDecimal = 12,
    ProtoDataTypeByteArray = 13,
    ProtoDataTypeCharArray = 14
};

@interface RTProtoDataReader : NSObject{
    //NSInputStream* stream;
    NSMutableArray* currentRow;
    NSMutableArray* colReaders;
    NSMutableArray* colNames;
    NSMutableArray* colTypes;
    PBCodedInputStream* inputStream;
    int32_t currentField;
    int32_t currentTableToken;
    BOOL reachEndOfCurrentTable;
}
-(id)initWithStream:(NSInputStream*)aStream;

-(NSString*)getName:(NSUInteger) index;
-(id)getValue:(NSUInteger)index;
-(NSUInteger)getOrdinal:(NSString*)name;
-(NSUInteger)fieldCount;
-(BOOL)read;
-(NSArray*)currentRow;
-(NSArray*)columnNames;
@end
