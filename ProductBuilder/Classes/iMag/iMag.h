//
//  iMag.h
//  IDTech
//
//  Created by Randy Palermo on 5/4/10.
//  Copyright 2010 ID_Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import	<ExternalAccessory/ExternalAccessory.h>

extern NSString* const iMagDidReceiveDataNotification;
extern NSString* const iMagDidConnectNotification;
extern NSString* const iMagDidDisconnectNotification;

typedef NS_ENUM(NSUInteger, RTSwipeReaderType) {RTSwipeReaderTypeIdTech = 0, RTSwipeReaderiDynamo = 1, RTHoneywellReader = 2, RTInfinitePeripherals = 3, RTSwipeReaderBTMagReader = 4};

@interface RTSwipeReaderDescription: NSObject{
    NSString* protocolString;
    RTSwipeReaderType type;
}
@property (nonatomic, readonly)NSString* protocolString;
@property (nonatomic, readonly)RTSwipeReaderType type;
-(id)initWithProtocolString:(NSString*)aProtocolString type:(RTSwipeReaderType)aType;
+(RTSwipeReaderDescription*)swipeReaderDescription:(NSString*)aProtocolString type:(RTSwipeReaderType)aType;
@end

@interface RTSwipeReaderScan : NSObject{
    NSData* data;
    RTSwipeReaderType readerType;
}
@property (nonatomic, readonly)NSData* data;
@property (nonatomic, readonly)RTSwipeReaderType readerType;
@property (nonatomic, readonly)NSString* ksn;
@property (nonatomic, readonly)NSString* encryptedData;
-(id)initWithData:(NSData*)aData type:(RTSwipeReaderType)aType;
-(id)initWithData:(NSData*)aData type:(RTSwipeReaderType)aType ksn:(NSString*)aKsn encTracks:(NSString*)encTracks;
+(RTSwipeReaderScan*)swipeReaderScanWithData:(NSData*)aData type:(RTSwipeReaderType)aType;

+(RTSwipeReaderScan*)encryptedSwipeWithTracks:(NSString*)encTracks ksn:(NSString*)ksn maskedTrack1:(NSData*)maskedtrack1 type:(RTSwipeReaderType)aType;
@end

@interface iMag : NSObject <EAAccessoryDelegate, NSStreamDelegate> {
//	NSStream *os;
    EASession *session;
    BOOL _connected;
    RTSwipeReaderType type;
    NSMutableData* data;
    BOOL waitForMoreData;
    RTSwipeReaderDescription* currentReaderDescription;
}
@property(nonatomic,readonly)RTSwipeReaderDescription* currentReaderDescription;
-(id)init;
+(iMag*)sharedInstance;
- (void) sendCommand: (NSString*)cmd;

//@property (nonatomic, retain) NSStream	*os;
@property (readonly) BOOL iMagConnected;


@end
