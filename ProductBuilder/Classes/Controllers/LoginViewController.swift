//
//  LoginViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/13/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    let settingValues : NSMutableDictionary? = MainViewController50.sharedInstance()?.settingValues

    var username : String {
        if let value = settingValues?["username"] as? String {
            return value
        }
        return ""
    }

    var password : String? {
        if let value = settingValues?["password"] as? String {
            return value
        }
        return ""
    }

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var showPasswordButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    
    @IBAction func showPasswordButtonClick(_ sender: Any) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        showPasswordButton.setTitle(passwordTextField.isSecureTextEntry ? "Show Password" : "Hide Password", for: UIControlState.normal)
    }
    
    @IBAction func loginButtonClick(_ sender: Any) {
        if let currentUserName = usernameTextField.text, let currentPassword = passwordTextField.text {
            if currentUserName == username && SecurityManager.getPaswordHash(currentPassword) == password {
                let employee : Employee = Employee()
                employee.loginName = currentUserName
                employee.password = currentPassword
                SecurityManager.setCurrentEmployee(employee)
                self.navigationController?.dismiss(animated: true, completion: nil)
                return
            }
        }
        ModalAlert.show("Login failed", message: "Username or password is incorrect")
    }
    
    private func enableControls() {
        loginButton.isEnabled = usernameTextField.text != nil && passwordTextField.text != nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableControls()
        
        usernameTextField.textContentType = UITextContentType(rawValue: "")
        passwordTextField.textContentType = UITextContentType(rawValue: "")
    }

    override func viewDidAppear(_ animated: Bool) {
        usernameTextField.becomeFirstResponder()
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.selectAllNoMenu()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        enableControls()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
            loginButtonClick(loginButton)
        }
        return true
    }
}
