//
//  TextFieldTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 7/29/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
    
    static let nibName = "TextFieldTableViewCell50"

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var buttonPadding : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        button.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    public var isButtonHiden: Bool {
       set {
            self.button.isHidden = isButtonHiden
            buttonPadding.constant = (isButtonHiden ? -self.button.bounds.width - 8 : 8)
        }
        get {

            return self.button.isHidden
        }
    }

}
