//
//  TextValueTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 7/29/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class TextValueTableViewCell: CustomFieldTableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var arrowWidth: NSLayoutConstraint!
    @IBOutlet weak var arrowLeading: NSLayoutConstraint!
    
    override var readOnly:Bool {
        didSet {
            setArrowHidden(readOnly)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutMargins = UIEdgeInsets.zero
        arrowImageView.image = ImageHelper.tableCellArrowImage
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setLabelText(_ text:String?) {
        titleLabel.text = text
    }
    override func setValue(_ value:AnyObject?){
        
        if value == nil {
            valueLabel.textColor = ColorUtils.tableCellGrayTextColor()
            valueLabel.text = NSLocalizedString("NONE_TEXT", comment: "")
        } else {
            if let s =  value as? String {
                valueLabel.textColor = UIColor.black
                valueLabel.text = s
            } else {
                valueLabel.textColor = ColorUtils.tableCellGrayTextColor()
                valueLabel.text = NSLocalizedString("NONE_TEXT", comment: "")
            }
            
        }
    }
    
    func setArrowHidden(_ hidden: Bool) {
        arrowWidth.constant = hidden ? 0 : 15
        arrowLeading.constant = hidden ? 4 : 8
        arrowImageView.isHidden = hidden
    }

}
