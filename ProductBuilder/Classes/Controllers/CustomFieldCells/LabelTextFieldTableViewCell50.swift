//
//  LabelTextFieldTableViewCell.swift
//  RPlus
//
//  Created by PavelGurkovskii on 9/12/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class LabelTextFieldTableViewCell: CustomFieldTableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //valueTextField.placeholder = NSLocalizedString("ENTER_TEXT_HERE", comment: "")
        valueTextField.delegate = self
        valueTextField.autocorrectionType = .default
    }
    
    override  func setValue(_ value:AnyObject?) {
        super.setValue(value)
        //valueTextField.attributedPlaceholder = NSAttributedString(string:(self.readOnly ?  NSLocalizedString("NO_TEXT", comment: "") : NSLocalizedString("ENTER_TEXT_HERE", comment: "")), attributes:[NSForegroundColorAttributeName:ColorUtils.tableCellGrayTextColor()])
        if value != nil  {
            valueTextField.text = value as? String
        }
        
    }
    
    override func setLabelText(_ text:String?) {
        label.text = text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let del = self.customFieldDelegate {
            del.customFieldDidChangeValue(self, value: textField.text as AnyObject?)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return !self.readOnly
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !self.readOnly
    }
}
