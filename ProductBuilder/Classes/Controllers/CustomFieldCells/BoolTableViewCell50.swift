//
//  BoolTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 7/29/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

protocol BoolTableViewCellDelegate: AnyObject {
    func boolValueChanged(_ cell: BoolTableViewCell, control: CustomCheckboxControl)
}

class BoolTableViewCell: CustomFieldTableViewCell, CustomCheckboxControlDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var checkbox: CustomCheckboxControl!
    
    weak var delegate: BoolTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutMargins = UIEdgeInsets.zero
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        checkbox.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func valueChanged(inControl: CustomCheckboxControl) {
        delegate?.boolValueChanged(self, control: inControl)
        
        if let del = self.customFieldDelegate {
            del.customFieldDidChangeValue(self, value:inControl.isOn as AnyObject?)
        }
    }
    
    override func setLabelText(_ text:String?) {
        checkbox.isUserInteractionEnabled = !self.readOnly
        titleLabel.text = text
    }
    
    override func setValue(_ value:AnyObject?){
        if value == nil {
            checkbox.isOn = false
            valueLabel.text = NSLocalizedString("NO_TITLE", comment: "")
        } else {
            checkbox.isOn = (value as? Bool)!
            valueLabel.text = (value as? Bool)! ? NSLocalizedString("YES_TITLE", comment: "") : NSLocalizedString("NO_TITLE", comment: "")
        }
        checkbox.isHidden = self.readOnly
        valueLabel.isHidden = !self.readOnly
        
    }
}
