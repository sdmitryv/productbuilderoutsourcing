//
//  RTTableViewCell50.swift
//  ProductBuilder
//
//  Created by Alexander Martyshko on 2/14/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class RTBaseControlTableViewCell: UITableViewCell {
    
    fileprivate var stackView: UIStackView!
    var titleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initDefaults()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initDefaults()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initDefaults()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate var defaultsInitialized = false
    fileprivate func initDefaults() {
        if !defaultsInitialized {
            
            self.selectionStyle = UITableViewCellSelectionStyle.none
            
            titleLabel = RPDynamicFontLabel()
            titleLabel.font = UIFont.systemFont(ofSize: 17)
            
            var views: [UIView] = Array()
            views.append(titleLabel)
            
            if let controlView = controlView {
                views.append(controlView)
            }
            
            titleLabel.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, for: UILayoutConstraintAxis.horizontal)
            titleLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired + 1, for: UILayoutConstraintAxis.horizontal)
            
            stackView = UIStackView(arrangedSubviews: views)
            stackView.axis = .horizontal
            stackView.alignment = .center
            stackView.spacing = 4
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            self.contentView.addSubview(stackView)
            
            self.contentView.addConstraints([self.contentView.centerYAnchor.constraint(equalTo: stackView.centerYAnchor)])
            NSLayoutConstraint.activate([titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 21) , self.contentView.heightAnchor.constraint(equalTo: titleLabel.heightAnchor, multiplier: 1, constant: 2*verticalOffset)])
            
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[stackView]-16-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["stackView" : stackView]))
            
            defaultsInitialized = true
        }
    }
    
    var verticalOffset: CGFloat {
        return 12
    }
    
    var controlView: UIView? {
        return nil
    }

}

class RTSelectorButtonTableViewCell: RTBaseControlTableViewCell {
    
    private var _selectorButton: RTSelectorButton50?
    var selectorButton: RTSelectorButton50 {
        if _selectorButton == nil {
            _selectorButton = RTSelectorButton50()
            _selectorButton?.titleLabel?.font = UIFont.systemFont(ofSize: titleLabel.font.pointSize)
            _selectorButton?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            NSLayoutConstraint.activate([_selectorButton!.widthAnchor.constraint(greaterThanOrEqualToConstant: 40)])
        }
        return _selectorButton!
    }
    
    override var controlView: UIView? {
        return selectorButton
    }
}

class RTCheckboxTableViewCell: RTBaseControlTableViewCell {
    
    private var _checkbox: CustomCheckboxControl?
    var checkbox: CustomCheckboxControl {
        if _checkbox == nil {
            _checkbox = CustomCheckboxControl()
            NSLayoutConstraint.activate([_checkbox!.widthAnchor.constraint(greaterThanOrEqualToConstant: 40)])
            _checkbox?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        }
        return _checkbox!
    }
    
    override var controlView: UIView? {
        return checkbox
    }
}

class RTTextFieldTableViewCell: RTBaseControlTableViewCell {
    
    private var _textField: RPDynamicFontTextField?
    var textField: UITextField {
        if _textField == nil {
            _textField = RPDynamicFontTextField()
            _textField?.font = UIFont.systemFont(ofSize: 17)
            _textField?.textAlignment = NSTextAlignment.right
            
        }
        return _textField!
    }
    
    override var controlView: UIView? {
        return textField
    }
    
//    override var verticalOffset: CGFloat {
//        return 12
//    }
    
    fileprivate override func initDefaults() {
        super.initDefaults()
        
        titleLabel.setContentHuggingPriority(UILayoutPriorityRequired, for: UILayoutConstraintAxis.horizontal)
        titleLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: UILayoutConstraintAxis.horizontal)
        
        textField.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, for: UILayoutConstraintAxis.horizontal)
        textField.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow - 1, for: UILayoutConstraintAxis.horizontal)
    }
}

class RTLabelTableViewCell: RTBaseControlTableViewCell {
    
    private var _label: RPDynamicFontLabel?
    var label: RPDynamicFontLabel {
        if _label == nil {
            _label = RPDynamicFontLabel()
            _label?.font = UIFont.systemFont(ofSize: 17)
            _label?.textColor = UIColor.black
            _label?.textAlignment = .right
        }
        return _label!
    }
    
    override var controlView: UIView? {
        return label
    }
}

class RTSwitchTableViewCell: RTBaseControlTableViewCell {
    
    private var _switchControl: UISwitch?
    var switchControl: UISwitch {
        if _switchControl == nil {
            _switchControl = UISwitch()
        }
        return _switchControl!
    }
    
    override var controlView: UIView? {
        return switchControl
    }
}

class RTNumericTextFieldTableViewCell: RTBaseControlTableViewCell {
    
    private var _textField: UITextFieldNumeric?
    var textField: UITextFieldNumeric {
        if _textField == nil {
            _textField = UITextFieldNumeric()
            _textField?.font = UIFont.systemFont(ofSize: 17)
            _textField?.textAlignment = NSTextAlignment.right
            _textField?.autocorrectionType = .no
            _textField?.keyboardType = .numbersAndPunctuation
            _textField?.style = .numbersStyle
            _textField?.maxValue = (99999999)
            _textField?.minValue = (-99999999)
        }
        return _textField!
    }
    
    override var controlView: UIView? {
        return textField
    }
    
    fileprivate override func initDefaults() {
        super.initDefaults()
        
        titleLabel.setContentHuggingPriority(UILayoutPriorityRequired, for: UILayoutConstraintAxis.horizontal)
        titleLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, for: UILayoutConstraintAxis.horizontal)
        
        textField.setContentHuggingPriority(UILayoutPriorityDefaultLow - 1, for: UILayoutConstraintAxis.horizontal)
        textField.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow - 1, for: UILayoutConstraintAxis.horizontal)
    }
}

