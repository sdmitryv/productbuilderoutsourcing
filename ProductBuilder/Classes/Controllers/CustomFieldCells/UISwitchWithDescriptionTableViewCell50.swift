//
//  UISwitchWithDescriptionTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 10/17/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class UISwitchWithDescriptionTableViewCell: UITableViewCell {
    
    static let nibName = "UISwitchWithDescriptionTableViewCell50"
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//        switchControl.onTintColor = ColorUtils.mainRedColor()

        // Configure the view for the selected state
    }

}
