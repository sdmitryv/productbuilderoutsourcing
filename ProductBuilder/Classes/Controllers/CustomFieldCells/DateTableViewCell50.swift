//
//  DateValueTableViewCell.swift
//  RPlus
//
//  Created by Alexander Martyshko on 8/1/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

protocol DateTableViewCellDelegate: AnyObject {
    func cellSelected(_ cell: DateTableViewCell)
    func dateSelected(_ inCell: DateTableViewCell, date: Date?)
}

class DateTableViewCell: CustomFieldTableViewCell, UITextFieldDelegate {
    
    var datePicker : UIDatePicker!
    var onlyDate:Bool = false
    var clearEnabled:Bool = true
    
    @IBOutlet weak var fakeTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var arrowWidth: NSLayoutConstraint!
    @IBOutlet weak var arrowLeading: NSLayoutConstraint!
    var clear: UIBarButtonItem?
    
    weak var delegate : DateTableViewCellDelegate?
    var date : Date? {
        didSet {
            if date == nil || !clearEnabled {
                clear?.title = ""
                clear?.isEnabled = false
            } else {
                clear?.title = NSLocalizedString("CLEARBTN_TITLE", comment: "")
                clear?.isEnabled = true
            }
            datePicker.date = date ?? Date()
            updateDateValueLabel(date)
        }
    }

    override var readOnly:Bool {
        didSet {
            arrowWidth.constant = readOnly ? 0 : 15
            arrowLeading.constant = readOnly ? 4 : 8
            arrowImageView.isHidden = readOnly
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        arrowImageView.image = ImageHelper.tableCellArrowImage
        
        datePicker = UIDatePicker()
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        datePicker.date = date ?? Date()
        fakeTextField.inputView = datePicker
        
        let toolBar = UIToolbar()
        toolBar.frame = CGRect(x: 0, y: 0, width: 0, height: 44)
        toolBar.tintColor = ColorUtils.mainRedColor()
        toolBar.backgroundColor = UIColor.white
        fakeTextField.inputAccessoryView = toolBar
        let cancel = UIBarButtonItem(title: NSLocalizedString("CANCELBTN_TITLE", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DateTableViewCell.cancelTouched(_:)))
        clear = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DateTableViewCell.clearTouched(_:)))
        clear?.isEnabled = false
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(title: NSLocalizedString("DONE_TITLE", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(DateTableViewCell.doneTouched(_:)))
        toolBar.setItems([cancel, clear!, flexibleSpace, done], animated: false)
 
        let backView = UIView()
        backView.backgroundColor = ColorUtils.ColorWithRGB(red: 217, green: 217, blue: 217)
        self.selectedBackgroundView = backView
        
        fakeTextField.delegate = self
        
        updateDateValueLabel(date)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func cancelTouched(_ sender: AnyObject?) {
        fakeTextField.resignFirstResponder()
    }
    
    func clearTouched(_ sender: AnyObject?) {
        fakeTextField.resignFirstResponder()
        date = nil
        updateDateValueLabel(date)
        if let del = delegate {
            del.dateSelected(self, date: date)
        }
        
        if let del = self.customFieldDelegate {
            del.customFieldDidChangeValue(self, value:date as AnyObject?)
        }
    }
    
    func doneTouched(_ sender: AnyObject?) {
        fakeTextField.resignFirstResponder()
        date = datePicker.date
        updateDateValueLabel(date)
        if let del = delegate {
            del.dateSelected(self, date: date)
        }
        
        if let del = self.customFieldDelegate {
            del.customFieldDidChangeValue(self, value:date as AnyObject?)
        }
    }
    
    fileprivate func updateDateValueLabel(_ withDate: Date?) {
        if let d = withDate {
            valueLabel.textColor = UIColor.black
            let nearDateDescription = DateUtil.getInstance().nearDateDescription(d) ?? ""
            if onlyDate {
                valueLabel.text = nearDateDescription.characters.count > 0 ? nearDateDescription : DateUtil.getInstance().dateFormatter.string(from: d)
            } else {
                valueLabel.text = nearDateDescription.characters.count > 0 ? nearDateDescription + ", " + DateUtil.getInstance().timeFormatter.string(from: d) : DateUtil.getInstance().dateFormatter.string(from: d) + ", " + DateUtil.getInstance().timeFormatter.string(from: d)
            }
            
            
            
        }
        else {
            valueLabel.textColor = ColorUtils.tableCellGrayTextColor()
            valueLabel.text = NSLocalizedString("NONE_TEXT", comment: "")
        }
    }

    @IBAction func buttonTouched(_ sender: AnyObject) {
        if self.readOnly {
            return
        }
        fakeTextField.becomeFirstResponder()
        if let del = delegate {
            del.cellSelected(self)
        }

    }
    
    override func setValue(_ value:AnyObject?){
        onlyDate = true
        datePicker.datePickerMode = .date
        date = value as? Date

    }
    
    override func setLabelText(_ text:String?) {
        titleLabel.text = text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.isSelected = true
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.isSelected = false
        return true
    }
}
