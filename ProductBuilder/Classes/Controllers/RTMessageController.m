//
//  RTMessageController.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/5/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "RTMessageController.h"

@interface RTMessageController ()

@end

@implementation RTMessageController
@synthesize titleView;
@synthesize detailsView;
@synthesize detailsLabel;
@synthesize titleLabel;
@synthesize bClose;

- (id)init
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"RTMessageView" bundle:nil];
    }
    else {
        self = [super initWithNibName:@"RTMessageView_iPhone" bundle:nil];
    }
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [bClose setTitle:NSLocalizedString(@"CLOSEBTN_TITLE", nil)
            forState:UIControlStateNormal];
	// Do any additional setup after loading the view, typically from a nib.
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (void)dealloc {
    [titleView release];
    [detailsView release];
    [detailsLabel release];
    [titleLabel release];
    [bClose release];
    [super dealloc];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

-(void)adjustSize{

    titleLabel.frame = titleView.bounds;
    CGFloat oldTitleLabelHeight = titleLabel.bounds.size.height;
    [titleLabel sizeToFit];
    CGFloat deltaTitle = titleLabel.bounds.size.height - oldTitleLabelHeight;
    titleView.frame = CGRectMake(titleView.frame.origin.x, titleView.frame.origin.y,
                                 titleView.frame.size.width, titleView.bounds.size.height + deltaTitle);
    titleLabel.frame = titleView.bounds;

    detailsLabel.frame = detailsView.bounds;
    CGFloat oldDetailsLabelHeight = detailsLabel.bounds.size.height;
    [detailsLabel sizeToFit];
    CGFloat deltaDetails = detailsLabel.bounds.size.height - oldDetailsLabelHeight;
    
    detailsView.frame = CGRectMake(detailsView.frame.origin.x, detailsView.frame.origin.y + deltaTitle,
                                   detailsView.bounds.size.width, detailsView.bounds.size.height + deltaDetails);
    detailsLabel.frame = detailsView.bounds;
    //bClose.frame = CGRectMake(bClose.frame.origin.x, bClose.frame.origin.y + deltaTitle + deltaDetails,
    //                          bClose.bounds.size.width, bClose.bounds.size.height);
    self.view.bounds = CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height + deltaTitle + deltaDetails);
}

@end
