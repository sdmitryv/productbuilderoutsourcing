//
//  BaseListViewController.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/7/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "BaseListViewController.h"
#import "BaseDetailsViewController.h"
#import "SqlSortOrder.h"
#import "UITextField+Input.h"
#import "Global.h"
#import "SystemVer.h"
#import "UIView+FirstResponder.h"

@implementation BaseListViewController

@synthesize titleButton;
@synthesize toolbar;
@synthesize filterSegmentedControl;
@synthesize searchBar;
@synthesize itemsView;
@synthesize itemsPlaceHolderLabel;
@synthesize detailsTitleView;
@synthesize detailsButton;
@synthesize detailsView;
@synthesize totalView;
@synthesize totalLabel;
@synthesize contentView;
@synthesize pageControl;

- (id)initWithTitle:(NSString *)text  detailsController:(BaseDetailsViewController *)controller {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"BaseListView" bundle:nil];
    }
    else {
        if (IS_IPHONE_6_OR_MORE)
            self = [super initWithNibName:@"BaseListView_iPhone@3x" bundle:nil];
        else
            self = [super initWithNibName:@"BaseListView_iPhone" bundle:nil];
     }
    if (self) {
        _itemDetailsIsHidden = NO;
        title = [text retain];
        detailsController = [controller retain];
        self.countFormatString = NSLocalizedString(@"SEARCH_RESULTCOUNT_FORMAT_TEXT", nil);
    }
    return self;
}

- (void)dealloc
{
    [leftBarButtons release];
    [rightBarButtons release];
    [leftBarDividers release];
    [rightBarDividers release];
    [itemGridTable release];
    [titleButton release];
    [toolbar release];
    [filterSegmentedControl release];
    [searchBar release];
    [itemsView release];
    [detailsTitleView release];
    [detailsButton release];
    [detailsView release];
    [totalView release];
    [totalLabel release];
    
    [upArrow release];
    [downArrow release];
    [itemList release];
    [title release];
    [detailsController release];
    
    [contentView release];
    
    [pageControl release];
    
    [_countFormatString release];
    [searchBarButtonItem release];
    [_titleLabel release];
    [_barcodeButton release];
    [_buttonsView release];
    [_itemsBaseListView release];
    [_backView release];
    [itemsPlaceHolderLabel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(BOOL)shouldShowToolBarButton3{
    return FALSE;
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self buildToolBar];
    [filterSegmentedControl setTitle:NSLocalizedString(@"ALLBTN_TITLE", nil) forSegmentAtIndex:0];
    [filterSegmentedControl setTitle:NSLocalizedString(@"HELDBTN_TITLE", nil) forSegmentAtIndex:1];
    
    upArrow = [[UIImage imageNamed:@"2uparrow.png"] retain];
	downArrow = [[UIImage imageNamed:@"2downarrow.png"] retain];
    
    searchBar.placeholder = NSLocalizedString(@"SEARCH_TITLE", nil);
    searchBar.delegate = self;
    
    titleButton.text = title;
    _titleLabel.text = title;
    
    [self loadItems];

    if (IS_IPHONE_6_OR_MORE) {
        _itemsBaseListView.tableViewDelegateAndDataSource = self;
        _itemsBaseListView.delegate = self;
        _itemsBaseListView.headerHeight = 106;
        [_buttonsView removeFromSuperview];
        _buttonsView.frame = CGRectMake(0, 0, _itemsBaseListView.headerView.frame.size.width, 52);
        [_itemsBaseListView.headerView addSubview:_buttonsView];
        totalLabel = [_itemsBaseListView.countLabel retain];
        [_backView setRoundedCorners:UIViewRoundedCornerUpperLeft | UIViewRoundedCornerUpperRight radius:5.0f];
    }
    else {
        // Adjust table view
        
        itemGridTable = [[GridTable alloc] initWithFrame:itemsView.bounds];
        itemGridTable.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        itemGridTable.scrollPosition = UITableViewScrollPositionBottom;
        [self adjustTableView];
        
        [itemsView addSubview:itemGridTable];
        
        if (detailsController != nil) {
            
            [detailsView addSubview:detailsController.view];
            [detailsController displayItem:nil];
        }
        else {
            
            CGRect rect = itemsView.frame;
            rect.size.height = detailsView.frame.origin.y + detailsView.frame.size.height - rect.origin.y;
            itemsView.frame = rect;
            [detailsView removeFromSuperview];
            [detailsTitleView removeFromSuperview];
        }
        
        itemGridTable.delegate = self;
        [itemGridTable selectRow:0];
        
        if (IS_IPHONE) {
            CGRect frame = itemGridTable.frame;
            frame.size.width = 960;
            itemGridTable.frame = frame;
            ((UIScrollView*)itemsView).contentSize = frame.size;
        }
    }
    //[totalView setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight radius:5.0f];
    [detailsTitleView setRoundedCorners:UIViewRoundedCornerUpperLeft | UIViewRoundedCornerUpperRight radius:5.0f];
    //searchBar.transform = CGAffineTransformMakeScale(1.2, 1.2);
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        [searchBar textField].font = [UIFont systemFontOfSize:17];
    searchBar.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
        searchBar.frame = CGRectMake(searchBar.frame.origin.x, searchBar.frame.origin.y, searchBar.frame.size.width, 56);
    else {
        
        CGFloat toolbarHeight = toolbar.frame.size.height;
        toolbar.frame = CGRectMake(toolbar.frame.origin.x, toolbar.frame.origin.y, toolbar.frame.size.width, 60);
        itemsView.frame = CGRectMake(itemsView.frame.origin.x,
                                     itemsView.frame.origin.y + (60 - toolbarHeight),
                                     itemsView.frame.size.width,
                                     itemsView.frame.size.height - (60 - toolbarHeight));
                                     
        searchBar.frame = CGRectMake(searchBar.frame.origin.x, searchBar.frame.origin.y, searchBar.frame.size.width, 60);
    }
    
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
        toolbar.barStyle = UIBarStyleBlack;
#endif
}

#pragma mark BarButton Items


-(UIView*)createToolBarDeviderView{
    
    UIView* dividerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 33 : 50)];
    dividerView.backgroundColor = [UIColor darkGrayColor];
    return [dividerView autorelease];
}

-(void)buildToolBar{
    
    NSMutableArray *toolBarItems = [[NSMutableArray alloc]initWithArray:toolbar.items copyItems:FALSE];
    [leftBarButtons release];
    leftBarButtons = [NSMutableArray new];
    [rightBarButtons release];
    rightBarButtons = [NSMutableArray new];
    [leftBarDividers release];
    leftBarDividers = [NSMutableArray new];
    [rightBarDividers release];
    rightBarDividers = [NSMutableArray new];
    
    NSMutableArray *fullLeftBarButtons = [NSMutableArray new];
    NSMutableArray *fullRightBarButtons = [NSMutableArray new];
    
    NSInteger leftCnt = [self leftBarButtonsCount];
    NSInteger rightCnt = [self rightBarButtonsCount];
    
    if (leftCnt)
    for (NSUInteger i = 0; i <= [self leftBarButtonsCount]; i++){
        
        if (i > 0) {
         
            NSString *itemTitle = [self leftBarButtonTitleAtIndex:i - 1];
            if (!itemTitle) itemTitle = @"";
            
            UIButton *toolBatton = [UIButton buttonWithType:UIButtonTypeCustom];
            [toolBatton setTitle:itemTitle forState:UIControlStateNormal];
            [toolBatton addTarget:self action:@selector(leftBarButtonClick:) forControlEvents:UIControlEventTouchDown];
            toolBatton.titleLabel.font = [UIFont systemFontOfSize:17];
            [toolBatton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [toolBatton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
            
            CGFloat toolBattonWidth = [itemTitle sizeWithAttributes:@{NSFontAttributeName:toolBatton.titleLabel.font}].width + 6;
            toolBatton.frame = CGRectMake(0,
                                          0,
                                          toolBattonWidth,
                                          [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 34 : 50);

            UIBarButtonItem* item = [[UIBarButtonItem alloc]initWithCustomView:toolBatton];
            item.tintColor = [UIColor whiteColor];
            [leftBarButtons addObject:toolBatton];
            [fullLeftBarButtons addObject:item];
            [item release];
        }
        
        UIView *dividerView = [self createToolBarDeviderView];
        UIBarButtonItem* dividerItem = [[UIBarButtonItem alloc]initWithCustomView:dividerView];
        [leftBarDividers addObject:dividerView];
        [fullLeftBarButtons addObject:dividerItem];
        [dividerItem release];
    }
    
    if (rightCnt)
    for (NSUInteger i = 0; i <= [self rightBarButtonsCount]; i++){
        
        if (i > 0) {
            
            NSString *itemTitle = [self rightBarButtonTitleAtIndex:i - 1];
            
            if (!itemTitle) itemTitle = @"";
            
            UIButton *toolBatton = [UIButton buttonWithType:UIButtonTypeCustom];
            [toolBatton setTitle:itemTitle forState:UIControlStateNormal];
            [toolBatton addTarget:self action:@selector(rightBarButtonClick:) forControlEvents:UIControlEventTouchDown];
            toolBatton.titleLabel.font = [UIFont systemFontOfSize:17];
            [toolBatton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [toolBatton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
            
            CGFloat toolBattonWidth = [itemTitle sizeWithAttributes:@{NSFontAttributeName:toolBatton.titleLabel.font}].width + 6;
            toolBatton.frame = CGRectMake(0,
                                          0,
                                          toolBattonWidth,
                                          [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 34 : 50);
            
            UIBarButtonItem* item = [[UIBarButtonItem alloc]initWithCustomView:toolBatton];
            item.tintColor = [UIColor whiteColor];
            [rightBarButtons addObject:toolBatton];
            [fullRightBarButtons addObject:item];
            [item release];
        }
        
        UIView *dividerView = [self createToolBarDeviderView];
        UIBarButtonItem* dividerItem = [[UIBarButtonItem alloc]initWithCustomView:dividerView];
        [rightBarDividers addObject:dividerView];
        [fullRightBarButtons addObject:dividerItem];
        [dividerItem release];
    }
    
    NSInteger searchIndex = [toolBarItems indexOfObject:searchBarButtonItem];
    
    if (fullLeftBarButtons.count)
        [toolBarItems insertObjects:fullLeftBarButtons
                          atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange((searchIndex == NSNotFound ? 0 : searchIndex) + 1, fullLeftBarButtons.count)]];
    
    if (fullRightBarButtons.count)
        [toolBarItems addObjectsFromArray:fullRightBarButtons];

    
    toolbar.items = toolBarItems;
    [toolBarItems release];
    [fullLeftBarButtons release];
    [fullRightBarButtons release];
}

-(NSUInteger)leftBarButtonsCount{
    return 0;
}

-(NSUInteger)rightBarButtonsCount{
    return 0;
}

-(NSString*)leftBarButtonTitleAtIndex:(NSUInteger)index{
    return nil;
}

-(NSString*)rightBarButtonTitleAtIndex:(NSUInteger)index{
    return nil;
}

-(void)leftBarButtonClickAtIndex:(NSUInteger)index{
    
}

-(void)rightBarButtonClickAtIndex:(NSUInteger)index{

}

-(UIButton*)leftBarButtonByIndex:(NSUInteger)index{
    if (index >=leftBarButtons.count) return nil;
    return leftBarButtons[index];
}

-(UIButton*)rightBarButtonByIndex:(NSUInteger)index{
    if (index >=rightBarButtons.count) return nil;
    return rightBarButtons[index];
}

-(UIView*)leftBarDividerByIndex:(NSUInteger)index{
    if (index >= leftBarDividers.count) return nil;
    return leftBarDividers[index];
}

-(UIView*)rightBarDividerByIndex:(NSUInteger)index{
    if (index >= rightBarDividers.count) return nil;
    return rightBarDividers[index];
}

-(void)leftBarButtonClick:(UIBarButtonItem*)sender{
    NSInteger idx = [leftBarButtons indexOfObject:sender];
    if (idx!=NSNotFound){
        [self leftBarButtonClickAtIndex:idx];
    }
}

-(void)rightBarButtonClick:(UIBarButtonItem*)sender{
    NSInteger idx = [rightBarButtons indexOfObject:sender];
    if (idx!=NSNotFound){
        [self rightBarButtonClickAtIndex:idx];
    }
}



#pragma mark - Actions

- (IBAction) returnButtonClick {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)detailsButtonClick {
    [self setDetailsIsHidden:!_itemDetailsIsHidden animated:TRUE];
}

-(void)setDetailsIsHidden:(BOOL)value animated:(BOOL)animated{
    if (_itemDetailsIsHidden==value) return;
    _itemDetailsIsHidden = value;
    void(^hideDetails)(void) = ^{
        if (!_itemDetailsIsHidden)
            detailsView.hidden = FALSE;
        int deltaY = (_itemDetailsIsHidden ? 1 : -1) * detailsView.frame.size.height;
        detailsTitleView.frame = CGRectOffset(detailsTitleView.frame, 0, deltaY);
        detailsView.frame = CGRectOffset(detailsView.frame, 0, deltaY);
        CGRect itemsRect = itemsView.frame;
        itemsRect.size.height += deltaY;
        itemsView.frame = itemsRect;
        [detailsButton setImage:_itemDetailsIsHidden ? upArrow : downArrow forState:UIControlStateNormal];
        if (animated){
            CATransition *transition = [CATransition animation];
            transition.duration = 0.5f;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionFade;
            [detailsButton.layer addAnimation:transition forKey:nil];
        }
    };
    void(^comp)(BOOL) = ^(BOOL finished){
        [itemGridTable selectRow:itemGridTable.selectedRowIndex animated:YES];
        if (_itemDetailsIsHidden)
            detailsView.hidden = TRUE;
    };
    if (animated){
        [UIView animateWithDuration:0.3f animations:hideDetails completion:comp];
    }
    else{
        hideDetails();
        comp(TRUE);
    }
}


- (IBAction)filterValueChanged:(id)sender {
}

- (IBAction)controlPageValueChanged:(id)sender {
    [(UIScrollView*)itemsView setContentOffset:CGPointMake(pageControl.currentPage * itemsView.frame.size.width, 0) animated:YES];
}

- (IBAction)keyboardButtonClick {
    [searchBar resignFirstResponder];
}


- (void)barcodeButtonClick:(id)sender {

}


#pragma mark -
#pragma mark GridTableDelegate

- (void)gridTable:(GridTable*)gridTable setContentForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content {
}

- (NSInteger)gridTableNumberOfRows:(GridTable*)gridTable {
    totalLabel.text = [NSString stringWithFormat:NSLocalizedString(@"OPERATION_RECORDSPROGRESS_FORMAT_TEXT", nil), itemList.count];
	return itemList.count;
}

- (void)gridTable:(GridTable*)gridTable sortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order {
    NSString* sortingColumn = column.name;
    if (order != NotSorted){
        itemList.sortOrderList = @[[SqlSortOrder orderBy:sortingColumn order:order == AscendingSorted ? SqlSortOrderAscending : SqlSortOrderDescending]];
    }
    else{
        itemList.sortOrderList = nil;
    }
}

- (void)gridTable:(GridTable*)gridTable didSelectRow:(NSInteger)row {
    [detailsController displayItem:[itemList objectAtIndex:row]];
}

- (void)gridTable:(GridTable*)gridTable didDeselectRow:(NSInteger)row {
    [detailsController displayItem:nil];
}

#pragma mark -
#pragma mark UISearchBarDelegate

// called when keyboard search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar.textField selectAllNoMenu];
}

#pragma mark -
#pragma mark Virtual methods

- (void) loadItems {
}

- (void)adjustTableView {
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControl.currentPage = scrollView.contentOffset.x / itemsView.frame.size.width;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    totalLabel.text = [NSString stringWithFormat:self.countFormatString, itemList.count];
    return itemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView dequeueReusableCellWithIdentifier:@""];;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - Table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - RTaseListView delegate methods

- (void)baseList:(RTBaseListView *)listView sortWithColumn:(GridTableColumnDescription *)column order:(SortingMode)order {
    NSString* sortingColumn = column.name;
    if (order != NotSorted){
        itemList.sortOrderList = @[[SqlSortOrder orderBy:sortingColumn order:order == AscendingSorted ? SqlSortOrderAscending : SqlSortOrderDescending]];
    }
    else {
        itemList.sortOrderList = nil;
    }
    [_itemsBaseListView.tableView reloadData];
}

@end
