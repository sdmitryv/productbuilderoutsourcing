//
//  RTDialogView.swift
//  CloudworksPOS
//
//  Created by dsm on 12/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

enum RTDialogResult50: Int {
    case cancel = 0
    case ok = 1
}

class RTNavigationController50 : UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    private var disappeared = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disappeared = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disappeared = true
    }
    
    var completionBlock: (()->Void)?
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag) { [weak self] in
            if (self?.disappeared ?? false) {
                self?.completionBlock?()
            }
            completion?()
        }
    }
}

class RTDialogController50: UIViewControllerExtended{
    
    private(set) var saving: Bool = false
    var cancelling: Bool = false
    
    private var _saveHidden: Bool = true
    private var _cancelHidden: Bool = true


    var saveHidden: Bool {
        get {
            
            return _cancelHidden
        }
        set(newSaveHidden) {
            
            _saveHidden = newSaveHidden;
            
            if saveButton != nil {
                
                self.navigationItem.rightBarButtonItem = (_saveHidden ? nil : saveButton)
            }
        }
    }
    
    var cancelHidden: Bool {
        get {
            
            return _cancelHidden
        }
        set(newCancelHidden) {
            
            _cancelHidden = newCancelHidden;
            
            if cancelButton != nil {
                
                self.navigationItem.leftBarButtonItem = (_cancelHidden ? nil : cancelButton)
            }
        }
    }
    
    var dialogResult: RTDialogResult50 = RTDialogResult50.cancel
    var ignoredAlertViewOnSave: RTAlertView? = nil
    
    var saveButton : UIBarButtonItem?
    var cancelButton : UIBarButtonItem?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let bar = self.navigationController?.navigationBar
        bar?.barStyle = UIBarStyle.default
        bar?.barTintColor = UIColor.init(colorLiteralRed: 0, green: 120/255.0, blue: 230/255.0, alpha: 1)
        bar?.tintColor = UIColor.white
        bar?.backgroundColor = UIColor.clear
        bar?.isTranslucent = true
        bar?.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.view.backgroundColor = UIColor.init(colorLiteralRed: 247/255.0, green: 249/255.0, blue: 251/255.0, alpha: 1)
        
        saveButton = UIBarButtonItem(title: selectButtonTitle(), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RTDialogController50.saveClick))
        cancelButton = UIBarButtonItem(title: cancelButtonTitle(), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RTDialogController50.cancelClick))
        
        if !_saveHidden {
            
            self.navigationItem.rightBarButtonItem = saveButton
        }
        
        if !_cancelHidden {
            
             self.navigationItem.leftBarButtonItem = cancelButton
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.title = dilogTitle()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if completionBlock != nil {
            
            completionBlock()
            completionBlock = nil
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    var validate : ((Void)->Bool)?
    
    func dilogTitle() -> String? {
        return "";
    }

    func selectedValue() -> AnyObject?{
        return nil;
    }

    func setSelectedValue(_ value:AnyObject?){
    
    }

    func selectButtonTitle() -> String{
        return NSLocalizedString("SAVEBTN_TITLE", comment: "")
    }

    func cancelButtonTitle() -> String{
        return NSLocalizedString("CANCELBTN_TITLE", comment: "")
    }
    
    
    @IBAction func saveClick() {
        self.save()
    }
    
    func save(){
        
        saving = true
        
        if let responder = self.view.getFirstResponder() {
            
            if responder.canResignFirstResponder{
                
                responder.resignFirstResponder()
            }
            else {
                
                saving = false;
                return;
            }
        }
        
        let topAlertView = RTAlertView.topMostAlertView()
        if (topAlertView != nil && topAlertView != self.ignoredAlertViewOnSave) {
            
            self.saving = false
            return
        }
        
        if !willSave() {
            
            self.saving = false
            return
        }
        
        self.dialogResult = RTDialogResult50.ok
        self.close()
        saving = false
    }
    
    
    @IBAction func cancelClick() {
        self.cancel()
    }
    
    func cancel(){
        
        self.cancelling = true
        if let responder = self.view.getFirstResponder() {
            
            responder.resignFirstResponder()
        }
        
        if !willCancel() {
            return
        }
        
        self.dialogResult = RTDialogResult50.cancel
        self.close()
        cancelling = false
    }
    
    func close(){
        
        willClose()
        if let navigationController = self.navigationController{
            navigationController.dismiss(animated: true, completion: {
                
                self.didClose()
            })
        }
        else if self.parentModal != nil{
            self.dismiss(animated: true, completion: {
                self.didClose()
            })
        }
    }
    

    func willSave() -> Bool {
        
        if self.validate != nil {
            
            if !self.validate!() {
                
                setSelectedValue(nil)
                return false;
            }
        }
        
        return true;
    }
    
    func willCancel() -> Bool{
        return true
    }
    
    func willClose(){
        
    }
    
    func didClose(){
        
        self.validate = nil;
    }
}



