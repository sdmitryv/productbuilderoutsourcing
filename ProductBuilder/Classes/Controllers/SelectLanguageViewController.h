//
//  SelectLanguageViewController.h
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 4/22/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "RTDialogViewController.h"
#import "RTSelectorButton.h"

@interface SelectLanguageViewController : RTDialogContentViewController

@property (nonatomic, retain) IBOutlet RTUnderlinedSelectorButton   *languageSelector;

@end
