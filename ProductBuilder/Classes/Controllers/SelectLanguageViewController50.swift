//
//  SelectLanguageViewController50.swift
//  ProductBuilder
//
//  Created by Olga Ivchenko on 1/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

class SelectLanguageViewController50 : RTDialogController50, UITableViewDelegate, UITableViewDataSource {
    
    static let cellReuseIdentifier = "LanguageCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentLanguage : String? = nil
    var savedLanguage : String? = nil
    var languagesArray : NSMutableArray = NSMutableArray()
    
    required init(){
        
        super.init(nibName: "SelectLanguageView50", bundle: Bundle.main)
        
        self.cancelHidden = false
        self.saveHidden = false
        
        if let lang = AppSettingManager.instance().deviceLanguage {
            
            currentLanguage = lang
            savedLanguage = lang
        }

        if let filePath = Bundle.main.path(forResource: "LanguageList", ofType: "plist"),
            let arrFromFile = NSArray(contentsOfFile: filePath) {
            languagesArray.addObjects(from: arrFromFile as [AnyObject])
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tableView.register(UINib(nibName: UISwitchWithDescriptionTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: UISwitchWithDescriptionTableViewCell.nibName)
        self.saveButton?.isEnabled = false
    }
    
    override func dilogTitle() -> String? {
        
         return NSLocalizedString("SELECT_LANGUAGE_TITLE", comment: "")
    }
    
    override func selectButtonTitle() -> String {
    
        return NSLocalizedString("DONE_BTN_TITLE", comment: "")
    }
    
    override func willSave() -> Bool {
        
        var languageStr = NSLocalizedString("SELECT_SYSTEM_LANGUAGE", comment: "")
        if currentLanguage != nil {
            
            for case let lang as NSDictionary in languagesArray {
                
                if lang["Key"] as! String == currentLanguage! {
                    
                    languageStr = lang["Title"] as! String
                    break
                }
            }
        }
        
        let alertTitle = NSLocalizedString("SELECT_LANGUAGE_MSG_TITLE", comment: "") + " " + languageStr
        
        ModalAlert.ask(withTitle: alertTitle, question: NSLocalizedString("SELECT_LANGUAGE_MSG", comment: ""), withCancel: NSLocalizedString("CANCELBTN_TITLE", comment: ""), withButtons: [NSLocalizedString("CONTINUEBTN_TITLE", comment: "")], completed: {(index) -> Void in
            
            if index == 0 {
                
                if let lang = AppSettingManager.instance().deviceLanguage {
                    
                    self.currentLanguage = lang
                    self.savedLanguage = lang
                }
                else if let languageDict = self.languagesArray[0] as? NSDictionary, let lang = languageDict["Key"] as? String {
                    self.currentLanguage = lang
                }
                self.saveButton?.isEnabled = false
                self.tableView.reloadData()
            }
            else {

                AppSettingManager.instance().deviceLanguage = self.currentLanguage
                if self.currentLanguage != nil {
                    UserDefaults.standard.set([self.currentLanguage], forKey: "AppleLanguages")
                }
                else {
                    UserDefaults.standard.removeObject(forKey: "AppleLanguages")
                }
                UserDefaults.standard.synchronize()
                
                self.close()
            }
        })
        
        return false
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        if sender.isOn {
            
            self.currentLanguage = nil
        }
        else if let languageDict = self.languagesArray[0] as? NSDictionary, let lang = languageDict["Key"] as? String {
            self.currentLanguage = lang
        }
        
        tableView.reloadData()
        
        self.saveButton?.isEnabled = savedLanguage != currentLanguage
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            return nil
        }
        
        let backView = UIView()
        backView.backgroundColor = self.view.backgroundColor
        
        let label = GrayLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        
        backView.addSubview(label)
        
        let viewsDict = ["label" : label]
        backView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[label]-6-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        backView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[label]-16-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        label.text = NSLocalizedString("MANUALLY_SELECTED_LANGUAGE", comment: "")

        return backView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 1
        }
        
        return RPDynamicFontLabel.dynamicTextHeight(withFontPointSize: 17) + 6
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        }

        return languagesArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return currentLanguage == nil ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: UISwitchWithDescriptionTableViewCell.nibName) as! UISwitchWithDescriptionTableViewCell
           
            if cell.switchControl.allTargets.count == 0 {
                
                let langCode = Locale.current.languageCode!
                let locale = NSLocale.init(localeIdentifier: langCode)
                var language = locale.displayName(forKey: NSLocale.Key.identifier, value:langCode)!
                language = language.capitalized(with: Locale.current)
                //let regionCode = Locale.current.regionCode!
                cell.titleLabel.text = NSLocalizedString("SELECT_SYSTEM_LANGUAGE", comment: "")
                
                
                cell.descriptionLabel.text = "";
                for case let lang as NSDictionary in languagesArray {
                    
                    //if lang["Key"] as! String == langCode {
                    if (lang["Key"] as! String).range(of:langCode) != nil {
                        
                        cell.descriptionLabel.text =  String(format:  NSLocalizedString("SELECT_SYSTEM_LANGUAGE2", comment: ""), language)
                        break
                    }
                }
                if cell.descriptionLabel.text == "" {
                    
                    cell.descriptionLabel.text =  String(format:  NSLocalizedString("SELECT_SYSTEM_LANGUAGE3", comment: ""), language)
                }

                
                
                cell.switchControl.addTarget(self, action: #selector(switchValueChanged(_:)), for: UIControlEvents.valueChanged)
            }
            
            cell.switchControl.isOn = currentLanguage == nil
            
            return cell
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: SelectLanguageViewController50.cellReuseIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: SelectLanguageViewController50.cellReuseIdentifier)
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
        }
        
        if let languageDict = languagesArray[(indexPath as NSIndexPath).row] as? NSDictionary {
            cell?.textLabel?.text = languageDict["Title"] as? String
            
            if let lang = languageDict["Key"] as? String , lang == currentLanguage {
                cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            }  
            else {
                cell?.accessoryType = UITableViewCellAccessoryType.none
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let languageDict = self.languagesArray[(indexPath as NSIndexPath).row] as? NSDictionary, let lang = languageDict["Key"] as? String {
        
            self.currentLanguage = lang
            tableView.reloadData()
        }
        
        self.saveButton?.isEnabled = savedLanguage != currentLanguage
    }
}
