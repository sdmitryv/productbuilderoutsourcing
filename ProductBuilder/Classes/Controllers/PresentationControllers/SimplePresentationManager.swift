//
//  SimplePresentationManager.swift
//  TeamworkPOS
//
//  Created by valery on 12/21/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

enum PresentationDirection {
  case left
  case top
  case right
  case bottom
}

final class SimplePresentationManager: NSObject {

  // MARK: - Properties
    var direction = PresentationDirection.left
    var sizeCoefficient : CGFloat = 1.0
    var hidesNavigationBar : Bool = false
    var disableCompactHeight = false
}

// MARK: - UIViewControllerTransitioningDelegate
extension SimplePresentationManager: UIViewControllerTransitioningDelegate {

  func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
    let presentationController = SimplePresentationController(presentedViewController: presented, presenting: presenting, direction: direction, sizeCoefficient:sizeCoefficient)
        presentationController.delegate = self
    //presentationController.hidesNavigationBar = hidesNavigationBar
    return presentationController
  }

  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    let animator = SimplePresentationAnimator(direction: direction, isPresentation: true)
    animator.sourceController = source
    animator.hidesNavigationBar = hidesNavigationBar
    return animator
  }

  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return SimplePresentationAnimator(direction: direction, isPresentation: false)
  }
}

// MARK: - UIAdaptivePresentationControllerDelegate
extension SimplePresentationManager: UIAdaptivePresentationControllerDelegate {

  func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
    if traitCollection.verticalSizeClass == .compact && disableCompactHeight {
      return .overFullScreen
    } else {
      return .none
    }
  }
//
//  func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
//    guard case(.overFullScreen) = style else { return nil }
//
//    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RotateViewController")
//  }
}
