//
//  RPBackgroundOperationViewController.swift
//  RPlusTeamwork
//
//  Created by Alexander Martyshko on 7/6/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import UIKit

class BackgroundOperationController50: BackgroundOperationViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var progressBar: YLProgressBar!
    @IBOutlet weak var activityLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    fileprivate var _hideCancel: Bool = true
    @IBOutlet weak var heightConstraint : NSLayoutConstraint!
    
    fileprivate var _alertView : RTAlertView50?
    fileprivate var _task : (() -> Void?)? = nil
    fileprivate var _completed : (() -> Void?)? = nil
    fileprivate var _alertClosing : Bool? = false
    
    
    
    var originalHeight : CGFloat = 0
    
    init!(title: String?, message: String?, hideCancel: Bool) {
        
        super.init(style: .alert)
        
        if (hideCancel) {
            
            _alertView = RTAlertView50(title: message == nil && title != nil ? (" \n" + title! + "\n ") : title, message: title == nil && title != nil ? (" \n" + message! + "\n ") : message)
        }
        else {
            
            _alertView = RTAlertView50(title: message == nil && title != nil ? (" \n" + title! + "\n ") : title, message: title == nil && title != nil ? (" \n" + message! + "\n ") : message, delegate: nil, cancelButtonTitle: NSLocalizedString("CLOSEBTN_TITLE", comment:""), otherButtonTitle:nil)
        }
        
        _hideCancel = hideCancel
    }
    
    override init!(style: BackgroundOperationStyle) {
        
        super.init(style: style)
    }
    
    
    convenience init() {
    
        self.init(style: BackgroundOperationStyle.custom)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
    
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
    
        super.viewDidLoad()

        if (self.style == .alert) {
            return
        }
        self.view.layer.shadowColor = UIColor.clear.cgColor
        bCancel.setTitle(NSLocalizedString("CLOSEBTN_TITLE", comment:""), for: UIControlState.normal)
    }

    
    override func nibName(for style: BackgroundOperationStyle) -> String! {
        
        if (style == .custom2Lines) {
            return "BackgroundOperationView_250"
        }
        
        return "BackgroundOperationView50"
    }
    
    
    override var hideCancel: Bool {
        
        set {
            
            if !Thread.isMainThread {
                
                DispatchQueue.main.async{ [weak self] in
                    self?.hideCancel = newValue
                }
                return
            }
            
            _hideCancel = newValue;
            
            if self.view != nil {
                
                if (originalHeight == 0) {
                    
                    originalHeight = self.view.bounds.size.height
                }
                
                if (self.style == .alert) {
                    return
                }
               
                bCancel.superview?.isHidden = _hideCancel
                heightConstraint.constant = _hideCancel ? 0 : 44
                var rect = self.view.bounds
                rect.size.height = originalHeight - (_hideCancel ? 44 : 0)
                self.view.bounds = rect
            }
        }
        get {
            
            return _hideCancel
        }
    }
    
    
    func setProgressValue(_ value: Float) {
        
        if !Thread.isMainThread {
            
            DispatchQueue.main.async{ [weak self] in
                self?.setProgressValue(value)
            }
            return
        }
    }
   
    override var title : String?{
        
        get{
            
            if (self.style == .alert) {
                return _alertView?.title
            }
            
            return activityLabel.text
        }
        set(newTitle){
            
            if !Thread.isMainThread {
                
                DispatchQueue.main.async{ [weak self] in
                    self?.title = newTitle!
                }
                return
            }
            
            if (self.style == .alert) {
                return
            }
            
            if self.view != nil{
                
                activityLabel.text = newTitle
            }
        }
    }
    
    
    func startAlertTask(task : (() -> Void)?, completed : (() -> Void)?)  {
        
        if (self.style != .alert) {
            
            super.startTask(task, completed: completed)
            
            return
        }
        
        _task = task
        _completed = completed
        _alertClosing = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.alertShowed),
                                               name: NSNotification.Name.RTAlertViewDidPresent,
                                               object:_alertView)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.runCompliteBlock),
                                               name: NSNotification.Name.RTAlertViewDidDismiss,
                                               object:nil)
        
        _alertView?.completedBlock = { (buttonIndex : NSInteger) in
            
            self.closeAlert(isCanceling: true)
        }
        _alertView?.show()
    }
    
    func alertShowed(_ notification: Notification) {
        
        if ((notification.object as! RTAlertView50) == _alertView) {
            
            NotificationCenter.default.removeObserver(self)
            
            DispatchQueue.global().async {
                
                if (self._task != nil) {
                    self._task!()
                }
                else {
                    return
                }
                
                DispatchQueue.main.async {
                    
                    self.closeAlert(isCanceling: false)
                }
            }
            
            return
        }
    }
    
    
    func runCompliteBlock() {
        
        if (UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        
        if (self._completed != nil) {
            self._completed!()
        }
    }
    
    
    func closeAlert(isCanceling: Bool) {
        
        if (_alertClosing!) {
            
            return
        }

        _alertClosing = true
        
        if (isCanceling && (onCancel != nil)) {
            
            NotificationCenter.default.removeObserver(self)
            onCancel()
        }
        
        if (!isCanceling) {
            
            
            _alertView?.dismissAlert()
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.perform(#selector(self.runCompliteBlock), with: nil, afterDelay: 1)
        }
    }
    
    
    override func show() {
    
        startAlertTask(task: nil, completed: nil)
    }
    
    override func hide() {
        
        if (self.style != .alert) {
            
            super.hide()
            return
        }
        
        closeAlert(isCanceling: false)
    }

}
