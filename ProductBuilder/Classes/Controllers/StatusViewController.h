//
//  StatusViewController.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 9/28/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface StatusViewController : UIViewController {
	UIButton	*associateButton;
	UIButton	*storeNameButton;
	UIButton	*buildButton;
	UIButton	*synchronizedButton;
    UIButton *logo;
    UIButton    *deviceIdButton;
    NSTimer* timer;
    NSDate      *syncDate;
    NSTimer* updateLastSyncTimer;
}

@property (nonatomic, retain) IBOutlet UIButton		*associateButton;
@property (nonatomic, retain) IBOutlet UIButton		*storeNameButton;
@property (nonatomic, retain) IBOutlet UIButton		*buildButton;
@property (nonatomic, retain) IBOutlet UIButton		*synchronizedButton;
@property (nonatomic, retain) IBOutlet UIButton *logo;
@property (nonatomic, retain) IBOutlet UIButton     *deviceIdButton;

@property (nonatomic, assign) NSString * associate;
@property (nonatomic, assign) NSString * storeName;
@property (nonatomic, retain) NSDate   * syncDate;
@property (atomic, retain) NSDate   * lastSyncTime;

+(StatusViewController*) sharedInstance;
//-(void)setStatus:(BOOL)online;
-(void)updateDeviceIdTitle;

@end
