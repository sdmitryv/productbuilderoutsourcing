//
//  CopyBackupViewController50.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 1/12/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

class CopyBackupViewController : UIViewController{
    enum CopyBackupCurrentState  {
        case notStartedState
        case inProgressState
        case sendingState
    }
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var startBackupButton :  UIButton!
    @IBOutlet weak var infoLabel : UILabel!
    @IBOutlet weak var view2: UIView!
    @IBOutlet var progressView: YLProgressBar?
    @IBOutlet weak var backupInProgressLabel : UILabel!
    @IBOutlet weak var percentageLabel : UILabel!
    @IBOutlet weak var cancelBackupButton :  MOButtonBlueTransparent!
    
    let backupQueue = BackupOperationQueue.backupQueuesharedInstance
    var backupOperation : BackupDbOperation?
    
    required init(){
        super.init(nibName: "CopyBackupView50", bundle: Bundle.main)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        startBackupButton.setTitle(NSLocalizedString("SETTINGS_COPY_BACKUP_BUTTON_TITLE", comment: ""),for: .normal)
        startBackupButton.addTarget(self, action: #selector(self.startBackupButtonClick(_:)), for: .touchUpInside)
        infoLabel.text = NSLocalizedString("SETTINGS_COPY_BACKUP_INFO_LABEL_TITLE", comment: "")
        backupInProgressLabel.text = NSLocalizedString("SETTINGS_COPY_BACKUP_IN_PROGRESS_TITLE", comment: "")
        progressView?.progress = 0.0
        progressView?.behavior = YLProgressBarBehavior.default
        progressView?.hideStripes = true
        progressView?.type = YLProgressBarType.flat
        progressView?.trackTintColor = UIColor(colorLiteralRed: 197/255.0, green: 199/255.0, blue: 200/255.0, alpha: 1)
        progressView?.progressTintColors = [UIColor(colorLiteralRed: 0/255.0, green: 139/255.0, blue: 232/255.0, alpha: 1)]
        progressView?.layer.cornerRadius = 3
        progressView?.layer.masksToBounds = true
        cancelBackupButton.setTitle(NSLocalizedString("SETTINGS_COPY_BACKUP_CANCEL_BUTTON_TITLE", comment: ""), for: .normal)
        cancelBackupButton.addTarget(self, action: #selector(self.cancelButtonClick(_:)), for: .touchUpInside)
        setupBackupOperation()
        if backupQueue.operationCount<=0{
           self.updateUIforState(state: .notStartedState)
        }
        else{
            let opeartion : BackupDbOperation = backupQueue.operations[0] as! BackupDbOperation
            switch opeartion.type {
            case PreparingBackUp:
                self.updateUIforState(state: .inProgressState)
            case SendingBackUp:
                self.updateUIforState(state: .sendingState)
            default:
                break
            }

        }
}
    deinit{
      clearBackupQueue()
    }
    
    func startBackupButtonClick(_ sender: AnyObject) {
      self.startBackup()
      self.updateUIforState(state: .inProgressState)
    }
    
    func cancelButtonClick(_ sender: AnyObject) {
        if backupOperation != nil{
          backupOperation!.cancel()
        }
    }
    
    func startBackup() {
       
        progressView?.isStripesAnimated = true
        if backupOperation != nil{
            backupOperation = nil
        }
        backupOperation = BackupDbOperation()
        if backupQueue.operationCount<=0{
            backupQueue.addOperation(backupOperation!)
        }
    }
    
    func setupBackupOperation() {
       
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueDidComplete(_:)),
                                               name: NSNotification.Name.SyncQueueDidComplete,
                                               object: backupQueue)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueDidChangeOperationTitle(_:)),
                                               name: NSNotification.Name.SyncQueueDidChangeOperationTitle,
                                               object: backupQueue)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueInProgress(_:)),
                                               name: NSNotification.Name.SyncQueueInProgress,
                                               object: backupQueue)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueDidStartOperation(_:)),
                                               name: NSNotification.Name.SyncQueueDidStartOperation,
                                               object: backupQueue)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueDidErrorOccur(_:)),
                                               name: NSNotification.Name.SyncQueueDidErrorOccur,
                                               object: backupQueue)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.backupQueueDidEndOperation(_:)),
                                               name: NSNotification.Name.SyncQueueDidEndOperation,
                                               object: backupQueue)
        
        //syncQueueDidEndOperation
    }
    
    func clearBackupQueue() {
      
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueDidErrorOccur, object: backupQueue)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueDidStartOperation, object: backupQueue)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueInProgress, object: backupQueue)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueDidChangeOperationTitle, object: backupQueue)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueDidComplete, object: backupQueue)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.SyncQueueDidEndOperation, object: backupQueue)
    }
    
    // MARK: - BackupQueue Notifications
    
    func backupQueueDidEndOperation(_ notification : Notification) {
        progressView?.progress = 1.0
        self.updateUIforState(state: .notStartedState)
    }
    
    func backupQueueDidComplete(_ notification : Notification) {
        if let queue = notification.object as? BackupOperationQueue , queue == backupQueue {
            //clearBackupQueue()
            self.updateUIforState(state: .notStartedState)
        }
    }
    
    func backupQueueDidChangeOperationTitle(_ notification : Notification) {
      
        if let queue = notification.object as? BackupOperationQueue , queue == backupQueue {
            let userInfo = (notification as NSNotification).userInfo
            if let operation = userInfo?[SyncQueueCurrentOperationKey] as? BackupDbOperation {
                switch operation.type {
                case PreparingBackUp:
                    self.updateUIforState(state: .inProgressState)
                case SendingBackUp:
                    self.updateUIforState(state: .sendingState)
                default:
                    break
                }
            }
        }
    }
    
    func backupQueueInProgress(_ notification : Notification) {
        if let queue = notification.object as? BackupOperationQueue , queue == backupQueue {
            let userInfo = (notification as NSNotification).userInfo
            if let operation = userInfo?[SyncQueueCurrentOperationKey] as? BackupDbOperation {
                progressView?.progress = operation.progress
                let n=Double(operation.progress) * 100
                let number = Int(n)
                switch operation.type {
                case PreparingBackUp:
                    percentageLabel.text = String(format: NSLocalizedString("SETTINGS_COPY_BACKUP_PREPARING_BACKUP_TITLE", comment: ""),number)
                case SendingBackUp:
                    percentageLabel.text = String(format: NSLocalizedString("SETTINGS_COPY_BACKUP_SENDING_TITLE", comment: ""),number)
                default:
                    break
                }
            }
        }
    }
    
    func backupQueueDidStartOperation(_ notification : Notification) {
        if let queue = notification.object as? BackupOperationQueue , queue == backupQueue {
            progressView?.progress = (0)
        }
    }
    
    func backupQueueDidErrorOccur(_ notification : Notification) {
        if let queue = notification.object as? BackupOperationQueue , queue == backupQueue {
            let userInfo = (notification as NSNotification).userInfo
            if let error = userInfo?[SyncQueueErrorKey] as? NSError  {
                if error.code == NSURLErrorTimedOut || error.code == 404 || error.code == 503 || error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotFindHost {
                    ModalAlert.show(NSLocalizedString("SETTINGS_COPY_BACKUP_FAILED_TITLE", comment: "BACKUP_UNABLE_TO_SEND_TEXT"), message: NSLocalizedString("SETTINGS_COPY_BACKUP_FAILED_MESSAGE", comment: ""))
                }
                else {
                    ModalAlert.showError(error)
                }
            }
            backupQueue.cancelAllOperations()
        }
    }
    func updateUIforState(state: CopyBackupCurrentState){
        switch state {
        case .notStartedState:
            view1.isHidden = false
            view2.isHidden = true
            break
        case .inProgressState:
            view2.isHidden = false
            view1.isHidden = true
            break
        case .sendingState:
            view2.isHidden = false
            view1.isHidden = true
            break
        }
    }
}
