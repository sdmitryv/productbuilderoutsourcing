//
//  HelpSupportViewController50.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 2/8/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//
import Foundation
class HelpSupportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HelpSupportCommonCellDelegate {
    @IBOutlet weak var generalTableView : UITableView!
    @IBOutlet var beginButton : MOGlassButtonMarine!
    @IBOutlet var stopButton : MOGlassButtonRed!
    @IBOutlet var footerLabel: RPDynamicFontLabel!
    var sessionKey : UITextField?
    var headerTitle : String!
    var keyText: String?
    
    required init(){
        super.init(nibName: "HelpSupportView50", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        generalTableView.register(UINib(nibName: HelpSupportCommonCell.nibName, bundle: nil), forCellReuseIdentifier: "HelpSupportCommonCell")
        generalTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        stopButton.setTitle(NSLocalizedString("SETTINGS_HELP_SUPPORT_STOP_SUPPORT_SESSION_BUTTON_TITLE", comment: ""),for: .normal)
        beginButton.setTitle(NSLocalizedString("SETTINGS_HELP_SUPPORT_BEGIN_SUPPORT_SESSION_BUTTON_TITLE", comment: ""),for: .normal)
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 2
        }
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if SettingManager.instance().bomgarEnabled(){
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpSupportCommonCell", for: indexPath) as!  HelpSupportCommonCell
        cell.layer.cornerRadius = 3
        cell.layoutMargins = UIEdgeInsets.zero
        cell.delegate=self
        switch ((indexPath as NSIndexPath).section, (indexPath as NSIndexPath).row) {
        case (0,0):
            cell.leftLabel.text = NSLocalizedString("SETTINGS_HELP_SUPPORT_PHONE_LABEL_TITLE", comment: "")
            if let phone = SettingManager.instance().supportPhone(){
                cell.rightLabel.text = phone
            }
            cell.textField.isHidden = true
            cell.button.isHidden = true
        case (0,1):
            cell.leftLabel.text = NSLocalizedString("SETTINGS_HELP_SUPPORT_EMAIL_LABEL_TITLE", comment: "")
            if let email = SettingManager.instance().supportEmail(){
                cell.button.setTitle(email, for: UIControlState.normal)
            }
            cell.textField.isHidden = true
            cell.separator.isHidden = true
            cell.rightLabel.isHidden=true

        case(1,0):
            cell.leftLabel.text = NSLocalizedString("SETTINGS_HELP_SUPPORT_SESSION_ID", comment: "")
            cell.textField.placeholder = NSLocalizedString("SETTINGS_HELP_SUPPORT_PLACEHOLDER_ID", comment: "")
            if let txt = keyText{
                cell.textField.text = txt
            }
            cell.rightLabel.isHidden=true
            cell.separator.isHidden = true
            sessionKey = cell.textField

        default: break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0001
        } else {
            return 50
        }
    }  
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1  {
            return headerTitle
        }
        return ""
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 0 {
            if let supportText = SettingManager.instance().supportText(){
                return supportText
            }
        }
        if section == 1 {
           return NSLocalizedString("SETTINGS_HELP_SUPPORT_INFO_TEXT", comment: "")
        }
        
        return ""
    }

    func enableButton(enabled: Bool){
        beginButton.isEnabled = enabled
    }
    
     func buttonClick() {
       
      
    }
   
}
protocol HelpSupportCommonCellDelegate: class {
    func enableButton(enabled: Bool)
}
class HelpSupportCommonCell : UITableViewCell, UITextFieldDelegate {
    
    static let nibName = "HelpSupportCommonCell50"
    @IBOutlet var leftLabel: UILabel!
    @IBOutlet var rightLabel : UILabel!
    @IBOutlet var textField: UITextField!
    @IBOutlet var separator: UIView!
    @IBOutlet var button: UIButton!
    weak var delegate: HelpSupportCommonCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        textField.delegate = self
    }
    @IBAction func buttonClick(_ sender: AnyObject) {
        if let neddedURL = button.titleLabel?.text{
            
            if let url = URL(string: "mailto:" + (neddedURL)), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let resultString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
        if resultString.characters.count < 6 {
           self.delegate?.enableButton(enabled: false)
        }
        else{
            self.delegate?.enableButton(enabled: true)
        }
        return true
    }
    

}
