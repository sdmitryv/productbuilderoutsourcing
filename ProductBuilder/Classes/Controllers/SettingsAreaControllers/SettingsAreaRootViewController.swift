//
//  SettingsAreaRootViewController.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 12/27/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//
protocol SettingsAreaRootViewControllerDelegate: class {
    func settingsAreaRootViewController(_ rootController:SettingsAreaRootViewController, areaSelected: SettingsArea)
}

enum  SettingsArea : Int{
    case generalInfo = 0
    case changePassword
    case synchronize
    case printers
    case helpAndSupport
    case language
    case reinit
    case copyBackup
    case checkIndex
    case networkCheck
    case verifyTimeSettings
    
    var title: String {
        switch self {
        case .generalInfo: return L("SETTINGS_AREAS_TITLE_GENERAL_INFO")
        case .changePassword: return L("SETTINGS_AREAS_TITLE_CHANGE_PASSWORD")
        case .synchronize:return L("SETTINGS_AREAS_TITLE_SYNCHRONIZE")
        case .printers:return L("SETTINGS_AREAS_TITLE_PRINTERS")
        case .helpAndSupport:return L("SETTINGS_AREAS_TITLE_HELP_SUPPORT")
        case .language:return L("SETTINGS_AREAS_TITLE_LANGUAGE")
        case .reinit:return L("SETTINGS_AREAS_TITLE_REINITIALIZE")
        case .copyBackup:return L("SETTINGS_AREAS_TITLE_COPY_BACKUP")
        case .checkIndex:return L("SETTINGS_AREAS_TITLE_CHECK_INDEX")
        case .networkCheck:return L("SETTINGS_AREAS_TITLE_NETWORK_CHECK")
        case .verifyTimeSettings:return L("SETTINGS_AREAS_TITLE_VERIFY_TIME_SETTINGS")
        }
    }
    static var defaultArea: SettingsArea {
        get{
            return .generalInfo
        }
    }
}

class SettingsAreaRootViewController: UITableViewController{
    
    @IBOutlet weak var homeButton: UIBarButtonItem!
    @IBOutlet weak var settingsLabel: UILabel!
    
    var selectedArea:SettingsArea = .defaultArea{
        didSet{
            if isViewLoaded{
                handleSelectedArea()
            }
        }
    }
    weak var delegate: SettingsAreaRootViewControllerDelegate?
    var previousSelectionIndex = IndexPath(row: 0, section: 0)
    
    let areasTitles: [[SettingsArea]] = [[.generalInfo]]
    let areasDontNeedToBeSelected:[SettingsArea] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeButton.title=NSLocalizedString("HOME_BUTTON_TITLE", comment: "")
        settingsLabel.text=NSLocalizedString("SETTINGS_AREA_TITLE", comment: "")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "SettingsCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleSelectedArea()
    }
    
    private func handleSelectedArea(){
        var selectedSection = 0
        var selectedRow = 0
        for section in 0..<areasTitles.count{
            if let row = areasTitles[section].index(of: selectedArea){
                selectedSection = section
                selectedRow = row
                break
            }
        }
        tableView.layoutIfNeeded()
        var indexPath = IndexPath(row: selectedRow, section: selectedSection)
        if let newIndexPath = tableView.delegate?.tableView!(tableView, willSelectRowAt: indexPath){
            indexPath = newIndexPath
        }
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        tableView.delegate?.tableView!(tableView, didSelectRowAt: indexPath)
    }

    //MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return areasTitles[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath)
        cell.textLabel?.text = areasTitles[indexPath.section][indexPath.row].title
        cell.textLabel?.highlightedTextColor = UIColor.white
        cell.textLabel?.textColor = UIColor.black
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if let user = SecurityManager.currentEmployee(), SecurityManager.isUser(inRole: user.id, role: UserRoleSystemAccessAdministrationMenu){
            return areasTitles.count
        }
        else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let area = areasTitles[indexPath.section][indexPath.row]
        if (areasDontNeedToBeSelected.contains(area)) {
            self.delegate?.settingsAreaRootViewController(self, areaSelected: area)
            return previousSelectionIndex
        
        }
        return indexPath
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at:indexPath)!
        selectedCell.contentView.backgroundColor = ColorUtils.ColorWithRGB(red: 79, green: 157, blue: 237)
        
        self.delegate?.settingsAreaRootViewController(self, areaSelected: areasTitles[indexPath.section][indexPath.row])
        previousSelectionIndex = indexPath
        
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at:indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.white
       
    }
    @IBAction func homeButtonClick(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
