//
//  VerifyTimeMessageViewController.swift
//  ProductBuilder
//
//  Created by Julia Korevo on 1/18/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import Foundation
class VerifyTimeMessageViewController50: UIViewController{
    var initDate : NSDate!
    var initTimeZone : NSTimeZone!
    
    required init(date: NSDate!, timeZone: NSTimeZone!) {
         super.init(nibName: nil, bundle: nil)
        initDate = date
        initTimeZone = timeZone
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    class func verifyTime(completionBlock:@escaping (_ showSuccess: Bool)->()){
        let operation = GetDateTimeOperation()
        var isCanceled = false
        let backgroundTimeVerificationViewController = BackgroundOperationController50(title: NSLocalizedString("SETTINGS_VERIFYING_TIME_ACTIVITY_LABEL",comment: ""),
                                                                                       message: nil,
                                                                                       hideCancel: false)
        backgroundTimeVerificationViewController?.onCancel = {
            operation.cancel()
            isCanceled = true
        }
        backgroundTimeVerificationViewController?.startAlertTask(task: {
            
            let queue = SyncOperationQueue()
            queue.addOperations([operation as Any], waitUntilFinished: true, breakOnError: true)
        },
                                                                 completed: {
                                                                    
                                                                    if operation.result() {
                                                                        
                                                                        let deviceDate = NSDate()
                                                                        let hqDate = operation.date
                                                                        let localLocation = Location.getInstanceBy(AppSettingManager.instance().locationId)!
                                                                        var localTimeZone : Product_Builder.TimeZone?
                                                                        var localUtcOffset = 0
                                                                        if localLocation.timeZoneID != nil {
                                                                            localTimeZone = Product_Builder.TimeZone.getInstanceBy(localLocation.timeZoneID)
                                                                            localUtcOffset = NSDecimalNumber(decimal: localTimeZone!.utcOffset * Decimal(3600)).intValue
                                                                        }
                                                                        let deviceTimeZone = NSTimeZone.local
                                                                        let deviceUtcOffset = deviceTimeZone.secondsFromGMT() -  Int(deviceTimeZone.daylightSavingTimeOffset())
                                                                        if (fabs(deviceDate.timeIntervalSince(hqDate!)) > 60 * 5 || localTimeZone == nil || localUtcOffset != deviceUtcOffset) {
                                                                            let verifyTimeMessageViewController = VerifyTimeMessageViewController50(date: operation.date as NSDate!, timeZone: operation.timeZone as NSTimeZone!)
                                                                            verifyTimeMessageViewController.showDifference()
                                                                            completionBlock(false)
                                                                        }
                                                                        else{
                                                                            completionBlock(true)
                                                                        }
                                                                    }
                                                                    else if !isCanceled {
                                                                        let error : NSError = operation.lastErrorObject as NSError
                                                                        if error.domain == "WebServiceResponseHTTP" {
                                                                            ModalAlert.show(NSLocalizedString("SERVER_UNAVAILABLE",comment: ""), message: error.localizedDescription, completed: {
                                                                                completionBlock(false)
                                                                            })
                                                                        }
                                                                        else {
                                                                            ModalAlert.show(NSLocalizedString("SERVER_UNAVAILABLE",comment: ""), message: NSLocalizedString("SERVER_UNAVAILABLE_DESCRIPTION",comment: ""), completed: {
                                                                                completionBlock(false)
                                                                            })
                                                                        }
                                                                    }
        })

    }
    
    func showDifference(){
        
        NSLog("!!!!!!!!!!!!!!!!!!!")
        
        let localLocation = Location.getInstanceBy(AppSettingManager.instance().locationId)!
        var localTimeZone : Product_Builder.TimeZone?
        var localUtcOffset : NSInteger = 0
        var locationTimeZone : Foundation.TimeZone?
        if localLocation.timeZoneID != nil {
            localTimeZone = Product_Builder.TimeZone.getInstanceBy(localLocation.timeZoneID)
            localUtcOffset = NSDecimalNumber(decimal: localTimeZone!.utcOffset * Decimal(60)).intValue
            locationTimeZone = (Foundation.TimeZone(secondsFromGMT: localUtcOffset * 60))! as Foundation.TimeZone
        }
        let deviceTimeZone = NSTimeZone.local
        let  deviceDate = NSDate()
        let deviceSeconds = deviceTimeZone.secondsFromGMT() -  Int(deviceTimeZone.daylightSavingTimeOffset())
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let  deviceTimeString = dateFormatter.string(from: deviceDate as Date)
        var deviceUtcString = ""
        deviceUtcString =  String(format:"UTC %@%02li:%02li - %@", deviceSeconds > 0 ? "+" : "-", labs(deviceSeconds) / 3600, (labs(deviceSeconds) % 3600) / 60,
                                  deviceTimeZone.localizedName(for: NSTimeZone.NameStyle.shortDaylightSaving, locale: NSLocale.current)!)
        var hqTimeString : String?
        var hqUtcString : String?
        if locationTimeZone != nil {
            dateFormatter.timeZone = locationTimeZone
            hqTimeString = dateFormatter.string(from: initDate as Date)
            hqUtcString = String(format:"UTC %@%02li:%02li - %@",localUtcOffset > 0 ? "+" : "-", labs(localUtcOffset) / 60, labs(localUtcOffset) % 60,            (localTimeZone?.name)!)
        }
        else {
            hqTimeString = localTimeZone == nil ? NSLocalizedString("TIME_ZONE_VERIFICATION_NO_TIME_ZONE_TEXT", comment:"") :
                NSLocalizedString("TIME_ZONE_VERIFICATION_BAD_TIME_ZONE_TEXT", comment:"")
            hqUtcString = nil
        }
        let devString =  String(format:"%@ - %@", deviceTimeString, deviceUtcString)
        var  hqString : String?
        if hqUtcString != nil {
            hqString = String(format:"%@ - %@", hqTimeString!, hqUtcString!)
        }
        else {
            hqString =  hqTimeString!
        }
        ModalAlert.show(NSLocalizedString("SETTINGS_VERIFYING_TIME_MISMATCH_TITLE",comment: ""), message: String(format:NSLocalizedString("SETTINGS_VERIFYING_TIME_MISMATCH_MESSAGE",comment: ""), devString, hqString!) , completed: {})
    }
}
