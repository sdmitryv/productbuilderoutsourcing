//
//  EditModificationsViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class EditModificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, RTSelectorButtonDelegate {

    private let modifications : NSArray? = MainViewController50.sharedInstance()?.modifications[0] as? NSArray
    
    private var selectedCategory : String? {
        didSet {
            filterModifications()
            if filteredItems.count > 0 {
                itemsTableView.isHidden = false
                itemsTableView.reloadData()
            }
            else {
                itemsTableView.isHidden = true
                noResultLabel.text = "No results found in \(selectedCategory == nil ? "All" : selectedCategory!)"
            }
        }
    }
    
    private var searchText = String() {
        didSet {
            filterModifications()
            if filteredItems.count > 0 {
                itemsTableView.isHidden = false
                itemsTableView.reloadData()
            }
            else {
                itemsTableView.isHidden = true
                noResultLabel.text = "No results found in \(selectedCategory == nil ? "All" : selectedCategory!)"
            }
        }
    }
    
    public var selectedModifications = Set<String>()
    
    public var save : (() -> Void)?
    
    private var filteredItems = Array<String>()
    
    @IBOutlet var noResultLabel: UILabel!
    @IBOutlet var itemsTableView: UITableView!
    @IBOutlet var itemSearchBar: UISearchBar!
    @IBOutlet var filterSelectorButton: RTSelectorButton!
    @IBAction func canceButtonClick(_ sender: Any) {
        self.dismiss(animated: true) { 
        }
    }
    
    @IBAction func selectButtonClick(_ sender: UIButton) {
        let isSelected = sender.isSelected
        if let indexPath = itemsTableView.indexPathForRow(at: itemsTableView.convert(sender.center, from: sender.superview)) {
            let modification = filteredItems[indexPath.row]
            if isSelected {
                selectedModifications.remove(modification)
            }
            else {
                selectedModifications.insert(modification)
            }
            sender.isSelected = !isSelected
            itemsTableView.reloadData()
            self.title = "Edit Modifications (\(selectedModifications.count))"
        }
    }
   
    @IBAction func doneButtonClick(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.save != nil {
                self.save!()
            }
        }
    }
    
    @IBAction func selectCategory(_ sender: UISegmentedControl) {
        selectedCategory = sender.selectedSegmentIndex == 0 ? nil : sender.titleForSegment(at: sender.selectedSegmentIndex)
    }
    
    private func filterModifications() {
        filteredItems.removeAll()
        for modification in modifications! {
            if let item = modification as? Dictionary<String, Any> {
                if let category = item["category"] as? String {
                    if selectedCategory == nil || selectedCategory! == category {
                        if let labels = item["labels"] as? Array<String> {
                            if searchText == "" {
                                filteredItems.append(contentsOf: labels)
                            }
                            else {
                                for label in labels {
                                    if label.range(of: searchText, options: String.CompareOptions.caseInsensitive) != nil {
                                        filteredItems.append(label)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var categories = Array<String>()
        categories.append("All")
        for modification in modifications! {
            if let item = modification as? Dictionary<String, Any> {
                if let category = item["category"] as? String {
                    categories.append(category)
                }
            }
        }
        filterSelectorButton.initializeControl("self", dataSource: categories, currentValue: "All" as NSObject, title: "Categories")
        filterSelectorButton.delegate = self
        
        itemsTableView.tableFooterView = UIView(frame: CGRect.zero)

        self.title = "Edit Modifications (\(selectedModifications.count))"

        selectedCategory = nil
    }

    // MARk: UITableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modification = filteredItems[indexPath.row]
        let cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "ItemCellIdentifier")
        let selectButton: UIButton? = cell?.contentView.viewWithTag(2) as? UIButton
        selectButton?.isSelected = selectedModifications.contains(modification)
        let titleLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
        titleLabel?.text = modification
        return cell!
    }

    // MARK: UISearchBar Delegate

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
    }

    // MARK: RTSelectorButtonDelegate
    
    func selectorButton(_ button: RTSelectorButton!, didSelectValue value: Any!) {
        if let category = value as? String {
            selectedCategory = category == "All" ? nil : category
        }
    }
    
}
