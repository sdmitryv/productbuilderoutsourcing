//
//  MyClass.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 10/11/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "ActionsHelper.h"
#import "BackgroundOperationViewController.h"
#import "SecurityManager.h"
#import "RTAlertView.h"
#import "MessageMismatchController.h"
#import "NSAttributedString+Attributes.h"
#import "ModalAlert.h"
#import "AppSettingManager.h"
#import "Product_Builder-Swift.h"

@implementation ActionsHelper

BOOL locationMismatchShown;
NSObject* syncShowLocationMismatchMessage;

+ (void)initialize{
    syncShowLocationMismatchMessage = [[NSObject alloc]init];
}

+(BOOL)checkSyncOperationErrorForLocationMismatch:(SyncOperation*)syncOperation completion:(void (^)(void))aCompletion{
    
    if (syncOperation.lastErrorObject.code == LocationMismatchErrorCode){
        
        LocationBase* currentLocation = (syncOperation.lastErrorObject.userInfo)[SyncQueueCurrentLocationKey];
        LocationBase* opsLocation = (syncOperation.lastErrorObject.userInfo)[SyncQueueOpsLocationKey];
        [ActionsHelper showLocationMismatchMessage:currentLocation newLocation:opsLocation completion:aCompletion];
        
        return TRUE;
    }
    
    return FALSE;
}

+(BOOL)checkSyncOperationErrorForLocationMismatch:(SyncOperation*)syncOperation{
    return [self checkSyncOperationErrorForLocationMismatch:syncOperation completion:nil];
}

+(BOOL)validateSyncOperationForLocationMismatchError:(SyncOperation*)syncOperation error:(NSError**)error{
    
    if (syncOperation.lastErrorObject.code == LocationMismatchErrorCode){
        if (error){
            LocationBase* currentLocation = (syncOperation.lastErrorObject.userInfo)[SyncQueueCurrentLocationKey];
            LocationBase* opsLocation = (syncOperation.lastErrorObject.userInfo)[SyncQueueOpsLocationKey];
            NSDictionary* userInfo = @{NSLocalizedDescriptionKey:NSLocalizedString(@"SYNC_LOCATION_MISMATCH_TITLE", nil), @"originLocationKey" : currentLocation, @"newLocationKey":opsLocation};
            *error = [[[NSError errorWithDomain:@"com.cloudwk.locationMismatch" code:0 userInfo:userInfo] retain]autorelease];
        }
        return NO;
    }
    return YES;
}

+(void)showLocationMismatchMessage:(LocationBase*)originLocation newLocation:(LocationBase*)newLocation completion:(void (^)(void))aCompletion{
    @synchronized(syncShowLocationMismatchMessage){
        if (locationMismatchShown) return;
        locationMismatchShown = TRUE;
        MessageMismatchController* message = [[MessageMismatchController alloc]init];
        NSMutableAttributedString* title =    
        [[NSMutableAttributedString alloc]initWithString:NSLocalizedString(@"SYNC_LOCATION_MISMATCH_TITLE", nil)];
        [title setTextAlignment:NSTextAlignmentCenter lineBreakMode:NSLineBreakByClipping];
        [title setFont:[UIFont boldSystemFontOfSize:19]];
        
        NSMutableAttributedString* initDescription = 
        [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ - %@",originLocation.locationCode, originLocation.name]];
        [initDescription setTextAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByClipping];
        [initDescription setFont:[UIFont boldSystemFontOfSize:16]];
        
        NSMutableAttributedString* syncDescription = 
        [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ - %@",newLocation.locationCode, newLocation.name]];
        [syncDescription setTextAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByClipping];
        [syncDescription setFont:[UIFont boldSystemFontOfSize:16]];
        
        [message loadView];
        message.titleLabel.text = [title string];
        message.locationInitLabel.text = [initDescription string];
        message.locationSyncLabel.text = [syncDescription string];
        [title release];
        [initDescription release];
        [syncDescription release];
        
        NSMutableAttributedString* detailsText = 
         [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\r\n", NSLocalizedString(@"SYNC_LOCATION_MISMATCH_DETAILS_SYNC_DESCRIPTION", nil)]];
        [detailsText setTextAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByClipping];
        [detailsText setFont:[UIFont systemFontOfSize:16]];

        message.detailsLabel.text = [detailsText string];
        [detailsText release];
        [message adjustSize];
        [message showModal:TRUE completion:^{
            @synchronized(syncShowLocationMismatchMessage){
                locationMismatchShown = FALSE;
            }
            if (aCompletion)
                aCompletion();
        }];
        [message release];
    }
}


+(void)cancelSynchronization{
    if (CANCEL_SYNC_ON_USER_OPERATIONS)
        [[SyncOperationQueue sharedInstance] cancelAllOperationsAsync];
}


+(NSDictionary*)serializeLocation:(Location*)location{
    NSMutableDictionary* locationDict = [[NSMutableDictionary alloc] init];
    locationDict[@"locationId"] = location.id.stringRepresentation;
    locationDict[@"locationCode"] = location.locationCode;
    locationDict[@"address1"] = location.address1;
    locationDict[@"address2"] = location.address2;
    locationDict[@"city"] = location.city;
    locationDict[@"state"] = location.state;
    locationDict[@"countryCode"] = location.country;
    locationDict[@"postalCode"] = location.postalCode;
//    if (!CPDecimalEquals0(location.latitude)){
//        locationDict[@"latitude"] = [[NSDecimalNumber decimalNumberWithDecimal:location.latitude] stringValue];
//    }
//    if (!CPDecimalEquals0(location.longitude)){
//        locationDict[@"longitude"] = [[NSDecimalNumber decimalNumberWithDecimal:location.longitude] stringValue];
//    }
    return [locationDict autorelease];
}

+(BOOL)validateLocation:(Location*)location error:(NSError**)error{
    NSString* domain = @"com.receipt.location.validate";
    if (!location){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_MSG", nil) domain:domain];
        return NO;
    }
    if (!location.address1.length){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_ADDRESS_1_MSG", nil) domain:domain];
        return NO;
    }
    if (!location.city.length){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_CITY_MSG", nil) domain:domain];
        return NO;
    }
    if (!location.state.length){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_STATE_MSG", nil) domain:domain];
        return NO;
    }
    if (!location.country.length){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_COUNTRY_MSG", nil) domain:domain];
        return NO;
    }
    if (!location.postalCode.length){
        [CEntity writeError:error description:NSLocalizedString(@"TAX_SERVICE_VALIDATE_LOCATION_POSTAL_CODE_MSG", nil) domain:domain];
        return NO;
    }
    return YES;
}


@end
