//
//  MaintenanceViewController50.swift
//  ProductBuilder
//
//  Created by valery on 12/21/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class MaintananceViewController50 : UIViewController{
    
    @IBOutlet var activityIndicatorView:UIActivityIndicatorView?
    @IBOutlet var titleLabel:UILabel?
    @IBOutlet var cancelButton:UIButton?
    
    
    @IBAction func cancel(_ sender: UIButton) {
        if (cancelAction != nil){
            cancelAction!(sender)
        }
    }
    
    var needToBeShown: (() -> Void)?
    var needToBeClosed: (() -> Void)?
    
    func startup(){
        setTitle(text: "Preparing..")
        DataManager.instance().enableDatabaseAccess()
        DeviceStats.statsLastAppRun()
        setTitle(text: "Preparing..")
        
        if (AppSettingManager.instance().productBuilderData == nil) {
            
            let semaphore = DispatchSemaphore(value: 0)
            
            let rootRef = Database.database().reference()
            rootRef.observe(DataEventType.value, with: { (snapshot) in
                
                let snapshotDictionary = snapshot.value as? NSDictionary
                
                do {
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: snapshotDictionary ?? "{}", options: [])
                    
                    let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
                    if (jsonString != nil) {
                        
                        AppSettingManager.instance().productBuilderData = jsonString! as String
                        
                        MainViewController50.needUpadteProductData = true
                    }
                } catch {
                }
                
                rootRef.removeAllObservers()
                semaphore.signal()
            })
            
            semaphore.wait()
        }
    }
    
    var cancelAction:((_ sender: UIButton) -> Void)?
    
    func setTitle(text:String, showActivityIndicator:Bool = true, cancelButtonTitle:String? = nil, cancelAction:((_ sender: UIButton) -> Void)? = nil){
        if titleLabel == nil{
            return
        }
        if !Thread.isMainThread{
            DispatchQueue.main.async {
                self.setTitle(text: text, cancelButtonTitle: cancelButtonTitle, cancelAction:cancelAction)
            }
        }
        else{
            self.titleLabel!.text = text
            if showActivityIndicator{
                self.activityIndicatorView?.isHidden = false
                self.activityIndicatorView?.startAnimating()
            }
            else{
                self.activityIndicatorView?.isHidden = true
                self.activityIndicatorView?.stopAnimating()
            }
            if (cancelButtonTitle != nil || cancelAction != nil){
                self.cancelButton?.isHidden = false
                self.cancelButton?.setTitle(cancelButtonTitle ?? "Skip", for: .normal)
                self.cancelAction = cancelAction
            }
            else{
                self.cancelButton?.isHidden = true
            }
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.titleLabel!.text = nil
        self.cancelButton?.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(syncStart), name: NSNotification.Name.SyncQueueDidStartOperation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(syncErrorOccur), name: NSNotification.Name.SyncQueueDidErrorOccur, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(syncComplete), name: NSNotification.Name.SyncQueueDidComplete, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataManagerWillStartUpdateNotification), name: NSNotification.Name.DataManagerWillStartUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataManagerDidEndUpdateNotification), name: NSNotification.Name.DataManagerDidEndUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DataManagerIndexesCheckingWillStartNotification), name: NSNotification.Name.DataManagerIndexesCheckingWillStart, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DataManagerIndexesCheckingDidEndNotification), name: NSNotification.Name.DataManagerIndexesCheckingDidEnd, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DataManagerIndexesWillStartUpdateNotification), name: NSNotification.Name.DataManagerIndexesWillStartUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DataManagerIndexesWillEndUpdateNotification), name: NSNotification.Name.DataManagerIndexesWillEndUpdate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(syncQueueDidEndOperationNotification), name: NSNotification.Name.SyncQueueDidEndOperation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deviceStateChangedNotification), name: NSNotification.Name.deviceStateChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(egoDatabaseEndLockNotification), name: NSNotification.Name.EGODatabaseEndLock, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func syncStart(_ notification: Notification) {
    }
    @objc private func syncErrorOccur(_ notification: Notification) {
    }
    @objc private func syncComplete(_ notification: Notification) {
    }
    @objc private func dataManagerWillStartUpdateNotification(_ notification: Notification) {
    }
    @objc private func dataManagerDidEndUpdateNotification(_ notification: Notification) {
    }
    @objc private func DataManagerIndexesCheckingWillStartNotification(_ notification: Notification) {
    }
    @objc private func DataManagerIndexesWillStartUpdateNotification(_ notification: Notification) {
    }
    @objc private func DataManagerIndexesCheckingDidEndNotification(_ notification: Notification) {
    }
    @objc private func DataManagerIndexesWillEndUpdateNotification(_ notification: Notification) {
    }
    @objc private func syncQueueDidEndOperationNotification(_ notification: Notification) {
    }
    
    @objc private func deviceStateChangedNotification(_ notification: Notification) {
        checkDeviceAgentState()
    }
    
    func checkDeviceAgentState(){
        if DeviceAgentManager.sharedInstance().isDeviceActive(){
            if needToBeClosed != nil{
                needToBeClosed!()
            }
            return
        }
        setTitle(text: L("DEVICE_AGENT_NO_SUCH_DEVICE_MESSAGE2"), showActivityIndicator:false)
        
        BackgroundOperationController50.closeBackgroundOperations()
        
        //close all views
        closeViews()
        
        if needToBeShown != nil{
            needToBeShown!()
        }
    }
    
    private func closeViews(){
        if let topViewController = UIViewController.topViewController() {
            
            let viewController = topViewController.parent ?? topViewController
            
            if viewController is SuperViewController50 || viewController is MainViewController50{
                return
            }
            viewController.dismiss(animated: true, completion: {
                self.closeViews()
            })
        }

    }
    
    @objc private func egoDatabaseEndLockNotification(_ notification: Notification) {
    }
}

