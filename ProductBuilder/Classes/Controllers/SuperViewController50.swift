//
//  SuperViewController.swift
//  ProductBuilder
//
//  Created by valery on 12/15/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class SuperViewController50 : UIViewControllerExtended{
    
    var statusViewController : StatusViewController50?
    var _navigationController : UINavigationController?
    private var originalStatusBarHeight:CGFloat?
    @IBOutlet weak var statusViewHeightConstraint: NSLayoutConstraint?{
        didSet{
            originalStatusBarHeight = statusViewHeightConstraint?.constant
        }
    }
    
    var mainViewController : MainViewController50?{
        return _navigationController?.viewControllers.first as? MainViewController50
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    static func instance()->SuperViewController50?{
        return UIApplication.shared.delegate?.window??.rootViewController as? SuperViewController50
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        modalPresentationStyle = .currentContext
        UIViewControllerExtended.setRootViewController(self.mainViewController)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.destination is UINavigationController){
            _navigationController = segue.destination as? UINavigationController
        }
        if (segue.destination is StatusViewController50){
            self.statusViewController = segue.destination as? StatusViewController50
        }
    }
    
    
    var isStatusBarHidden:Bool{
        get{
            return statusViewHeightConstraint != nil ? statusViewHeightConstraint!.constant == 0 : false;
        }
        set{
            statusViewHeightConstraint?.constant = newValue ? 0 : originalStatusBarHeight ?? 0
        }
    }
}
