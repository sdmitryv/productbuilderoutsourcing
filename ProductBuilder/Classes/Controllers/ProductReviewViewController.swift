//
//  ProductReviewViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class ProductReviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var product: Product?

    @IBOutlet var cluLabel: UILabel!
    @IBOutlet var copiedImageView: UIImageView!
    @IBOutlet var copiedLabel: UILabel!
    @IBOutlet var fieldsTableView: UITableView!
    
    @IBAction func copyButtonClick(_ sender: Any) {
        copiedLabel.isHidden = false
        copiedImageView.isHidden = false
    }
    
    @IBAction func doneButtonClick(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        copiedImageView.image = UIImage(named: "checked.png")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        copiedImageView.tintColor = UIColor(r: 69, g: 120, b: 9)
        
        cluLabel.text = "CLU \(product!.clu!)"
        copiedLabel.isHidden = true
        copiedImageView.isHidden = true
        fieldsTableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemDescriptionCellIdentifier")
            let descriptionLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
            descriptionLabel?.text = product?.description
        case (0, 1):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceCellIdentifier")
            let priceLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
            priceLabel?.text = NSCurrencyFormatter.formatCurrency(from: product?.price)
        case (1, _):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemInstructionsCellIdentifier")
            let instuctionsLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
            instuctionsLabel?.text = product?.instructions
        default:
            cell = UITableViewCell()
        }
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Product" : "Instructions"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat = 44
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let description : String = product!.description
            let constraintRect = CGSize(width: 700, height: 300)
            let boundingBox = description.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
            height = ceil(boundingBox.height) + 16
        case (0, 1):
            height = 60
        case (1, 0):
            height = 60
        default:
            height = 1
        }
        return height
    }

}
