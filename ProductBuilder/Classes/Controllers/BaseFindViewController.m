//
//  BaseListViewController.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/7/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "BaseFindViewController.h"
#import "SqlSortOrder.h"
#import "ILazyLoadableList.h"
#import "ActionsHelper.h"
#import "NSAttributedString+Attributes.h"
#import "BackgroundOperationViewController.h"
#import "Global.h"
#import "SystemVer.h"
#import "NSString+Utils.h"
#import "SystemVer.h"
#import "BaseSearchTableViewCell.h"
#import "UIImage+Arrow.h"

@implementation BaseFindViewController

@synthesize btnKeyboardSwitch;
@synthesize titleLabel;
@synthesize titleBarButtonItem;
@synthesize toolbar;
@synthesize searchBar;
@synthesize searchBarBig;
@synthesize itemsView;
@synthesize totalView;
@synthesize totalLabel;
@synthesize selectedLabel;
@synthesize btnSelect;
@synthesize btnCancel;
@synthesize pageControl;
@synthesize bgView;
@synthesize searchText = _searchText;

-(void)initDefaults{
    //highlightFont = [[UIFont boldSystemFontOfSize:16]retain];
    highlightSelectedColor = [[UIColor whiteColor]retain];
	highlightColor = [global_blue_color retain];
    itemDetailsIsHidden = NO;
}

- (id)initWithTitle:(NSString *)text list:(id) aList desired:(NSString *)desired {
    self = [self initWithTitle:text list:aList searchBlock:nil desired:desired];
    if (self) {
        [self initDefaults];
        title = [text retain];
        itemList = [aList retain];
    }
    return self;
}

- (id)initWithTitle:(NSString *)text list:(id)aList searchBlock:(SearchBlock)aSearchBlock desired:(NSString *)desired {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"BaseFindView" bundle:nil];
    }
    else {
        if (IS_IPHONE_6_OR_MORE)
            self = [super initWithNibName:@"BaseFindView_iPhone@3x" bundle:nil];
        else
            self = [super initWithNibName:@"BaseFindView_iPhone" bundle:nil];
    }
    if (self) {
        [self initDefaults];
        title = [text retain];
        itemList = [aList retain];
        searchBlock = [aSearchBlock copy];
        _searchText = [desired retain];
        self.countFormatString = NSLocalizedString(@"SEARCH_RESULTCOUNT_FORMAT_TEXT", nil);
    }
    return self;
}

- (void)dealloc
{
    itemTableView.delegate = nil;
    [itemTableView release];
    plainListView.delegate = nil;
    [plainListView release];
    [titleLabel release];
    [titleBarButtonItem release];
    [toolbar release];
    [searchBar release];
    [searchBarBig release];
    [itemsView release];
    [totalView release];
    [totalLabel release];
    [selectedLabel release];
    
    [upArrow release];
    [downArrow release];
    [itemList release];
    [title release];
    
    [btnSelect release];
    [btnCancel release];
    [pageControl release];
    //[highlightFont release];
    [highlightColor release];
    [highlightSelectedColor release];
    [btnKeyboardSwitch release];
    [selectedValue release];
    [searchBlock release];
    [_searchText release];
    
    [bgView release];
    
    [_countFormatString release];
    [_searchBarButtonItem release];
    [_searchBarButtonItemBig release];
    [_searchView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

-(BOOL)needToShowKeyboardHideButton{
    
    return TRUE;
}

//true means kb is hidden
-(BOOL)keyboardHideButtonDefaultValue{
    return TRUE;
}

-(void)removeKeyboardHideButton{
    NSMutableArray* itemsToRemove = nil;
    for (UIBarButtonItem* item in self.toolbar.items){
        if ([item.customView isKindOfClass:RTKeyboardSwitchButton.class]){
            if (!itemsToRemove){
                itemsToRemove = [[NSMutableArray alloc]init];
            }
            [itemsToRemove addObject:item];
        }
    }
    if (itemsToRemove.count){
        NSMutableArray* toolbarItems = [[NSMutableArray alloc]initWithArray:self.toolbar.items];
        for (UIBarButtonItem* item in itemsToRemove){
            [toolbarItems removeObject:item];
        }
        self.toolbar.items = toolbarItems;
        [toolbarItems release];
    }
    [itemsToRemove release];
}

- (void)viewDidLoad {

    [super viewDidLoad];
    
    [self removeSearchBar];
    
    upArrow = [[UIImage imageNamed:@"2uparrow.png"] retain];
	downArrow = [[UIImage imageNamed:@"2downarrow.png"] retain];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        titleLabel.text = title;
    else{
        titleBarButtonItem.title = title;
        titleBarButtonItem.tintColor=[UIColor blackColor];
    }
    
    searchBar.placeholder = NSLocalizedString(@"SEARCH_TITLE", nil);
    
    searchBar.text = _searchText;

    [self loadItems];
    
	// Adjust table view
    
    if (IS_IPHONE_6_OR_MORE) {
//        plainTableView = [[RTUITableView alloc] initWithFrame:itemsView.bounds style:UITableViewStylePlain];
//        plainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//        plainTableView.rowHeight = 60;
//        plainTableView.scrollsToTop = YES;
//        plainTableView.separatorColor = MO_RGBCOLOR(38, 38, 38);
//        UIView *backView = [[[UIView alloc] initWithFrame:plainTableView.bounds] autorelease];
//        backView.backgroundColor = [UIColor clearColor];
//        [plainTableView setBackgroundView:backView];
//        [plainTableView setBackgroundColor:[UIColor clearColor]];
//        [plainTableView setSeparatorInset:UIEdgeInsetsZero];
//        [plainTableView setLayoutMargins:UIEdgeInsetsZero];
//        plainTableView.delegate = self;
//        plainTableView.dataSource = self;
//        [plainTableView registerClass:[BaseSearchTableViewCell class] forCellReuseIdentifier:self.plainCellReuseIdentifier];
//        [itemsView addSubview:plainTableView];
        
        plainListView = [[RTBaseListView alloc] initWithFrame:itemsView.bounds];
        plainListView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        plainListView.tableView.rowHeight = 60;
        plainListView.tableView.scrollsToTop = YES;
        plainListView.tableViewDelegateAndDataSource = self;
        [plainListView.tableView registerClass:[BaseSearchTableViewCell class] forCellReuseIdentifier:self.plainCellReuseIdentifier];
        plainListView.delegate = self;
        [itemsView addSubview:plainListView];
        
        plainListView.headerHeight = 96;
        [plainListView.headerView addSubview:self.searchView];
        self.searchView.frame = CGRectMake(0, 0, plainListView.bounds.size.width, 47);
        
        [totalLabel release];
        totalLabel = [plainListView.countLabel retain];
        
        if ([[self availableForSortDescriptions] count]) {
            plainListView.sortableColumns = [self availableForSortDescriptions];
            SortingMode order = [self defaultOrder];
            GridTableColumnDescription *defaultSort = [self defaultSortColumnDescription];
            
            [plainListView setCurrentSortDescription:defaultSort order:order];
        }
    }
    else {
        itemTableView = [[GridTable alloc] initWithFrame:itemsView.bounds];
        itemTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self adjustTableView];
        
        itemTableView.selectRowOnReload = NO;
        
        [itemsView addSubview:itemTableView];
        [totalView setRoundedCorners:UIViewRoundedCornerLowerLeft | UIViewRoundedCornerLowerRight radius:5.0f];
    }
    
	searchBar.delegate = self;
    [ActionsHelper cancelSynchronization];
    //here reload data will be called
    itemTableView.delegate = self;
    
    if (![itemList count])
        [searchBar becomeFirstResponder];

    searchBar.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    if (self.needToShowKeyboardHideButton)
        [btnKeyboardSwitch boundSearchBar:searchBar selected:self.keyboardHideButtonDefaultValue];
    else
        [self removeKeyboardHideButton];
}


-(void)removeSearchBar{
    if (!IS_IPHONE_6_OR_MORE) {
        NSMutableArray *itemsArray = [NSMutableArray arrayWithArray:toolbar.items];
        [itemsArray removeObjectAtIndex:1];
        toolbar.items = itemsArray;
        
        self.searchBarBig = nil;
        self.searchBarButtonItemBig = nil;
    }
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
        searchBar.frame = CGRectMake(searchBar.frame.origin.x, searchBar.frame.origin.y, searchBar.frame.size.width, 56);
    else {
        
        searchBar.frame = CGRectMake(searchBar.frame.origin.x, searchBar.frame.origin.y, searchBar.frame.size.width, 60);
        [bgView setRoundedCorners:UIViewRoundedCornerAll
                           radius:5];
    }
    
    [self resetKeyboardState];
    
#ifdef __IPHONE_7_0
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
        toolbar.barStyle = UIBarStyleBlack;
#endif
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if (IS_IPHONE_6_OR_MORE) {
        self.searchView.frame = CGRectMake(0, 0, plainListView.bounds.size.width, 47);
    }
}

-(void)resetKeyboardState{
    if (self.needToShowKeyboardHideButton){
        BOOL defaultValue = self.keyboardHideButtonDefaultValue;
        [searchBar setKeyboardVisible:!defaultValue];
        btnKeyboardSwitch.selected = defaultValue;
    }
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Disabling of keyboardswitch
    //[btnKeyboardSwitch saveSetting];
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
*/

#pragma mark - Actions

- (IBAction) returnButtonClick {
    _dialogResult = RTDialogResultCancel;
	[self close];
}

#pragma mark -
#pragma mark GridTableDelegate

- (void)gridTable:(GridTable*)gridTable setContentForRow:(NSInteger)row column:(GridTableColumnDescription *)column content:(id)content {
    [itemTableView setContentForRowDefault:row columnView:content column:column value:itemList[row]];
    if (_searchText && [content isKindOfClass:[UILabel class]]){
        UILabel* fontLabel = (UILabel*)content;
        NSMutableAttributedString* coloredText = [fontLabel.text stringByHighlightingString:_searchText normalFont:fontLabel.font highlightFont:[UIFont boldSystemFontOfSize:fontLabel.font.pointSize] color:highlightColor];
        if (coloredText){
            fontLabel.attributedText = coloredText;
        }
    }
}

- (NSInteger)gridTableNumberOfRows:(GridTable*)gridTable {
    totalLabel.text = [NSString stringWithFormat:NSLocalizedString(@"SEARCH_RESULTCOUNT_FORMAT_TEXT", nil), [itemList count]];
	return [itemList count];
}

- (void)gridTable:(GridTable*)gridTable sortByColumn:(GridTableColumnDescription *)column order:(SortingMode)order {
    [self gridTable:gridTable sortByColumnName:column.name order:order];
}

- (void)gridTable:(GridTable*)gridTable sortByColumnName:(NSString*)columnName order:(SortingMode)order {
    [self applySortByColumnName:columnName order:order];
}

- (void)gridTable:(GridTable*)gridTable didSelectRow:(NSInteger)row {
    [selectedValue release];
    selectedValue  = nil;
    if ([itemList count]){
        selectedValue = [itemList[[itemTableView selectedRowIndex]]retain];
        _dialogResult = RTDialogResultOK;
        [self close];
    }
}

- (void)gridTable:(GridTable*)gridTable didDeselectRow:(NSInteger)row {
    
}

- (void)applySortByColumnName:(NSString*)columnName order:(SortingMode)order {
    if ([itemList conformsToProtocol:@protocol(IObjectList)]){
        id<IObjectList> objectList = (id<IObjectList>)itemList;
        if (order != NotSorted){
            objectList.sortOrderList = @[[SqlSortOrder orderBy:columnName order:order == AscendingSorted ? SqlSortOrderAscending : SqlSortOrderDescending]];
        }
        else{
            objectList.sortOrderList = nil;
        }
    }
    else if ([itemList isKindOfClass:[NSSortedArray class]]){
        NSSortedArray* sortedArray = (NSSortedArray*)itemList;
        if (order != NotSorted) {
            [sortedArray sortByKey:columnName ascending:order == AscendingSorted];
        }else {
            [sortedArray removeSort];
        }
    }
    [self.plainListView.tableView reloadData];
}

#pragma mark -
#pragma mark UISearchBarDelegate

// called when keyboard search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar{
    [aSearchBar resignFirstResponder];
    [_searchText release];
    _searchText = [[aSearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] retain];
    aSearchBar.text = _searchText;
    if (_searchText.length<1) return;
    if ([itemList conformsToProtocol:@protocol(ILazyLoadableList)]){
        [ActionsHelper cancelSynchronization];
    }
    [self refreshList:_searchText completed:^(BOOL completed){
        if (completed){
            [itemTableView reloadData];
            [itemTableView resetScroll];
            [itemsView setContentOffset:CGPointZero animated:YES];
            pageControl.currentPage = 0;
            //aSearchBar.text = nil;
            if (IS_IPHONE_6_OR_MORE) {
                [plainListView.tableView reloadData];
                if ([itemList count])
                    [plainListView.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
        }
        if (![itemList count]) {
            [searchBar becomeFirstResponder];
        }
    }];
}



- (void)refreshList:(NSString*)text completed:(void(^)(BOOL))completed{
    BOOL result = FALSE;
    if (searchBlock){
        id searchPredicate = searchBlock(text);
        if ([itemList conformsToProtocol:@protocol(ILazyLoadableList)]){
//            result = TRUE;
            id<ILazyLoadableList> lazyList = (id<ILazyLoadableList>)itemList;
            [lazyList setFilterCondition:searchPredicate];
            BackgroundOperationViewController* progressController = [[BackgroundOperationViewController alloc]init];
            [progressController setTitle:NSLocalizedString(@"STATUS_SEARHING", nil)];
            [progressController startTask:^{
                [itemList count];
            } delay:0.5f completed:^{
                if (completed)
                    completed(TRUE);
            }];
            [progressController release];
            return;
        }
        else if ([itemList isKindOfClass:[NSSortedArray class]]){
            NSSortedArray* sortedArray = (NSSortedArray*)itemList;
            if ([searchPredicate isKindOfClass:[NSPredicate class]]){
                [sortedArray applyFilterWithPredicate:searchPredicate];
                result = TRUE;
            }
        }
    }
    if (completed){
        completed(result);
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)aSearchBar {
    [aSearchBar.textField selectAllNoMenu];
}

#pragma mark -
#pragma mark Virtual methods

- (void) loadItems {
}

- (void)adjustTableView {
}

#pragma mark - Actions

-(void)close{
    _closing = TRUE;
    [self dismissViewControllerAnimated:YES completion:nil];
    _closing = FALSE;
}

- (IBAction)btnSelectClick:(id)sender {
    [selectedValue release];
    selectedValue  = nil;
    if ([itemList count]){
        selectedValue = [itemList[[itemTableView selectedRowIndex]]retain];
        _dialogResult = RTDialogResultOK;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)btnCancelClick:(id)sender {
    [selectedValue release];
    selectedValue = nil;
    _dialogResult = RTDialogResultCancel;
    [self close];
}

- (IBAction)controlPageValueChanged:(id)sender {
    [itemsView setContentOffset:CGPointMake(pageControl.currentPage * itemsView.frame.size.width, 0) animated:YES];
}

-(id)selectedValue{
    return selectedValue;
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControl.currentPage = itemsView.contentOffset.x / itemsView.frame.size.width;
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    totalLabel.text = [NSString stringWithFormat:self.countFormatString, [itemList count]];
    return [itemList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    BaseSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.plainCellReuseIdentifier forIndexPath:indexPath];
    if (!cell)
        cell = [[[BaseSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:self.plainCellReuseIdentifier] autorelease];
    
    NSObject *obj = itemList[indexPath.row];
    [cell displayObject:obj currentSearchText:_searchText highlightColor:highlightColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [selectedValue release];
    selectedValue  = nil;
    if ([itemList count]){
        selectedValue = [itemList[indexPath.row]retain];
        _dialogResult = RTDialogResultOK;
        [self close];
    }
}

#pragma mark - iPhone 6 Plain table

-(void)baseList:(RTBaseListView *)listView sortWithColumn:(GridTableColumnDescription *)column order:(SortingMode)order {
    [self applySortByColumnName:column.name order:order];
}

- (RTBaseListView *)plainListView {
    return plainListView;
}

- (NSString *)plainCellReuseIdentifier {
    return @"BaseSearchCell";
}

-(NSArray *)availableForSortDescriptions {
    return nil;
}

-(GridTableColumnDescription *)defaultSortColumnDescription {
    return nil;
}

-(SortingMode)defaultOrder {
    return NotSorted;
}

@end
