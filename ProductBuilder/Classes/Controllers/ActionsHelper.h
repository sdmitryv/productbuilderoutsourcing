//
//  MyClass.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 10/11/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncOperation.h"
#import "ActionTracker.h"
#import "Location.h"

@interface ActionsHelper : NSObject

+(void)cancelSynchronization;
+(void)showLocationMismatchMessage:(LocationBase*)originLocation newLocation:(LocationBase*)newLocation completion:(void (^)(void))aCompletion;
+(BOOL)checkSyncOperationErrorForLocationMismatch:(SyncOperation*)syncOperation;
+(BOOL)checkSyncOperationErrorForLocationMismatch:(SyncOperation*)syncOperation completion:(void (^)(void))aCompletion;
+(BOOL)validateSyncOperationForLocationMismatchError:(SyncOperation*)syncOperation error:(NSError**)error;

@end
