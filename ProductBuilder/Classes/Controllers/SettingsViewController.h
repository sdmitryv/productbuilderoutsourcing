//
//  SettingsViewController.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 5/14/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewControllerExtended.h"
#import "UIRoundedCornersView.h"
#import "Settings.h"
#import "SimpleTableListViewController.h"
#import "UICustomSwitchYesNo.h"
#import "AppSettingManager.h"

@interface SettingsViewController : UIViewControllerExtended <SimpleTableListViewControllerDelegate> {
    BOOL    _showItemImage;
    ItemDescriptionModeView _itemDescriptionMode;
    ItemAttributeModeView   _itemAttributeMode;
    SimpleTableListViewController   * _descriptionListViewController;
    SimpleTableListViewController   * _attributeListViewController;
}

@property (nonatomic, retain) IBOutlet UILabel                  * itemImageLabel;
@property (nonatomic, retain) IBOutlet UIRoundedCornersView     * itemImageView;
@property (nonatomic, retain) IBOutlet UICustomSwitchYesNo      * itemImageSwitch;
@property (nonatomic, retain) IBOutlet UIRoundedCornersView     * line1View;
@property (nonatomic, retain) IBOutlet UILabel                  * line1Label;
@property (nonatomic, retain) IBOutlet UILabel                  * line1ValueLabel;
@property (nonatomic, retain) IBOutlet UIButton                 * line1Button;
@property (nonatomic, retain) IBOutlet UIRoundedCornersView     * line2View;
@property (nonatomic, retain) IBOutlet UILabel                  * line2Label;
@property (nonatomic, retain) IBOutlet UILabel                  * line2ValueLabel;
@property (nonatomic, retain) IBOutlet UIButton                 * line2Button;

-(IBAction)cancelButtonPress:(id)sender;
-(IBAction)saveButtonPress:(id)sender;
-(IBAction)line1ButtonPress:(id)sender;
-(IBAction)line2ButtonPress:(id)sender;

@end
