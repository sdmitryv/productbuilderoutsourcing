//
//  MainViewController50.swift
//  ProductBuilder
//
//  Created by valery on 12/15/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation


class MainViewController50 : UIViewControllerExtended{
    
    @IBOutlet var appNavigationItem: AppNavigationItem?
    @IBOutlet var maintananceViewContainer:UIView?
    var maintananceViewController: MaintananceViewController50?
    var productsViewController: ProductsViewController?
    private var skipInitAlert:Bool = false
    
    var modifications : NSMutableArray = NSMutableArray()
    var productLabels : NSMutableArray = NSMutableArray()
    var productValues : NSMutableArray = NSMutableArray()
    var settingValues : NSMutableDictionary = NSMutableDictionary()
    
    static func sharedInstance()->MainViewController50?{
        
        return SuperViewController50.instance()?.mainViewController
    }
    

   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()

        maintananceViewContainer?.backgroundColor = .clear
        let btnLeft = UIButton(type: .custom)
        btnLeft.titleLabel?.numberOfLines = 0
        btnLeft.titleLabel?.lineBreakMode = .byWordWrapping
        btnLeft.translatesAutoresizingMaskIntoConstraints = true
        btnLeft.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
        let leftBarButton = UIBarButtonItem(customView: btnLeft)
        self.appNavigationItem?.leftBarButtonItem = leftBarButton
        self.appNavigationItem?.isLeftBarButtonItemsHidden = true
        self.appNavigationItem?.isRightBarButtonItemsHidden = false
        SuperViewController50.instance()?.isStatusBarHidden = false
    }
    
    
    static var handleFirstRun : Bool = false
    static var needUpadteProductData : Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if (!type(of:self).handleFirstRun){
            
            type(of:self).handleFirstRun = true
            
            DispatchQueue.global().async {
                self.maintananceViewController?.startup()
                DispatchQueue.main.async {
                    
                    let overlay = UIVisualEffectView()
                    overlay.frame = (self.maintananceViewController?.view.bounds)!
                    self.maintananceViewController?.view.addSubview(overlay)
                    
                    self.viewDidAppearInternal()
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self.maintananceViewController?.view.alpha = 0
                        overlay.effect = UIBlurEffect(style:.light)
                        //overlay.alpha = 0
                    }) { _ in
                        self.maintananceViewContainer?.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height)
                        overlay.removeFromSuperview()
                    }
                }
                
                self.login()
            }
        }
        
        if (MainViewController50.needUpadteProductData) {
            
            loadProductData()

        }
        
        productsViewController?.products = ProductList()
        
    }
    
    func viewDidAppearInternal() {
        
        loadProductData()
        
        self.enableButtons()
        StatusViewController50.sharedInstance()?.lastSyncTime = AppSettingManager.instance().lastSyncDate as Date!
        
        let rootRef = Database.database().reference()
        rootRef.observe(DataEventType.value, with: { (snapshot) in
            
            let snapshotDictionary = snapshot.value as? NSDictionary
            
            
            do {
                
                let jsonData = try JSONSerialization.data(withJSONObject: snapshotDictionary ?? "{}", options: [])
                
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
                if (jsonString != nil) {
                    
                    AppSettingManager.instance().productBuilderData = jsonString! as String
                    
                    MainViewController50.needUpadteProductData = true
                }
            } catch {
            }
        })
    }
    
    func loadProductData() {
        
        MainViewController50.needUpadteProductData = false;
        
        let jsonString = AppSettingManager.instance().productBuilderData
        
        if (jsonString == nil) {
            
            return;
        }
        
        let jsonData = jsonString?.data(using: .utf8)
        
        do {
            
            let json = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSDictionary
            
            modifications.removeAllObjects()
            productLabels.removeAllObjects()
            productValues.removeAllObjects()
            settingValues.removeAllObjects()
            
            if (json["dataList"] != nil) {
                
                if ((json["dataList"] as! NSDictionary)["valuesLabels"] != nil) {
                    
                    productLabels.add((json["dataList"] as! NSDictionary)["valuesLabels"] as! NSArray)
                }
                
                if ((json["dataList"] as! NSDictionary)["valuesList"] != nil) {
                    
                    productValues.add((json["dataList"] as! NSDictionary)["valuesList"] as! NSArray)
                }
            }
            
            if (json["modifications"] != nil) {
                
                if ((json["modifications"] as! NSDictionary)["categories"] != nil) {
                    
                    modifications.add((json["modifications"] as! NSDictionary)["categories"] as! NSArray)
                }
            }

            if (json["settings"] != nil) {
                
                settingValues.setDictionary(json["settings"] as! Dictionary)
                AppSettingManager.instance().serverApiKey = settingValues["apiKey"] as! String
                AppSettingManager.instance().serverUrl = settingValues["server"] as! String
                NSCurrencyFormatter.defaultCurrencyFormatter().currencyCode = settingValues["currencyCode"] as! String
                if let inputBasePrice = settingValues["userInputBasePrice"] as? NSNumber {
                    AppSettingManager.instance().userInputBasePrice = inputBasePrice.intValue
                }
                if let inputOrderCost = settingValues["userInputOrderCost"] as? NSNumber {
                    AppSettingManager.instance().userInputOrderCost = inputOrderCost.intValue
                }
                if let sourceDescription4 = settingValues["useSourceDescription4"] as? NSNumber {
                    AppSettingManager.instance().useSourceDescription4 = sourceDescription4.boolValue
                }
                

            }
        } catch {
        }
        
    }
    
    func enableButtons(){
        
        SuperViewController50.instance()?.isStatusBarHidden = false
        self.appNavigationItem?.isLeftBarButtonItemsHidden = true
        self.appNavigationItem?.isRightBarButtonItemsHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.destination is MaintananceViewController50){
            maintananceViewController = segue.destination as? MaintananceViewController50
            maintananceViewController?.needToBeShown = { [unowned self] in
                
                let uiUpdateBlock = {
                    UIView.animate(withDuration: 1, animations: {
                        self.maintananceViewController?.view.alpha = 1
                        self.maintananceViewContainer?.transform = CGAffineTransform.identity
                    })
                }
                
                if Thread.isMainThread {
                    uiUpdateBlock()
                }
                else {
                    DispatchQueue.main.async {
                        uiUpdateBlock()
                    }
                }
                
            }
            maintananceViewController?.needToBeClosed = { [unowned self] in
                
                let uiUpdateBlock = {
                    UIView.animate(withDuration: 1, animations: {
                        self.maintananceViewController?.view.alpha = 0
                    }) { _ in
                        self.maintananceViewContainer?.transform = CGAffineTransform(translationX: 0, y: self.view.bounds.size.height)
                    }
                }
                
                if Thread.isMainThread {
                    uiUpdateBlock()
                }
                else {
                    DispatchQueue.main.async {
                        uiUpdateBlock()
                    }
                }
            }
        }
        else if segue.destination is ProductsViewController {
            productsViewController = segue.destination as? ProductsViewController
        }
    }
    
    private func login() {
        if SecurityManager.currentEmployee() == nil {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "LoginViewControllerSegue", sender:self);
            }
        }

    }
    
    // MARK: - Actions
    private lazy var startPing: Void = {
        
        VerifyTimeMessageViewController50.verifyTime(completionBlock: { (Bool) in
            PingManager.instance().startPing()
            self.maintananceViewController?.checkDeviceAgentState()
        })
    }()
}
