//
//  ProductDetailsViewController.swift
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 8/31/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var product: Product?
    
    @IBOutlet var cluLabel: UILabel!
    @IBOutlet var itemDetailsTableView: UITableView!
    @IBOutlet var copiedImageView: UIImageView!
    @IBOutlet var copiedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cluLabel.text = "CLU \(product!.clu!)"
        itemDetailsTableView.tableFooterView = UIView(frame: CGRect.zero)
        copiedImageView.image = UIImage(named: "checked.png")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        copiedImageView.tintColor = UIColor(r: 69, g: 120, b: 9)
        copiedImageView.isHidden = true
        copiedLabel.isHidden = true
    }

    @IBAction func closeButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func copyButtonClick(_ sender: Any) {
        if product != nil {
            let pasteboard = UIPasteboard.general
            pasteboard.string = product!.clu
            copiedImageView.isHidden = false
            copiedLabel.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemDescriptionCellIdentifier")
            let descriptionLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
            descriptionLabel?.text = product?.description
        case (0, 1):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceCellIdentifier")
            let priceLabel: UILabel? = cell?.contentView.viewWithTag(1) as? UILabel
            priceLabel?.text = NSCurrencyFormatter.formatCurrency(from: product?.price)
        case (1, 0):
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemInstructionsCellIdentifier")
            let instuctionsTextView: UITextView? = cell?.contentView.viewWithTag(1) as? UITextView
            instuctionsTextView?.text = product?.instructions
        default:
            cell = UITableViewCell()
        }
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Product" : "Instructions"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat = 44
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let description : String = product!.description
            let constraintRect = CGSize(width: 400, height: 300)
            let boundingBox = description.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)], context: nil)
            height = ceil(boundingBox.height) + 16
        case (0, 1):
            height = 44
        case (1, 0):
            height = 60
        default:
            height = 1
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 40 : 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 700, height: 40))
        footerView.backgroundColor = UIColor.white
        let separatorView = UIView(frame: CGRect(x: 0, y: 0, width: 700, height: 0.5))
        separatorView.backgroundColor = UIColor.lightGray
        footerView.addSubview(separatorView)
        return footerView
    }
}
