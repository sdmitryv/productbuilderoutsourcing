//
//  RTMessageController.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/5/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewControllerExtended.h"
#import "UIRoundedCornersView.h"

@interface RTMessageController : UIViewControllerExtended

@property (retain, nonatomic) IBOutlet UIView *titleView;
@property (retain, nonatomic) IBOutlet UIRoundedCornersView *detailsView;
@property (retain, nonatomic) IBOutlet UILabel *detailsLabel;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIButton *bClose;
- (IBAction)close:(id)sender;
-(void)adjustSize;
@end
