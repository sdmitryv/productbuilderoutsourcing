//
//  BaseListViewController.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/8/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridTable.h"
#import "UIViewControllerExtended.h"
#import "NSSortedArray.h"
#import "IObjectList.h"
#import "RTKeyboardSwitchButton.h"
#import "UIRoundedCornersView.h"
#import "RTDialogViewController.h"
#import "RTSelectorButton.h"
#import "RTUITableView.h"
#import "RTBaseListView.h"

typedef id(^SearchBlock)(NSString*);

@interface BaseFindViewController : UIViewControllerExtended <GridTableDelegate, UISearchBarDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, RTBaseListViewDelegate> {
    //NSObject<IObjectList>*           itemList;
    id                        itemList;
    BOOL                      itemDetailsIsHidden;
    UIImage                 * upArrow;
    UIImage                 * downArrow;
    NSString                * title;
    
    // Controls
    
    UIToolbar               * toolbar;
    GridTable               * itemTableView;
    RTBaseListView          * plainListView;
	UISearchBar             * searchBar;
    UISearchBar             * searchBarBig;
    UIScrollView            * itemsView;
    UIRoundedCornersView    * totalView;
    UILabel                 * selectedLabel;
    UILabel                 * totalLabel;
    UIButton                * btnSelect;
    UIButton                * btnCancel;
    UIPageControl           * pageControl;
    id                        selectedValue;
    SearchBlock 			  searchBlock;
    NSString                * _searchText;
    UIColor*  				  highlightColor;
    UIColor*  				  highlightSelectedColor;
    RTKeyboardSwitchButton  * btnKeyboardSwitch;
    UIRoundedCornersView    * bgView;
    BOOL _closing;
    RTDialogResult          _dialogResult;
}

@property (nonatomic, retain) IBOutlet RTKeyboardSwitchButton *btnKeyboardSwitch;

@property (nonatomic, retain) IBOutlet UILabel         * titleLabel;
@property (nonatomic, retain) IBOutlet UIBarButtonItem * titleBarButtonItem;
@property (nonatomic, retain) IBOutlet UIToolbar       * toolbar;
@property (nonatomic, retain) IBOutlet UISearchBar     * searchBar;
@property (nonatomic, retain) IBOutlet UISearchBar     * searchBarBig;
@property (nonatomic, retain) IBOutlet UIScrollView    * itemsView;
@property (nonatomic, retain) IBOutlet UIView          * totalView;
@property (nonatomic, retain) IBOutlet UILabel         * totalLabel;
@property (nonatomic, retain) IBOutlet UILabel         * selectedLabel;
@property (nonatomic, retain) IBOutlet UIButton         * btnSelect;
@property (nonatomic, retain) IBOutlet UIButton         * btnCancel;
@property (nonatomic, retain) IBOutlet UIPageControl    * pageControl;
@property (nonatomic, retain) IBOutlet UIRoundedCornersView    * bgView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *searchBarButtonItem;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *searchBarButtonItemBig;
@property (retain, nonatomic) IBOutlet UIView *searchView;
// to display your custom cell you should subclass BaseSearchTableViewCell and rigister it for plainTableView with this reuse identifier (for iPhone 6 only)
@property (nonatomic, readonly) NSString *plainCellReuseIdentifier;
@property (nonatomic, readonly) RTBaseListView          * plainListView;
@property (nonatomic, retain) NSString *countFormatString;

@property (nonatomic, readonly)id selectedValue;
@property (nonatomic, readonly)RTDialogResult dialogResult;
@property (nonatomic, readonly) NSString *searchText;

- (IBAction)returnButtonClick;
- (IBAction)btnSelectClick:(id)sender;
- (IBAction)btnCancelClick:(id)sender;
- (IBAction)controlPageValueChanged:(id)sender;
- (void)gridTable:(GridTable*)gridTable sortByColumnName:(NSString*)columnName order:(SortingMode)order;
- (id)initWithTitle:(NSString *)text list:(id)list desired:(NSString *)desired;
- (id)initWithTitle:(NSString *)text list:(id)list searchBlock:(SearchBlock)searchBlock desired:(NSString *)desired;
- (void)loadItems;
- (void)adjustTableView;
- (id)selectedValue;
- (void)refreshList:(NSString*)text completed:(void(^)(BOOL aCompleted))completed;
-(void)initDefaults;
-(void)close;
-(BOOL)needToShowKeyboardHideButton;
-(BOOL)keyboardHideButtonDefaultValue;
-(NSArray *)availableForSortDescriptions;
-(GridTableColumnDescription *)defaultSortColumnDescription;
-(SortingMode)defaultOrder;
@end
