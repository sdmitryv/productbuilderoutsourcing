//
//  NSMutableDictionaryFast.m
//  ProductBuilder
//
//  Created by valera on 1/17/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "NSMutableDictionaryFast.h"
#if (defined __IPHONE_7_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    #include <unordered_map>
#else
    #include <tr1/unordered_map>
#endif

namespace {
    struct eqid
    {
        bool operator()(const id s1, const id s2) const
        {
            return (s1 == s2) || [s1 isEqual:s2];
        }
    };
    
    struct hashid
    {
        size_t operator()(const id s1) const
        {
            return (size_t)[s1 hash];
        }
    };
}

#if (defined __IPHONE_7_0) && __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
typedef std::unordered_map<id, id, hashid, eqid> dictionary;
#else
    typedef std::tr1::unordered_map<id, id, hashid, eqid> dictionary;
#endif



@interface NSMutableDictionaryFast()
{
    dictionary map;
}
@end

@implementation NSMutableDictionaryFast

-(id)init{
    if ((self = [super init])){
    }
    return self;
}
-(id)initWithCapacity:(NSUInteger)numItems{
    if ((self = [super init])){
        map.rehash(numItems);
    }
    return self;
}

-(void)setObject:(id)value forKeyedSubscript:key{
    [self setObject:value forKey:key];
}

- (void)setObject: (id)obj forKey: (id)key
{
    if (!obj){
        [NSException raise:NSInvalidArgumentException format:@"Key cannot be nil"];
    }
    [obj retain];
    [key retain];
    //key = [key copy];
    //map[key] = obj;
    dictionary::iterator it = map.find(key);
    if (it!=map.end()){
        [self releaseIterator:it];
        map.erase(it);
    }
    map.insert(std::make_pair<NSObject*,NSObject*>(key, obj));
}

- (void)removeObjectForKey:(id)key
{
    dictionary::iterator it = map.find(key);
    if (it!=map.end()){
        [self releaseIterator:it];
        map.erase(it);
    }
}

-(void)removeAllObjects{
    [self releaseValues];
    map.clear();
}

- (NSUInteger)count
{
    return (NSUInteger)map.size();
}

- (id)objectForKey: (id)key
{
    //return map[key];
    dictionary::iterator it = map.find(key);
    if (it!=map.end()){
        return it->second;
    }
    return nil;
}

-(id)objectForKeyedSubscript:(id)key{
    return [self objectForKey:key];
}

- (void)enumerateKeysAndObjectsUsingBlock:(void (^)(id key, id obj, BOOL *stop))block{
    if (!block) return;
    dictionary::iterator it = map.begin();
    BOOL stop = FALSE;
    while(!stop && it != map.end())
    {
        block( it->first, it->second, &stop);
        ++it;
    }
}

-(void)releaseIterator:(dictionary::iterator)it{
    [it->first release];
    [it->second release];
}

-(void)releaseValues{
    for (dictionary::iterator it= map.begin(); it != map.end();  ++it) {
        [self releaseIterator:it];
    }
}

-(void)dealloc{
    [self releaseValues];
    [super dealloc];
}
@end
