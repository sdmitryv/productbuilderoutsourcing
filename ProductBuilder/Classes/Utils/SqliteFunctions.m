//
//  SqliteFunctions.m
//  ProductBuilder
//
//  Created by Roman on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SqliteFunctions.h"
#import <libxml/encoding.h>


char *only_digits(const char *string) {
    int len=0;
    while (string[len]!=0) len++;
    
    char *result = malloc(len+1);
    int k=0;
    for (int i=0; i<len; i++) {
        if (string[i] >= '0' && string[i] <= '9') {
            result[k] = string[i];
            k++;
        }
    }
    result[k] = 0;
    return result;
}



void sqlite3_onlydigits(sqlite3_context *context, int argc, sqlite3_value **argv) {
    const char *zString = sqlite3_value_text16(argv[0]);
    if( !zString ){
        return;
    }
    int len=0;
    while (zString[len]!=0) {
        len++;
    }
    char *result = only_digits(zString);
    sqlite3_result_text( context, result, -1, SQLITE_TRANSIENT );
    if (result) {
        free(result);
    }
}



#define SWP(x,y) (x^=y, y^=x, x^=y)

void strrev(char *p)
{
    char *q = p;
    while(q && *q) ++q; /* find eos */
    for(--q; p < q; ++p, --q) SWP(*p, *q);
}

void strrev_utf8(char *p)
{
    char *q = p;
    strrev(p); /* call base case */
    
    /* Ok, now fix bass-ackwards UTF chars. */
    while(q && *q) ++q; /* find eos */
    while(p < --q)
        switch( (*q & 0xF0) >> 4 ) {
            case 0xF: /* U+010000-U+10FFFF: four bytes. */
                SWP(*(q-0), *(q-3));
                SWP(*(q-1), *(q-2));
                q -= 3;
                break;
            case 0xE: /* U+000800-U+00FFFF: three bytes. */
                SWP(*(q-0), *(q-2));
                q -= 2;
                break;
            case 0xC: /* fall-through */
            case 0xD: /* U+000080-U+0007FF: two bytes. */
                SWP(*(q-0), *(q-1));
                q--;
                break;
        }
}

void sqlite3_strinvert(sqlite3_context *context, int argc, sqlite3_value **argv) {
    
    const char *paramCStr = (const char *)sqlite3_value_text(argv[0]);
    if(!paramCStr)
        return;
    
    char * retStr = sqlite3_malloc((int)strlen(paramCStr) + 1);
    strcpy(retStr, paramCStr);
    strrev_utf8(retStr);
    
    sqlite3_result_text(context, retStr, -1, SQLITE_TRANSIENT);
    sqlite3_free(retStr);
}



//#include <regex.h>
//
//char *regexp (char *string, char *patrn, int *begin, int *end) {    
//    int i, w=0, len;                 
//    char *word = NULL;
//    regex_t rgT;
//    regmatch_t match;
//    regcomp(&rgT,patrn,REG_EXTENDED);
//    if ((regexec(&rgT,string,1,&match,0)) == 0) {
//        *begin = (int)match.rm_so;
//        *end = (int)match.rm_eo;
//        len = *end-*begin;
//        word=malloc(len+1);
//        for (i=*begin; i<*end; i++) {
//            word[w] = string[i];
//            w++; }
//        word[w]=0;
//    }
//    regfree(&rgT);
//    return word;
//}
//
//void sqlite3_regexp_replace(sqlite3_context *context, int argc, sqlite3_value **argv) {
//    const char *zString = sqlite3_value_text16(argv[1]);
////    const UChar *zString = sqlite3_value_text16(argv[1]);
//    
//    /* If the text is NULL, then the result is also NULL. */
//    if( !zString ){
//        return;
//    }
//    
//    const char *zReplacement = sqlite3_value_text16(argv[2]);
////    const UChar *zReplacement = sqlite3_value_text16(argv[2]);
//    if ( !zReplacement ) {
//        sqlite3_result_error(context, "no replacement string", -1);
//        return;
//    }
//    
//    const char *zPattern = sqlite3_value_text16(argv[0]);
////    const UChar *zPattern = sqlite3_value_text16(argv[0]);
//    if( !zPattern ){
//        return;
//    }
//    int b,e;
//    const char *result = regexp((char*)zString,(char*)zPattern,&b,&e);
//    sqlite3_result_text( context, result, -1, SQLITE_TRANSIENT );
//}
