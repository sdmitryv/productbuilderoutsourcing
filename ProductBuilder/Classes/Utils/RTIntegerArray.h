//
//  RTIntegerArray.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 5/28/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTIntegerArray : NSObject{//<NSFastEnumeration>{
    CFMutableArrayRef array;
}
- (NSUInteger)count;
- (NSUInteger)indexOfInteger:(NSUInteger)value;
- (void)addInteger:(NSUInteger)value;
- (NSArray *)subarrayWithRange:(NSRange)range;
- (NSNumber*)objectAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfObject:(NSNumber*)object;
@end
