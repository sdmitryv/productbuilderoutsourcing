//
//  MathUtils.m
//  ProductBuilder
//
//  Created by Valera on 10/20/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "MathUtils.h"
#import "DecimalHelper.h"

@implementation MathUtils


+(NSDecimal)roundMoney:(NSDecimal) value{
	/*
	NSDecimalNumberHandler *behavior = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundBankers scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
	NSDecimalNumber *d = [[NSDecimalNumber alloc]initWithDecimal:value];
	NSDecimal result = [[d decimalNumberByRoundingAccordingToBehavior: behavior]decimalValue];
	[behavior release];
	[d release];
	return result;
	*/
    //unfortunatly, NSRoundBankers works incorrect with negative values :(
    if (NSDecimalIsNotANumber(&value)){
        return D0;
    }
	NSDecimal result;
    BOOL isNegative = value._isNegative;
    value._isNegative = FALSE;
	NSDecimalRound(&result, &value, 2, NSRoundBankers);
    result._isNegative = isNegative;
    if (NSDecimalIsNotANumber(&result)){
        result = D0;
    }
	return result;
}

+(NSDecimal)roundPercent:(NSDecimal) value{
    if (NSDecimalIsNotANumber(&value)){
        return D0;
    }
	NSDecimal result;
	NSDecimalRound(&result, &value, 2, NSRoundPlain);
    if (NSDecimalIsNotANumber(&result))
        result = D0;
	return result;
}

@end
