//
//  RTEncryption.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 5/18/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTEncryption : NSObject
+(NSString*)encrypt:(NSString*)string;
+(NSString*)decrypt:(NSString*)string;
@end
