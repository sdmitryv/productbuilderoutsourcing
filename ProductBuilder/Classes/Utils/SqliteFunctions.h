//
//  SqliteFunctions.h
//  ProductBuilder
//
//  Created by Roman on 3/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

//void sqlite3_regexp_replace(sqlite3_context *context, int argc, sqlite3_value **argv);
void sqlite3_onlydigits(sqlite3_context *context, int argc, sqlite3_value **argv);
void sqlite3_strinvert(sqlite3_context *context, int argc, sqlite3_value **argv);
