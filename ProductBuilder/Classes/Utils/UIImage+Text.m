//
//  UIImage+Text.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/24/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UIImage+Text.h"
#import <CoreText/CoreText.h>

@implementation UIImage (Text)

+(UIImage *)imageWithText:(NSString *)text size:(CGSize)size fontSize:(CGFloat)fontSize cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
// Fill by gradient
    
    CGGradientRef myGradient;
    CGColorSpaceRef myColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 1.0,  // Start color
        0.8, 0.8, 0.8, 1.0 }; // End color
    
    UIBezierPath *beizerPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(cornerRadius, cornerRadius, size.width - cornerRadius * 2, size.height - cornerRadius * 2)
                                                     byRoundingCorners:UIRectCornerAllCorners
                                                           cornerRadii:CGSizeMake(cornerRadius * 4, cornerRadius * 4)];
    
    myColorspace = CGColorSpaceCreateDeviceRGB();
    myGradient = CGGradientCreateWithColorComponents(myColorspace, components, locations, num_locations);
    
    CGContextSaveGState(context);
    CGContextAddPath(context, beizerPath.CGPath);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, myGradient, CGPointMake(0.0, 0.0), CGPointMake(300, 300), 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(myGradient);
    CGColorSpaceRelease(myColorspace);
    
// Stroke rounded corner rectangle
    
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    CGContextSetLineWidth(context, borderWidth);
    CGContextAddPath(context, beizerPath.CGPath);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    CGContextSaveGState(context);
    
// Text rendering
    
    CGContextTranslateCTM(context, 0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetTextMatrix(context, CGAffineTransformIdentity);
    
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    UIColor *color = [UIColor lightGrayColor];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: color,
                                NSParagraphStyleAttributeName: paragraphStyle};
    [paragraphStyle release];
    NSAttributedString * attrString = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)attrString);
    [attrString release];
    
    CGSize paragraphSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), (CFDictionaryRef)attributes, CGSizeZero, nil);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGRect bounds = CGRectMake(10.0, (size.height - paragraphSize.height) / 2, size.width - 20.0, paragraphSize.height);
    CGPathAddRect(path, NULL, bounds);
    
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), path, NULL);
    CTFrameDraw(frame, context);
    
    CFRelease(frame);
    CFRelease(path);
    CFRelease(framesetter);
    
    CGContextRestoreGState(context);
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

+ (UIImage *)imageForTableViewRowActionWithTitle:(NSString *)title textAttributes:(NSDictionary *)attributes backgroundColor:(UIColor *)color cellSize:(CGSize)cellSize
{
    NSString *titleString = title;
//    if (cellSize.width < 62) { // I've added this because I think that UITableViewRowAction has some min width, and in case when result image.width<minWidth image taled space...
//        cellSize.width = 62;
//    }
    CGRect drawingRect = CGRectIntegral(CGRectMake(0, 0, cellSize.width, cellSize.height));
    UIGraphicsBeginImageContextWithOptions(drawingRect.size, NO, [UIScreen mainScreen].nativeScale);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, color.CGColor);
    CGContextFillRect(contextRef, drawingRect);
    
    //draws border just to debug the image size
    //CGContextSetRGBStrokeColor(contextRef, 1.0, 1.0, 1.0, 1.0);
    //CGContextStrokeRectWithWidth(contextRef, CGRectMake(0, 0, cellSize.width, cellSize.height), 1);

    
//    UILabel *label = [[UILabel alloc] initWithFrame:drawingRect];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:attributes];
//    [label drawTextInRect:drawingRect];
//    [label release];
    
    //This is other way how to render string
    CGSize size = [titleString sizeWithAttributes:attributes];
    CGFloat x =  (drawingRect.size.width - size.width)/2;
    CGFloat y =  (drawingRect.size.height - size.height)/2;
    drawingRect.origin = CGPointMake(x, y);
    [titleString drawInRect:drawingRect withAttributes:attributes];
    
    UIImage *returningImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return returningImage;
}

@end
