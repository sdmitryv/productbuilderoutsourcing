//
//  VirtualNumberpadViewController.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 12/6/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumpadKeyboardController.h"
#import "RTPopoverController.h"

@class NumpadKeyboardController;

@interface VirtualNumberpadViewController : UIViewController <RTPopoverControllerDelegate> {
    NumpadKeyboardController    * _numpadKeyboardController;
	RTPopoverController         * popoverController;
}

@property (retain, nonatomic) IBOutlet UIView * keyboardView;
@property(nonatomic, readonly)RTPopoverController* popoverController;

- (id)initWithTextField:(UITextField *)textField;
- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
- (void)dismissPopover;

- (IBAction)cancelButtonClick;
@property (nonatomic, assign) UITextField* textField;

@property (nonatomic, assign) NumpadKeyboardContent contents;

@end
