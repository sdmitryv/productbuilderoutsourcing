//
//  UIImage+Arrow.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ArrowDirection) {ArrowDirectionRight, ArrowDirectionLeft, ArrowDirectionUp, ArrowDirectionDown};

@interface UIImage (Arrow)
+ (UIImage *)arrowImageWithSize:(CGSize)size color:(UIColor *)color direction:(ArrowDirection)direction;
@end
