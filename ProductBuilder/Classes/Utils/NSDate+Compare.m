//
//  NSDate+Compare.m
//  iPadPOS
//
//  Created by valera on 3/24/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "NSDate+Compare.h"
#import "NSDate+System.h"

@implementation NSDate (Compare)

-(BOOL)isEqualToDay:(NSDate*)anotherDate
{
    if (!anotherDate) return FALSE;
	NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
	NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
	NSDateComponents *componentsFromDate = [calendar components:unitFlags fromDate:self];
	NSDateComponents *componentsFromAnotherDate = [calendar components:unitFlags fromDate:anotherDate];
    componentsFromDate.timeZone = componentsFromAnotherDate.timeZone = calendar.timeZone;
	return (
			[componentsFromDate year] == [componentsFromAnotherDate year] &&
			[componentsFromDate month] == [componentsFromAnotherDate month] &&
			[componentsFromDate day] == [componentsFromAnotherDate day]
			);
}

- (BOOL)lessThanDate:(NSDate*)anotherDate{
    if ([self isEqualToDay:anotherDate]) return FALSE;
    return ([self compare:anotherDate]==NSOrderedAscending);
    
}

- (BOOL)lessOrEqualToDate:(NSDate*)anotherDate{
    if ([self isEqualToDay:anotherDate]) return TRUE;
    return ([self compare:anotherDate]==NSOrderedAscending);
    
}

- (BOOL)isFutureDate {
    NSDate* _today = [[NSDate systemDate] dateOnly];
    return [self compare:_today] > 0;
}

- (id)dateByAddingDays:(NSInteger)days{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    components.timeZone = calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [components setDay:days];
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:self options:0];
    [components release];
    return newDate;
}

-(id)dateOnly{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDateComponents* components = [calendar components:flags fromDate:self];
    components.timeZone = calendar.timeZone;
    return [calendar dateFromComponents:components];
}
@end
