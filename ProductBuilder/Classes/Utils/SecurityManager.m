//
//  SecurityManager.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SecurityManager.h"
#import <CommonCrypto/CommonDigest.h>
#import "USAdditions.h"
#import "Location.h"

NSString* const SecurityManagerCurrentEmployeeDidChange = @"SecurityManagerCurrentEmployeeDidChange";

static Employee *_currentEmployee = nil;

@implementation SecurityManager

+(Employee*)authorizeUser:(NSString*)loginName password:(NSString*)password {
    
    if (!loginName || !loginName.length) return nil;
    NSString* passwordHash = [self getPaswordHash:password];
    
    Employee* employee = [Employee getEmployeesByName:loginName
                                          andPassword:passwordHash
                                                isAll:NO];
    return employee;
}


+(Employee*)findEmployeeByLoginName:(NSString*)loginName {

    return [self findEmployeeByLoginName:loginName isAll:NO];
}


+(Employee*)findEmployeeByLoginName:(NSString*)loginName isAll:(BOOL)isAll {
    
    if (!loginName || !loginName.length) return nil;
    
    Employee* employee = [Employee getEmployeesByName:loginName andPassword:nil isAll:isAll];
    return employee;
}


NSString* const kPasswordKey = @"{29B4EAB1-4446-46e6-8C6D-4CE994D2581E}";

+(NSString*)getPaswordHash:(NSString*)password {
    NSString* key = kPasswordKey;
    if (password.length){
        key = [key stringByAppendingString:password];
    }
    const char *str = [key UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), result);
    
    NSData *resData = [NSData dataWithBytes:result length:CC_MD5_DIGEST_LENGTH];
    
    return [resData base64Encoding];
}

+(Employee*)currentEmployee{
    return _currentEmployee;
}

+(void)setCurrentEmployee:(Employee*)employee {
    if ((!employee && !_currentEmployee) || [employee isEqual:_currentEmployee]) return;
    [employee retain];
    [_currentEmployee release];
    _currentEmployee = employee;
    [[NSNotificationCenter defaultCenter] postNotificationName:SecurityManagerCurrentEmployeeDidChange object:self userInfo:nil];
}

+(BOOL)isUserInRole:(BPUUID*)employeeId role:(NSString*)role {
    return FALSE;
}

@end


// customers
NSString* const UserRoleCustomersAdd = @"CUST-ADD"; // Customers - Add
NSString* const UserRoleCustomersEdit = @"CUST-EDIT"; // Customers - Edit
NSString* const UserRoleCustomersAdjustStoreCredit= @"CUST-ADJ STORECREDIT"; // Customers - Adjust Store Credit
NSString* const UserRoleCustomerDeactivate = @"CUDEACTIVATE"; // Customer - Deactivate
NSString* const UserRoleCustomersAdjustGiftCard = @"SALES-ADJGCBAL"; // Customers - Adjust Gift Card
NSString* const UserRoleCustomersLoyaltyRewardsAdj = @"CUST-ADJ LOYALTYREWARDS"; // Customers - Allow Loyalty Rewards adjustments
NSString* const UserRoleCustomersEditSecureCustomField = @"CUST-EDITSCRCSTMRFLD"; // Customers - Allow Edit Secure Custom Field
// drawer memos
NSString* const UserRoleDrawerMemosEditMedia= @"DRAWMEMOS-EDITMEDIA"; // Drawer Memos - Edit media
NSString* const UserRoleDrawerMemosEditOpenAmt = @"DRAWMEMOS-EDITOPNAMT"; // Drawer Memos - Edit Open Amt
NSString* const UserRoleDrawerMemosFinalize = @"DRAWMEMOS-FINALIZE"; // Drawer Memos - Finalize Drawer
NSString* const UserRoleDrawerMemosOpenNew= @"DRAWMEMOS-OPENNEW"; // Drawer Memos - Open new
NSString* const UserRoleDrawerMemosAddPaidInOut = @"DRAWMEMOS-PAIDINOUTA"; // Drawer Memos - Add Paid in/out
NSString* const UserRoleDrawerMemosAddVoidInOut = @"DRAWMEMOS-PAIDINOUTV"; // Drawer Memos-Void Paid in/out
NSString* const UserRoleDrawerMemosTakeOffline = @"DRAWMEMOS-TAKEOFFLIN"; // Drawer Memos - Take Dr Offline
NSString* const UserRoleDrawerMemosManageOtherWs = @"DRAWMEMOS-MNGOTHERWS"; // Drawer Memos - Allow to manage other workstations drawer memos
// items
NSString* const UserRoleItemsAddEdit = @"INVITEM-ADD/EDIT"; // Inventory Items - Add/Edit
NSString* const UserRoleItemsDelete = @"INVITEM-DELETE"; // Inventory Items - Delete
NSString* const UserRoleViewCosts = @"VIEW COSTS"; // View Costs
NSString* const UserRoleItemsPrintTags = @"INVITEM-PRINT TAGS"; // Inventory Items - Print tags
NSString* const UserRoleItemsManualWeight = @"SALESRCPTS-MANWEIGHT"; //Inventory Items - Allow manual weight entry
// physical inventory
NSString* const UserRolePIAdd = @"PI-ADD"; // PI - Add
NSString* const UserRolePIDeleteScanCount = @"PI-DEL SCAN COUNT"; // PI - Delete scan count
NSString* const UserRolePIEditScanCount = @"PI-EDIT SCAN COUNT"; // PI - Edit scan count
NSString* const UserRolePIPrepareForHQ = @"PI-PREPARE FOR HQ"; // PI - Prepare for HQ
// purchase orders
NSString* const UserRolePOAddEdit = @"PO-ADDEDIT"; // Purchase Orders - Add/Edit
// prices
NSString* const UserRoleItemPricesAddEdit = @"INVITEMPRICE-ADDEDIT"; // Inven Item Prices - Add/Edit
NSString* const UserRoleChangePriceLevel = @"SR/CUSTMR-CHNGPRCLVL"; // SR/Custmr - Change price level
NSString* const UserRoleChangeOfferPrice = @"SR-CHNGOFFRP";//SR - Change offer price
// purchase receipts
NSString* const UserRolePurchReceiptsAdd = @"PURCHRCPT-ADD"; // Purchase Receipts - Add
NSString* const UserRolePurchReceiptsCancelDate = @"PURCHRCPT-CANCDATE"; // Prch Rcpts-Past PO canc date
NSString* const UserRolePurchReceiptsChangeAssociate = @"PURCHRCPT-CHNGASSC"; // Prch Rcpts - Change associate
NSString* const UserRolePurchReceiptsReprint = @"PURCHRCPT-REPRINT"; // Purchase Receipts - Reprint
NSString* const UserRolePurchReceiptsVoid = @"PURCHRCPT-VOID/REV";// Purchase Rcpts - Void/Reverse
// sales receipts
NSString* const UserRoleSalesReceiptsAdd = @"SALESRCPTS-ADD"; // Sales Rcpts - Add
NSString* const UserRoleSalesReceiptsOpenCashDrawer = @"SALESRCPTS-CASH DRW"; // Sales Rcpts - Open cash drawer
NSString* const UserRoleSalesReceiptsOpenCashDrawerWithSale = @"SALESRCPTS-CASHDRWSL"; // Open the cash drawer when a sale is made that requires the drawer to open
NSString* const UserRoleSalesReceiptsAddAnotherLocation = @"SALESRCPTS-ADDALOC"; //Allow the user to create a sales receipt for another location
NSString* const UserRoleSalesReceiptsOverrideCashDrawerCloseRestriction = @"SLSRCPT-CASHDRWOVRCR"; // Override the restriction of not printing until the cash drawer is closed
NSString* const UserRoleSalesReceiptsReprint = @"SALESRCPTS-REPR";
NSString* const UserRoleSalesReceiptsChangeAssociate = @"SALESRCPTS-CHNGASSC"; // Sales Rcpts - Change associate
NSString* const UserRoleSalesReceiptsChangeCashier = @"SALESRCPTS-CHNGCSHR"; // Sales Rcpts - Change cashier
NSString* const UserRoleSalesReceiptsHold = @"SALESRCPTS-HOLD"; // Sales Rcpts - Hold
NSString* const UserRoleSalesReceiptsVoid = @"SALESRCPTS-VOID/REV"; // Sales Rcpts - Void/Reverse
NSString* const UserRoleSalesReceiptsUpdTradeInPrice = @"SALESRCPTS-UPDINVTRP"; // Sales Receipts - Update Trade-in Price
NSString* const UserRoleSalesReceiptsOriginalReceiptNotRequiredForReturn  = @"SALESRCPTS-ORGRRFR"; // Sales Receipts - Original Receipts Not Required for Return
NSString* const UserRoleAllowReturnWithoutCustomer = @"SR-RETURN-WTHT-CUST"; //Allow return without customer
NSString* const UserRoleSalesReceiptsApplyMemberPricing = @"SALESRCPTS-MEMPRICE"; //Sales Receipts - Manually apply Member Pricing
NSString* const UserRoleSalesReceiptsAllowCardCredit = @"CCARDCREDIT-ALLOW"; // Allow credit card 'Credit' transaction
NSString* const UserRoleSalesReceiptsAllowMOTO = @"CCARDMOTO-ALLOW"; // Allow credit card MOTO transaction processing
NSString* const UserRoleSalesReceiptsAllowVoiceAuth = @"CCARDVOICEAUTH-ALLOW"; // Allow voice Authorization for CC payments
NSString* const UserRoleSalesReceiptsAllowKeyedAuth = @"CCARDKEYEDAUTH-ALLOW"; // Allow Keyed processing for CC payments
NSString* const UserRoleSalesReceiptCardOnFileSaveAllowed = @"CCONFILE-SAVE-ALLOW"; // Allows to save the card on file by processed payment
NSString* const UserRoleSalesReceiptCardOnFileUseAllowed = @"CCONFILE-USE-ALLOW"; // Allows to use saved cards on files for payment processing
NSString* const UserRoleSalesReceipOverrideCustomerCreditLimit = @"OVERRIDE-LIMIT-ALLOW"; // Allows override customer credit limit

//NSString* const UserRoleSalesReceiptsChangeTax = @"SALESRCPTS-CHNGTAX"; // Sales Receipts - Change Tax on Receipt
NSString* const UserRoleSalesChangeTaxArea = @"SALE-CHTAXAREASI"; //Allows user to change Tax Area for Sale Items on the Sales Receipt and Sales Order
NSString* const UserRoleSalesChangeTaxPercent = @"SALE-CHTAXPERCSI";// Allows user to edit Tax percentage for Tax Jurisdiction for Sale Items on the Sales Receipt and Sales Order
NSString* const UserRoleSalesChangeTaxExempt = @"SALE-CHTAXEXMPTSI"; //Allows user to change Tax Exempt flag for Sale Items on the Sales Receipt and Sales Order

NSString* const UserRoleSalesChangeTaxAreaForReturns = @"SALE-CHTAXAREARI";// Allows user to change Tax Area for Return Items on the Sales Receipt
NSString* const UserRoleSalesChangeTaxPercentForReturns = @"SALE-CHTAXPERCRI";// Allows user to edit Tax percentage for Tax Jurisdiction for Return Items on the Sales Receipt
NSString* const UserRoleSalesChangeTaxExemptForReturns = @"SALE-CHTAXEXMPTRI";// Allows user to change Tax Exempt flag for Return Items on the Sales Receipt
NSString* const UserRoleSalesAllowChangeDefaultDiscount = @"SALES-CHANGEDISC";// Allow changing default discount percent/amount in sales receipt and sales order
NSString* const UserRoleSalesReceiptsDiscardAllHeld = @"SLSRCPT-DSCRDALLHELD"; // Sales Receipts - Discard All held receipts
NSString* const UserRoleSalesReceiptsChangeExchangeRate = @"SLSRCPT-CHEXRATE"; // Sales Receipts - Allows user to change exchange rate for Foreign Currency payment
// sales orders
NSString* const UserRoleSalesOrdersAddEdit = @"SO-ADDEDIT"; // Sales Orders - Add/Edit
NSString* const UserRoleOverrideSalesOrderDeposit = @"SODEPOSIT-OVERRIDE"; // Sales Orders - Override SO Item Deposit
NSString* const UserRoleAccessSalesOrders = @"SO-ACCESS"; // Sales Orders - User may access, create and edit sales orders
NSString* const UserRoleSalesOrderChangeAssociate = @"SO-CHANGEASC"; // Sales Orders - User can change associate
NSString* const UserRoleSalesOrderChangeCashier = @"SO-CHANGECSH"; // Sales Orders - User can change cashier
NSString* const UserRoleSalesOrderChangeSellFromLocation = @"SO-CHANGESELLLOC"; // Sales Orders - User may change Sell/Fulfill From Location on the order
NSString* const UserRoleSalesOrderChangeSaleCreditLocation = @"SO-CHANGESALELOC"; // Sales Orders - User may change Sale Credit Location on the order
NSString* const UserRoleSalesOrderRefundDeposit = @"SO-REFDEP"; // Sales Orders - User may refund deposit on a SalesOrder
NSString* const UserRoleSalesOrderRefundDepositArchived = @"SO-REFARCHDEP"; // Sales Orders - User may refund deposit on a SalesOrder which is in Archive state
NSString* const UserRoleSalesOrderRefundDepositExternalOMS = @"SO-REFEXTRNLDEP"; // Sales Orders - User may refund deposit on a SalesOrder which is in External OMS state
NSString* const UserRoleSalesOrderAllowsIgnoreQtyCheck = @"SO-DNREQITEMAV"; // Sales Orders - Allows to ignore Qty check,
NSString* const UserRoleSalesOrderAllowsEditLinkedToPOItemWithDropShip = @"SO-EDTDROPSHIPPOITEM"; // Sales Orders -  Allows user to cancel or change quantity of sales order item which refers purchase order with Drop Ship option selected.
// repair orders
NSString* const UserRoleRepairOrdersAddEdit = @"REPORDER-ADDEDIT"; // Repair Orders - Add/Edit
// wish lists
NSString* const UserRoleWishListsAddEdit = @"WISHLIST-ADDEDIT"; // Wish Lists - Add/Edit
//delivery
NSString* const UserRoleDeliveryOrdersAddEdit = @"DELIVERY-ADDEDIT"; // Delivery Orders - Add/Edit
//layaway
NSString* const UserRoleLayawaysAddEdit = @"LAYAWAY-ADDEDIT"; // Layaway - Add/Edit
//retnal
NSString* const UserRoleRentalAddEdit = @"RENTAL-ADDEDIT"; // Rental - Add/Edit
// system
NSString* const UserRoleCustomDesignAdmin = @"SYS-DESIGNERADMIN"; 
NSString* const UserRoleSystemAccessCustomer = @"SYS-ACCESSCUST"; // System - Customer Access
NSString* const UserRoleSystemAccessDrawerMemos = @"SYS-ACCESSDRWMEMOS"; // System - Access Drawer Memos
NSString* const UserRoleSystemAccessItems = @"SYS-ACCESSITEMS"; // System - Access Items
NSString* const UserRoleSystemAccessPurchaseOrders = @"SYS-ACCESSPRCHORD"; // System - Access Purch. Orders
NSString* const UserRoleSystemAccessPurchaseReceipts = @"SYS-ACCESSPRCHREC"; // System - Access Purch. Rcpts
NSString* const UserRoleSystemAccessSalesReceipts = @"SYS-ACCESSSALESRPT"; // System - Access Sales Receipts
NSString* const UserRoleSystemAccessTransferMemos = @"SYS-ACCESSTRNSMEMOS"; // System - Access Transfer Memos
NSString* const UserRoleSystemAccessTransferNotices = @"SYS-ACCESSTRNSNTS"; // System - Access Trans. Notices
NSString* const UserRoleSystemAccessInternalMessaging = @"SYS-ACCINTRLMES"; // System - Access Internal Messaging
NSString* const UserRoleSystemAccessMenuDesigner = @"SYS-ACCMENUDES"; // System - Access Menu Designer
NSString* const UserRoleSystemAccessPPN = @"SYS-ACCPPN"; // System - Access Price Plan Notices
NSString* const UserRoleSystemAccessRepairOrders = @"SYS-ACCREPORDER"; // System - Access Repair Orders
NSString* const UserRoleSystemAccessVendors = @"SYS-ACCVENDOR"; // System - Access Vendors
NSString* const UserRoleSystemAccessWishList = @"SYS-ACCWISHLIST"; // System - Access Wish Lists
NSString* const UserRoleSystemAccessLayawayOrder = @"SYS-ACCLAYAWAY"; // System - Access Layaway Orders
NSString* const UserRoleSystemAccessRental = @"SYS-ACCRENTAL"; // System - Access Rental Orders
NSString* const UserRoleSystemAccessSalesOrder = @"SYS-ACCESSSO"; // System - Sales Orders
NSString* const UserRoleSystemAccessDeliveryOrders = @"SYS-ACCESSDO"; // System - Delivery Orders
NSString* const UserRoleSystemAccessQtyAdjustment = @"SYS-ACCQTYADJ"; // System - Qty Adjustments
NSString* const UserRoleSystemAccessInventoryCatalogMemos = @"SYS-ACCINVCATMEM"; // System - Inventory Catalog Memos
NSString* const UserRoleSystemAccessAdministrationMenu = @"SYS-ACCADMINMENU"; // System - Administration Menu
// transfer memos
NSString* const UserRoleTransferMemosChangeAssociate = @"TRSMEMOS-CHNGASSC"; // Transfer Memos - Change assoc.
NSString* const UserRoleTransferMemosHold = @"TRSMEMOS-HOLD"; // Transfer Memos - Hold
NSString* const UserRoleTransferMemosVoid = @"TRSMEMOS-VOID/REV"; // Transfer Memos - Void/Reverse
// vendors
NSString* const UserRoleVendorAddEdit = @"VENDOR-ADD"; // Vendor - Add/Edit
// Call center
NSString* const UserRoleCallCenterPractice = @"CALLCENTER-PRACTICE";
// inventory catalog memos
NSString* const UserRoleInventoryCatalogMemoChangeAssociate = @"INVCATMEM-CHNGASSC"; // Inventory Catalog Memos - Change Associate
NSString* const UserRoleInventoryCatalogMemoAddEditCorporative = @"INVITEM-ADDEDIT-CORP";
//stock count
NSString* const UserRoleStockCountAdd = @"STOCKCOUNT-ADD"; //Stock Count -New Stock Count
NSString* const UserRoleStockCountEdit = @"STOCKCOUNT-EDIT"; //Stock Count - Edit Stock Count
NSString* const UserRoleStockCountArchive = @"STOCKCOUNT-ARCHIVE"; //Stock Count - Archive Stock Count
NSString* const UserRoleStockCountFinalize = @"STOCKCOUNT-FINALIZE"; //Stock Count - Finalize Stock Count
NSString* const UserRoleStockCountViewOHQ = @"STOCKCOUNT-VIEWOHQ"; //Stock Count - View On Hand Quantities
//stored value service
NSString* const UserRoleGiftCardsOffline = @"GIFT-OFFLINE"; // Gift Cards - allow working offline
NSString* const UserRoleLinkGiftCardsToCustomer = @"LINKGIFTCARD"; // Gift Cards - allow link gift card to customer
NSString* const UserRoleGiftCardOverrideSellAmount = @"SALES-OVRRDEGCMAXMIN"; // Gift Cards - allow to sell gift card on amount less then minimum or greater tham maximum
NSString* const UserRoleUseGiftCardWithoutPassword = @"SLSRCPT-ALLOWGCNOPWD"; // Gift Cards - allow to select a gift card from the payment dialog without requiring a customer password

NSString* const UserRoleStoreCreditOffline = @"SCREDIT-OFFLINE"; // Store Credit - making offline transactions

//TOKENS
NSString* const UserRoleTokenAdjustment = @"TOKEN-ADJ";
NSString* const UserRoleTokenRedeem = @"TOKEN-REDEEM";
NSString* const UserRoleTokenNegative = @"TOKEN-NEGATIVE";
NSString* const UserRoleTokenOffline = @"TOKEN-OFFLINE";

//Membership
NSString* const UserRoleSalesReceiptMembershipOverride= @"SALESRCPTS-MEMOVER";
NSString* const UserRoleAccessCustomerMembershipLevel = @"CUSTOMER-MEMLEVEL";
NSString* const UserRoleAccessCustomerMembershipExpiresDate = @"CUSTOMER-MEMEXP";
NSString* const UserRoleAccessCustomerMembershipNewCode = @"CUSTOMER-MEMNEWCODE";
NSString* const UserRoleAccessCustomerMembershipChangeCode = @"CUSTOMER-MEMCHGCODE";
NSString* const UserCustomerHistoryReport = @"CUSTOMER-HISTORY";

// Frequent Buyer Program
NSString* const UserRoleFrequentBuyerAdjustment = @"FREQUENTBUYER-ADJ";  // Frequent Buyer Program - balance adjustment

// House Charge Program
NSString* const UserRoleHouseChargeOffline = @"HOUSECHARGE-OFFLINE"; // House Charge - making offline transactions
NSString* const UserRoleHouseChargeChangeLimit = @"CHANGE-HC-LIMIT"; // House Charge settings changing


// Loyalty Reward
NSString* const UserRoleLRPCheckGCBalance = @"LRP-CHECKGCBALANCE"; // LRP - allow to check Gift card balance received as award.

NSString* const ShipMemoAcces = @"SM-ACCESS"; // User may access and process Ship Memos.
NSString* const ShipMemoArchive = @"SM-ARCHIVE"; // User may archive Ship Memos

//Reject Appointments
NSString* const UserRoleRejectAppointments = @"SR-REJECTAPPOINTMENT"; // User may reject appointment
