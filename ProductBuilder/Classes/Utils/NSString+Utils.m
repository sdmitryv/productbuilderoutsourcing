//
//  NSString(Removing).m
//  ProductBuilder
//
//  Created by valera on 10/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NSString+Utils.h"
#import "NSAttributedString+Attributes.h"

@implementation NSString(Utils)


- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet
{
    return [self stringByReplacingCharactersInSet:characterSet withString:nil];
}


- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet
                                    withString:(NSString*)string
{
    NSScanner*       cleanerScanner = [NSScanner scannerWithString:self];
    [cleanerScanner setCharactersToBeSkipped:nil];
    NSMutableString* cleanString    = [NSMutableString stringWithCapacity:[self length]];
    
    while (![cleanerScanner isAtEnd]){
        NSString* stringFragment;
        if ([cleanerScanner scanUpToCharactersFromSet:characterSet intoString:&stringFragment])
            [cleanString appendString:stringFragment];
        
        if ([cleanerScanner scanCharactersFromSet:characterSet intoString:nil])
            if (string)
                [cleanString appendString:string];
    }
    
    return cleanString;
}

- (NSString *)stringByRemovingCharactersInSet:(NSCharacterSet*)characterSet inRange:(NSRange)range
{
    return [self stringByReplacingCharactersInSet:characterSet withString:nil inRange:range];
    
}

- (NSString *)stringByReplacingCharactersInSet:(NSCharacterSet*)characterSet
                                    withString:(NSString*)string inRange:(NSRange)range
{
    if (!range.length || (range.location + range.length) > self.length) return self;
    NSString* beforeRangeStr = nil;
    NSString* afterRangeStr = nil;
    if (range.location)
        beforeRangeStr = [self substringToIndex:range.location];
    if ((range.location + range.length) < self.length) 
        afterRangeStr = [self substringFromIndex:range.location + range.location + range.length];
    NSString* inRangeStr = [[self substringWithRange:range] stringByReplacingCharactersInSet:characterSet withString:string];
    if (!beforeRangeStr && !afterRangeStr) return inRangeStr;
    NSMutableString* result = [[NSMutableString alloc]initWithString:beforeRangeStr ? beforeRangeStr : @""];
    if (inRangeStr)
        [result appendString:inRangeStr];
    if (afterRangeStr)
        [result appendString:afterRangeStr];
    return [result autorelease];
}

- (NSString *)stringByLeavingOnlyDigits {
    if (!self) return nil;
    NSString *returnString = [self stringByRemovingCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
//    NSString *returnString = [self stringByRemovingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
    return [returnString isEqualToString:@""] ? nil : returnString;
}

-(NSMutableAttributedString*)stringByHighlightingString:(NSString*)highlightingString normalFont:(UIFont*)normalFont
                                          highlightFont:(UIFont*)highlightFont color:(UIColor*)color{
    return [self stringByHighlightingString:highlightingString normalFont:normalFont highlightFont:highlightFont color:color backgroundColor:nil];
}

//highlight string from beginning or from till the end
-(NSMutableAttributedString*)stringByHighlightingString:(NSString*)highlightingString normalFont:(UIFont*)normalFont
                                         highlightFont:(UIFont*)highlightFont color:(UIColor*)color backgroundColor:(UIColor*)backgroundColor{
	
	NSMutableAttributedString *str = nil;
    NSArray* highlightingWords = [[[highlightingString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]] valueForKeyPath:@"@distinctUnionOfObjects.self"];
    for (NSString* highlitingWord in highlightingWords){
        if (highlitingWord && [self length]>=[highlitingWord length]){
            NSRange textRange = NSMakeRange(0, [self length]);
            while(TRUE){
                textRange =[self rangeOfString:highlitingWord options:NSCaseInsensitiveSearch range:textRange];
                if (textRange.location==NSNotFound) break;
                if (!str){
                    str = [[NSMutableAttributedString alloc]initWithString:self];
                    [str setFont:normalFont];
                }
                [str setTextColor:color range:textRange];
                if (backgroundColor){
                    [str addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:textRange];
                }
                [str setFont:highlightFont range:textRange];
                textRange.location += textRange.length;
                if (NSMaxRange(textRange) > [self length]) break;
                textRange.length = [self length] - textRange.location;
            }
        }
    }
	return [str autorelease];
}

- (NSString *)whitespaceReplacementWithSystemAttributes:(NSDictionary *)systemAttributes newAttributes:(NSDictionary *)newAttributes
{
    NSString *stringTitle = self;
    NSString* const fillingString = @" ";
    NSMutableString *stringTitleWS = [[NSMutableString alloc] initWithString:@""];
    
    CGFloat diff = 0;
    CGSize  stringTitleSize = [stringTitle sizeWithAttributes:newAttributes];
    CGSize stringTitleWSSize;
    CGFloat roundingWidth = [fillingString sizeWithAttributes:systemAttributes].width/2.0f;
    do {
        [stringTitleWS appendString:fillingString];
        stringTitleWSSize = [stringTitleWS sizeWithAttributes:systemAttributes];
        diff = (stringTitleSize.width - stringTitleWSSize.width);
    }
    while (fabs(diff) >= roundingWidth);
    
    return [stringTitleWS autorelease];
}

@end

@implementation NSMutableString (NewLine)

- (void)appendStringOnNewLine:(NSString *)string
{
    if (string.length > 0) {
        if (self.length > 0) {
            [self appendString:@"\n"];
        }
        [self appendString:string];
    }
}

@end
