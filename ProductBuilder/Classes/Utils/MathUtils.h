//
//  MathUtils.h
//  ProductBuilder
//
//  Created by Valera on 10/20/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MathUtils : NSObject
+(NSDecimal)roundMoney:(NSDecimal) value;
+(NSDecimal)roundPercent:(NSDecimal) value;
@end
