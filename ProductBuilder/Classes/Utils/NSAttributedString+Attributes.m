/***********************************************************************************
 *
 * Copyright (c) 2010 Olivier Halligon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 ***********************************************************************************
 *
 * Created by Olivier Halligon  (AliSoftware) on 20 Jul. 2010.
 *
 * Any comment or suggestion welcome. Please contact me before using this class in
 * your projects. Referencing this project in your AboutBox/Credits is appreciated.
 *
 ***********************************************************************************/


#import "NSAttributedString+Attributes.h"


@implementation NSMutableAttributedString (OHCommodityStyleModifiers)

-(void)setFont:(UIFont*)font {
    [self setFont:font range:NSMakeRange(0,[self length])];
}
-(void)setFont:(UIFont*)font range:(NSRange)range {
    [self addAttribute:NSFontAttributeName value:font range:range];
}
-(void)setFontName:(NSString*)fontName size:(CGFloat)size {
	[self setFontName:fontName size:size range:NSMakeRange(0,[self length])];
}
-(void)setFontName:(NSString*)fontName size:(CGFloat)size range:(NSRange)range {
    UIFont* aFont = [UIFont fontWithName:fontName size:size];
	if (!aFont) return;
    [self addAttribute:NSFontAttributeName value:aFont range:range];
}

-(void)setTextColor:(UIColor*)color {
	[self setTextColor:color range:NSMakeRange(0,[self length])];
}

-(void)setTextColor:(UIColor*)color range:(NSRange)range {
	[self addAttribute:NSForegroundColorAttributeName value:color range:range];
}

-(void)setTextIsUnderlined:(BOOL)underlined {
	[self setTextIsUnderlined:underlined range:NSMakeRange(0,[self length])];
}
-(void)setTextIsUnderlined:(BOOL)underlined range:(NSRange)range {
	[self addAttribute:NSUnderlineStyleAttributeName value:@(underlined) range:range];
}


-(void)setTextBold:(BOOL)isBold range:(NSRange)range {
	NSUInteger startPoint = range.location;
    if (startPoint>=self.length) return;
	NSRange effectiveRange;
	do {
		// Get font at startPoint
		UIFont* currentFont = (UIFont*)[self attribute:NSFontAttributeName atIndex:startPoint effectiveRange:&effectiveRange];
        if (currentFont){
            // The range for which this font is effective
            NSRange fontRange = NSIntersectionRange(range, effectiveRange);
            CTFontRef currentFontRef = CTFontCreateWithName((CFStringRef)currentFont.fontName, currentFont.pointSize, NULL);
            // Create bold/unbold font variant for this font and apply
            CTFontRef newFont = CTFontCreateCopyWithSymbolicTraits(currentFontRef, 0.0, NULL, (isBold?kCTFontBoldTrait:0), kCTFontBoldTrait);
            if (newFont) {
                NSString *fontPostScriptName = (NSString *)CTFontCopyPostScriptName(newFont);
                UIFont *fontFromCTFont = [UIFont fontWithName:fontPostScriptName size:currentFont.pointSize];
                [self setFont:fontFromCTFont range:fontRange];
                [fontPostScriptName release];
                CFRelease(newFont);
            } else {
                NSLog(@"[OHAttributedLabel] Warning: can't find a bold font variant for font %@. Try another font family (like Helvetica) instead.",currentFont.fontName);
            }
            CFRelease(currentFontRef);
        }
   		// If the fontRange was not covering the whole range, continue with next run
		startPoint = NSMaxRange(effectiveRange);
        
	} while(startPoint<NSMaxRange(range));
}

-(void)setTextAlignment:(NSTextAlignment)alignment lineBreakMode:(NSLineBreakMode)lineBreakMode {
	[self setTextAlignment:alignment lineBreakMode:lineBreakMode range:NSMakeRange(0,[self length])];
}
-(void)setTextAlignment:(NSTextAlignment)alignment lineBreakMode:(NSLineBreakMode)lineBreakMode range:(NSRange)range {
   
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc]init];
    [paragraph setAlignment:alignment];
    [paragraph setLineBreakMode:lineBreakMode];
    [self addAttribute:NSParagraphStyleAttributeName value:paragraph range:NSMakeRange(0, self.length)];
    [paragraph release];

}

@end
