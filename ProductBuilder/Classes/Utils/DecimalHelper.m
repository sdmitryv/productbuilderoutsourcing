//
//  DecimalHelper.m
//  ProductBuilder
//
//  Created by Valera on 11/15/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "DecimalHelper.h"

@implementation NSString (DecimalBehaviuor)

-(NSDecimal)decimalValue{
	NSDecimal result;
    NSScanner *theScanner = [[NSScanner alloc] initWithString:self];
	[theScanner scanDecimal:&result];
	[theScanner release];
	return result;
}
@end

@implementation NSCurrencyFormatter

static NSNumberFormatter* defaultCurrencyFormatter;
static NSNumberFormatter* defaultPercentFormatter;
static NSNumberFormatter* defaultQtyFormatter;
static NSNumberFormatter* defaultNumberFormatter;
static NSNumberFormatter* defaultDecimalFormatter;
static NSNumberFormatter* invariantDecimalFormatter;

+(NSNumberFormatter*)defaultCurrencyFormatter{
    if (!defaultCurrencyFormatter){
		defaultCurrencyFormatter = [[NSNumberFormatter alloc]init];
        defaultCurrencyFormatter.locale = [NSLocale currentLocale];
        defaultCurrencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
        defaultCurrencyFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        
        //        defaultCurrencyFormatter.positiveFormat = @"$ #,##0.00";
        //        defaultCurrencyFormatter.negativeFormat = @"- $ #,##0.00";
        defaultCurrencyFormatter.maximumFractionDigits = [NSCurrencyFormatter currencyFractionDigits];
        defaultCurrencyFormatter.minimumFractionDigits = [NSCurrencyFormatter currencyFractionDigits];
        defaultCurrencyFormatter.roundingMode = NSNumberFormatterRoundHalfEven;
	}
    return  defaultCurrencyFormatter;
}


+(int)currencyFractionDigits{
    
	return 2;
}


+(NSNumberFormatter*)defaultPercentFormatter{
    if (!defaultPercentFormatter){
		defaultPercentFormatter = [[NSNumberFormatter alloc]init];
		defaultPercentFormatter.numberStyle = NSNumberFormatterPercentStyle;
		defaultPercentFormatter.generatesDecimalNumbers = YES;
		defaultPercentFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
		defaultPercentFormatter.maximumFractionDigits = 2;
		defaultPercentFormatter.minimumFractionDigits = 2;
		defaultPercentFormatter.multiplier = @1;
		defaultPercentFormatter.locale = [NSLocale currentLocale];
	}
    return defaultPercentFormatter;
}


+(NSNumberFormatter*)defaultNumberFormatter{
    if (!defaultNumberFormatter){
        defaultNumberFormatter = [[NSNumberFormatter alloc]init];
        defaultNumberFormatter.generatesDecimalNumbers = YES;
        defaultNumberFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        defaultNumberFormatter.locale = [NSLocale currentLocale];
        defaultNumberFormatter.roundingMode = NSNumberFormatterRoundHalfEven;
        defaultNumberFormatter.paddingPosition = NSNumberFormatterPadAfterPrefix;
        defaultNumberFormatter.paddingCharacter = @"0";
        defaultNumberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        defaultNumberFormatter.maximumFractionDigits = 3;
        defaultNumberFormatter.minimumFractionDigits = 0;
        defaultNumberFormatter.usesGroupingSeparator = TRUE;
    }
    return defaultNumberFormatter;
}

+(NSNumberFormatter*)defaultQtyFormatter{
    if (!defaultQtyFormatter){
        defaultQtyFormatter = [[NSNumberFormatter alloc]init];
        defaultQtyFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        defaultQtyFormatter.generatesDecimalNumbers = NO;
        defaultQtyFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        defaultQtyFormatter.maximumFractionDigits = 2;
        defaultQtyFormatter.minimumFractionDigits = 0;
        defaultQtyFormatter.locale = [NSLocale currentLocale];
        defaultQtyFormatter.usesGroupingSeparator = TRUE;
    }
    return defaultQtyFormatter;
}

+(NSNumberFormatter*)defaultDecimalFormatter{
    if (!defaultDecimalFormatter){
        defaultDecimalFormatter = [[NSNumberFormatter alloc]init];
        defaultDecimalFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        defaultDecimalFormatter.generatesDecimalNumbers = NO;
        defaultDecimalFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        defaultDecimalFormatter.maximumFractionDigits = NSIntegerMax;
        defaultDecimalFormatter.minimumFractionDigits = 0;
        defaultDecimalFormatter.locale = [NSLocale currentLocale];
    }
    return defaultDecimalFormatter;
}

+(NSNumberFormatter*)invariantDecimalFormatter{
    if (!invariantDecimalFormatter){
        invariantDecimalFormatter = [[NSNumberFormatter alloc]init];
        invariantDecimalFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        invariantDecimalFormatter.generatesDecimalNumbers = NO;
        invariantDecimalFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
        invariantDecimalFormatter.maximumFractionDigits = 2;
        invariantDecimalFormatter.minimumFractionDigits = 2;
        invariantDecimalFormatter.locale = [NSLocale currentLocale];
        invariantDecimalFormatter.usesGroupingSeparator = FALSE;
        invariantDecimalFormatter.decimalSeparator = @".";
    }
    return invariantDecimalFormatter;
}

+(void)setDefaultCurrencyFormatter:(NSNumberFormatter*)aDefaultCurrencyFormatter{
    [defaultCurrencyFormatter release];
    defaultCurrencyFormatter = [aDefaultCurrencyFormatter copy];
}

+(void)setDefaultPercentFormatter:(NSNumberFormatter*)aDefaultPercentFormatter{
    [defaultPercentFormatter release];
    defaultPercentFormatter = [aDefaultPercentFormatter copy];
}

+(void)setDefaultQtyFormatter:(NSNumberFormatter*)aDefaultQtyFormatter{
    [defaultQtyFormatter release];
    defaultQtyFormatter = [aDefaultQtyFormatter copy];
}

+(void)setDefaultNumberFormatter:(NSNumberFormatter*)aDefaultNumberFormatter{
    [defaultNumberFormatter release];
    defaultNumberFormatter = [aDefaultNumberFormatter copy];
}

+(NSString*)formatQty:(NSDecimal)value{
    return [self formatQtyFromNumber:[NSDecimalNumber decimalNumberWithDecimal:value]];
}

+(NSString*)formatQtyFromNumber:(NSNumber*)value{
    return [self.defaultQtyFormatter stringFromNumber:value];
}

+(NSString*)formatQtyFromDouble:(double)value{
    NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", value];
    NSDecimal decimal = CPDecimalFromString(stringDouble);
    [stringDouble release];
    return [self formatQtyFromNumber:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
}


+(NSString*)formatNumber:(NSDecimal)value{
    return [self formatNumberFromNumber:[NSDecimalNumber decimalNumberWithDecimal:value]];
}

+(NSString*)formatNumberFromNumber:(NSNumber*)value{
    return [self.defaultNumberFormatter stringFromNumber:value];
}

+(NSString*)formatNumberFromDouble:(double)value{
    NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", value];
    NSDecimal decimal = CPDecimalFromString(stringDouble);
    [stringDouble release];
    return [self formatNumberFromNumber:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
}

+(NSString*)formatDecimal:(NSDecimal)value{
    return [self formatDecimalFromNumber:[NSDecimalNumber decimalNumberWithDecimal:value]];
}

+(NSString*)formatDecimalFromNumber:(NSNumber*)value{
    return [self.defaultDecimalFormatter stringFromNumber:value];
}

+(NSString*)formatDecimalFromDouble:(double)value{
    NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", value];
    NSDecimal decimal = CPDecimalFromString(stringDouble);
    [stringDouble release];
    return [self formatDecimalFromNumber:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
}

+(NSString*)formatDecimalFromObject:(id)value{
    if ([value isKindOfClass:[NSNumber class]]){
        return [[self class]formatDecimalFromNumber:value];
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [[self class] formatDecimal:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSValue class]]){
        if (!strcmp(@encode(NSDecimal), [value objCType])){
            NSDecimal d;
            [value getValue:&d];
            return [[self class] formatDecimal:d];
        }
        else if (!strcmp(@encode(NSInteger), [value objCType])){
            NSInteger i;
            [value getValue:&i];
            return [[self class] formatDecimalFromNumber:@(i)];
        }
        else if (!strcmp(@encode(float), [value objCType])){
            float f;
            [value getValue:&f];
            return [[self class] formatDecimalFromDouble:f];
        }
        else if (!strcmp(@encode(double), [value objCType])){
            double d;
            [value getValue:&d];
            return [[self class] formatDecimalFromDouble:d];
        }
    }
    return nil;
}

+(NSString*)formatQtyFromObject:(id)value{
    if ([value isKindOfClass:[NSNumber class]]){
        return [[self class]formatQtyFromNumber:value];
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [[self class] formatQty:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSValue class]]){
        if (!strcmp(@encode(NSDecimal), [value objCType])){
            NSDecimal d;
            [value getValue:&d];
            return [[self class] formatQty:d];
        }
        else if (!strcmp(@encode(NSInteger), [value objCType])){
            NSInteger i;
            [value getValue:&i];
            return [[self class] formatQtyFromNumber:@(i)];
        }
        else if (!strcmp(@encode(float), [value objCType])){
            float f;
            [value getValue:&f];
            return [[self class] formatQtyFromDouble:f];
        }
        else if (!strcmp(@encode(double), [value objCType])){
            double d;
            [value getValue:&d];
            return [[self class] formatQtyFromDouble:d];
        }
    }
    return nil;
}


+(NSString*)formatNumberFromObject:(id)value{
    if ([value isKindOfClass:[NSNumber class]]){
        return [[self class]formatNumberFromNumber:value];
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [[self class] formatNumber:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSValue class]]){
        if (!strcmp(@encode(NSDecimal), [value objCType])){
            NSDecimal d;
            [value getValue:&d];
            return [[self class] formatNumber:d];
        }
        else if (!strcmp(@encode(NSInteger), [value objCType])){
            NSInteger i;
            [value getValue:&i];
            return [[self class] formatNumberFromNumber:@(i)];
        }
        else if (!strcmp(@encode(float), [value objCType])){
            float f;
            [value getValue:&f];
            return [[self class] formatNumberFromDouble:f];
        }
        else if (!strcmp(@encode(double), [value objCType])){
            double d;
            [value getValue:&d];
            return [[self class] formatNumberFromDouble:d];
        }
    }
    return nil;
}

+(NSString*)formatCurrencyFromNumber:(NSNumber*)value{
	return [self defaultCurrencyFromNumber:value];
}

+(NSString*)defaultCurrencyFromNumber:(NSNumber*)value{
    if (!value || [value isEqual:[NSDecimalNumber notANumber]])
        value = [NSDecimalNumber zero];
	return [self.defaultCurrencyFormatter stringFromNumber:value];
}

+(NSString*)formatCurrency:(NSDecimal)value{
	return [self formatCurrencyFromNumber:[NSDecimalNumber decimalNumberWithDecimal:value]];
}

+(NSString*)formatCurrencyFromDouble:(double)value{
    NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", value];
    NSDecimal decimal = CPDecimalFromString(stringDouble);
    [stringDouble release];
    return [self formatCurrencyFromNumber:[NSDecimalNumber decimalNumberWithDecimal:decimal]];

}

+(NSString*)formatCurrencyFromObject:(id)value{
    if ([value isKindOfClass:[NSNumber class]]){
        return [[self class]formatCurrencyFromNumber:value];
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [[self class] formatCurrency:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSValue class]]){
        if (!strcmp(@encode(NSDecimal), [value objCType])){
            NSDecimal d;
            [value getValue:&d];
            return [[self class] formatCurrency:d];
        }
        else if (!strcmp(@encode(NSInteger), [value objCType])){
            NSInteger i;
            [value getValue:&i];
            return [[self class] formatCurrencyFromNumber:@(i)];
        }
        else if (!strcmp(@encode(float), [value objCType])){
            float f;
            [value getValue:&f];
            return [[self class] formatCurrencyFromDouble:f];
        }
        else if (!strcmp(@encode(double), [value objCType])){
            double d;
            [value getValue:&d];
            return [[self class] formatCurrencyFromDouble:d];
        }
    }
    return nil;
}

+(NSString*)formatPercent:(NSDecimal)value{
	return [self formatPercentFromNumber:[NSDecimalNumber decimalNumberWithDecimal:value]];
}

+(NSString*)formatPercentFromNumber:(NSNumber*)value{
	return [self.defaultPercentFormatter stringFromNumber:value];
}

+(NSString*)formatPercentFromDouble:(double)value{
    NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", value];
    NSDecimal decimal = CPDecimalFromString(stringDouble);
    [stringDouble release];
    return [self formatPercentFromNumber:[NSDecimalNumber decimalNumberWithDecimal:decimal]];
}

+(NSString*)formatPercentFromObject:(id)value{
    if ([value isKindOfClass:[NSNumber class]]){
        return [[self class]formatPercentFromNumber:value];
    }
    else if ([value isKindOfClass:[NSString class]]){
        return [[self class] formatPercent:[value decimalValue]];
    }
    else if ([value isKindOfClass:[NSValue class]]){
        if (!strcmp(@encode(NSDecimal), [value objCType])){
            NSDecimal d;
            [value getValue:&d];
            return [[self class] formatPercent:d];
        }
        else if (!strcmp(@encode(NSInteger), [value objCType])){
            NSInteger i;
            [value getValue:&i];
            return [[self class] formatPercentFromNumber:@(i)];
        }
        else if (!strcmp(@encode(float), [value objCType])){
            float f;
            [value getValue:&f];
            return [[self class] formatPercentFromDouble:f];
        }
        else if (!strcmp(@encode(double), [value objCType])){
            double d;
            [value getValue:&d];
            return [[self class] formatPercentFromDouble:d];
        }
    }
    return nil;
}

@end


#pragma mark -
#pragma mark Convert primitive types to NSDecimal

/**
 *      @brief Converts an 8-bit integer value to an NSDecimal.
 *      @param i The integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromChar(int8_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts a 16-bit integer value to an NSDecimal.
 *      @param i The integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromShort(int16_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts a 32-bit integer value to an NSDecimal.
 *      @param i The integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromLong(int32_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts a 64-bit integer value to an NSDecimal.
 *      @param i The integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromLongLong(int64_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an int value to an NSDecimal.
 *      @param i The int value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromInt(int i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an NSInteger value to an NSDecimal.
 *      @param i The NSInteger value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromInteger(NSInteger i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an unsigned 8-bit integer value to an NSDecimal.
 *      @param i The unsigned integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedChar(uint8_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an unsigned 16-bit integer value to an NSDecimal.
 *      @param i The unsigned integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedShort(uint16_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an unsigned 32-bit integer value to an NSDecimal.
 *      @param i The unsigned integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedLong(uint32_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an unsigned 64-bit integer value to an NSDecimal.
 *      @param i The unsigned integer value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedLongLong(uint64_t i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an unsigned int value to an NSDecimal.
 *      @param i The unsigned int value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedInt(unsigned int i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts an NSUInteger value to an NSDecimal.
 *      @param i The NSUInteger value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromUnsignedInteger(NSUInteger i)
{
	return [@(i) decimalValue];
}

/**
 *      @brief Converts a float value to an NSDecimal.
 *      @param f The float value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromFloat(float f)
{
	return [@(f) decimalValue];
}

/**
 *      @brief Converts a double value to an NSDecimal.
 *      @param d The double value.
 *      @return The converted value.
 **/
NSDecimal CPDecimalFromDouble(double d)
{
	return [@(d) decimalValue];
}

/**
 *      @brief Parses a string and extracts the numeric value as an NSDecimal.
 *      @param stringRepresentation The string value.
 *      @return The numeric value extracted from the string.
 **/
NSDecimal CPDecimalFromString(NSString *stringRepresentation)
{
	// The following NSDecimalNumber-based creation of NSDecimals from strings is slower than
	// the NSScanner-based method: (307000 operations per second vs. 582000 operations per second for NSScanner)
	
	if (stringRepresentation == nil) {
        return D0;
    }
    
	NSDecimal result;
	NSScanner *theScanner = [[NSScanner alloc] initWithString:stringRepresentation];
	[theScanner scanDecimal:&result];
	[theScanner release];
	
	return result;
}

#pragma mark -
#pragma mark NSDecimal arithmetic

/**
 *      @brief Adds two NSDecimals together.
 *      @param leftOperand The left-hand side of the addition operation.
 *      @param rightOperand The right-hand side of the addition operation.
 *      @return The result of the addition.
 **/
NSDecimal CPDecimalAdd(NSDecimal leftOperand, NSDecimal rightOperand)
{
	NSDecimal result;
	NSDecimalAdd(&result, &leftOperand, &rightOperand, NSRoundBankers);
	return result;
}

/**
 *      @brief Subtracts one NSDecimal from another.
 *      @param leftOperand The left-hand side of the subtraction operation.
 *      @param rightOperand The right-hand side of the subtraction operation.
 *      @return The result of the subtraction.
 **/
NSDecimal CPDecimalSubtract(NSDecimal leftOperand, NSDecimal rightOperand)
{
	NSDecimal result;
	NSDecimalSubtract(&result, &leftOperand, &rightOperand, NSRoundBankers);
	return result;
}

/**
 *      @brief Multiplies two NSDecimals together.
 *      @param leftOperand The left-hand side of the multiplication operation.
 *      @param rightOperand The right-hand side of the multiplication operation.
 *      @return The result of the multiplication.
 **/
NSDecimal CPDecimalMultiply(NSDecimal leftOperand, NSDecimal rightOperand)
{
	NSDecimal result;
	NSDecimalMultiply(&result, &leftOperand, &rightOperand, NSRoundBankers);
    if (NSDecimalIsNotANumber(&result))
        result = D0;
	return result;
}

/**
 *      @brief Divides one NSDecimal by another.
 *      @param numerator The numerator of the multiplication operation.
 *      @param denominator The denominator of the multiplication operation.
 *      @return The result of the division.
 **/
NSDecimal CPDecimalDivide(NSDecimal numerator, NSDecimal denominator)
{
	if (CPDecimalEquals0(denominator)) return D0;
    NSDecimal result;
	NSDecimalDivide(&result, &numerator, &denominator, NSRoundBankers);
    if (NSDecimalIsNotANumber(&result))
        result = D0;
	return result;
}

NSDecimal CPDecimalModulo(NSDecimal numerator, NSDecimal denominator){
    NSDecimal result;
    NSDecimalDivide(&result, &numerator, &denominator, NSRoundDown);
    NSDecimalRound(&result, &result, 0, result._isNegative ? NSRoundUp : NSRoundDown);
    NSDecimalMultiply(&result, &result, &denominator, NSRoundBankers);
    NSDecimalSubtract(&result, &numerator, &result, NSRoundBankers);
    if (NSDecimalIsNotANumber(&result))
        result = D0;
    else if (denominator._isNegative) {
        result._isNegative = !result._isNegative;
    }
    return result;
}

#pragma mark -
#pragma mark NSDecimal comparison

/**
 *      @brief Checks to see if one NSDecimal is greater than another.
 *      @param leftOperand The left side of the comparison.
 *      @param rightOperand The right side of the comparison.
 *      @return YES if the left operand is greater than the right, NO otherwise.
 **/
BOOL CPDecimalGreaterThan(NSDecimal leftOperand, NSDecimal rightOperand)
{
	return (NSDecimalCompare(&leftOperand, &rightOperand) == NSOrderedDescending);
}

/**
 *      @brief Checks to see if one NSDecimal is greater than or equal to another.
 *      @param leftOperand The left side of the comparison.
 *      @param rightOperand The right side of the comparison.
 *      @return YES if the left operand is greater than or equal to the right, NO otherwise.
 **/
BOOL CPDecimalGreaterThanOrEqualTo(NSDecimal leftOperand, NSDecimal rightOperand)
{
	return (NSDecimalCompare(&leftOperand, &rightOperand) != NSOrderedAscending);
}

/**
 *      @brief Checks to see if one NSDecimal is less than another.
 *      @param leftOperand The left side of the comparison.
 *      @param rightOperand The right side of the comparison.
 *      @return YES if the left operand is less than the right, NO otherwise.
 **/
BOOL CPDecimalLessThan(NSDecimal leftOperand, NSDecimal rightOperand)
{
	return (NSDecimalCompare(&leftOperand, &rightOperand) == NSOrderedAscending);
}

/**
 *      @brief Checks to see if one NSDecimal is less than or equal to another.
 *      @param leftOperand The left side of the comparison.
 *      @param rightOperand The right side of the comparison.
 *      @return YES if the left operand is less than or equal to the right, NO otherwise.
 **/
BOOL CPDecimalLessThanOrEqualTo(NSDecimal leftOperand, NSDecimal rightOperand)
{
	return (NSDecimalCompare(&leftOperand, &rightOperand) != NSOrderedDescending);
}

/**
 *      @brief Checks to see if one NSDecimal is equal to another.
 *      @param leftOperand The left side of the comparison.
 *      @param rightOperand The right side of the comparison.
 *      @return YES if the left operand is equal to the right, NO otherwise.
 **/
BOOL CPDecimalEquals(NSDecimal leftOperand, NSDecimal rightOperand)
{
	return (NSDecimalCompare(&leftOperand, &rightOperand) == NSOrderedSame);
}



BOOL CPDecimalGreaterThan0(NSDecimal d){
	return CPDecimalGreaterThan(d, D0);
}
BOOL CPDecimalGreaterThanOrEqualTo0(NSDecimal d){
	return CPDecimalGreaterThanOrEqualTo(d, D0);
}
BOOL CPDecimalLessThan0(NSDecimal d){
	return CPDecimalLessThan(d, D0);
}
BOOL CPDecimalLessThanOrEqualTo0(NSDecimal d){
	return CPDecimalLessThanOrEqualTo(d, D0);
}
BOOL CPDecimalEquals0(NSDecimal d){
    return CPDecimalEquals(d, D0);
}

#pragma mark -
#pragma mark NSDecimal utilities

/**
 *      @brief Creates and returns an NSDecimal struct that represents the value "not a number".
 *
 *      Calling <code>NSDecimalIsNotANumber()</code> on this value will return <code>YES</code>.
 *
 *      @return An NSDecimal struct that represents the value "not a number".
 **/
NSDecimal CPDecimalNaN(void)
{
	NSDecimal decimalNaN = [[NSDecimalNumber notANumber] decimalValue];
	decimalNaN._length = 0;
	decimalNaN._isNegative = YES;
	
	return decimalNaN;
}

NSDecimal CPDecimalNegate(NSDecimal d){
    //return CPDecimalMultiply(d, [[NSDecimalNumber decimalNumberWithMantissa: 1
    //                                                              exponent: 0
    //                                                            isNegative: YES] decimalValue]);
    if (NSDecimalCompare(&d, &D0) == NSOrderedSame)
        return d;
    d._isNegative = !d._isNegative;
    return d;
}

/**
 *      @brief Converts an NSDecimal value to an NSString.
 *      @param decimalNumber The NSDecimal value.
 *      @return The converted value.
 **/
NSString *CPDecimalStringValue(NSDecimal decimalNumber)
{
	return NSDecimalString(&decimalNumber, [NSLocale systemLocale]);
}

NSDecimal CPDecimalABS(NSDecimal d){
    //return CPDecimalLessThan(d, CPDecimalFromInt(0)) ? CPDecimalMultiply(CPDecimalFromInt(-1),d) : d;
    d._isNegative = FALSE;
    return d;
}

double  CPDecimalDoubleValue(NSDecimal decimalNumber) {
	return [[NSDecimalNumber decimalNumberWithDecimal:decimalNumber] doubleValue];
}


