//
//  NumkeyboardController.m
//  StockCount
//
//  Created by Valera on 12/1/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "NumpadKeyboardController.h"
#import "UITextField+Input.h"
#import "UIView+FirstResponder.h"
#import "UITextFieldNumeric.h"

@interface UITextField (SelectedRange)

@property (nonatomic, readonly) NSRange selectedRange;

@end

@implementation UITextField (SelectedRange)

-(NSRange)selectedRange{
    UITextRange *selectedTextRange = self.selectedTextRange;
    NSUInteger location = [self offsetFromPosition:self.beginningOfDocument
                                        toPosition:selectedTextRange.start];
    NSUInteger length = [self offsetFromPosition:selectedTextRange.start
                                      toPosition:selectedTextRange.end];
    return NSMakeRange(location, length);
}

@end

@implementation NumpadKeyboardController

@synthesize delegate;
@synthesize contents = _contents;

-(id)init {
    self = [super initWithNibName:@"NumpadKeyboard" bundle:nil];
    if (self) {
        _contents = NumpadKeyboardContentsDot | NumpadKeyboardContentsKeyboard | NumpadKeyboardContentsMinus;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _contents = NumpadKeyboardContentsDot | NumpadKeyboardContentsKeyboard | NumpadKeyboardContentsMinus;
    }
    return self;
}

-(void)dealloc{
    [btnBs release];
	[btnKb release];
	[btnMinus release];
	[btnTab release];
	[btnEnter release];
	[btnSmallEnter release];
	[btnBigEnter release];
	[btnWide0 release];
    [btn1 release];
    [btn2 release];
    [btn3  release];
    [btn4 release];
    [btn5 release];
    [btn6 release];
    [btn7 release];
    [btn8 release];
    [btn9 release];
    [btn0 release];
    [btnDot release];
    [contentView release];
    [super dealloc];
}

-(CGRect)makeFrameforKeyInRow:(NSInteger)row column:(NSInteger)column width:(NSInteger)width height:(NSInteger)height {
    CGRect keyFrame = btn1.frame;
    CGFloat keyWidth = keyFrame.size.width;
    CGFloat keyHeight = keyFrame.size.height;
    CGFloat horizontalMargin = btn2.frame.origin.x - keyFrame.origin.x - keyWidth;
    CGFloat verticalMargin = btn4.frame.origin.y - keyFrame.origin.y - keyHeight;
    return CGRectMake(keyFrame.origin.x + (keyWidth + horizontalMargin) * column, keyFrame.origin.y + (keyHeight + verticalMargin) * row, keyWidth * width + horizontalMargin * (width - 1), keyHeight* height + verticalMargin * (height - 1));
}

-(void)adjustContents {
    NSString * decimalSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
    [btnDot setTitle:decimalSeparator forState:UIControlStateNormal];
    if (_contents & NumpadKeyboardContentsDot) {
        btnDot.frame = [self makeFrameforKeyInRow:3 column:0 width:1 height:1];
        [contentView addSubview:btnDot];
        btn0.frame = [self makeFrameforKeyInRow:3 column:1 width:1 height:1];
        [contentView addSubview:btn0];
        [btnWide0 removeFromSuperview];
    }
    else {
        [btnDot removeFromSuperview];
        [btn0 removeFromSuperview];
        btnWide0.frame = [self makeFrameforKeyInRow:3 column:0 width:2 height:1];
        [contentView addSubview:btnWide0];
    }

    UIButton * enterButton = btnBigEnter;

    if (_contents & NumpadKeyboardContentsKeyboard) {
        btnKb.frame = [self makeFrameforKeyInRow:0 column:3 width:1 height:1];
        [contentView addSubview:btnKb];
        enterButton = btnEnter;
    }
    else {
        [btnKb removeFromSuperview];
        if (_contents & NumpadKeyboardContentsTab) {
            btnTab.frame = [self makeFrameforKeyInRow:0 column:3 width:1 height:1];
            [contentView addSubview:btnTab];
            enterButton = btnEnter;
        }
        else {
            [btnTab removeFromSuperview];
        }
    }
    if (_contents & NumpadKeyboardContentsMinus) {
        CGRect frame = btnMinus.frame;
        frame.origin.y = _contents & (NumpadKeyboardContentsKeyboard | NumpadKeyboardContentsTab) ? btn6.frame.origin.y : btn3.frame.origin.y;
        btnMinus.frame = [self makeFrameforKeyInRow:_contents & (NumpadKeyboardContentsKeyboard | NumpadKeyboardContentsTab) ? 1 : 0 column:3 width:1 height:1];
        [contentView addSubview:btnMinus];
        enterButton = enterButton == btnBigEnter ? btnEnter : btnSmallEnter;
    }
    else {
        [btnMinus removeFromSuperview];
    }
    [btnEnter removeFromSuperview];
    [btnSmallEnter removeFromSuperview];
    [btnBigEnter removeFromSuperview];
    btnEnter.frame = [self makeFrameforKeyInRow:1 column:3 width:1 height:3];
    btnSmallEnter.frame = [self makeFrameforKeyInRow:2 column:3 width:1 height:2];
    btnBigEnter.frame = [self makeFrameforKeyInRow:0 column:3 width:1 height:4];
    [contentView addSubview:enterButton];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    if (!contentView){
        contentView = [self.view retain];
    }
    [self adjustContents];
}

-(void)notifyValueChanged:(UITextField *)textField {
    if ((textField.delegate != nil) && ([textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)])) {
        [textField.delegate performSelector:@selector(textFieldShouldReturn:) withObject:textField];
    }
}

-(IBAction)btnClick:(UIButton*)button {
    
	UIWindow *mWindow = [UIApplication sharedApplication].windows[0];
//	UIWindow *mWindow = self.view.window;

	id currentResponder = [mWindow getFirstResponder];
    if (!currentResponder)
        return;

    if ([currentResponder isKindOfClass:[UISearchBar class]])
        currentResponder = [((UISearchBar*)currentResponder) textField];

    UITextField* textField = (UITextField*)currentResponder;


	if (button.tag == 100) { //keyboard key
        
        if ([delegate respondsToSelector:@selector(keyboardButtonClick:)])
            [delegate keyboardButtonClick:self];

        return;
    }
    if (button.tag == 101 && [currentResponder isKindOfClass:[UITextField class]]) { //return key
        
        if (delegate != nil && [delegate respondsToSelector:@selector(enterClick:)])
            [delegate enterClick:self];

        if (textField.delegate
            && [textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)]
            && [textField.delegate textFieldShouldReturn:textField] && textField.canResignFirstResponder ){
                UIView* superview = textField.superview.superview;
            if ([superview isKindOfClass:[UISearchBar class]]){
                UISearchBar* searchBar = (UISearchBar*)superview;
                if (searchBar.delegate && [searchBar.delegate respondsToSelector:@selector(searchBarSearchButtonClicked:)]){
                    [searchBar.delegate searchBarSearchButtonClicked:searchBar];
                }
            }
            [textField resignFirstResponder];
        }

        return;
    }
    if (button.tag == 102){

        if ([delegate respondsToSelector:@selector(tabButtonClick:)])
            [delegate tabButtonClick:self];

        return;
    }
	if (button.tag == 103) { // hide keyboard
        
        [textField resignFirstResponder];

        return;
    }
    else if (button.tag == 99 || button.titleLabel.text.length > 0){
        
        NSString* text = button.tag == 99 ? @"" : button.titleLabel.text;
        
        if ([currentResponder isKindOfClass:[UITextField class]]){

            id responderDelegate = nil;
            if ([currentResponder isKindOfClass:[UITextFieldNumeric class]])
                responderDelegate = ((UITextFieldNumeric*)currentResponder).internalDelegate;
            else
                responderDelegate = [textField delegate];

            if (responderDelegate
                && [responderDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]){
                
                NSRange range = textField.selectedRange;
                if (button.tag == 99 && range.length == 0 && range.location > 0)
                    range = NSMakeRange(range.location - 1, 1);
                
                if (![responderDelegate textField:textField shouldChangeCharactersInRange:range replacementString:text])
                    return;
            }
        }
        
        if (button.tag == 99) { //backspace
            
            if ([currentResponder respondsToSelector:@selector(deleteBackward)])
                [currentResponder deleteBackward];
        }
        else if ([currentResponder respondsToSelector:@selector(insertText:)])
            [currentResponder performSelector:@selector(insertText:) withObject:text];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)setContents:(NumpadKeyboardContent)value {
    
    if (_contents != value) {
        
        _contents = value;
        
        if (self.view) {
                // Force loading view
        }
        
        [self adjustContents];
    }
}

@end
