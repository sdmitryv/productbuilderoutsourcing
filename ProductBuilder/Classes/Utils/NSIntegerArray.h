//
//  NSVectorArray.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 7/12/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

//struct NSIntegerArrayImpl;

@interface NSIntegerArray : NSObject{
    //struct NSIntegerArrayImpl* vector;
}
- (NSUInteger)count;
- (NSUInteger)indexOfInteger:(NSInteger)value;
- (NSInteger)integerAtIndex:(NSInteger) index;
- (void)addInteger:(NSInteger)value;
- (void)setInteger:(NSInteger) integer atIndex:(NSInteger) index;
- (void)removeLast;
- (NSArray *)subarrayWithRange:(NSRange)range;
- (NSNumber*)objectAtIndex:(NSUInteger)index;
- (NSNumber*)objectAtIndexedSubscript:(NSUInteger)index;
- (NSUInteger)indexOfObject:(NSNumber*)object;
- (void)removeObject:(NSNumber*)object;
- (void)removeObjectAtIndex:(NSUInteger)index;

@end
