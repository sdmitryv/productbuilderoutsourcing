//
//  NSSortedArray.m
//  StockCount
//
//  Created by Valera on 12/7/10.
//  Copyright 2010 DBBest Technologies. All rights reserved.
//

#import "NSSortedArray.h"


@implementation NSArray(FunctionalStyle)
- (NSArray*)filter:(BOOL(^)(id elt))filterBlock
{
	id filteredArray = [NSMutableArray array];
	for (id elt in self)
		if (filterBlock(elt))
            [filteredArray addObject:elt];
	return	filteredArray;
}
@end

static NSComparator caseInsensitiveComparator = ^(id first, id second) {
    
    if ([first isKindOfClass:[NSString class]]){
        static NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch | NSNumericSearch | NSWidthInsensitiveSearch | NSForcedOrderingSearch;
        return [first compare:second options:comparisonOptions];
    }
    return [first compare:second];
};

static NSComparator caseInsensitiveStringComparator = ^(id first, id second) {
    if ([first isKindOfClass:[NSString class]]){
        static NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch | NSWidthInsensitiveSearch | NSForcedOrderingSearch;
        return [first compare:second options:comparisonOptions];
    }
    return [first compare:second];
};

@implementation NSMutableArray(CaseInsensitiveSort)

- (void)insensitiveSortByKey:(NSString*)key ascending:(BOOL)ascending{
	NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:ascending comparator:caseInsensitiveComparator];
	[self sortUsingDescriptors:@[sortDescriptor]];
    [sortDescriptor release];
}

@end


@implementation NSSortedArray

- (id)initWithArray:(NSArray*)array{
	if ((self = [super init])){
        if ([self isKindOfClass:[NSMutableSortedArray class]] && ![array isKindOfClass:[NSMutableArray class]]){
            array = [NSMutableArray arrayWithArray:array];
        }
        self.originalArray = array;
	}
	return self;
}

- (id)initWithCEntityList:(CEntityList*)array{
    if ((self = [super init])){
		self.originalArray = array;
	}
	return self;
}

- (NSUInteger)count{
	return [(filtered ? arrayFiltered : arrayOriginal) count];
}

- (id)originalArray{
    return arrayOriginal;
}

-(void)setOriginalArray:(id)value{
    [arrayOriginal release];
    arrayOriginal = [value retain];
    [self refresh];
}

- (id)objectAtIndex:(NSUInteger)index{
	return (sorted ? arraySorted : (filtered ? arrayFiltered : arrayOriginal))[index];
}
- (id)objectAtIndexedSubscript:(NSUInteger)index{
    return [self objectAtIndex:index];
}

- (NSUInteger)indexOfObject:(id)anObject{
	return [(sorted ? arraySorted : (filtered ? arrayFiltered : arrayOriginal)) indexOfObject:anObject];
}


#pragma mark -
#pragma mark sorting

- (void)sortByKey:(NSString*)key ascending:(BOOL)ascending{
	
    [self sortByKey:key ascending:ascending comparator:caseInsensitiveComparator useDescriptor:YES];
}


- (void)sortStringByKey:(NSString*)key ascending:(BOOL)ascending{
	
    [self sortByKey:key ascending:ascending comparator:caseInsensitiveStringComparator useDescriptor:YES];
}

- (void)sortByKey:(NSString*)key ascending:(BOOL)ascending comparator:(NSComparator)comparator useDescriptor:(BOOL)useDescriptor{

	if (!arraySorted)
        arraySorted = [[NSMutableArray alloc]initWithArray:(filtered ? arrayFiltered  : self.arrayOriginal )];

    if (useDescriptor) {
        
        NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:ascending comparator:comparator];
        [arraySorted sortUsingDescriptors:@[sortDescriptor]];
        [sortDescriptor release];
    }
    else {
        
        NSMutableArray *newArray = [[NSMutableArray alloc] initWithArray:[arraySorted sortedArrayUsingComparator:^(id a, id b) {
            
            NSObject *obj1 = [a valueForKeyPath:key];
            NSObject *obj2 = [b valueForKeyPath:key];
            
            NSComparisonResult res = comparator(obj1, obj2);
            return res*(ascending ? 1 : -1);
        }]];
        
        [arraySorted release];
        arraySorted = newArray;
    }
    
	sorted = TRUE;
	sortingKey = [key retain];
	sortAscending = ascending;
}

- (void)resort{
	[arraySorted release];
	arraySorted = nil;
	if (sorted){
		[self sortByKey:sortingKey ascending:sortAscending];
	}
}

- (void)removeSort{
	[sortingKey release];
	sortingKey = nil;
	sorted = FALSE;
	[self resort];
}

-(BOOL)sorted{
	return sorted;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len{
	return [(sorted ? arraySorted : (filtered ? arrayFiltered : arrayOriginal)) countByEnumeratingWithState:state objects:stackbuf count:len];
}

-(NSArray*)arrayOriginal{
    return [arrayOriginal isKindOfClass:[CEntityList class]] ? ((CEntityList*)arrayOriginal).list : arrayOriginal;
}

#pragma mark -
#pragma mark filtering

- (void)applyFilter:(BOOL(^)(id elt))filterBlock{
    [filteringPredicate release];
    filteringPredicate = nil;
    if (filteringBlock!=filterBlock){
        [filteringBlock release];
        filteringBlock = [filterBlock copy];
    }
    filtered = filterBlock!=nil;
    [arrayFiltered release];
	arrayFiltered = [(NSMutableArray*)[self.arrayOriginal filter:filterBlock] retain];
	[self resort];
}

- (void)applyFilterWithPredicate:(NSPredicate*)predicate{
    [arrayFiltered release];
    [filteringBlock release];
    filteringBlock = nil;
    if (filteringPredicate!=predicate){
        [filteringPredicate release];
        filteringPredicate = [predicate retain];
    }
	filtered = predicate!=nil;
	arrayFiltered = [[NSMutableArray arrayWithArray:[self.arrayOriginal filteredArrayUsingPredicate:filteringPredicate]]retain];
	[self resort];
}

- (void)reApplyFilter{
	if (filtered){
        if (filteringBlock){
            [self applyFilter:filteringBlock];
        }
        else{
            [self applyFilterWithPredicate:filteringPredicate];
        }
	}
}

- (void)removeFilter{
	filteringBlock = nil;
	filtered = FALSE;
	[arrayFiltered release];
	arrayFiltered = nil;
	[self resort];
}

- (BOOL)filtered{
	return filtered;
}

- (void)refresh{
	[self reApplyFilter];
	[self resort];
}

- (NSArray*)filteredArray{
    return arrayFiltered;
}

- (void) dealloc{
	[sortingKey release];
	[arrayOriginal release];
	[arraySorted release];
	[arrayFiltered release];
    [filteringBlock release];
    [filteringPredicate release];
	[super dealloc];
}

@end

@implementation NSMutableSortedArray

- (id)init{
	if ((self = [super init])){
		arrayOriginal = [[NSMutableArray alloc]init];
	}
	return self;
}
		
- (id)initWithMutableArray:(NSMutableArray*)array{
	if ((self = [super init])){
		arrayOriginal = [array retain];
	}
	return self;
}

- (void)addObject:(id)anObject{
	[arrayOriginal addObject:anObject];
	[self refresh];
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
	[arrayOriginal insertObject:anObject atIndex:index];
	[self refresh];
}

- (void)removeObjectAtIndex:(NSUInteger)index{
    if (!filtered && !sorted){
        [arrayOriginal removeObjectAtIndex:index];
        return;
    }
    id anObject = [self objectAtIndex:index];
    [arrayOriginal removeObject:anObject];
    if (sorted){
        [arraySorted removeObject:anObject];
    }
    if (filtered){
        [arrayFiltered removeObject:anObject];
    }
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)object{
    if (!filtered && !sorted){
		arrayOriginal[index] = object;
		return;
	}
	[self refresh];
}

- (void)setObject:(id)anObject atIndexedSubscript:(NSUInteger)index{
    [self replaceObjectAtIndex:index withObject:anObject];
}

- (void)removeObjectsAtIndexes:(NSIndexSet *)indexes{
    if (!filtered && !sorted){
		[arrayOriginal removeObjectsAtIndexes:indexes];
		return;
	}
	NSArray* objects = [arrayOriginal objectsAtIndexes:indexes];
	[arrayOriginal removeObjectsAtIndexes:indexes];
	if (sorted){
		[arraySorted removeObjectsInArray:objects];
	}
	if (filtered){
		[arrayFiltered removeObjectsInArray:objects];
	}
}

- (void)removeAllObjects{
    [arrayOriginal removeAllObjects];
    [arrayFiltered removeAllObjects];
    [arraySorted removeAllObjects];
}


@end
