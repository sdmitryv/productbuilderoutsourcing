//
//  DataUtils.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/5/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

@interface DataUtils : NSObject {

}
+(void)resetFormatters;
+(NSString *)stringFromDate:(NSDate *)date;
+(NSString *)readableDateTimeStringFromDate:(NSDate *)date;
+(NSString *)readableDateTimeStringFromDate:(NSDate *)date format:(NSString*)format;
+(NSString *)readableDateStringFromDate:(NSDate *)date;
+(NSString *)readableDateStringFromDate:(NSDate *)date format:(NSString*)format;
+(NSString *)readableDateStringFromDate:(NSDate *)date formatterStyle:(NSDateFormatterStyle)formatterStyle;
+(NSString *)readableDateTimeStringFromDate:(NSDate *)date dateFormatterStyle:(NSDateFormatterStyle)dateFormatterStyle timeFormatterStyle:(NSDateFormatterStyle)timeFormatterStyle;
+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date;
+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date format:(NSString*)format;
+(NSString *)readableUTCDateTimeStringFromDate:(NSDate *)date dateFormatterStyle:(NSDateFormatterStyle)dateFormatterStyle timeFormatterStyle:(NSDateFormatterStyle)timeFormatterStyle;
+(NSDate *)dateFromString:(NSString *)string;
+(NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
+(NSString *)fileNameFromDate:(NSDate *)date;
+(NSString *)fileNameFromCurrentDate;
+(NSString *)stringFromBool:(BOOL)data;
+(BOOL)BoolFromString:(NSString *)string;
+(BPUUID *)CodeToGuid:(NSString *)code;
+(NSNumber *)intFromString:(NSString *)string;
+(NSString *)stringFromXmlString:(NSString *)string;
+(BOOL)isValidEmail:(NSString *)string;
+(BOOL)validateAndFormatPhone:(UITextField*)textField;
@end
