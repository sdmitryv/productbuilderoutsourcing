//
//  RTIntegerArray.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 5/28/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "RTIntegerArray.h"

@implementation RTIntegerArray

-(id)init{
    if ((self=[super init])){
        //array = [[NSMutableArray alloc]init];
        array = CFArrayCreateMutable(kCFAllocatorDefault,0,NULL);
    }
        return self;
}

-(void)dealloc{
    //[array release];
    CFRelease(array);
    [super dealloc];
}

- (NSUInteger)count{
    return CFArrayGetCount(array);
}

- (NSUInteger)indexOfInteger:(NSUInteger)value{
    //return [array indexOfObject:(id)value];
    return CFArrayGetFirstIndexOfValue(array, CFRangeMake(0, [self count]), (const void *)value);
}

- (NSUInteger)intAtIndex:(NSUInteger)index{
    //return [array indexOfObject:(id)value];
    return (NSUInteger)CFArrayGetValueAtIndex(array, index);
}

-(NSArray*)subarrayWithRange:(NSRange)range{
    NSUInteger values[range.length];
    CFArrayGetValues(array, CFRangeMake(range.location, range.length), (const void**)&values);
    NSMutableArray* a = [[NSMutableArray alloc]init];
    for(int i=0; i<range.length;i++){
        [a addObject:@(values[i])];
    }
    return [a autorelease];
}

- (void)addInteger:(NSUInteger)value{
    //[array addObject:(id)value];
    CFArrayAppendValue(array, (void*)value);
}

- (NSNumber*)objectAtIndex:(NSUInteger)index{
    NSUInteger value = [self intAtIndex:index];
    return @(value);
}

- (NSUInteger)indexOfObject:(NSNumber*)object{
    return [self indexOfInteger:[object intValue]];
 }

//#pragma mark -
//#pragma mark for fast enumeration
//
//- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id *)stackbuf count:(NSUInteger)len
//{
//	return [array countByEnumeratingWithState:state objects:stackbuf count:len];
//}
@end
