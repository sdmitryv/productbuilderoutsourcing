//
//  NSDecimal+Operators.h
//  NSDecimalOperators
//
//  Created by Markus Gasser on 26.11.13.
//  Copyright (c) 2013 konoma GmbH. All rights reserved.
//
//  Modified by Valery Fomenko
//  Copyright (c) 2014 Cloudworks. All rights reserved.

#ifdef __cplusplus // make it safe to be included in plain C/Objective-C code

#define DECIMAL_SUFFIX _D

extern NSDecimal operator + (const NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal operator + (const NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal operator - (const NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal operator - (const NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal operator * (const NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal operator * (const NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal operator / (const NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal operator / (const NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal& operator += (NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal& operator += (NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal& operator -= (NSDecimal& lhs, const NSDecimal& rhs);
extern NSDecimal& operator -= (NSDecimal& lhs, const NSInteger& rhs);
extern NSDecimal& operator ++ (NSDecimal& lhs);
extern NSDecimal operator ++ (NSDecimal& lhs, int);
extern NSDecimal& operator -- (NSDecimal& lhs);
extern NSDecimal operator -- (NSDecimal& lhs, int);
extern NSDecimal operator-(const NSDecimal& lhs);

extern BOOL operator == (const NSDecimal& lhs, const NSDecimal& rhs);
extern BOOL operator != (const NSDecimal& lhs, const NSDecimal& rhs);
extern BOOL operator < (const NSDecimal& lhs, const NSDecimal& rhs);
extern BOOL operator > (const NSDecimal& lhs, const NSDecimal& rhs);
extern BOOL operator <= (const NSDecimal& lhs, const NSDecimal& rhs);
extern BOOL operator >= (const NSDecimal& lhs, const NSDecimal& rhs);

extern BOOL operator == (const NSDecimal& lhs, const NSInteger& rhs);
extern BOOL operator != (const NSDecimal& lhs, const NSInteger& rhs);
extern BOOL operator < (const NSDecimal& lhs, const NSInteger& rhs);
extern BOOL operator > (const NSDecimal& lhs, const NSInteger& rhs);
extern BOOL operator <= (const NSDecimal& lhs, const NSInteger& rhs);
extern BOOL operator >= (const NSDecimal& lhs, const NSInteger& rhs);

extern NSDecimal operator "" DECIMAL_SUFFIX (const char *value);

#endif
