//
//  UIImage+Text.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 12/24/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Text)

+(UIImage *)imageWithText:(NSString *)text size:(CGSize)size fontSize:(CGFloat)fontSize cornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth;
+(UIImage *)imageForTableViewRowActionWithTitle:(NSString *)title textAttributes:(NSDictionary *)attributes backgroundColor:(UIColor *)color cellSize:(CGSize)cellSize;
@end
