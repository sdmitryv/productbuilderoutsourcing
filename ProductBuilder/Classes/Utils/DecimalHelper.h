//
//  DecimalHelper.h
//  ProductBuilder
//
//  Created by Valera on 11/15/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DecimalBehaviuor)

-(NSDecimal)decimalValue;
@end

@interface NSCurrencyFormatter : NSNumberFormatter

+(NSString*)formatCurrency:(NSDecimal)value;
+(NSString*)formatCurrencyFromDouble:(double)value;
+(NSString*)formatCurrencyFromNumber:(NSNumber*)value;
+(NSString*)formatCurrencyFromObject:(id)value;
+(NSString*)defaultCurrencyFromNumber:(NSNumber*)value;

+(NSString*)formatPercent:(NSDecimal)value;
+(NSString*)formatPercentFromDouble:(double)value;
+(NSString*)formatPercentFromNumber:(NSNumber*)value;
+(NSString*)formatPercentFromObject:(id)value;

+(NSString*)formatQty:(NSDecimal)value;
+(NSString*)formatQtyFromDouble:(double)value;
+(NSString*)formatQtyFromNumber:(NSNumber*)value;
+(NSString*)formatQtyFromObject:(id)value;

+(NSString*)formatNumber:(NSDecimal)value;
+(NSString*)formatNumberFromDouble:(double)value;
+(NSString*)formatNumberFromNumber:(NSNumber*)value;
+(NSString*)formatNumberFromObject:(id)value;

+(NSString*)formatDecimal:(NSDecimal)value;
+(NSString*)formatDecimalFromDouble:(double)value;
+(NSString*)formatDecimalFromNumber:(NSNumber*)value;
+(NSString*)formatDecimalFromObject:(id)value;

+(NSNumberFormatter*)defaultCurrencyFormatter;
+(NSNumberFormatter*)defaultPercentFormatter;
+(NSNumberFormatter*)defaultQtyFormatter;
+(NSNumberFormatter*)defaultNumberFormatter;
+(NSNumberFormatter*)defaultDecimalFormatter;

+(NSNumberFormatter*)invariantDecimalFormatter;

+(int)currencyFractionDigits;

+(void)setDefaultCurrencyFormatter:(NSNumberFormatter*)defaultCurrencyFormatter;
+(void)setDefaultPercentFormatter:(NSNumberFormatter*)defaultPercentFormatter;
+(void)setDefaultQtyFormatter:(NSNumberFormatter*)defaultQtyFormatter;
+(void)setDefaultNumberFormatter:(NSNumberFormatter*)defaultNumberFormatter;

@end


static const NSDecimal zero = {0, 0, 0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0}};
static const NSDecimal one  = {0, 1, 0, 1, 0, {1, 0, 0, 0, 0, 0, 0, 0}};
static const NSDecimal hundred  = {2, 1, 0, 1, 0, {1, 0, 0, 0, 0, 0, 0, 0}};
static const NSDecimal hundredth  = {-2, 1, 0, 1, 0, {1, 0, 0, 0, 0, 0, 0, 0}};

#define D0 zero
#define D1 one
#define D100 hundred
#define D001 hundredth

#ifdef __cplusplus
extern "C" {
#endif

/// @name Convert primitive types to NSDecimal
/// @{
extern NSDecimal CPDecimalFromChar(int8_t i);
extern NSDecimal CPDecimalFromShort(int16_t i);
extern NSDecimal CPDecimalFromLong(int32_t i);
extern NSDecimal CPDecimalFromLongLong(int64_t i);
extern NSDecimal CPDecimalFromInt(int i);
extern NSDecimal CPDecimalFromInteger(NSInteger i);

extern NSDecimal CPDecimalFromUnsignedChar(uint8_t i);
extern NSDecimal CPDecimalFromUnsignedShort(uint16_t i);
extern NSDecimal CPDecimalFromUnsignedLong(uint32_t i);
extern NSDecimal CPDecimalFromUnsignedLongLong(uint64_t i);
extern NSDecimal CPDecimalFromUnsignedInt(unsigned int i);
extern NSDecimal CPDecimalFromUnsignedInteger(NSUInteger i);

extern NSDecimal CPDecimalFromFloat(float f);
extern NSDecimal CPDecimalFromDouble(double d);

extern NSDecimal CPDecimalFromString(NSString *stringRepresentation);
/// @}

/// @name NSDecimal arithmetic
/// @{
extern NSDecimal CPDecimalAdd(NSDecimal leftOperand, NSDecimal rightOperand);
extern NSDecimal CPDecimalSubtract(NSDecimal leftOperand, NSDecimal rightOperand);
extern NSDecimal CPDecimalMultiply(NSDecimal leftOperand, NSDecimal rightOperand);
extern NSDecimal CPDecimalDivide(NSDecimal numerator, NSDecimal denominator);
extern NSDecimal CPDecimalModulo(NSDecimal numerator, NSDecimal denominator);
/// @}

/// @name NSDecimal comparison
/// @{
extern BOOL CPDecimalGreaterThan(NSDecimal leftOperand, NSDecimal rightOperand);
extern BOOL CPDecimalGreaterThanOrEqualTo(NSDecimal leftOperand, NSDecimal rightOperand);
extern BOOL CPDecimalLessThan(NSDecimal leftOperand, NSDecimal rightOperand);
extern BOOL CPDecimalLessThanOrEqualTo(NSDecimal leftOperand, NSDecimal rightOperand);
extern BOOL CPDecimalEquals(NSDecimal leftOperand, NSDecimal rightOperand);

extern BOOL CPDecimalGreaterThan0(NSDecimal d);
extern BOOL CPDecimalGreaterThanOrEqualTo0(NSDecimal d);
extern BOOL CPDecimalLessThan0(NSDecimal d);
extern BOOL CPDecimalLessThanOrEqualTo0(NSDecimal d);
extern BOOL CPDecimalEquals0(NSDecimal d);

extern NSDecimal CPDecimalABS(NSDecimal d);
extern NSDecimal CPDecimalNegate(NSDecimal d);
/// @}

/// @name NSDecimal utilities
/// @{
extern NSDecimal CPDecimalNaN(void);
/// @}
extern NSString * CPDecimalStringValue(NSDecimal decimalNumber);
extern double  CPDecimalDoubleValue(NSDecimal decimalNumber);

#ifdef __cplusplus
}
#endif
