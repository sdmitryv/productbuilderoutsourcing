//
//  NumberpadInputViewController.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 1/15/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "NumberpadInputViewController.h"

@interface NumberpadInputViewController ()

@end

@implementation NumberpadInputViewController

-(id)init {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            self = [super initWithNibName:@"Numberpad7InputView" bundle:nil];
        }
        else {
            self = [super initWithNibName:@"NumberpadInputView" bundle:nil];
        }
    }
    else {
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            self = [super initWithNibName:@"Numberpad7InputView_iPhone" bundle:nil];
        }
        else {
            self = [super initWithNibName:@"NumberpadInputView_iPhone" bundle:nil];
        }
    }
    if (self) {
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustContents {
    _contents |= NumpadKeyboardContentsKeyboard;
    [super adjustContents];
}

@end
