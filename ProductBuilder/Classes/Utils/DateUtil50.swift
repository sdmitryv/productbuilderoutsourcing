//
//  DateUtil.swift
//  RPlus
//
//  Created by Alexander Martyshko on 8/1/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

class DateUtil {
    private static var __once: () = {
                instance = DateUtil()
            }()
    fileprivate static var instance : DateUtil!
    fileprivate static var token: Int = 0
    
    fileprivate let dayDateFormatter: DateFormatter!
    fileprivate let calendar: Calendar!
    
    let dateFormatter: DateFormatter
    let timeFormatter: DateFormatter
    
    class func getInstance()->DateUtil {
        if instance == nil {
            _ = DateUtil.__once
        }
        return instance
    }
    
    fileprivate init() {
        dayDateFormatter = DateFormatter()
        dayDateFormatter.dateFormat = "dd.MM.yyyy"
        dayDateFormatter.locale = Locale(identifier: "en_US")
        
        calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.none
        timeFormatter = DateFormatter()
        timeFormatter.dateStyle = DateFormatter.Style.none
        timeFormatter.timeStyle = DateFormatter.Style.short
    }
    
    // returns Yesterday, Today, Tommorow or ""
    func nearDateDescription(_ forDate: Date) -> String? {
        
        let now = Date()
        let forDateStr = dayDateFormatter.string(from: forDate)
        let todayStr = dayDateFormatter.string(from: now)
        
        var components = DateComponents()
        components.day = -1
        let yesterday = calendar.date(byAdding: components, to: now)
        components.day = 1
        let tomorrow = calendar.date(byAdding: components, to: now)
        
        let tomorrowStr = dayDateFormatter.string(from: tomorrow!)
        let yesterdayStr = dayDateFormatter.string(from: yesterday!)
        
        if forDateStr == todayStr {
            return NSLocalizedString("TODAY", comment: "")
        }
        else if forDateStr == tomorrowStr {
            return NSLocalizedString("TOMORROW", comment: "")
        }
        else if forDateStr == yesterdayStr {
            return NSLocalizedString("YESTERDAY", comment: "")
        }
        return nil
    }
}
