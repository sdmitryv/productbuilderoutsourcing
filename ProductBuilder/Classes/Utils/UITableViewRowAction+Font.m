//
//  UITableViewRowAction.m
//  ProductBuilder
//
//  Created by valery on 7/13/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UITableViewRowAction+Font.h"
#import "NSString+Utils.h"
#import "UIImage+Text.h"

@implementation UITableViewRowAction(Font)


+ (instancetype)rowActionWithTitle:(NSString *)title font:(UIFont*)font backgroundColor:(UIColor*)backgroundColor foregroundColor:(UIColor*)foregroundColor cellHeight:(CGFloat)cellHeight handler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler{

    NSDictionary *systemAttributes = @{ NSFontAttributeName: [UIFont systemFontOfSize:18] };
    NSDictionary *newAttributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: foregroundColor};

    NSString *titleWhiteSpaced = [title whitespaceReplacementWithSystemAttributes:systemAttributes newAttributes:newAttributes];
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:titleWhiteSpaced handler:handler];
    CGSize cellSize = CGSizeMake([titleWhiteSpaced sizeWithAttributes:systemAttributes].width + 30, cellHeight);

    UIImage *patternImage = [UIImage imageForTableViewRowActionWithTitle:title textAttributes:newAttributes backgroundColor:backgroundColor cellSize:cellSize];
    editAction.backgroundColor = [UIColor colorWithPatternImage:patternImage];
    return editAction;
}
@end
