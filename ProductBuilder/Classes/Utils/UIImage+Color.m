//
//  UIImage+Color.m
//  ProductBuilder
//
//  Created by valery on 1/5/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UIImage+Color.h"

@implementation UIImage(Color)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
