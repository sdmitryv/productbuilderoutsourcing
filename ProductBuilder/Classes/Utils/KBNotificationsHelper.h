//
// IQNotificationsHelper.h
// iQueue
//
// Created by Seivan Heidari on 7/14/10.
// Copyright 2010 __MyCompanyName__. All rights reserved.
//

@protocol KBNotificationsHelperDelegate<NSObject>
@optional
-(void)keyboardDidShow:(id)sender;
-(void)keyboardWillShow:(id)sender;
-(void)keyboardDidHide:(id)sender;
-(void)keyboardWillHide:(id)sender;
@end


@interface KBNotificationsHelper : NSObject {
	id<KBNotificationsHelperDelegate> delegate;
}
@property(nonatomic,assign) id<KBNotificationsHelperDelegate> delegate;
+(void)setKeyboardNotificationsToDelegate:(id<KBNotificationsHelperDelegate>)delegate;
+(void)removeKeyboardNotificationsFromDelegate:(id<KBNotificationsHelperDelegate>)delegate;
@end