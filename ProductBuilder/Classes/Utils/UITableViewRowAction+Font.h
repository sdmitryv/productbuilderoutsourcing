//
//  UITableViewRowAction.h
//  ProductBuilder
//
//  Created by valery on 7/13/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewRowAction(Font)

+ (instancetype)rowActionWithTitle:(NSString *)title font:(UIFont*)font backgroundColor:(UIColor*)backgroundColor foregroundColor:(UIColor*)foregroundColor cellHeight:(CGFloat)cellHeight handler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler;

@end
