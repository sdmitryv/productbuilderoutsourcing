//
//  EGODatabase.m
//  EGODatabase
//
//  Created by Shaun Harrison on 3/6/09.
//  Copyright (c) 2009 enormego
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

/* 
 * Some of the code below is based on code from FMDB
 * (Mostly bindObject, and the error checking)
 *
 * We've reworked it, rewritten a lot of it, and feel we've improved on it.
 * However credit is still due to FMDB.
 *
 * @see http://code.google.com/p/flycode/soureturnCodee/browse/#svn/trunk/fmdb
 */


#import "EGODatabase.h"
#import "EGOStatement.h"
#import "DBAdditions.h"
//#import "EGOTransactionState.h"
#import <pthread.h>
#import "DataManager.h"

#define EGODatabaseLockLog 0
#if EGODatabaseLockLog
#define EGODBLockLog(s,...) NSLog(s, ##__VA_ARGS__)
#else
#define EGODBLockLog(s,...)
#endif

#define VAToArray(firstarg) ({\
NSMutableArray* valistArray = [NSMutableArray array];\
id obj = nil;\
va_list arguments;\
va_start(arguments, firstarg);\
while ((obj = va_arg(arguments, id))) {\
[valistArray addObjectNilSafe:obj];\
}\
va_end(arguments);\
valistArray;\
})
NSString* const NSEGOCustomRunLoopMode = @"NSEGOCustomRunLoopMode";

#ifdef EGODatabaseRunQueryInBackgoundForMainThread
NSString* const EGODatabaseStartExecutingMainThreadNotification = @"EGODatabaseStartExecutingMainThreadNotification";
NSString* const EGODatabaseEndExecutingMainThreadNotification = @"EGODatabaseEndExecutingMainThreadNotification";
#endif
NSString* const EGODatabaseStartLockNotification = @"EGODatabaseStartLockNotification";
NSString* const EGODatabaseEndLockNotification = @"EGODatabaseEndLockNotification";

@implementation EGODatabase

#pragma mark -
#pragma mark Cache

- (void) clearCachedStatements {
    [cachedStatements removeAllObjects];
}

- (EGOStatement*) cachedStatementForQuery:(NSString*)query {
    return CFDictionaryGetValue((CFDictionaryRef)cachedStatements, query);
}

- (void) setCachedStatement:(EGOStatement*)statement forQuery:(NSString*)query {
    if (!statement){
        [cachedStatements removeObjectForKey:query];
        return;
    }
    cachedStatements[query] = statement;
}

- (BOOL)shouldCacheStatements {
    return shouldCacheStatements;
}

- (void)setShouldCacheStatements:(BOOL)value {
    
    shouldCacheStatements = value;
    
    if (shouldCacheStatements && !cachedStatements) {
        cachedStatements = [[NSMutableDictionary alloc]init];
    }
    
    if (!shouldCacheStatements) {
        [cachedStatements release];
        cachedStatements = nil;
    }
}

- (void)interrupt{
    sqlite3_interrupt(_handle);
}

#pragma mark -
#pragma mark Lock stuff

-(void)beginExecute{
}

-(void)endExecute{
    if (!_handle || _shuttingDown) return;
    int autocommit = sqlite3_get_autocommit(_handle);
    if (autocommit){
        self.inTransaction = FALSE;
    }
    else{
        self.inTransaction = TRUE;
    }
    [_runningCondition lock];
    [_runningCondition broadcast];
    [_runningCondition unlock];
}

#pragma mark -
#pragma mark Executing

- (BOOL)execute:(NSString*)sql{
#ifdef EGODatabaseRunQueryInBackgoundForMainThread
    if ([NSThread isMainThread]){
         [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseStartExecutingMainThreadNotification object:self userInfo:nil];
        __block BOOL result = FALSE;
        __block BOOL executing = TRUE;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            result = [self execute:sql];
            executing = FALSE;
        });
        
        while (executing){
            [[NSRunLoop currentRunLoop] runMode:NSEGOCustomRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseEndExecutingMainThreadNotification object:self userInfo:nil];
        return result;
    }
#endif
    [self beginExecute];
	int returnCode = 0;
    @try {
        
        if(![self open]) {
            return NO;
        }
        
        char* errmsg = 0;
        returnCode = sqlite3_exec(_handle, [sql UTF8String], 0, 0, &errmsg);
        
        if (SQLITE_BUSY == returnCode) {
            EGODBLockLog(@"[EGODatabase] Exec Failed, Database Busy:\n%@\n\n", sql);
        } else if (SQLITE_OK != returnCode) {
            EGODBDebugLog(@"[EGODatabase] Exec Failed, Error: %d \"%@\"\n%@\n\n", returnCode, errmsg ? @(errmsg) : [self lastErrorMessage], sql);
        }
	}
    @finally{
        [self endExecute];
	}
	return (returnCode == SQLITE_OK);
}

NSString* const kBindFailedMsg = @"[EGODatabase] Invalid bind count for number of arguments: %@.";

#ifdef EGODatabaseRunQueryInBackgoundForMainThread
-(void)wakeUpMainThread{
    
}
#endif

- (void)executeQuery:(NSString*)sql parametersList:(id)parametersList 
 handle_errors_block:(void (^)(int errorCode, const NSString* errorMessage))handle_errors_block
   handle_rows_block:(void (^)(sqlite3_stmt* statement))handle_rows_block{

#ifdef EGODatabaseRunQueryInBackgoundForMainThread
    if ([NSThread isMainThread]){
         [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseStartExecutingMainThreadNotification object:self userInfo:nil];
        __block BOOL executing = TRUE;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self executeQuery:sql parametersList:parametersList handle_errors_block:handle_errors_block handle_rows_block:handle_rows_block];
            executing = FALSE;
            [self performSelectorOnMainThread:@selector(wakeUpMainThread) withObject:nil waitUntilDone:NO];
        });
        while (executing){
            [[NSRunLoop currentRunLoop] runMode:NSEGOCustomRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseEndExecutingMainThreadNotification object:self userInfo:nil];
        return;
    }
#endif
    [self beginExecute];
    EGOStatement* statement = nil;
	@try{
        if(![self open]) {
            return;
        }
        
        EGOStatement *cachedStmt = NULL;
        
        if (shouldCacheStatements) {
            cachedStmt = [self cachedStatementForQuery:sql];
            if(cachedStmt){
                if (![cachedStmt reset]){
                    [self setCachedStatement:nil forQuery:sql];
                    cachedStmt = nil;
                }
                else{
                    statement = [cachedStmt retain];
                }
            }
        }
        
        if (!statement){
            statement = [[EGOStatement alloc]initWithDatabase:self stmt:sql];
            if (!statement){
                if (handle_errors_block){
                    handle_errors_block([self lastErrorCode], [self lastErrorMessage]);
                }
                return;
            }
        }
        
        if (parametersList) {
            if (![statement bind:parametersList]) {
                EGODBDebugLog(kBindFailedMsg, sql);
                if (handle_errors_block){
                    handle_errors_block(1001, [NSString stringWithFormat:kBindFailedMsg, sql]);
                }
                return;
            }
        }

        if (handle_rows_block)
            handle_rows_block(statement.statement);
        
        if (shouldCacheStatements){
            if (!cachedStmt) {
                [self setCachedStatement:statement forQuery:sql];
            }
            else {
                cachedStmt.useCount = cachedStmt.useCount + 1;
            }
        }
	}
    @catch (NSException* e) {
        @throw;
    }
    @finally {
        [statement release];
        [self endExecute];
    }
}

- (NSIntegerArray*)executeQueryFastArray:(NSString*)sql parametersList:(id)parametersList {
    NSIntegerArray* array = [[[NSIntegerArray alloc] init] autorelease];
    
    [self executeQuery:sql parametersList:parametersList handle_errors_block:^(int errorCode, NSString *errorMessage) {
    } handle_rows_block:^(sqlite3_stmt *statement) {
        
        int columnCount = sqlite3_column_count(statement);
        if (!columnCount) return;
        
        @autoreleasepool {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                int columnType = sqlite3_column_type(statement,0);
                if(columnType==SQLITE_INTEGER) {
                    if (columnType != SQLITE_BLOB) {
                        sqlite3_int64 value = sqlite3_column_int64(statement, 0);
                        [array addInteger:value];
                        
                    }
                    else {
                        //we shouldn't get there
                        [array addInteger:0];
                    }
                    
                }
            }
        }
    }];
    return array;
}

- (EGODatabaseResult*)executeQuery:(NSString*)sql parametersList:(id)parametersList {
    EGODatabaseResult* result = [[[EGODatabaseResult alloc] init] autorelease];
    
    [self executeQuery:sql parametersList:parametersList handle_errors_block:^(int errorCode, NSString *errorMessage) {
        result.errorCode = [self lastErrorCode];
        result.errorMessage = [self lastErrorMessage];
    } handle_rows_block:^(sqlite3_stmt *statement) {
        
        int columnCount = sqlite3_column_count(statement);
        int x;
        @autoreleasepool {
            for(x=0;x<columnCount;x++) {
                const char * columnName = sqlite3_column_name(statement,x);
                NSString* columnNameStr = nil;
                if(columnName) {
                    columnNameStr = [[NSString alloc ]initWithUTF8String:columnName];
                } else {
                    columnNameStr = [[NSString alloc] initWithFormat:@"%d", x];
                }
                NSString* columnNameStrLwC =[columnNameStr lowercaseString];
                NSUInteger columnNameStrLwCCounter = 1;
                //if the column name is duplicated then the row in columnNamesCI won't be filled
                //so fill the row in columnNamesCI in that case with any value to make columnNamesCI.count equal to the column count
                while ([result.columnNamesCI indexOfObject:columnNameStrLwC]!=NSNotFound){
                    columnNameStrLwC = [NSString stringWithFormat:@"%@%lu",columnNameStr, (unsigned long)columnNameStrLwCCounter++];
                }
                [result.columnNames addObject:columnNameStr];
                [result.columnNamesCI addObject:columnNameStrLwC];
                [columnNameStr release];

                const char * column_decltype = sqlite3_column_decltype(statement,x);
                if(column_decltype) {
                    NSString* decltype = [[NSString alloc]initWithUTF8String:column_decltype];
                    [result.columnTypes addObject:decltype];
                    [decltype release];
                } else {
                    [result.columnTypes addObject:@""];
                }
            }
        }
        @autoreleasepool {
            while(sqlite3_step(statement) == SQLITE_ROW) {
                EGODatabaseRow* row = [[EGODatabaseRow alloc] initWithDatabaseResult:result];
                for(x=0;x<columnCount;x++) {
                    int columnType = sqlite3_column_type(statement,x);
                    switch (columnType) {
                        case SQLITE_NULL:
                            [row.columnData addObject:[NSNull null]];
                            break;
                        case SQLITE_BLOB:
                        {
                            int bytes = sqlite3_column_bytes(statement, x);
                            NSObject* data = nil;
                            @try{
                                data = bytes == 0 ? [NSNull null] : [[[NSData alloc] initWithBytes:sqlite3_column_blob(statement, x) length: bytes] autorelease];
                            }
                            @catch(NSException*){
                                
                            }
                            [row.columnData addObject:data ?: [NSNull null]];
                        }
                            break;
                        case SQLITE_INTEGER:
                        case SQLITE_FLOAT:
                        {
                            double d = sqlite3_column_double(statement,x);
                            //do not use NSNumber initializers with NSDecimalNumber. For example 72.82 gives rounded value
                            NSDecimal decimal;
                            NSString *stringDouble = [[NSString alloc] initWithFormat:@"%f", d];
                            NSScanner *theScanner = [[NSScanner alloc] initWithString:stringDouble];
                            [stringDouble release];
                            [theScanner scanDecimal:&decimal];
                            [theScanner release];
                            NSDecimalNumber* number = [[NSDecimalNumber alloc]initWithDecimal:decimal];
                            //[row.columnData addObject:[num stringValue]];
                            [row.columnData addObject:number];
                            [number release];
                        }
                            break;
                        default:
                        {
                            char * text = (char*)sqlite3_column_text(statement,x);
                            if (text){
                                NSString* stringValue = [[NSString alloc] initWithUTF8String:text];
                                [row.columnData addObject:stringValue ? stringValue : [NSNull null]];
                                [stringValue release];
                            }
                            else{
                                [row.columnData addObject:[NSNull null]];
                            }
                        }
                            break;
                    }

                }
                
                [result addRow:row];
                [row release];
            }
        }
    }];
    return result;
}

- (BOOL)executeUpdate:(NSString*)sql parametersList:(id)parametersList {
#ifdef EGODatabaseRunQueryInBackgoundForMainThread
    if ([NSThread isMainThread]){
         [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseStartExecutingMainThreadNotification object:self userInfo:nil];
        __block BOOL result = FALSE;
        __block BOOL executing = TRUE;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            result = [self executeUpdate:sql parametersList:parametersList];
            executing = FALSE;
        });
        while (executing){
            [[NSRunLoop currentRunLoop] runMode:NSEGOCustomRunLoopMode beforeDate:[NSDate distantFuture]];
        }
         [[NSNotificationCenter defaultCenter] postNotificationName:EGODatabaseEndExecutingMainThreadNotification object:self userInfo:nil];
        return result;
    }
#endif
    [self beginExecute];
    int returnCode = 0;
    EGOStatement* statement = NULL;
    @try{
        if(![self open]) {
            return NO;
        }
        
        EGOStatement *cachedStmt = NULL;
        
        if (shouldCacheStatements) {
            cachedStmt = [self cachedStatementForQuery:sql];
            if (cachedStmt){
                if (![cachedStmt reset]){
                    [self setCachedStatement:nil forQuery:sql];
                    cachedStmt = nil;
                }
                else{
                    statement = [cachedStmt retain];
                }
            }
        }
        if (!statement){
            statement = [[EGOStatement alloc]initWithDatabase:self stmt:sql];
            if (!statement){
                return NO;
            }
        }
        if (parametersList) {
            if (![statement bind:parametersList]) {
                EGODBDebugLog(kBindFailedMsg, sql);
                return NO;
            }		
        }
        
        returnCode = [statement execute];
        
        if (shouldCacheStatements){
            if (![statement reset]){
                if (cachedStmt){
                    [self setCachedStatement:nil forQuery:sql]; 
                }
            }
            else{
                if (!cachedStmt) {
                    [self setCachedStatement:statement forQuery:sql];
                }
                else {
                    cachedStmt.useCount = cachedStmt.useCount + 1;
                }
            }
        }
    }
    @finally {
        [statement release];
        [self endExecute];
    }
	
	return (returnCode == SQLITE_OK || returnCode>=SQLITE_ROW);
}

#pragma mark --
#pragma mark Factory and inits

+ (id)databaseWithPath:(NSString*)aPath {
	return [[[[self class] alloc] initWithPath:aPath] autorelease];
}

- (id)initWithPath:(NSString*)aPath {
	if((self = [super init])) {
		databasePath = [aPath retain];
		//executeLock = [[NSLock alloc] init];
        //spinlock = OS_SPINLOCK_INIT;
        _lock = OS_UNFAIR_LOCK_INIT;
	}
	
	return self;
}

#pragma mark --
#pragma mark Request with Query

- (EGODatabaseRequest*)requestWithQueryAndParameters:(NSString*)sql, ... {
	return [self requestWithQuery:sql parameters:VAToArray(sql)];
}

- (EGODatabaseRequest*)requestWithQuery:(NSString*)sql {
	return [self requestWithQuery:sql parameters:nil];
}

- (EGODatabaseRequest*)requestWithQuery:(NSString*)sql parameters:(NSArray*)parameters {
	EGODatabaseRequest* request = [[[EGODatabaseRequest alloc] initWithQuery:sql parameters:parameters] autorelease];
	
	request.database = self;
	request.requestKind = EGODatabaseSelectRequest;
	
	return request;
}

- (NSInteger)changedRowsCount{
    return sqlite3_changes(_handle);
}

#pragma mark --
#pragma mark Request with Update

- (EGODatabaseRequest*)requestWithUpdateAndParameters:(NSString*)sql, ... {
	return [self requestWithUpdate:sql parameters:VAToArray(sql)];
}

- (EGODatabaseRequest*)requestWithUpdate:(NSString*)sql {
	return [self requestWithUpdate:sql parameters:nil];
}

- (EGODatabaseRequest*)requestWithUpdate:(NSString*)sql parameters:(NSArray*)parameters {
	EGODatabaseRequest* request = [[[EGODatabaseRequest alloc] initWithQuery:sql parameters:parameters] autorelease];
	
	request.database = self;
	request.requestKind = EGODatabaseUpdateRequest;
	
	return request;
}

#pragma mark --
#pragma mark open and close

- (BOOL)open {
    if (_shuttingDown) return NO;
	if(_isOpened) return YES;
	//OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
    if(_isOpened) return YES;
	int err = sqlite3_open_v2([databasePath fileSystemRepresentation], &_handle, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);
	if(err != SQLITE_OK || !_handle) {
        _handle = nil;
		EGODBDebugLog(@"[EGODatabase] Error opening DB: %d", err);
		return NO;
	}
	sqlite3_busy_handler(_handle, &databaseBusyHandler, self);
    _isOpened = YES;
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
	return YES;
}

-(BOOL)beginUsingHandle{
    os_unfair_lock_lock(&_lock);
    //OSSpinLockLock(&spinlock);
    return _handle!=NULL;
}

-(void)endUsingHandle{
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
}


static int databaseBusyHandler(void *f, int count) {
    [_runningCondition lock];
    [_runningCondition waitUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
    [_runningCondition unlock];
    return 1;
}

- (void)close {
    _shuttingDown = YES;
    [self clearCachedStatements];
    if(!_isOpened) return;
    //OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
        if(!_isOpened) return;
        sqlite3_interrupt(_handle);
        sqlite3_close_v2(_handle);
        _isOpened = NO;
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
}

-(void)wal_checkpoint{
    if ([self beginUsingHandle]){
        sqlite3_wal_checkpoint_v2(_handle, NULL, SQLITE_CHECKPOINT_PASSIVE, NULL, NULL);
    }
    [self endUsingHandle];
}

#pragma mark -
#pragma mark Updates

- (BOOL)executeUpdateWithParameters:(NSString*)sql,... {
	return [self executeUpdate:sql parameters:VAToArray(sql)];
}

- (BOOL)executeUpdate:(NSString*)sql {
	return [self executeUpdate:sql parameters:nil];
}

- (BOOL)executeUpdate:(NSString*)sql parameters:(NSArray*)parameters{
	return [self executeUpdate:sql parametersList:parameters];
}
- (BOOL)executeUpdate:(NSString*)sql namedParameters:(NSDictionary*)parameters{
	return [self executeUpdate:sql parametersList:parameters];
}

#pragma mark --
#pragma mark Query

- (EGODatabaseResult*)executeQueryWithParameters:(NSString*)sql,... {
	return [self executeQuery:sql parameters:VAToArray(sql)];
}

- (EGODatabaseResult*)executeQuery:(NSString*)sql {
	return [self executeQuery:sql parameters:nil];
}

- (EGODatabaseResult*)executeQuery:(NSString*)sql parameters:(NSArray*)parameters
{
	return [self executeQuery:sql parametersList:parameters];
}

- (EGODatabaseResult*)executeQuery:(NSString*)sql namedParameters:(NSDictionary*)namedParameters {
	return [self executeQuery:sql parametersList:namedParameters];
}

- (NSInteger)lastInsertRowid{
    return sqlite3_last_insert_rowid(_handle);
}

-(BOOL)alive{
    return _isOpened && !_isShuttingDown;
}

#pragma mark -
#pragma mark transactions

NSCondition* _runningCondition;

+(void)initialize{
    _runningCondition = [[NSCondition alloc]init];
#ifdef EGODatabaseRunQueryInBackgoundForMainThread
    CFRunLoopAddCommonMode(CFRunLoopGetMain(), (CFStringRef)NSEGOCustomRunLoopMode);
#endif
}

- (BOOL) rollback {
    return [self executeUpdate:@"ROLLBACK TRANSACTION;"];
}

- (BOOL) commit {
    return  [self executeUpdate:@"COMMIT TRANSACTION;"];
}

- (BOOL) beginDeferredTransaction {
    return  [self executeUpdate:@"BEGIN DEFERRED TRANSACTION;"];
}

- (BOOL) beginTransaction {
    return  [self executeUpdate:@"BEGIN EXCLUSIVE TRANSACTION;"];
}

-(BOOL)inTransaction{
    return _inTransaction;
}

-(void)setInTransaction:(BOOL)value{
    _inTransaction = value;
}

#pragma mark --
#pragma mark Errors

- (NSString*)lastErrorMessage {
	if([self hadError]) {
		return @(sqlite3_errmsg(_handle));
	} else {
		return nil;
	}
}

- (BOOL)hadError {
    SQLITE_API int code = [self lastErrorCode];
	return code!= SQLITE_OK && code!=SQLITE_DONE &&  code!=SQLITE_ROW;
}

- (int)lastErrorCode {
	return sqlite3_errcode(_handle);
}

-(BOOL)isInLock {
    return NO;
}

#pragma mark -

- (void)dealloc {
	[self close];
	[databasePath release];
    [cachedStatements release];
	[super dealloc];
}

@end
