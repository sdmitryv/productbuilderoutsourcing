//
//  EGODatabaseRow.h
//  EGODatabase
//
//  Created by Shaun Harrison on 3/6/09.
//  Copyright (c) 2009 enormego
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

#define ISNULL(value) (!value || value == [NSNull null])
#define ISNULL_OR_EMPTY(value) (ISNULL(value) || ([value isKindOfClass:[NSString class]] && !((NSString*)value).length))

@class EGODatabaseResult;
@interface EGODatabaseRow : NSObject {
@private
	NSMutableArray* columnData;
	EGODatabaseResult* result;
}

- (id)initWithDatabaseResult:(EGODatabaseResult*)aResult;

- (id)objectForColumn:(NSString*)columnName;
- (id)objectForColumnIndex:(NSUInteger)index;

- (int)intForColumn:(NSString*)columnName;
- (int)intForColumnIndex:(NSInteger)index;

- (long)longForColumn:(NSString*)columnName;
- (long)longForColumnIndex:(NSInteger)index;

- (BOOL)boolForColumn:(NSString*)columnName;
- (BOOL)boolForColumnIndex:(NSInteger)index;

- (double)doubleForColumn:(NSString*)columnName;
- (double)doubleForColumnIndex:(NSInteger)index;

- (NSDecimal)decimalForColumn:(NSString*)columnName;
- (NSDecimal)decimalForColumnIndex:(NSInteger)index;

- (NSDecimalNumber*)decimalNumberForColumn:(NSString*)columnName;
- (NSDecimalNumber*)decimalNumberForColumnIndex:(NSInteger)index;

- (NSString*)stringForColumn:(NSString*)columnName;
- (NSString*)stringForColumnIndex:(NSInteger)index;

- (NSDate*)dateForColumn:(NSString*)columnName;
- (NSDate*)dateForColumnIndex:(NSInteger)index;

- (NSData*)dataForColumn:(NSString*)columnName;
- (NSData*)dataForColumnIndex:(NSInteger)index;

- (NSNumber*)numberForColumn:(NSString*)columnName;
- (NSNumber*)numberForColumnIndex:(NSInteger)index;

- (BPUUID*)UuidForColumn:(NSString*)columnName;
- (BPUUID*)UuidForColumnIndex:(NSInteger)index;

@property(readonly) NSMutableArray* columnData;
@end

//used to read data from SVS service
@interface EGODatabaseRowDictionary : EGODatabaseRow{
    NSDictionary* dictionary;
}
-(id)initWithDictionary:(NSDictionary *)aDictionary;
@end
