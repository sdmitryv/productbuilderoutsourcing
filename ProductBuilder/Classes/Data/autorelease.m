//
//  autorelease.m
//  ProductBuilder
//
//  Created by Valera on 9/9/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#import "autorelease.h"

void release_id(id *n) 
{
	[(*n) release];
	*n = nil;
}
