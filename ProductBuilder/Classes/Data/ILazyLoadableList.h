//
//  ILazyLoading.h
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SqlCommand.h"

@protocol ILazyItem<NSObject>
-(NSNumber*)rowid;
@end

@protocol ILazyLoadableList <NSObject>

-(SqlCommand*) command;
-(void)refresh;
+(NSObject<ILazyItem>*)getItem:(EGODatabaseRow*)row;
-(SqlFilterCondition*) filterCondition;
-(void) setFilterCondition:(SqlFilterCondition*)condition;
-(NSArray*) sortOrderList;
-(void)setSortOrderList:(NSArray*)list;
@end
