//
//  CustomRequiredFieldsManager.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/16/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomRequiredField.h"

@protocol CustomRequiredFieldsManagerDelegate<NSObject>

- (BOOL)isEmptyField:(NSString *)fieldName;
- (void)markField:(NSString *)fieldName withSymbol:(NSString *)symbol;

@optional

- (BOOL)isCompany;

@end

@interface CustomRequiredFieldsManager : NSObject

@property (nonatomic, assign) id<CustomRequiredFieldsManagerDelegate> delegate;
@property (nonatomic, readonly) NSArray * requiredFieldsArray;

- (id)initForArea:(CustomRequiredFieldArea)area delegate:(id<CustomRequiredFieldsManagerDelegate>) delegate;
- (NSString *)checkRequiredFields:(NSArray *)fieldNames;
- (void)markRequiredFields:(NSArray *)fieldNames;

@end
