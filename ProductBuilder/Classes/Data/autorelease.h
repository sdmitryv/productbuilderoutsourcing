
void release_id(id *n) ;

#define auto_released __attribute__((cleanup(release_id)))
#define CONCAT(X,Y) X##Y
#define GENLOCAL(X) CONCAT(__local__,X)
#define with_autopool auto_released NSAutoreleasePool * GENLOCAL(__LINE__) = [[NSAutoreleasePool alloc] init]
//#define with_autopool auto_released NSAutoreleasePool * $_autoreleasePool##__LINE__  = [[NSAutoreleasePool alloc] init]

#define VAToNSArray(firstarg) ({\
NSMutableArray* valistArray = [NSMutableArray array];\
va_list arguments;\
va_start(arguments, firstarg);\
for (id obj = firstarg; obj != nil; obj = va_arg(arguments, id))\
[valistArray addObject:obj];\
va_end(arguments);\
valistArray;\
})


