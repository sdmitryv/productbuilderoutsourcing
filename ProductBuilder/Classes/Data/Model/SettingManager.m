//
//  SettingManager.m
//  CloudworksPOS
//
//  Created by valera on 9/5/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "SettingManager.h"
#import "DecimalHelper.h"
#import "Location.h"
#import "BinaryStorageData.h"

#define DEFAULT_TEMP_UPC 99

@implementation SettingManager

#pragma mark - General

-(NSInteger)systemDecimals{
    return [Setting intForKey:@"Area: System.Decimals" type:SettingTypeCompany defaultValue:2];
}

-(NSInteger)deviceHistoryPurgeLimit {
    return [Setting intForKey:@"Area: System.DeviceHistoryPurgeLimit" type:SettingTypeLocation defaultValue:0];
}

-(NSString *)defaultCurrencyLabel{
    return [Setting stringForKey:@"Area: System.DefaultCurrencyLabel" type:SettingTypeCompany defaultValue:@"Currency"];
}

-(NSString *)defaultCustomerPhone1Label{
    NSString* val = [Setting stringForKey:@"Area: Customer.Phone1Label" type:SettingTypeCompany defaultValue:@"Phone 1"];
    
    if (val.length == 0) {
        val = @"Phone 1";
    }
    
    return val;
}
-(NSString *)defaultCustomerPhone2Label{
    NSString* val =  [Setting stringForKey:@"Area: Customer.Phone2Label" type:SettingTypeCompany defaultValue:@"Phone 2"];
    
    if (val.length == 0) {
        val = @"Phone 2";
    }
    
    return val;
}
-(NSString *)defaultCustomerPhone3Label{
    NSString* val = [Setting stringForKey:@"Area: Customer.Phone3Label" type:SettingTypeCompany defaultValue:@"Phone 3"];
    
    if (val.length == 0) {
        val = @"Phone 3";
    }
    
    return val;
}
-(NSString *)defaultCustomerPhone4Label{
    NSString* val =  [Setting stringForKey:@"Area: Customer.Phone4Label" type:SettingTypeCompany defaultValue:@"Phone 4"];
    
    if (val.length == 0) {
        val = @"Phone 4";
    }
    
    return val;
}
-(NSString *)defaultCustomerShipToPhoneLabel{
    NSString* val =   [Setting stringForKey:@"Area: Customer.ShipPhoneLabel" type:SettingTypeCompany defaultValue:NSLocalizedString(@"PHONE_TITLE", nil)];
    
    if (val.length == 0) {
        val = @"Phone No";
    }
    
    return val;
}


-(BOOL)clearEmptyBlock { return [Setting boolForKey:@"Area: Print.ClearEmptyBlock" type:SettingTypeLocation defaultValue:NO]; }
-(BOOL)bigPageSaze {     return [Setting boolForKey:@"Area: Print.BigPageSaze"     type:SettingTypeLocation defaultValue:NO]; }


-(BOOL) autoInsertAfterPost{
    return [Setting boolForKey:@"Area: Receipts.AutoInsertAfterPost" type:SettingTypeWorkstation defaultValue:FALSE];
}

-(BOOL) giftCardsServiceUse{
    BOOL local = [Setting boolForKey:@"Area: SVS.UseSVSLocation" type:SettingTypeLocation defaultValue:FALSE];
    return local ? [Setting boolForKey:@"Area: SVS.UseSVSGiftCards" type:SettingTypeLocation defaultValue:FALSE] :
                   [Setting boolForKey:@"Area: SVS.UseSVSGiftCards_cmp" type:SettingTypeCompany defaultValue:FALSE];
}

-(NSString *) svsAccountUserName{
    BOOL local = [Setting boolForKey:@"Area: SVS.UseSVSLocation" type:SettingTypeLocation defaultValue:FALSE];
    NSString * result = nil;
    if (local) {
        result = [Setting stringForKey:@"Area: SVS.AccountName" type:SettingTypeLocation];
    }
    if ([result length] == 0) {
        result = [Setting stringForKey:@"Area: SVS.AccountName_cmp" type:SettingTypeCompany];
    }
    return result;
}

-(NSString *) svsAccountPassword{
    BOOL local = [Setting boolForKey:@"Area: SVS.UseSVSLocation" type:SettingTypeLocation defaultValue:FALSE];
    NSString * result = nil;
    if (local) {
        result = [Setting stringForKey:@"Area: SVS.AccountPwd" type:SettingTypeLocation];
    }
    if ([result length] == 0) {
        result = [Setting stringForKey:@"Area: SVS.AccountPwd_cmp" type:SettingTypeCompany];
    }
    return result;
}

//-(NSString *) svsAuthUrl {
//    return [Setting stringForKey:@"Area: SVS.AuthUrl" type:SettingTypeCompany];
//}

-(NSString *) svsServiceUrl {
    return [Setting stringForKey:@"Area: SVS.ServiceUrl" type:SettingTypeCompany];
}

-(NSString *) svs2ServiceUrl {
    return [Setting stringForKey:@"Area: SVS2.ServiceUrl" type:SettingTypeCompany];
}

-(NSString *) gaeLoggerServiceUrl {
    return [Setting stringForKey:@"Area: System.GaeLoggerServiceUrl" type:SettingTypeCompany];
}

-(NSDecimalNumber*)settingMaxSalesDiscount{
    NSString* setting = [Setting stringForKey:@"Area: Receipt.MaxSalesDiscount" type:SettingTypeLocation];
    return setting ? [NSDecimalNumber decimalNumberWithDecimal:[setting decimalValue]] : nil;
}

-(BOOL)sendCCTrack2IfValid{
    return [Setting boolForKey:@"Area: Sales.SendCCTrack2IfValid" type:SettingTypeLocation defaultValue:TRUE];
}

-(BOOL)SRtoSvsOffline{
    return [Setting boolForKey:@"Area: Sales.SRtoSvsOffline" type:SettingTypeCompany defaultValue:NO];
}

-(NSString *)receiptTrackingMerchantId {
    return [Setting stringForKey:@"Area: System.MerchantID" type:SettingTypeCompany];
}

-(BOOL)requireCustomerForAllSales {
    return [Setting boolForKey:@"Area: Sales.CustomerRequired" type:SettingTypeLocation defaultValue:FALSE];
}

-(BOOL)requireCustomerForReturn {
    return [Setting boolForKey:@"Area: Receipts.RequireCustomerForReturn" type:SettingTypeLocation defaultValue:FALSE];
}

-(BOOL)requireDiscountOverrideCode{
    return [Setting boolForKey:@"Area: Receipts.RequireDiscOverrideCode" type:SettingTypeLocation defaultValue:FALSE];
}

#pragma mark intellivative payment

-(BOOL)paymentIntellivativeIsInTestMode{
    return [Setting boolForKey:@"Area: Payment.Imtellivative.IsInTestMode" type:SettingTypeLocation defaultValue:FALSE];
}

-(NSString*)paymentIntellivativeURL{
    return [self paymentIntellivativeIsInTestMode] ? [self paymentIntellivativeTestURL] : [self paymentIntellivativeProductionURL];
}

-(NSString*)paymentIntellivativeTestURL{
    return @"https://apiint.paymentsite.com/UniversalAPI/postXML";
    return [Setting stringForKey:@"Area: Payment.Imtellivative.TestURL" type:SettingTypeCompany defaultValue:nil/*@"http://apiint.intellivative.net:8080/UniversalAPI/postXML"*/];
}

-(NSString*)paymentIntellivativeProductionURL{
/*
    return [Setting stringForKey:@"Area: Payment.Imtellivative.ProductionURL" type:SettingTypeCompany defaultValue:
            //@"https://api.intellivative.net/UniversalAPI/postXML"
            @"https://api.paymentsite.com/UniversalAPI/postXML"
            ];
*/
    return [Setting stringForKey:@"Area: Payment.Imtellivative.ProductionURL" type:SettingTypeCompany defaultValue:nil/*@"https://api.intellivative.net/UniversalAPI/postXML"*/];
}

-(NSString*)paymentIntellivativeApiURL{
    return [self paymentIntellivativeIsInTestMode] ? [self paymentIntellivativeTestApiURL] : [self paymentIntellivativeProductionApiURL];
}

-(NSString*)paymentIntellivativeTestApiURL{
    return [Setting stringForKey:@"Area: Payment.Intellivative.TestApiURL" type:SettingTypeCompany defaultValue:@"https://apiint.paymentsite.com/UniversalAPI/postAPI"];
}

-(NSString*)paymentIntellivativeProductionApiURL{
    return [Setting stringForKey:@"Area: Payment.Imtellivative.ProductionApiURL" type:SettingTypeCompany defaultValue:@"https://api.paymentsite.com/UniversalAPI/postAPI"];
}

-(BOOL)paymentIntellivativeWriteLogs{
    return [Setting boolForKey:@"Area: Payment.Intellivative.WriteLogs" type:SettingTypeLocation defaultValue:FALSE];
}

-(BOOL)paymentIntellivativeWriteFullLog{
    return [Setting boolForKey:@"Area: Payment.Intellivative.FullLog" type:SettingTypeLocation defaultValue:FALSE];
}

-(BOOL)paymentIntellivativeAutomaticTimeoutRequest{
    return [Setting boolForKey:@"Area: Payment.Intellivative.AutomaticTimeoutRequest" type:SettingTypeCompany defaultValue:TRUE];
}

#pragma Credit Card Payment

-(BOOL)oneCreditCardPerSale {
    BOOL local = [Setting boolForKey:@"Area: Sales.UseLocationBasedPaymentOptions" type:SettingTypeLocation defaultValue:FALSE];
    return local ? [Setting boolForKey:@"Area: Sales.OneCreditCardPerSale" type:SettingTypeLocation defaultValue:FALSE] :
    [Setting boolForKey:@"Area: Sales.CompanyOneCreditCardPerSale" type:SettingTypeCompany defaultValue:FALSE];
}

-(NSString*)iAceptaScheme {
    return [Setting stringForKey:@"Area: Sales.iAceptaScheme" type:SettingTypeLocation defaultValue:@"iacepta2"];
}

- (NSString*)cieloMobileScheme {
    return [Setting stringForKey:@"Area: Sales.CieloScheme" type:SettingTypeLocation defaultValue:@"cielomobile"];
}

-(NSString*)billpocketScheme {
    return [Setting stringForKey:@"Area: Sales.BillpocketScheme" type:SettingTypeLocation defaultValue:@"billpocket"];
}

-(NSString*)rsaCertificateName {
    return [Setting stringForKey:@"Area: Certificate.OfflineCCPaymentEncryption" type:SettingTypeCompany defaultValue:@"OfflineCCPaymentCertificate"];
}

-(BOOL)preventNegativeDiscount {
    //BOOL local = [Setting boolForKey:@"Area: Sales.UseLocationBasedPaymentOptions" type:SettingTypeLocation defaultValue:FALSE];
    //return local ? [Setting boolForKey:@"Area: Sales.PreventNegativeDiscount" type:SettingTypeLocation defaultValue:FALSE] :
    //[Setting boolForKey:@"Area: Sales.CompanyPreventNegativeDiscount" type:SettingTypeCompany defaultValue:FALSE];
    return [Setting boolForKey:@"Area: Sales.PreventNegativeDiscount" type:SettingTypeLocation defaultValue:FALSE];
}

#pragma mark - PaymentMethod settings

-(NSString*)voiceAuthPhoneCompany {
    return [Setting stringForKey:@"Area: Sales.VoiceAuthPhoneCompany" type:SettingTypeCompany defaultValue:@"No Voice Authorization phone"];
}

-(NSString*)voiceAuthPhoneLocation {
    return [Setting stringForKey:@"Area: Sales.VoiceAuthPhoneLocation" type:SettingTypeLocation defaultValue:nil];
}

-(NSString*)voiceAuthTextCompany {
    NSString* voiceAuthTextCompanyValue = [Setting stringForKey:@"Area: Sales.VoiceAuthTextCompany" type:SettingTypeCompany defaultValue:@"No Voice Authorization text"];
    NSMutableString* voiceAuthTextCompany = voiceAuthTextCompanyValue ? [NSMutableString stringWithString:voiceAuthTextCompanyValue] : [NSMutableString string];
    for (NSString* key in @[@"Area: Sales.VoiceAuthText2Company",@"Area: Sales.VoiceAuthText3Company",@"Area: Sales.VoiceAuthText4Company"]){
        NSString* voiceAuthPart = [Setting stringForKey:key type:SettingTypeCompany defaultValue:nil];
        if (voiceAuthPart){
            if (voiceAuthTextCompany.length){
                [voiceAuthTextCompany appendString:@"\n"];    
            }
            [voiceAuthTextCompany appendString:voiceAuthPart];
        }
    }
    return voiceAuthTextCompany;
}

-(NSString*)voiceAuthTextLocation {
    NSString* voiceAuthTextLocationValue = [Setting stringForKey:@"Area: Sales.VoiceAuthTextLocation" type:SettingTypeLocation defaultValue:nil];
    NSMutableString* voiceAuthTextLocation = voiceAuthTextLocationValue ? [NSMutableString stringWithString:voiceAuthTextLocationValue] : [NSMutableString string];
    for (NSString* key in @[@"Area: Sales.VoiceAuthText2Location",@"Area: Sales.VoiceAuthText3Location",@"Area: Sales.VoiceAuthText4Location"]){
        NSString* voiceAuthPart = [Setting stringForKey:key type:SettingTypeLocation defaultValue:nil];
        if (voiceAuthPart){
            if (voiceAuthTextLocation.length){
                [voiceAuthTextLocation appendString:@"\n"];    
            }
            [voiceAuthTextLocation appendString:voiceAuthPart];
        }
    }
    return voiceAuthTextLocation.length ? voiceAuthTextLocation : nil;
}

#pragma mark - Rich Content Manager Service

-(NSString *)rcmServiceURL {
    return [Setting stringForKey:@"Area: RCM.ServiceUrl" type:SettingTypeCompany];
}

#pragma mark - Static PLU

-(NSInteger)salesRentalFeesPLU {
    return [Setting intForKey:@"Area: Sales.RentalPLU" type:SettingTypeLocation defaultValue:0];
}

-(NSInteger)salesRentalQtyAdjPLU {
    return [Setting intForKey:@"Area: Sales.RentalQtyAdjPLU" type:SettingTypeLocation defaultValue:0];
}

-(NSInteger)storeCreditAdjustmentPLU {
    return [Setting intForKey:@"Area: Sales.StoreCreditAdjustmentPLU" type:SettingTypeCompany defaultValue:0];
}

-(NSInteger)giftCardAdjustmentPLU {
    return [Setting intForKey:@"Area: Sales.GiftCardAdjustmentPLU" type:SettingTypeCompany defaultValue:0];
}

-(NSInteger)tempItemPLU {
    NSInteger tryFromSettings = [Setting intForKey:@"Area: Sales.TempItemPLU" type:SettingTypeCompany defaultValue:99];
    return tryFromSettings > 0 ? tryFromSettings : DEFAULT_TEMP_UPC;
}

-(NSInteger)loyaltyRewardsAdjustmentPLU {
    BOOL local = [Setting boolForKey:@"Area: SVS.UseSVSLocation" type:SettingTypeLocation defaultValue:FALSE];
    return local ? [Setting intForKey:@"Area: SVS.PtsAdjItemPLU" type:SettingTypeLocation defaultValue:0] :
    [Setting intForKey:@"Area: SVS.PtsAdjItemPLU_cmp" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)isDemoMode{
    return [Setting boolForKey:@"Area: System.DemoMode" type:SettingTypeLocation defaultValue:FALSE];
}

-(PromptForReceiptLoginType)promptForTransactionLoginForSales{
    return [Setting intForKey:@"Area: System.PromptForTransactionLogin_Sales" type:SettingTypeLocation defaultValue:FALSE];
}

#pragma mark - House Charge

-(NSInteger)houseChargedAdjustmentPLU {
    return [Setting intForKey:@"Area: Payment.HouseCharge.AdjItemPLU" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)houseChargeEnabled {
    
    Setting *local = [Setting getSettingForKey:@"Area: System.HouseChargeEnabled" type:SettingTypeLocation];
    return local ? local.value.boolValue : [Setting boolForKey:@"Area: System.HouseChargeEnabled" type:SettingTypeCompany defaultValue:NO];
}

-(HouseChargeType)houseChargeType {
    
    Setting *local = [Setting getSettingForKey:@"Area: System.HouseAccountCharge" type:SettingTypeLocation];
    return local ? local.value.intValue : [Setting intForKey:@"Area: System.HouseAccountCharge" type:SettingTypeCompany defaultValue:HouseChargeDisabled];
}

-(SalesReceiptTabType)receiptStartingPoint {
    NSString* str=[Setting stringForKey:@"Area: Receipts.NewReceiptStartPoint" type:SettingTypeLocation].lowercaseString;
    if ([str isEqualToString:@"customer"]) {
        return SalesReceiptTabCustomer;
    }
    else if ([str isEqualToString:@"items"]) {
        return SalesReceiptTabItems;
    }
    else if ([str isEqualToString:@"payment"]) {
        return SalesReceiptTabPayment;
    }
    else if ([str isEqualToString:@"ship to"]) {
        return SalesReceiptTabShipto;
    }
    else if ([str isEqualToString:@"info"]) {
        return SalesReceiptTabInfo;
    }
    else if ([str isEqualToString:@"order"]) {
        return SalesReceiptTabOrder;
    }
    else if ([str isEqualToString:@"returns"]) {
        return SalesReceiptTabReturn;
    }
    else if ([str isEqualToString:@"giftdeposit"]) {
        return  SalesReceiptTabGiftsDeposits;
    }
    else if ([str isEqualToString:@"presets"]) {
        return  SalesReceiptTabPreset;
    }
    else if ([str isEqualToString:@"registry"]) {
        return  SalesReceiptTabRegistry;
    }
    else if ([str isEqualToString:@"sessions"]) {
        return  SalesReceiptTabSessions;
    }
    else if ([str isEqualToString:@"summary"]) {
        return  SalesReceiptTabSummary;
    }
    else
        return SalesReceiptTabCustomer;
}

-(SalesOrderTabType)orderStartingPoint {
    //there is no such setting on server, it always takes default value
    return (SalesOrderTabType)[Setting intForKey:@"Area: Order.NewOrderStartPoint" type:SettingTypeWorkstation defaultValue:SalesOrderTabCustomer];
}

-(BOOL)alwaysOpenCashDrawer {
    return [Setting boolForKey:@"Area: Receipts.AlwaysOpenCashDrawer" type:SettingTypeLocation defaultValue:NO];
} 

-(NSDecimal)cashDropWarningThreshold {
    Setting * setting;
    if ([Setting boolForKey:@"Area: CashDrawer.CashDropWarningThresholdUseLocal" type:SettingTypeLocation defaultValue:NO]) {
        setting = [Setting getSettingForKey:@"Area: CashDrawer.CashDropWarningThresholdLocal" type:SettingTypeLocation];
    }
    else {
        setting = [Setting getSettingForKey:@"Area: CashDrawer.CashDropWarningThreshold" type:SettingTypeCompany];
    }
    return setting != nil ? CPDecimalFromString(setting.value) : D0;
}

-(BOOL)warnIfNotEnoughFunds {
    return [Setting boolForKey:@"Area: CashDrawer.WarnNotEnoughFundsForChange" type:SettingTypeCompany defaultValue:NO];
}


-(NSDecimal)cashExceedThreshold {
    return [Setting decimalForKey:@"Area: Sales.ThresholdSalesPayment" type:SettingTypeLocation defaultValue:D0];
}

-(NSString *)cashExceedThresholdWarning {
    return [Setting stringForKey:@"Area: Sales.ThresholdMessage" type:SettingTypeLocation defaultValue:NSLocalizedString(@"SALE_RECEIPT_CASH_EXCEED_AMOUNT_MESSAGE", nil)];
}

-(BOOL)registryEnabled {
    return [Setting boolForKey:@"Area: Receipts.EnableRegistry" type:SettingTypeCompany defaultValue:NO];
}
-(BOOL)sessionsEnabled{
    return YES;
}
-(BOOL)useLocalSalesReceiptTabs{
    BOOL b = [Setting boolForKey:@"Area: Sales.UseLocalSalesReceiptTabs" type:SettingTypeLocation defaultValue:NO];
    return b;
}
-(BOOL)useLocalSalesOrderTabs{
    BOOL b = [Setting boolForKey:@"Area: Sales.UseLocalSalesOrderTabs" type:SettingTypeLocation defaultValue:NO];
    return b;
}
-(BOOL)passHeldReceiptToCTS {
    return [Setting boolForKey:@"Area: Receipts.PassHeldToCTS" type:SettingTypeLocation defaultValue:NO];
}
-(BOOL)onlineOMSShipTo {
    return [Setting boolForKey:@"Area: Sales.OnlineOMSShipTo" type:SettingTypeCompany defaultValue:NO];
}
-(BOOL)requireCustomerOnSalesReceiptWithShipItems {
    return [Setting boolForKey:@"Area: Receipts.ReqCustShipItems" type:SettingTypeCompany defaultValue:NO];
}


-(BOOL)doNotAllowShipItemWithQtyNotEqual1 {
    return [Setting boolForKey:@"Area: Sales.DoNotAllowShipItemWithQtyNotEqual1" type:SettingTypeCompany defaultValue:NO];
}
-(NSInteger)onlineOMSOrderExpiresInMin {
    return [Setting intForKey:@"Area: Sales.OnlineOMSOrderExpiresMin" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)doNotAllowShipToWithNoShipItem {
    return [Setting boolForKey:@"Area: Sales.DoNotAllowShipToWithNoShipItem" type:SettingTypeCompany defaultValue:NO];
}

-(NSMutableArray*)receiptVisibleTabsArray{
    NSString* string=[Setting stringForKey:@"Area: Sales.SalesReceiptVisibleTabs" type:self.useLocalSalesReceiptTabs? SettingTypeLocation:SettingTypeCompany];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""].lowercaseString;
    const NSDictionary * visibleTabsArray = @{@"customer":   @(SalesReceiptTabCustomer),
                                              @"items":      @(SalesReceiptTabItems),
                                              @"payment":    @(SalesReceiptTabPayment),
                                              @"order":      @(SalesReceiptTabOrder),
                                              @"repair":     @(SalesReceiptTabRepair),
                                              @"wishlist":   @(SalesReceiptTabWishlist),
                                              @"wish list":  @(SalesReceiptTabWishlist),
                                              @"layaway":    @(SalesReceiptTabLayaway),
                                              @"rental":     @(SalesReceiptTabRental),
                                              @"delivery":   @(SalesReceiptTabDelivery),
                                              @"ship to":    @(SalesReceiptTabShipto),
                                              @"shipto":     @(SalesReceiptTabShipto),
                                              @"billing":    @(SalesReceiptTabBilling),
                                              @"info":       @(SalesReceiptTabInfo),
                                              @"returns":    @(SalesReceiptTabReturn),
                                              @"trades":     @(SalesReceiptTabTrade),
                                              @"giftdeposit":@(SalesReceiptTabGiftsDeposits),
                                              @"presets":    @(SalesReceiptTabPreset),
                                              @"sessions":   @(SalesReceiptTabSessions),
                                              @"registry":   @(SalesReceiptTabRegistry),
                                              @"accounts":   @(SalesReceiptTabAccounts),
                                              @"summary":    @(SalesReceiptTabSummary)
                                              };
    NSMutableArray *array=[[[NSMutableArray alloc]init]autorelease];
    for (NSString* str in [string componentsSeparatedByString:@","] ) {
        NSObject * obj = visibleTabsArray[str];
        if (obj != nil) {
            [array addObject:obj];
        }
    }
   return array;
}

-(NSMutableArray*)orderVisibleTabsArray{
    NSString* string=[Setting stringForKey:@"Area: Sales.SalesOrderVisibleTabs" type:self.useLocalSalesOrderTabs ? SettingTypeLocation : SettingTypeCompany defaultValue:@"customer,items,shipto,instructions,notes,activity,orderinfo"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""].lowercaseString;
    const NSDictionary * visibleTabsArray = @{@"customer":      @(SalesOrderTabCustomer),
                                              @"items":         @(SalesOrderTabItems),
                                              @"shipto":        @(SalesOrderTabShipTo),
                                              @"instructions":  @(SalesOrderTabInstructions),
                                              @"notes":         @(SalesOrderTabNotes),
                                              @"activity":      @(SalesOrderTabActivity),
                                              @"orderinfo":     @(SalesOrderTabOrderInfo)
                                              };
    NSMutableArray *array=[[[NSMutableArray alloc]init]autorelease];
    for (NSString* str in [string componentsSeparatedByString:@","] ) {
        NSObject * obj = visibleTabsArray[str];
        if (obj != nil) {
            [array addObject:obj];
        }
    }
    return array;
}



-(RequireFieldsType)returnItemRequireFields{
    
    RequireFieldsType res = RequireNone;
    
    EGODatabase *db = [DataManager instance].currentDatabase;
    EGODatabaseResult* result = [db executeQuery:@"SELECT FieldName FROM SalesReturnRequiredField WHERE Required = 1"];
    for(EGODatabaseRow* row in result) {
    
        NSString *str = [row stringForColumnIndex:0];
        if ([str.lowercaseString isEqualToString:@"associate"])
            res = res | RequireAssociateField;
        else if ([str.lowercaseString isEqualToString:@"location"])
            res = res | RequireLocationField;
        else if ([str.lowercaseString isEqualToString:@"receiptnum"])
            res = res | RequireReceiptNumField;
        else if ([str.lowercaseString isEqualToString:@"receiptdate"])
            res = res | RequireReceiptDateField;
    }
    
    return res;
}


-(BOOL)showSrchBarcodeReturnTab {
    
    return [Setting boolForKey:@"Area: Sales.ShowSrchBarcodeReturnTab" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)useOpenReturnsCustomPanel{
    return [Setting boolForKey:@"Area: Sales.UseOpenReturnsCustomPanel" type:SettingTypeCompany defaultValue:NO];
}

-(NSString *)inventoryItemCostCode {

    return [Setting stringForKey:@"Area: Sales.InventoryItemCostCodeCompany" type:SettingTypeCompany defaultValue:nil];
}



-(BOOL)requireCDCloseBeforePrinting {
    return NO;//[Setting boolForKey:@"Area: System.RecuireCDCloseBeforePrinting" type:SettingTypeLocation defaultValue:NO];
}

-(CustomLookupDisplayType)customLookupDisplayType {
    
    return [Setting intForKey:@"Area: Inventory.CustomLookup.DisplayType" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)requireCustomerInformationForTaxExempt {
    return [Setting boolForKey:@"Area: Receipts.ReqCustTaxExempt" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)allowToChangeTaxAreaForSaleItems{
    return [Setting boolForKey:@"Area: Receipts.AllowChangeTaxAreaSI" type:SettingTypeCompany defaultValue:TRUE];
}

-(BOOL)allowNonZeroChangeDue{
    //todo:rename key to DisableNonZeroChangeDue
    return ![Setting boolForKey:@"Area: Receipts.AllowNonZeroChangeDue" type:SettingTypeCompany defaultValue:FALSE];
}

-(BOOL)allowToChangeReceiptWithPayments{
    return [Setting boolForKey:@"Area: Receipts.AllowToChangeReceiptWithPayments" type:SettingTypeCompany defaultValue:TRUE];//Allow to make changes in sales receipt with payments".
}

-(NSString*)cashPaymentName {

    return [Setting stringForKey:@"Area: Sales.CashName" type:SettingTypeLocation defaultValue:nil];
}

#pragma mark - Token

-(BOOL)tokensEnabled {
    return [Setting boolForKey:@"Area: SVS.EnableTokens" type:SettingTypeCompany defaultValue:FALSE];
}

#pragma mark - Store Credit

-(BOOL)storeCreditEnabled {
    BOOL local = [Setting boolForKey:@"Area: SVS.UseSVSLocation" type:SettingTypeLocation defaultValue:FALSE];
    return local ? [Setting boolForKey:@"Area: SVS.UseSVSStoreCredit" type:SettingTypeLocation defaultValue:FALSE] :
    [Setting boolForKey:@"Area: SVS.UseSVSStoreCredit_cmp" type:SettingTypeCompany defaultValue:FALSE];
}

-(BOOL)promptToUseAvailableStoreCredit{
    return [Setting boolForKey:@"Area: Receipts.PromptToUseAvaiableStoreCredit" type:SettingTypeLocation defaultValue:FALSE];
}

#pragma mark membership


-(BOOL)customerPromptToSellMembership{
    return [Setting boolForKey:@"Area: Customers.PromptToSellMembership" type:SettingTypeLocation defaultValue:TRUE];
}

-(BOOL)customerPromptForMembershipUpgrade{
    return [Setting boolForKey:@"Area: Customers.PromptForMembershipUpgrade" type:SettingTypeLocation defaultValue:TRUE];
}

-(BOOL)customerPromptForMembershipRenewal{
    return [Setting boolForKey:@"Area: Customers.PromptForMembershipRenewal" type:SettingTypeLocation defaultValue:TRUE];
}

-(NSInteger)customerMembershipRenewalNumberOfDays{
    return [Setting intForKey:@"Area: Customers.MembershipRenewalNumberOfDays" type:SettingTypeLocation defaultValue:30];
}

-(BOOL)customerUseLocationBasedMembership{
    return [Setting boolForKey:@"Area: Customers.UseLocationBasedMembership" type:SettingTypeLocation defaultValue:TRUE];
}

-(NSString *)customerMembershipLevel0Label {
    return [Setting stringForKey:@"Area: Customer.MembershipLevelOLabel" type:SettingTypeCompany defaultValue:NSLocalizedString(@"BASIC", nil)];
}

-(NSString *)customerMembershipLevel0Description {
    return [Setting stringForKey:@"Area: Customer.MembershipLevelODescription" type:SettingTypeCompany defaultValue:NSLocalizedString(@"BASIC_MEMBERSHIP_LABEL", nil)];
}

#pragma mark - Customers

-(SettingCheckEmailPhone)customerDuplicateEmailPhone {
    if ([self universalCustomersEnabled]){
        return SettingCheckEmailPhonePrevent;
    }
    NSString* value = [[Setting stringForKey:@"Area: Customers.DuplicateEmailPhone" type:SettingTypeCompany defaultValue:nil] lowercaseString];
    value = [[value componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    if (value){
        if ([value isEqualToString:@"prevent"]){
            return SettingCheckEmailPhonePrevent;
        }
        if ([value isEqualToString:@"warn"]){
            return SettingCheckEmailPhoneWarn;
        }
    }
    return SettingCheckEmailPhoneNoAction;
}

-(BOOL)usePostalCodeToGenarateCountry {
    return [Setting boolForKey:@"Area: Sales.UsePostalCode" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)allowAddCustomerContact{
    return [Setting boolForKey:@"Area: Sales.AllowAddCustomerContact" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - Scheduler

-(BOOL)schedulerFeatureEnabled {
    return [Setting boolForKey:@"Area: Scheduler.EnableSchedulerFeatures" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark TAS
-(NSString *)tasServerLocation {
    
    return [Setting stringForKey:@"Area: Reports.TASAddress" type:SettingTypeCompany defaultValue:@""];
}


-(NSString *)customerHistoryReportName {
    
    return [Setting stringForKey:@"Area: Customer.HistoryReport" type:SettingTypeCompany defaultValue:@""];
}


#pragma mark - Sales Orders
-(BPUUID*)defaultSellFromLocationId {
    NSString *defSellFromLocation=[Setting stringForKey:@"Area: SO.DefaultSellFromLocation" type:SettingTypeLocation defaultValue:nil];
    if (defSellFromLocation.length){
        return [BPUUID UUIDWithString:defSellFromLocation];
    }
    return nil;
}

-(BPUUID*)defaultSaleCreditLocationId {
    NSString *defSaleCreditLocation= [Setting stringForKey:@"Area: SO.DefaultSaleCreditLocation" type:SettingTypeLocation defaultValue:nil];
    if (defSaleCreditLocation.length){
        return [BPUUID UUIDWithString:defSaleCreditLocation];
    }
    return nil;
}

-(NSInteger)holdExpiresAfterNumDays {
    return [Setting intForKey:@"Area: SO.HoldExpirationNumDays" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)enableSalesOrders {
    return [Setting boolForKey:@"Area: SO.EnableSalesOrders" type:SettingTypeCompany defaultValue:YES];
}

-(BOOL)sendSaleAutoApprove {
    return [Setting boolForKey:@"Area: SO.SendSaleAutoApprove" type:SettingTypeCompany defaultValue:YES];
}

-(BOOL)expireLayawayOrders {
    return [Setting boolForKey:@"Area: Sales.ExpireLayawayOrders" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - Frequent Buyer Program

-(BOOL)frequentBuyerProgramEnable {
    return [Setting boolForKey:@"Area: SVS.EnableFrequentBuyerProgram" type:SettingTypeCompany defaultValue:FALSE];
}

#pragma mark - Loyalty Revard program

-(BOOL)lrpEnabled {
    return [Setting boolForKey:@"Area: SVS.EnableLoyaltyRewards" type:SettingTypeCompany defaultValue:FALSE];
}

-(NSInteger)lrpRate {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsAwardRate" type:SettingTypeCompany defaultValue:1];
}

-(NSInteger)lrpAdjustmentPLU {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsAjustPLU" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)lrpReturnAllowed {
    return [Setting boolForKey:@"Area: SVS.LoyaltyRewardsDeductReturns" type:SettingTypeCompany defaultValue:TRUE];
}

-(BOOL)lrpMembershipRequired {
    return [Setting boolForKey:@"Area: SVS.LoyaltyRewardsMembershipRequired" type:SettingTypeCompany defaultValue:FALSE];
}

-(NSInteger)lrpGCAwardAmount {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsGCRedemptionAmount" type:SettingTypeCompany defaultValue:1];
}

-(NSInteger)lrpTokenAwardAmount {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsTokensRedemptionAmount" type:SettingTypeCompany defaultValue:1];
}

-(LRPAward)lrpAwardType {
    NSInteger val = [Setting intForKey:@"Area: SVS.LoyaltyRewardsAwardType" type:SettingTypeCompany defaultValue:LRPAwardGiftCard];
    
    return (LRPAward)val;
}

-(NSInteger)lrpAwardThreshold {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsRedemptionThreshold" type:SettingTypeCompany defaultValue:1];
}

-(LRPProgramNumber)lrpProgramNumber {
    return [Setting intForKey:@"Area: SVS.LoyaltyRewardsProgramNumber" type:SettingTypeLocation defaultValue:LRPProgramNumber1]; // default is LRP1 program
}

#pragma mark - Real-Time Quantity service

-(NSString *)rtqServiceURL {
    return [Setting stringForKey:@"Area: System.RTQServerUrl" type:SettingTypeCompany defaultValue:@"http://api.teamwork-rta.appspot.com"];
}

-(BOOL)rtqServiceEnabled {
    return [Setting boolForKey:@"Area: System.RTQEnabled" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)accItemAvailable {
    return [Setting boolForKey:@"Area: System.AccItemAvailMobile" type:SettingTypeCompany defaultValue:YES];
}

-(BOOL)restrictToSingleDiscountType {
    return [Setting boolForKey:@"Area: Sales.RestrictToSingleDiscountType" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - Coupons
- (BOOL)couponLookupAvailable {
    return [Setting boolForKey:@"Area: Receipts.CouponLookupAvailability" type:SettingTypeCompany defaultValue:NO];
}

- (BOOL)enableCouponDiscounts {
    return [Setting boolForKey:@"Area: SVS.EnableCouponDiscounts" type:SettingTypeCompany defaultValue:NO];
}

- (BOOL)enableCustomerAssociatedCoupons {
    return [Setting boolForKey:@"Area: Sales.EnableCustomerAssociatedCoupons" type:SettingTypeCompany defaultValue:NO];
}

- (BOOL)allowMultipleCouponDiscount {
    return [Setting boolForKey:@"Area: Sales.AllowMultipleCouponDiscount" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - Auto Logout
-(NSInteger)autoLogout {
    
    return [Setting intForKey:@"Area: System.AutoLogout" type:SettingTypeLocation defaultValue:0];
}

-(NSString *)autoLogoutAction {

    return [Setting stringForKey:@"Area: System.AutoLogoutAction" type:SettingTypeLocation defaultValue:@"Logout"];
}

#pragma mark - Items
-(NSInteger)useItemDescription {
    
    return [Setting intForKey:@"Area: Item.UseItemDescription" type:SettingTypeLocation defaultValue:4];
}

//-(NSDecimalNumber*)cashRoundingPrecision{
//    NSString* value = [Setting stringForKey:@"Area: Sales.CashRoundingPrecision" type:SettingTypeLocation defaultValue:nil];
//    if (!value){
//        return nil;
//    }
//    NSDecimal decimalValue = CPDecimalFromString(value);
//    if (CPDecimalEquals0(decimalValue))
//        return nil;
//    return [NSDecimalNumber decimalNumberWithDecimal:decimalValue];
//}

-(BOOL)universalCustomersEnabled {
    return TRUE;//[Setting boolForKey:@"Area: Customer.SyncUniversalCustomerswithHQ" type:SettingTypeCompany defaultValue:YES];
}

-(NSString*)MWMerchantName {
    return [Setting stringForKey:@"Area: Payment.Universal.GeniusName" type:SettingTypeLocation defaultValue:@""];
}

-(NSString*)MWMerchantKey {
    return [Setting stringForKey:@"Area: Payment.Universal.GeniusKey" type:SettingTypeLocation defaultValue:@""];
}

-(NSString*)MWMerchantSiteId {
    return [Setting stringForKey:@"Area: Payment.Universal.GeniusSiteID" type:SettingTypeLocation defaultValue:@""];
}

-(NSString*)MWForceDuplicate {
    //Area: Payment.Universal.ForceDuplicate
    BOOL forceDuplicate = [Setting boolForKey:@"Area: Payment.Universal.ForceDuplicate" type:SettingTypeLocation defaultValue:false];
    if (forceDuplicate) {
        return @"true";
    }
    else {
        return @"false";
    }
}

-(BOOL)MWRequireAddressOnKeyed {
    return [Setting boolForKey:@"Area: Payment.Cayan.RequireCustomerOnKeyed" type:SettingTypeLocation defaultValue:false];
}

-(BOOL)MWUseAsShopper {
    return [Setting boolForKey:@"Area: Payment.Universal.UseAsShopperDisplay" type:SettingTypeLocation defaultValue:TRUE];
}


-(NSString*)eSelectStoreId {
    return [Setting stringForKey:@"Area: Payment.eSelect.StoreId" type:SettingTypeLocation defaultValue:@"store_icmp"];
}

-(NSString*)eSelectApiToken {
    return [Setting stringForKey:@"Area: Payment.eSelect.APIToken" type:SettingTypeLocation defaultValue:@"icmpguy"];
}

-(NSString*)eSelectUSHostURL {
    if (self.paymentIntellivativeIsInTestMode) {
        return self.eSelectTestUSHostURL;
    }
    else {
        return self.eSelectLiveUSHostURL;
    }
}

-(NSString*)eSelectTestUSHostURL {
    NSString* url = [Setting stringForKey:@"Area: Payment.eSelectUS.TestURL" type:SettingTypeLocation defaultValue:@"esplusqa.moneris.com"];
    
    if (url.length == 0) {
        return @"esplusqa.moneris.com"; // Default Test URL
    }
    
    return url;
}
-(NSString*)eSelectLiveUSHostURL {
    NSString* url =  [Setting stringForKey:@"Area: Payment.eSelectUS.LiveURL" type:SettingTypeLocation defaultValue:@"esplus.moneris.com"];
    
    if (url.length == 0) {
        return @"esplus.moneris.com"; // Default Live URL
    }
    
    return url;
}

-(NSString*)eSelectCanadaHostURL {
    if (self.paymentIntellivativeIsInTestMode) {
        return self.eSelectTestCanadaHostURL;
    }
    else {
        return self.eSelectLiveCanadaHostURL;
    }
}

-(NSString*)eSelectHostURL {
    NSString* countryCode = [Location localLocation].country;
    
    if ([countryCode isEqualToString:@"US"]) {
        return self.eSelectUSHostURL;
    }
    else {
        return self.eSelectCanadaHostURL;
    }
    
}

-(NSString*)eSelectTestCanadaHostURL {
    NSString* url = [Setting stringForKey:@"Area: Payment.eSelect.TestURL" type:SettingTypeLocation defaultValue:@"esqa.moneris.com"];
    
    if (url.length == 0) {
        return @"esqa.moneris.com"; // Default Test URL
    }
    
    return url;
}

-(NSString*)eSelectLiveCanadaHostURL {
    NSString* url =  [Setting stringForKey:@"Area: Payment.eSelect.LiveURL" type:SettingTypeLocation defaultValue:@"www3.moneris.com"];
    
    if (url.length == 0) {
        return @"www3.moneris.com"; // Default Live URL
    }
    
    return url;
}

-(NSString*)payworksHostURL {
    if (self.paymentIntellivativeIsInTestMode) {
        return self.payworksTestHostURL;
    }
    else {
        return self.payworksLiveHostURL;
    }
}

-(NSString*)payworksTestHostURL {
    NSString* url =  [Setting stringForKey:@"Area: Payment.Payworks.TestURL" type:SettingTypeLocation defaultValue:nil];
    
    if (url.length == 0) {
        return @"https://test.payworks.io"; // Default Test URL for Payworks
    }
    
    return url;
}

-(NSString*)payworksLiveHostURL {
    NSString* url =  [Setting stringForKey:@"Area: Payment.Payworks.LiveURL" type:SettingTypeLocation defaultValue:nil];
    
    if (url.length == 0) {
        return @"https://api.payworks.io"; // Default Live URL for Payworks
    }
    
    return url;
}

-(NSString*)payworksMerchantId {
    NSString* merchantId =  [Setting stringForKey:@"Area: Payment.Payworks.MerchantId" type:SettingTypeLocation defaultValue:nil];
    
    if (merchantId.length == 0) {
        return @"dcf696f8-3432-4fd3-b60a-b9648f13d7b0";
    }
    
    return merchantId;
}
-(NSString*)payworksKey {
    NSString* merchantKey =  [Setting stringForKey:@"Area: Payment.Payworks.MerchantKey" type:SettingTypeLocation defaultValue:nil];
    
    if (merchantKey.length == 0) {
        return @"DIMGfLts9iAEqfSDC0GFS52KzizGUp3v";
    }
    
    return merchantKey;
}

-(NSString*)payworkAPIMerchantId {
//    return @"ihpDygP3FImUeveAdJatIPxjF0t0ZJ8e";
    
    if (self.paymentIntellivativeIsInTestMode) {
        return self.payworksAPITestId;
    }
    
    return self.payworksAPILiveId;
}

-(NSString*)payworksAPITestId {
    NSString* apiId =  [Setting stringForKey:@"Area: Payment.Payworks.APITestId" type:SettingTypeLocation defaultValue:nil];
    
    if (apiId.length == 0) {
        return @"ihpDygP3FImUeveAdJatIPxjF0t0ZJ8e";
    }
    
    return apiId;
}

-(NSString*)payworksAPILiveId {
    NSString* apiId =  [Setting stringForKey:@"Area: Payment.Payworks.APILiveId" type:SettingTypeLocation defaultValue:nil];
    
    if (apiId.length == 0) {
        return nil;
    }
    
    return apiId;
}

-(NSString*)payworkAPISecretKey {
    if (self.paymentIntellivativeIsInTestMode) {
        return self.payworksAPITestKey;
    }
    
    return self.payworksAPILiveKey;
}

-(NSString*)payworksAPITestKey {
    NSString* apiKey =  [Setting stringForKey:@"Area: Payment.Payworks.APITestKey" type:SettingTypeLocation defaultValue:nil];
    
    if (apiKey.length == 0) {
        return @"koFN8kYA9rkvHw8UT3N5AmR43wmkHZUg";
    }
    
    return apiKey;
}

-(NSString*)payworksAPILiveKey {
    NSString* apiKey =  [Setting stringForKey:@"Area: Payment.Payworks.APILiveKey" type:SettingTypeLocation defaultValue:nil];
    
    if (apiKey.length == 0) {
        return nil;
    }
    
    return apiKey;
}

-(NSInteger)MWOperationTimeout {
    return [Setting intForKey:@"Area: Payment.MWTimeout" type:SettingTypeLocation defaultValue:30];
}

-(BOOL)MWGeniusAssignToStoreReceipt {
    return [Setting boolForKey:@"Area: Payment.Genius.AssignStoreRcpt" type:SettingTypeLocation defaultValue:true];
}

-(BOOL)MWGeniusAssignToSalesReceipt {
    return [Setting boolForKey:@"Area: Payment.Genius.AssignStoreRcpt" type:SettingTypeLocation defaultValue:true];
}

-(NSString*)payPalAPIUrl {
    if (self.paymentIntellivativeIsInTestMode) {
        return [self payPalTestURL];
    }
    
    return [self payPalLiveURL];
}

-(NSString*)payPalLiveURL {
    return [Setting stringForKey:@"Area: Payment.PayPal.LiveURL" type:SettingTypeLocation defaultValue:@"paypal.com"];
}

-(NSString*)payPalTestURL {
    return [Setting stringForKey:@"Area: Payment.PayPal.TestURL" type:SettingTypeLocation defaultValue:@"sandbox.paypal.com"];
}

-(NSString*)payPalLocationId {
    if (self.paymentIntellivativeIsInTestMode) {
        return [self payPalTestLocationId];
    }
    
    return [self payPalLiveLocationId];
}

-(NSString*)payPalTestLocationId {
    return [Setting stringForKey:@"Area: PayPal.LocationId.Test" type:SettingTypeLocation];
}

-(NSString*)payPalLiveLocationId {
    return [Setting stringForKey:@"Area: PayPal.LocationId.Live" type:SettingTypeLocation];
}

#pragma mark - CliSiTef settings

-(NSString*)clisitefTerminalId {
    return @"IP000001";
}

#pragma mark - Custom app launch
-(NSString*)customAppLaunchButtonTitle {
    return [Setting stringForKey:@"Area: System.CustomPOSAppButtonLabel" type:SettingTypeCompany defaultValue:@""];
}
-(NSString*)customeAppLaunchURL{
    return [Setting stringForKey:@"Area: System.CustomPOSAppURL" type:SettingTypeCompany defaultValue:@""];
}

#pragma mark - Sales History
-(BOOL)showDeviceSalesHistory {
    return [Setting boolForKey:@"Area: Sales.ShowDeviceSalesHistory" type:SettingTypeCompany defaultValue:NO];
}

-(NSInteger)salesHistorySearchDays {
    return [Setting intForKey:@"Area: Sales.SalesHistorySearchDays" type:SettingTypeCompany defaultValue:90];
}

-(NSString *)salesHistorySearch {
    return [Setting stringForKey:@"Area: Sales.SalesHistorySearch" type:SettingTypeCompany];
}

#pragma mark - Gift Card constrains

-(NSDecimal)minAllowedGiftCardSellAmount {
    Setting * setting = [Setting getSettingForKey:@"Area: Sales.MinimumAllowedGiftCardSellAmount" type:SettingTypeLocation];
    if (setting == nil) {
        setting = [Setting getSettingForKey:@"Area: Sales.MinimumAllowedGiftCardSellAmount" type:SettingTypeCompany];
    }
    return setting != nil ? CPDecimalFromString(setting.value) : D0;
}
-(NSDecimal)maxAllowedGiftCardSellAmount {
    Setting * setting = [Setting getSettingForKey:@"Area: Sales.MaximumAllowedGiftCardSellAmount" type:SettingTypeLocation];
    if (setting == nil) {
        setting = [Setting getSettingForKey:@"Area: Sales.MaximumAllowedGiftCardSellAmount" type:SettingTypeCompany];
    }
    return setting != nil ? CPDecimalFromString(setting.value) : D0;
}

-(NSString *)giftCardNumberRegex {
    return [Setting stringForKey:@"Area: SVS.GiftCardRegEx" type:SettingTypeCompany];
}

-(NSString *)giftCardDisplayNumberRegex {
    return [Setting stringForKey:@"Area: SVS.GiftCardDisplayRegex" type:SettingTypeCompany];
}

-(NSInteger)giftCardMaskLeft {
    return [Setting intForKey:@"Area: SVS.GiftCardMaskLeft" type:SettingTypeCompany defaultValue:0];
}

-(NSInteger)giftCardMaskRight {
    return [Setting intForKey:@"Area: SVS.GiftCardMaskRight" type:SettingTypeCompany defaultValue:0];
}

-(BOOL)giftCardsClearNumber {
    return [Setting boolForKey:@"Area: SVS.GiftCardClearNumber" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)showCLUonItemsGrids{
    return [Setting boolForKey:@"Area: POS.ShowCLUInItemGrid" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)giftCardsRequirePassword {
    return [Setting boolForKey:@"Area: Sales.ReqCustomerPwdforGC" type:SettingTypeCompany defaultValue:YES];
}

-(BOOL)giftCardsSellOnlyRegistered {
    return [Setting boolForKey:@"Area: Sales.SellOnlyRegisteredGiftCards" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - Prinitng Settings

-(BOOL)useLocalSettingsForMail {
    return [Setting boolForKey:@"Area: System.Mail.UseLocalSettingsForMail" type:SettingTypeLocation];
}

-(BOOL)includeHTMLBodyToEmail {
    BOOL useLocalSetting = [self useLocalSettingsForMail];
    NSInteger v = useLocalSetting ? [Setting intForKey:@"Area: System.Mail.eReceiptOptionLoc" type:SettingTypeLocation] :
    [Setting intForKey:@"Area: System.Mail.eReceiptOption" type:SettingTypeCompany];
    return  v == 1 || v == 2;
}

-(BOOL)attachPDFToEmail {
    BOOL useLocalSetting = [self useLocalSettingsForMail];
    NSInteger v = useLocalSetting ? [Setting intForKey:@"Area: System.Mail.eReceiptOptionLoc" type:SettingTypeLocation] :
    [Setting intForKey:@"Area: System.Mail.eReceiptOption" type:SettingTypeCompany];
    return  v == 0 || v == 2;
}

-(BOOL)cameraBarcodeReaderEnabled {
    return [Setting boolForKey:@"Area: System.CameraBarcodeReaderEnabled" type:SettingTypeLocation defaultValue:NO];
}

#pragma mark - Email validation
-(BOOL)useFreshAddressValidation {
    return [Setting boolForKey:@"Area: System.UseFreshAddress" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - URL Buttons Settings

-(NSString *)mainMenuBtn1Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn1Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn1Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn1Url" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn2Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn2Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn2Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn2Url" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn3Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn3Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn3Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn3Url" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn4Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn4Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn4Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn4Url" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn5Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn5Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn5Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn5Url" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn6Label {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn6Label" type:SettingTypeCompany];
}

-(NSString *)mainMenuBtn6Url {
    return [Setting stringForKey:@"Area: Sales.POSDeviceMainMenuBtn6Url" type:SettingTypeCompany];
}

#pragma mark -
-(BOOL)roundTaxesBeforeAggregate{
    NSString* value =  [Setting stringForKey:@"Area: Sales.RoundingWorkflow" type:SettingTypeCompany];
    if (value){
        return [value caseInsensitiveCompare:@"TaxSExtS"]==NSOrderedSame;
    }
    return FALSE;
}

-(NSString *)cloudHQUrl {
    return [Setting stringForKey:@"Area: WebReceipts.CloudHqUrl" type:SettingTypeCompany defaultValue:nil];
}
#pragma mark - Phone validation

-(PhoneNumberFormat)phoneFormate {

    NSString *str=[Setting stringForKey:@"Area: Sales.PhoneNumberFormat" type:SettingTypeCompany defaultValue:nil];
    if ([str isEqualToString:@"No Validation"])
        return PhoneNumberFormatNoValidation;
    
    if ([str isEqualToString:@"E.164"])
        return PhoneNumberFormatE164;
    
    if ([str isEqualToString:@"International"])
        return PhoneNumberFormatInternational;
    
    if ([str isEqualToString:@"National"])
        return PhoneNumberFormatNational;
    
    return PhoneNumberFormatNoValidation;
}
-(NSString*)phoneNumberDefaultCountry{
    return [Setting stringForKey:@"Area: Sales.PhoneNumberDefaultCountry" type:SettingTypeCompany defaultValue:nil];
}

#pragma mark - Password Validation

-(BOOL)usePciPassword {
    return [Setting boolForKey:@"Area: System.UsePciPassword" type:SettingTypeCompany defaultValue:NO];
}

-(BOOL)enablePasswordExpiration {
    return [Setting boolForKey:@"Area: System.EnablePasswordExpiration" type:SettingTypeCompany defaultValue:NO];
}

-(NSInteger)daysBeforePasswordExpires {
    return [Setting intForKey:@"Area: System.DaysBeforePasswordExpires" type:SettingTypeCompany defaultValue:90];
}


-(AltDTNType)altDNTType {
    
    NSString *res = [Setting stringForKey:@"Area: Sales.AltDTNType" type:SettingTypeCompany defaultValue:@"None"];
    if ([@"LDTN" isEqualToString:res])
        return AltDtnLDTN;
    return AltDtnNone;
}

-(ShowDTNType)showDNTType {

    NSString *res = [Setting stringForKey:@"Area: Sales.ShowDTN" type:SettingTypeCompany defaultValue:@"DTN"];
    if ([@"AlternativeID" isEqualToString:res])
        return SowLDTN;

    return ShowDTN;
}

-(MediaSourceType)mediaSource{

    return [Setting boolForKey:@"Area: DAM.UploadEnabled" type:SettingTypeCompany defaultValue:YES]
                ? MediaSourceTypeDAM
                : MediaSourceTypeRCM;
}

#pragma mark - Tax Free
-(BOOL)taxFreeEnabled {
    return IS_IPAD ? [Setting boolForKey:@"Area: Sales.TaxFreeEnabled" type:SettingTypeLocation defaultValue:NO] : NO;
}

-(BOOL)emailReadyForPickup {
    return [Setting boolForKey:@"Sales: Email.ReadyForPickup" type:SettingTypeCompany defaultValue:NO];
}

#pragma mark - PluginXml

-(NSString *)pluginXml {
    Setting *pluginXmlSetting = [Setting getSettingForKey:@"Area: System.PosPluginXml" type:SettingTypeCompany];
    
    if (!pluginXmlSetting.value.boolValue) {
        return nil;
    }
    
    NSArray *binaryDataStorageArray = [BinaryStorageData getBinaryStorageDataWithOwnerID:pluginXmlSetting.id];
    if (binaryDataStorageArray && binaryDataStorageArray.count > 0) {
        
        BinaryStorageData *binaryStorageData = binaryDataStorageArray[0];
        NSData *binaryData = binaryStorageData.data;
        NSString *pluginXmlString = [[NSString alloc] initWithData:binaryData encoding:NSUnicodeStringEncoding];
        
        return [pluginXmlString autorelease];
    }
    
    return nil;
}

#pragma mark - DAM
-(NSString *)damURL {
    return [Setting stringForKey:@"Area: DAM.ServiceUrl" type:SettingTypeCompany defaultValue:nil];
}
#pragma mark - scandit
-(BOOL)useScanditScanner{
    if ([Setting boolForKey:@"Area: System.UseScanditBarcodeScanner" type:SettingTypeCompany defaultValue:NO] && self.scanditToken.length>0)
        return YES;
    else
        return NO;
}
-(NSString*)scanditToken{
    return [Setting stringForKey:@"Area: System.ScanditToken" type:SettingTypeCompany defaultValue:nil];
}

#pragma mark - Database backup

-(BOOL)uploadBackupToGAE {
    return [Setting boolForKey:@"Area: System.UploadBackupToGAE" type:SettingTypeCompany defaultValue:YES];
}

#pragma mark - BOMGAR

-(BOOL)bomgarEnabled {
    if ([Setting boolForKey:@"Area: System.Bomgar.Enabled" type:SettingTypeCompany defaultValue:NO] && self.bomgarCompanyId.length>0 && self.bomgarSiteAddress.length>0)
        return YES;
    else
        return NO;
}

-(NSString*)bomgarCompanyId {
    NSString* value = [Setting stringForKey:@"Area: System.Bomgar.CompanyId" type:SettingTypeCompany defaultValue:nil];
    
//    if (value.length == 0) {
//        value = @"retailteam_cloud";
//    }
    
    return value;
}

-(NSString*)bomgarSiteAddress {
    NSString* value = [Setting stringForKey:@"Area: System.Bomgar.SiteAddress" type:SettingTypeCompany defaultValue:nil];;
    
//    if (value.length == 0) {
//        value = @"teamworkretail.bomgarcloud.com";
//    }
    
    return value;
}


-(BOOL)useExtTaxService {
    return [Setting boolForKey:@"Area: Sales.UseExtTaxService" type:SettingTypeLocation defaultValue:NO];
}

-(BPUUID*)shipItemTaxArea {
    NSString* taxAreaStr = [Setting stringForKey:@"Area: Sales.ShipItemTaxArea" type:SettingTypeLocation defaultValue:nil];
    if (taxAreaStr.length){
        return [BPUUID UUIDWithString:taxAreaStr];
    }
    return nil;
}

-(NSString*)extTaxServiceURL {
    return [Setting stringForKey:@"Area: Sales.ExtTaxServiceURL" type:SettingTypeCompany defaultValue:nil];
}


-(BOOL)canReturnShipToItems{
    return ![Setting boolForKey:@"Area: Sales.CreateSendSaleOrderForShipItems" type:SettingTypeCompany defaultValue:NO]
    || ![Setting boolForKey:@"Area: System.ChqLedgerEnabled" type:SettingTypeCompany defaultValue:NO] || ![Setting boolForKey:@"Area: System.DoNotStreamTransactionalDocumentsToNavision" type:SettingTypeCompany defaultValue:NO];
}
-(BPUUID*)defaultCustomerPresetId{
    NSString* srt=[Setting stringForKey:@"Area: Customers.DefaultPresetID" type:SettingTypeCompany defaultValue:nil];
//    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"{}"];
//    srt = [[srt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    if (srt.length){
        BPUUID* b=[BPUUID UUIDWithString:srt];
        return b;
    }
    return nil;
}
-(NSString*)supportPhone{
    return [Setting stringForKey:@"Area: System.BusinessPartners.SupportPhone" type:SettingTypeCompany defaultValue:@"(727) 210-1700,  Option 1"];
}
-(NSString*)supportEmail{
   return [Setting stringForKey:@"Area: System.BusinessPartners.SupportEmail" type:SettingTypeCompany defaultValue:@"support@teamworkretail.com"];
}
-(NSString*)supportText{
    return [Setting stringForKey:@"Area: System.BusinessPartners.SupportText" type:SettingTypeCompany defaultValue:@"For assistance, call the phone number below or use the support email to submit a ticket for help."];
}
-(NSString*)companyDCSSDepartment{
    return [Setting stringForKey:@"Area: ClassificationNames.CompanyDCSSDepartment" type:SettingTypeCompany defaultValue:nil];
}

#pragma mark - Singleton Methods

-(id)init{
	if ((self=[super init])){
		//settingsList = [Setting getSettings];
	}
	return self;
}

+(SettingManager *)instance {
    static dispatch_once_t pred;
    static SettingManager *instance = nil;
    
    dispatch_once(&pred, ^{
        instance = [[SettingManager alloc] init];
    });
    return instance;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)retain {
	return self;
}

- (NSUInteger)retainCount {
	return UINT_MAX; //denotes an object that cannot be released
}

- (oneway void)release {
	// never release
}

- (id)autorelease {
	return self;
}

- (void)dealloc {
	// Should never be called, but just here for clarity really.
	//[settingsList release];
	[super dealloc];
}
@end
