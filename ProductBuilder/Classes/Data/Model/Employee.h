//
//  Employee.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/18/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

static const NSInteger kNumberOfDaysToKeepAfterLogin = 3;

@interface Employee : CROEntity {
	int _employeeNum;
	NSString *_loginName;
	NSString *_lastName;
	NSString *_firstName;
	BOOL _universal;
	BOOL _active;
	BOOL _isManager;
    NSDecimal _maxDiscPercent;
	NSDecimal _maxGlobalDiscPercent;
    BOOL _discRequireAuthCode;
    NSDecimal _maxDiscPercentRole;
    NSDecimal _maxGlobalDiscPercentRole;
    BOOL _discRequireAuthCodeRole;
	NSString *_code;
	NSString *_nickName;
	int _listOrder;
	NSString *_password;
    BPUUID *_locationID;
    BOOL _overrideRoleDiscountLimits;
}

@property (atomic, assign) int employeeNum;
@property (atomic, retain) NSString *loginName;
@property (atomic, retain) NSString *lastName;
@property (atomic, retain) NSString *firstName;
@property (atomic, assign) BOOL universal;
@property (atomic, assign) BOOL active;
@property (atomic, assign) BOOL isManager;
@property (atomic, assign) NSDecimal maxDiscPercent;
@property (atomic, assign) NSDecimal maxGlobalDiscPercent;
@property (atomic, assign) BOOL discRequireAuthCode;
@property (atomic, assign) NSDecimal maxDiscPercentRole;
@property (atomic, assign) NSDecimal maxGlobalDiscPercentRole;
@property (atomic, assign) BOOL discRequireAuthCodeRole;
@property (atomic, retain) NSString *code;
@property (atomic, retain) NSString *nickName;
@property (atomic, assign) int listOrder;
@property (atomic, retain) NSString *password;
@property (atomic, retain) BPUUID *locationID;
@property (atomic, readonly)NSString* displayName;
@property (atomic, readonly)NSString* initials;
@property (atomic, readonly)NSString* shortName;
@property (atomic, readonly)NSString* shortNameFormatted;
@property (atomic, retain) NSDate *lastLoginDate;
@property (atomic, retain) NSDate *passwordChangeDate;
@property (atomic, assign) BOOL overrideRoleDiscountLimits;

+(Employee *)getEmployeesByName:(NSString *)loginName andPassword:(NSString *)password isAll:(BOOL)isAll;

-(BOOL)updateLastLoginDateWithDate:(NSDate *)date error:(NSError **)error;
-(void)resetDiscountLoadFlag;

-(NSDictionary *)getProperties;

@end
