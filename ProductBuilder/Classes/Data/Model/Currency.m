//
//  TestClass.m
//  ProductBuilder
//
//  Created by Roman on 11/1/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Currency.h"
#import "Location.h"
#import "DecimalHelper.h"
#import "SettingManager.h"
#import "MathUtils.h"
#import "NSObject+MethodExchange.h"

@implementation MathUtils(Currency)

+(void)load{
    [[self class]exchangeMethod:@selector(roundMoney:) withNewMethod:@selector(roudnMoneyWithCurrency:)];
}

+(NSDecimal)roudnMoneyWithCurrency:(NSDecimal) value{
    return [[Currency localCurrency]roundAmount:value];
}

@end

@interface Currency()

@property (nonatomic, retain) NSLocale* locale;
@end

@implementation Currency

@synthesize description = _description;

Currency* _localCurrency;
Currency* _companyCurrency;

+(Currency *)localCurrency
{
    @synchronized(self){
        if (!_localCurrency) {
            _localCurrency = [[self locationCurrency:[Location localLocation]] retain];
        }
        return [[_localCurrency retain]autorelease];
    }
}

+(void)setLocalCurrencyUnsafe:(Currency*)currency{
    @synchronized(self){
        [_localCurrency release];
        _localCurrency = [currency retain];
     }
}

+(Currency *)locationCurrency:(Location*)location{
    NSArray *currencyList = [Currency list];
    for(Currency* currency in currencyList){
        if ([currency.id isEqual:location.locationBaseCurrencyID]) {
            return  currency;
        }
    }
    return [self companyCurrency];
}

+(Currency *)companyCurrency
{
    @synchronized(self){
        if (!_companyCurrency) {
            NSArray *currencyList = [Currency list];
            if (!_companyCurrency){
                for(Currency* currency in currencyList)
                {
                    if (currency.isBase) {
                        _companyCurrency = [currency retain];
                        break;
                    }
                }
            }
            if (!_companyCurrency){
                for(Currency* currency in currencyList)
                {
                    if ([currency.id isEmpty]) {
                        _companyCurrency = [currency retain];
                        [_companyCurrency initializeFromLocalSystem];
                        break;
                    }
                }
            }
            if (!_companyCurrency){
                _companyCurrency = [[Currency alloc] init];
                [_companyCurrency initializeFromLocalSystem];
            }
        }
        return [[_companyCurrency retain]autorelease];
    }
}

//+(BOOL)isLocalCurrencyId:(BPUUID*)currencyId{
//    if (!currencyId || currencyId.isEmpty){
//        currencyId = self.class.companyCurrency.id;
//    }
//    return [self.class.localCurrency.id isEqual:currencyId];
//}

+(void) resetCache{
    @synchronized(self){
        [_localCurrency release];
        _localCurrency = nil;
        [_companyCurrency release];
        _companyCurrency = nil;
    }
}

-(void)initializeFromLocalSystem
{
    self.currencyCode = [[NSLocale currentLocale] objectForKey: NSLocaleCurrencyCode];
    self.symbol = [[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol];
    self.locale = nil;
    
    self.description = [[SettingManager instance] defaultCurrencyLabel];
    self.displayDecimalPrecision = [[SettingManager instance] systemDecimals];
    
    self.isBase = true;
    self.amountRoundingPrecision = [[self class] getAmountRoundingPrecisionFromDecimalDigits:self.displayDecimalPrecision];
}

-(NSLocale*)locale{
    if (!_locale){
        if (self.currencyCode){
            NSDictionary *components = @{NSLocaleCurrencyCode: self.currencyCode};
            NSString *localeIdent = [NSLocale localeIdentifierFromComponents:components];
            if (localeIdent){
                _locale = [[NSLocale alloc]initWithLocaleIdentifier:localeIdent];
            }
        }
        else{
            _locale = [[NSLocale currentLocale]retain];
        }
    }
    return _locale;
}

+(NSDecimal)getAmountRoundingPrecisionFromDecimalDigits:(NSInteger)decimalDigits
{
    NSDecimal amountRoundingPrecision = D1;
    NSDecimal ten = CPDecimalFromInt(10);
    for (NSInteger i = 0; i < decimalDigits; i++)
    {
        amountRoundingPrecision = CPDecimalDivide(amountRoundingPrecision, ten);
    }
    return amountRoundingPrecision;
}

-(NSRoundingMode)roundingMode{
    switch (self.roundingType) {
        case RoundingTypeNearest:
            return NSRoundBankers;
        case RoundingTypeUp:
            return NSRoundUp;
        case RoundingTypeDown:
            return NSRoundDown;
        default:
            return NSRoundBankers;
    }
}

-(NSDecimal)roundDisplayAmount:(NSDecimal) value
{
	NSDecimal result = [self roundAmount:value];
    BOOL isNegative = value._isNegative;
    value._isNegative = FALSE;
    NSRoundingMode mode = [self roundingMode];
	NSDecimalRound(&result, &value, self.displayDecimalPrecision, mode);
    result._isNegative = isNegative;
    if (NSDecimalIsNotANumber(&result)){
        result = D0;
    }
	return result;
}

-(NSDecimal)roundAmount:(NSDecimal) value
{
    return [[self class]roundAmount:value toPrecision:self.amountRoundingPrecision roundingMode:[self roundingMode]];
}

+(NSDecimal)roundAmount:(NSDecimal) value toPrecision:(NSDecimal)amountRoundingPrecision roundingMode:(NSRoundingMode)roundingMode
{
    if (NSDecimalIsNotANumber(&value) || CPDecimalEquals0(value)){
        return D0;
    }
    if (CPDecimalEquals0(amountRoundingPrecision)){
        return value;
    }
	NSDecimal result;
    BOOL isNegative = value._isNegative;
    value._isNegative = FALSE;
    NSDecimal intPart;
    NSDecimalRound(&intPart, &value, 0, NSRoundDown);
    NSDecimal fractionalPart;
    NSDecimalSubtract(&fractionalPart, &value, &intPart, NSRoundPlain);
    NSDecimalDivide(&fractionalPart, &fractionalPart, &amountRoundingPrecision, NSRoundPlain);
    NSDecimalRound(&fractionalPart, &fractionalPart, 0 , roundingMode);
    NSDecimalMultiply(&fractionalPart, &fractionalPart, &amountRoundingPrecision, NSRoundPlain);
    NSDecimalAdd(&result, &intPart, &fractionalPart, NSRoundPlain);
    
	//NSDecimalRound(&result, &value, displayDecimalPrecision, mode);
    result._isNegative = isNegative;
    if (NSDecimalIsNotANumber(&result)){
        result = D0;
    }
	return result;
}


-(id) init
{
    if ( self = [super init] ) {
        self.roundingType = RoundingTypeNearest;
        self.symbolPosition = SymbolPositionLeft;
    }
    return self;
}

-(NSString*)description{
    return _description.length ?_description :_code;
}

-(NSDecimalNumber*)cashRoundingPrecision{
    if (!CPDecimalEquals0(self.paymentRoundingPrecision) && !CPDecimalEquals(self.paymentRoundingPrecision, self.amountRoundingPrecision)){
        return [NSDecimalNumber decimalNumberWithDecimal:CPDecimalABS(self.paymentRoundingPrecision)];
    }
    return nil;
}

-(void) dealloc
{
    [_locale release];
	[_currencyCode release];
    [_code release];
	[_description release];
	[_symbol release];
	[super dealloc];
}
@end
