//
//  Location.h
//  TeamworkPOS
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"
#import "Country.h"

@interface LocationBase:CROEntity{
    NSString* _name;
    NSString* _locationCode;
}

@property (nonatomic, retain) NSString*  name;
@property (nonatomic, retain) NSString* locationCode;

@end

@interface Location : LocationBase {
    Country* _countryObject;
}

@property (nonatomic, retain) NSString* name2;
@property (nonatomic, assign) int  locationNum;
@property (nonatomic, retain) BPUUID* taxZoneID;
@property (nonatomic, retain) BPUUID* locationGroupID;
@property (nonatomic, retain) BPUUID* locationPriceGroupID;
@property (nonatomic, retain) NSString* locationExternalID;

@property (nonatomic, retain) BPUUID* locationBaseCurrencyID;  // Roma

@property (nonatomic, retain) NSString* transferGroup;
@property (nonatomic, retain) NSString* area;
@property (nonatomic, retain) NSString* phone1;
@property (nonatomic, retain) NSString* faxNumber;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* homePage;
@property (nonatomic, assign) NSDecimal latitude;
@property (nonatomic, assign) NSDecimal longitude;
@property (nonatomic, retain) NSString*  defaultPriceLevelCode;

@property (nonatomic, retain) NSDecimalNumber * distance;
@property (nonatomic, retain) NSDecimalNumber * onHandQty;
@property (nonatomic, retain) NSDecimalNumber * incomingQty;
@property (nonatomic, retain) NSDecimalNumber * committedQty;
@property (nonatomic, retain) NSDecimalNumber * availableQty;
@property (nonatomic, retain) BPUUID * locationAvailabilityGroupID;
@property (nonatomic, readonly) NSString* description2;
@property (nonatomic, retain) BPUUID * timeZoneID;
@property (nonatomic, retain) NSString* address1;
@property (nonatomic, retain) NSString* address2;
@property (nonatomic, retain) NSString* address3;
@property (nonatomic, retain) NSString* address4;
@property (nonatomic, retain) NSString* city;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* postalCode;
@property (nonatomic, retain) NSString* country;
@property (nonatomic, readonly)Country* countryObject;

@property (nonatomic, assign) BOOL availableForStorePickup;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, assign) BOOL open;

+(Location *)localLocation;
+(void)resetCache;
+(NSMutableArray*)getLocations;
+(NSMutableArray*)getLocationsFromLocalAvailabilityGroup;
+(id)getLocationByCode:(NSString*)code;
+(id)getLocationByEID:(NSString*)eid;
+(NSDecimalNumber *)distanceFrom:(Location *)location1 to:(Location *)location2 inMiles:(BOOL)inMiles;
+(NSString*)getCompanyName;

@end
