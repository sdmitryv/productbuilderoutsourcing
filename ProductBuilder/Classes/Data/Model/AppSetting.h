//
//  Setting.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

@interface AppSetting : CEntity {
	NSString	* _type;
	NSString	* _key;
	NSString	* _value;
	
}

@property (nonatomic, retain) NSString	*type;
@property (nonatomic, retain) NSString	*key;
@property (nonatomic, retain) NSString	*value;

+ (NSMutableArray *)getSettings;
+ (AppSetting *)getSettingForKey:(NSString *)key;
+ (AppSetting *)getSettingForKey:(NSString *)key usingDb:(EGODatabase*)db;

+ (NSString*)stringForKey:(NSString *)key;
+ (BOOL)boolForKey:(NSString *)key;
+ (NSInteger)intForKey:(NSString *)key;

+ (NSString*)stringForKey:(NSString *)key defaultValue:(NSString*)defaultValue;
+ (BOOL)boolForKey:(NSString *)key defaultValue:(BOOL)defaultValue;
+ (NSInteger)intForKey:(NSString *)key defaultValue:(NSInteger)defaultValue;


+ (void)setString:(NSString *)value forKey:(NSString *)key;
+ (void)setBool:(BOOL)value forKey:(NSString *)key;
+ (void)setInt:(NSInteger)value forKey:(NSString *)key;

@end
