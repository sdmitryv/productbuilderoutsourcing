//
//  ProductList.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/7/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import "ProductList.h"
#import "Product.h"
#import "CROEntity+Sqlite.h"

@implementation ProductList

-(id)init
{
    if ((self = [super init])) {
        _lzController = [[LazyLoadingController alloc] initWithLazyLoadable:self];
        _command = [[SqlCommand alloc] initWithStmt:productSelectStm];
        _command.limit = 1000;
    }
    return self;
}

-(SqlFilterCondition*) filterCondition
{
    return _command.filterCondition;
}

-(NSArray*) sortOrderList
{
    return _command.sortOrderList;
}

-(void)setFilterCondition:(SqlFilterCondition*)filterCondition
{
    _command.filterCondition = filterCondition;
    [_lzController reset];
}

-(void)setSortOrderList:(NSArray*) sortOrderList{
    _command.sortOrderList = sortOrderList;
    [_lzController reset];
}


-(SqlCommand*) command
{
    return _command;
}

+(NSObject<ILazyItem>*)getItem:(EGODatabaseRow*)row
{
    return[Product instanceFromRow:row];
}

-(Product*)objectAtIndex:(NSUInteger)index
{
    return [_lzController objectAtIndex:index];
}

-(Product*)objectAtIndexedSubscript:(NSInteger)index{
    return [_lzController objectAtIndex:index];
}

-(Item*)objectByRowid:(NSNumber *)rowid {
    return [_lzController objectByRowid:rowid];
}

-(NSInteger)indexOfObject:(Product*)item
{
    return [_lzController indexOfObject:item];
}

-(NSInteger) count
{
    return [_lzController count];
}

- (NSInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len
{
    return [_lzController countByEnumeratingWithState:state objects:stackbuf count:len];
}

-(void)dealloc
{
    [_lzController release];
    [_command release];
    
    [super dealloc];
}

-(void)refresh{
    [_lzController reset];
}

@end
