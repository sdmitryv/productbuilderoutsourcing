//
//  Title.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface Title : CROEntity {
	NSString *_description;
	NSInteger _listOrder;
}

@property(nonatomic,retain) NSString *description;
@property(nonatomic,assign) NSInteger listOrder;
@property(nonatomic,assign) BOOL isReqRetired;

+(NSMutableArray*)getTitles;
@end

