//
//  CalendarStatus.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CalendarStatus.h"

@implementation CalendarStatus

@synthesize status = _status;
@synthesize label = _label;

-(void)dealloc {
    [_label release];
    
    [super dealloc];
}

+(id)getInstanceByType:(CalendarStatusType)type {
    return nil;
}

@end
