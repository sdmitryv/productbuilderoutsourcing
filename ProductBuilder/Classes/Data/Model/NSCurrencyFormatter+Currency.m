//
//  DecimalHelper+defaultCurrencyFormatter_Currency.m
//  ProductBuilder
//
//  Created by Roman on 11/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NSCurrencyFormatter+Currency.h"
#import "Currency.h"
#import "CROEntity+Sqlite.h"
#import "UITextFieldHandler.h"

@implementation NSCurrencyFormatter (Currency)

+(void)handleCurrencyFormatter:(NSNumberFormatter*)currencyFormatter{
    currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    currencyFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
    Currency* localCurrency = [Currency localCurrency];
//    if (localCurrency.locale){
//        currencyFormatter.locale = localCurrency.locale;
//    }
    
    if (localCurrency.currencyCode){
        currencyFormatter.currencyCode = localCurrency.currencyCode;
    }
    currencyFormatter.maximumFractionDigits = 2;
    currencyFormatter.minimumFractionDigits = 2;
    currencyFormatter.roundingMode = NSNumberFormatterRoundHalfEven;
    //this section provides support for currency symbol and position
    //it turned out that currenly it isn't needed now
    /*
    currencyFormatter.currencySymbol = @"";
    //here we'll get suffixes and prefixes without currency symbol
    NSString* positivePrefix = [currencyFormatter.positivePrefix retain];
    NSString* positiveSuffix = [currencyFormatter.positiveSuffix retain];
    NSString* negativePrefix = [currencyFormatter.negativePrefix retain];
    NSString* negativeSuffix = [currencyFormatter.negativeSuffix retain];
    currencyFormatter.currencySymbol = localCurrency.symbolPosition == SymbolPositionDontShow ? @"" : localCurrency.symbol;
        
    NSMutableString *formatString = [NSMutableString stringWithString:@"#,##0.00"];
    if (localCurrency.symbolPosition == SymbolPositionRight) {
        [formatString appendString:@" ¤"];
    }else if(localCurrency.symbolPosition == SymbolPositionLeft){
        [formatString insertString:@"¤ " atIndex:0];
    }
    NSMutableString* positiveFormat = [NSMutableString stringWithString:formatString];
    NSMutableString* negativeFormat = [NSMutableString stringWithString:formatString];
    if (positivePrefix){
        [positiveFormat insertString:positivePrefix atIndex:0];
    }
    if (positiveSuffix){
        [positiveFormat appendString:positivePrefix];
    }
    if (negativePrefix){
        [negativeFormat insertString:negativePrefix atIndex:0];
    }
    if (negativeSuffix){
        [negativeFormat appendString:negativeSuffix];
    }
    currencyFormatter.positiveFormat = positiveFormat;
    currencyFormatter.negativeFormat = negativeFormat;
    [positivePrefix release];
    [positiveSuffix release];
    [negativePrefix release];
    [negativeSuffix release];
    */
}

+(void)load{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDatabaseIsReady) name:DataManagerDatabaseIsReady object:nil];
}

+(void)handleDatabaseIsReady{
    [UITextFieldHandler setNumberFormatterHandler:^BOOL(NSNumberFormatter *formatter, NSTextFormatterStyle style){
        if (style == NSTextFormatterCurrencyStyle && [Currency localCurrency]){
            [self handleCurrencyFormatter:formatter];
            return TRUE;
        }
        return FALSE;
    }];
    NSNumberFormatter* defaultCurrencyFormatter = [[NSNumberFormatter alloc]init];
    [self handleCurrencyFormatter:defaultCurrencyFormatter];
    [self class].defaultCurrencyFormatter = defaultCurrencyFormatter;
    [defaultCurrencyFormatter release];
}

@end
