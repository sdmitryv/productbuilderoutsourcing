//
//  SettingManager.h
//  CloudworksPOS
//
//  Created by valera on 9/5/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Setting.h"

typedef NS_ENUM(NSUInteger, LRPAward) {LRPAwardUnknown = 0, LRPAwardGiftCard = 1, LRPAwardTokens = 2, LRPAwardCoupon = 3};

typedef NS_ENUM(NSUInteger, LRPProgramNumber) {LRPProgramNumber1 = 0, LRPProgramNumber2 = 1, LRPProgramNumber1_2 = 2};

typedef NS_ENUM(NSUInteger, MediaSourceType) {MediaSourceTypeRCM = 0, MediaSourceTypeDAM = 1};


typedef NS_ENUM(NSUInteger, AltDTNType) {AltDtnNone = 0, AltDtnLDTN = 1};
typedef NS_ENUM(NSUInteger, ShowDTNType) {ShowDTN = 0, SowLDTN = 1};
typedef NS_ENUM(NSUInteger, HouseChargeType) {HouseChargeDisabled = 0, HouseChargeOpenInvoice = 1, HouseChargeBalanceForward = 2};

typedef NS_ENUM(NSUInteger, PromptForReceiptLoginType) {
    PromptForReceiptLoginTypeNoPrompt = 0,
    PromptForReceiptLoginTypePromptForCashier = 1,
    PromptForReceiptLoginTypePromptForAssociate = 2,
    PromptForReceiptLoginTypePromptForCashierAssociate = 3
    };

typedef NS_ENUM(NSUInteger, SalesReceiptTabType) {
    SalesReceiptTabCustomer = 0,
    SalesReceiptTabItems = 1,
    SalesReceiptTabPayment = 2,
    SalesReceiptTabShipto = 3,
    SalesReceiptTabBilling = 4,
    SalesReceiptTabInfo = 5,
    SalesReceiptTabOrder = 6,
    SalesReceiptTabRepair = 7,
    SalesReceiptTabWishlist = 8,
    SalesReceiptTabDelivery = 9,
    SalesReceiptTabLayaway = 10,
    SalesReceiptTabRental = 11,
    SalesReceiptTabTrade = 12,
    SalesReceiptTabReturn = 13,
    SalesReceiptTabGiftsDeposits = 14,
    SalesReceiptTabPreset = 15,
    SalesReceiptTabRegistry=16,
    SalesReceiptTabSessions=17,
    SalesReceiptTabAccounts=18,
    SalesReceiptTabSummary=19,
    SalesReceiptTabGiftCards = 20,
    SalesReceiptTabDeposits = 21,
};


typedef NS_ENUM(NSUInteger, SalesOrderTabType) {
    SalesOrderTabCustomer = 0,
    SalesOrderTabItems = 1,
    SalesOrderTabShipTo = 2,
    SalesOrderTabInstructions = 3,
    SalesOrderTabNotes = 4,
    SalesOrderTabActivity = 5,
    SalesOrderTabOrderInfo = 6,
};

typedef NS_ENUM(NSUInteger, RequireFieldsType) {
    RequireNone                 = 0,
    RequireAssociateField       = 1 <<  0,
    RequireLocationField        = 1 <<  1,
    RequireReceiptNumField      = 1 <<  2,
    RequireReceiptDateField     = 1 <<  3
};

typedef NS_ENUM(NSUInteger, SettingCheckEmailPhone) {
    SettingCheckEmailPhoneNoAction,
    SettingCheckEmailPhoneWarn,
    SettingCheckEmailPhonePrevent,
};
typedef NS_ENUM(NSUInteger, PhoneNumberFormat) {
    PhoneNumberFormatNoValidation=0,
    PhoneNumberFormatE164=1,
    PhoneNumberFormatInternational=2,
    PhoneNumberFormatNational=3
};

typedef NS_ENUM(NSUInteger, CustomLookupDisplayType) {
    CustomLookupDisplayTypeName=0,
    CustomLookupDisplayTypeAlias=1,
    CustomLookupDisplayTypeBoth=2,
};

@interface SettingManager : NSObject {
    NSArray* settings;
}
+(SettingManager *)instance;

-(BOOL) autoInsertAfterPost;
-(NSInteger)deviceHistoryPurgeLimit;
-(BOOL) giftCardsServiceUse;
-(NSString *) svsAccountUserName;
-(NSString *) svsAccountPassword;
//-(NSString *) svsAuthUrl;
-(NSString *) svsServiceUrl;
-(NSString *) svs2ServiceUrl;
-(NSString *) gaeLoggerServiceUrl;
-(NSDecimalNumber*)settingMaxSalesDiscount;
-(BOOL)sendCCTrack2IfValid;
-(BOOL)SRtoSvsOffline;


-(BOOL)paymentIntellivativeIsInTestMode;
-(NSString*)paymentIntellivativeURL;
-(NSString*)paymentIntellivativeTestURL;
-(NSString*)paymentIntellivativeProductionURL;
-(NSString*)paymentIntellivativeApiURL;
-(NSString*)paymentIntellivativeTestApiURL;
-(NSString*)paymentIntellivativeProductionApiURL;
-(BOOL)paymentIntellivativeWriteLogs;
-(BOOL)paymentIntellivativeWriteFullLog;
-(BOOL)paymentIntellivativeAutomaticTimeoutRequest;

-(BOOL)oneCreditCardPerSale;

-(NSString*)iAceptaScheme;
-(NSString*)cieloMobileScheme;
-(NSString*)billpocketScheme;

-(NSInteger)systemDecimals;
-(NSString *)defaultCurrencyLabel;
-(NSString *)defaultCustomerPhone1Label;
-(NSString *)defaultCustomerPhone2Label;
-(NSString *)defaultCustomerPhone3Label;
-(NSString *)defaultCustomerPhone4Label;
-(NSString *)defaultCustomerShipToPhoneLabel;

-(BOOL)clearEmptyBlock;
-(BOOL)bigPageSaze;

-(NSString*)receiptTrackingMerchantId;
-(BOOL)preventNegativeDiscount;
-(BOOL)requireCustomerForAllSales;
-(BOOL)requireCustomerForReturn;
-(BOOL)requireDiscountOverrideCode;

-(NSString*)voiceAuthPhoneCompany;
-(NSString*)voiceAuthPhoneLocation;
-(NSString*)voiceAuthTextCompany;
-(NSString*)voiceAuthTextLocation;


-(NSString *)rcmServiceURL;

-(NSString*)rsaCertificateName;

-(NSInteger)salesRentalFeesPLU;
-(NSInteger)salesRentalQtyAdjPLU;
-(NSInteger)storeCreditAdjustmentPLU;
-(NSInteger)giftCardAdjustmentPLU;
-(NSInteger)tempItemPLU;
-(NSInteger)loyaltyRewardsAdjustmentPLU;
-(BOOL)isDemoMode;

-(NSInteger)houseChargedAdjustmentPLU;
-(BOOL)houseChargeEnabled;
-(HouseChargeType)houseChargeType;
-(PromptForReceiptLoginType)promptForTransactionLoginForSales;

-(SalesReceiptTabType)receiptStartingPoint;
-(SalesOrderTabType)orderStartingPoint;
-(BOOL)alwaysOpenCashDrawer;
-(NSDecimal)cashDropWarningThreshold;
-(BOOL)warnIfNotEnoughFunds;
-(NSDecimal)cashExceedThreshold;
-(NSString *)cashExceedThresholdWarning;
-(BOOL)registryEnabled;
-(BOOL)sessionsEnabled;
-(NSMutableArray*)receiptVisibleTabsArray;
-(NSMutableArray*)orderVisibleTabsArray;
-(RequireFieldsType)returnItemRequireFields;
-(BOOL)showSrchBarcodeReturnTab;
-(BOOL)useOpenReturnsCustomPanel;

-(NSString *)inventoryItemCostCode;
-(BOOL)useLocalSalesReceiptTabs;
-(BOOL)useLocalSalesOrderTabs;
-(BOOL)passHeldReceiptToCTS;
-(BOOL)onlineOMSShipTo;
-(BOOL)requireCustomerOnSalesReceiptWithShipItems;
-(BOOL)doNotAllowShipItemWithQtyNotEqual1;
-(NSInteger)onlineOMSOrderExpiresInMin;
-(BOOL)doNotAllowShipToWithNoShipItem;

-(BOOL)requireCDCloseBeforePrinting;
-(CustomLookupDisplayType)customLookupDisplayType;

-(BOOL)requireCustomerInformationForTaxExempt;
-(BOOL)allowToChangeTaxAreaForSaleItems;
-(BOOL)allowNonZeroChangeDue;// "Manual select payment method for Change Due before Finalize"
-(BOOL)allowToChangeReceiptWithPayments;
-(NSString*)cashPaymentName;

// Tokens
-(BOOL) tokensEnabled;
//store credit
-(BOOL)storeCreditEnabled;
-(BOOL)promptToUseAvailableStoreCredit;
//Frequent Buyer
-(BOOL)frequentBuyerProgramEnable;

//membership
-(BOOL)customerPromptToSellMembership;
-(BOOL)customerPromptForMembershipUpgrade;
-(BOOL)customerPromptForMembershipRenewal;
-(NSInteger)customerMembershipRenewalNumberOfDays;
-(BOOL)customerUseLocationBasedMembership;
-(NSString *)customerMembershipLevel0Label;
-(NSString *)customerMembershipLevel0Description;

//Customers

-(SettingCheckEmailPhone)customerDuplicateEmailPhone;
-(BOOL)usePostalCodeToGenarateCountry;
-(BOOL)allowAddCustomerContact;

//Scheduler

-(BOOL)schedulerFeatureEnabled;

-(NSString *)tasServerLocation;
-(NSString *)customerHistoryReportName;

// Sales Orders
-(BPUUID*)defaultSellFromLocationId;
-(BPUUID *)defaultSaleCreditLocationId;
-(NSInteger)holdExpiresAfterNumDays;
-(BOOL)enableSalesOrders;
-(BOOL)sendSaleAutoApprove;
-(BOOL)expireLayawayOrders;

//Loyalty Reward program
-(BOOL)lrpEnabled;
-(NSInteger)lrpRate;
-(NSInteger)lrpAdjustmentPLU;
-(BOOL)lrpReturnAllowed;
-(BOOL)lrpMembershipRequired;

-(NSInteger)lrpGCAwardAmount;
-(NSInteger)lrpTokenAwardAmount;
-(LRPAward)lrpAwardType;
-(NSInteger)lrpAwardThreshold;
-(LRPProgramNumber)lrpProgramNumber;

//Real-Time Quantity service
-(NSString *)rtqServiceURL;
-(BOOL)rtqServiceEnabled;
-(BOOL)accItemAvailable;

-(BOOL)restrictToSingleDiscountType;

// Coupons
- (BOOL)couponLookupAvailable;
- (BOOL)enableCouponDiscounts;
- (BOOL)enableCustomerAssociatedCoupons;
- (BOOL)allowMultipleCouponDiscount;

-(NSInteger)autoLogout;
-(NSString *)autoLogoutAction;

-(NSInteger)useItemDescription;

//-(NSDecimalNumber*)cashRoundingPrecision;
-(BOOL)universalCustomersEnabled;

-(NSString*)MWMerchantName;

-(NSString*)MWMerchantKey;

-(NSString*)MWMerchantSiteId;

-(NSString*)MWForceDuplicate;

-(NSInteger)MWOperationTimeout;

-(BOOL)MWRequireAddressOnKeyed;
-(BOOL)MWUseAsShopper;

-(NSString*)eSelectStoreId;
-(NSString*)eSelectApiToken;
-(NSString*)eSelectHostURL;
-(NSString*)eSelectCanadaHostURL;
-(NSString*)eSelectTestCanadaHostURL;
-(NSString*)eSelectLiveCanadaHostURL;
-(NSString*)eSelectUSHostURL;
-(NSString*)eSelectTestUSHostURL;
-(NSString*)eSelectLiveUSHostURL;
-(NSString*)payworksHostURL;
-(NSString*)payworksMerchantId;
-(NSString*)payworksKey;
-(NSString*)payworkAPIMerchantId;
-(NSString*)payworkAPISecretKey;


-(BOOL)MWGeniusAssignToStoreReceipt;

-(BOOL)MWGeniusAssignToSalesReceipt;

-(NSString*)payPalAPIUrl;
-(NSString*)payPalLiveURL;
-(NSString*)payPalTestURL;
-(NSString*)payPalLocationId;

// CliSiTef settings
-(NSString*)clisitefTerminalId; // Replace with Terminal id code generation

// Custom app launch
-(NSString*)customAppLaunchButtonTitle;
-(NSString*)customeAppLaunchURL;

//Sales History
-(BOOL)showDeviceSalesHistory;
-(NSInteger)salesHistorySearchDays;
-(NSString *)salesHistorySearch;

// Gift Card constrains
-(NSDecimal)minAllowedGiftCardSellAmount;
-(NSDecimal)maxAllowedGiftCardSellAmount;
-(NSString *)giftCardNumberRegex;
-(NSString *)giftCardDisplayNumberRegex;
-(NSInteger)giftCardMaskLeft;
-(NSInteger)giftCardMaskRight;
-(BOOL)giftCardsClearNumber;
-(BOOL)giftCardsRequirePassword;
-(BOOL)giftCardsSellOnlyRegistered;

//items grids UI
-(BOOL)showCLUonItemsGrids;

//Printing settings
-(BOOL)includeHTMLBodyToEmail;
-(BOOL)attachPDFToEmail;

// Email validation
-(BOOL)useFreshAddressValidation;
//var
-(BOOL)cameraBarcodeReaderEnabled;
-(BOOL)roundTaxesBeforeAggregate;

// Main Menu URL Buttons
-(NSString *)mainMenuBtn1Label;
-(NSString *)mainMenuBtn1Url;
-(NSString *)mainMenuBtn2Label;
-(NSString *)mainMenuBtn2Url;
-(NSString *)mainMenuBtn3Label;
-(NSString *)mainMenuBtn3Url;
-(NSString *)mainMenuBtn4Label;
-(NSString *)mainMenuBtn4Url;
-(NSString *)mainMenuBtn5Label;
-(NSString *)mainMenuBtn5Url;
-(NSString *)mainMenuBtn6Label;
-(NSString *)mainMenuBtn6Url;

-(NSString *)cloudHQUrl;

//phone Validation
-(PhoneNumberFormat)phoneFormate;
-(NSString*)phoneNumberDefaultCountry;

//password validation
-(BOOL)usePciPassword;
-(BOOL)enablePasswordExpiration;
-(NSInteger)daysBeforePasswordExpires;


-(AltDTNType)altDNTType;
-(ShowDTNType)showDNTType;
-(MediaSourceType)mediaSource;

//tax free
-(BOOL)taxFreeEnabled;
-(BOOL)emailReadyForPickup;

//PluginXML
-(NSString *)pluginXml;

// DAM
-(NSString *)damURL;

//scandit barcode
-(BOOL)useScanditScanner;
-(NSString*)scanditToken;

// Database backup
-(BOOL)uploadBackupToGAE;

// BOMGAR
-(BOOL)bomgarEnabled;
-(NSString*)bomgarCompanyId;
-(NSString*)bomgarSiteAddress;

-(BOOL)useExtTaxService;
-(BPUUID*)shipItemTaxArea;
-(NSString*)extTaxServiceURL;
-(BOOL)canReturnShipToItems;

-(BPUUID*)defaultCustomerPresetId;
    
-(NSString*)supportPhone;
-(NSString*)supportEmail;
-(NSString*)supportText;
-(NSString*)companyDCSSDepartment;
    
@end
