//
//  SVSTransaction.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 1/24/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SVSTransaction.h"
#import "Settings.h"
#import "Location.h"
#import "NSDate+System.h"
#import "AppSettingManager.h"
#import "DecimalHelper.h"

@implementation SVSTransaction

-(id)init{
    self = [super init];
    if (self != nil) {
        self.status = SVSTransactionStatusDefault;
        self.deviceUniqueID = [AppSettingManager instance].deviceUniqueId;
        self.deviceName = [AppSettingManager instance].iPadId;
        self.locationID = [Location localLocation].id;
    }
    return self;
}


-(void)setStatus:(SVSTransactionStatus)status{
    _status = status;
    self.localTransactionTime = [NSDate systemDate];
    [self save];
}


-(BOOL)save{
    //update will be called in save+sqlite implementation
    //[self update];
    return self.status!=SVSTransactionStatusDefault && [super save];
}

- (void)dealloc {
    [_account release];
    [_receiptID release];
    [_receiptLineID release];
    [_transactionID release];
    [_employeeID release];
    [_managerOverrideID release];
    [_locationID release];
    [_deviceUniqueID release];
    [_deviceName release];
    [_programID release];
    [_localTransactionTime release];
    [super dealloc];
}

#pragma mark -
#pragma mark Editing
- (BOOL)shouldEncodeProperty:(NSString *)name{
    if ([name isEqualToString:@"_receiptCharge"]
        || [name isEqualToString:@"_receiptPayment"]
        || [name isEqualToString:@"_receipt"]
        || [name isEqualToString:@"_receiptItem"]
        || [name isEqualToString:@"_receiptDiscountCoupon"]
        ){
        return FALSE;
    }
    return [super shouldEncodeProperty:name];
}

@end
