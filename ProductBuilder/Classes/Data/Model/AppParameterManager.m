//
//  AppParameterManager.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AppParameterManager.h"
#include <sys/types.h>
#include <sys/sysctl.h>


@implementation AppParameterManager

#pragma mark - helper methods

@synthesize deviceName = _deviceName;
@synthesize bundleId = _bundleId;
@synthesize cardSwipe = _cardSwipe;
@synthesize employeeName = _employeeName;
@synthesize employeePassword = _employeePassword;
@synthesize skipLogin = _skipLogin;
@synthesize enableLogs = _enableLogs;
@synthesize disableUploads = _disableUploads;
@synthesize disableSVS = _disableSVS;
@synthesize disableDeviceAgent = _disableDeviceAgent;
@synthesize isDemoMode = _isDemoMode;
@synthesize useNewUI = _useNewUI;


#pragma mark - properties

-(BOOL)isUploadsDisabled {
    return _disableDeviceAgent | _disableUploads;
}

-(BOOL)isSvsDisabled {
    return _disableDeviceAgent | _disableSVS;
}

-(NSString *)systemName {
    
    return device.systemName;
}

-(NSString *)deviceSystemVersion {
    
    return device.systemVersion;
}

-(NSString *)deviceModel {
    
    return device.model;
}


#pragma mark - private
+(NSString *)pathForAppParametes {
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = documentPaths[0];
    return [documentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.json", APP_NAME]];
}


-(BOOL)loadSettingsFromFile {

    NSError *error = nil;
    NSString *jsonStr = [[[NSString alloc] initWithContentsOfFile:[AppParameterManager pathForAppParametes]
                                                         encoding:NSUTF8StringEncoding
                                                            error:&error] autorelease];
    
    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"”" withString:@"\""];
    NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    if (!jsonData.length)
        return NO;
    
    id object = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if(![object isKindOfClass:[NSDictionary class]])
        return NO;
    
    NSDictionary * jsonDic = (NSDictionary*)object;
    
    NSLog(@"App settings file loaded: %@\n%@\n", [AppParameterManager pathForAppParametes], jsonDic.description);
    
    if (jsonDic[@"deviceName"]) {
        self.deviceName = jsonDic[@"deviceName"];
    }
    if (jsonDic[@"bundleId"]) {
        self.bundleId = jsonDic[@"bundleId"];
    }
    if (jsonDic[@"cardSwipe"]) {
        self.cardSwipe = jsonDic[@"cardSwipe"];
    }
    if (jsonDic[@"cardSwipeGC"]) {
        self.cardSwipeGC = jsonDic[@"cardSwipeGC"];
    }
    if (jsonDic[@"employeeName"]) {
        self.employeeName = jsonDic[@"employeeName"];
    }
    if (jsonDic[@"employeePassword"]) {
        self.employeePassword = jsonDic[@"employeePassword"];
    }
    
    if (jsonDic[@"skipLogin"]) {
        self.skipLogin = [jsonDic[@"skipLogin"] boolValue];
    }
    if (jsonDic[@"enableSyncLogs"]) {
        self.enableLogs = [jsonDic[@"enableSyncLogs"] boolValue];
    }
    if (jsonDic[@"disableUploads"]) {
        self.disableUploads = [jsonDic[@"disableUploads"] boolValue];
    }
    if (jsonDic[@"disableSVS"]) {
        self.disableSVS = [jsonDic[@"disableSVS"] boolValue];
    }
    if (jsonDic[@"disableDeviceAgent"]) {
        self.disableDeviceAgent = [jsonDic[@"disableDeviceAgent"] boolValue];
    }
    if (jsonDic[@"disablePing"]) {
        self.disablePing = [jsonDic[@"disablePing"] boolValue];
    }
    if (jsonDic[@"isDemoMode"]) {
        self.isDemoMode = [jsonDic[@"isDemoMode"] boolValue];
    }
    if (jsonDic[@"useNewUI"]) {
        self.useNewUI = [jsonDic[@"useNewUI"] boolValue];
    }
    
    return YES;
}

-(void)saveSettingsToFile {
    
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:_deviceName, @"deviceName",
                         _bundleId, @"bundleId",
                         _cardSwipe ?: @"", @"cardSwipe",
                         _cardSwipeGC ?: @"", @"cardSwipeGC",
                         _employeeName ?: @"", @"employeeName",
                         _employeePassword ?: @"", @"employeePassword",
                         @(_enableLogs), @"enableSyncLogs",
                         @(_disableUploads), @"disableUploads",
                         @(_disableSVS), @"disableSVS",
                         @(_disableDeviceAgent), @"disableDeviceAgent",
                         @(_skipLogin), @"skipLogin",
                         @(_disablePing), @"disablePing",
                         @(_isDemoMode), @"isDemoMode",
                         @(_useNewUI), @"useNewUI",
                         nil];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
    if (jsonData) {
        
        NSString *currentPath = [AppParameterManager pathForAppParametes];

        NSString *jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
        if ([jsonString writeToFile:currentPath
                         atomically:YES
                           encoding:NSUTF8StringEncoding
                              error:&error]) {
            
            NSLog(@"Create app settings file: %@\n%@\n", currentPath, jsonDictionary.description);
        
            NSURL *currentPathURL = [NSURL fileURLWithPath:[AppParameterManager pathForAppParametes]];
            [currentPathURL setResourceValue:@YES
                                      forKey:NSURLIsExcludedFromBackupKey
                                       error:&error];
        }
    }
}

-(void)loadDeviceSettings {

    self.deviceName = device.name;
    self.bundleId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];

    BOOL isFileLoaded = [self loadSettingsFromFile];
    if (isFileLoaded) {
        
        return;
    }
    
#if TARGET_IPHONE_SIMULATOR
#ifdef DEBUG

    NSString *deviceName = nil, *bundleId = nil, *cardSwipeGC = nil, *cardSwipe = nil, *employeeName = nil, *employeePassword = nil;
    
    char* settingsDevName = getenv("RTDeviceName");
    if (settingsDevName) {
        
        deviceName = @(settingsDevName);
        if (deviceName.length) {
            
            self.deviceName = deviceName;
        }
    }
    
    char* settingsAppBundleId = getenv("RTAppBundleId");
    if (settingsAppBundleId) {
        
        bundleId = @(settingsAppBundleId);
        if (bundleId.length) {
            
            self.bundleId = bundleId;
        }
    }
    
    char* cardSwipeGCStr = getenv("RTCardSwipeGC");
    if (cardSwipeGCStr) {
        
        cardSwipeGC = @(cardSwipeGCStr);
        
        if (cardSwipeGC.length) {
            
            self.cardSwipeGC = cardSwipeGC;
        }
    }
    
    char* cardSwipeStr = getenv("RTCardSwipe");
    if (cardSwipeStr) {
        
        cardSwipe = @(cardSwipeStr);
        if (cardSwipe.length) {
        
            self.cardSwipe = cardSwipe;
        }
    }
    
    char* employeeNameStr = getenv("RTEmployeeName");
    if (employeeNameStr) {
        
        employeeName = @(employeeNameStr);
        if (employeeName.length) {
        
            self.employeeName = employeeName;
        }
    }
    
    char* employeePasswordStr = getenv("RTEmployeePassword");
    if (employeePasswordStr) {
        
        employeePassword = @(employeePasswordStr);
        if (employeePassword.length) {
            
            self.employeePassword = employeePassword;
        }
    }
    
    char* disablePingStr = getenv("RTDisableLoopPing");
    if (disablePingStr) {
        
        _disablePing = disablePingStr && (strcmp(disablePingStr, "YES") == 0 || strcmp(disablePingStr, "1") == 0 || strcmp(disablePingStr, "TRUE") == 0);
    }
    
    char* enableSyncLogsStr = getenv("RTLogSyncManagerOutput");
    if (enableSyncLogsStr) {
        
        _enableLogs = enableSyncLogsStr && (strcmp(enableSyncLogsStr, "YES") == 0 || strcmp(enableSyncLogsStr, "1") == 0 || strcmp(enableSyncLogsStr, "TRUE") == 0);
    }
    
    
    [self saveSettingsToFile];
#else
#endif //#ifdef DEBUG

#endif //#if TARGET_IPHONE_SIMULATOR
}


#pragma mark Singleton Methods
-(id)init{
    
	if ((self=[super init])){
        
        device = [[UIDevice currentDevice] retain];
        _hardware = [[self hardwareString] retain];
        _isDemoMode = _useNewUI = NO;
        [self loadDeviceSettings];
	}
	return self;
}


- (NSString*)hardwareString {
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}


-(HardwareType)hardwareType {
    
    if (_hardware && hardwareTypes[_hardware]) {
     
        return ((NSNumber *)(hardwareTypes[_hardware])).integerValue;
    }
    
    return HardwareTypeUnknown;
}

-(NSString*)hardwareDescription {
    
    HardwareType hType = self.hardwareType;

    if (hType != HardwareTypeUnknown && hardwareDescriprions[@(hType)]) {
        
        return hardwareDescriprions[@(hType)];
    }
        
    return [NSString stringWithFormat:@"Unknown: %@",_hardware];
}


static NSDictionary *hardwareTypes = nil;
static NSDictionary *hardwareDescriprions = nil;

static AppParameterManager *instance = nil;

+(AppParameterManager *)instance {
    
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        
        hardwareTypes = @{@"iPhone1,1": @(HardwareTypeiPhone2G),
                          @"iPhone1,2": @(HardwareTypeiPhone3G),
                          @"iPhone2,1": @(HardwareTypeiPhone3GS),
                          @"iPhone3,1": @(HardwareTypeiPhone4),
                          @"iPhone3,2": @(HardwareTypeiPhone4),
                          @"iPhone3,3": @(HardwareTypeiPhone4CDMA),
                          @"iPhone4,1": @(HardwareTypeiPhone4S),
                          @"iPhone5,1": @(HardwareTypeiPhone5),
                          @"iPhone5,2": @(HardwareTypeiPhone5GSMCDMA),
                          @"iPhone5,3": @(HardwareTypeiPhone5CGSMCDMA),
                          @"iPhone5,4": @(HardwareTypeiPhone5CCDMA),
                          @"iPhone6,1": @(HardwareTypeiPhone5S),
                          @"iPhone6,2": @(HardwareTypeiPhone5SGSMCDMA),
                          @"iPod1,1"  : @(HardwareTypeiPodTouch1Gen),
                          @"iPod2,1"  : @(HardwareTypeiPodTouch2Gen),
                          @"iPod3,1"  : @(HardwareTypeiPodTouch3Gen),
                          @"iPod4,1"  : @(HardwareTypeiPodTouch4Gen),
                          @"iPod5,1"  : @(HardwareTypeiPodTouch5Gen),
                          @"iPad1,1"  : @(HardwareTypeiPad),
                          @"iPad1,2"  : @(HardwareTypeiPad3G),
                          @"iPad2,1"  : @(HardwareTypeiPad2),
                          @"iPad2,2"  : @(HardwareTypeiPad2GSM),
                          @"iPad2,3"  : @(HardwareTypeiPad2CDMA),
                          @"iPad2,4"  : @(HardwareTypeiPad2),
                          @"iPad2,5"  : @(HardwareTypeiPadMini),
                          @"iPad2,6"  : @(HardwareTypeiPadMiniGSM),
                          @"iPad2,7"  : @(HardwareTypeiPadMiniGSMCDMA),
                          @"iPad3,1"  : @(HardwareTypeiPad3),
                          @"iPad3,2"  : @(HardwareTypeiPad3GSMCDMA),
                          @"iPad3,3"  : @(HardwareTypeiPad3GSM),
                          @"iPad3,4"  : @(HardwareTypeiPad4),
                          @"iPad3,5"  : @(HardwareTypeiPad4GSM),
                          @"iPad3,6"  : @(HardwareTypeiPad4GSMCDMA),
                          @"iPad4,1"  : @(HardwareTypeiPadAir),
                          @"iPad4,2"  : @(HardwareTypeiPadAirGSM),
                          @"iPad4,3"  : @(HardwareTypeiPadAirGSMCDMA),
                          @"iPad5,3"  : @(HardwareTypeiPadAir2),
                          @"iPad5,4"  : @(HardwareTypeiPadAir2),
                          @"iPad4,4"  : @(HardwareTypeiPadMini2),
                          @"iPad4,5"  : @(HardwareTypeiPadMini2GSM),
                          @"iPad6,7"  : @(HardwareTypeiPadProBig),
                          @"iPad6,8"  : @(HardwareTypeiPadProBig),
                          @"iPad6,3"  : @(HardwareTypeiPadPro),
                          @"iPad6,4"  : @(HardwareTypeiPadPro),
                          @"i386"     : @(HardwareTypeSimulator),
                          @"x86_64"   : @(HardwareTypeSimulator)};
        [hardwareTypes retain];
        hardwareDescriprions = @{@(HardwareTypeiPhone2G)        : @"iPhone 2G",
                                 @(HardwareTypeiPhone3G)        : @"iPhone 3G",
                                 @(HardwareTypeiPhone3GS)       : @"iPhone 3GS",
                                 @(HardwareTypeiPhone4)         : @"iPhone 4",
                                 @(HardwareTypeiPhone4CDMA)     : @"iPhone 4 (CDMA)",
                                 @(HardwareTypeiPhone4S)        : @"iPhone 4S",
                                 @(HardwareTypeiPhone5)         : @"iPhone 5",
                                 @(HardwareTypeiPhone5GSMCDMA)  : @"iPhone 5 (GSM+CDMA)",
                                 @(HardwareTypeiPhone5CGSMCDMA) : @"iPhone 5C (GSM/CDMA)",
                                 @(HardwareTypeiPhone5CCDMA)    : @"iPhone 5C (CDMA)",
                                 @(HardwareTypeiPhone5S)        : @"iPhone 5S",
                                 @(HardwareTypeiPhone5SGSMCDMA) : @"iPhone 5S (GSM/CDMA)",
                                 @(HardwareTypeiPodTouch1Gen)   : @"iPod Touch (1 Gen)",
                                 @(HardwareTypeiPodTouch2Gen)   : @"iPod Touch (2 Gen)",
                                 @(HardwareTypeiPodTouch3Gen)   : @"iPod Touch (3 Gen)",
                                 @(HardwareTypeiPodTouch4Gen)   : @"iPod Touch (4 Gen)",
                                 @(HardwareTypeiPodTouch5Gen)   : @"iPod Touch (5 Gen)",
                                 @(HardwareTypeiPad)            : @"iPad",
                                 @(HardwareTypeiPad3G)          : @"iPad 3G",
                                 @(HardwareTypeiPad2)           : @"iPad 2",
                                 @(HardwareTypeiPad2GSM)        : @"iPad 2 (GSM)",
                                 @(HardwareTypeiPad2CDMA)       : @"iPad 2 (CDMA)",
                                 @(HardwareTypeiPadMini)        : @"iPad Mini",
                                 @(HardwareTypeiPadMiniGSM)     : @"iPad Mini (GSM)",
                                 @(HardwareTypeiPadMiniGSMCDMA) : @"iPad Mini (GSM+CDMA)",
                                 @(HardwareTypeiPad3)           : @"iPad 3",
                                 @(HardwareTypeiPad3GSMCDMA)    : @"iPad 3 (GSM+CDMA)",
                                 @(HardwareTypeiPad3GSM)        : @"iPad 3 (GSM)",
                                 @(HardwareTypeiPad4)           : @"iPad 4",
                                 @(HardwareTypeiPad4GSM)        : @"iPad 4 (GSM)",
                                 @(HardwareTypeiPad4GSMCDMA)    : @"iPad 4 (GSM+CDMA)",
                                 @(HardwareTypeiPadAir)         : @"iPad Air",
                                 @(HardwareTypeiPadAirGSM)      : @"iPad Air (GSM)",
                                 @(HardwareTypeiPadAirGSMCDMA)  : @"iPad Air (GSM+CDMA)",
                                 @(HardwareTypeiPadAir2)        : @"iPad Air 2",
                                 @(HardwareTypeiPadMini2)       : @"iPad Mini 2",
                                 @(HardwareTypeiPadMini2GSM)    : @"iPad Mini 2 (GSM)",
                                 @(HardwareTypeiPadPro)         : @"iPad Pro (9.7 inch)",
                                 @(HardwareTypeiPadProBig)      : @"iPad Pro (12.9 inch)",
                                 @(HardwareTypeSimulator)       : @"Simulator"};
        [hardwareDescriprions retain];
    });
    
    @synchronized ([self class]) {
        
        if (instance)
            return instance;
        
        instance = [[AppParameterManager alloc] init];
    }
    return instance;
}


+(void)resetCache {
    
    @synchronized (self) {
        
        [instance release];
        instance = [[AppParameterManager alloc] init];
    }
}


- (id)retain {
	return self;
}

- (NSUInteger)retainCount {
	return UINT_MAX; //denotes an object that cannot be released
}

- (oneway void)release {
	// never release
}

- (id)autorelease {
	return self;
}

- (void)dealloc {
    
	// Should never be called, but just here for clarity really.
    
    [device release];
    [_deviceName release];
    [_bundleId release];
    [_cardSwipe release];
    [_cardSwipeGC release];
    [_employeeName release];
    [_employeePassword release];
    [_parameters release];
    [_hardware release];

    [super dealloc];
}

@end

