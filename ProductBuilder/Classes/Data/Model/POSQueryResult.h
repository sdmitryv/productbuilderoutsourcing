//
//  POSQueryResult.h
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CEntity.h"

@interface POSQueryResult : CEntity {
    BPUUID* queryID;
    NSString* result;
}

@property (nonatomic, retain) BPUUID* queryID;
@property (nonatomic, retain) NSString* result;

@end
