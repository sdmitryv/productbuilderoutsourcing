//
//  Country.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/13/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "CROEntity.h"

@interface Country : CROEntity {
	NSString *_code;
	NSString *_shortName;
}

@property(nonatomic,retain) NSString *code;
@property(nonatomic,retain) NSString *shortName;
+(Country*)getCountryByCode:(NSString*)code;
+(NSMutableArray*)getCountries;
@end
