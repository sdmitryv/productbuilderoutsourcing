//
//  ReturnReason.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/18/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "ReturnReason.h"

@implementation ReturnReason

@synthesize description = _description;

-(void) dealloc
{
	[_description release];
    [_defaultLogicalBinId release];
	[super dealloc];
}


+(NSMutableArray*)getReturnReasons{
	return nil;
}
@end
