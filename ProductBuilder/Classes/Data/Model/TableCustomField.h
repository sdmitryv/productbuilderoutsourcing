//
//  TableCustomField.h
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"


typedef NS_ENUM(NSUInteger, TableFieldType) { TableFieldTypeText = 0, TableFieldTypeLookup = 1, TableFieldTypeEmpty = 2, TableFieldTypeNumber = 3};

@interface TableCustomField : CEntity {

    NSString *_code;
    NSString *_fieldTypeStr;
    
    NSString *_title;
    NSString *_description;
    
    NSString *_lookupTable;
    NSString *_lookupTablePK;
    NSString *_lookupTableDescription;
    NSString *_lookupText;
    NSString *_lookupDefaultText;
    BOOL _require;
    
    NSString *_stringValue;
    NSNumber *_numberValue;
    BPUUID *_lookupId;
}

@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *fieldTypeStr;
@property (nonatomic, readonly) TableFieldType fieldType;

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *description;

@property (nonatomic, retain) NSString *lookupTable;
@property (nonatomic, retain) NSString *lookupTablePK;
@property (nonatomic, retain) NSString *lookupTableDescription;
@property (nonatomic, retain) NSString *lookupText;
@property (nonatomic, retain) NSString *lookupDefaultText;
@property (nonatomic, assign) BOOL require;

@property (nonatomic, retain) NSString *stringValue;
@property (nonatomic, retain) NSNumber *numberValue;
@property (nonatomic, retain) BPUUID *lookupId;

@end
