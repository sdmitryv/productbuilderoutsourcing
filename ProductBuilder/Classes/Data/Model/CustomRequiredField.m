//
//  CustomRequiredField.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/14/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "CustomRequiredField.h"

@implementation CustomRequiredField

- (void) dealloc
{
    [_requireSymbol release];
    [_fieldName release];
    [_fieldLabel release];
    
    [super dealloc];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ %@ %@", _fieldName, _fieldLabel, _requireSymbol];
}

+ (NSArray *)getRequiredFieldsListForArea:(CustomRequiredFieldArea)area {
    return nil;
}

+ (NSArray *)getCaseSensitiveFieldsListForArea:(CustomRequiredFieldArea)area {
    return nil;
}

+ (CustomRequiredField *)getRequiredFieldByName:(NSString*)fieldName forArea:(CustomRequiredFieldArea)area {
    NSString* lowerFieldName = [fieldName lowercaseString];
    for (CustomRequiredField* field in [CustomRequiredField getRequiredFieldsListForArea:area]) {
        if ([field.fieldName isEqualToString:lowerFieldName]) {
            return field;
        }
    }
    return nil;
}

+ (NSArray *)getSearchFieldsListForArea:(CustomRequiredFieldArea)area {
    return nil;
}

@end
