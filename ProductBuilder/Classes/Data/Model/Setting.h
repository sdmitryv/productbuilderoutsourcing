//
//  Setting.h
//  StockCount
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"
#import "BPUUID.h"

typedef NS_ENUM(NSUInteger, SettingType) { SettingTypeCompany, SettingTypeLocation, SettingTypeWorkstation };

extern NSString* const settingWorkstation;
extern NSString* const settingCompany;
extern NSString* const settingLocation;

@interface Setting : CROEntity {
	NSString	* _type;
	NSString	* _key;
	NSString	* _value;
    BPUUID*     _designate;
	
}

@property (nonatomic, retain) NSString	*type;
@property (nonatomic, retain) NSString	*key;
@property (nonatomic, retain) NSString	*value;
@property (nonatomic, retain)BPUUID* designate;

+ (NSMutableArray *)getSettings;
+ (Setting *)getSettingForKey:(NSString *)key type:(SettingType)type;

+ (NSString*)stringForKey:(NSString *)key type:(SettingType)type;
+ (BOOL)boolForKey:(NSString *)key type:(SettingType)type;
+ (NSInteger)intForKey:(NSString *)key type:(SettingType)type;

+ (NSString*)stringForKey:(NSString *)key  type:(SettingType)type defaultValue:(NSString*)defaultValue;
+ (BOOL)boolForKey:(NSString *)key  type:(SettingType)type defaultValue:(BOOL)defaultValue;
+ (NSInteger)intForKey:(NSString *)key  type:(SettingType)type defaultValue:(NSInteger)defaultValue;
+ (NSDecimal)decimalForKey:(NSString *)key type:(SettingType)type defaultValue:(NSDecimal)defaultValue;

@end
