//
//  Product.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import "CEntity.h"

@interface Product : CEntity

@property (nonatomic, retain) NSString * clu;
@property (nonatomic, retain) NSMutableArray * elements;
@property (nonatomic, retain) NSMutableArray * modifications;
@property (nonatomic, retain) NSString * instructions;
@property (nonatomic, retain) NSDecimalNumber *price;
@property (nonatomic, retain) NSDecimalNumber *cost;
@property (nonatomic, retain) NSString *taxCategoryCode;
@property (nonatomic, retain) NSString *manufacturerName;
@property (nonatomic, retain) NSString *department;
@property (nonatomic, retain) NSString *procuctClass;
@property (nonatomic, retain) NSString *primaryVendor;

@property (nonatomic, readonly) NSString * elementsDescription;
@property (nonatomic, readonly) NSString * modificationsDescriptions;

@property (nonatomic, readonly) NSString * longDescription;
@property (nonatomic, retain) NSString * storeDescription;

+(NSMutableArray*)getProducts;

@end
