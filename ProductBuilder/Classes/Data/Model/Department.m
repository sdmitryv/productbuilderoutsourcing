//
//  Department.m
//  ProductBuilder
//
//  Created by Valera on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Department.h"

@implementation Department
@synthesize name;
@synthesize level;
@synthesize parent;
@synthesize classificationType;

-(NSArray*) subLevels{
	return subLevels;
}

+(NSArray*)getDepartments:(BOOL)alt{
    return nil;
}

-(void)dealloc{
	[name release];
	[parent release];
	[subLevels release];
	[classificationType release];
	[super dealloc];
}

@end

