//
//  CEntityList.h
//  CloudworksPOS
//
//  Created by valera on 8/4/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

@interface CEntityList : NSObject<NSCopying, NSMutableCopying, NSCoding, NSFastEnumeration, EditableObject> {
    NSMutableArray* list;
    NSMutableArray* deletedList;
    NSUInteger _editLevel;
    NSMutableArray* savedStates;
    BOOL _isCancellingEdit;
    BOOL _isBeginningEdit;
    BOOL _isEndingEdit;
    CEntity* owner;
    NSInteger _unsafeEdit;
    NSInteger _ignoringReadonly;
}
-(id)initWithList:(NSMutableArray*)aList;
-(id)initWithOwner:(id<CEntity>)anOwner;
- (NSMutableArray*)list;
- (NSArray*)deletedList;
- (NSUInteger)count;
- (id)objectAtIndex:(NSUInteger)index;
- (NSArray *)objectsAtIndexes:(NSIndexSet *)indexes;
- (NSUInteger)indexOfObject:(id)anObject;
- (BOOL)containsObject:(id)anObject;
- (void)addObject:(id<CEntity>)anObject;
- (void)addObjectsFromArray:(NSArray *)otherArray;
- (void)insertObject:(id<CEntity>)anObject atIndex:(NSUInteger)index;
- (void)objectAdded:(id<CEntity>)anObject;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)removeObjectAtIndexes:(NSIndexSet*)indexSet;
- (void)removeAllObjects;
- (void)removeObject:(id<CEntity>)anObject;
- (void)objectRemoved:(id<CEntity>)anObject;
- (void)sortUsingDescriptors:(NSArray *)sortDescriptors;
- (void)enumerateObjectsUsingBlock:(void (^)(id obj, NSUInteger idx, BOOL *stop))block;
- (id)objectAtIndexedSubscript:(NSUInteger)idx;
- (id)objectForKeyedSubscript:(id <NSCopying>)key;
- (BOOL)isDirty;
- (void)markNew;
- (void)markClean;
-(BOOL)canAddObject:(NSError**)error;
-(BOOL)canRemoveObject:(id<CEntity>)object error:(NSError**)error;
-(void)beginUnsafeListEditing;
-(void)endUnsafeListEditing;
@property (nonatomic, assign)id<CEntity> owner;
@property (nonatomic, readonly)BOOL isBeginningEdit;
@property (nonatomic, readonly)BOOL isEndingEdit;
@property (nonatomic, readonly)BOOL isCancellingEdit;
@end
