//
//  BPUUID.m
//  Skates
//
//  Created by Valery Fomenko on 2/3/10.
//  Copyright 2010 Cloudworks Ltd. All rights reserved.
//

#import "BPUUID.h"

@implementation BPUUID

#pragma mark -
#pragma mark Construction and deallocation

+ (BPUUID *)UUID {
	return [[[self.class alloc]init]autorelease];
}

+ (BPUUID *)UUIDWithString:(NSString *)uuidString {
	if (uuidString != nil) {
		return [[[BPUUID alloc] initWithString:uuidString] autorelease];
	}
	return nil;
}

+ (BPUUID *)UUIDWithBytes:(NSData *)uuidBytes {
	return [[[BPUUID alloc]initWithBytes:uuidBytes] autorelease];
}

+ (BPUUID *)UUIDWithUUIDBytes:(uuid_t)bytes{
    return [[[BPUUID alloc] initWithUUIDBytes:bytes] autorelease];
}

-(id)init{
    if ((self =[super init])){
        _uuid = [[NSUUID alloc]init];
    }
    return self;
}

-(id)initWithNSUUID:(NSUUID*)uuid{
    if ((self =[super init])){
        _uuid = [uuid retain];
    }
    return self;
}

- (id)initWithString:(NSString *)uuidString {
    if ((self =[super init])){
        _uuid = [[NSUUID alloc]initWithUUIDString:uuidString];
        if (!_uuid){
            [self release];
            return nil;
        }
    }
	return self;
}

- (id)initWithBytes:(NSData *)data {
    if ((self =[super init])){
        uuid_t bytes;
        assert(sizeof(bytes) == [data length]);
        [data getBytes:&bytes length:sizeof(bytes)];
        _uuid = [[NSUUID alloc]initWithUUIDBytes:bytes];
        if (!_uuid){
            [self release];
            return nil;
        }
    }
	return self;
}

- (id)initWithUUIDBytes:(uuid_t)bytes {
    if ((self =[super init])){
        _uuid = [[NSUUID alloc]initWithUUIDBytes:bytes];
        if (!_uuid){
            [self release];
            return nil;
        }
    }
	return self;
}

- (void)dealloc {
    [_uuid release];
	[super dealloc];
}

#pragma mark -
#pragma mark Accessors

//@synthesize CFUUID;

#pragma methods
-(BOOL)isEmpty{
    return [self isEqual:self.class.empty];
}

+(BPUUID*)empty{
    static dispatch_once_t pred;
    static BPUUID *empty = nil;
    dispatch_once(&pred, ^{
        uuid_t bytes;
        uuid_clear(bytes);
        empty = [[self alloc] initWithUUIDBytes:bytes];
    });
    return empty;
}

#pragma mark -
#pragma mark UUID Representations

- (NSString *)stringRepresentation {
	return [_uuid UUIDString];
}

- (NSData *)byteRepresentation {
    uuid_t uuidBytes;
    [_uuid getUUIDBytes:uuidBytes];
	return [NSData dataWithBytes:uuidBytes length:sizeof(uuidBytes)];
}

//free memory after use!
-(char*)UTF8String{
    char* uuid_string = calloc(37, sizeof(char));
    uuid_t uuidBytes;
    [_uuid getUUIDBytes:uuidBytes];
    uuid_unparse(uuidBytes, uuid_string);
    return uuid_string;
}

- (void)getUUIDBytes:(uuid_t)uuid{
    return [_uuid getUUIDBytes:uuid];
}


#pragma mark -
#pragma mark Description

- (NSString *)description {
    return [_uuid UUIDString];
}

#pragma mark -
#pragma mark NSCoding implementation

- (id)initWithCoder:(NSCoder *)decoder {
	NSString *uuidString = [decoder decodeObjectForKey:@"UUID"];
	return [self initWithString:uuidString];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:[self stringRepresentation] forKey:@"UUID"];
}

#pragma mark -
#pragma mark isEqual implementation
- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![other isKindOfClass:[BPUUID class]])
        return NO;
    return [_uuid isEqual:((BPUUID*)other)->_uuid];
}

- (NSUInteger) hash{
    return [_uuid hash];
}

#pragma mark
#pragma mark NSCopying implementation

- (id) copy {
    return [self retain];
}
- (id)copyWithZone:(NSZone *)zone
{
    return [[[self class] allocWithZone: zone] initWithNSUUID:_uuid];
}

@end