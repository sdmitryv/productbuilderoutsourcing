//
//  TimeZone.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface TimeZone:CROEntity {
    NSString    * _name;
    NSString    * _description;
    BOOL          _isDeleted;
    NSDecimal     _utcOffset;
}

@property (nonatomic, retain) NSString  * name;
@property (nonatomic, assign) BOOL        isDeleted;
@property (nonatomic, assign) NSDecimal   utcOffset;
-(void)setDescription:(NSString *)description;

+(NSMutableArray*)getTimeZones;

@end
