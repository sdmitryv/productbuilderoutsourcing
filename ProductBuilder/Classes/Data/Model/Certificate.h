//
//  Certificate.h
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/16/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CROEntity.h"

@interface Certificate : CROEntity

@property (nonatomic, retain)NSString* fileName;
@property (nonatomic, retain)NSData* fileContent;

- (NSData*) encryptValue:(NSString*)value;
+(Certificate*)getLatesCertificateByName:(NSString*)name;

+(NSData*)ecnryptString:(NSString*)string;

@end
