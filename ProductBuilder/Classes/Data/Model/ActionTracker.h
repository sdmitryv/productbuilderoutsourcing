//
//  EventLog.h
//  ProductBuilder
//
//  Created by valera on 10/10/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BPUUID.h"

@class EGODatabase;

@interface ActionTracker : NSObject{
    }

+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId;
+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId reason:(NSString*)reason;
+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId reason:(NSString*)reason database:(EGODatabase *)db;


+(void)logOpenCashDrawer:(BPUUID*)objectID employeeId:(BPUUID*)employeeId documentType:(NSString *)documentType reason:(NSString*)reason details:(NSString*)details;

+(void)logDocumentStarted:(BPUUID*)objectId employeeId:(BPUUID*)employeeId documentType:(NSString *)documentType;

+(void)logCreateDrawerMemoWithID:(BPUUID *)drawerMemoId;

+(void)updateReceipt:(BPUUID *)receiptId fields:(NSDictionary *)fieldsList database:(EGODatabase *)db;

@end
