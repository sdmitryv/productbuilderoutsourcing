//
//  SecurityLog.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 9/10/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "SecurityLog.h"
#import "NSDate+System.h"

@implementation SecurityLog

@synthesize description, destEmployeeId, docLineType, docType, documentId, documentLineId, localLogDate, roleCode, srcEmployeeId, utcLogDate;

-(id)initWithReceipt:(BPUUID*)receiptId rcptItemline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription {
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        
        self.documentId = receiptId;
        self.docType = DocumentTypeSalesReceipt;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypeItem;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}

-(id)initWithReceipt:(BPUUID*)receiptId rcptChargeline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription {
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = receiptId;
        self.docType = DocumentTypeSalesReceipt;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypeCharge;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}

-(id)initWithReceipt:(BPUUID*)receiptId rcptPaymentline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription {
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = receiptId;
        self.docType = DocumentTypeSalesReceipt;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypePayment;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}

-(id)initWithReceipt:(BPUUID*)receiptId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription {
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = receiptId;
        self.docType = DocumentTypeSalesReceipt;
        self.documentLineId = nil;
        self.docLineType = DocumentLineTypeNone;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}

/* ---------------- ORDER Init Methods ------------------------ */

-(id)initWithOrder:(BPUUID*)orderId orderItemline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription{
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = orderId;
        self.docType = DocumentTypeSalesOrder;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypeItem;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}
-(id)initWithOrder:(BPUUID*)orderId orderChargeline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription{
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = orderId;
        self.docType = DocumentTypeSalesOrder;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypeCharge;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
    
}
-(id)initWithOrder:(BPUUID*)orderId orderPaymentline:(BPUUID*) lineId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription{
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = orderId;
        self.docType = DocumentTypeSalesOrder;
        self.documentLineId = lineId;
        self.docLineType = DocumentLineTypePayment;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
    
}
-(id)initWithOrder:(BPUUID*)orderId srcEmployee:(BPUUID*)sourceEmployee destEmployee:(BPUUID*)destEmployee roleCode:(NSString*)aroleCode description:(NSString*)adescription{
    if (self = [self init]) {
        if (!destEmployee) {
            destEmployee = sourceEmployee;
        }
        self.documentId = orderId;
        self.docType = DocumentTypeSalesOrder;
        self.documentLineId = nil;
        self.docLineType = DocumentLineTypeNone;
        self.srcEmployeeId = sourceEmployee;
        self.destEmployeeId = destEmployee;
        self.roleCode = aroleCode;
        self.description = adescription;
        self.localLogDate = [NSDate systemDate];
        self.utcLogDate = [NSDate date]; // UTC Date
    }
    
    return self;
}

-(void)dealloc {
    self.documentId = nil;
    self.documentLineId = nil;
    self.srcEmployeeId = nil;
    self.destEmployeeId = nil;
    self.roleCode = nil;
    self.description = nil;
    self.localLogDate = nil;
    self.utcLogDate = nil;
    
    [super dealloc];
}

+(NSMutableArray*)getSecurityLogsForReceipt:(BPUUID*)receiptId {
    return nil;
}

+(NSMutableArray*)getSecurityLogsForOrder:(BPUUID*)orderId {
    return nil;
}

@end
