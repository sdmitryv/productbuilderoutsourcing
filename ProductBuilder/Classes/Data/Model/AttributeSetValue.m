//
//  AttributeSetValue.m
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AttributeSetValue.h"
#import "NSString+Ext.h"

@implementation AttributeSetValue

@synthesize attributeSetId = _attributeSetId;
@synthesize setCode = _setCode;
@synthesize value = _value;
@synthesize alias = _alias;
@synthesize alias2 = _alias2;
@synthesize order = _order;

-(id)init {
    
	if ((self = [super init])) {

	}
    
	return self;
}

-(void)initInternal{
    
    [super initInternal];
}


-(void) dealloc {
    
	[_attributeSetId release];
	[_setCode release];
    [_value release];
    [_alias release];
    [_alias2 release];
    
	[super dealloc];
}


+(NSMutableArray *)loadAttributeSetValuesForSetCode:(NSString *)code { return nil; }
+(NSMutableArray *)loadAttributeSetValuesForSetCode:(NSString *)code attrName:(NSString *)attrName andStyle:(BPUUID *)styleId{ return nil; }
+(BOOL)hasAttributeSetValuesWithAlias:(NSString *)code { return YES; }

@end
