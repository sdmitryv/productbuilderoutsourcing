//
//  SVSTransaction.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 1/24/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"
#import "StoredValuesService.h"

typedef NS_ENUM(NSUInteger, SVSTransactionType) {
    SVSTransactionTypeNone = 0,
    SVSTransactionTypeGiftCard = 1,
    SVSTransactionTypeCoupon = 2,
    SVSTransactionTypeToken = 3,
    SVSTransactionTypeHouseCharge = 4,
    SVSTransactionTypeStoreCredit = 5,
    SVSTransactionTypeLRP = 6,
    SVSTransactionTypeReceipt = 7,
};

typedef NS_ENUM(NSInteger, SVSTransactionStatus) {
    SVSTransactionStatusDefault = -1,
    SVSTransactionStatusAuthorize = 0,
    SVSTransactionStatusAuthorized = 1,
    SVSTransactionStatusAuthorizeTimeout = 2,
    SVSTransactionStatusAuthorizeFailed = 3,
    SVSTransactionStatusPreCapture = 4,
    SVSTransactionStatusCaptured = 5,
    SVSTransactionStatusDiscarded = 6,
    SVSTransactionStatusDiscardFailed = 7,
    SVSTransactionStatusCreate = 8,
    SVSTransactionStatusOfflineToInactive = 9
};

@class Receipt;
@class ReceiptItem;
@class ReceiptCharge;
@class ReceiptPayment;
@class GiftCard;
@class ReceiptDiscountCoupon;

@interface SVSTransaction : CEntity{
    SVSTransactionStatus _status;
}

@property(nonatomic, assign) SVSTransactionType    type;
@property(nonatomic, retain) NSString   * account;
@property(nonatomic, assign) SVSTransactionStatus status;
@property(nonatomic, assign) NSDecimal    amount;
@property(nonatomic, retain) BPUUID     * receiptID;
@property(nonatomic, retain) BPUUID     * receiptLineID;
@property(nonatomic, assign) SVSLineType  receiptLineType;
@property(nonatomic, retain) NSString   * transactionID;
@property(nonatomic, retain) BPUUID     * employeeID;
@property(nonatomic, retain) BPUUID     * managerOverrideID;
@property(nonatomic, assign) BOOL         isOffline;
@property(nonatomic, assign) NSDecimal    afterBalance;
@property(nonatomic, retain) BPUUID     * locationID;
@property(nonatomic, retain) BPUUID     * deviceUniqueID;
@property(nonatomic, retain) NSString   * deviceName;
@property(nonatomic, retain) NSString   * programID;
@property(nonatomic, retain) NSDate     * localTransactionTime;
@property(nonatomic, assign) BOOL isRefund;

@end
