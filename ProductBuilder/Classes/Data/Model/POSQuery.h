//
//  POSQuery.h
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CROEntity.h"

@interface POSQuery : CROEntity {
    BPUUID* deviceID;
    BPUUID* locationID;
    NSString* queryStr;
}

@property (nonatomic, readonly)BPUUID* deviceID;
@property (nonatomic, readonly)BPUUID* locationID;
@property (nonatomic, readonly)NSString* queryStr;

+(NSMutableArray*)getActiveQueries;

@end
