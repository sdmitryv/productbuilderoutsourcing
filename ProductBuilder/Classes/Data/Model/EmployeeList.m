//
//  EmployeeList.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EmployeeList.h"
#import "Employee.h"
#import "CROEntity+Sqlite.h"
#import "NSDate+Compare.h"

@implementation EmployeeList

@synthesize filterCondition;
@synthesize lzController;
@synthesize sortOrderList;

-(id)init {
    
    if (self = [self init:NO]) {
        
    }
    
    return self;
}

-(id)init:(BOOL)isAll {
    
    if ((self = [super init])) {
        
        _lzController = [[LazyLoadingController alloc] initWithLazyLoadable:self];
        _command = [[SqlCommand alloc] initWithStmt:isAll ? employeeSelectAllStm : employeeSelectStm];
        _command.sortOrderList = @[[SqlSortOrder orderBy:@"DisplayName collate nocase" order:SqlSortOrderAscending]];
    }
    
    return self;
}

-(SqlFilterCondition*) filterCondition
{
	return _command.filterCondition;
}

-(NSArray*) sortOrderList
{
	return _command.sortOrderList;
}

-(void)setFilterCondition:(SqlFilterCondition*)_filterCondition
{
    
	_command.filterCondition = [SqlAndCondition list:_filterCondition, [SqlOrCondition list: [SqlFieldCondition condition:@"ExpirationDate" compareOperator:Null value:nil],  [SqlFieldCondition condition:@"ExpirationDate" compareOperator:GreaterOrEqual value:NSDate.date.dateOnly], nil], nil];
	[_lzController reset];
}

-(void)setSortOrderList:(NSArray*) _sortOrderList
{
	_command.sortOrderList = _sortOrderList;
	[_lzController reset];
}


-(SqlCommand*) command
{
	return _command;
}

+(NSObject<ILazyItem>*)getItem:(EGODatabaseRow*)row
{
	return[Employee instanceFromRow:row];
}

-(Employee*)objectAtIndex:(NSUInteger)index {
    return [_lzController objectAtIndex:index];
}

-(Employee*)objectAtIndexedSubscript:(NSUInteger)index{
    return [_lzController objectAtIndex:index];
}

-(Employee*)objectByRowid:(NSNumber *)rowid {
    return [_lzController objectByRowid:rowid];
}

-(NSUInteger)indexOfObject:(Employee*)employee {
    return [_lzController indexOfObject:employee];
}

-(NSUInteger) count {
    return [_lzController count];
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])stackbuf count:(NSUInteger)len
{
	return [_lzController countByEnumeratingWithState:state objects:stackbuf count:len];
}

-(void)dealloc
{
	[_lzController release];
	[_command release];
    [filterCondition release];
    [sortOrderList release];

    [super dealloc];
}

-(void)refresh{
	[_lzController reset];
}

@end
