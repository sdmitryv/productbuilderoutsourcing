//
//  CustomField.h
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 7/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"


typedef NS_ENUM(NSUInteger, CustomFieldArea) {
    CustomFieldAreaInventory = 0,
    CustomFieldAreaCustomer = 1,
    CustomFieldAreaVendor = 2,
    CustomFieldAreaLocation = 3,
    CustomFieldAreaPurchase = 4,
    CustomFieldAreaSales = 5,
    CustomFieldAreaTransfer = 6,
    CustomFieldAreaAdjustment = 7,
    CustomFieldAreaPurchaseLine = 8,
    CustomFieldAreaSalesLine = 9,
    CustomFieldAreaTransferLine = 10,
    CustomFieldAreaAdjustmentLine = 11,
    CustomFieldAreaReserved = 12,
    CustomFieldAreaCustomerContact = 13,
    CustomFieldAreaVendorContact = 14,
    CustomFieldAreaSalesperson = 15,
    CustomFieldAreaSerialNumber = 16,
    CustomFieldAreaCustomerCustomField = 17,
    CustomFieldAreaItemCustomField = 18,
    CustomFieldAreaItem = 19,
    CustomFieldAreaCategory = 20,
    CustomFieldAreaReceiptPayment = 21,
    CustomFieldAreaOrderHeader = 22,
    CustomFieldAreaOrderItem = 23,
    CustomFieldAreaInventoryItem = 31
};

typedef NS_ENUM(NSUInteger, CustomFieldType) { CustomFieldTypeText = 0, CustomFieldTypeDate = 1, CustomFieldTypeInteger = 2, CustomFieldTypeDecimal = 3, CustomFieldTypeFlag = 4, CustomFieldTypeLookup = 5, CustomFieldTypeLongText = 6 };

@interface CustomField : CROEntity {
    NSString *_label;
    NSString *_defaultLabel;
    NSInteger _type;
    NSInteger _customFieldType;
    NSInteger _customFieldNumber;
    NSInteger _language;
    BOOL _used;
    BOOL _secure;
}

@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *defaultLabel;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger customFieldType;
@property (nonatomic, assign) NSInteger customFieldNumber;
@property (nonatomic, assign) NSInteger language;
@property (nonatomic, assign) BOOL used;
@property (nonatomic, assign) BOOL Secure;
@property (nonatomic, assign) BOOL isRequired;
@property (nonatomic, assign) NSInteger listOrder;
@property (nonatomic, readonly) NSString *caption;

+(NSString*)getCaption:(CustomFieldArea)areatype fieldtype:(CustomFieldType)fieldtype fieldNumber:(NSInteger)fieldnumber;
+(NSArray*)getCustomFieldControls:(CustomFieldArea)areaType fieldType:(CustomFieldType)fieldType fieldNum:(NSInteger)fieldNum;
+(BOOL)getSecure:(CustomFieldArea)areaType fieldType:(CustomFieldType)fieldType fieldNum:(NSInteger)fieldNum;

+(NSArray*)getCustomFieldsByArea:(NSInteger)area;

@end
