//
//  OnlineOMSTransaction.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 1/9/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CEntity.h"

typedef NS_ENUM(NSUInteger,OnlineOMSTransactionStatus) {
    OnlineOMSTransactionStatusNone = 0,
    OnlineOMSTransactionPreConfirmed = 1,
    OnlineOMSTransactionConfirmed = 2,
};

@interface OnlineOMSTransaction : CEntity

@property (nonatomic, retain) BPUUID *receiptId;
@property (nonatomic, retain) NSString *orderId;
@property (nonatomic, assign) OnlineOMSTransactionStatus status;
@property (nonatomic, retain) NSDate *firstItemDateTime;
@property (nonatomic, retain) NSDate *preConfirmDateTime;
@property (nonatomic, retain) NSDate *confirmDateTime;

+ (OnlineOMSTransaction *)onlineOMSTransactionForReceiptWithId:(BPUUID *)receiptId;

@end
