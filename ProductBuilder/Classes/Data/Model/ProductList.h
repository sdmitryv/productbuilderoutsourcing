//
//  ProductList.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/7/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LazyLoadingController.h"
#import "IObjectList.h"

@class Product;

extern NSString * const productSelectStm;

@interface ProductList : NSObject<ILazyLoadableList, IObjectList, NSFastEnumeration> {
    LazyLoadingController* _lzController;
    SqlCommand* _command;
}

-(Product*)objectAtIndex:(NSInteger)index;
-(Product*)objectByRowid:(NSNumber *)rowid;
-(NSInteger)indexOfObject:(Product*)item;
-(NSInteger)count;

@property (nonatomic, retain) SqlFilterCondition* filterCondition;
@property (nonatomic, retain) NSArray* sortOrderList;
@property (nonatomic,readonly) LazyLoadingController* lzController;

@end
