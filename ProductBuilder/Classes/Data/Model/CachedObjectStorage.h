//
//  CachedObjectStorage.h
//  ProductBuilder
//
//  Created by valery on 7/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CachedObjectStorage : NSObject

-(id)objectByName:(NSString*)name class:(Class)aClass;
+(instancetype)instance;
@end
