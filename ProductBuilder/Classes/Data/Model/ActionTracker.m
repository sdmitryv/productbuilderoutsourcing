//
//  EventLog.m
//  ProductBuilder
//
//  Created by valera on 10/10/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "ActionTracker.h"
#import <libxml/xmlstring.h>
#import "SecurityManager.h"
#import "USAdditions.h"
#import "DataManager.h"

@implementation ActionTracker


+(void)logOpenCashDrawer:(BPUUID*)objectId employeeId:(BPUUID*)employeeId documentType:(NSString *)documentType reason:(NSString*)reason details:(NSString*)details{
    
    xmlDocPtr doc = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr root = xmlNewDocNode(doc, NULL, [@"CashDrawerOpen" xmlString], NULL);
    xmlDocSetRootElement(doc, root);
    xmlNodePtr node = xmlNewDocRawNode(doc, NULL, [@"OpenType" xmlString], [details xmlString]);
    xmlAddChild(root, node);
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    xmlFreeDoc(doc);
    if (buf){
        NSString *str = @((const char*)buf);
        xmlFree(buf);

        [ActionTracker logAction:@"CashDrawerOpened"
                         details:str
                        objectID:objectId
                        parentID:nil
                     documetType:documentType
                      employeeId:employeeId
                         reason:reason];
    }
}


+(void)logDocumentStarted:(BPUUID*)objectId employeeId:(BPUUID*)employeeId documentType:(NSString *)documentType{
    
    [ActionTracker logAction:@"DocumentStarted"
                     details:nil
                    objectID:objectId
                    parentID:nil
                 documetType:documentType
                  employeeId:employeeId
                      reason:nil];
}




-(void)dealloc{
    
    
    [super dealloc];
}


-(void)propertyChanged:(id)sender keyPath:(NSString*)keyPath value:(id)value{
    
}





+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId {
    
    [self logAction:action details:details objectID:objectID parentID:parentID documetType:documentType employeeId:employeeId reason:nil];
}

+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*)objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId reason:(NSString*)reason{

    [self logAction:action details:details objectID:objectID parentID:parentID documetType:documentType employeeId:employeeId reason:reason database:[DataManager instance].currentDatabase];
}

+(void)logAction:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId reason:(NSString*)reason database:(EGODatabase *)db {
    
}


+(NSString*)serializeEmployee:(Employee*)employee{
    
    xmlDocPtr doc = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    xmlDocSetRootElement(doc,[[employee getProperties] xmlNodeForDoc:doc elementName:@"Employee" elementNSPrefix:nil]);
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    xmlFreeDoc(doc);
    NSString *str = nil;
    if (buf){
        str = @((const char*)buf);
        xmlFree(buf);
    }
    return str;
}


+(NSString*)serializeValue:(NSString *)value withTagName:(NSString *)tagName withRootElem:(BOOL)withRootElem{
    
    xmlDocPtr doc = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    
    if (withRootElem)
        xmlDocSetRootElement(doc,[@{tagName:value} xmlNodeForDoc:doc elementName:@"root" elementNSPrefix:nil]);
    else
        xmlDocSetRootElement(doc, [value xmlNodeForDoc:doc elementName:tagName elementNSPrefix:nil]);
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    xmlFreeDoc(doc);
    NSString *str = nil;
    if (buf){
        str = @((const char*)buf);
        xmlFree(buf);
    }
    return str;
}


+(void)logLogout:(Employee*)employee {
    
        [[self class] logAction:@"Logout"
                        details:[self serializeEmployee:employee]
                       objectID:employee.id
                       parentID:nil
                    documetType:nil
                     employeeId:employee.id];
}


+(void)logCreateDrawerMemoWithID:(BPUUID *)drawerMemoId {
    
            [[self class] logAction:@"DrawerMemoCreated"
                        details:[self serializeValue:drawerMemoId.description withTagName:@"DrawerMemoId" withRootElem:NO]
                       objectID:drawerMemoId
                       parentID:nil
                    documetType:nil
                     employeeId:[SecurityManager currentEmployee].id];
}

+(void)updateReceipt:(BPUUID *)receiptId fields:(NSDictionary *)fieldsList database:(EGODatabase *)db {
    
    xmlDocPtr doc = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr root = xmlNewDocNode(doc, NULL, [@"Fields" xmlString], NULL);
    xmlDocSetRootElement(doc, root);
    
    for (NSString *fieldName in fieldsList.allKeys) {
        
        xmlNodePtr fieldNode = xmlNewDocRawNode(doc, NULL, [@"Field" xmlString], nil);
        
        NSString *fieldValue = [(NSObject *)fieldsList[fieldName] description];
        xmlNodePtr fieldNameNode = xmlNewDocRawNode(doc, NULL, [@"Name" xmlString] , [fieldName xmlString]);
        xmlAddChild(fieldNode, fieldNameNode);
        xmlNodePtr fieldValueNode = xmlNewDocRawNode(doc, NULL, [@"Value" xmlString], [fieldValue xmlString]);
        xmlAddChild(fieldNode, fieldValueNode);
        
        xmlAddChild(root, fieldNode);
    }
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    xmlFreeDoc(doc);
    if (buf){
        NSString *str = @((const char*)buf);
        xmlFree(buf);

        [[self class] logAction:@"UpdateReceipt"
                        details:str
                       objectID:receiptId
                       parentID:nil
                    documetType:nil
                     employeeId:[SecurityManager currentEmployee].id
                         reason:@"Plugin"
                       database:db];
    }
}

@end
