//
//  AttributeSetValue.h
//  Buyer
//
//  Created by Lulakov Viacheslav on 11/2/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

extern NSString * const attrSetValSelectStm;

@interface AttributeSetValue : CROEntity  {

    BPUUID      *_attributeSetId;
    NSString    *_setCode;
    NSString    *_value;
    NSString    *_alias;
    NSString    *_alias2;
    NSInteger    _order;
}

@property (nonatomic, retain) BPUUID      *attributeSetId;
@property (nonatomic, retain) NSString    *setCode;
@property (nonatomic, retain) NSString    *value;
@property (nonatomic, retain) NSString    *alias;
@property (nonatomic, retain) NSString    *alias2;

@property (nonatomic, assign) NSInteger    order;


+(NSMutableArray *)loadAttributeSetValuesForSetCode:(NSString *)code;
+(NSMutableArray *)loadAttributeSetValuesForSetCode:(NSString *)code attrName:(NSString *)attrName andStyle:(BPUUID *)styleId;
+(BOOL)hasAttributeSetValuesWithAlias:(NSString *)code;


@end
