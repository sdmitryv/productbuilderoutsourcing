//
//  BPUUID.h
//  Skates
//
//  Created by Valery Fomenko on 2/3/10.
//  Copyright 2010 Cloudworks Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BPUUID : NSObject<NSCopying, NSCoding> {
	NSUUID* _uuid;
}

+ (BPUUID *)UUID;
+ (BPUUID *)UUIDWithString:(NSString *)uuidString;
+ (BPUUID *)UUIDWithBytes:(NSData *)uuidBytes;
+ (BPUUID *)UUIDWithUUIDBytes:(uuid_t)bytes;

- (id)initWithNSUUID:(NSUUID*)uuid;
- (id)initWithString:(NSString *)uuidString;
- (id)initWithBytes:(NSData *)uuidBytes;
- (id)initWithUUIDBytes:(uuid_t)bytes;

- (NSString *)stringRepresentation;
- (NSData *)byteRepresentation;
- (void)getUUIDBytes:(uuid_t)uuid;
+ (BPUUID*)empty;
- (BOOL)isEmpty;
- (char*)UTF8String;
@end