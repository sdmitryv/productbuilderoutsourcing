//
//  Location.m
//  TeamworkPOS
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Location.h"
#import "Workstation.h"
#import "DecimalHelper.h"
#import "AppSettingManager.h"

#define EARTH_RADIUS 6372.795

@implementation LocationBase

@synthesize name = _name; 
@synthesize locationCode = _locationCode;
-(void) dealloc
{
	[_name release];
	[_locationCode release];
	[super dealloc];
}
@end

@implementation Location

-(void) dealloc
{
	[_name2 release];
	[_taxZoneID release];
	[_locationGroupID release];

	[_locationBaseCurrencyID release]; // Roma
    
	[_transferGroup release];
	[_area release];
    [_phone1 release];
    [_defaultPriceLevelCode release];
    [_distance release];
    [_onHandQty release];
    [_incomingQty release];
    [_committedQty release];
    [_availableQty release];
    [_locationAvailabilityGroupID release];
    [_timeZoneID release];
    [_locationExternalID release];
    
    self.faxNumber = nil;
    self.email = nil;
    self.homePage = nil;
    
    self.address1 = nil;
    self.address2 = nil;
    self.city = nil;
    self.state = nil;
    self.postalCode = nil;
    self.country = nil;
    [_address3 release];
    [_address4 release];
    [_locationPriceGroupID release];
    [_countryObject release];
	[super dealloc];
}

Location* _localLocation;

+(Location*)localLocation{
    
    Location* aLocalLocation = nil;
    @synchronized(self){
        if (!_localLocation){
            _localLocation = [[Location getInstanceById:[AppSettingManager instance].locationId] retain];
        }
        aLocalLocation = [[_localLocation retain]autorelease];
    }
    return aLocalLocation;
}

+(void)resetCache{
    
    @synchronized(self){
        
        [_localLocation release];
        _localLocation = nil;
        [Workstation resetCache];
    }

}

- (NSString *)description {
    NSString *lc = _locationCode ? [_locationCode stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]] : @"";
    NSString *ln = _name ? [_name stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]] : @"";
    if (lc.length == 0 && ln.length == 0)
        return @"";
    if (lc.length == 0)
        return ln;
    if (ln.length == 0)
        return lc;
    
    return [NSString stringWithFormat:@"%@ - %@", lc, ln];
}

- (NSString *)description2 {
    return _name.length ? _name :_locationCode;
}

-(Country*)countryObject{
    if (!_countryObject){
        @synchronized (self) {
            if (!_countryObject){
                _countryObject = [[Country getCountryByCode:self.country] retain];
            }
        }
        
    }
    return _countryObject;
}

+(NSMutableArray*)getLocations{ return nil; }

+(NSMutableArray*)getLocationsFromLocalAvailabilityGroup { return nil; }

+(id)getLocationByCode:(NSString*)code { return nil; }
+(id)getLocationByEID:(NSString*)eid { return nil; }

+(NSDecimalNumber *)distanceFrom:(Location *)location1 to:(Location *)location2  inMiles:(BOOL)inMiles {
    
    if (CPDecimalEquals0(location1.latitude) || CPDecimalEquals0(location1.longitude) || CPDecimalEquals0(location2.latitude) || CPDecimalEquals0(location2.longitude))
        return nil;
    
    double lat1 = CPDecimalDoubleValue((location1.latitude)) * M_PI / 180;
    double lat2 = CPDecimalDoubleValue((location2.latitude)) * M_PI / 180;
    double long1 = CPDecimalDoubleValue((location1.longitude)) * M_PI / 180;
    double long2 = CPDecimalDoubleValue((location2.longitude)) * M_PI / 180;
    
    double cl1 = cos(lat1);
    double cl2 = cos(lat2);
    double sl1 = sin(lat1);
    double sl2 = sin(lat2);
    double delta = long2 - long1;
    double cdelta = cos(delta);
    double sdelta = sin(delta);
    
    double y = sqrt(pow(cl2 * sdelta, 2) + pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
    double x = sl1 * sl2 + cl1 * cl2 * cdelta;
    
    double ad = atan2(y, x);
    double dist = ad * EARTH_RADIUS;
    
    if (inMiles)
        dist /= 1.609344;
    
    return [NSDecimalNumber decimalNumberWithDecimal:CPDecimalFromDouble(dist)];
}
+(NSString*)getCompanyName{
    return @"";
}

@end
