//
//  ReturnReason.h
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/18/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEntity.h"

@interface ReturnReason : CROEntity{
    NSString* _description;
}

@property (nonatomic, retain) NSString	* description;
@property (nonatomic, retain) BPUUID	* defaultLogicalBinId;
@property (nonatomic, assign) NSInteger   listOrder;
@property (nonatomic, assign) BOOL		  deleted;

+(NSMutableArray*)getReturnReasons;
@end
