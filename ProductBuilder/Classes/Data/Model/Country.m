//
//  Country.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/13/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "Country.h"

@implementation Country

@synthesize code = _code; 
@synthesize shortName = _shortName;

-(void) dealloc
{
	[_code release];
	[_shortName release];
	
	[super dealloc];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@  %@", _code, _shortName];
}

+(NSMutableArray*)getCountries{
    return nil;
}

+(Country*)getCountryByCode:(NSString*)code{
    return nil;
}
@end
