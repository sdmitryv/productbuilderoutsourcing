//
//  CalendarStatus.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CEntity.h"

typedef NS_ENUM(NSUInteger, CalendarStatusType) {
    CalendarStatusUnscheduled = 0,
    CalendarStatusScheduled  = 1,
    CalendarStatusCompletePaymentDue = 2,
    CalendarStatusCompleteInvoiced = 3,
    CalendarStatusCancelled  = 4,
    CalendarStatusCustomType = 5
};

@interface CalendarStatus : CROEntity {
    CalendarStatusType    _status;
    NSString            * _label;
}

@property(nonatomic, assign) CalendarStatusType   status;
@property(nonatomic, retain) NSString           * label;

+(id)getInstanceByType:(CalendarStatusType)type;

@end
