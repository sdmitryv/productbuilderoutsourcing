//
//  CurrencyDenomination.m
//  DrawerMemo
//
//  Created by Alexander Martyshko on 11/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CurrencyDenomination.h"

@implementation CurrencyDenomination

@synthesize description;

- (void)dealloc {
    
    [_currencyID release];
    [description release];
    [_code release];

    [super dealloc];
}

- (NSString *)description {
    return description ? description : _code;
}

+(NSArray*)getDenominationByCurrency:(BPUUID *)currencyId quickTenderOnly:(BOOL)quickTenderOnly {
    return nil;
}

@end
