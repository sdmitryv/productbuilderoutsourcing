//
//  EmailValidation.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 4/2/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "EmailValidation.h"

@implementation EmailValidation

- (void)dealloc {
    [_reason release];
    [_email release];
    [_suggestion release];
    [_reasonCode release];
    [_localizedDescription release];
    
    [super dealloc];
}

@end
