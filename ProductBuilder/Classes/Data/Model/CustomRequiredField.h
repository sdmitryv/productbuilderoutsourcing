//
//  CustomRequiredField.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 11/14/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

typedef NS_ENUM(NSUInteger, ConvertEntry) { ConvertEntryDefault = 0, ConvertEntrySentenceCase = 1, ConvertEntryAllCapitols = 2 };

typedef NS_ENUM(NSUInteger, CustomRequiredFieldArea) { CustomRequiredFieldAreaCustomer = 1, CustomRequiredFieldAreaContact = 13, CustomRequiredFieldAreaSalesReturn = 2041, CustomRequiredFieldAreaShipToAddress = 2043 };

@interface CustomRequiredField : CROEntity

@property(nonatomic,retain) NSString *requireSymbol;
@property(nonatomic,retain) NSString *fieldName;
@property(nonatomic,retain) NSString *fieldLabel;
@property(nonatomic,assign) ConvertEntry convertEntry;
@property(nonatomic,assign) int code;

+ (NSArray *)getRequiredFieldsListForArea:(CustomRequiredFieldArea)area;
+ (NSArray *)getCaseSensitiveFieldsListForArea:(CustomRequiredFieldArea)area;
+ (CustomRequiredField *)getRequiredFieldByName:(NSString*)fieldName forArea:(CustomRequiredFieldArea)area;
+ (NSArray *)getSearchFieldsListForArea:(CustomRequiredFieldArea)area;

@end
