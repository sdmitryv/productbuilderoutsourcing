//
//  LocationAvailabilityGroup.h
//  TeamworkPOS
//
//  Created by Lulakov Viacheslav on 1/16/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CROEntity.h"

@interface LocationAvailabilityGroup : CROEntity {
    NSString    * _description;
    NSString    * _code;
    NSInteger     _displayDistance;
}

@property(nonatomic, retain) NSString    * description;
@property(nonatomic, retain) NSString    * code;
@property(nonatomic, assign) NSInteger     displayDistance;

@end
