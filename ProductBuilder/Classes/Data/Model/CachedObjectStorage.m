//
//  CachedObjectStorage.m
//  ProductBuilder
//
//  Created by valery on 7/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CachedObjectStorage.h"
#import "Employee.h"
#import "Location.h"

@interface CachedObjectStorage(){
    NSMutableDictionary* _cachedObjects;
    dispatch_queue_t _cachedObjectsQueue;
}

@end

@implementation CachedObjectStorage

-(instancetype)init{
    if ((self=[super init])){
        _cachedObjectsQueue = dispatch_queue_create("com.cloudworks.cachedObjectsQueue", nil);
    }
    return self;
}

+(instancetype)instance{
    static id instance;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        instance = [[CachedObjectStorage alloc]init];
    });
    return instance;
}


-(void)dealloc{
    [_cachedObjects release];
    dispatch_release(_cachedObjectsQueue);
    [super dealloc];
}


-(id)objectByName:(NSString*)name class:(Class)aClass{
    if (!name) return nil;
    id object = [self objectByKey:name];
    if (!object){
        if ([aClass isEqual:Employee.class]){
            object = [Employee getEmployeesByName:name andPassword:nil isAll:YES];
            if (!object){
                object = @"";
            }
            [self setObject:object forKey:name];
        }
        else if ([aClass isEqual:Location.class]){
            object = [Location getLocationByCode:name];
            if (!object){
                object = @"";
            }
            [self setObject:object forKey:name];
        }
    }
    return [object isKindOfClass:NSString.class] && [object isEqualToString:@""] ? nil : object;
}

-(id)objectByKey:(NSString*)key{
    __block id object = nil;
    dispatch_sync(_cachedObjectsQueue, ^{
        object = [_cachedObjects[key] retain];
    });
    return [object autorelease];
}

-(void)setObject:(id)object forKey:(NSString*)key{
    if (!key) return;
    dispatch_sync(_cachedObjectsQueue, ^{
        if (!_cachedObjects){
            _cachedObjects = [[NSMutableDictionary alloc]init];
        }
        _cachedObjects[key] = object;
    });
    ;
}

@end
