//
//  Validatable.h
//  iPadPOS
//
//  Created by valera on 5/16/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "Errors.h"

@protocol Validatable

- (BOOL) is_valid;
- (Errors *) errors;

@end
