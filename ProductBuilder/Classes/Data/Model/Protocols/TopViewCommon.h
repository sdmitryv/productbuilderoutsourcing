//
//  TopViewCommon.h
//  ProductBuilder
//
//  Created by Julia Korevo on 9/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TopViewCommon 
-(NSString*)name;

@end
