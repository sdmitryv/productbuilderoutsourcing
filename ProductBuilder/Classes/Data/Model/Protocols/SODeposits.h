//
//  SODeposits.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 6/26/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DepositOptionType) {
    DepositOptionTypeNone = 0 , // default value, sales order not have requirement for deposit
    DepositOptionTypePercent = 1, // percent of total price items in the sales order
};

typedef NS_ENUM(NSUInteger, SODepositRequiredOption) {
    SODepositRequiredOptionNone = 0 , // default value, Item or Fee not have requirement for deposit, but order-base setting can be used
    SODepositRequiredOptionNotRequired = 1, // Item or Fee not need deposit, order-base setting ignored also
    SODepositRequiredOptionPercent = 2, // percent of total price Item or Fee
};
