//
//  URLSettingHelper.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 7/28/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "URLSettingHelper.h"
#import "Location.h"
#import "SecurityManager.h"
#import <libxml/xmlstring.h>
#import "USAdditions.h"
#import "DeviceAgentManager.h"
#import "Settings.h"

@implementation URLSettingHelper

+(NSString *)processUrlString:(NSString *)urlString {
    if ([urlString hasPrefix:@"RunHqReport"]) {
        return [[self class] composeUrlStringForReportRun:urlString];
    }
    
    return urlString;
}

+(NSString *)composeUrlStringForReportRun:(NSString *)str {
    NSInteger startLocation = [str rangeOfString:@"{"].location + 1;
    NSInteger endLocation = [str rangeOfString:@"}"].location;
    if (startLocation != NSNotFound && endLocation != NSNotFound) {
        
        NSString *reportIdKey = @"reportid";
        NSString *objectIdKey = @"objectid";
        
        NSString *paramsStr = [str substringWithRange:NSMakeRange(startLocation, endLocation - startLocation)];
        NSArray *paramsArray = [paramsStr componentsSeparatedByString:@","];
        NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] initWithCapacity:paramsArray.count];
        for (NSString *param in paramsArray) {
            NSArray *arr = [param componentsSeparatedByString:@":"];
            if (arr.count == 2) {
                NSString *val = arr[1];
                if ([[val lowercaseString] isEqualToString:@"@locationid"]) {
                    val = [[Location localLocation].id description];
                }
                else if ([[val lowercaseString] isEqualToString:@"@employeeid"]) {
                    val = [[SecurityManager currentEmployee].id description];
                }
                paramsDict[[arr[0] lowercaseString]] = val;
            }
        }
        
        NSString *reportVersionParamPart = [NSString stringWithFormat:@"ReportVersion=%@", paramsDict[reportIdKey]];
        
        NSArray * languages = [NSBundle mainBundle].preferredLocalizations;
        NSString * language = languages.count > 0 ? languages[0] : @"en";
        NSDateFormatter *localDateFormatter = [[NSDateFormatter alloc] init];
        [localDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [localDateFormatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
        NSDictionary *contextDict = @{
                                      @"AppName" : @"Teamwork POS",
                                      @"AppVersion" : [DeviceAgentManager sharedInstance].appVersion,
                                      @"LocationCode" : [Location localLocation].locationCode.length ? [Location localLocation].locationCode : @"",
                                      @"LanguageCode" : language,
                                      @"LoginName" : [SecurityManager currentEmployee].loginName.length ? [SecurityManager currentEmployee].loginName : @"",
                                      @"ReportRunDate" : [localDateFormatter stringFromDate:[NSDate date]]
                                      };
        [localDateFormatter release];
        NSString *objectIdStr = paramsDict[objectIdKey];
        [paramsDict release];
        NSString *objectIdPart = objectIdStr.length ? [NSString stringWithFormat:@"ObjectID=%@", objectIdStr] : @"";
        NSString *xmlStr = [[[self class] serializeDictionary:contextDict] stringByReplacingOccurrencesOfString:@"=" withString:@"{&pvd&}"];
        NSString *xmlPart = [NSString stringWithFormat:@"Xml=%@", xmlStr];
        
        NSString *reportParamsValue = objectIdPart.length ? [NSString stringWithFormat:@"%@;%@", objectIdPart, xmlPart] : xmlPart;
        NSString *escapedReportParamsValue = [reportParamsValue stringByAddingPercentEncodingWithAllowedCharacters:
                                              [[NSCharacterSet characterSetWithCharactersInString:@" !*'();:@&=+$,/?%#[]{}<>\"\n"] invertedSet]];
        NSString *reportParamsPart = [NSString stringWithFormat:@"ReportParams=%@", escapedReportParamsValue];
        
        return [NSString stringWithFormat:@"%@/CloudHqPrintReport.aspx?%@&%@", [[Settings sharedInstance] hqAddress], reportVersionParamPart, reportParamsPart];
    }
    
    return @"";
}

+(NSString*)serializeDictionary:(NSDictionary*)dict{
    
    xmlDocPtr doc = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    

    xmlDocSetRootElement(doc,[dict xmlNodeForDoc:doc elementName:@"Root" elementNSPrefix:nil]);
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    
    NSString *str = @((const char*)buf);
    xmlFree(buf);
    xmlFreeDoc(doc);
    return str;
}

@end
