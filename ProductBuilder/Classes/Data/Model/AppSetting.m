//
//  AppSetting.m
//  StockCount
//
//  Created by Lulakov Viacheslav on 10/19/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "AppSetting.h"

@implementation AppSetting

@synthesize type = _type;
@synthesize key = _key;
@synthesize value = _value;

-(void) dealloc
{
	[_type release];
	[_key release];
	[_value release];
	
	[super dealloc];
}

+ (NSMutableArray *)getSettings{
	return nil;
}

+ (AppSetting *)getSettingForKey:(NSString *)key{
	return nil;
}

+ (AppSetting *)getSettingForKey:(NSString *)key usingDb:(EGODatabase*)db{
    return nil;
}

+ (NSString*)stringForKey:(NSString *)key{
	return [[self class]getSettingForKey:key].value;
}

+ (BOOL)boolForKey:(NSString *)key{
	return [[[self class]stringForKey:key] boolValue];
}

+ (NSInteger)intForKey:(NSString *)key{
	return [[[self class]stringForKey:key] intValue];
}

+ (NSString*)stringForKey:(NSString *)key defaultValue:(NSString*)defaultValue{
	AppSetting* setting = [[self class]getSettingForKey:key];
	if (!setting) return defaultValue;
	return setting.value;
}

+ (BOOL)boolForKey:(NSString *)key defaultValue:(BOOL)defaultValue{
	AppSetting* setting = [[self class]getSettingForKey:key];
	if (!setting) return defaultValue;
	return [setting.value boolValue];
}

+ (NSInteger)intForKey:(NSString *)key defaultValue:(NSInteger)defaultValue{
	AppSetting* setting = [[self class]getSettingForKey:key];
	if (!setting) return defaultValue;
	return [setting.value intValue];
}


+ (void)setString:(NSString *)value forKey:(NSString *)key{
}

+ (void)setBool:(BOOL)value forKey:(NSString *)key{
	[[self class] setString:[@(value)stringValue] forKey:key];
}

+ (void)setInt:(NSInteger)value forKey:(NSString *)key{
	[[self class] setString:[@(value)stringValue] forKey:key];
}


@end

