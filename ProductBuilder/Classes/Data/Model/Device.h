//
//  Device.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/7/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "CROEntity.h"

typedef NS_ENUM(NSUInteger, DeviceType) {
    DeviceTypeIPad = 0,
    DeviceTypeIPodIPhone = 1
};

@class Location;

@interface Device : CROEntity {
    Location *_location;
}

@property (nonatomic, retain) BPUUID *deviceAgentId;
@property (nonatomic, retain) BPUUID *locationId;
@property (nonatomic, retain) NSString *deviceName;
@property (nonatomic, retain) NSString *deviceAlias;
@property (nonatomic, assign) DeviceType deviceType;
@property (nonatomic, retain) NSString *printServerIP;
@property (nonatomic, assign) NSInteger printerPort;
@property (nonatomic, assign) NSInteger scalesPort;
@property (nonatomic, assign) NSInteger deviceNo;
@property (nonatomic, retain) BPUUID *shopperDisplayId;
@property (nonatomic, assign) BOOL isDeactivated;
@property (nonatomic, retain) NSString *notes;
@property (nonatomic, retain) NSString *techInfo1;
@property (nonatomic, retain) NSString *techInfo2;
@property (nonatomic, retain) NSString *techInfo3;
@property (nonatomic, retain) NSString *techInfo4;
@property (nonatomic, retain) NSString *techInfo5;
@property (nonatomic, retain) NSString *techInfo6;
@property (nonatomic, retain) NSString *techInfo7;
@property (nonatomic, retain) NSString *techInfo8;
@property (nonatomic, retain) NSString *techInfo9;
@property (nonatomic, retain) NSString *techInfo10;
@property (nonatomic, retain) NSString *IPAddress;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, retain) NSString *systemName;
@property (nonatomic, retain) NSString *systemVersion;
@property (nonatomic, retain) NSString *model;

@property (nonatomic, retain) BPUUID *mobileApplicationId;
@property (nonatomic, retain) NSString *applicationVersion;
@property (nonatomic, retain) NSDate *firstLaunchDate;
@property (nonatomic, retain) NSString *SVSAuthToken;
@property (nonatomic, retain) NSString *SVSDeviceId;

@property (nonatomic, assign) NSInteger locationDeviceCode;

@property (nonatomic, readonly) Location *location;
@property (nonatomic, readonly) NSString *deviceTypeStringRepresentation;

@property (nonatomic, retain) NSString *lastTransactionNo;

@property (nonatomic, assign) BOOL pushLogsToGae;
@property (nonatomic, assign) NSInteger gaeLogSeverity;

@property (nonatomic, assign) BOOL collectNetworkStats;

@property (nonatomic, assign) BOOL isMultiWorkstation;

@property (nonatomic, retain) NSArray *settings;
@property (nonatomic, readonly) NSString *devicePlatformeStr;


// returns a dictionary with system properties of current iDevice
- (NSDictionary *)dictionaryRepresentation;

@end
