//
//  EmailValidation.h
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 4/2/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmailValidation : NSObject

@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, retain) NSString * reason;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * suggestion;
@property (nonatomic, retain) NSString * reasonCode;
@property (nonatomic, retain) NSString * localizedDescription;

@end
