//
//  SyncAction.h
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/4/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "CEntity.h"

@interface SyncAction : CEntity {

	NSString *_action;
	NSString *_target;
}

@property(nonatomic,retain) NSString *action;
@property(nonatomic,retain) NSString *target;

+(SyncAction *)getSyncAction:(NSString *)act target:(NSString *)targ;

@end
