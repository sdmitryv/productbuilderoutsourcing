//
//  SqlItemSearchCondition.m
//  ProductBuilder
//
//  Created by Roman on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SqlItemSearchCondition.h"
#import "SqlCommand.h"
#import "SettingManager.h"

@interface SqlFieldCondition (Private)
+(NSString*)operatorToString:(CompareOperator) compareOperator;
@end

@implementation SqlItemSearchCondition


+(SqlFilterCondition*)getItemSearchCondition:(NSString*)searchText includePLU:(BOOL)includePLU includeCLU:(BOOL)includeCLU includeUPC:(BOOL)includeUPC includeSN:(BOOL)includeSN includeStyleNo:(BOOL)includeStyleNo searchType:(ItemSearchType)searchType {
    
    NSString *likeFormat = @"%%%@%%";
    
    NSScanner *sc = [NSScanner scannerWithString: searchText];
    BOOL isInteger = FALSE;
    if ([sc scanInt:Nil])
        isInteger = [sc isAtEnd];
    
    switch (searchType) {
        case ItemSearchByCLU:
            return [SqlCLUSearchCondition condition:@"CLU" compareOperator:Equal value:searchText];
        case ItemSearchByStyleNum:
            return [SqlStyleNoSearchCondition condition:@"StyleNo"
                                        compareOperator:Like
                                                  value:[NSString stringWithFormat:likeFormat, searchText]];
        case ItemSearchByUPC:
            return [SqlUPCSearchCondition condition:@"UPC" compareOperator:Equal value:searchText];
        case ItemSearchBySN:
            return [SqlSNSearchCondition condition:@"SN" compareOperator:Equal value:searchText];
        case ItemSearchByPLU:
            return [SqlFieldCondition condition:@"PLU" compareOperator:Equal value:isInteger ? @([searchText intValue]) : @(-1)];
        default:
            break;
    }
    
    SqlInvenItemCondition* ftsCondition = [SqlInvenItemCondition condition:@"InvenStyleFTS" compareOperator:Like value:searchText];
    ftsCondition.searchType = searchType;
    
    if (searchType == ItemSearchByDescription || searchType == ItemSearchByAttribute || (!includeCLU && !includeUPC && !includePLU && !includeStyleNo && !includeSN)){
        return ftsCondition;
    }
    
    SqlOrCondition* orCondition = [[[SqlOrCondition alloc] init] autorelease];
    if (includeCLU && includeUPC){
        [orCondition add:[SqlCLU_UPCSearchCondition condition:@"CLU" compareOperator:Equal value:searchText]];
    }
    else {
        if (includeCLU)
            [orCondition add:[SqlCLUSearchCondition condition:@"CLU" compareOperator:Equal value:searchText]];
        if (includeUPC)
            [orCondition add:[SqlUPCSearchCondition condition:@"UPC" compareOperator:Equal value:searchText]];
    }
    if (includeSN){
        [orCondition add:[SqlSNSearchCondition condition:@"SN" compareOperator:Equal value:searchText]];
    }
    if (includeStyleNo)
        [orCondition add:[SqlStyleNoSearchCondition condition:@"StyleNo" compareOperator:Equal value:searchText]];
    if (includePLU && isInteger)
        [orCondition add:[SqlFieldCondition condition:@"PLU" compareOperator:Equal value:@([searchText intValue])]];
    
    [orCondition add:ftsCondition];
    return orCondition;
}


+(ItemSearchResultType)filterOnItemList:(ItemList*)itemList searchText:(NSString *)searchText searchType:(ItemSearchType)searchType{
    
    BOOL isInteger = FALSE;
    NSScanner *sc = [NSScanner scannerWithString: searchText];
    if ([sc scanInt:Nil])
        isInteger = [sc isAtEnd];
    
    BOOL includePLU = FALSE, includeCLU  = FALSE, includeUPC  = FALSE, includeSN = FALSE, includeStyleNo = NO;
    
    if (searchType == ItemSearchByAll) {
    
        
        itemList.filterCondition = [SqlCLUSearchCondition condition:@"CLU" compareOperator:Equal value:searchText];
        if (itemList.count == 1)
            return ItemSearchResultCLU;
        includeCLU = itemList.count > 0;
        
        itemList.filterCondition = [SqlUPCSearchCondition condition:@"UPC" compareOperator:Equal value:searchText];
        if (itemList.count == 1 && !includeCLU)
            return ItemSearchResultUPC;
        includeUPC = itemList.count > 0;
        
        itemList.filterCondition = [SqlSNSearchCondition condition:@"SN" compareOperator:Equal value:searchText];
        if (itemList.count == 1 && !includeCLU && !includeUPC)
            return ItemSearchResultSN;
        includeSN = itemList.count > 0;
        
        if (isInteger) {
            
            itemList.filterCondition = [SqlFieldCondition condition:@"PLU" compareOperator:Equal value:@([searchText intValue])];
            if (itemList.count == 1 && !includeCLU && !includeUPC && !includeSN)
                return ItemSearchResultPLU;
            
            includePLU = itemList.count > 0;
        }
        
        itemList.filterCondition = [SqlStyleNoSearchCondition condition:@"StyleNo" compareOperator:Equal value:searchText];
        if (itemList.count == 1 && !includeCLU && !includeUPC && !includeSN && !includePLU)
            return ItemSearchResultStyleNum;
        includeStyleNo = itemList.count > 0;
    
    }
    
    itemList.filterCondition = [SqlItemSearchCondition getItemSearchCondition:searchText includePLU:includePLU includeCLU:includeCLU includeUPC:includeUPC includeSN:includeSN includeStyleNo:includeStyleNo searchType:searchType];
    
    NSString *orderColumn = [SettingManager instance].useItemDescription == 3 ? @"s.description3" : @"s.description4";
    itemList.sortOrderList = @[[SqlSortOrder orderBy:orderColumn
                                                                       order:SqlSortOrderAscending]];

    if (searchType == ItemSearchByCLU) {
        return ItemSearchResultCLU;
    }
    else if (searchType == ItemSearchByStyleNum) {
        return ItemSearchResultStyleNum;
    }
    else if (searchType == ItemSearchByUPC) {
        return ItemSearchResultUPC;
    }
    else if (searchType == ItemSearchBySN) {
        return ItemSearchResultSN;
    }
    else if (searchType == ItemSearchByPLU) {
        return ItemSearchResultPLU;
    }
    
    return ItemSearchResultUnknown;
}


+(NSString *)typeName:(ItemSearchType)type {
    
    switch (type) {
        case ItemSearchByAll:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_GENERAL_TITLE", nil);
        case ItemSearchByDescription:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_DESCRIPTION_TITLE", nil);
        case ItemSearchByAttribute:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_ATTRIBUTE_TITLE", nil);
        case ItemSearchByCLU:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_CLU_TITLE", nil);
        case ItemSearchByStyleNum:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_STYLE_TITLE", nil);
        case ItemSearchByUPC:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_UPC_TITLE", nil);
        case ItemSearchBySN:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_SN_TITLE", nil);
        case ItemSearchByPLU:
            return NSLocalizedString(@"ITEM_SEARCH_FIELD_PLU_TITLE", nil);
    }
}

+(BOOL)isField:(NSString *)field ofSearchType:(ItemSearchType)type {
    
    field = [field lowercaseString];
    
    switch (type) {
        case ItemSearchByAll:
            return YES;
        case ItemSearchByDescription:
            return [field isEqualToString:@"description"] | [field isEqualToString:@"description2"] | [field isEqualToString:@"description3"] | [field isEqualToString:@"description4"];
        case ItemSearchByAttribute:
            return [field isEqualToString:@"attr1"] | [field isEqualToString:@"attr2"] | [field isEqualToString:@"attr3"];
        case ItemSearchByCLU:
            return [field isEqualToString:@"clu"];
        case ItemSearchByStyleNum:
            return NO;
        case ItemSearchByUPC:
            return [field isEqualToString:@"upc"];
        case ItemSearchBySN:
            return [field isEqualToString:@"sn"];
        case ItemSearchByPLU:
            return [field isEqualToString:@"plu"];
    }
    
    return NO;
}


@end


@implementation SqlUPCSearchCondition

-(NSString *)description {
    
    NSString* parameterName = [self parameterName];
    return [NSString stringWithFormat:@"ii.ItemID in (select ItemID FROM [InvenItemIdentifier] WHERE [IDClass] = 0 AND [Value] = %@ union all SELECT ItemID FROM [InvenStyleVendorItem] WHERE [VendorUPC] = %@)", parameterName, parameterName];
}


+(BOOL)isThisItem:(BPUUID*)itemId upc:(NSString *)upc{
    
    EGODatabase *db = [DataManager instance].currentDatabase;
    EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT COUNT(1) FROM (SELECT [Value] UPC FROM [InvenItemIdentifier] WHERE [IDClass] = 0 AND [Value] = ? AND ItemID = ? UNION SELECT [VendorUPC] UPC FROM [InvenStyleVendorItem] WHERE [VendorUPC] = ? AND ItemID = ?)", itemId.description, upc, itemId.description, nil];
    
    for(EGODatabaseRow* row in result) {
     
        return [row intForColumnIndex:0] > 0;
    }
    
    return NO;
}

@end


@implementation SqlCLUSearchCondition

-(NSString *)description {

    NSString* parameterName = [self parameterName];
    return [NSString stringWithFormat:@"ii.ItemID in (select ItemID FROM [InvenItemIdentifier] WHERE [IDClass] = 1 AND [Value] = %@)", parameterName];
}

@end


@implementation SqlSNSearchCondition


-(NSString *)description {
    
    NSString* parameterName = [self parameterName];
    return [NSString stringWithFormat:@"ii.ItemID in (SELECT ItemID from InvenItemSerialNumber WHERE [Value] = %@ AND [IsDeleted] = 0)", parameterName];
}

+(BOOL)isThisItem:(BPUUID*)itemId sn:(NSString *)sn{
    
    EGODatabase *db = [DataManager instance].currentDatabase;
    EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT COUNT(1) FROM InvenItemSerialNumber WHERE [Value] = ? AND [IsDeleted] = 0 AND ItemID = ?", sn, itemId.description, nil];
    
    for(EGODatabaseRow* row in result) {
        
        return [row intForColumnIndex:0] > 0;
    }
    
    return NO;
}

@end


@implementation SqlCLU_UPCSearchCondition

-(NSString *)description {

    NSString* parameterName = [self parameterName];
    return [NSString stringWithFormat:@"ii.ItemID in (SELECT ItemID FROM [InvenItemIdentifier] WHERE [IDClass] in (0,1) AND [Value] = %@)", parameterName];
}

@end



@implementation SqlStyleNoSearchCondition

-(NSString *)description {
    
    return [NSString stringWithFormat:@"ii.StyleID IN (SELECT StyleID FROM InvenStyle WHERE StyleNo %@ %@)", [SqlFieldCondition operatorToString:_operator], [self parameterName]];
}

@end



@implementation SqlInvenItemCondition

@synthesize searchType;

//-(NSString *)invertStr:(NSString *)paramValue {
//    
//    NSUInteger length = paramValue.length;
//    unichar *cc = calloc(length+1, sizeof(unichar));
//    [paramValue getCharacters:cc];
//    for (NSUInteger i = 0 ; i < length/2 ; i++) {
//        
//        unichar tmp = cc[i];
//        cc[i] = cc[length - i - 1];
//        cc[length - i - 1] = tmp;
//    }
//    NSString *invertedParamValue = [NSString stringWithCharacters:cc length:length];
//    free(cc);
//    return invertedParamValue;
//}

-(NSString *)description {
    
    //remove whitespaces
    NSArray* words = [[self.parameterValue stringByReplacingOccurrencesOfString:@"'" withString:@"''"]
                        componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //remove empty strings
    words = [words filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
    //remove duplicates
    words = [words valueForKeyPath:@"@distinctUnionOfObjects.self"];
    NSMutableArray* wordsTransformed = [[[NSMutableArray alloc]init]autorelease];
    for (NSString* word in words){
        [wordsTransformed addObject:[NSString stringWithFormat:@"\"%@*\"", word]];
    }
    words = wordsTransformed;
   
    NSString *whereFormat = @"%@ MATCH '%@'";
    NSMutableString *unionStatemet = [[[NSMutableString alloc] init] autorelease];
    
    if (searchType == ItemSearchByAll) {

        NSString* paramValue = [words componentsJoinedByString:@" "];
        NSString *param = [paramValue stringByReplacingOccurrencesOfString:@" " withString:@"* "];
        
        NSString *  attrIds = [NSString stringWithFormat:@"(SELECT AttributeSetValueID FROM AttributeSetValue WHERE rowid in (SELECT docid FROM AttributeSetValueFTS WHERE %@))", [NSString stringWithFormat:whereFormat, @"AttributeSetValueFTS", param]];
        [unionStatemet appendFormat:@"SELECT rowid FROM InvenItem WHERE StyleId IN (SELECT StyleId FROM InvenStyle WHERE rowid in (SELECT docid FROM InvenStyleFTS WHERE %@)) OR Attribute1Id IN %@ OR Attribute2Id IN %@ OR Attribute3Id IN %@", [NSString stringWithFormat:whereFormat, @"InvenStyleFTS", param], attrIds, attrIds, attrIds];
    }
    else {
        
        NSArray *tabls = searchType == ItemSearchByDescription ? @[@"InvenStyleFTS"] : @[@"AttributeSetValueFTS"];
        NSArray *params = searchType == ItemSearchByDescription
                            ? @[@"description", @"description2", @"description3", @"description4"]
                            : @[@"value"];
        
        NSString *unionFormat   = @"SELECT docid FROM %@ WHERE %@";
        NSString *unionFormat2  = @" UNION ALL SELECT docid FROM %@ WHERE %@";
        
        for (NSString *tabl in tabls) {
            
            NSMutableString *tablUnionStatemet = [[[NSMutableString alloc] init] autorelease];
            
            for (NSString *word in words) {
                
                NSString *paramValue = word;
                //NSString *paramValueInvert = [NSString stringWithFormat:@"%@*", [self invertStr:word]];
                
                NSMutableString *paramUnionStatemet = [[[NSMutableString alloc] init] autorelease];
                
                for (NSString *paramName in params) {
                
                    if (paramUnionStatemet.length == 0) {
                        
                        [paramUnionStatemet appendFormat:unionFormat, tabl, [NSString stringWithFormat:whereFormat, paramName, paramValue]];
                        //[paramUnionStatemet appendFormat:unionFormat2, tabl, [NSString stringWithFormat:whereFormat, paramName, paramValueInvert]];
                    }
                    else {
                     
                        [paramUnionStatemet appendFormat:unionFormat2, tabl, [NSString stringWithFormat:whereFormat, paramName, paramValue]];
                        //[paramUnionStatemet appendFormat:unionFormat2, tabl, [NSString stringWithFormat:whereFormat, paramName, paramValueInvert]];
                    }
                }

                if (tablUnionStatemet.length == 0)
                    [tablUnionStatemet appendFormat:@"SELECT docid FROM (%@)", paramUnionStatemet];
                else
                    [tablUnionStatemet appendFormat:@" INTERSECT SELECT docid FROM (%@)", paramUnionStatemet];
            }
            
            if (unionStatemet.length == 0)
                [unionStatemet appendFormat:@"SELECT docid FROM (%@)", tablUnionStatemet];
            else
                [unionStatemet appendFormat:@" UNION ALL SELECT docid FROM (%@)", tablUnionStatemet];
            
            if (searchType == ItemSearchByDescription) {
                [unionStatemet insertString:@"SELECT rowid FROM InvenItem WHERE StyleId in (SELECT StyleId FROM InvenStyle WHERE rowid in (" atIndex:0];
                [unionStatemet appendString:@"))"];
            }
            else {
                [unionStatemet insertString:@"(SELECT AttributeSetValueID FROM AttributeSetValue WHERE rowid in (" atIndex:0];
                [unionStatemet appendString:@"))"];
                unionStatemet = [NSMutableString stringWithFormat:@"SELECT rowid FROM InvenItem WHERE Attribute1Id IN %@ OR Attribute2Id IN %@ OR Attribute3Id IN %@", unionStatemet, unionStatemet, unionStatemet];
            }
        }

    }
    
    NSString *res = [NSString stringWithFormat:@"rowid IN (%@)", unionStatemet];
    return res;
}

-(NSString*) parameterName
{
    return nil;
}

@end




