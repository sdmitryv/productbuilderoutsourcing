//
//  SqlAndCondition.m
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlAndCondition.h"
#import "autorelease.h"

@implementation SqlAndCondition

-(instancetype)init{
	if ((self = [super init])) {
		_list = [[NSMutableArray alloc]init];
	}
	return self;
}

-(instancetype) initWithList:(SqlFilterCondition*) condition,...{
	if ((self = [self initWithConditions:VAToNSArray(condition)])) {
	}
	return self;
}

-(instancetype) initWithConditions:(NSArray<SqlFilterCondition*>*) conditions
{
	if ((self = [super init])) {
		_list = [NSMutableArray arrayWithArray:conditions];
		[_list retain];
	}
	return self;
}

+(instancetype) list:(SqlFilterCondition*) condition,...{

	return [[[self alloc] initWithConditions:VAToNSArray(condition)]autorelease];
}

+(instancetype) conditions:(NSArray<SqlFilterCondition*>*) conditions{
	return [[[self alloc] initWithConditions:conditions] autorelease];
}

-(void) add:(SqlFilterCondition*) condition
{
	[_list addObject:condition];
}

-(NSArray*) conditions
{
	return _list;
}

-(NSString*)linkPerdicate
{
	return @"and";
}

-(NSString*) description
{
	NSMutableString* descr = [[NSMutableString alloc]init];
	int i = 0;
	for (id condition in _list)
	{
		if(![condition isKindOfClass:[SqlFilterCondition class]])
			continue;
		if (i++!=0) {
			[descr appendString:@" "];
			[descr appendString:[self linkPerdicate]];
			[descr appendString:@" "];
		}
		[descr appendString:@"("];
		[descr appendString:[condition description]];
		[descr appendString:@")"];
	}
	return [descr autorelease];
}

-(void) dealloc{
	[_list release];
	
	[super dealloc];
}
@end
