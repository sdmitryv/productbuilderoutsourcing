//
//  SqlFilterCondition.h
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EGODatabase.h"

@class SqlCommand;

@interface SqlFilterCondition : NSObject {
	SqlCommand* _command;
}
@property (nonatomic,assign) SqlCommand* sqlCommand;
@end

@protocol ISqlFieldCondition
-(NSString*) parameterName;
-(id)parameterValue;
@end

@protocol ISqlFieldConditionRange
-(NSDictionary*)parameters;
@end

@protocol ISqlFilterConditionList
-(NSArray*) conditions;
@end


