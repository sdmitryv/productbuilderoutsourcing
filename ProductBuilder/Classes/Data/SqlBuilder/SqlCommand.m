//
//  SqlBuilder.m
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlCommand.h"

@interface SqlCommand(Private)
-(void)extractParams;
-(void)extractParams:(SqlFilterCondition*) condition;
-(void)resetStmtWhere;
-(void)resetStmtOrder;
-(void)resetStmtLimit;
-(void)resetParams;
@end

@implementation SqlCommand

-(void)resetParams
{
	_paramsExtracted = FALSE;
	[_parameters removeAllObjects];
}

-(void)resetStmtWhere
{
	_stmtWherePrepared = FALSE;
	[_preparedStmtWhere release];
	[_preparedStmt release];
	_preparedStmt = nil;
	_preparedStmtWhere = nil;
}

-(void)resetStmtOrder
{
	_stmtOrderPrepared = FALSE;
	[_preparedStmtOrder release];
	[_preparedStmt release];
	_preparedStmt = nil;
	_preparedStmtOrder = nil;
}

-(void)resetStmtLimit
{
	_stmtLimitPrepared = FALSE;
	[_preparedStmtLimit release];
	[_preparedStmt release];
	_preparedStmt = nil;
	_preparedStmtLimit = nil;
}

-(void)extractParams{
	if (!_paramsExtracted){
		[_parameters removeAllObjects];
		[self extractParams:_filterCondition];
		_paramsExtracted = TRUE;
	}
}

-(void)extractParams:(SqlFilterCondition*) condition{
	if (!condition) return;
	condition.sqlCommand = self;
	if ([condition conformsToProtocol:@protocol(ISqlFieldCondition)])	{
		id<ISqlFieldCondition> fieldCondition = (id<ISqlFieldCondition>)condition;
		[self registerParameterName:[fieldCondition parameterName] parameterValue:[fieldCondition parameterValue]];
	}
	else if ([condition conformsToProtocol:@protocol(ISqlFieldConditionRange)]){
		id<ISqlFieldConditionRange> fieldConditionRange = (id<ISqlFieldConditionRange>)condition;
		for (NSString* paramName in fieldConditionRange.parameters)
			[self registerParameterName:paramName parameterValue:(fieldConditionRange.parameters)[paramName]];
	}
	else if ([condition conformsToProtocol:@protocol(ISqlFilterConditionList)]){
		id<ISqlFilterConditionList> conditionList = (id<ISqlFilterConditionList>)condition;
		for (SqlFilterCondition* c in [conditionList conditions])
			[self extractParams:c];
    }
}


@synthesize filterCondition = _filterCondition;
@synthesize sortOrderList = _sortOrderList;
@synthesize limit;

-(id)initWithStmt:(NSString*) stmt{
	if ((self = [super init])) {
		return [self initWithStmt:stmt condition:nil sortOrderList:nil];
	}
	return self;
}

-(id)initWithStmt:(NSString*) stmt condition:(SqlFilterCondition*)condition{
	if ((self = [super init])) {
		return [self initWithStmt:stmt condition:condition sortOrderList:nil];
	}
	return self;
}

-(id)initWithStmt:(NSString*) stmt sortOrderList:(NSArray*) sortOrderList{
	
	if ((self = [super init])) {
		return [self initWithStmt:stmt condition:nil sortOrderList:sortOrderList];
	}
	return self;
}


-(id)initWithStmt:(NSString*) stmt condition:(SqlFilterCondition*)condition sortOrderList:(NSArray*) sortOrderList{
	if ((self = [super init])) {
		_filterCondition = [condition retain];
		_sortOrderList = [sortOrderList retain];
		_stmt = [stmt retain];
		_parameters = [[NSMutableDictionary alloc] init];
	}
	return self;
}

-(void)setFilterCondition:(SqlFilterCondition*) filterCondition{
	if (!_filterCondition || ![_filterCondition isEqual:filterCondition]){
		[_filterCondition release];
		_filterCondition = [filterCondition retain];
		[self resetParams];
		[self resetStmtWhere];
	}
}

-(void)setSortOrderList:(NSArray*) sortOrderList{
	if (!_sortOrderList || ![_sortOrderList isEqual:sortOrderList]){
		[_sortOrderList release];
		_sortOrderList = [sortOrderList retain];
		[self resetStmtOrder];
	}
}

-(void)setLimit:(NSUInteger)aLimit{
	if (limit!=aLimit){
		limit = aLimit;
		[self resetStmtLimit];
	}
}



-(NSString*)registerParameterName:(NSString*) parameterName parameterValue:(id)parameterValue{
	
    if (!parameterName || !parameterValue){
        return nil;   
    }
	NSArray* keys = [_parameters allKeysForObject:parameterValue];
	if ([keys count])
		return keys[0];
	
	NSMutableString* validParameterName = [NSMutableString stringWithString:parameterName];
	if (![[parameterName substringToIndex:1] isEqualToString:@"@"])
		[validParameterName insertString:@"@" atIndex:0];
    
	while(_parameters[validParameterName]!=nil)
		[validParameterName appendString:@"0"];
    
	_parameters[validParameterName] = parameterValue;
	return validParameterName;
}

-(NSString*)buildStmt{
	if (_preparedStmt)
		return _preparedStmt;
	if (!_stmtWherePrepared){
		if (_filterCondition){
			[_preparedStmtWhere release];
            _preparedStmtWhere = nil;
			[self extractParams];
            NSString* filterStr = [_filterCondition description];
            if (filterStr.length){
                _preparedStmtWhere = [[NSMutableString alloc]initWithString: @" where "];
                [_preparedStmtWhere appendString: filterStr];
            }
		}
		_stmtWherePrepared = TRUE;
	}
	if (!_stmtOrderPrepared){
		if (_sortOrderList)	{
			[_preparedStmtOrder release];
			_preparedStmtOrder = [[NSMutableString alloc]init];
			BOOL moreThanOneColumn = FALSE;
			for(SqlSortOrder* sortOrder in _sortOrderList){
                NSString* sortOrderStr = [sortOrder description];
                if (sortOrderStr.length){
                    if (!_preparedStmtOrder.length)
                        [_preparedStmtOrder appendString:@" order by "];    
                    if (moreThanOneColumn)
                        [_preparedStmtOrder appendString:@","];
                    [_preparedStmtOrder appendString:sortOrderStr];
                    moreThanOneColumn = TRUE;
                }
			}
		}
		_stmtOrderPrepared = TRUE;
	}
    if (!_stmtLimitPrepared){
		if (limit)	{
			[_preparedStmtLimit release];
			_preparedStmtLimit = [[NSString alloc]initWithFormat:@" limit %lu",(unsigned long)limit];
		}
		_stmtLimitPrepared = TRUE;
	}
	_preparedStmt = [[NSMutableString alloc]initWithString:_stmt ? _stmt : @""];
	if (_preparedStmtWhere)
		[_preparedStmt appendString:_preparedStmtWhere];
	if (_preparedStmtOrder)
		[_preparedStmt appendString:_preparedStmtOrder];
    if (_preparedStmtLimit)
		[_preparedStmt appendString:_preparedStmtLimit];
	return _preparedStmt;
}

-(NSDictionary*) parameters{
	[self extractParams];
	return _parameters;
}

-(NSString*) stmt{
	return _stmt;
}

-(int)executeCount{
	EGODatabase* db = [DataManager instance].currentDatabase;
	EGODatabaseResult* result =  [db executeQuery:[NSString stringWithFormat:@"select count(*) from (%@)",[self buildStmt]] namedParameters:[self parameters]];
	if ([result count]){
		EGODatabaseRow* row = [result rowAtIndex:0];
		return [row intForColumnIndex:0];
	}
	return 0;
}

-(EGODatabaseResult*)execute
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	return [db executeQuery:[self buildStmt] namedParameters:[self parameters]];
}

//-(EGODatabaseResult*)executeIds{
//-(NSArray*)executeIds{
-(NSIntegerArray*)executeIds{
	EGODatabase* db = [DataManager instance].currentDatabase;
	//return [db executeQuery:[NSString stringWithFormat:@"select rowid from (%@)",[self buildStmt]] namedParameters:[self parameters]];
    return [db executeQueryFastArray:[NSString stringWithFormat:@"select rowid from (%@)",[self buildStmt]] parametersList:[self parameters]];
}

-(EGODatabaseResult*)executeWithLimit:(NSUInteger)aLimit offset:(NSUInteger)anOffset
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	return [db executeQuery:[NSString stringWithFormat:@"select * from (%@) limit %lu offset %lu",[self buildStmt], (unsigned long)aLimit, (unsigned long)anOffset]
            namedParameters:[self parameters]];
}

-(void) dealloc{
	
	[_filterCondition release];
	[_sortOrderList release];
	[_stmt release];
	[_preparedStmtOrder release];
	[_preparedStmtWhere release];
    [_preparedStmtLimit release];
	[_preparedStmt release];
	[_parameters release];
	
	[super dealloc];
}
@end
