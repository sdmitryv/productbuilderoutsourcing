//
//  SqlFieldCondition.m
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlFieldCondition.h"
#import "SqlCommand.h"

@interface SqlFieldCondition (Private)
+(NSString*)operatorToString:(CompareOperator) compareOperator;
@end

@implementation SqlFieldCondition (Private)
+(nonnull NSString*)operatorToString:(CompareOperator) compareOperator{
	switch (compareOperator) {
		case NonEqual:
			return @"!=";
		case Greater:
			return @">";
		case Less:
			return @"<";
		case GreaterOrEqual:
			return @">=";
		case LessOrEqual:
			return @"<=";
		case Like:
			return @"like";
		case Null:
			return @"is null";
		case NotNull:
			return @"is not null";
		default:
			return @"=";
	}
}
@end

@implementation SqlFieldCondition

@synthesize columnName = _columnName;
@synthesize value = _value;
@synthesize compareOperator = _operator;

-(nonnull instancetype) init:(nonnull NSString*)columnName compareOperator:(CompareOperator)operator value:(nullable id)value{
	if ((self = [super init])) {
		_operator = operator;
		_columnName = [columnName retain];
		_value = [value retain];
	}
	return self;
}

+(nonnull instancetype) condition:(nonnull NSString*)columnName compareOperator:(CompareOperator)operator value:(nullable id)value{
	return [[[[self class] alloc]init:columnName compareOperator:operator value:value] autorelease];
	
}

-(nonnull NSString*)description{
	if ((_operator == Null) || (_operator == NotNull)) {
		return [NSString stringWithFormat:@"%@ %@",_columnName, [SqlFieldCondition operatorToString:_operator]];
	}
	return [NSString stringWithFormat:@"%@ %@ %@",self->_columnName, [SqlFieldCondition operatorToString:self->_operator], [self parameterName]];
}

-(nullable NSString*) parameterName{
    if (_operator == Null || _operator == NotNull)
        return nil;
    NSInteger dotIndex = [_columnName rangeOfString:@"."].location;
    NSString *paramName = [NSString stringWithFormat:@"@%@", dotIndex != NSNotFound ? [_columnName substringFromIndex:dotIndex+1] : _columnName];
	if (_command)
		return [_command registerParameterName:paramName parameterValue:_value];
	return paramName;	
}

-(nullable id)parameterValue{
	return _value;
}

-(void) dealloc{
	
	[_columnName release];
	[_value release];
	[super dealloc];
}

@end
