//
//  SqlBuilder.h
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqlFilterCondition.h"
#import "SqlSortOrder.h"
#import "SqlFieldCondition.h"
#import "SqlFieldConditionRange.h"
#import "SqlAndCondition.h"
#import "SqlOrCondition.h"
#import "DataManager.h"
#import "NSIntegerArray.h"

@interface SqlCommand : NSObject {
	NSString* _stmt;
	SqlFilterCondition* _filterCondition;
	NSArray* _sortOrderList;
	NSMutableDictionary* _parameters;
	BOOL _paramsExtracted;
	BOOL _stmtWherePrepared;
	BOOL _stmtOrderPrepared;
   	BOOL _stmtLimitPrepared;
	NSMutableString* _preparedStmtWhere;
	NSMutableString* _preparedStmtOrder;
  	NSString* _preparedStmtLimit;
	NSMutableString* _preparedStmt;
    NSUInteger limit;
}

-(id)initWithStmt:(NSString*) stmt;
-(id)initWithStmt:(NSString*) stmt condition:(SqlFilterCondition*)condition;
-(id)initWithStmt:(NSString*) stmt condition:(SqlFilterCondition*)condition sortOrderList:(NSArray*) sortOrderList;
-(id)initWithStmt:(NSString*) stmt sortOrderList:(NSArray*) sortOrderList;
-(NSDictionary*) parameters;
-(NSString*) stmt;
-(NSString*)buildStmt;
-(EGODatabaseResult*)execute;
//-(EGODatabaseResult*)executeIds;
//-(NSArray*)executeIds;
-(NSIntegerArray*)executeIds;
-(EGODatabaseResult*)executeWithLimit:(NSUInteger)limit offset:(NSUInteger)offset;
-(int)executeCount;
-(NSString*)registerParameterName:(NSString*) parameterName parameterValue:(id) parameterValue;

@property (nonatomic, retain) NSArray *sortOrderList;
@property (nonatomic, retain) SqlFilterCondition *filterCondition;
@property (nonatomic, assign)NSUInteger limit;
@end
