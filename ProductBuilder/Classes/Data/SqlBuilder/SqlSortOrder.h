//
//  SqlSortOrder.h
//  ProductBuilder
//
//  Created by Valera on 9/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SortOrder)
{
	SqlSortOrderAscending = 0,
	SqlSortOrderDescending = 1
};

@interface SqlSortOrder : NSObject {
	NSString* _columnName;
	SortOrder _sortOrder;
}
-(id) initWithColumnName:(NSString*)columnName order:(SortOrder)order;
+(id) orderBy:(NSString*)columnName order:(SortOrder)order;
@property (nonatomic, retain) NSString *columnName;
@property (nonatomic, assign) SortOrder sortOrder;
@end
