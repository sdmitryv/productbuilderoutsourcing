//
//  SqlFieldCondition.m
//  ProductBuilder
//
//  Created by Valera on 9/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SqlFieldConditionRange.h"
#import "SqlCommand.h"

@interface SqlFieldConditionRange (Private)
+(NSString*)operatorToString:(CompareOperatorRange) compareOperator;
-(NSString*)paramsToString;
-(void) genParameters;
@end

@implementation SqlFieldConditionRange (Private)
+(NSString*)operatorToString:(CompareOperatorRange) compareOperator
{
	switch (compareOperator) {
		case CompareOperatorRangeInRange:
			return @"between";
        case CompareOperatorRangeNotInRange:
			return @"not between";
        case CompareOperatorRangeNotInList:
			return @"not in";
        case CompareOperatorRangeInList:
		default:
			return @"in";
	}
}

-(NSString*)paramsToString
{
	NSMutableString *str = [[NSMutableString alloc]init];
	[self genParameters];
	if (_parameters)
	{
		if (_compareOperator==CompareOperatorRangeInList || _compareOperator==CompareOperatorRangeNotInList){
			int i = 0;
			[str appendString:@"("];
			for(NSString* param in _parameters)
			{
				if(i++>0) [str appendString:@","];
				[str appendString:param];
			}
			[str appendString:@")"];
		}
		else if(_compareOperator==CompareOperatorRangeInRange || _compareOperator==CompareOperatorRangeNotInRange){
			if([_parameters count]==2){
				[str appendFormat:@"%@ and %@",[_parameters allKeys][0], [_parameters allKeys][1]];	
			}
		}
	}
	return [str autorelease];
}


-(void) genParameters
{ 
	if(_parametersGenerated) return;
	_parameters = [[NSMutableDictionary alloc]init];
	NSInteger count = [_values count];
	if ((_compareOperator == CompareOperatorRangeInRange || _compareOperator == CompareOperatorRangeNotInRange) && count>2) count = 2;
	for(int i = 0;i<count;i++)
	{
		NSString *str = nil;
		if (_command){
			str = [_command registerParameterName:[NSString stringWithFormat:@"@%@",_columnName] parameterValue:_values[i]];
		}
		else {
			str = [NSString stringWithFormat:@"@%@%i",_columnName,i];
		}
		_parameters[str] = _values[i];
	}
	_parametersGenerated = TRUE;
}
@end

@implementation SqlFieldConditionRange

@synthesize columnName = _columnName;
@synthesize values = _values;

-(id) init:(NSString*)columnName compareOperatorRange:(CompareOperatorRange)compareOperator values:(NSArray*)values
{
	if ((self = [super init])) {
		_compareOperator = compareOperator;
		_columnName = [columnName retain];
		if (values){
			_values = [values retain];
			_parametersGenerated = FALSE;
		}
	}
	return self;
}

+(id) condition:(NSString*)columnName compareOperatorRange:(CompareOperatorRange)operator values:(NSArray*)values
{
	return [[[SqlFieldConditionRange alloc]init:columnName compareOperatorRange:operator values:values] autorelease];
	
}

-(NSString*)description
{
	return [NSString stringWithFormat:@"%@ %@ %@",_columnName, [SqlFieldConditionRange operatorToString:_compareOperator], [self paramsToString]];
}


-(NSDictionary*)parameters
{
	[self genParameters];
	return _parameters;
}

-(void) dealloc
{
	[_columnName release];
	[_values release];
	[_parameters release];
	
	[super dealloc];
}

@end
