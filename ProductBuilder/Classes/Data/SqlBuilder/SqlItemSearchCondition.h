//
//  SqlItemSearchCondition.h
//  ProductBuilder
//
//  Created by Roman on 1/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqlFieldCondition.h"
#import "ItemList.h"

typedef NS_ENUM(NSUInteger, ItemSearchType) { ItemSearchByAll = 0, ItemSearchByDescription = 1, ItemSearchByAttribute = 2, ItemSearchByCLU = 3, ItemSearchByStyleNum = 4, ItemSearchByUPC = 5, ItemSearchByPLU = 6, ItemSearchBySN = 7 } ;

typedef NS_ENUM(NSUInteger, ItemSearchResultType) { ItemSearchResultUnknown = 0, ItemSearchResultCLU = 1, ItemSearchResultStyleNum = 2, ItemSearchResultUPC = 3, ItemSearchResultPLU = 4, ItemSearchResultSN = 5 } ;




@interface SqlItemSearchCondition : NSObject

+(SqlFilterCondition*)getItemSearchCondition:(NSString*)searchText includePLU:(BOOL)includePLU includeCLU:(BOOL)includeCLU includeUPC:(BOOL)includeUPC includeSN:(BOOL)includeSN includeStyleNo:(BOOL)includeStyleNo searchType:(ItemSearchType)searchType;

+(ItemSearchResultType)filterOnItemList:(ItemList*)itemList searchText:(NSString *)searchText searchType:(ItemSearchType)searchType;

+(NSString *)typeName:(ItemSearchType)type;

+(BOOL)isField:(NSString *)field ofSearchType:(ItemSearchType)type;

@end


@interface SqlUPCSearchCondition : SqlFieldCondition

+(BOOL)isThisItem:(BPUUID*)itemId upc:(NSString *)upc;

@end

@interface SqlCLUSearchCondition : SqlFieldCondition

@end

@interface SqlSNSearchCondition : SqlFieldCondition

+(BOOL)isThisItem:(BPUUID*)itemId sn:(NSString *)sn;

@end

@interface SqlCLU_UPCSearchCondition : SqlFieldCondition

@end

@interface SqlStyleNoSearchCondition : SqlFieldCondition

@end

@interface SqlInvenItemCondition : SqlFieldCondition {
}

@property (nonatomic, assign) ItemSearchType searchType;

@end
