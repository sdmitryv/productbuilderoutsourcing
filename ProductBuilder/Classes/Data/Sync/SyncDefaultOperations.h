//
//  SyncDefaultOperations.h
//  CloudworksPOS
//
//  Created by valera on 7/21/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncOperationQueue.h"

@interface SyncDefaultOperations : NSObject

+(NSArray*)defaultDownloadOperations:(SyncOperationMode)mode;
+(NSArray*)defaultUploadOperations;
@end

