//
//  SyncOperation.h
//  CloudworksPOS
//
//  Created by valera on 7/18/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncManager.h"
#import "SyncOperationQueue.h"
#import "WebServiceOperation.h"

#define LocationMismatchErrorCode 1001
#define NoOnlineDrawerMemoErrorCode 1002L
#define DMWasChangedErrorCode 1003
#define SOWasChangedErrorCode 1004
#define ReceiptNotFoundErrorCode 1005
#define PasswordMustBeDifferentErrorCode 1006
#define PasswordExpiredErrorCode 1007
#define SMWasChangedErrorCode 1008
#define UserExpiredErrorCode 1009
#define UserInvalidErrorCode 1010
#define UserNotAllowedErrorCode 1011

@interface SyncOperation : NSOperation<SyncManagerDelegate> {
@protected
	float	  progress;
    SyncOperationMode mode;
	NSString	* name;
    SyncOperationQueue* parentQueue;
    BOOL isExecuting;
    BOOL isFinished;
    SyncManager* syncManager;
    dispatch_semaphore_t waitForRequestSemaphore;
    NSObject* operationResult;
    BOOL result;
    NSString* lastError;
    NSError* lastErrorObject;
    NSTimeInterval workingTime;
    NSTimeInterval startTime;
    NSString* title;
}
@property (atomic, readonly) WebServiceOperation* currentOperation;
@property (nonatomic, assign) float	  progress;
@property (nonatomic, readonly)SyncOperationMode mode;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
@property (atomic, retain) NSString	* name;
#pragma clang diagnostic pop
@property (atomic, retain) NSString	* title;
@property (nonatomic, assign) SyncOperationQueue*  parentQueue;
@property (atomic, readonly) NSObject* operationResult;
@property (nonatomic, readonly) NSTimeInterval workingTime;
@property (nonatomic, readonly) NSTimeInterval startTime;
@property (nonatomic, readonly)NSString* address;
@property (atomic, retain)NSError* lastErrorObject;
@property (atomic, retain)NSString* lastError;
-(void)sync;
-(void)waitForRequest;
-(void)postNotification:(NSString*)name;
-(BOOL)result;
-(BOOL)shouldCancelOnError:(NSError*)error;
-(BOOL)shouldPostError:(NSError*)error;
-(void)signalOperationDidComplete;
-(void)postError:(NSString*)errorText error:(NSError*)error;
+(id)syncOperation;
+(id)syncOperationWithMode:(SyncOperationMode)mode;
@end





