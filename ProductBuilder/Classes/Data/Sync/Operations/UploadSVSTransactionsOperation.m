//
//  UploadSVSTransactionsOperation.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 1/24/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UploadSVSTransactionsOperation.h"

@implementation UploadSVSTransactionsOperation

-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_SVSTRANSACTIONS_TEXT", nil);
}


-(SEL)uploadSelector {
    return @selector(updateSVSTransactions:);
}

-(NSString*)tableName {
    return @"SVSTransaction";
}

@end
