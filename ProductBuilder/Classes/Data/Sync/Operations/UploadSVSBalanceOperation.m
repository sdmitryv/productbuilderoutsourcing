//
//  UploadSVSBalanceOperation.m
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 1/10/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "UploadSVSBalanceOperation.h"

@implementation UploadSVSBalanceOperation


-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_BALANCES_TEXT", nil);
}


-(SEL)uploadSelector {
    return @selector(updateSVSBalances:);
}

-(NSString*)tableName {
    return @"ReceiptSVSBalance";
}

@end
