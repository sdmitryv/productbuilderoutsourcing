//
//  SyncRecordStatusDeletedOperation.m
//  ProductBuilder
//
//  Created by valera on 3/6/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncRecordStatusDeletedOperation.h"
#import "DataManager.h"
#import "DBAdditions.h"

@implementation SyncRecordStatusDeletedOperation

+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode beforeRowsWillDelete:(void (^)(NSString* tableName))beforeRowsWillDelete{
    SyncRecordStatusDeletedOperation* operation = [[[self.class alloc]initWithTable:table mode:mode]autorelease];
    operation.beforeRowsWillDelete = beforeRowsWillDelete;
    return operation;
}
-(void)dealloc{
    [_beforeRowsWillDelete release];
    [super dealloc];
}

-(NSString*)tableName{
    return @"RecordStatusDeleted";
}

-(NSString*)sourceTableName{
    if (self.mode == SyncOperationModeInitialization){
        return @"RecordStatusDeletedLast"; 
    }
    return self.tableName;
}

- (void) sync{
    [super sync];
    if (mode==SyncOperationModeSynchronization){
        NSString* oldTitle = [self.title retain];
        self.title = [NSString stringWithFormat:NSLocalizedString(@"OPERATION_PROCESSING_FORMAT_TEXT", nil), self.tableName];
        EGODatabase* db = [[DataManager instance]currentDatabase];
        EGODatabaseResult* res = [db executeQuery:@"select TableName from RecordStatusDeleted where Handled is null group by TableName"];
        for (EGODatabaseRow* row in res.rows){
            NSString* tName = [row stringForColumn:@"TableName"];
            if (_beforeRowsWillDelete){
                _beforeRowsWillDelete(tName);
            }
            NSString* tblName = tName;
            NSString* pkColumnName = nil;
            if ([tName isEqualToString:@"MobilePrintingDesign"]){
                pkColumnName = @"MobilePrintingDesignId";
            }
            else{
               
                if ([tblName isEqualToString:@"LocationAvailabilityGroupLocation"])
                    tblName = @"LocationAvailabilityGroup_Location";
                
                TableDetails* tableDetails = [[DataManager instance] getTableInfo:tblName];
                if (!tableDetails){
                    continue;
                }
                if (tableDetails.tablePrimaryKeys.count!=1){
                    continue;
                }
                TableRowInfo* pkRowInfo = (tableDetails.tablePrimaryKeys)[0];
                pkColumnName = [[DataManager class ]quoteName:pkRowInfo.columnName];
            }
            NSMutableDictionary* params = [[NSMutableDictionary alloc]init];
            [params setObjectNilSafe:tName forKey:@"tableName"];
            [db beginTransaction];
            if (([db executeUpdate:[NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ IN (SELECT rsd.RecordID FROM RecordStatusDeleted rsd, %@ delTab WHERE rsd.TableName=@tableName AND rsd.Handled is null AND rsd.RecordID = delTab.%@ AND rsd.RecModified > delTab.RecModified)", [[DataManager class ] quoteName:tblName], pkColumnName, [[DataManager class ] quoteName:tblName], pkColumnName] namedParameters:params] || ![[DataManager instance] object_exists:@"POS$RTW RB Report" type:@"table"])
                && [db executeUpdate:@"update RecordStatusDeleted set Handled = 1 where TableName=@tableName and Handled is null" namedParameters:params]){
                [db commit];
            }
            else{
                [db rollback];
            }
            [params release];
        }
        self.title = oldTitle;
        [oldTitle release];
    }
    else{
        [[[DataManager instance]currentDatabase] executeUpdate:@"update RecordStatusDeleted set Handled = 1"];
    }
}

@end
