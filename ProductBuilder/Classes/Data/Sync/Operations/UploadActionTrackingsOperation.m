//
//  UploadActionTrackingsOperation.m
//  ProductBuilder
//
//  Created by valera on 10/17/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UploadActionTrackingsOperation.h"

@implementation UploadActionTrackingsOperation


-(id)init{

    if ((self =[super init])){
        uploadTablesCnt = 1;
    }
    
    return self;
}


-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_ACTIONS_TEXT", nil);
}


-(SEL)uploadSelector {
    return @selector(updateActionsTracking:);
}

-(NSString*)tableName {
    return @"ActionsTracking";
}

@end
