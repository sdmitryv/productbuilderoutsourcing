//
//  UploadQueryResultOperation.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UploadQueryResultOperation.h"
#import "POSQuery.h"
#import "POSQueryResult.h"

@implementation UploadQueryResultOperation


-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_QUERY_TEXT", nil);
}


-(SEL)uploadSelector {
    return @selector(uploadQueryResults:);
}

-(NSString*)tableName {
    return @"PosQueryResult";
}

-(void)sync {
    NSMutableArray* queries = [POSQuery getActiveQueries];
    EGODatabase* db = [DataManager instance].currentDatabase;
    for (POSQuery *query in queries) {
        POSQueryResult* qresult = [[POSQueryResult alloc] init];
        qresult.queryID = query.id;
        EGODatabaseResult* dbresult = [db executeQuery:query.queryStr];
        
        if (!dbresult || [db hadError]) {
            qresult.result = [db lastErrorMessage];
        }
        else {
            xmlDocPtr doc = xmlNewDoc((const xmlChar*)XML_DEFAULT_VERSION);
            
            NSNamedMutableArray* nmdArray = [[DataManager instance] getRecords:dbresult];
            nmdArray.name = @"Record";
            
            xmlNodePtr bodyNodeContent = [nmdArray xmlNodeForDoc:doc elementName:@"Table" elementNSPrefix:nil];
            xmlDocSetRootElement(doc, bodyNodeContent);
            xmlChar *buf;
            int size;
            xmlDocDumpFormatMemory(doc, &buf, &size, 1);
            
            qresult.result = @((const char*)buf);
            xmlFree(buf);
            
            xmlFreeDoc(doc);
        }
        
        [qresult save];
        [qresult release];
    }
    
    [super sync];
}

@end
