//
//  UploadCustomUILayoutOperation.m
//  Shipments
//
//  Created by valery on 6/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "UploadCustomUILayoutOperation.h"
#import "Location.h"
#import "AppSettingManager.h"
#import "SecurityManager.h"

@implementation UploadCustomUILayoutOperation

-(NSString *)operationTitle {
    return NSLocalizedString(@"UPLOADING_SVSTRANSACTIONS_TEXT", nil);
}

-(NSString*)tableName {
    return @"CustomUILayout";
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return FALSE;
}

-(id)initWithCustomUILayoutManager:(CustomUILayoutManager*)layoutManager{
    if ((self =[super init])){
        self.name = self.title = [self operationTitle];
        _layoutManager = [layoutManager retain];
    }
    return self;
}

-(void)dealloc{
    [_layoutManager release];
    [super dealloc];
}

- (void) sync{
    
    NSString* tblName = [self tableName];
    NSNamedMutableArray* updateItems = [[NSNamedMutableArray alloc]init];
    @try{
        NSArray* layouts = _layoutManager.layouts;
        for (CustomUILayout* layout in layouts){
            if (!layout.key || !layout.value) continue;
            NSMutableDictionary* dict = [[NSMutableDictionary alloc]init];
            dict[@"RecCreated"] = layout.recCreated ?:[NSDate date];
            dict[@"RecModified"] = layout.recModified?:[NSDate date];
            dict[@"CustomUILayoutId"] = layout.id?:[BPUUID UUID];
            dict[@"LocationId"] = layout.locationId?:[Location localLocation].id;
            dict[@"EditDeviceId"] = layout.editDeviceId?:[AppSettingManager instance].deviceServerId;
            dict[@"EditEmployeeId"] = layout.editEmployeeId?:[SecurityManager currentEmployee].id;
            dict[@"Key"] = layout.key;
            dict[@"Value"] = layout.value;
            [updateItems addObject:dict];
            [dict release];
        }
        if (self.isCancelled || !updateItems.count) return;
        updateItems.name = tblName;
        result = FALSE;
        [syncManager uploadCustomUILayouts:updateItems];
        [self waitForRequest];
        self.progress = 1.0;
    }
    @finally{
        [updateItems release];
    }
}

@end
