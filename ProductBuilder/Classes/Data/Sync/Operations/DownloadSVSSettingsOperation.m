#import "DownloadSVSSettingsOperation.h"
#import "AppSettingManager.h"
#import "EGODatabaseRow.h"
#import "StoredValuesService.h"

@interface DownloadSVSSettingsOperation(){
}

@property (atomic, retain)StoredValuesServiceRequest* settingsRequest;
@end

@implementation DownloadSVSSettingsOperation

-(id)init{
    if ((self =[super init])){
        self.name = NSLocalizedString(@"DOWNLOADING_SVS_SETTINGS_TEXT", nil);
        self.title = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"DOWNLOADING_SVS_SETTINGS_TEXT", nil)];
    }
    return self;
}

-(void)dealloc{
    _settingsRequest.delegate = nil;
    [_settingsRequest waitUntilDelegateQueueCompleted];
    [_settingsRequest release];
    [super dealloc];
}

- (void) sync {
    self.settingsRequest = [[StoredValuesService sharedInstance] getSettings];
    self.settingsRequest.delegate = self;
    [self.settingsRequest exec];
    [self waitForRequest];
}

- (void)cancel {
    [super cancel];
    result = NO;
    self.settingsRequest.delegate = nil;
    [self.settingsRequest cancel];
    [self signalOperationDidComplete];
}

- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withResult:(NSObject *)svsResult {
    if (svsResult) {
        if (request.requestType == GETSETTINGS) {
            NSDictionary * lrpSettingsDict = ((NSDictionary *)svsResult)[@"lrp"];
            if ([lrpSettingsDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* lrpSettings = [[EGODatabaseRowDictionary alloc]initWithDictionary:lrpSettingsDict];
                AppSettingManager.instance.svs_LRPThreshold = [lrpSettings decimalNumberForColumn:@"threshold"];
                [lrpSettings release];
            }
            NSDictionary * systemSettingsDict = ((NSDictionary *)svsResult)[@"system"];
            if ([systemSettingsDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* systemSettings = [[EGODatabaseRowDictionary alloc]initWithDictionary:systemSettingsDict];
                AppSettingManager.instance.svs_CrmLRPCalculationEnabled = [systemSettings boolForColumn:@"crmLRPCalculationEnabled"];
                [systemSettings release];
            }
            NSDictionary * tokenSettingsDict = ((NSDictionary *)svsResult)[@"token"];
            if ([tokenSettingsDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* tokenSettings = [[EGODatabaseRowDictionary alloc]initWithDictionary:tokenSettingsDict];
                AppSettingManager.instance.svs_TokenEnabled = [tokenSettings boolForColumn:@"isTokenEnabled"];
                [tokenSettings release];
            }
            NSDictionary * emailSettingsDict = ((NSDictionary *)svsResult)[@"email"];
            if ([emailSettingsDict isKindOfClass:NSDictionary.class]){
                EGODatabaseRowDictionary* emailSettings = [[EGODatabaseRowDictionary alloc]initWithDictionary:emailSettingsDict];
                AppSettingManager.instance.svs_FreshAddressEnabled = [emailSettings boolForColumn:@"freshAddressEnabled"];
                [emailSettings release];
            }
            [operationResult release];
            operationResult = [svsResult retain];
            result = TRUE;
            [self signalOperationDidComplete];
        }
    }
}

- (void)storedValuesServiceRequestDidComplete:(StoredValuesServiceRequest *)request withError:(NSError *)error {
    if (request.requestType == GETSETTINGS) {
        [operationResult release];
        operationResult  = nil;
        result = FALSE;
        
        if (error.code == NSURLErrorNotConnectedToInternet) {
            
            NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
            [errorDetail setValue:NSLocalizedString(@"INTERNET_OFFLINE_MSG", nil) forKey:NSLocalizedDescriptionKey];
            [errorDetail setValue:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil] forKey:NSUnderlyingErrorKey];
            error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:errorDetail];
        }
        
        [self postError:error.localizedDescription error:error];
        if ([self shouldCancelOnError:error])
            [self cancel];
        else
            [self signalOperationDidComplete];
    }
}

@end
