//
//  SyncOperation_DownloadSVSSettingsOperation.h
//  ProductBuilder
//
//  Created by valery on 4/15/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"
#import "StoredValuesServiceRequest.h"

@interface DownloadSVSSettingsOperation : SyncOperation<StoredValuesServiceDelegate>

@end
