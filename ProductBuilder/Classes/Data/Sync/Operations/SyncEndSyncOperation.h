//
//  SyncEndSyncOperation.h
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncOperation.h"

@interface SyncEndSyncOperation : SyncOperation

@end
