//
//  CommitReturnItemsOperation.m
//  ProductBuilder
//
//  Created by valera on 6/26/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "CommitReturnItemsOperation.h"

@implementation CommitReturnItemsOperation


- (void) sync {
    [syncManager commitReturnItems:self.commitItems];
    [self waitForRequest];
}

-(void)dealloc{
    self.commitItems = nil;
    [super dealloc];
}

@end
