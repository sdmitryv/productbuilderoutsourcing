//
//  CheckServiceAvailability.h
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 10/17/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@interface CheckServiceAvailabilityOperation : SyncOperation

@end
