//
//  ReserveReturnItemsOperation.m
//  ProductBuilder
//
//  Created by valera on 6/26/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "ReserveReturnItemsOperation.h"

@implementation ReserveReturnItemsOperation


- (void) sync {
    [syncManager reserveReturnItems:self.reserveItems];
    [self waitForRequest];
}

-(void)dealloc{
    self.reserveItems = nil;
    [super dealloc];
}

@end


@implementation ReserveItem

-(void)dealloc{
    self.originalReceiptItemId = nil;
    self.originalReceiptId = nil;
    self.returnReceiptItemId = nil;
    self.returnReceiptId = nil;
    [super dealloc];
}
@end
