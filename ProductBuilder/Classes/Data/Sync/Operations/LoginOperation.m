//
//  LoginOperation.m
//  DeviceAgent
//
//  Created by Alexander Martyshko on 12/25/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "LoginOperation.h"
#import "Employee.h"

@implementation LoginOperation

@synthesize employee = _employee;

- (void)dealloc {
    [_login release];
    [_password release];
    [_successfulNum release];
    [_employee release];
    [super dealloc];
}

- (id)initWithLogin:(NSString *)login password:(NSString *)password {
    self = [super init];
    if (self) {
        _login = [login retain];
        _password = [password retain];
    }
    return self;
}

- (void) sync {
    [_successfulNum release];
    _successfulNum = nil;
    
    [syncManager authorizeWithLogin:_login password:_password];
    [self waitForRequest];
    
    if (result) {
        XmlElement *xmlLocation = (XmlElement *)operationResult;
        [_employee release];
        _employee = [[Employee alloc] initWithRow:xmlLocation.childrenToEGODatabaseRow];
        //        _successfulNum = [NSNumber numberWithBool:[[xmlLocation.value lowercaseString] isEqualToString:@"true"]];
        _successfulNum = @((BOOL)(_employee.id != nil));
    }
}


@end
