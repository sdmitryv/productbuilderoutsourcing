//
//  UploadActionTrackingsOperation.h
//  ProductBuilder
//
//  Created by valera on 10/17/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadTableOperation.h"

@interface UploadActionTrackingsOperation : UploadTableOperation

@end
