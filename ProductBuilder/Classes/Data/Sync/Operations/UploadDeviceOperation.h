//
//  UploadDeviceOperation.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 11/21/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@class Device;

@interface UploadDeviceOperation : SyncOperation {

    NSString *SVSAuthTokenForUpload;
}

@property (nonatomic, retain) NSString *SVSAuthTokenForUpload;


@end
