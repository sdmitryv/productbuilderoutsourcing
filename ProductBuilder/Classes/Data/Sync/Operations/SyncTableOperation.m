//
//  SyncTableOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncTableOperation.h"
#import "SyncTableHandler.h"
#import "RTProtoDataReader.h"
#import <libkern/OSAtomic.h>
#import "GAELog.h"
#import "DataUtils.h"

#define USE_THREAD
//#define USE_MUTEX

@interface RTBufferFifo : NSObject{
    CFMutableArrayRef array;
    dispatch_semaphore_t semaphore;
    dispatch_semaphore_t semaphore_limit;
    //dispatch_queue_t queue;
#ifdef USE_MUTEX
    pthread_mutex_t mutex;
#else
//    OSSpinLock spinlock;
    os_unfair_lock _lock;
#endif
    NSUInteger maxSize;
    BOOL isClosed;
}
-(id)initWithMaxSize:(NSUInteger)size;
-(NSObject*)pop;
-(void)push:(NSObject*)obj;
-(void)push:(NSObject *)obj invokeSelector:(SEL)sel;
-(void)pushFinish;
-(BOOL)isClosed;
-(NSUInteger)count;
-(NSUInteger)maxSize;
@end

@implementation RTBufferFifo

-(id)initWithMaxSize:(NSUInteger)size{
    if ((self=[super init])){
#ifdef USE_MUTEX
        pthread_mutexattr_t mta;
        pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_NORMAL);
        pthread_mutexattr_init(&mta);
        pthread_mutex_init(&mutex,  &mta);
#else
        //spinlock = OS_SPINLOCK_INIT;
        _lock = OS_UNFAIR_LOCK_INIT;
#endif
        maxSize = size;
        array = CFArrayCreateMutable(kCFAllocatorDefault, maxSize, &kCFTypeArrayCallBacks);
        semaphore = dispatch_semaphore_create(0);
        semaphore_limit = dispatch_semaphore_create(maxSize);
        //queue = dispatch_queue_create("com.cloudwk.rtBuffer",NULL);
        
    }
    return self;
}
-(void)dealloc{
    [self close];
    //dispatch_sync(queue,^{});
    CFRelease(array);
    //dispatch_release(queue);
    dispatch_release(semaphore);
    // you can't dispose of a semaphore when the current value is less than the value it was created with.
    //http://stackoverflow.com/questions/8287621/why-does-this-code-cause-exc-bad-instruction
    for (NSInteger i = 0; i < maxSize; i++){
        dispatch_semaphore_signal(semaphore_limit);
    }
    dispatch_release(semaphore_limit);
#ifdef USE_MUTEX
    pthread_mutex_destroy(&mutex);
#endif
    [super dealloc];
}

-(void)close{
    isClosed = TRUE;
    dispatch_semaphore_signal(semaphore);
    dispatch_semaphore_signal(semaphore_limit);
}

-(void)pushFinish{
    dispatch_semaphore_signal(semaphore);
}

-(BOOL)isClosed{
    return isClosed;
}

-(NSUInteger)maxSize{
    return maxSize;
}

-(NSUInteger)count{
#ifdef USE_MUTEX
    pthread_mutex_lock(&mutex);
#else
//    OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
#endif
    NSUInteger result = CFArrayGetCount(array);
    
#ifdef USE_MUTEX
    pthread_mutex_unlock(&mutex);
#else
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
#endif
    return result;
}

-(NSObject*)pop{
    if (isClosed) return nil;
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    if (isClosed) return nil;
    __block NSObject* obj = nil;
    //dispatch_barrier_sync(queue,^{
#ifdef USE_MUTEX
    pthread_mutex_lock(&mutex);
#else
    //OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
#endif
        //if (isClosed) return;
    if (isClosed) return nil;
    if (CFArrayGetCount(array)){
        obj = [(id)CFArrayGetValueAtIndex(array, 0) retain];
        CFArrayRemoveValueAtIndex(array, 0);
        dispatch_semaphore_signal(semaphore_limit);
    }
#ifdef USE_MUTEX
    pthread_mutex_unlock(&mutex);
#else
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
#endif

    //});
    return [obj autorelease];
}

-(void)push:(NSObject *)obj{
    [self push:obj invokeSelector:nil];
}

-(void)push:(NSObject *)obj invokeSelector:(SEL)sel{
    if (isClosed) return;
    id objNew = nil;
    if (sel){
        long result = dispatch_semaphore_wait(semaphore_limit, DISPATCH_TIME_NOW);
        if (result){
            objNew = [obj performSelector:sel withObject:nil];
            dispatch_semaphore_wait(semaphore_limit, DISPATCH_TIME_FOREVER);
        }
    }
    else{
        dispatch_semaphore_wait(semaphore_limit, DISPATCH_TIME_FOREVER);
    }
    
    if (isClosed) return;
    //dispatch_barrier_async(queue,^{
#ifdef USE_MUTEX
    pthread_mutex_lock(&mutex);
#else
    //OSSpinLockLock(&spinlock);
    os_unfair_lock_lock(&_lock);
#endif
        if (isClosed) return;
        id objToAdd = nil;
        if (sel){
            if (objNew){
                objToAdd = objNew;
            }
            else{
                //do nothing
                objToAdd = obj;//[obj performSelector:sel withObject:nil];
            }
        }
        else{
            objToAdd = obj;
        }
    CFArrayAppendValue(array, objToAdd);
    dispatch_semaphore_signal(semaphore);
    //});
#ifdef USE_MUTEX
    pthread_mutex_unlock(&mutex);
#else
    //OSSpinLockUnlock(&spinlock);
    os_unfair_lock_unlock(&_lock);
#endif

}

@end

@interface SyncTableOperation(){
    NSInputStream* inputStream;
    dispatch_queue_t streamReadQueue;
    RTBufferFifo* bufferFifo;
#ifdef USE_THREAD
    dispatch_queue_t networkThreadAccessQueue;
    NSThread* networkThread;
#else
    dispatch_queue_t dbRecordsQueue;
#endif
    BOOL requestCompleted;
    BOOL _atLeastOneRowReceived;
    BOOL _forceCancelOnError;
}

@end

@implementation SyncTableOperation

@synthesize tableName;
@synthesize itemsCount;
@synthesize whereCondition = _whereCondition;
@synthesize conditionParams = _conditionParams;

NSNumber* serverVersion;

+(NSNumber*) serverVersion{
    return serverVersion;
}

+(void)setServerVersion:(NSNumber*)version{
    [serverVersion release];
    serverVersion = [version retain];
}

-(id)init{
    if ((self =[super init])){
        self.qualityOfService = NSQualityOfServiceUtility;
        syncManager.defaultTimeout = 240;
        streamReadQueue =  dispatch_queue_create("com.cloudwk.readqueue", NULL);
        dispatch_queue_t high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_set_target_queue(streamReadQueue,high);
        _isNotRequiresTableCount = NO;
#ifdef USE_THREAD
        networkThreadAccessQueue = dispatch_queue_create("com.cloudwk.networkThreadAccessQueue", NULL);
        dispatch_set_target_queue(networkThreadAccessQueue,high);
#endif
    }
    return self;
}

-(id) initWithTable:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)aMode {
    if ((self = [self initWithTable:table title:titleVal mode:aMode whereCondition:nil conditionParams:nil completedSync:nil])) {
        
    }
    return self;
}

-(id) initWithTable:(NSString *)table mode:(SyncOperationMode)aMode{
    if ((self = [self initWithTable:table title:nil mode:aMode whereCondition:nil conditionParams:nil completedSync:nil])) {
        
    }
    return self;
}

-(id) initWithTable:(NSString *)table mode:(SyncOperationMode)aMode isMandatory:(BOOL)isMandatoryParam{
    if ((self = [self initWithTable:table title:nil mode:aMode whereCondition:nil conditionParams:nil  completedSync:nil])) {
        
        isMandatory = isMandatoryParam;
    }
    
    return self;
}

-(id) initWithTable:(NSString *)table mode:(SyncOperationMode)aMode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams {
    if ((self = [self initWithTable:table title:nil mode:aMode whereCondition:whereCondition conditionParams:conditionParams completedSync:nil])) {
        
    }
    return self;
}

-(id) initWithTable:(NSString *)table mode:(SyncOperationMode)aMode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams isNotRequiresTableCount:(BOOL)isNotRequiresTableCount {
    if ((self = [self initWithTable:table title:nil mode:aMode whereCondition:whereCondition conditionParams:conditionParams completedSync:nil])) {
        _isNotRequiresTableCount = isNotRequiresTableCount;
    }
    return self;
}

-(id) initWithTable:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)aMode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock{
    
    if ((self = [self init])) {
        
        isMandatory = YES;
        self.title = [NSString stringWithFormat:@"%@ %@s...", aMode == SyncOperationModeSynchronization
                      ? NSLocalizedString(@"OPERATION_SYNCHRONIZING_TEXT", nil) : NSLocalizedString(@"OPERATION_INITIALIZING_TEXT", nil), titleVal ?:table];
        
        self.whereCondition = whereCondition;
        self.conditionParams = conditionParams;
        self.tableName = self.name = table;
        mode = aMode;
        if (mode==SyncOperationModeInitialization){
            self.qualityOfService = NSQualityOfServiceUserInteractive;
        }
        self.completedSyncBlock = aCompletedSyncBlock;
    }
    
    return self;
}


+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode{
    return [[[[self class]alloc]initWithTable:table mode:mode]autorelease];
}

+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode isMandatory:(BOOL)isMandatory{
    return [[[[self class]alloc]initWithTable:table mode:mode isMandatory:isMandatory]autorelease];
}

+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock{
    return [[[[self class]alloc]initWithTable:table title:nil mode:mode whereCondition:nil conditionParams:nil completedSync:aCompletedSyncBlock] autorelease];
}

+(id)syncTableOperation:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)mode {
    return [[[[self class]alloc]initWithTable:table title:titleVal mode:mode]autorelease];
}

+(id)syncTableOperation:(NSString *)table title:(NSString*)titleVal mode:(SyncOperationMode)mode completedSync:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock {
    return [[[[self class]alloc]initWithTable:table title:titleVal mode:mode whereCondition:nil conditionParams:nil completedSync:aCompletedSyncBlock] autorelease];
}

+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode whereCondition:(NSString *)whereCondition {
    return [[[[self class]alloc]initWithTable:table mode:mode whereCondition:whereCondition conditionParams:nil ]autorelease];
}

+(id)syncTableOperation:(NSString *)table mode:(SyncOperationMode)mode whereCondition:(NSString *)whereCondition conditionParams:(NSDictionary *)conditionParams isNotRequiresTableCount:(BOOL)isNotRequiresTableCount {
    return [[[[self class]alloc]initWithTable:table mode:mode whereCondition:whereCondition conditionParams:conditionParams isNotRequiresTableCount:isNotRequiresTableCount]autorelease];
}

-(void)dealloc{
    [bufferFifo release];
    [inputStream release];
    [_whereCondition release];
    [_conditionParams release];
    dispatch_release(streamReadQueue);
    [tableName release];
    [_completedSyncBlock release];
    
#ifdef USE_THREAD
    dispatch_sync(networkThreadAccessQueue, ^{});
    [networkThread release];
    dispatch_release(networkThreadAccessQueue);
#else
    if (dbRecordsQueue)
        dispatch_release(dbRecordsQueue);
#endif
    [super dealloc];
}

-(void)cancel{
    [super cancel];
    [self shutDownReadWriteThreads];
}

-(void)shutDownReadWriteThreads{
    [bufferFifo close];
    [inputStream close];
    dispatch_sync(streamReadQueue, ^{});
#ifdef USE_THREAD
    [self joinAndDestroyThreadForRequest];
#endif
}

#define RECORDS_IN_CACHE_BINARY 2000

-(void)insertRecordsFromBuffer:(NSArray*)columnNames{
    @autoreleasepool {
        NSUInteger recordsInsertedCount = 0;
        @try{
            DataManager *dm = [DataManager instance];
            if (![[dm currentDatabase] inTransaction]){
                if (![dm getTableInfo:self.tableName].tableRowsInfoArray.count){
                    [NSException raise:NSInternalInconsistencyException format:@"table rows info for table %@ is empty!", self.tableName];
                }
                [[dm currentDatabase]execute:@"PRAGMA foreign_keys = 0; PRAGMA ignore_check_constraints = 1; PRAGMA count_changes=OFF;"];
                [[dm currentDatabase]beginTransaction];
            }
            NSObject* row = nil;
            while (TRUE){
                if (self.isCancelled) break;
                
                @autoreleasepool {
                    row = [bufferFifo pop];
                    [row retain];
                    if (!row){
                        break;
                    }
                    _atLeastOneRowReceived = YES;
                    BOOL dbResult;
                    if (columnNames){
                        dbResult = [dm insertRecordFromColumns:columnNames values:(NSArray*)row toTable:self.tableName replace:(mode == SyncOperationModeSynchronization)];
                        if (!dbResult && mode == SyncOperationModeInitialization && dm.currentDatabase.lastErrorCode == SQLITE_CONSTRAINT){
                            dbResult = [dm insertRecordFromColumns:columnNames values:(NSArray*)row toTable:self.tableName replace:YES];
                        }
                    }
                    else{
                        NSDictionary* dictionary = nil;
                        if ([row isKindOfClass:[XmlElement class]]){
                            dictionary = [((XmlElement*)row) childrenToNoRetainedDictionary];
                        }
                        else{
                            dictionary = (NSDictionary*)row;
                        }
                        dbResult = [dm insertRecord:dictionary toTable:self.tableName replace:(mode == SyncOperationModeSynchronization)];
                        if (!dbResult && mode == SyncOperationModeInitialization && dm.currentDatabase.lastErrorCode == SQLITE_CONSTRAINT){
                            dbResult = [dm insertRecord:dictionary toTable:self.tableName replace:YES];
                        }
                    }

                    if (dbResult){
                        recordsInserted = TRUE;
                    }
                    recordsInsertedCount++;
                    if (recordsInsertedCount > 100000 && itemsCount > 120000){
                        if ([[dm currentDatabase] inTransaction]){
                            [[dm currentDatabase]commit];
                        }
                        [[dm currentDatabase]beginTransaction];
                        recordsInsertedCount = 0;
                        NSThread.threadPriority = 1.0;
                    }
                    self.progress += 1.0/itemsCount;
                }
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    [row release];
                });
            }
        }
        @catch (NSException* exception) {
            NSLog(@"%@", exception);
            [operationResult release];
            operationResult  = nil;
            result = FALSE;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                NSDictionary* dict = @{NSLocalizedDescriptionKey: exception.description};
                NSError* error = [NSError errorWithDomain:@"com.cloudworks.syncOperation.parser" code:100 userInfo:dict];
                if ([self shouldCancelOnError:error]){
                    [self cancel];
                }
                [self postError:exception.description error:error];
            });
        }
    @finally{
        DataManager *dm = [DataManager instance];
        if ([[dm currentDatabase] inTransaction]){
            if (result){
                [[dm currentDatabase]commit];
            }
            else{
                recordsInserted = NO;
                [[dm currentDatabase]rollback];
                self.progress -= (float)recordsInsertedCount/itemsCount;
            }
            [[dm currentDatabase]execute:@"PRAGMA foreign_keys = 1; PRAGMA ignore_check_constraints = 0; PRAGMA count_changes=ON;"];
        }
    }
    }//autorelease

}

-(void)syncManager:(SyncManager *)syncManager didReceiveBinaryStream:(NSInputStream *)stream{
    
    [GAELog addStackTraceToPublicLog:@"SyncTableOperation didReceiveBinaryStream" calcDelta:YES];
    
    if (!stream || inputStream) return;
    inputStream  = [stream retain];
    dispatch_async(streamReadQueue, ^{
        @autoreleasepool{
            [inputStream open];
            RTProtoDataReader* reader = nil;
        @try{
            reader = [[RTProtoDataReader alloc]initWithStream:inputStream];
            NSArray* columnNames = reader.columnNames;
#ifdef USE_THREAD
                    if (!networkThread){
                        [self startThreadForRequest:columnNames];
                    }
#else
                    if (!dbRecordsQueue){
                        dbRecordsQueue = dispatch_queue_create("com.cloudwk.dbRecordsQueue", NULL);
                        dispatch_queue_t high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,NULL);
                        dispatch_set_target_queue(dbRecordsQueue,high);
                        dispatch_async(dbRecordsQueue, ^{
                            [self insertRecordsFromBuffer:columnNames];
                        });
                    }
#endif
            while([reader read]){
                @autoreleasepool{
                    NSArray* currentRow = reader.currentRow;
                    [bufferFifo push:currentRow];
                    if (self.isCancelled || (requestCompleted && !result)){
                        return;
                    }
                }//autoreleasepool
            }//while
            [bufferFifo pushFinish];
        }//try
        @catch (NSException* exception) {
            NSLog(@"%@", exception);
            [operationResult release];
            operationResult  = nil;
            result = FALSE;
            if (!self.isCancelled){
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    NSDictionary* dict = @{NSLocalizedDescriptionKey: exception.description};
                    NSError* error = [NSError errorWithDomain:@"com.cloudworks.syncOperation.parser" code:100 userInfo:dict];
                    if ([self shouldCancelOnError:error]){
                        [self cancel];
                    }
                    [self postError:exception.description error:error];
                });
            }
        }
        @finally{
            [reader release];
            [inputStream close];
            [inputStream release];
            inputStream = nil;
            [bufferFifo pushFinish];
            #ifndef USE_THREAD
            if (dbRecordsQueue){
                //join queue
                dispatch_sync(dbRecordsQueue, ^{});
            }
            #endif
        }
        }//autorelease
    });
}

//#ifndef USE_THREAD

-(void)syncManager:(SyncManager*)syncManager didReceiveRecord:(id)object{
    
    [GAELog addStackTraceToPublicLog:@"SyncTableOperation didReceiveRecord" calcDelta:YES];
    
    if (self.isCancelled || ![object isKindOfClass:[XmlElement class]]) return;
#ifdef USE_THREAD
    if (!networkThread){
        [self startThreadForRequest:nil];
    }
#else
        if (!dbRecordsQueue){
            dbRecordsQueue = dispatch_queue_create("com.cloudwk.dbRecordsQueue", NULL);
            dispatch_queue_t high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,NULL);
            dispatch_set_target_queue(dbRecordsQueue,high);
            dispatch_async(dbRecordsQueue, ^{
                [self insertRecordsFromBuffer:nil];
            });
        }
#endif
        if (self.isCancelled)
            return;
        [bufferFifo push:(XmlElement*)object invokeSelector:@selector(childrenToDictionary)];
}

-(NSString*)sourceTableName{
    return self.tableName;
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return _forceCancelOnError || self.whereCondition || !([error.domain isEqualToString:@"com.cloudworks.syncOperation.parser"] || ([error.domain isEqualToString:@"WebServiceResponseHTTP"] && error.code == 404) || ([error.domain isEqualToString:NSURLErrorDomain] && (error.code == NSURLErrorNetworkConnectionLost || error.code == NSURLErrorTimedOut || error.code == NSURLErrorNotConnectedToInternet)));
}

-(BOOL)shouldPostError:(NSError *)error{
    return [self shouldCancelOnError:error];
}

- (void) sync{
    if (mode == SyncOperationModeSynchronization && DataManager.instance.isTempDb) return;
    
    [GAELog addStackTraceToPublicLog:[NSString stringWithFormat:@"SyncTableOperation start sync: %@", self.tableName]];
    
    if (self.isCancelled || (!_whereCondition.length && [SyncOperationQueue partialSynchronization] && !isMandatory)) return; // if we have whereCondiiton defined - ignore partialSynchronization
    NSThread.threadPriority = 1.0;
    //recordsInCache = 0;
    itemsCount = 0;
    [operationResult release];
    operationResult = nil;
    __block NSData* maxTimestamp = nil;
    __block NSDate* maxRecModifiedDate = nil;
    //for init mode it doesn't matter what version is
    __block BOOL useTimeStamp = FALSE;
    //initially set cancel behaviour to break on all errors
    _forceCancelOnError = YES;
    void(^updatemaxRecModifiedDate)(void) = ^{
        if (_whereCondition.length || self.ignoreMaxModifiedRecord) return;
        useTimeStamp = [[self class].serverVersion intValue]>=39;
        if (useTimeStamp){
            maxTimestamp = [[DataManager instance]getMaxTs:self.tableName];
        }
        
        if (!maxTimestamp){
            maxRecModifiedDate = [[DataManager instance]getMaxRecModifiedDate:self.tableName];
            if ([maxRecModifiedDate timeIntervalSince1970] == 0)
                maxRecModifiedDate = nil;
        }
    };
    
    if (mode == SyncOperationModeSynchronization) {
        if (![self class].serverVersion){
            [GAELog addStackTraceToPublicLog:@"SyncTableOperation start getServerVersion" startCalcDelta:YES];
            [syncManager getVersion];
            [self waitForRequest];
            if (self.isCancelled) return;
            if (!operationResult){
                return;
            }
            if([operationResult isKindOfClass:[NSNumber class]]){
                [self class].serverVersion = (NSNumber*)operationResult;
            }
        }
        updatemaxRecModifiedDate();
    }
    @try{
        if (mode == SyncOperationModeInitialization) {
            NSString* oldTitle = [self.title retain];
            self.title = [NSString stringWithFormat:NSLocalizedString(@"OPERATION_TRANCATING_FORMAT_TEXT", nil), self.tableName];
            [SyncTableHandler beforeTruncateTable:self];
            [[DataManager instance] clearTable:self.tableName];
            self.title = oldTitle;
            [oldTitle release];
        }

        [operationResult release];
        operationResult = nil;
        
        if (!_isNotRequiresTableCount) {
            [GAELog addStackTraceToPublicLog:@"SyncTableOperation start getTableCount" startCalcDelta:YES];
            [syncManager getTableCount:self.sourceTableName modifiedDate:maxRecModifiedDate whereCondition:_whereCondition conditionParams:_conditionParams mode:mode timestamp:nil];
            [self waitForRequest];
            if (self.isCancelled || !result) return;
        }
        else // in case we don't want to retrieve table count - we will just skip this step
            operationResult = [@(1) retain];
        
         _forceCancelOnError = NO;
        
        if (operationResult && [operationResult isKindOfClass:[NSNumber class]]){
            itemsCount = [((NSNumber*)operationResult) intValue];
            
            [GAELog addStackTraceToPublicLog:[NSString stringWithFormat:@"SyncTableOperation getTableCount result: %lu", (unsigned long)itemsCount]];

            if (!itemsCount) return;
            
            [SyncTableHandler beforeInsertIntoTable:self totalRows:itemsCount];
            NSInteger failedAttemptsCount = 0;
            while (YES){
                self.lastError = nil;
                self.lastErrorObject = nil;
                recordsInserted = NO;
                _atLeastOneRowReceived = NO;
                requestCompleted = NO;
                [bufferFifo release];
                bufferFifo = [[RTBufferFifo alloc] initWithMaxSize:RECORDS_IN_CACHE_BINARY];
                [GAELog addStackTraceToPublicLog:@"SyncTableOperation start getTableAsBinary" startCalcDelta:YES];
                [syncManager getTableAsBinary:self.sourceTableName ids:nil modifiedDate:maxRecModifiedDate whereCondition:_whereCondition conditionParams:_conditionParams mode:mode timestamp:maxTimestamp retryCount:failedAttemptsCount];
                [self waitForRequest];
                //if error is occured shudDown threads here
                if (!result){
                    [self shutDownReadWriteThreads];
                }
                //join queue
                dispatch_sync(streamReadQueue, ^{});
        #ifdef USE_THREAD
                [self joinAndDestroyThreadForRequest];
        #endif
                if (recordsInserted){
                    [self postNotification:SyncQueueDidUpdateRecordsOperationNotification];
                }
                
                if (self.whereCondition || self.ignoreMaxModifiedRecord || self.isCancelled || (result && !_atLeastOneRowReceived)){
                    break;
                }
                if (!result){
                    if (!_atLeastOneRowReceived){
                        failedAttemptsCount++;
                    }
                    else if (![self.lastErrorObject.domain isEqualToString:@"com.cloudworks.syncOperation.parser"]){
                        failedAttemptsCount = 0;
                    }
                    if (failedAttemptsCount > 9){
                        NSError* error = self.lastErrorObject;
                        if (!error){
                            NSDictionary* userInfo = @{NSLocalizedDescriptionKey : @"There were 10 failed attempts to get the table."};
                            error = [NSError errorWithDomain:@"com.syncTable" code:0 userInfo:userInfo];
                        }
                        _forceCancelOnError = YES;
                        [self cancel];
                        [self postError:error.localizedDescription error:error];
                        break;
                    }
                    else{
                        NSError* error = self.lastErrorObject;
                        if (([error.domain isEqualToString:@"WebServiceResponseHTTP"] && error.code == 404) || ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorNotConnectedToInternet)){
                            dispatch_semaphore_wait(waitForRequestSemaphore, dispatch_time(DISPATCH_TIME_NOW, 2LL * NSEC_PER_SEC));
                        }
                        self.lastError = nil;
                        self.lastErrorObject = nil;
                    }
                }
                else{
                    updatemaxRecModifiedDate();
                    if (recordsInserted && !maxRecModifiedDate && !maxTimestamp){
                        break;
                    }
                }
            }
        }
    }
    @finally{
        [SyncTableHandler afterInsertIntoTable:self totalRows:itemsCount];
        if (self.completedSyncBlock){
            self.completedSyncBlock(self, itemsCount);
        }
    }
}

#ifdef USE_THREAD

-(void)runRequests:(id)object{
    dispatch_sync(networkThreadAccessQueue, ^{
        @try {
            @autoreleasepool {
                [self insertRecordsFromBuffer:object];
            }
        }
        @catch (NSException *exception) {
            @throw exception;
        }
        @finally {
            [bufferFifo close];
        }
    });
}

- (void)startThreadForRequest:(id)object{
    if (!networkThread) {
        dispatch_sync(networkThreadAccessQueue, ^{
            if (!networkThread) {
                if (self.isCancelled){
                    return;
                }
                networkThread = [[NSThread alloc] initWithTarget:self selector:@selector(runRequests:) object:object];
                networkThread.threadPriority = 1.0;
                networkThread.qualityOfService = NSQualityOfServiceUserInteractive;
                networkThread.name = @"threadForRequest";
                [networkThread start];
            }
        });
    }
}

-(void)joinAndDestroyThreadForRequest{
    if (networkThread){
        dispatch_sync(networkThreadAccessQueue, ^{
            if (networkThread){
                [networkThread release];
                networkThread = nil;
            }
        });
    }
}
#endif


-(void)setCompletedSyncBlock:(void (^)(SyncTableOperation* operation, NSInteger affectedRowsCount))aCompletedSyncBlock{
    [_completedSyncBlock release];
    _completedSyncBlock = [aCompletedSyncBlock copy];
}

-(void)signalOperationDidComplete{
    [super signalOperationDidComplete];
    requestCompleted = YES;
}
    
@end
