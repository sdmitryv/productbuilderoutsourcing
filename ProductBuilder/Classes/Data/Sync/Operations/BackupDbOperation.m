//
//  BackupDbOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "BackupDbOperation.h"
#import "GoogleCloudStorageManager.h"
#import "DataManager.h"
#import "ASIDataCompressor.h"
#import "SettingManager.h"

@interface BackupDbOperation() {
    GoogleCloudStorageManager * _googleCloudStorageManager;
}

@end

@implementation BackupDbOperation

- (void)dealloc {
    [_googleCloudStorageManager release];

    [super dealloc];
}

- (void) sync{
    if (self.isCancelled) return;
    result = FALSE;
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = documentPaths[0];
    NSString* databaseZipPath = [documentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", APPLICATION_DATABASE_NAME, @".zip"]];
    NSError* error = nil;
    self.title = NSLocalizedString(@"PREPARING_BACK_UP_TITLE", nil);
    _type=PreparingBackUp;
    [DataManager.instance.currentDatabase wal_checkpoint];
    [ASIDataCompressor compressDataFromFile:[DataManager instance].currentDatabasePath
                                     toFile:databaseZipPath error:&error delegate:self];
    if (error && !self.isCancelled){
        [self postError:[error localizedDescription] error:error];
    }
    if (self.isCancelled) return;
    self.title = NSLocalizedString(@"SENDING_BACK_UP_TITLE", nil);
    _type=SendingBackUp;
    self.progress = 0.0f;
    if ([SettingManager instance].uploadBackupToGAE) {
        _googleCloudStorageManager = [[GoogleCloudStorageManager alloc] initWithDelegate:self];
        [_googleCloudStorageManager uploadFile:databaseZipPath];
    }
    else {
        syncManager.defaultTimeout = 1200;
        [syncManager uploadBackup:databaseZipPath];
        [self waitForRequest];
        self.progress = 1.0;
    }
}

-(void)syncManager:(SyncManager*)syncManager completedUploadWithProgress:(float)aProgress{
    self.progress = aProgress;
}

- (void)cancel {
    [_googleCloudStorageManager cancelUpload];
    [super cancel];
}

#pragma mark ASIDataCompressorDelegate

- (void)dataCompressor:(ASIDataCompressor *)request didCompressChunk:(float)aProgress{
    self.progress = aProgress;
}

- (BOOL)dataCompressorShouldCancel:(ASIDataCompressor *)compressor{
    return self.isCancelled;
}

#pragma mark - GoogleCloudStorageManagerDelegate Methods

-(void)manager:(GoogleCloudStorageManager *)manager uploadProgress:(float)aProgress {
    self.progress = aProgress;
}

-(void)manager:(GoogleCloudStorageManager *)manager completedUploadWithError:(NSError *)error {
    if (error != nil) {
        [self postError:error.localizedDescription error:error];
    }
}

@end
