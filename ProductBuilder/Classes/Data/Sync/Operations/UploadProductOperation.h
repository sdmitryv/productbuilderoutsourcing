//
//  UploadProductOperation.h
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"
#import "Product.h"


typedef NS_ENUM(NSInteger, UploadProductStatus) {
    UploadProductStatusNone          = 0,
    UploadProductStatusOk            = 1,
    UploadProductStatusInProgress    = 2,
};

@interface UploadProductOperation : SyncOperation <NSURLSessionDelegate> {
}

@property (assign, nonatomic) UploadProductStatus uploadStatus;
@property (readonly, nonatomic) NSString *inProcessApiDocumentId;
@property (retain, nonatomic) Product *product;
@property (retain, nonatomic) NSString *checkApiDocumentId;

@property (retain, nonatomic) NSString *deptSetting;
@property (retain, nonatomic) NSString *vendorSetting;


@end
