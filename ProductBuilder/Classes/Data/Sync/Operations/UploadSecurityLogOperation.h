//
//  UploadSecurityLogOperation.h
//  ProductBuilder
//
//  Created by Vitaliy Gervazuk on 9/13/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "UploadTableOperation.h"

@interface UploadSecurityLogOperation : UploadTableOperation

@end
