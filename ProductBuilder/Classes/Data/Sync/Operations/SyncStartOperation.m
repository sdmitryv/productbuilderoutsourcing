//
//  SyncStartOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/21/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "SyncStartOperation.h"
#import "DeviceStats.h"

@implementation SyncStartOperation

-(id)init{
    if ((self =[super init])){
        self.title = NSLocalizedString(@"OPERATION_PREPARING_TEXT", nil);
    }
    return self;
}

-(void)start{
    
    [super start];
    
    if (mode != SyncOperationModeInitialization) {
        
        if ([SyncOperationQueue partialSynchronization])
            [DeviceStats statsLastForceSyncStart];
        else
            [DeviceStats statsLastSyncStart];
    }
    else
        [DeviceStats statsLastInit];
}

@end
