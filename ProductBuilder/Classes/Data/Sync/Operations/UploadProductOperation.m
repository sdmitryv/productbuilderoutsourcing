//
//  UploadProductOperation.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "UploadProductOperation.h"
#import "GAELog.h"
#import "NSURLSession+Extentions.h"
#import "SettingManager.h"
#import "AppSettingManager.h"
#import "NSCurrencyFormatter+Currency.h"
#import "AppSettingManager.h"
#import "USAdditions.h"
#import "AppParameterManager.h"
#import "NSDate+ISO8601Unparsing.h"

@interface UploadProductOperation()

@property (atomic, retain) NSURLSessionDataTask* task;

@end

@implementation UploadProductOperation

- (void)dealloc {
    
    
    if (_task) {
        [_task cancel];
        [_task release];
        _task = nil;
    }
    
    [_product release];
    [_inProcessApiDocumentId release];
    [_checkApiDocumentId release];
    [_deptSetting release];
    [_vendorSetting release];
    
    [super dealloc];
}

-(id)init{
    if ((self =[super init])){
        self.name = NSLocalizedString(@"UPLOADING_GAE_LOGS_TEXT", nil);
        self.title = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"UPLOADING_GAE_LOGS_TEXT", nil)];
    }
    return self;
}


- (NSData *)createBodyWithParameters:(NSDictionary *)parameters {

    NSMutableString *httpBody = [NSMutableString string];
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        
        if (httpBody.length != 0) {
            
            [httpBody appendString:@"&"];
        }
        [httpBody appendString:parameterKey];
        [httpBody appendString:@"="];
        [httpBody appendString:parameterValue];
    }];
    
    return [httpBody dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)percentEscapeString:(NSString *)string
{
    NSString *res = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                 (CFStringRef)string,
                                                                                 (CFStringRef)@" ",
                                                                                 (CFStringRef)@":/?@!$&'()*+,;=",
                                                                                 kCFStringEncodingUTF8));
    return [res stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

- (void) sync {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@",
                                                 [AppSettingManager instance].serverUrl,
                                                 self.product != nil ? @"api.ashx" : [NSString stringWithFormat:@"ApiStatus.ashx?id=%@", self.checkApiDocumentId]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:30];
    [request setValue:@"application/x-www-form-urlencoded"  forHTTPHeaderField:@"Content-Type"];
    
    if (self.product != nil) {

        [request setHTTPMethod:@"POST"];
        
        NSData* xmlData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ProductBuilderInventoryPattern" ofType:@"xml"]];
        NSString* xmlStr = [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!Manufacturer!-!"     withString:[(_product.manufacturerName ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!PrimaryVendor!-!"    withString:[(_product.primaryVendor ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!OrderCost!-!"        withString:[_product.cost.stringValue stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!BaseProce!-!"        withString:[_product.price.stringValue stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!Department!-!"       withString:[(_product.department ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!Class!-!"            withString:[(_product.procuctClass ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!TaxCategoryCode!-!"  withString:[(_product.taxCategoryCode ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!Description4!-!"     withString:[(_product.storeDescription ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!CustomLongText10!-!" withString:[(_product.longDescription ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!CLU!-!"              withString:[(_product.clu ?: @"") stringByEscapingXML]];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!DeptSetting!-!"      withString:(_deptSetting ?: @"Name")];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!VendorSetting!-!"    withString:(_vendorSetting ?: @"Name")];
        xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"!-!CustomDate6!-!"      withString:[[NSDate date] ISO8601DateStringWithTime:NO]];
        
        
        
        
        NSString *serializedForm = [self percentEscapeString:xmlStr];
        
        NSDictionary *params = @{@"apiTypeOption"   : @"on",
                                 @"apiKey"          : AppSettingManager.instance.serverApiKey ?: @"",
                                 @"Source"          : @"test",
                                 @"data"            : serializedForm,
                                 @"ApiRequestType"  : @"inventory-import",
                                 @"Async"           : @"False"};
        
        NSData *postData = [self createBodyWithParameters:params];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
    }
    else if (self.checkApiDocumentId != nil){
        
        [request setHTTPMethod:@"GET"];
    }
    
    
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 30;
    NSOperationQueue* delegateQueue = [[[NSOperationQueue alloc] init]autorelease];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:delegateQueue];
    
    dispatch_semaphore_t waitSemaphore = dispatch_semaphore_create(0);
    self.task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable anError) {
        
        if (data){
            
            NSLog(@"%@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
        }
        
        self.uploadStatus = UploadProductStatusNone;
        @try {
           
            if (anError.code==NSURLErrorCancelled){
                [session invalidateAndCancel];
                return;
            }
            
            
            [session finishTasksAndInvalidate];
            if (anError) {
                
                [self postError:[anError localizedDescription] error:anError];
                return;
            }
            
            
            NSError *error = nil;
            NSHTTPURLResponse *httpResponse = httpResponse = [response isKindOfClass:[NSHTTPURLResponse class]] ? (NSHTTPURLResponse *) response : nil;
            if ([httpResponse statusCode] >= 400 && [httpResponse statusCode] < 500) {
                
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: [NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]};
                error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:[httpResponse statusCode] userInfo:userInfo];
                [self postError:[error localizedDescription] error:error];
                return;
            }
            
            
            xmlDocPtr doc = xmlParseMemory([data bytes], (int)[data length]);
            xmlNodePtr node = doc->children;
            if (!xmlStrEqual(node->name,(const xmlChar*)"ApiDocument")){
                
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey:[NSString stringWithUTF8String:[data bytes]]};
                error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:200 userInfo:userInfo];
                [self postError:[error localizedDescription] error:error];
                return;
            }
            
            
            XmlElement* apiDocument = [[[XmlElement alloc]initWithXmlNodePtr:node]autorelease];
            for (XmlValue *attr in apiDocument.attributes) {
                
                if ([attr.name isEqualToString:@"Status"]) {
                    
                    if ([attr.value isEqualToString:@"Successful"]) {
                        
                        self.uploadStatus = UploadProductStatusOk;
                        return;
                    }
                    
                    if ([attr.value isEqualToString:@"InProcess"]) {
                        
                        self.uploadStatus = UploadProductStatusInProgress;
                        if (_inProcessApiDocumentId != nil) {
                            return;
                        }
                    }
                    
                    if ([attr.value isEqualToString:@"Error"]) {
                        
                        NSArray* ApiDocumentLines = [apiDocument childrenWithNameByPath:@"ApiDocumentLines\\ApiDocumentLine"];
                        for (XmlElement* apiDocumentLine in ApiDocumentLines){
                            
                            for (XmlValue *lineAttr in apiDocumentLine.attributes) {
                                
                                if ([lineAttr.name isEqualToString:@"Error"]) {
                                    
                                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:lineAttr.value};
                                    error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:200 userInfo:userInfo];
                                    [self postError:[error localizedDescription] error:error];
                                    return;
                                }
                            }
                        }
                    }
                }
                
                
                if ([attr.name isEqualToString:@"Error"]) {
                    
                    NSDictionary *userInfo = @{NSLocalizedDescriptionKey:attr.value};
                    error = [NSError errorWithDomain:@"WebServiceResponseHTTP" code:200 userInfo:userInfo];
                    [self postError:[error localizedDescription] error:error];
                    return;
                }
                
                if ([attr.name isEqualToString:@"ApiDocumentId"]) {
                    
                    _inProcessApiDocumentId = [attr.value retain];
                    if (self.uploadStatus == UploadProductStatusInProgress) {
                        
                        return;
                    }
                }
            }
            
            xmlFreeDoc(doc);
        }  @finally {
           
            
            dispatch_semaphore_signal(waitForRequestSemaphore);
            dispatch_semaphore_signal(waitSemaphore);
        }
    }];
    
    [self.task resume];
    
    dispatch_semaphore_wait(waitSemaphore, DISPATCH_TIME_FOREVER);
    dispatch_release(waitSemaphore);
    
    [request release];
    [pool release];
}

- (void)cancel {
    [super cancel];
    if (self.task) {
        [self.task cancel];
        self.task = nil;
    }
}

@end
