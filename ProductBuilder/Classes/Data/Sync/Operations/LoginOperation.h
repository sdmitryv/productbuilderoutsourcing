//
//  LoginOperation.h
//  DeviceAgent
//
//  Created by Alexander Martyshko on 12/25/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "SyncOperation.h"

@class Employee;

@interface LoginOperation : SyncOperation {
    NSString *_login;
    NSString *_password;
    Employee *_employee;
}

@property (nonatomic, readonly) NSNumber *successfulNum;
@property (nonatomic, readonly) Employee *employee;

- (id)initWithLogin:(NSString *)login password:(NSString *)password;

@end
