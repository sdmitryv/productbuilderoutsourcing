//
//  SyncEndSyncOperation.m
//  ProductBuilder
//
//  Created by Valery Fomenko on 3/2/12.
//  Copyright (c) 2012 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncEndSyncOperation.h"
#import "SyncAction.h"
#import "Settings.h"
#import "Location.h"
#import "Currency.h"
#import "SyncTableOperation.h"
#import "AppSettingManager.h"
#import "DeviceStats.h"

@implementation SyncEndSyncOperation

-(id)init {
    
    if ((self =[super init])){
        
        self.name = self.title = NSLocalizedString(@"OPERATION_FINISHING_TEXT", nil);
    }
    
    return self;
}

-(void)sync{
    
    if (self.isCancelled) return;
    
    if (mode == SyncOperationModeInitialization){

        SyncAction *initAction = [SyncAction getSyncAction:@"Init.End" target:@"System"];
        if (initAction != nil) {
            
            initAction.recModified = [NSDate date];
            [initAction markDirty];
        }
        else{
            
            initAction = [[[SyncAction alloc] init] autorelease];
            initAction.action = @"Init.End";
            initAction.target = @"System";
        }
        
        [initAction save];
        
        AppSettingManager.instance.fillFromLocationId = nil;
    }
    else {
        
        /*BOOL hasErrors = [MainViewController sharedInstance].hasSyncErrors;
        
        if (!hasErrors) {
            
            [DeviceStats statsSyncErrorTable:nil];
            [DeviceStats statsSyncErrorMessage:nil];
        }
        
        if (![SyncOperationQueue partialSynchronization] && !hasErrors)
            [DeviceStats statsLastSyncEnd];
         */
    }
    
    SyncAction *syncAction = [SyncAction getSyncAction:@"Synchronization" target:@"System"];
    if (syncAction != nil) {
        
        syncAction.recModified = [NSDate date];
        [syncAction markDirty];
    }
    else {
       
        syncAction = [[[SyncAction alloc] init] autorelease];
        syncAction.action = @"Synchronization";
        syncAction.target = @"System";
    }
    
    [syncAction save];
    SyncTableOperation.serverVersion = nil;
}

@end
