//
//  SyncOperation.m
//  CloudworksPOS
//
//  Created by valera on 7/18/11.
//  Copyright 2011 Retail Teamwork Ukraine. All rights reserved.
//

#import "SyncOperation.h"
#import "Location.h"
#import "GAELog.h"
#import "DataUtils.h"

NSString* const LocationMismatchCode = @"LOCATION_MISMATCH";
NSString* const LocationMismatchCodeSuffix = @":LOCATION_MISMATCH";
NSString* const NoOnlineDrawerMemoCode = @"NO_ONLINE_DRAWER_MEMO";
NSString* const DMWasChangedCode = @"DRAWER_MEMO_WAS_CHANGED";
NSString* const SOWasChangedCode = @"SALES_ORDER_WAS_CHANGED";
NSString* const SMWasChangedCode = @"SHIP_MEMO_CHANGED";
NSString* const ReceiptNotFoundCode = @"RECEIPT_NOT_FOUND";
NSString* const PasswordMustBeDifferentCode = @"PASSWORD_MUST_BE_DIFFERENT";
NSString* const PasswordExpiredCode = @"PASSWORD_EXPIRED";
NSString* const UserExpiredCode = @"USER_EXPIRED";
NSString* const UserInvalidCode = @"INVALID_USER";
NSString* const UserNotAllowedCode = @"INIT_NOT_ALLOWED";

@interface SyncOperation (){
    float lastProgressNotified;
    dispatch_queue_t postQueue;
}
//@property (atomic, retain)NSError* lastErrorObject;
//@property (atomic, retain)NSString* lastError;
@property (atomic, retain) NSObject* operationResult;

-(void)finish;
@end

@implementation SyncOperation

@synthesize
progress,
mode,
name,
parentQueue,
operationResult,
workingTime,
title,
lastErrorObject,
lastError;

-(id)init{
    if ((self=[super init])){
        syncManager = [[SyncManager alloc]init];
        syncManager.syncDelegate = self;
        progress = 0;
    }
    return self;
}

-(void)start{
    startTime = [NSDate timeIntervalSinceReferenceDate];
    workingTime = 0;
    result = FALSE;
    //userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
    if (![self isCancelled]){
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        [self willChangeValueForKey:@"isExecuting"];
        isExecuting = YES;
        waitForRequestSemaphore = dispatch_semaphore_create(0);
        [self didChangeValueForKey:@"isExecuting"];
        [self postNotification:SyncQueueDidStartOperationNotification];
        result = TRUE;
        [self sync];
        [pool release];
    }
    [self signalOperationDidComplete];
    self.progress = 1;
    workingTime = [NSDate timeIntervalSinceReferenceDate] - startTime;
    [self finish];
    //[userInfo release];
    //userInfo = nil;
}

-(void)sync{
    
}

-(BOOL)isFinished{
    return isFinished;
}

-(BOOL)isExecuting{
    return isExecuting;
}

-(NSTimeInterval)startTime{
    return startTime;
}
-(NSTimeInterval)workingTime{
    return isFinished ? workingTime : startTime ? [NSDate timeIntervalSinceReferenceDate] - startTime : 0;
}

-(BOOL)isConcurrent{
    return FALSE;
}

- (void)finish
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    isExecuting = NO;
    isFinished = YES;

    [self postNotification:SyncQueueDidEndOperationNotification];
    if (postQueue)
        dispatch_sync(postQueue, ^{});
    parentQueue = nil;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

-(WebServiceOperation*)currentOperation{
    return syncManager.currentOperation;
}

- (void)dealloc {
    if (postQueue){
        dispatch_sync(postQueue, ^{});
        dispatch_release(postQueue);
    }
	[name release];
    [title release];
    syncManager.syncDelegate = nil;
    [syncManager release];
    [operationResult release];
    //[userInfo release];
    [lastError release];
    [lastErrorObject release];
    if (waitForRequestSemaphore)
        dispatch_release(waitForRequestSemaphore);
    [super dealloc];
}

-(void)setTitle:(NSString *)aTitle{
    @synchronized(self){
        if ([title isEqualToString:aTitle]) return;
        [title release];
        title = [aTitle retain];
    }
    [self postNotification:SyncQueueDidChangeOperationTitleNotification];
}

-(NSString*)title{
    NSString* aTitle = nil;
    @synchronized(self){
        aTitle = [[title retain]autorelease];
    }
    return aTitle;
}

-(void)setProgress:(float)value{
    if (progress!=value){
        progress = value;
        if (progress==0 || progress==1 || fabsf(progress - lastProgressNotified) > 0.000488)
            lastProgressNotified = progress;
            [self postNotification:SyncQueueInProgressNotification];
    }
}

-(void)postNotification:(NSString*)aName{
    if (!postQueue){
        postQueue = dispatch_queue_create(NULL, NULL);
    }
    dispatch_async(postQueue, ^{
        NSDictionary* userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
        [parentQueue postNotification:aName userInfo:userInfo sender:self];
        [userInfo release];
    });
}

-(void)cancel{
    [super cancel];
    result = FALSE;
    [syncManager.currentOperation cancel];
    syncManager.syncDelegate = nil;
    [self signalOperationDidComplete];
}

-(BOOL)shouldCancelOnError:(NSError*)error{
    return TRUE;
}

-(BOOL)shouldPostError:(NSError*)error{
    return TRUE;
}

-(void)signalOperationDidComplete{
    if (waitForRequestSemaphore)
        dispatch_semaphore_signal(waitForRequestSemaphore);
}

+(id)syncOperation{
    return [[[[self class]alloc]init]autorelease];
}
+(id)syncOperationWithMode:(SyncOperationMode)aMode{
    SyncOperation* obj = [[[[self class]alloc]init]autorelease];
    obj->mode = aMode;
    return obj;
}

-(void)postError:(NSString *)errorText error:(NSError *)error{
    self.lastError = errorText;
    self.lastErrorObject = error;
    if (![self shouldPostError:error] || !parentQueue) return;
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey, parentQueue, SyncQueueOperationObject, errorText ?: NSLocalizedString(@"UNKNOWNERROR_TEXT", nil), SyncQueueErrorDescriptionKey, nil];
    if (error)
        dict[SyncQueueErrorKey] = error;
    [parentQueue postNotification:SyncQueueDidErrorOccurNotification userInfo:dict sender:self];
    [dict release];
}

#pragma mark SyncManager delegate methods

-(void)syncManager:(SyncManager*)aSyncManager didCompleteOperation:(NSString *)name withResult:(NSObject *)res {
    
    [GAELog addStackTraceToPublicLog:@"SyncOperation didCompleteOperation" calcDelta:YES];
    [operationResult release];
    operationResult = [res retain];
    result = TRUE;
    [self signalOperationDidComplete];
}

-(void)syncManager:(SyncManager*)aSyncManager didOccurError:(NSString *)text error:(NSError *)error{
    
    [GAELog addStackTraceToPublicLog:[NSString stringWithFormat:@"SyncOperation didOccurError: %@", error.description] calcDelta:YES];
    
    [operationResult release];
    operationResult  = nil;
    result = FALSE;
    
    //[self postError:text error:error];
    BOOL errorPosted = FALSE;
    //check for location mistmach error
    for(id bodyPart in aSyncManager.currentOperation.response.bodyParts) {
        if ([bodyPart isKindOfClass:[SOAPFault class]]) {
            @autoreleasepool {
                SOAPFault* soapFault = (SOAPFault *)bodyPart;
                if ([soapFault.faultcode isEqualToString:LocationMismatchCode] 
                    || [soapFault.faultcode hasSuffix:LocationMismatchCodeSuffix]
                    || [soapFault.faultsubcode isEqualToString:LocationMismatchCode]){
                    XmlElement* detailElement = soapFault.detailElement;
                    if (detailElement){
                        XmlElement* locationDetails = [detailElement childWithName:@"currentLocation"];
                        if (locationDetails){
                            LocationBase* location = [[LocationBase alloc]init];
                            location.id = [BPUUID UUIDWithString:[locationDetails childWithName:@"id"].value];
                            location.name = [locationDetails childWithName:@"name"].value;
                            location.locationCode = [locationDetails childWithName:@"code"].value;
                            //post message
                            NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                            userInfo[SyncQueueOpsLocationKey] = location;
                            [location release];
                            userInfo[SyncQueueCurrentLocationKey] = [Location localLocation];
                            userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                             NSError* locationMismatchError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:LocationMismatchErrorCode userInfo:userInfo];
                            [self postError:[locationMismatchError localizedDescription] error:locationMismatchError];
                            errorPosted = TRUE;
                            [parentQueue postNotification:SyncQueueSyncLocationMistmatch userInfo:locationDetails sender:self];
                            [userInfo release];
                            [parentQueue cancelAllOperationsAsync];
                        }
                    }
                }
                else if ([soapFault.faultcode isEqualToString:SOWasChangedCode]
                         || [soapFault.faultcode hasSuffix:SOWasChangedCode]){
                    //post message
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* soWasChangedError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:SOWasChangedErrorCode userInfo:userInfo];
                    [self postError:[soWasChangedError localizedDescription] error:soWasChangedError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:SMWasChangedCode]
                         || [soapFault.faultcode hasSuffix:SMWasChangedCode]){
                    //post message
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* smWasChangedError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:SMWasChangedErrorCode userInfo:userInfo];
                    [self postError:[smWasChangedError localizedDescription] error:smWasChangedError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:ReceiptNotFoundCode]
                         || [soapFault.faultcode hasSuffix:ReceiptNotFoundCode]){
                    //post message
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* soWasChangedError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:ReceiptNotFoundErrorCode userInfo:userInfo];
                    [self postError:[soWasChangedError localizedDescription] error:soWasChangedError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:NoOnlineDrawerMemoCode] || [soapFault.faultsubcode isEqualToString:NoOnlineDrawerMemoCode]){
                     NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* noOnlineDrawerMemoError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:NoOnlineDrawerMemoErrorCode userInfo:userInfo];
                    [self postError:[noOnlineDrawerMemoError localizedDescription] error:noOnlineDrawerMemoError];
                    errorPosted = TRUE;
                    [userInfo release];
                }
                else if ([soapFault.faultcode isEqualToString:PasswordMustBeDifferentCode]
                         || [soapFault.faultcode hasSuffix:PasswordMustBeDifferentCode]){
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* passwordMustBeDifferentError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:PasswordMustBeDifferentErrorCode userInfo:userInfo];
                    [self postError:[passwordMustBeDifferentError localizedDescription] error:passwordMustBeDifferentError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:PasswordExpiredCode]
                         || [soapFault.faultcode hasSuffix:PasswordExpiredCode]) {
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* passwordExpiredError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:PasswordExpiredErrorCode userInfo:userInfo];
                    [self postError:[passwordExpiredError localizedDescription] error:passwordExpiredError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:UserExpiredCode]
                         || [soapFault.faultcode hasSuffix:UserExpiredCode]) {
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* passwordExpiredError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:UserExpiredErrorCode userInfo:userInfo];
                    [self postError:[passwordExpiredError localizedDescription] error:passwordExpiredError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:UserInvalidCode]
                         || [soapFault.faultcode hasSuffix:UserInvalidCode]) {
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* passwordExpiredError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:UserInvalidErrorCode userInfo:userInfo];
                    [self postError:[passwordExpiredError localizedDescription] error:passwordExpiredError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
                else if ([soapFault.faultcode isEqualToString:UserNotAllowedCode]
                         || [soapFault.faultcode hasSuffix:UserNotAllowedCode]) {
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self, SyncQueueCurrentOperationKey,parentQueue, SyncQueueOperationObject, nil];
                    userInfo[NSLocalizedDescriptionKey] = [error localizedDescription];
                    NSError* passwordExpiredError = [NSError errorWithDomain:@"com.cloudworks.syncOperation" code:UserNotAllowedErrorCode userInfo:userInfo];
                    [self postError:[passwordExpiredError localizedDescription] error:passwordExpiredError];
                    errorPosted = TRUE;
                    [userInfo release];
                    [parentQueue cancelAllOperationsAsync];
                }
            }
        }
    }
    if (!errorPosted){
        [self postError:text error:error];
    }
    
    if ([self shouldCancelOnError:error])
        [self cancel];
    else
        [self signalOperationDidComplete];
}

-(void)waitForRequest{
    dispatch_semaphore_wait(waitForRequestSemaphore, DISPATCH_TIME_FOREVER);
}

-(BOOL)result{
    return result;
}

-(NSString*)address{
    return syncManager.address;
}

@end
