//
//  ApppleTVmanager.h
//  ProductBuilder
//
//  Created by User on 5/7/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CWMovieViewController;

@interface ApppleTVmanager : NSObject{
    BOOL isDisplayed;
    CWMovieViewController *currentMovieController;
}
@property (retain, nonatomic) UIWindow *secondaryWindow;
@property (nonatomic) BOOL isDisplayed;

- (void)initScreens;
+(ApppleTVmanager *)sharedInstance;
-(void)setImage:(UIImage*)image;
-(void)setDefaultImage;
-(void)changeDisplayButtonState;
- (void)setMovieController:(CWMovieViewController *)movieController;
@end
