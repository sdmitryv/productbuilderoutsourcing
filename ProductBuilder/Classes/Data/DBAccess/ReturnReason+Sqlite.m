//
//  ReturnReason+Sqlite.m
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/18/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "ReturnReason.h"
#import "CROEntity+Sqlite.h"

@implementation ReturnReason (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"ReturnReasonID"];
	self.description = [[row stringForColumn:@"Description"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.listOrder = [row intForColumn:@"ListOrder"];
	self.deleted = [row boolForColumn:@"IsDeleted"];
    self.defaultLogicalBinId = [row UuidForColumn:@"DefaultLogicalBinID"];
}

+(void)load{
    EXCHANGE_METHOD(getReturnReasons, getReturnReasonsImpl);
}

+(NSMutableArray*)getReturnReasonsImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
    EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM ReturnReason WHERE IsDeleted = 0 ORDER BY ListOrder, trim(Description) collate nocase"];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

+(id)getInstanceById:(BPUUID *)reasonId {
    if (!reasonId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"select * from ReturnReason where ReturnReasonID=?", [reasonId description],nil];
	if ([result count]>0){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

@end
