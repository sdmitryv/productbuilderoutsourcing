//
//  TimeZone+Sqlite.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 6/25/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "TimeZone.h"
#import "CROEntity+Sqlite.h"

@implementation TimeZone(Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"TimeZineID"];
	self.name = [row stringForColumn:@"Name"];
	self.description = [row stringForColumn:@"Description"];
	self.isDeleted = [row boolForColumn:@"IsDeleted"];
	self.utcOffset = [row decimalForColumn:@"UTCOffset"];
}

+(void)load{
    EXCHANGE_METHOD(getTimeZones, getTimeZonesImpl);
}

+(NSMutableArray*)getTimeZonesImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM TimeZone"];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

+(NSString*)stmtGetInstanceById {
    return @"SELECT rowid, * FROM TimeZone WHERE TimeZoneID = ?";
}

@end
