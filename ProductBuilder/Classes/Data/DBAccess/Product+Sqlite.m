//
//  Product+Sqlite.m
//  ProductBuilder
//
//  Created by Viacheslav Lulakov on 9/5/17.
//  Copyright © 2017 Cloudworks. All rights reserved.
//

#import "Product.h"
#import "CEntity+Sqlite.h"

NSString * const productSelectStm = @"SELECT rowid, * FROM Product ORDER BY RecCreated DESC";

@implementation Product (Sqlite)

+(void)load{
    EXCHANGE_METHOD(getProducts, getProductsImpl);
}

-(void)fillProperties:(NSMutableDictionary*)parameters{
    [parameters setObjectNilSafe:[_id description] forKey:@"ProductId"];
    [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
    [parameters setObjectNilSafe:_recModified forKey:@"RecModified"];
    [parameters setObjectNilSafe:self.clu forKey:@"CLU"];
    NSString * elements = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@{@"elements":self.elements} options:0 error:nil] encoding:NSUTF8StringEncoding];
    [parameters setObjectNilSafe:elements forKey:@"Elements"];
    [elements release];
    NSString * modifications = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@{@"modifications":self.modifications} options:0 error:nil] encoding:NSUTF8StringEncoding];
    [parameters setObjectNilSafe:modifications forKey:@"Modifications"];
    [modifications release];
    [parameters setObjectNilSafe:self.instructions forKey:@"Instructions"];
    [parameters setDecimal:self.price.decimalValue forKey:@"Price"];
}

-(BOOL)save:(EGODatabase*)db error:(NSError**)error {
    
    static NSString* stmtIns = @"INSERT INTO Product(\
     RecCreated\
    ,RecModified \
    ,ProductId \
    ,CLU \
    ,Elements \
    ,Modifications \
    ,Instructions \
    ,Price \
    ) \
    VALUES (\
     @RecCreated \
    ,@RecModified \
    ,@ProductId \
    ,@CLU \
    ,@Elements \
    ,@Modifications \
    ,@Instructions \
    ,@Price \
    )";
    static NSString* smtUpdate = @"UPDATE Product SET \
    RecCreated = @RecCreated\
    ,RecModified = @RecModified \
    ,ProductId = @ProductId \
    ,CLU = @CLU \
    ,Elements = @Elements \
    ,Modifications = @Modifications \
    ,Instructions = @Instructions \
    ,Price = @Price \
    where ProductId = @ProductId";
    
    NSString* stmt = nil;
    BOOL result = YES;
    if (super.isDirty){
        NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
        [parameters setObjectNilSafe:[_id description] forKey:@"ProductId"];
        stmt = self.isNew ? stmtIns : smtUpdate;
        [self fillProperties:parameters];
        
        result = [db executeUpdate:stmt namedParameters:parameters];
        [parameters release];
        
        if (!result || [db hadError]){
            [[self class] writeDbError:db error:error domain:@"com.product.save"];
            return NO;
        }
    }
    
    return result;
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"ProductId"];
    self.clu = [row stringForColumn:@"CLU"];
    NSString * elements = [row stringForColumn:@"Elements"];
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:[elements dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    self.elements = [dict valueForKey:@"elements"];
    NSString * modifications = [row stringForColumn:@"Modifications"];
    dict = [NSJSONSerialization JSONObjectWithData:[modifications dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    self.modifications = [dict valueForKey:@"modifications"];
    self.instructions = [row stringForColumn:@"Instructions"];
    self.price = [row decimalNumberForColumn:@"Price"];
    [self markClean];
}

+(NSMutableArray*)getProductsImpl {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM Product ORDER BY RecCreated DESC LIMIT 20"];
    for(EGODatabaseRow* row in result) {
        id item = [[self class] instanceFromRow:row];
        [list addObject:item];
    }
    return [list autorelease];
}

@end
