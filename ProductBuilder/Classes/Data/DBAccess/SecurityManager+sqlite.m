//
//  SecurityManager+sqlite.m
//  CloudworksPOS
//
//  Created by Vitaliy Gervazuk on 8/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SecurityManager.h"
#import "CROEntity+Sqlite.h"


@implementation SecurityManager(Sqlite)

+(void)load{
    [[self class] exchangeMethod:@selector(isUserInRole:role:) withNewMethod:@selector(isUserInRoleImpl:role:)];
}

+(BOOL)isUserInRoleImpl:(BPUUID *)employeeId role:(NSString *)role {
    //if ([role isEqualToString:UserRoleGiftCardsOffline]) return FALSE;
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray* params = [[NSMutableArray alloc]init];
    [params addObjectNilSafe:role];
    [params addObjectNilSafe:employeeId];
	EGODatabaseResult* result = [db executeQuery:
//  @"SELECT 1 FROM EmployeeRole as er INNER JOIN Role r ON er.RoleID = r.RoleID WHERE er.EmployeeID = ? AND r.RoleCode = ? limit 1" 
     @"select 1 from (\
     SELECT emp.LoginName, er.EmployeeID FROM Employee emp LEFT JOIN \
     (select er.EmployeeID from RoleEmployee er \
                            inner join Role r ON er.RoleID = r.RoleID \
                            inner join RoleRight rr ON rr.RoleId = r.RoleId \
                            inner join Right rght ON rght.RightID = rr.RightId \
                                 where rght.RightCode = ?) er \
     on emp.EmployeeID=er.EmployeeID WHERE emp.EmployeeID = ? \
     ) where LoginName='system' or EmployeeID is not null limit 1"
                                      parameters:params];
    [params release];
	if([result.rows count] > 0) {
		return TRUE;
	}
    
	return FALSE;
}

@end
