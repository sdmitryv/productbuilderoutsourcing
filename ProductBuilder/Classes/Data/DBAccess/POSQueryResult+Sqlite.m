//
//  POSQueryResult+Sqlite.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "POSQueryResult.h"
#import "CEntity+Sqlite.h"
#import "Settings.h"
#import "AppSettingManager.h"

@implementation POSQueryResult(Sqlite)

-(BOOL)save {
    EGODatabase *db = [DataManager instance].currentDatabase;
	NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    self.recModified = [NSDate date];
    [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
	[parameters setObjectNilSafe:_recModified forKey:@"RecModified"];
    [parameters setObjectNilSafe:_id forKey:@"QueryResultID"];
    [parameters setObjectNilSafe:queryID forKey:@"QueryID"];
    [parameters setObjectNilSafe:[[AppSettingManager instance].deviceUniqueId description] forKey:@"DeviceID"];
    [parameters setObjectNilSafe:result forKey:@"QueryResult"];
    [parameters setInteger:0 forKey:@"SyncState"];
    
	int res =[db executeUpdate:@"INSERT OR REPLACE INTO PosQueryResult (\
                                     QueryResultID,\
                                     QueryID,\
                                     RecCreated,\
                                     RecModified,\
                                     DeviceID,\
                                     QueryResult, \
                                     SyncState \
                                     )  VALUES  (\
                                     @QueryResultID,\
                                     @QueryID,\
                                     @RecCreated,\
                                     @RecModified,\
                                     @DeviceID,\
                                     @QueryResult, \
                                     @SyncState)"
      namedParameters:parameters];
    
    [parameters release];
	if (!res || [db hadError])
	{
		NSLog(@"error: %@", [db lastErrorMessage]);
		return FALSE;
	}
    
    return TRUE;
}

@end
