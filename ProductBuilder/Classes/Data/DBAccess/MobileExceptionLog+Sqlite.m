//
//  MobileExceptionLog+Sqlite.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/3/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "MobileExceptionLog.h"
#import "CROEntity+Sqlite.h"

@implementation MobileExceptionLog (Sqlite)

-(BOOL)save:(EGODatabase*)db error:(NSError**)error{
    //if (!self.isDirty) return TRUE;
	NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];

    [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
	[parameters setObjectNilSafe:_recModified forKey:@"RecModified"];
    [parameters setObjectNilSafe:_id forKey:@"MobileExceptionLogID"];
    [parameters setObjectNilSafe:self.message forKey:@"Message"];
    
    [parameters setObjectNilSafe:self.locationID forKey:@"LocationID"];
    [parameters setObjectNilSafe:self.worstationID forKey:@"WorkstationID"];
    [parameters setObjectNilSafe:self.deviceID forKey:@"DeviceID"];
    [parameters setObjectNilSafe:self.description forKey:@"Description"];
    [parameters setInt:2 forKey:@"SyncState"];
    
    NSString* const stmtInsert = @"INSERT INTO MobileExceptionLog(\
    RecCreated\
    ,RecModified\
    ,MobileExceptionLogID\
    ,Message\
    ,LocationID\
    ,WorkstationID\
    ,DeviceID\
    ,Description\
    ,SyncState\
        )  VALUES  (\
    @RecCreated\
    ,@RecModified\
    ,@MobileExceptionLogID\
    ,@Message\
    ,@LocationID\
    ,@WorkstationID\
    ,@DeviceID\
    ,@Description\
    ,@SyncState\
    )";
    
	int result = [db executeUpdate:stmtInsert namedParameters:parameters];
    [parameters release];
	if (!result || [db hadError])
	{
		NSLog(@"error: %@", [db lastErrorMessage]);
		return FALSE;
	}
    
	return TRUE;
}


@end
