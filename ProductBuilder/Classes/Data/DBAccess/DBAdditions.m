//
//  DBAdditions.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/8/10.
//  Copyright 2010 DBBestTechnologies. All rights reserved.
//

#import "DBAdditions.h"

#define NULLSAFE(anObject) ({ anObject ? : [NSNull null]; })

@implementation NSMutableOrderedSet (NSNilSafeOrderedSet)
- (void)addObjectNilSafe:(id)anObject{
	[self addObject:NULLSAFE(anObject)];
}
@end

@implementation NSMutableArray (NSNilSafeArray)
- (void)addObjectNilSafe:(id)anObject{
	[self addObject:NULLSAFE(anObject)];
}

- (void)addChar:(char)value{
	[self addObject:@(value)];
}

- (void)addUnsignedChar:(unsigned char)value{
	[self addObject:@(value)];
}

- (void)addShort:(short)value{
	[self addObject:@(value)];
}

- (void)addUnsignedShort:(unsigned short)value{
	[self addObject:@(value)];
}

- (void)addInt:(int)value{
	[self addObject:@(value)];
}

-(void)addUnsignedInt:(unsigned int)value{
	[self addObject:@(value)];
}

- (void)addLong:(long)value{
	[self addObject:@(value)];
}

- (void)addUnsignedLong:(unsigned long)value{
	[self addObject:@(value)];
}

- (void)addLongLong:(long long)value{
	[self addObject:@(value)];
}

- (void)addUnsignedLongLong:(unsigned long long)value{
	[self addObject:@(value)];
}

- (void)addFloat:(float)value{
	[self addObject:@(value)];
}

- (void)addDouble:(double)value{
	[self addObject:@(value)];
}

- (void)addBool:(BOOL)value{
	[self addObject:@(value)];
}

- (void)addInteger:(NSInteger)value{
	[self addObject:@(value)];
}

- (void)addUnsignedInteger:(NSUInteger)value{
   [self addObject:@(value)];
}

- (void)addDecimal:(NSDecimal)value{
	  [self addObject:[NSDecimalNumber decimalNumberWithDecimal:value]];
}
	 

@end


@implementation NSMutableDictionary (NSNilSafeArray)
- (void)setObjectNilSafe:(id)anObject forKey:(NSString*)key{
	self[key] = NULLSAFE(anObject);
}

- (void)setChar:(char)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedChar:(unsigned char)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setShort:(short)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedShort:(unsigned short)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setInt:(NSInteger)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedInt:(unsigned int)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setLong:(long)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedLong:(unsigned long)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setLongLong:(long long)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedLongLong:(unsigned long long)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setFloat:(float)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setDouble:(double)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setBool:(BOOL)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setInteger:(NSInteger)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setUnsignedInteger:(NSUInteger)value forKey:(NSString*)key{
	self[key] = @(value);
}

- (void)setDecimal:(NSDecimal)value forKey:(NSString*)key{
	self[key] = [NSDecimalNumber decimalNumberWithDecimal:value];
}

@end
