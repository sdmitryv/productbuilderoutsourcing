//
//  CROEntity+Sqlite.m
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CROEntity+Sqlite.h"

@implementation CROEntity (Sqlite)

+(NSString*)stmtGetInstanceById{
    return nil;
}

+(id)getInstanceByIdImpl:(BPUUID*)id{
    NSString* stmt = [self stmtGetInstanceById];
    if (!stmt) return nil;
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters:stmt, [id description],nil];
	if ([result count]){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

+(instancetype)instanceFromRow:(EGODatabaseRow *)row{
	return [[[[self class] alloc]initWithRow:row]autorelease];
}

-(id)initWithRow:(EGODatabaseRow *)row{
	
	if ((self = [super init])){
        [self initInternal];
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
		[self fetch:row];
        [pool release];
	}
	return self;
}

-(void)fetch:(EGODatabaseRow *)row{
	self.rowid = @([row intForColumn:@"rowid"]);
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
}

-(void)refreshImpl {
    NSString* stmt = [[self class] stmtGetInstanceById];
    if (!stmt) return;
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	EGODatabaseResult* result = [db executeQueryWithParameters:stmt, [self.id description],nil];
	if ([result count]){
		[self initWithRow:[result rowAtIndex:0]];
	}
}

+(BOOL)writeDbError:(EGODatabase*)db error:(NSError**)error domain:(NSString*)domain{
    NSLog(@"error: %@", [db lastErrorMessage]);
    if (error){
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:[db lastErrorMessage] forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:domain code:100 userInfo:errorDetail];
    }
    return error!=nil;
}

+(NSString*)stmtGetList{
    return nil;
}

+(void)load{
    EXCHANGE_METHOD(getInstanceById:,getInstanceByIdImpl:);
    EXCHANGE_METHOD(list,listImpl);
    EXCHANGE_METHOD(listWithQuery:, listImpleWithQuery:);
    EXCHANGE_METHOD(refresh, refreshImpl);
}

+(NSArray*)listImpl{
//    NSString* stmtGetList = [self stmtGetList];
//    if (!stmtGetList) return nil;
//	NSMutableArray *list = [[NSMutableArray alloc] init];
//	EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQuery:stmtGetList];
//	for(EGODatabaseRow* row in result) {
//		id item = [[self class] instanceFromRow:row];
//		[list addObject:item];
//	}
//	return [list autorelease];
    return [self.class listWithQuery:[self stmtGetList]];
}

+(NSArray*)listImpleWithQuery:(NSString*)query {
    NSString* stmtGetList = query;
    if (!stmtGetList) return nil;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQuery:stmtGetList];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

+(NSString*)getActualSortingColumn:(NSString*)sortingColumn{
    return sortingColumn;
}

@end
