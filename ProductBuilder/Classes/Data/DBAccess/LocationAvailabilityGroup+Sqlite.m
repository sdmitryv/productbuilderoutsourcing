//
//  LocationAvailabilityGroup+Sqlite.m
//  TeamworkPOS
//
//  Created by Lulakov Viacheslav on 1/16/13.
//  Copyright (c) 2013 Cloudworks. All rights reserved.
//

#import "LocationAvailabilityGroup.h"
#import "CROEntity+Sqlite.h"

@implementation LocationAvailabilityGroup (Sqlite)

+(id)getInstanceById:(BPUUID *)id {
    if (!id) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"SELECT rowid, * FROM LocationAvailabilityGroup WHERE LocationAvailabilityGroupID=?",[id description],nil];
	if ([result count]>0){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"LocationAvailabilityGroupID"];
    self.description = [row stringForColumn:@"Description"];
    self.code = [row stringForColumn:@"Code"];
    self.displayDistance = [row intForColumn:@"DisplayDistance"];
}

@end
