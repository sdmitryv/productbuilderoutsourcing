//
//  DeviceStats+Sqlite.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "DeviceStats.h"
#import "CEntity+Sqlite.h"

#define deviceStatsSelectSTMT @"select rowid, key, value, state from DeviceStats"
NSString * const deviceStatsSelectStmt = @""deviceStatsSelectSTMT"";

@implementation DeviceStats (Sqlite)

+(void)load{
    
    [[self class] exchangeMethod:@selector(getStatsList) withNewMethod:@selector(getStatsListImpl)];
}

+(NSDictionary*)getStatsListImpl{
    
    SqlCommand* command = [[SqlCommand alloc] initWithStmt:deviceStatsSelectStmt];
    EGODatabaseResult* result = [command execute];
    
    NSMutableDictionary* statsDic = [[NSMutableDictionary alloc] initWithCapacity:result.rows.count];
    
    for (EGODatabaseRow* row in result.rows){
        
        DeviceStats* stats = [[DeviceStats alloc] initWithRow:row];
        statsDic[stats.key] = stats.value;
        [stats release];
    }
    
    if (!statsDic[@"LastRun"])
        statsDic[@"LastRun"] = [DeviceStats getLastRun];
    
    [command release];
    
    return [statsDic autorelease];
}

-(void)fetch:(EGODatabaseRow *)row{
    
    [super fetch:row];
    
    self.key = [row stringForColumn:@"key"];
    self.value = [row stringForColumn:@"value"];
    self.state = [row intForColumn:@"state"];
}

-(BOOL)save:(NSError **)error{
    
    @try {
        
        EGODatabase *db = [DataManager instance].currentDatabase;
        if (!db)
            return NO;
        static NSString *stmtMemo = @"INSERT OR REPLACE INTO DeviceStats (key, value, state) VALUES (@key, @value, 1)";
         
        NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
        [parameters setObjectNilSafe:self.key   forKey:@"key"];
        [parameters setObjectNilSafe:self.value forKey:@"value"];
        BOOL result = [db executeUpdate:stmtMemo namedParameters:parameters];
        [parameters release];

        return result;
    }
    @finally{
    }
}

@end
