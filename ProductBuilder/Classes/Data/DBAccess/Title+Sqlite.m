//
//  Title+Sqlite.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/13/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "Title.h"
#import "CROEntity+Sqlite.h"

@implementation Title(Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"TitleID"];
	self.description = [[row stringForColumn:@"Description"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	self.listOrder = [row intForColumn:@"ListOrder"];
	self.isReqRetired = [row intForColumn:@"IsReqRetired"];
}

+(void)load{
    EXCHANGE_METHOD(getTitles, getTitlesImpl);
}

+(NSMutableArray*)getTitlesImpl
{
	DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
	EGODatabaseResult* result = [db executeQuery:@"SELECT rowid, * FROM Title"];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

@end
