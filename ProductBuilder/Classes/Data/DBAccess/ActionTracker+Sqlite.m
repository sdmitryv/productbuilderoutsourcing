//
//  EventLog+Sqlite.m
//  ProductBuilder
//
//  Created by valera on 10/10/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "ActionTracker.h"
#import "SecurityManager.h"
#import "Workstation.h"
#import "Location.h"
#import "Settings.h"
#import "AppSettingManager.h"
#import "DBAdditions.h"
#import "NSObject+MethodExchange.h"

@implementation ActionTracker (Sqlite)

+(void)load{
    
    [[self class]exchangeMethod:@selector(logAction:    details:objectID:parentID:documetType:employeeId:reason:database:)
                  withNewMethod:@selector(logActionImpl:details:objectID:parentID:documetType:employeeId:reason:database:)];
}

+(void)logActionImpl:(NSString*)action details:(NSString*)details objectID:(BPUUID*) objectID parentID:(BPUUID*)parentID documetType:(NSString*)documentType employeeId:(BPUUID*)employeeId reason:(NSString*)reason database:(EGODatabase *)db {
    if (![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [self.class logActionImpl:action details:details objectID:objectID parentID:parentID documetType:documentType employeeId:employeeId reason:reason database:db];
            return;
    });
    }
    NSString* const stmt = @"insert into ActionsTracking(\
RecCreated,\
RecModified,\
ActionsTrackingID,\
EmployeeID,\
WorkstationID,\
LocationID,\
DeviceID,\
DeviceUniqueID,\
Action,\
DocumentType,\
ObjectID,\
ParentID,\
DetailedInfo,\
Application,\
Reason,\
SyncState)\
values \
(\
(julianday('now') - 2440587.5)*86400.0,\
(julianday('now') - 2440587.5)*86400.0,\
@ActionsTrackingID,\
@EmployeeID,\
@WorkstationID,\
@LocationID,\
@DeviceID,\
@DeviceUniqueID,\
@Action,\
@DocumentType,\
@ObjectID,\
@ParentID,\
@DetailedInfo,\
@Application,\
@Reason,\
1\
)";
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    [parameters setObjectNilSafe:[BPUUID UUID] forKey:@"ActionsTrackingID"];
    [parameters setObjectNilSafe:employeeId ? employeeId :[SecurityManager currentEmployee].id forKey:@"EmployeeID"];
    [parameters setObjectNilSafe:[Workstation currentWorkstation].id forKey:@"WorkstationID"];
    [parameters setObjectNilSafe:[Location localLocation].id forKey:@"LocationID"];
    [parameters setObjectNilSafe:[AppSettingManager instance].iPadId forKey:@"DeviceID"];
    [parameters setObjectNilSafe:[AppSettingManager instance].deviceUniqueId forKey:@"DeviceUniqueID"];
    [parameters setObjectNilSafe:action forKey:@"Action"];
    [parameters setObjectNilSafe:documentType forKey:@"DocumentType"];
    [parameters setObjectNilSafe:objectID forKey:@"ObjectID"];
    [parameters setObjectNilSafe:parentID forKey:@"ParentID"];
    [parameters setObjectNilSafe:details forKey:@"DetailedInfo"];
    [parameters setObjectNilSafe:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] forKey:@"Application"];
    [parameters setObjectNilSafe:reason forKey:@"Reason"];
    [db executeUpdate:stmt namedParameters:parameters];
    [parameters release];
}
@end
