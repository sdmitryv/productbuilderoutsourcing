//
//  CurrencyDenomination+Sqlite.m
//  DrawerMemo
//
//  Created by Alexander Martyshko on 11/23/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CurrencyDenomination.h"
#import "CROEntity+Sqlite.h"
#import "DBAdditions.h"

@implementation CurrencyDenomination (Sqlite)

+(void)load{
    [[self class]exchangeMethod:@selector(getDenominationByCurrency:quickTenderOnly:) withNewMethod:@selector(getDenominationByCurrency:quickTenderOnlyImpl:)];
}

+(id)getInstanceById:(BPUUID*)currencyDenominationID{
	if (!currencyDenominationID) return nil;
	EGODatabaseResult* result = [((DataManager*)[DataManager instance]).currentDatabase
								 executeQueryWithParameters:@"select *  from CurrencyDenomination where CurrencyDenominationID=?", [currencyDenominationID description],nil];
	if ([result count]>0){
		return [[self class] instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"CurrencyDenominationID"];
    
    self.currencyID = [row UuidForColumn:@"CurrencyID"];
    self.description = [row stringForColumn:@"Description"];
    self.value = [row decimalForColumn:@"Value"];
    self.listOrder = [row intForColumn:@"ListOrder"];
    self.status = [row intForColumn:@"Status"];
    self.code = [row stringForColumn:@"Code"];
    self.isQuickTender = [row boolForColumn:@"IsQuickTender"];
}

+(NSString*)stmtGetList{
    return @"SELECT * FROM CurrencyDenomination";
}

+(NSArray*)getDenominationByCurrency:(BPUUID *)currencyId quickTenderOnlyImpl:(BOOL)quickTenderOnly {
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSMutableString * selectStatement = [NSMutableString stringWithFormat:@"SELECT rowid, * FROM CurrencyDenomination WHERE CurrencyID = '%@'", [currencyId description]];
    if (quickTenderOnly) {
        [selectStatement appendString:@" AND IsQuickTender = 1"];
    }
    
    EGODatabaseResult* result = [db executeQuery:selectStatement];
    for(EGODatabaseRow* row in result) {
        id item = [[self class] instanceFromRow:row];
        [list addObject:item];
    }
    return [list autorelease];
}

@end
