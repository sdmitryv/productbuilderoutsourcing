//
//  CROEntity+Sqlite.h
//  ProductBuilder
//
//  Created by Valera on 9/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CROEntity.h"
#import "EGODatabase.h"
#import "DBAdditions.h"
#import "DataManager.h"
#import "NSObject+MethodExchange.h"

@interface CROEntity (Sqlite)

-(id)initWithRow:(EGODatabaseRow *)row;
-(void)fetch:(EGODatabaseRow *)row;
+(instancetype)instanceFromRow:(EGODatabaseRow *)row;
+(BOOL)writeDbError:(EGODatabase*)db error:(NSError**)error domain:(NSString*)domain;
+(NSString*)stmtGetList;
+(NSString*)stmtGetInstanceById;
+(NSString*)getActualSortingColumn:(NSString*)sortingColumn;
@end
