//
//  POSQuery+Sqlite.m
//  ProductBuilder
//
//  Created by Виталий Гервазюк on 3/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "POSQuery.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"
#import "Settings.h"
#import "Location.h"
#import "AppSettingManager.h"

@implementation POSQuery(Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"QueryID"];
    deviceID = [[row UuidForColumn:@"DeviceID"] retain];
    locationID = [[row UuidForColumn:@"LocationID"] retain];
    queryStr = [[row stringForColumn:@"QueryStr"] retain];
}

+(void)load{
    EXCHANGE_METHOD(getActiveQueries, getActiveQueriesImpl);
}

+(NSMutableArray *)getActiveQueriesImpl {
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
	NSMutableArray *list = [[NSMutableArray alloc] init];
    
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc]init];
    [parameters setObjectNilSafe:[[AppSettingManager instance].deviceUniqueId description] forKey:@"DeviceID"];
    [parameters setObjectNilSafe:[[Location localLocation].id description] forKey:@"LocationID"];
    
	EGODatabaseResult* result = [db executeQuery:
                                 @"SELECT * FROM (\
                                 SELECT rq.rowid, rq.* FROM PosQuery rq\
                                 LEFT JOIN PosQueryResult rqr ON rqr.QueryID = rq.QueryID AND rqr.DeviceID = @DeviceID\
                                 WHERE rqr.QueryResultID IS NULL)\
                                 WHERE (DeviceID = @DeviceID OR DeviceID IS NULL) AND (LocationID = @LocationID OR LocationID IS NULL)" namedParameters:parameters];
    [parameters release];
	for(EGODatabaseRow* row in result) {
		id item = [[self class] instanceFromRow:row];
		[list addObject:item];
	}
	return [list autorelease];
}

@end
