//
//  CalendarStatus+Sqlite.m
//  ProductBuilder
//
//  Created by Lulakov Viacheslav on 10/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "CalendarStatus.h"
#import "CEntity+Sqlite.h"

@implementation CalendarStatus (Sqlite)

+(id)getInstanceByTypeImpl:(CalendarStatusType)type {
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    
	EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT * FROM CalendarStatus where Status = ? and isDeleted = 0", @(type), nil];
	if (result.count > 0) {
        return [[self class] instanceFromRow:(result.rows)[0]];
    }
	return nil;
}

+(id)getInstanceById:(BPUUID *)statusId{
	if (!statusId) return nil;
	EGODatabaseResult* result = [[DataManager instance].currentDatabase
								 executeQueryWithParameters:@"select * from CalendarStatus where CalendarStatusId=?", [statusId description],nil];
	if ([result count] > 0) {
		return [CalendarStatus instanceFromRow:[result rowAtIndex:0]];
	}
	return nil;
}

+(void)load {
    EXCHANGE_METHOD(getInstanceByType:, getInstanceByTypeImpl:);
}

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
    
	self.id = [row UuidForColumn:@"CalendarStatusId"];
    self.status = [row intForColumn:@"Status"];
    self.label = [row stringForColumn:@"Label"];

}

@end
