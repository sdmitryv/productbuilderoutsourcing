//
//  TableCustomFields+Sqlite.m
//  CloudworksPOS
//
//  Created by
//  Copyright 2015 __MyCompanyName__. All rights reserved.
//

#import "TableCustomFields.h"
#import "TableCustomField.h"
#import "DBAdditions.h"
#import "CROEntity+Sqlite.h"

NSString * const tableCustomFieldsStm = @"SELECT ft.*, t.Require, t.FieldOrder FROM CustomFieldTypes ft, CustomFieldTables t WHERE t.tableName = ? AND t.FieldTypeId = ft.CustomFieldTypeId";


@implementation TableCustomFields (Sqlite)

+(void)load{
    
    [[self class] exchangeMethod:@selector(loadFields) withNewMethod:@selector(loadFieldsImpl)];
    [[self class] exchangeMethod:@selector(resetCache) withNewMethod:@selector(resetCacheImpl)];
}


-(void)loadFieldsImpl {

    [_fieldsList removeAllObjects];
    
    @synchronized (self) {
            if (!_cashedTableCustomFields){
                _cashedTableCustomFields = [[NSMutableDictionary alloc]init];
            }
        
        NSMutableArray* cachedList = _cashedTableCustomFields[_tableName];
        if (!cachedList){
            EGODatabaseResult* result = [[DataManager instance].currentDatabase executeQueryWithParameters:[NSString stringWithFormat:@"SELECT * FROM (%@) ORDER BY FieldOrder", tableCustomFieldsStm], _tableName ,nil];
            cachedList = [[NSMutableArray alloc]init];
            for(EGODatabaseRow* row in result) {
                TableCustomField *field = [[TableCustomField alloc] initWithRow:row];
                [_fieldsList addObject:field];
                TableCustomField *fieldCached = [field copy];
                [cachedList addObject:fieldCached];
                [field release];
                [fieldCached release];
            }
            _cashedTableCustomFields[_tableName] = cachedList;
            [cachedList release];
        }
        else{
            for(TableCustomField* fieldCached in cachedList) {
                TableCustomField *field = [fieldCached copy];
                [_fieldsList addObject:field];
                [field release];
            }
        }
    }
}

NSMutableDictionary* _cashedTableCustomFields;

+(void)resetCacheImpl{
    if (_cashedTableCustomFields){
        @synchronized (self) {
            if (_cashedTableCustomFields){
                [_cashedTableCustomFields release];
                _cashedTableCustomFields = nil;
            }
        }
    }
}

@end
