//
//  PostalCode+Sqlite.m
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/14/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

#import "PostalCode.h"
#import "CROEntity+Sqlite.h"

@implementation PostalCode(Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
	[super fetch:row];
	self.recCreated = [row dateForColumn:@"RecCreated"];
	self.recModified = [row dateForColumn:@"RecModified"];
	self.id = [row UuidForColumn:@"PostalCodeID"];
	self.postalCode = [row stringForColumn:@"PostalCode"];
	self.city = [row stringForColumn:@"City"];
	self.state = [row stringForColumn:@"State"];
	self.country = [row stringForColumn:@"Country"];
	self.stateCode = [row stringForColumn:@"SCode"];
	self.countryCode = [row stringForColumn:@"CCode"];
}

+(void)load{
    EXCHANGE_METHOD(getPostalCode:, getPostalCodeImpl:);
}

+(PostalCode *)getPostalCodeImpl:(NSString *)code {
    if (!code) return nil;
    PostalCode *postalCode = nil;
    DataManager *dbManager =[DataManager instance];
	EGODatabase *db = dbManager.currentDatabase;
    NSArray* words = [code componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    code = [words componentsJoinedByString:@""];
	EGODatabaseResult* result = [db executeQueryWithParameters:@"SELECT rowid, *, (SELECT Code FROM Country c WHERE c.CountryID=pc.CountryId) as CCode,(SELECT Code FROM State s WHERE s.StateId=pc.StateId) as SCode FROM PostalCode pc WHERE pc.PostalCode = ?", code, nil];
	for(EGODatabaseRow* row in result) {
		postalCode = [[self class] instanceFromRow:row];
		break;
	}
	return postalCode;
}

@end