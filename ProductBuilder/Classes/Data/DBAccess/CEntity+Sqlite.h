//
//  CEntity+Sqlite.h
//  iPadPOS
//
//  Created by Lulakov Viacheslav on 3/11/11.
//  Copyright 2011 DBBestTechnologies. All rights reserved.
//

#import "CEntity.h"
#import "EGODatabase.h"
#import "CROEntity+Sqlite.h"
#import "DBAdditions.h"

@interface CEntity (Sqlite)

-(BOOL)save:(EGODatabase*)db error:(NSError**)error;
@end
