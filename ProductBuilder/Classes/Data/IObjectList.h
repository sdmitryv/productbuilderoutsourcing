//
//  IObjectList.h
//  CloudworksPOS
//
//  Created by Lulakov Viacheslav on 7/8/11.
//  Copyright 2011 Cloudworks. All rights reserved.
//

@protocol IObjectList <NSObject>

-(id)objectAtIndex:(NSInteger)index;
-(id)objectAtIndexedSubscript:(NSInteger)index;
-(id)objectByRowid:(NSNumber *)rowid;
-(NSInteger)indexOfObject:(id)item;
-(NSInteger)count;
-(void)refresh;

@property (nonatomic, retain) NSArray* sortOrderList;

@end
