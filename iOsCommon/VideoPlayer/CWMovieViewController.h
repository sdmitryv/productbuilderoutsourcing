/*
 
 File: AVPlayerDemoPlaybackView.h
 
 Abstract: UIView subclass to display the content associated with an AVPlayer
 
 Version: 1.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by 
 Apple Inc. ("Apple") in consideration of your agreement to the
 following terms, and your use, installation, modification or
 redistribution of this Apple software constitutes acceptance of these
 terms.  If you do not agree with these terms, please do not use,
 install, modify or redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software. 
 Neither the name, trademarks, service marks or logos of Apple Inc. 
 may be used to endorse or promote products derived from the Apple
 Software without specific prior written permission from Apple.  Except
 as expressly stated in this notice, no other rights or licenses, express
 or implied, are granted by Apple herein, including but not limited to
 any patent rights that may be infringed by your derivative works or by
 other works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010-2011 Apple Inc. All Rights Reserved.
 
 */

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#define CWMoviePlayerPlaybackDidFinishNotification      @"CWMoviePlayerPlaybackDidFinishNotification"
#define CWMoviePlayerFulscreenChangedNotification       @"CWMoviePlayerPlaybackDidFinishNotification"


@class AVPlayer;

@interface AVPlayerDemoPlaybackView : UIView

@property (nonatomic, retain) AVPlayer* player;

- (void)setPlayer:(AVPlayer*)player;
- (void)setVideoFillMode:(NSString *)fillMode;

@end

@interface ControlButton : UIButton {
    NSInteger imageSize;
}
@property (nonatomic, assign) NSInteger imageSize;
@end

@interface PlayButton : ControlButton {

}
@end

@interface PauseButton : ControlButton {
}
@end

@interface FullScreenButton : ControlButton {
    BOOL enterFullscreenMode;
}

@property (nonatomic, assign) BOOL enterFullscreenMode;

@end

@interface PanelView : UIView {
    UISlider *slider;
    UILabel *timePastLabel;
    UILabel *timeRemaininglabel;
    PlayButton *playButton;
    PauseButton *stopButton;
    FullScreenButton *fullscreenButton;
    
    UIView *sliderWithLabelsView;
    BOOL isFullscreen;
}

@property (nonatomic, retain) UISlider *slider;
@property (nonatomic, retain) UILabel *timePastLabel;
@property (nonatomic, retain) UILabel *timeRemaininglabel;
@property (nonatomic, retain) UIButton *playButton;
@property (nonatomic, retain) UIButton *stopButton;
@property (nonatomic, retain) UIButton *fullscreenButton;

@property (nonatomic, retain) UIView *sliderWithLabelsView;
@property (nonatomic, assign) BOOL isFullscreen;
@property (nonatomic, assign) BOOL pseudoFullScreen;

@end


@class AVPlayer;

@interface CWMovieViewController : UIViewController{
    
    AVPlayerDemoPlaybackView* mPlaybackView;
    
    AVPlayer* mPlayer;
    AVPlayerItem * mPlayerItem;
    
    PanelView *panelView;

    float mRestoreAfterScrubbingRate;
	BOOL seekToZeroBeforePlay;
	id mTimeObserver;
    
	NSURL* mURL;
    MPVolumeView *volume;
    
    BOOL fullscreen;
    BOOL shouldAutoplay;
    UIView *contentView;
    CGAffineTransform initialTransform;
    //UIStatusBarStyle initialStatusBarStyle;
    
    UIToolbar *fullscreenTopBar;
    UIView *fullscreenCenterView;
    long currentIndex;
    BOOL isHidden;
    BOOL appleTVActive;
    
}

@property (readwrite, retain, setter=setPlayer:, getter=player) AVPlayer* mPlayer;
@property (nonatomic, retain) AVPlayerItem* mPlayerItem;
@property(nonatomic,retain)AVPlayerDemoPlaybackView* mPlaybackView;

@property(nonatomic,retain) PanelView *panelView;
@property (nonatomic, assign) BOOL appleTVActive;
@property (nonatomic, assign) BOOL fullscreen;
@property (nonatomic, assign) BOOL shouldAutoplay;
@property (nonatomic, assign) BOOL supportsPseudoFullScreen;
@property (nonatomic, assign) BOOL pseudoFullScreen;

- (void)changeFullscreenMode;

- (void)setURL:(NSURL*)URL;
- (NSURL*)URL;
- (void)play;
- (void)pause;
- (void)stop;

@end
