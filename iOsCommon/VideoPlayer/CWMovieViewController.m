//
//  MyAvViewController.m
//  AVPlayerDemo
//
//  Created by user on 15.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CWMovieViewController.h"
#import "RTAlertView.h"

/* Asset keys */
NSString * const kTracksKey         = @"tracks";
NSString * const kPlayableKey		= @"playable";

/* PlayerItem keys */
NSString * const kStatusKey         = @"status";

/* AVPlayer keys */
NSString * const kRateKey			= @"rate";
NSString * const kCurrentItemKey	= @"currentItem";

@interface CWMovieViewController ()
- (void)play:(id)sender;
- (void)pause:(id)sender;
- (void)initScrubberTimer;
- (void)showPlayButton;
- (void)showStopButton;
- (void)syncScrubber;
- (void)beginScrubbing:(id)sender;
- (void)scrub:(id)sender;
- (void)endScrubbing:(id)sender;
- (BOOL)isScrubbing;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
//- (id)init;
- (void)dealloc;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (void)viewDidLoad;
- (void)viewWillDisappear:(BOOL)animated;
- (void)handleSwipe:(UISwipeGestureRecognizer*)gestureRecognizer;
- (void)syncPlayPauseButtons;
@end

@interface CWMovieViewController (Player)
- (void)removePlayerTimeObserver;
- (CMTime)playerItemDuration;
- (BOOL)isPlaying;
- (void)playerItemDidReachEnd:(NSNotification *)notification ;
- (void)observeValueForKeyPath:(NSString*) path ofObject:(id)object change:(NSDictionary*)change context:(void*)context;
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys;
@end

static void *MyAvViewControllerRateObservationContext = &MyAvViewControllerRateObservationContext;
static void *MyAvViewControllerStatusObservationContext = &MyAvViewControllerStatusObservationContext;
static void *MyAvViewControllerCurrentItemObservationContext = &MyAvViewControllerCurrentItemObservationContext;

@implementation CWMovieViewController
@synthesize mPlayer,mPlayerItem,mPlaybackView,panelView, appleTVActive, fullscreen, shouldAutoplay;
#pragma mark Asset URL

- (void)setURL:(NSURL*)URL
{
	if (mURL != URL)
	{
		[mURL release];
		mURL = [URL copy];
		
        /*
         Create an asset for inspection of a resource referenced by a given URL.
         Load the values for the asset keys "tracks", "playable".
         */
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:mURL options:nil];
        
        NSArray *requestedKeys = @[kTracksKey, kPlayableKey];
        
        /* Tells the asset to load the values of any of the specified keys that are not already loaded. */
        [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{		 
             dispatch_async( dispatch_get_main_queue(), 
                            ^{
                                /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
                                [self prepareToPlayAsset:asset withKeys:requestedKeys];
                            });
         }];
	}
}

- (NSURL*)URL
{
	return mURL;
}

- (void)setFullscreen:(BOOL)fscreen {
    if (fullscreen == fscreen) {
        return;
    }
    fullscreen = !fscreen;
    [self changeFullscreenMode];
}

- (void)setAppleTVActive:(BOOL)TVActive {
    appleTVActive = TVActive;
    if (appleTVActive) {
        panelView.hidden = NO;
        panelView.fullscreenButton.hidden = YES;
        contentView.backgroundColor=[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
        /*
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(contentView.frame.size.width/2-imageView.frame.size.width/2, contentView.frame.size.height/2-imageView.frame.size.height/2, 60, 60)];
        imageView.image=[UIImage imageNamed:@"icon.png"];
        [contentView addSubview:imageView];
        UILabel *firstLabel=[[UILabel alloc]initWithFrame:CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y-imageView.frame.size.height-5, 60, 60)];
        [contentView addSubview:firstLabel];
        UILabel *secondLabel=[[UILabel alloc]initWithFrame:CGRectMake(firstLabel.frame.origin.x, firstLabel.frame.origin.y-firstLabel.frame.size.height-5, 60, 60)];
        [contentView addSubview:secondLabel];
         */
        
    }
    else {
        [mPlaybackView removeFromSuperview];
        mPlaybackView.frame = self.view.bounds;
        [contentView addSubview:mPlaybackView];
        [contentView bringSubviewToFront:panelView];
        [contentView bringSubviewToFront:fullscreenCenterView];
        [contentView bringSubviewToFront:fullscreenTopBar];
        panelView.fullscreenButton.hidden = NO;
    }
}

- (void)play {
    [self play:nil];
}

- (void)pause {
    [self pause:nil];
}

- (void)stop {
    [self pause];
    [self removePlayerTimeObserver];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark
#pragma mark Button Action Methods

- (void)play:(id)sender
{
	/* If we are at the end of the movie, we must seek to the beginning first 
     before starting playback. */
	if (YES == seekToZeroBeforePlay) 
	{
		seekToZeroBeforePlay = NO;
		[mPlayer seekToTime:kCMTimeZero];
      
	}
    
	[mPlayer play];
	
    [self showStopButton];
    currentIndex++;
    [self performSelector:@selector(hidePanelView:) withObject:
    @(currentIndex) afterDelay:6.0f];
       
}
-(void)hidePanelView:(NSNumber *)param{
    if ([param intValue]==currentIndex) {
        [self changeState];
    }
}
- (void)changeState {
    isHidden=!isHidden;
    [self showPanelView];
}

-(void)showPanelView{
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.3f];
    [animation setType:kCATransitionFade];
    [animation setValue:@"animation" forKey:@"showView"];
    
    if (isHidden) {
        if (!appleTVActive) {
            fullscreenTopBar.hidden=YES;
            [[fullscreenTopBar layer] addAnimation:animation forKey:@"showView"];
            
            panelView.hidden=YES;
            [[panelView layer] addAnimation:animation forKey:@"showView"];
            
            fullscreenCenterView.hidden=YES;
            [[fullscreenCenterView layer] addAnimation:animation forKey:@"showView"];
        }
                
    }
    else {
        if (fullscreen){
            
            panelView.hidden=YES;
            
            fullscreenTopBar.hidden=NO;
            [[fullscreenTopBar layer] addAnimation:animation forKey:@"showView"];
            
            fullscreenCenterView.hidden=NO;
            [[fullscreenCenterView layer] addAnimation:animation forKey:@"showView"];
            currentIndex++;
            [self performSelector:@selector(hidePanelView:) withObject:
             @(currentIndex) afterDelay:6.0f];
            
        }
        
        else {
            fullscreenTopBar.hidden=YES;
            fullscreenCenterView.hidden=YES;
            
            panelView.hidden=NO;
            [[panelView layer] addAnimation:animation forKey:@"showView"];
            
            currentIndex++;
            [self performSelector:@selector(hidePanelView:) withObject:
             @(currentIndex) afterDelay:6.0f];
        }
    }
}

- (void)pause:(id)sender
{
	[mPlayer pause];
    
    [self showPlayButton];
}
#pragma mark Play, Stop buttons

/* Show the stop button in the movie player controller. */
-(void)showStopButton
{
    panelView.stopButton.userInteractionEnabled=YES;
    panelView.stopButton.hidden=NO;
    panelView.playButton.userInteractionEnabled=NO;
    panelView.playButton.hidden=YES;
}

/* Show the play button in the movie player controller. */
-(void)showPlayButton
{
    panelView.stopButton.userInteractionEnabled=NO;
    panelView.stopButton.hidden=YES;
    panelView.playButton.userInteractionEnabled=YES;
    panelView.playButton.hidden=NO;
}

/* If the media is playing, show the stop button; otherwise, show the play button. */
- (void)syncPlayPauseButtons
{
	if ([self isPlaying])
	{
        [self showStopButton];
	}
	else
	{
        [self showPlayButton];        
	}
}

-(void)enablePlayerButtons
{
    panelView.playButton.enabled = YES;
    panelView.stopButton.enabled = YES;
}

-(void)disablePlayerButtons
{
    panelView.playButton.enabled = NO;
    panelView.stopButton.enabled = NO;
}

#pragma mark -
#pragma mark Movie scrubber control

/* ---------------------------------------------------------
 **  Methods to handle manipulation of the movie scrubber control
 ** ------------------------------------------------------- */

/* Requests invocation of a given block during media playback to update the movie scrubber control. */
-(void)initScrubberTimer
{
	double interval = .1f;	
	
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration)) 
	{
		return;
	} 
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		CGFloat width = CGRectGetWidth([panelView.slider bounds]);
		interval = 0.5f * duration / width;
       
       	}
    
	/* Update the scrubber during normal playback. */
	mTimeObserver = [[mPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC) 
                                                           queue:NULL /* If you pass NULL, the main queue is used. */
                                                      usingBlock:^(CMTime time) 
                      {
                          [self syncScrubber];
                      }] retain];
    
    double time = CMTimeGetSeconds([mPlayer currentTime]);
    int minutes = time/60;
    int seconds = time - minutes * 60;
    
    panelView.timePastLabel.text=[NSString stringWithFormat:@"%02i:%.02i",minutes,seconds];
    double remainTime=duration-time;
    int minutes2 = remainTime / 60;
    int seconds2 = remainTime-minutes2*60;
    panelView.timeRemaininglabel.text=[NSString stringWithFormat:@"-%02i:%02i",minutes2,seconds2];
    
}

/* Set the scrubber based on the player current time. */
- (void)syncScrubber
{
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration)) 
	{
		panelView.slider.minimumValue = 0.0;
		return;
	} 
    
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		float minValue = [panelView.slider minimumValue];
		float maxValue = [panelView.slider maximumValue];
		double time = CMTimeGetSeconds([mPlayer currentTime]);
		
		[panelView.slider setValue:(maxValue - minValue) * time / duration + minValue];
        
        int minutes = time/60;
        int seconds = time - minutes * 60;
        
        panelView.timePastLabel.text=[NSString stringWithFormat:@"%02i:%.02i",minutes,seconds];
        double remainTime=duration-time;
        int minutes2 = remainTime / 60;
        int seconds2 = remainTime-minutes2*60;
        panelView.timeRemaininglabel.text=[NSString stringWithFormat:@"-%02i:%02i",minutes2,seconds2];
	}
  
}

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (void)beginScrubbing:(id)sender
{
	mRestoreAfterScrubbingRate = [mPlayer rate];
	[mPlayer setRate:0.f];
	
	/* Remove previous timer. */
	[self removePlayerTimeObserver];
    }

/* Set the player current time to match the scrubber position. */
- (void)scrub:(id)sender
{
	if ([sender isKindOfClass:[UISlider class]])
	{
		UISlider* mSlider = sender;
		
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration)) {
			return;
		} 
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			float minValue = [mSlider minimumValue];
			float maxValue = [mSlider maximumValue];
			float value = [mSlider value];
			
			double time = duration * (value - minValue) / (maxValue - minValue);
			
			[mPlayer seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
            int minutes = time/60;
            int seconds = time - minutes * 60;
            
            panelView.timePastLabel.text=[NSString stringWithFormat:@"%02i:%.02i",minutes,seconds];
            double remainTime=duration-time;
            int minutes2 = remainTime / 60;
            int seconds2 = remainTime-minutes2*60;
            panelView.timeRemaininglabel.text=[NSString stringWithFormat:@"-%02i:%02i",minutes2,seconds2];
            currentIndex++;
            [self performSelector:@selector(hidePanelView:) withObject:
             @(currentIndex) afterDelay:6.0f];
            
		}
	}
    
}

/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (void)endScrubbing:(id)sender
{
	if (!mTimeObserver)
	{
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration)) 
		{
			return;
		} 
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			CGFloat width = CGRectGetWidth([panelView.slider bounds]);
			double tolerance = 0.5f * duration / width;
            
			mTimeObserver = [[mPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:NULL usingBlock:
                              ^(CMTime time)
                              {
                                  [self syncScrubber];
                              }] retain];
		}
	}
    
	if (mRestoreAfterScrubbingRate)
	{
		[mPlayer setRate:mRestoreAfterScrubbingRate];
		mRestoreAfterScrubbingRate = 0.f;
	}
}
- (BOOL)isScrubbing
{
	return mRestoreAfterScrubbingRate != 0.f;
}

-(void)enableScrubber
{
    panelView.slider.enabled = YES;
}

-(void)disableScrubber
{
    panelView.slider.enabled = NO;    
}

////////////////////////////////////////////////////////////////////////
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
	{
		mPlayer = nil;		
		fullscreen = NO;
        shouldAutoplay = NO;
//		[self setWantsFullScreenLayout:YES];
	}
	
	return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    mPlayer = nil;
	//UIView* view  = [self view];
    
    self.view.autoresizesSubviews = YES;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    contentView.backgroundColor = [UIColor blackColor];
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentView.autoresizesSubviews = YES;
    [self.view addSubview:contentView];
    
    
//	UISwipeGestureRecognizer* swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//    //self.view.frame=CGRectMake(0,0,155,30);
//    //self.view.backgroundColor=[UIColor redColor];
//	[swipeUpRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
//	[self.view addGestureRecognizer:swipeUpRecognizer];
//	[swipeUpRecognizer release];
//	
//	UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//	[swipeDownRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
//	[self.view addGestureRecognizer:swipeDownRecognizer];
//	[swipeDownRecognizer release];
    
    mPlaybackView=[[AVPlayerDemoPlaybackView alloc]initWithFrame:CGRectMake(0,0,contentView.frame.size.width, contentView.frame.size.height)];
    mPlaybackView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    mPlaybackView.backgroundColor=[UIColor blackColor];
    mPlaybackView.contentMode = UIViewContentModeScaleAspectFit;
    [contentView addSubview:mPlaybackView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeState)];
    tapGesture.numberOfTapsRequired = 1;
    [mPlaybackView addGestureRecognizer:tapGesture];
    [tapGesture release];

    const int panelViewHeight = 36;
    panelView = [[PanelView alloc] init];
    panelView.frame = CGRectMake(0,self.view.frame.size.height-panelViewHeight,self.view.frame.size.width,panelViewHeight);
    panelView.backgroundColor=[UIColor clearColor];
    panelView.hidden=NO;
    panelView.autoresizingMask=UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [contentView addSubview:panelView];
    
    [panelView.stopButton addTarget:self  action:@selector(pause:)forControlEvents:UIControlEventTouchUpInside]; 
    [panelView.playButton addTarget:self  action:@selector(play:)forControlEvents:UIControlEventTouchUpInside]; 
    panelView.stopButton.showsTouchWhenHighlighted = YES;
    panelView.playButton.showsTouchWhenHighlighted = YES;
    
    [panelView.slider addTarget:self action:@selector(endScrubbing:)forControlEvents:UIControlEventTouchCancel];
    [panelView.slider addTarget:self action:@selector(beginScrubbing:)forControlEvents: UIControlEventTouchDown];
    [panelView.slider addTarget:self action:@selector(scrub:)forControlEvents: UIControlEventTouchDragInside];
    [panelView.slider addTarget:self action:@selector(endScrubbing:)forControlEvents: UIControlEventTouchUpInside];
    [panelView.slider addTarget:self action:@selector(endScrubbing:)forControlEvents: UIControlEventTouchUpOutside];
    [panelView.slider addTarget:self action:@selector(scrub:)forControlEvents: UIControlEventValueChanged];

    [panelView.fullscreenButton addTarget:self action:@selector(fullscreenButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    fullscreenTopBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, 44)];
    fullscreenTopBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    fullscreenTopBar.barStyle = UIBarStyleBlackTranslucent;
    fullscreenTopBar.hidden = YES;
    
    [contentView addSubview:fullscreenTopBar];
    
    const int fullscreenCenterViewWidth = 395, fullscreenCenterViewHeight = 105;
    fullscreenCenterView = [[UIView alloc] initWithFrame:CGRectMake((contentView.frame.size.width - fullscreenCenterViewWidth)/2, 
                                                                    contentView.frame.size.height - 2*fullscreenCenterViewHeight, 
                                                                    fullscreenCenterViewWidth, fullscreenCenterViewHeight)];
    fullscreenCenterView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | 
    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    fullscreenCenterView.backgroundColor = [UIColor clearColor];
    fullscreenCenterView.hidden = YES;
    [contentView addSubview:fullscreenCenterView];
    
    UIImageView *im = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, fullscreenCenterViewWidth, fullscreenCenterViewHeight)];
    im.image = [UIImage imageNamed:@"fullscreenBack.png"];
    [fullscreenCenterView addSubview:im];
    [im release];
    
    volume = [[MPVolumeView alloc] init];
    volume.backgroundColor = [UIColor clearColor];
    volume.showsVolumeSlider = YES;
    volume.showsRouteButton = NO;
    
    int volumeWidth = (int)(fullscreenCenterView.frame.size.width*0.7);
    int volumeHeight = 20;
    volume.frame = CGRectMake((fullscreenCenterView.frame.size.width - volumeWidth)/2, 
                              fullscreenCenterView.frame.size.height - volumeHeight - 20, 
                              volumeWidth, volumeHeight);
    [fullscreenCenterView addSubview:volume];
    currentIndex=0;
    isHidden=NO;
    
   // mDuration = mPlayerItem.duration; 
   // mCurrentTime = mPlayerItem.currentTime;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[mPlayer pause];
	
	[super viewWillDisappear:animated];
}

//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    panelView.frame = CGRectMake(0, self.view.frame.size.height - panelView.frame.size.height, panelView.frame.size.width, panelView.frame.size.height);
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)barDoneClicked:(id)sender {
    [self changeFullscreenMode];
}

-(void)fullscreenButtonClick:(id)sender {
    [self changeFullscreenMode];
}

- (void)changeFullscreenMode {
    NSLog(@"Fullscreen");
    
    if (_supportsPseudoFullScreen) {
        
        if (_pseudoFullScreen) {
            _pseudoFullScreen = NO;
            panelView.pseudoFullScreen = NO;
        }
        else {
            _pseudoFullScreen = YES;
            panelView.pseudoFullScreen = YES;
        }
        
        [panelView setNeedsLayout];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CWMoviePlayerFulscreenChangedNotification object:self];
        
        return;
    }
    
    if (fullscreen) {
        fullscreen = NO;
        
        // Configuring top toolbar
        panelView.isFullscreen = NO;
        [panelView.sliderWithLabelsView removeFromSuperview];
        [panelView.playButton removeFromSuperview];
        [panelView.stopButton removeFromSuperview];
        [panelView.fullscreenButton removeFromSuperview];
        [panelView addSubview:panelView.sliderWithLabelsView];
        [panelView addSubview:panelView.stopButton];
        [panelView addSubview:panelView.playButton];
        [panelView addSubview:panelView.fullscreenButton];
        
        
        panelView.hidden = NO;
        fullscreenTopBar.hidden = YES;
        fullscreenCenterView.hidden = YES;
        
        // Making contentView transorm back to init (because MainWindow is always in portrait orientation)
        contentView.transform = initialTransform;
        [contentView removeFromSuperview];
        contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:contentView];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIApplicationDidChangeStatusBarOrientationNotification 
                                                      object:nil];
        //[[UIApplication sharedApplication] setStatusBarStyle:initialStatusBarStyle];
    }
    else {
        //initialStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        // Configuring top toolbar
        panelView.isFullscreen = YES;
        [panelView.sliderWithLabelsView removeFromSuperview];
        [panelView.playButton removeFromSuperview];
        [panelView.stopButton removeFromSuperview];
        [panelView.fullscreenButton removeFromSuperview];
        
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(barDoneClicked:)];
        panelView.sliderWithLabelsView.frame = CGRectMake(panelView.sliderWithLabelsView.frame.origin.x, 0, 
                                                          panelView.sliderWithLabelsView.frame.size.width, fullscreenTopBar.frame.size.height);
        UIView *customView = [[UIView alloc] initWithFrame:panelView.sliderWithLabelsView.frame];
        customView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [customView addSubview:panelView.sliderWithLabelsView];
        UIBarButtonItem *viewItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
        UIBarButtonItem *fixedSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedSpaceItem.width = 100;
        
        [fullscreenTopBar setItems:@[done, viewItem, fixedSpaceItem] animated:NO];
        [done release];
        [viewItem release];
        [fixedSpaceItem release];
        [customView release];
        
        panelView.hidden = YES;
        fullscreenTopBar.hidden = NO;
        fullscreenCenterView.hidden = NO;
        
        panelView.playButton.frame = CGRectMake((fullscreenCenterView.frame.size.width - panelView.playButton.frame.size.width)/2, 
                                                18, 
                                                panelView.playButton.frame.size.width, panelView.playButton.frame.size.height);
        panelView.stopButton.frame = panelView.playButton.frame;
        panelView.fullscreenButton.frame = CGRectMake(fullscreenCenterView.frame.size.width - panelView.fullscreenButton.frame.size.width - 30, 
                                                      22, 
                                                      panelView.fullscreenButton.frame.size.width, panelView.fullscreenButton.frame.size.height);
        [fullscreenCenterView addSubview:panelView.playButton];
        [fullscreenCenterView addSubview:panelView.stopButton];
        [fullscreenCenterView addSubview:panelView.fullscreenButton];
        
        // Making contentView transorm (because MainWindow is always in portrait orientation)
        initialTransform = contentView.transform;
        [contentView removeFromSuperview];
        UIWindow *mainWindow = [UIApplication sharedApplication].windows[0];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(flipViewAccordingToStatusBarOrientation:) 
                                                     name:UIApplicationDidChangeStatusBarOrientationNotification 
                                                   object:nil];  
        [self flipViewAccordingToStatusBarOrientation:nil]; // upate view
        [mainWindow addSubview:contentView];
        
        fullscreen = YES;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CWMoviePlayerFulscreenChangedNotification object:self];
}

-(UIStatusBarStyle)prefersStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidded{
    return YES;
}

- (void)flipViewAccordingToStatusBarOrientation:(NSNotification *)note {
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    CGFloat angle = 0.0;
    CGRect newFrame = self.view.window.bounds;
    CGSize statusBarSize = [[UIApplication sharedApplication] statusBarFrame].size;
    
    switch (orientation) { 
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI; 
            newFrame.size.height -= statusBarSize.height;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = - M_PI / 2.0f;
            newFrame.origin.x += statusBarSize.width;
            newFrame.size.width -= statusBarSize.width; 
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI / 2.0f;
            newFrame.size.width -= statusBarSize.width;
            break;
        default: // as UIInterfaceOrientationPortrait
            angle = 0.0;
            newFrame.origin.y += statusBarSize.height;
            newFrame.size.height -= statusBarSize.height;
            break;
    } 

    contentView.transform = CGAffineTransformMakeRotation(angle);
    contentView.frame = newFrame;
    
//    if (!note) {
//        contentView.frame = CGRectMake(newFrame.origin.x, newFrame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
//        [UIView animateWithDuration:0.5 animations:^{
//            contentView.frame = newFrame;
//        }];
//    }
//    else {
//        contentView.frame = newFrame;
//    }
}

-(void)setViewDisplayName
{
    /* Set the view title to the last component of the asset URL. */
    self.title = [mURL lastPathComponent];
    
    /* Or if the item has a AVMetadataCommonKeyTitle metadata, use that instead. */
	for (AVMetadataItem* item in ([[[mPlayer currentItem] asset] commonMetadata]))
	{
		NSString* commonKey = [item commonKey];
		
		if ([commonKey isEqualToString:AVMetadataCommonKeyTitle])
		{
			self.title = [item stringValue];
		}
	}
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)gestureRecognizer
{
	UIView* view = [self view];
	UISwipeGestureRecognizerDirection direction = [gestureRecognizer direction];
	CGPoint location = [gestureRecognizer locationInView:view];
	
	if (location.y < CGRectGetMidY([view bounds]))
	{
		if (direction == UISwipeGestureRecognizerDirectionUp)
		{
			[UIView animateWithDuration:0.2f animations:
             ^{
                 [[self navigationController] setNavigationBarHidden:YES animated:YES];
             } completion:
             ^(BOOL finished)
             {
                 //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
             }];
		}
		if (direction == UISwipeGestureRecognizerDirectionDown)
		{
			[UIView animateWithDuration:0.2f animations:
             ^{
                 //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
             } completion:
             ^(BOOL finished)
             {
                 [[self navigationController] setNavigationBarHidden:NO animated:YES];
             }];
		}
	}
	else
	{
		if (direction == UISwipeGestureRecognizerDirectionDown)
		{
            if (![panelView isHidden])
			{
				[UIView animateWithDuration:0.2f animations:
                 ^{
                     [panelView setTransform:CGAffineTransformMakeTranslation(0.f, CGRectGetHeight([panelView bounds]))];
                 } completion:
                 ^(BOOL finished)
                 {
                     [panelView setHidden:YES];
                 }];
			}
		}
		else if (direction == UISwipeGestureRecognizerDirectionUp)
		{
            if ([panelView isHidden])
			{
				[panelView setHidden:NO];
				
				[UIView animateWithDuration:0.2f animations:
                 ^{
                     [panelView setTransform:CGAffineTransformIdentity];
                 } completion:^(BOOL finished){}];
			}
		}
	}
}
- (void)dealloc
{
	[self removePlayerTimeObserver];
	
	[mPlayer removeObserver:self forKeyPath:@"rate"];
    [mPlayer removeObserver:self forKeyPath:@"currentItem"];
	[mPlayer.currentItem removeObserver:self forKeyPath:@"status"];
	
	[mPlayer pause];
	[mPlayer release];
	
	[mURL release];
    
    [contentView release];
    [mPlaybackView release];
    [panelView release];
    [fullscreenTopBar release];
    [fullscreenCenterView release];
    [volume release];

    [mPlayerItem release];
    [super dealloc];
}

@end

@implementation CWMovieViewController (Player)

#pragma mark Player Item

- (BOOL)isPlaying
{
return mRestoreAfterScrubbingRate != 0.f || [mPlayer rate] != 0.f;
}

/* Called when the player item has played to its end time. */
- (void)playerItemDidReachEnd:(NSNotification *)notification 
{
	/* After the movie has played to its end time, seek back to time zero 
     to play it again. */
	seekToZeroBeforePlay = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CWMoviePlayerPlaybackDidFinishNotification object:self];
}

/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem. 
 ** ------------------------------------------------------- */

- (CMTime)playerItemDuration
{
	AVPlayerItem *playerItem = [mPlayer currentItem];
	if (playerItem.status == AVPlayerItemStatusReadyToPlay)
	{
        /* 
         NOTE:
         Because of the dynamic nature of HTTP Live Streaming Media, the best practice 
         for obtaining the duration of an AVPlayerItem object has changed in iOS 4.3. 
         Prior to iOS 4.3, you would obtain the duration of a player item by fetching 
         the value of the duration property of its associated AVAsset object. However, 
         note that for HTTP Live Streaming Media the duration of a player item during 
         any particular playback session may differ from the duration of its asset. For 
         this reason a new key-value observable duration property has been defined on 
         AVPlayerItem.
         
         See the AV Foundation Release Notes for iOS 4.3 for more information.
         */		
        
		return([playerItem duration]);
	}
	
	return(kCMTimeInvalid);
}


/* Cancels the previously registered time observer. */
-(void)removePlayerTimeObserver
{
	if (mTimeObserver)
	{
		[mPlayer removeTimeObserver:mTimeObserver];
		[mTimeObserver release];
		mTimeObserver = nil;
	}
}

#pragma mark -
#pragma mark Loading the Asset Keys Asynchronously

#pragma mark -
#pragma mark Error Handling - Preparing Assets for Playback Failed

/* --------------------------------------------------------------
 **  Called when an asset fails to prepare for playback for any of
 **  the following reasons:
 ** 
 **  1) values of asset keys did not load successfully, 
 **  2) the asset keys did load successfully, but the asset is not 
 **     playable
 **  3) the item did not become ready to play. 
 ** ----------------------------------------------------------- */

-(void)assetFailedToPrepareForPlayback:(NSError *)error
{
    [self removePlayerTimeObserver];
    [self syncScrubber];
    [self disableScrubber];
    [self disablePlayerButtons];
    
    /* Display the error. */
	RTAlertView *alertView = [[RTAlertView alloc] initWithTitle:[error localizedDescription]
														message:[error localizedFailureReason]
													   delegate:nil
											  cancelButtonTitle:NSLocalizedString(@"OKBTN_TITLE", nil)
											  otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}


#pragma mark Prepare to play asset, URL

/*
 Invoked at the completion of the loading of the values for all keys on the asset that we require.
 Checks whether loading was successfull and whether the asset is playable.
 If so, sets up an AVPlayerItem and an AVPlayer to play the asset.
 */
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
    /* Make sure that the value of each key has loaded successfully. */
	for (NSString *thisKey in requestedKeys)
	{
		NSError *error = nil;
		AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
		if (keyStatus == AVKeyValueStatusFailed)
		{
			[self assetFailedToPrepareForPlayback:error];
			return;
		}
		/* If you are also implementing -[AVAsset cancelLoading], add your code here to bail out properly in the case of cancellation. */
	}
    
    /* Use the AVAsset playable property to detect whether the asset can be played. */
    if (!asset.playable) 
    {
        /* Generate an error describing the failure. */
		NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
		NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
		NSDictionary *errorDict = @{NSLocalizedDescriptionKey: localizedDescription, 
								   NSLocalizedFailureReasonErrorKey: localizedFailureReason};
		NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];
        
        /* Display the error to the user. */
        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];
        
        return;
    }
	
	/* At this point we're ready to set up for playback of the asset. */
    
    /* Stop observing our prior AVPlayerItem, if we have one. */
    if (self.mPlayerItem)
    {
        /* Remove existing player item key value observers and notifications. */
        
        [self.mPlayerItem removeObserver:self forKeyPath:kStatusKey];            
		
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.mPlayerItem];
    }
	
    /* Create a new instance of AVPlayerItem from the now successfully loaded AVAsset. */
    self.mPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    /* Observe the player item "status" key to determine when it is ready to play. */
    [self.mPlayerItem addObserver:self 
                       forKeyPath:kStatusKey 
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:MyAvViewControllerStatusObservationContext];
	
    /* When the player item has played to its end time we'll toggle
     the movie controller Pause button to be the Play button */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.mPlayerItem];
	
    seekToZeroBeforePlay = NO;
	
    /* Create new player, if we don't already have one. */
    if (![self player])
    {
        /* Get a new AVPlayer initialized to play the specified player item. */
        [self setPlayer:[AVPlayer playerWithPlayerItem:self.mPlayerItem]];	
        //        self.player.usesAirPlayVideoWhileAirPlayScreenIsActive = YES;
		
        /* Observe the AVPlayer "currentItem" property to find out when any 
         AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did 
         occur.*/
        [self.player addObserver:self 
                      forKeyPath:kCurrentItemKey 
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:MyAvViewControllerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.player addObserver:self 
                      forKeyPath:kRateKey 
                         options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                         context:MyAvViewControllerRateObservationContext];
    }
    
    /* Make our new AVPlayerItem the AVPlayer's current item. */
    if (self.player.currentItem != self.mPlayerItem)
    {
        /* Replace the player item with a new player item. The item replacement occurs 
         asynchronously; observe the currentItem property to find out when the 
         replacement will/did occur*/
        [[self player] replaceCurrentItemWithPlayerItem:self.mPlayerItem];
        
        [self syncPlayPauseButtons];
    }
	
    [panelView.slider setValue:0.0];
}

#pragma mark -
#pragma mark Asset Key Value Observing
#pragma mark

#pragma mark Key Value Observer for player rate, currentItem, player item status

/* ---------------------------------------------------------
 **  Called when the value at the specified key path relative
 **  to the given object has changed. 
 **  Adjust the movie play and pause button controls when the 
 **  player item "status" value changes. Update the movie 
 **  scrubber control when the player item is ready to play.
 **  Adjust the movie scrubber control when the player item 
 **  "rate" value changes. For updates of the player
 **  "currentItem" property, set the AVPlayer for which the 
 **  player layer displays visual output.
 **  NOTE: this method is invoked on the main queue.
 ** ------------------------------------------------------- */

- (void)observeValueForKeyPath:(NSString*) path 
                      ofObject:(id)object 
                        change:(NSDictionary*)change 
                       context:(void*)context
{
	/* AVPlayerItem "status" property value observer. */
	if (context == MyAvViewControllerStatusObservationContext)
	{
		[self syncPlayPauseButtons];
        
        AVPlayerStatus status = [change[NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
                /* Indicates that the status of the player is not yet known because 
                 it has not tried to load new media resources for playback */
            case AVPlayerStatusUnknown:
            {
                [self removePlayerTimeObserver];
                [self syncScrubber];
                
                [self disableScrubber];
                [self disablePlayerButtons];
            }
                break;
                
            case AVPlayerStatusReadyToPlay:
            {
                /* Once the AVPlayerItem becomes ready to play, i.e. 
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 its duration can be fetched from the item. */
                
                [self initScrubberTimer];
                
                [self enableScrubber];
                [self enablePlayerButtons];
                if (self.shouldAutoplay) {
                    [self performSelector:@selector(play) withObject:nil afterDelay:1];
                }
            }
                break;
                
            case AVPlayerStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];
            }
                break;
        }
	}
	/* AVPlayer "rate" property value observer. */
	else if (context == MyAvViewControllerRateObservationContext)
	{
        [self syncPlayPauseButtons];
	}
	/* AVPlayer "currentItem" property observer. 
     Called when the AVPlayer replaceCurrentItemWithPlayerItem: 
     replacement will/did occur. */
	else if (context == MyAvViewControllerCurrentItemObservationContext)
	{
        AVPlayerItem *newPlayerItem = change[NSKeyValueChangeNewKey];
        
        /* Is the new player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            [self disablePlayerButtons];
            [self disableScrubber];
        }
        else /* Replacement of player currentItem has occurred */
        {
            /* Set the AVPlayer for which the player layer displays visual output. */
            [mPlaybackView setPlayer:mPlayer];
            
            [self setViewDisplayName];
            
            /* Specifies that the player should preserve the video’s aspect ratio and 
             fit the video within the layer’s bounds. */
            [mPlaybackView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            
            [self syncPlayPauseButtons];
        }
	}
	else
	{
		[super observeValueForKeyPath:path ofObject:object change:change context:context];
	}
}

@end


@implementation PanelView

@synthesize slider;
@synthesize timePastLabel;
@synthesize timeRemaininglabel;
@synthesize playButton;
@synthesize stopButton;
@synthesize fullscreenButton;

@synthesize sliderWithLabelsView;
@synthesize isFullscreen;

- (id)init {
    self = [super init];
    if (self) {
        self.autoresizesSubviews = YES;
        isFullscreen = NO;
        
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        backView.backgroundColor = [UIColor blackColor];
        backView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        backView.alpha = 0.6f;
        [self addSubview:backView];
        [backView release];
        
        stopButton=[[PauseButton alloc]init];
        stopButton.backgroundColor=[UIColor clearColor];
        stopButton.hidden = YES;
//        UIImage *stopImage=[UIImage imageNamed:@"Pause.png"];
//        [stopButton setImage:stopImage forState:UIControlStateNormal];
        stopButton.showsTouchWhenHighlighted = YES;
        [self addSubview:stopButton];
        
        playButton=[[PlayButton alloc]init];
//        playButton.backgroundColor=[UIColor clearColor];
//        UIImage *image=[UIImage imageNamed:@"Play.png"];
//        [playButton setImage:image forState:UIControlStateNormal];
        playButton.showsTouchWhenHighlighted = YES;
        [self addSubview:playButton];
        
        
        sliderWithLabelsView = [[UIView alloc] init];
        sliderWithLabelsView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        sliderWithLabelsView.backgroundColor = [UIColor clearColor];
        [self addSubview:sliderWithLabelsView];
        
        timePastLabel=[[UILabel alloc]init];
        timePastLabel.text=@"";
        timePastLabel.font = [UIFont boldSystemFontOfSize:12.0];
        timePastLabel.backgroundColor=[UIColor clearColor];
        timePastLabel.textColor=[UIColor whiteColor];
        [sliderWithLabelsView addSubview:timePastLabel];
        
        slider= [[UISlider alloc] init];
        slider.backgroundColor = [UIColor clearColor];	
        UIImage *stetchLeftTrack = [[UIImage imageNamed:@"playbar_active.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
        UIImage *stetchRightTrack = [[UIImage imageNamed:@"playbar_passive.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
        UIImage *thumb = [UIImage imageNamed:@"playbar_round_bt.png"];
        [slider setThumbImage:thumb forState:UIControlStateNormal];
        [slider setMinimumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
        [slider setMaximumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        [sliderWithLabelsView addSubview:slider];
        
        timeRemaininglabel=[[UILabel alloc]init];
        timeRemaininglabel.text=@"";
        timeRemaininglabel.font = [UIFont boldSystemFontOfSize:12.0];
        timeRemaininglabel.backgroundColor=[UIColor  clearColor];
        timeRemaininglabel.textColor=[UIColor whiteColor];
        [sliderWithLabelsView addSubview:timeRemaininglabel];
        
        fullscreenButton = [[FullScreenButton alloc] init];
        fullscreenButton.showsTouchWhenHighlighted = YES;
        [self addSubview:fullscreenButton];
    
//        [self addSubview:volume];
    }
    
    return self;
}

- (void)dealloc {
    [slider release];
    [timePastLabel release];
    [timeRemaininglabel release];
    [playButton release];
    [stopButton release];
    [fullscreenButton release];
    
    [sliderWithLabelsView release];
    
    [super dealloc];
}

- (void)layoutSubviews {
    
    const int timeLabelsWidth = 50, volumeViewWidth = 0, fullscreenButtonSize = 16, playButtonSize = 17;
    int x = 20;
    const int offset = 10;
    
    int sliderWithLabelsWidth = 0;
    
    if (isFullscreen) {
        sliderWithLabelsWidth = sliderWithLabelsView.superview.frame.size.width - 2*offset - 2*timeLabelsWidth;
        sliderWithLabelsView.frame = CGRectMake(sliderWithLabelsView.frame.origin.x, 0, sliderWithLabelsWidth, sliderWithLabelsView.superview.frame.size.height);
        
        timePastLabel.frame = CGRectMake(0, 0 ,timeLabelsWidth, sliderWithLabelsView.frame.size.height);
        slider.frame = CGRectMake(timePastLabel.frame.origin.x + timePastLabel.frame.size.width + offset,
                                  0,
                                  sliderWithLabelsWidth - 2*timeLabelsWidth - 2*offset,
                                  sliderWithLabelsView.frame.size.height);
        timeRemaininglabel.frame = CGRectMake(slider.frame.origin.x + slider.frame.size.width + offset, 0 , timeLabelsWidth, sliderWithLabelsView.frame.size.height);
        
        int size = 30;
        stopButton.frame = CGRectMake(stopButton.frame.origin.x, stopButton.frame.origin.y, size, size);
        playButton.frame = CGRectMake(playButton.frame.origin.x, playButton.frame.origin.y, size, size);
        stopButton.imageSize = playButton.imageSize = size;
        fullscreenButton.frame = CGRectMake(fullscreenButton.frame.origin.x, fullscreenButton.frame.origin.y, 22, 22);
        fullscreenButton.enterFullscreenMode = NO;
        [stopButton setNeedsDisplay];
        [playButton setNeedsDisplay];
        [fullscreenButton setNeedsDisplay];
    }
    else {
        
        
        //    int minWidth = x + playButtonWidth + 2*timeLabelsWidth + volumeViewWidth + fullscreenButtonWidht + 100 + 6*offset;
        //    if (self.frame.size.width < minWidth) {
        //        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, minWidth, self.frame.size.height);
        //    }

        playButton.imageSize = stopButton.imageSize = playButtonSize;
        stopButton.frame = CGRectMake(x, 0, self.frame.size.height, self.frame.size.height);
        playButton.frame = CGRectMake(stopButton.frame.origin.x,stopButton.frame.origin.y,stopButton.frame.size.width,stopButton.frame.size.height);
        x += stopButton.frame.size.width + offset;
        
        sliderWithLabelsWidth = self.frame.size.width-x-4*offset - volumeViewWidth - fullscreenButtonSize + 2*offset;
        sliderWithLabelsView.frame = CGRectMake(x, 0, sliderWithLabelsWidth, self.frame.size.height);
        x += sliderWithLabelsWidth + offset;
        
        timePastLabel.frame = CGRectMake(0, 0 ,timeLabelsWidth, self.frame.size.height);
        slider.frame = CGRectMake(timePastLabel.frame.origin.x + timePastLabel.frame.size.width + offset,
                                  0,
                                  sliderWithLabelsWidth - 2*timeLabelsWidth - 2*offset,
                                  self.frame.size.height);
        timeRemaininglabel.frame = CGRectMake(slider.frame.origin.x + slider.frame.size.width + offset, 0 , timeLabelsWidth, self.frame.size.height);
        
        fullscreenButton.enterFullscreenMode = !_pseudoFullScreen;
        fullscreenButton.frame = CGRectMake(x, (self.frame.size.height - fullscreenButtonSize)/2, fullscreenButtonSize, fullscreenButtonSize);
        [fullscreenButton setNeedsDisplay];
        [stopButton setNeedsDisplay];
        [playButton setNeedsDisplay];
    }
}

- (void)setIsFullscreen:(BOOL)fullscreen {
    isFullscreen = fullscreen;
    if (isFullscreen) {
        
    }
    else {
        
    }
}

@end

/* ---------------------------------------------------------
 **  To play the visual component of an asset, you need a view 
 **  containing an AVPlayerLayer layer to which the output of an 
 **  AVPlayer object can be directed. You can create a simple 
 **  subclass of UIView to accommodate this. Use the view’s Core 
 **  Animation layer (see the 'layer' property) for rendering.  
 **  This class, AVPlayerDemoPlaybackView, is a subclass of UIView  
 **  that is used for this purpose.
 ** ------------------------------------------------------- */


@implementation AVPlayerDemoPlaybackView

+ (Class)layerClass
{
	return [AVPlayerLayer class];
}

- (AVPlayer*)player
{
	return [(AVPlayerLayer*)[self layer] player];
}

- (void)setPlayer:(AVPlayer*)player
{
	[(AVPlayerLayer*)[self layer] setPlayer:player];
    
}

/* Specifies how the video is displayed within a player layer’s bounds. 
 (AVLayerVideoGravityResizeAspect is default) */
- (void)setVideoFillMode:(NSString *)fillMode
{
	AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self layer];
  	playerLayer.videoGravity = fillMode;
}

@end


@implementation ControlButton

@synthesize imageSize;

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        imageSize = 0;
    }
    return self;
}

@end



@implementation PlayButton

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (-0.2f,  -0.7f);
    CGContextSaveGState(context);
    CGContextSetShadowWithColor (context, myShadowOffset, 0.0f, [UIColor blackColor].CGColor);
    
    int offset = 1;
    CGRect workRect = self.bounds;
    if (imageSize > 0 && imageSize < self.bounds.size.width && imageSize < self.bounds.size.height) {
        workRect = CGRectMake((self.bounds.size.width - imageSize)/2, (self.bounds.size.height - imageSize)/2, imageSize, imageSize);
    }
    int x = workRect.origin.x, y = workRect.origin.y;
    CGContextBeginPath(context);
    CGContextMoveToPoint   (context, x + offset, y + offset);  // top left
    CGContextAddLineToPoint(context, CGRectGetMaxX(workRect), CGRectGetMidY(workRect) + offset/2);  // mid right
    CGContextAddLineToPoint(context, x + offset, CGRectGetMaxY(workRect));  // bottom left
    CGContextClosePath(context);
    
    if (self.enabled) {
         CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    }
    else {
         CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    }
   
    CGContextFillPath(context);
}

@end

@implementation PauseButton

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect workRect = self.bounds;
    if (imageSize > 0 && imageSize < self.bounds.size.width && imageSize < self.bounds.size.height) {
        workRect = CGRectMake((self.bounds.size.width - imageSize)/2, (self.bounds.size.height - imageSize)/2, imageSize, imageSize);
    }
    int offset = 2;
    int height = workRect.size.height - offset, width = workRect.size.width*0.25;
    int x = workRect.origin.x + (workRect.size.width - width*3)/2, y = workRect.origin.y + workRect.size.height - height;
    
    CGSize          myShadowOffset = CGSizeMake (0,  -0.5f);// 2
    
    CGContextSaveGState(context);
    CGContextSetShadowWithColor (context, myShadowOffset, 0.0f, [UIColor blackColor].CGColor);
    
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, [UIColor clearColor].CGColor);
    CGRect rectangle = CGRectMake(x, y, width, height);
    CGContextAddRect(context, rectangle);
    CGContextStrokePath(context);
    if (self.enabled) {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    }
    else {
        CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    }
    CGContextFillRect(context, rectangle);
    
    x += 2*width;
    rectangle = CGRectMake(x, y, width, height);
    CGContextAddRect(context, rectangle);
    CGContextStrokePath(context);
    if (self.enabled) {
        CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    }
    else {
        CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
    }
    CGContextFillRect(context, rectangle);
}

@end


@implementation FullScreenButton

@synthesize enterFullscreenMode;

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        enterFullscreenMode = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    int offset = 1;
    int sideSize = 7;
    CGRect workRect = CGRectMake(offset, offset, self.frame.size.width - 2*offset, self.frame.size.height - 2*offset);
    
    if (enterFullscreenMode) {
        // draw lines
        CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextSetLineWidth(context, 2.0);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, workRect.origin.x+offset, workRect.origin.y+offset);
        CGContextAddLineToPoint(context, sideSize, sideSize);
        CGContextStrokePath(context);
        
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, workRect.origin.x + workRect.size.width - offset, workRect.origin.y + workRect.size.height - offset);
        CGContextAddLineToPoint(context, workRect.origin.x + workRect.size.width - sideSize+1, workRect.origin.y + workRect.size.height - sideSize+1);
        CGContextStrokePath(context);
        
        
        // draw triangles
        CGContextBeginPath(context);
        CGContextMoveToPoint   (context, workRect.origin.x, workRect.origin.y);
        CGContextAddLineToPoint(context, workRect.origin.x + sideSize, workRect.origin.y);
        CGContextAddLineToPoint(context, workRect.origin.x, workRect.origin.y + sideSize);
        CGContextClosePath(context);
        
        if (self.enabled) {
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        }
        else {
            CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
        }
        CGContextFillPath(context);
        
        CGContextBeginPath(context);
        CGContextMoveToPoint   (context, workRect.origin.x + workRect.size.width, workRect.origin.y + workRect.size.height);
        CGContextAddLineToPoint(context, workRect.origin.x + workRect.size.width - sideSize, workRect.origin.y + workRect.size.height);
        CGContextAddLineToPoint(context, workRect.origin.x + workRect.size.width, workRect.origin.y + workRect.size.height - sideSize);
        CGContextClosePath(context);
        
        if (self.enabled) {
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        }
        else {
            CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
        }
        CGContextFillPath(context);
    }
    else {
        CGSize          myShadowOffset = CGSizeMake (-0.2f,  -0.7f);
        CGContextSaveGState(context);
        CGContextSetShadowWithColor (context, myShadowOffset, 0.0f, [UIColor blackColor].CGColor);
        
        int helpRectSize = workRect.size.width/2 - 1;
        int x = workRect.origin.x, y = workRect.origin.y, width = workRect.size.width, height = workRect.size.height;
        
        // draw top arrow
        CGContextBeginPath(context);
        CGContextMoveToPoint   (context, x + helpRectSize, y + helpRectSize);
        CGContextAddLineToPoint(context, x + helpRectSize, y + helpRectSize - sideSize);
        CGContextAddLineToPoint(context, x + helpRectSize - sideSize, y + helpRectSize);
        CGContextClosePath(context);
        
        if (self.enabled) {
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        }
        else {
            CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
        }
        CGContextFillPath(context);
        
        CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
        CGContextSetLineWidth(context, 2.0);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x, y);
        CGContextAddLineToPoint(context, x + helpRectSize - 3, y + helpRectSize - 3);
        CGContextStrokePath(context);
        
        // draw bottom arrow
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x + width, y + height);
        CGContextAddLineToPoint(context, x + width - helpRectSize + offset, y + height - helpRectSize + offset);
        CGContextStrokePath(context);
    
        CGContextBeginPath(context);
        CGContextMoveToPoint   (context, x + width - helpRectSize, y + height - helpRectSize);
        CGContextAddLineToPoint(context, x + width - helpRectSize + sideSize, y + height - helpRectSize);
        CGContextAddLineToPoint(context, x + width - helpRectSize, y + height - helpRectSize + sideSize);
        CGContextClosePath(context);
        
        if (self.enabled) {
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
        }
        else {
            CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
        }
        CGContextFillPath(context);
    }
}

@end

