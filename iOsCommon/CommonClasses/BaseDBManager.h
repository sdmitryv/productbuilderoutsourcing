//
//  BaseDBManager.h
//  TimeCard
//
//  Created by macuser on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "sqlite3.h"

@interface BaseDBManager : NSObject {
    
    sqlite3 * database;
    NSString *_pathToDatabase;
    NSString *databaseName;
    NSInteger databaseCurrentVersion;
}

@property (nonatomic, readonly) NSString *pathToDatabase;
@property (nonatomic, retain) NSString *databaseName;


-(void)checkDBVersion;

-(BOOL)connectToDataBase;
-(sqlite3 *)getDataBaseConnection;
- (void)closeDataBaseConnection;
-(void)createDataBase;

-(BOOL)dropDataBase;
-(void)clearDataBase;


+ (NSString *)getStr:(sqlite3_stmt *)statement index:(int)index;
+ (int)getInt:(sqlite3_stmt *)statement index:(int)index;
+ (double)getDouble:(sqlite3_stmt *)statement index:(int)index;
+ (BOOL)getBool:(sqlite3_stmt *)statement index:(int)index;
+ (NSMutableArray *)getBytes:(sqlite3_stmt *)statement index:(int)index;

@end
