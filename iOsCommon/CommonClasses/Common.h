//
//  Common.h
//  StoreManager
//
//  Created by  on 12/13/11.
//  Copyright (c) 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject

+(NSString*)getMD5Base64:(NSString*)txt;

+ (NSUInteger)memoryAvailable;

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end
