//
//  CustomUILayout_Sqlite.h
//  Shipments
//
//  Created by valery on 6/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CustomUILayout.h"
//#import "CEntity+Sqlite.h"
#import "CROEntity+Sqlite.h"

@implementation CustomUILayout (Sqlite)

-(void)fetch:(EGODatabaseRow *)row {
    [super fetch:row];
    self.id = [row UuidForColumn:@"CustomUILayoutId"];
    self.key = [row stringForColumn:@"Key"];
    self.value = [row stringForColumn:@"Value"];
    self.editEmployeeId = [row UuidForColumn:@"EditEmployeeId"];
    self.editDeviceId = [row UuidForColumn:@"EditDeviceId"];
    self.locationId = [row UuidForColumn:@"LocationId"];
    //[self markClean];
}

+(void)load{
    EXCHANGE_METHOD(getInstanceByKey:, getInstanceByKeyImpl:);
}


+(instancetype)getInstanceById:(BPUUID*)customUILayoutId{
    if (!customUILayoutId) return nil;
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    EGODatabaseResult* result = [db executeQueryWithParameters: @"SELECT * FROM CustomUILayout where CustomUILayoutId=?", customUILayoutId, nil];
    if (!result.rows.count) return nil;
    return [[[self.class alloc] initWithRow:(result.rows)[0]] autorelease];
}

+(instancetype)getInstanceByKeyImpl:(NSString*)key{
    if (!key) return nil;
    DataManager *dbManager =[DataManager instance];
    EGODatabase *db = dbManager.currentDatabase;
    EGODatabaseResult* result = [db executeQueryWithParameters: @"SELECT * FROM CustomUILayout where Key=?", key, nil];
    if (!result.rows.count) return nil;
    return [[[self.class alloc] initWithRow:(result.rows)[0]] autorelease];
}

//-(BOOL)save:(EGODatabase*)db error:(NSError**)error{
//    if (!self.isDirty || (self.isDeleted && self.isNew))
//        return TRUE;
//
//    NSString* const sqlSettingInsert = @"insert or replace into CustomUILayout (RecCreated, RecModified, CustomUILayoutId, EditEmployeeId, EditDeviceId, LocationId, Key, Value) values (@RecCreated, @RecModified, @CustomUILayoutId, @EditEmployeeId, @EditDeviceId, @LocationId, @Key, @Value)";
//    NSString* const sqlSettingDelete = @"delete from CustomUILayout where CustomUILayoutId=@CustomUILayoutId";
//
//    NSString*	sqlStmt  = nil;
//    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
//
//    [parameters setObjectNilSafe:[_id stringRepresentation] forKey:@"CustomUILayoutId"];
//
//    if (self.isDeleted){
//        sqlStmt = sqlSettingDelete;
//    }
//    else{
//
//        sqlStmt = sqlSettingInsert;
//        [parameters setObjectNilSafe:_recCreated forKey:@"RecCreated"];
//        [parameters setObjectNilSafe:_recModified  forKey:@"RecModified"];
//        [parameters setObjectNilSafe:self.key forKey:@"Key"];
//        [parameters setObjectNilSafe:self.value forKey:@"Value"];
//        [parameters setObjectNilSafe:self.locationId?:[Location localLocation].id forKey:@"LocationId"];
//        [parameters setObjectNilSafe:self.editDeviceId?:[AppSettingManager instance].iPadId forKey:@"EditDeviceId"];
//        [parameters setObjectNilSafe:self.editEmployeeId?:[SecurityManager currentEmployee] forKey:@"EditEmployeeId"];
//    BOOL result = [db executeUpdate:sqlStmt namedParameters:parameters];
//    [parameters release];
//
//    if (!result || [db hadError]) {
//        NSLog(@"error: %@", [db lastErrorMessage]);
//        return FALSE;
//    }
//
//    return TRUE;
//}

@end
