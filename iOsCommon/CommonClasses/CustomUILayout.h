//
//  CustomUILayout.h
//  Shipments
//
//  Created by valery on 6/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CEntity.h"
#import "CEntityList.h"
#import "GridTable+Serialization.h"

@interface CustomUILayout : CROEntity

@property (nonatomic, retain) BPUUID* editEmployeeId;
@property (nonatomic, retain) BPUUID* editDeviceId;
@property (nonatomic, retain) BPUUID* locationId;
@property (nonatomic, retain)NSString* key;
@property (nonatomic, retain)NSString* value;
-(instancetype)initWithKey:(NSString*)key value:(NSString*)value;
+(instancetype)getInstanceByKey:(NSString*)key;

@end

@interface CustomUILayoutManager : NSObject{
    NSMutableDictionary* _layouts;
    BOOL _layoutChangingEnabled;
}

-(CustomUILayout*)objectForKeyedSubscript:(NSString*)key;
-(NSInteger)layoutsCount;
-(void)addLayout:(CustomUILayout*)layout;
-(void)removeLayout:(CustomUILayout*)layout;
-(void)clearAllLayouts;
-(NSArray*)layouts;
//-(void)clearAndSaveAllLayouts;
+(instancetype)instance;
@property (atomic,assign)BOOL layoutChangingEnabled;

@end

@interface CustomLayoutHelper : NSObject
+(BOOL)loadLayoutForGrid:(GridTable*)grid layoutPath:(NSString*)layoutPath;
+(void)saveLayoutForGrid:(GridTable*)grid layoutPath:(NSString*)layoutPath;
@end
