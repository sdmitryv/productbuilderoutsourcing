//
//  NSString+MD5.h
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(MD5)

+(NSString*)MD5FromChar:(const char *)cStr withLength:(NSUInteger)length;
+(NSString *)MD5FromString:(NSString *)str;
-(NSString *)MD5;

@end
