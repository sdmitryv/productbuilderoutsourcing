//
//  NSString+MD5.m
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import "NSString+MD5.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(MD5)

////////////////////////////////////////////////////////////
+(NSString*)MD5FromChar:(const char *)cStr withLength:(NSUInteger)length {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)length, outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++)
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    
    return [outputString autorelease];
}


////////////////////////////////////////////////////////////
+(NSString *)MD5FromString:(NSString *)str {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [str MD5];
}


////////////////////////////////////////////////////////////
- (NSString *)MD5 {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (self == nil || [self length] == 0)
        return nil;
    
    return [NSString MD5FromChar:[self UTF8String] withLength:self.length];
}

@end
