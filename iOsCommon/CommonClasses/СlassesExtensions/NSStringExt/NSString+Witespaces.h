//
//  NSString+Witespaces.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Witespaces)

+(NSString *)removeWitespaces:(NSString *)str;
-(NSString *)removeWitespaces;

@end
