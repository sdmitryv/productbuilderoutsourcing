//
//  NSDate+Str.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSDate+Str.h"

@implementation NSDate(Str)

////////////////////////////////////////////////////////////
+(NSString *)stringRepresentationOfDate:(NSDate *)date format:(NSString *)format withMili:(BOOL)withMili {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif    
    
    if (!date)
        return @"";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:format];
    NSString *dateResult = [formatter stringFromDate:date];
    [formatter release];
    
    if (withMili)  {
        
        double mili = [date timeIntervalSince1970] - (int)[date timeIntervalSince1970];
        NSString *miliStr = [NSString stringWithFormat:@"%3.0f", mili*1000.0];
        miliStr = [miliStr stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        
        dateResult = [NSString stringWithFormat:@"%@.%@", dateResult, miliStr];
    }
    
    return dateResult;
}


////////////////////////////////////////////////////////////
+(NSString *)stringRepresentationOfDate:(NSDate *)date format:(NSString *)format {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSDate stringRepresentationOfDate:date format:format withMili:YES];
}


////////////////////////////////////////////////////////////
+(NSString *)stringRepresentationOfDate:(NSDate *)date {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSDate stringRepresentationOfDate:date format:@"yyyy-MM-dd'T'HH:mm:ss"];
}


////////////////////////////////////////////////////////////
-(NSString *)stringRepresentationForFormat:(NSString *)format {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSDate stringRepresentationOfDate:self format:format];
}


////////////////////////////////////////////////////////////
-(NSString *)stringRepresentation {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [self stringRepresentationForFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
}


////////////////////////////////////////////////////////////
+(NSString *)stringRepresentationOfCurrentDateForFormat:(NSString *)format {
    
    return [NSDate stringRepresentationOfDate:[NSDate date] format:format];
}


////////////////////////////////////////////////////////////
+(NSString *)stringRepresentationOfCurrentDate {
    
    return [NSDate stringRepresentationOfCurrentDateForFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
}


////////////////////////////////////////////////////////////
+(NSDate *)dateFromJSON:(NSString *)str {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!str)
        return nil;
    
    NSString* header = @"/Date(";
    NSUInteger headerLength = [header length];
    
    NSString*  timestampString;
    
    NSScanner* scanner = [[NSScanner alloc] initWithString:str];
    [scanner setScanLocation:headerLength];
    [scanner scanUpToString:@")" intoString:&timestampString];
    
    NSCharacterSet* timezoneDelimiter = [NSCharacterSet characterSetWithCharactersInString:@"+-"];
    NSRange rangeOfTimezoneSymbol = [timestampString rangeOfCharacterFromSet:timezoneDelimiter];
    
    [scanner release];
    
    if (rangeOfTimezoneSymbol.length != 0) {
        
        NSRange rangeOfFirstNumber;
        rangeOfFirstNumber.location = 0;
        rangeOfFirstNumber.length = rangeOfTimezoneSymbol.location;
        
        NSRange rangeOfSecondNumber;
        rangeOfSecondNumber.location = rangeOfTimezoneSymbol.location + 1;
        rangeOfSecondNumber.length = [timestampString length] - rangeOfSecondNumber.location;
        
        NSString* firstNumberString = [timestampString substringWithRange:rangeOfFirstNumber];
        //NSString* secondNumberString = [timestampString substringWithRange:rangeOfSecondNumber];
        
        long long firstNumber = [firstNumberString longLongValue];
        //uint secondNumber = [secondNumberString intValue];
        
        if (firstNumber < 0)
            return nil;
        
        NSTimeInterval interval = firstNumber/1000.0;
        
        return [NSDate dateWithTimeIntervalSince1970:interval];
    }
    
    long long firstNumber = [timestampString longLongValue];
    
    if (firstNumber < 0)
        return nil;
    
    NSTimeInterval interval = firstNumber/1000.0;
    
    return [NSDate dateWithTimeIntervalSince1970:interval];
}


////////////////////////////////////////////////////////////
+(NSString *)JSONRepresentationFromDate:(NSDate *)date {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!date)
        return @"";
    
    NSTimeInterval interval = [date timeIntervalSince1970];
    
    return [NSString stringWithFormat:@"/Date(%.0f)/", interval*1000.0];
}


////////////////////////////////////////////////////////////
-(NSString *)JSONRepresentation {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [NSDate JSONRepresentationFromDate:self];    
}


@end
