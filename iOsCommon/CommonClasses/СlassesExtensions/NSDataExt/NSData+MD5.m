//
//  NSData+MD5.m
//  Lessons
//
//  Created by Dmitry Smirnov on 5/15/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "NSData+MD5.h"
#import "NSData+Base64.h"
#import "NSString+MD5.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSData(MD5)

////////////////////////////////////////////////////////////
+(NSString *)MD5FromData:(NSData *)data {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!data)
        return nil;
    
    return [data MD5];
}


////////////////////////////////////////////////////////////
- (NSString *)MD5 {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (self == nil)
        return nil;
    
    return [NSString MD5FromChar:self.bytes withLength:self.length];
}

- (NSString *)sha256WithRSA:(SecKeyRef)key {
    NSData * signedData = PKCSSignBytesSHA256withRSA(self, key);
    NSString * retString = [signedData base64urlEncodedString];
    return retString;
}

NSData* PKCSSignBytesSHA256withRSA(NSData* plainData, SecKeyRef privateKey)
{
    size_t signedHashBytesSize = SecKeyGetBlockSize(privateKey);
    uint8_t* signedHashBytes = malloc(signedHashBytesSize);
    memset(signedHashBytes, 0x0, signedHashBytesSize);
    @try{
        size_t hashBytesSize = CC_SHA256_DIGEST_LENGTH;
        uint8_t* hashBytes = malloc(hashBytesSize);
        if (!CC_SHA256([plainData bytes], (CC_LONG)[plainData length], hashBytes)) {
            return nil;
        }
        
        SecKeyRawSign(privateKey,
                      kSecPaddingPKCS1SHA256,
                      hashBytes,
                      hashBytesSize,
                      signedHashBytes,
                      &signedHashBytesSize);
        
        NSData* signedHash = [NSData dataWithBytes:signedHashBytes
                                            length:(NSUInteger)signedHashBytesSize];
        if (hashBytes)
            free(hashBytes);
        return signedHash;
    }
    @finally{
        if (signedHashBytes)
            free(signedHashBytes);
    }
}

@end
