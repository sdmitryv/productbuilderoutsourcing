//
//  UIImage+Fragment.h
//  ProductBuilder
//
//  Created by Sergey Lugovoy on 4/27/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage(Fragment)

- (UIImage *)fragment:(CGRect)rect;

@end
