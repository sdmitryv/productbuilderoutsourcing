//
//  NSDate+Str.h
//  Lessons
//
//  Created by Dmitry Smirnov on 5/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate(Str)

+(NSString *)stringRepresentationOfDate:(NSDate *)date format:(NSString *)format withMili:(BOOL)withMili;
+(NSString *)stringRepresentationOfDate:(NSDate *)date format:(NSString *)format;
+(NSString *)stringRepresentationOfDate:(NSDate *)date;
-(NSString *)stringRepresentationForFormat:(NSString *)format;
-(NSString *)stringRepresentation;

+(NSString *)stringRepresentationOfCurrentDate;
+(NSString *)stringRepresentationOfCurrentDateForFormat:(NSString *)format;

+(NSDate *)dateFromJSON:(NSString *)str;
+(NSString *)JSONRepresentationFromDate:(NSDate *)date;
-(NSString *)JSONRepresentation;

@end
