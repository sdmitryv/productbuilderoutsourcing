//
//  NSURLSession+Extentions.h
//  ProductBuilder
//
//  Created by valery on 11/18/15.
//  Copyright © 2015 Cloudworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLSession(Extentions)

+ (NSData *)sendSynchronousRequest:(NSURLRequest *)request returningResponse:(NSURLResponse **)response error:(NSError **)error;

- (void)defaultHandlerForDidReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
                           completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * credential))completionHandler userName:(NSString*)userName password:(NSString*)password errorHandler:(void(^)(NSError*))errorHandler;
@end
