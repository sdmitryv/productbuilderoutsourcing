//
//  Synchonized.swift
//  ProductBuilder
//
//  Created by valery on 12/20/16.
//  Copyright © 2016 Cloudworks. All rights reserved.
//

import Foundation

public class Sync {

    public static func synchronized<T>(_ lock: AnyObject, _ body: () throws -> T) rethrows -> T {
        objc_sync_enter(lock)
        defer { objc_sync_exit(lock) }
        return try body()
    }
}
