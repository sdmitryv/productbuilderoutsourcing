//
//  BaseDBManager.m
//  TimeCard
//
//  Created by macuser on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseDBManager.h"
#import "Common.h"

@interface BaseDBManager(Private)
    
    -(NSInteger)getVersion;
    -(void)setCurrentVersion;
    -(BOOL)updateToVersion:(NSUInteger)version;
    -(BOOL)executeUpdate:(NSString*)pathToUpdate;
@end


@implementation BaseDBManager

@synthesize pathToDatabase, databaseName;

////////////////////////////////////////////////////////////
-(id)init {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    self = [super init];
    
    if (self) {
        _pathToDatabase = nil;
        self.databaseName = @"Default.sqlite";
    }
    
    return self;
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    [_pathToDatabase release]; 
    [databaseName release];
    
    [super dealloc];
}


////////////////////////////////////////////////////////////
//    Re-Implement this method if you want to change path from Documents directory
//    If not - simply set databaseName property
////////////////////////////////////////////////////////////
-(NSString *)pathToDatabase {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!_pathToDatabase) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES); 
        NSString *documentsDirectory = paths[0]; 
        _pathToDatabase = [documentsDirectory stringByAppendingPathComponent:self.databaseName ? self.databaseName : @""];
        [_pathToDatabase retain];
    }
    
    return _pathToDatabase;
}

////////////////////////////////////////////////////////////
-(BOOL)connectToDataBase {   
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL isExists = [fileManager fileExistsAtPath:self.pathToDatabase]; 
    
    database = nil;
    
    if (!isExists) {
        
        [self createDataBase];
        [Common addSkipBackupAttributeToItemAtPath:self.pathToDatabase];
    }
    
    if(sqlite3_open([self.pathToDatabase UTF8String], &database) != SQLITE_OK ) { 
        
        sqlite3_close(database);
#ifdef ShowLog 
        NSLog(@"!!!!%@", @"Failed to open database");
#endif
    }
    else
        return YES;
    
    database = nil;
    
    return NO;
}


////////////////////////////////////////////////////////////
-(sqlite3 *)getDataBaseConnection {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    return database;
}


////////////////////////////////////////////////////////////
- (void)closeDataBaseConnection {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    sqlite3_close(database);
    database = nil;
}


////////////////////////////////////////////////////////////
-(BOOL)dropDataBase {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL res = NO;
    
    @try {
        
        if ([fileManager fileExistsAtPath:self.pathToDatabase])
            [fileManager removeItemAtPath:self.pathToDatabase error:nil];
        
        res = YES;
    }
    @catch (NSException *e) { res = NO; }
    @finally {}
    
    return res;
}


////////////////////////////////////////////////////////////
-(void)clearDataBase {
    
}


////////////////////////////////////////////////////////////
//    Required
//    Implement here copy database from project resourses or create it dynamically
////////////////////////////////////////////////////////////
-(void)createDataBase {
    
}


#pragma mark Helpers for processing statements
////////////////////////////////////////////////////////////
+ (NSString *)getStr:(sqlite3_stmt *)statement index:(int)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif

	return sqlite3_column_text(statement, index) == NULL 
        ? @"" 
        : @((const char *)sqlite3_column_text(statement, index));
}


////////////////////////////////////////////////////////////
+ (int)getInt:(sqlite3_stmt *)statement index:(int)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
    return sqlite3_column_int(statement, index);
}


////////////////////////////////////////////////////////////
+ (double)getDouble:(sqlite3_stmt *)statement index:(int)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif

	return sqlite3_column_double(statement, index);
}


////////////////////////////////////////////////////////////
+ (BOOL)getBool:(sqlite3_stmt *)statement index:(int)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif

	return sqlite3_column_int(statement, index) == 1 ? YES : NO;
}

////////////////////////////////////////////////////////////
+ (NSMutableArray *)getBytes:(sqlite3_stmt *)statement index:(int)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    const Byte *blobBytes = sqlite3_column_blob(statement, index);
    int blobBytesLength = sqlite3_column_bytes(statement, index);
    
    
    NSMutableArray *arr = [[[NSMutableArray alloc] init] autorelease];
    for (NSInteger i = 0; i < blobBytesLength; i++)
        [arr addObject:@(blobBytes[i])];
    
    return arr;
}

#pragma mark - Database versioning
////////////////////////////////////////////////////////////
- (NSInteger)getVersion {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [self connectToDataBase];
    
    NSInteger dbVersion = databaseCurrentVersion;
    
    NSString * query = [NSString stringWithFormat:@"PRAGMA user_version"];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW)
            dbVersion = sqlite3_column_int(statement, 0);
        
#ifdef ShowLog
        NSLog(@"Database version: %d", dbVersion);
#endif
        
        sqlite3_finalize(statement);
    }
    
    [self closeDataBaseConnection];
    
    return dbVersion;
}


////////////////////////////////////////////////////////////
- (void)setCurrentVersion {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSString * query = [NSString stringWithFormat:@"PRAGMA user_version = %ld", (long)databaseCurrentVersion];
    sqlite3_stmt * statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_OK) {
            
#ifdef ShowLog
            NSLog(@"Database version updated: %d", databaseCurrentVersion);
#endif
        }
        
        sqlite3_finalize(statement);
    }
}


////////////////////////////////////////////////////////////
-(void)checkDBVersion {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSInteger dbVersion;
    
    if ((dbVersion = [self getVersion]) == databaseCurrentVersion)
        return;  
    
#ifdef IsTestMode
    
    [self dropDataBase];
    
    [self connectToDataBase];
    [self setCurrentVersion];
    [self closeDataBaseConnection];
#else 
    for (NSUInteger ver = dbVersion + 1; ver <= databaseCurrentVersion; ver++)
        if (![self updateToVersion:ver])
            [NSException raise:NSInternalInconsistencyException format:@"Unable to update database"];
#endif
    
}


////////////////////////////////////////////////////////////
- (BOOL)updateToVersion:(NSUInteger)version {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSString *file = [NSString stringWithFormat:@"update%lu",(unsigned long)version];
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:@"sql"];
    return [self executeUpdate:path];
}


////////////////////////////////////////////////////////////
- (BOOL)executeUpdate:(NSString*)pathToUpdate {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [self connectToDataBase];
    
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc]init];
    NSError* error = nil;
    NSString *stmt = [NSString stringWithContentsOfFile:pathToUpdate encoding:NSUTF8StringEncoding error:&error];
    if (error)
        return NO;
    
    BOOL result = FALSE;
    
    sqlite3_exec(database, "BEGIN", 0, 0, 0);
    
    if (sqlite3_exec(database, [stmt UTF8String], 0, 0, 0) == SQLITE_OK) {
        
        result = TRUE;
        sqlite3_exec(database, "COMMIT", 0, 0, 0);
    }
    else
        sqlite3_exec(database, "ROLLBACK", 0, 0, 0);
    
    [pool release];
    
    [self closeDataBaseConnection];
    
    return result;
}


@end
