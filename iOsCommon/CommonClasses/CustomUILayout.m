//
//  CustomUILayout.m
//  Shipments
//
//  Created by valery on 6/10/15.
//  Copyright (c) 2015 Cloudworks. All rights reserved.
//

#import "CustomUILayout.h"

@implementation CustomUILayout

-(void)dealloc{
    [_key release];
    [_value release];
    [_editEmployeeId release];
    [_editDeviceId release];
    [_locationId release];
    
    [super dealloc];
}

-(instancetype)initWithKey:(NSString*)key value:(NSString*)value{
    if ((self=[super init])){
        self.key = key;
        self.value = value;
    }
    return self;
}

+(instancetype)getInstanceByKey:(NSString*)key{
    return nil;
}

@end

@implementation CustomUILayoutManager

-(void)dealloc{
    [_layouts release];
    [super dealloc];
}

-(instancetype)init{
    if ((self=[super init])){
        _layouts = [[NSMutableDictionary alloc]init];
    }
    return self;
}

static dispatch_once_t pred;
static CustomUILayoutManager* instance = nil;

+(instancetype)instance {
    
    dispatch_once(&pred, ^{
        instance = [[CustomUILayoutManager alloc] init];
    });
    return instance;
}


- (CustomUILayout*)objectForKeyedSubscript:(NSString*)key{
    if (!key) return nil;
    return _layouts[key] ?:[CustomUILayout getInstanceByKey:key]?:[[[CustomUILayout alloc]initWithKey:key value:nil]autorelease];
}

-(NSInteger)layoutsCount{
    return _layouts.count;
}

-(void)addLayout:(CustomUILayout*)layout{
    if (!layout.key) return;
    _layouts[layout.key] = layout;
}

-(void)removeLayout:(CustomUILayout*)layout{
    if (!layout.key) return;
    [_layouts removeObjectForKey:layout.key];
}

-(void)clearAllLayouts{
    [_layouts removeAllObjects];
}

-(NSArray*)layouts{
    return _layouts.allValues;
}

//-(void)clearAndSaveAllLayouts{
//    [_layouts enumerateKeysAndObjectsUsingBlock:^(NSString* key, CustomUILayout* obj, BOOL *stop) {
//        [obj save];
//    }];
//    [self clearAllLayouts];
//}

@end


@implementation CustomLayoutHelper


+(BOOL)loadLayoutForGrid:(GridTable*)grid layoutPath:(NSString*)layoutPath{
    if (!grid || !layoutPath) return NO;
    BOOL result = NO;
    CustomUILayout* layout = CustomUILayoutManager.instance[layoutPath];
    if (layout.value.length){
        result = [grid deserialize:[layout.value dataUsingEncoding:NSUTF8StringEncoding] error:nil];
    }
    
    if (CustomUILayoutManager.instance.layoutChangingEnabled){
        grid.allowCustomization = YES;
        [grid startBlinking];
    }
    return result;
}

+(void)saveLayoutForGrid:(GridTable*)grid layoutPath:(NSString*)layoutPath{
    if (!grid || !layoutPath) return;
    if (CustomUILayoutManager.instance.layoutChangingEnabled){
        NSData* gridData = [grid serialize:nil];
        if(gridData){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                NSString *gridString = [[NSString alloc] initWithData:gridData encoding:NSUTF8StringEncoding];
                CustomUILayout* layout = CustomUILayoutManager.instance[layoutPath];
                layout.value = gridString;
                [CustomUILayoutManager.instance addLayout:layout];
                [gridString release];
            });
        }
    }
}


@end
