//
//  GAELog.m
//  ProductBuilder
//
//  Created by Alexander Martyshko on 10/20/14.
//  Copyright (c) 2014 Cloudworks. All rights reserved.
//

#import "GAELog.h"
#import "AppSettingManager.h"
#import "DataUtils.h"
#import "NSDate+System.h"
#import "NSDate+ISO8601Unparsing.h"
#import "DataUtils.h"
#import "DeviceAgentManager.h"

@implementation GAELog

- (void)dealloc {
    
    [_serverName release];
    [_appName release];
    [_dateTimeStr release];
    [_threadNo release];
    [_area release];
    [_message release];
    [_stackTrace release];
    [_locationId release];
    [_workstationId release];
    [_appShortVersion release];
    [_appVersion release];
    [_systemVersion release];
    [_additionalInfo release];
    [_deviceNo release];
    
    [super dealloc];
}

+ (NSArray *)listOfLogs {
    return nil;
}

+ (NSInteger)gaeLogSendSeverity {
    NSInteger gaeLogSendSeverity = [AppSettingManager instance].gaeLogSeverity;
    if (gaeLogSendSeverity == 0)
        gaeLogSendSeverity = GAELogTypeError;
    
    return gaeLogSendSeverity;
}


+ (void)logfWithType:(GAELogType)type area:(NSString *)area message:(NSString *)message stackTrace:(NSString *)stackTrace {
    
    if (![AppSettingManager instance].pushLogsToGae) {
        return;
    }
    
    NSInteger gaeLogSendSeverity = [[self class] gaeLogSendSeverity];
    if (type > gaeLogSendSeverity) {
        return;
    }
    
    GAELog *log = [[GAELog alloc] init];
    
    log.serverName = [AppSettingManager instance].defaultServerCode;
    log.appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    log.dateTimeStr = [[NSDate date] ISO8601DateString];
    log.threadNo = @"[0]";
    log.type = type;
    log.area = area;
    log.message = message;
    log.stackTrace = stackTrace;
    log.locationId = [AppSettingManager instance].locationId;
    log.workstationId = [AppSettingManager instance].workstationId;
    log.deviceNo = [AppSettingManager instance].iPadId;
    NSString *buildVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildVersion"];
    NSString *buildSubversion1 = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildSubVersion1"];
    NSString *buildSubversion2 = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildSubVersion2"];
    log.appShortVersion = [NSString stringWithFormat:@"%@.%@", buildVersion, buildSubversion1];
    log.appVersion = [NSString stringWithFormat:@"%@.%@.%@", buildVersion, buildSubversion1, buildSubversion2];
    log.systemVersion = [UIDevice currentDevice].systemVersion;
    
    NSString *bundleId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSString *deviceName = [UIDevice currentDevice].name;
    NSString *deviceModel = [UIDevice currentDevice].model;
    
    log.additionalInfo = [NSString stringWithFormat:@"BundleId: %@\nLocal DateTime: %@\nDevice Name: %@\nDevice Model: %@", bundleId, [DataUtils readableDateTimeStringFromDate:[NSDate systemDate]], deviceName, deviceModel];
    
    [log save];
    [log release];
}

static GAELog *publicLog;
static NSDate *startDate;
static NSDate *startDeltaDate;

+(void)startPublicLog:(GAELogType)type area:(NSString *)area message:(NSString *)message{
    
    if (![AppSettingManager instance].pushLogsToGae)
        return;
    
    NSInteger gaeLogSendSeverity = [[self class] gaeLogSendSeverity];
    if (type > gaeLogSendSeverity) {
        return;
    }

    @synchronized(self) {
        
        [publicLog release];
        publicLog = [[GAELog alloc] init];
        
        [startDate release];
        startDate = [[NSDate date] retain];
        
        [startDeltaDate release];
        startDeltaDate = [[NSDate date] retain];
        
        publicLog.serverName = [AppSettingManager instance].defaultServerCode;
        publicLog.appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        publicLog.dateTimeStr = [[NSDate date] ISO8601DateString];
        publicLog.threadNo = @"[0]";
        publicLog.type = type;
        publicLog.area = area;
        publicLog.message = message;
        publicLog.stackTrace = @"";
        publicLog.locationId = [AppSettingManager instance].locationId;
        publicLog.workstationId = [AppSettingManager instance].workstationId;
        publicLog.deviceNo = [AppSettingManager instance].iPadId;
        NSString *buildVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildVersion"];
        NSString *buildSubversion1 = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildSubVersion1"];
        NSString *buildSubversion2 = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBuildSubVersion2"];
        publicLog.appShortVersion = [NSString stringWithFormat:@"%@.%@", buildVersion, buildSubversion1];
        publicLog.appVersion = [NSString stringWithFormat:@"%@.%@.%@", buildVersion, buildSubversion1, buildSubversion2];
        publicLog.systemVersion = [UIDevice currentDevice].systemVersion;
        
        NSString *bundleId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
        NSString *deviceName = [UIDevice currentDevice].name;
        NSString *deviceModel = [UIDevice currentDevice].model;
        
        publicLog.additionalInfo = [NSString stringWithFormat:@"BundleId: %@\nLocal DateTime: %@\nDevice Name: %@\nDevice Model: %@", bundleId, [DataUtils readableDateTimeStringFromDate:[NSDate systemDate]], deviceName, deviceModel];
    }
}

+(void)addStackTraceToPublicLog:(NSString *)stackTrace {
    
    [[self class] addStackTraceToPublicLog:stackTrace calcDelta:NO startCalcDelta:NO];
}


+(void)addStackTraceToPublicLog:(NSString *)stackTrace calcDelta:(BOOL)calcDelta {
    
    [[self class] addStackTraceToPublicLog:stackTrace calcDelta:calcDelta startCalcDelta:YES];

}

+(void)addStackTraceToPublicLog:(NSString *)stackTrace startCalcDelta:(BOOL)startCalcDelta {
 
     [[self class] addStackTraceToPublicLog:stackTrace calcDelta:NO startCalcDelta:startCalcDelta];
}


+(void)addStackTraceToPublicLog:(NSString *)stackTrace calcDelta:(BOOL)calcDelta startCalcDelta:(BOOL)startCalcDelta {
    
    if (![AppSettingManager instance].pushLogsToGae)
        return;

    @synchronized(self) {
        
        if (!publicLog)
            return;
        
        if (publicLog.stackTrace.length > 0)
            publicLog.stackTrace = [publicLog.stackTrace stringByAppendingString:@"\n"];
        
        publicLog.stackTrace = [publicLog.stackTrace stringByAppendingFormat:@"%@ - %@%@",
                                [DataUtils readableDateTimeStringFromDate:[NSDate date] format:@"MM/dd/yyyy HH:mm:ss.SSSSSS"],
                                calcDelta ? [NSString stringWithFormat:@"%3.3f - ", fabs([startDeltaDate timeIntervalSinceNow])] : @"        ",
                                stackTrace];
        
        if (startCalcDelta) {
            
            [startDeltaDate release];
            startDeltaDate = [[NSDate date] retain];
        }
    }
}


+(void)pushPublicLog:(BOOL)calcTotalTime {
    
    if (![DeviceAgentManager sharedInstance].currentDevice.pushLogsToGae) {
        return;
    }

    @synchronized(self) {
        
        if (!publicLog)
            return;
        
        if (calcTotalTime)
            publicLog.stackTrace = [publicLog.stackTrace stringByAppendingFormat:@"\nTotal time: %.3f", fabs([startDate timeIntervalSinceNow])];
        [publicLog save];
        
        [publicLog release];
        publicLog = nil;
        
        [startDate release];
        startDate = nil;
        
        [startDeltaDate release];
        startDeltaDate = nil;
    }
}



- (NSString *)severity {
    switch (_type) {
        case GAELogTypeCritical:
            return @"CRITICAL";
        case GAELogTypeError:
            return @"ERROR";
        case GAELogTypeWarning:
            return @"WARNING";
        case GAELogTypeInfo:
            return @"INFO";
        case GAELogTypeVerbose:
            return @"VERBOSE";
    }
    return @"";
}

- (NSDictionary *)dictionatyRepresentation {
    return @{@"ServerName" : self.serverName,
             @"AppName" : self.appName,
             @"DateTime": self.dateTimeStr,
             @"ThreadNo": self.threadNo,
             @"Severity": self.severity,
             @"Area": self.area,
             @"Message": self.message,
             @"StackTrace": self.stackTrace,
             @"LocationId" : self.locationId.description,
             @"WorkstationId" : self.workstationId.description,
             @"DeviceNo" : self.deviceNo,
             @"AppShortVersion" : self.appShortVersion,
             @"AppVersion" : self.appVersion,
             @"SystemVersion" : self.systemVersion,
             @"AdditionalInfo" : self.additionalInfo
             };
}

+(BOOL)deleteFromList:(NSArray*)list error:(NSError**)error{
    return FALSE;
}

-(void)installPropertyObservers {
    
}

@end
