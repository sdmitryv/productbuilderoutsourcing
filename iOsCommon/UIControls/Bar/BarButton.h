//
//  ButtomBarButton.h
//  Restaurant
//
//   on 8/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, BarButtonType) { BarButtonTypeButton = 0, BarButtonTypeCheck  = 1, };


@protocol BarButtonDelegate

-(void)barButtonChecked:(id)sender;
-(void)barButtonPressed:(id)sender;
@end


@interface BarButton : UIView {
    
    BarButtonType _buttonType;
    BOOL _checked;
    BOOL _enabled;
    
    int _padding;
    
    UIButton *_button;
    UIView *_checkView;
    
    id<BarButtonDelegate> delegate;
    
    UIView *_backgroundView;
    UIView *_disabledView;
}

-(id)initCheck:(UIView *)backgroundView
  disabledView:(UIView *)disabledView
     checkView:(UIView *)checkView
       padding:(int)padding;

@property (nonatomic, readonly) BarButtonType buttonType;
@property (nonatomic, assign) BOOL checked;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, readonly) UIView *backgroundView;

@property (nonatomic, assign) id<BarButtonDelegate> delegate;

@end
