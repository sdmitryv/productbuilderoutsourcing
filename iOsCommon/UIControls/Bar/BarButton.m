//
//  ButtomBarButton.m
//  Restaurant
//
//   on 8/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BarButton.h"

@implementation BarButton

@synthesize buttonType = _buttonType, checked = _checked, enabled = _enabled,  delegate;


/////////////////////////////////////////////////////////////////////////////////////////
-(id)initCheck:(UIView *)backgroundView
  disabledView:(UIView *)disabledView
     checkView:(UIView *)checkView
       padding:(int)padding {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if(!(self = [super init]))
        return nil;
    
    _buttonType = BarButtonTypeCheck;
    
    if (checkView) {
        
        _checkView = [checkView retain];
        [self addSubview:checkView];
    }
    
    if (backgroundView) {
        
        _backgroundView = [backgroundView retain];
        [self addSubview:backgroundView];
    }
    
    
    if (disabledView) {
        
        _disabledView = [disabledView retain];
        [self addSubview:disabledView];
        _disabledView.hidden = YES;
    }
    
    
    _button = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [self addSubview:_button];
    [_button addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    [_button addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.checked = NO;
    self.enabled = YES;
    
    _padding = padding;
    
    return self;
}

-(UIView *)backgroundView {
    return _backgroundView;
}


/////////////////////////////////////////////////////////////////////////////////////////
-(void)setEnabled:(BOOL)enabled {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    _enabled = enabled;
    
    if (_backgroundView && _disabledView) {
        
        _backgroundView.hidden = !enabled;
        _disabledView.hidden = !enabled;
    }
    
    _button.enabled = enabled;
}


/////////////////////////////////////////////////////////////////////////////////////////
-(void)setChecked:(BOOL)checked {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (_buttonType != BarButtonTypeCheck)
        return;
    
    if (_checkView)
        _checkView.hidden = !checked;
    
    _checked = checked;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setFrame:(CGRect)frame {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    super.frame = frame;
    
    if (_backgroundView)
        _backgroundView.frame = CGRectMake(_padding, _padding, frame.size.width - 2*_padding, frame.size.height - 2*_padding);
    
    if (_disabledView)
        _disabledView.frame = CGRectMake(_padding, _padding, frame.size.width - 2*_padding, frame.size.height - 2*_padding);
    
    if (_checkView)
        _checkView.frame = self.bounds;
    
    if (_button)
        _button.frame = self.bounds;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)buttonDown:(id)sender {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)buttonPress:(id)sender {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (_buttonType == BarButtonTypeCheck)
        [delegate barButtonChecked:self];
    else
        [delegate barButtonPressed:self];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_backgroundView release];
    [_disabledView release];
    [_button release];
    
    [_checkView release];
    [super dealloc];
}

@end
