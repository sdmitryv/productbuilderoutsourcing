//
//  Bar.m
//  Restaurant
//
//   on 8/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Bar.h"


@interface Bar (Private)

-(void)updateContentPossitions;

@end


@implementation Bar

@synthesize delegate,
checkedButtonIndex = _checkedButtonIndex,
buttonSize = _buttonSize,
centerFirstAndLast = _centerFirstAndLast,
offset = _offset,
centerChecked = _centerChecked,
gradient;

////////////////////////////////////////////////////////////
-(id)init {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if(!(self = [super init]))
        return nil;
    
    _checkedButtonIndex = -1;
    
    
    _buttons = [[NSMutableArray alloc] init];
    
    
    _scroll = [[UIScrollView alloc] init];
    _scroll.delegate = self;
    _scroll.decelerationRate = 0.2;
    _scroll.pagingEnabled = NO;
    [self addSubview:_scroll];
    
    _centerChecked = YES;
    
    gradient = [[CAGradientLayer layer] retain];
    [self.layer insertSublayer:gradient atIndex:0];
    
    return self;
}


////////////////////////////////////////////////////////////
-(void)addCheck:(UIView *)backgroundView
   disabledView:(UIView *)disabledView
      checkView:(UIView *)checkView
        padding:(int)padding {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BarButton *button = [[BarButton alloc] initCheck:backgroundView
                                        disabledView:disabledView
                                           checkView:checkView
                                             padding:padding];
    if (backgroundView)
        button.frame = backgroundView.frame;
    else if (disabledView)
        button.frame = disabledView.frame;
    
    [_buttons addObject:button];
    button.delegate = self;
    
    if (_checkedButtonIndex < 0) {
        
        button.checked = YES;
        _checkedButtonIndex = [_buttons count] - 1;
    }
    
    [button release];
    
    
    [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)clear {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_buttons removeAllObjects];
    
    [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(NSInteger)getItemsCount {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    return [_buttons count];
}


////////////////////////////////////////////////////////////
-(void)barButtonChecked:(id)sender {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BarButton *item = (BarButton *)sender;
    
    NSInteger currentIndex = [_buttons indexOfObject:item];
    
    if (_centerChecked)
        [self checkItemAtIndex:currentIndex];
    else
        for (BarButton *button in _buttons) {
            
            if (item == button)
                button.checked = YES;
            else if (button.checked)
                button.checked = NO;
        }
    
    [delegate buttonChecked:currentIndex sender:self];
}


////////////////////////////////////////////////////////////
-(void)barButtonPressed:(id)sender {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
}


////////////////////////////////////////////////////////////
-(void)checkItemAtIndex:(NSInteger)index {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    NSInteger i = -1;
    
    CGPoint checkPoint = CGPointZero;
    
    for (BarButton *button in _buttons) {
        
        i++;
        
        if (i == index) {
            
            button.checked = YES;
            checkPoint = button.center;
        }
        else if (button.checked)
            button.checked = NO;
    }
    
    if (!_centerChecked && !_centerFirstAndLast)
        return;
    
    float center = roundf(_scroll.contentOffset.x + _scroll.frame.size.width/2);
    int sign = checkPoint.x > center ? 1 : (checkPoint.x < center ? -1 : 0);
    float delta = sign*_scroll.frame.size.width/2;
    CGRect visibleRect = CGRectMake(checkPoint.x + delta - (index == [_buttons count] - 1 ? 1 : 0), checkPoint.y, 1, 1);
    if (visibleRect.origin.x < 0) {
        visibleRect.origin.x = 0;
    }
    else if (visibleRect.origin.x >= _scroll.contentSize.width) {
        visibleRect.origin.x = _scroll.contentSize.width - 1;
    }
    
    _checkedButtonIndex = index;
    
    if  (delegate)
        [delegate barAnimationBegined];
    
    [UIView beginAnimations:@"swipe" context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationEnded)];
    
    [_scroll scrollRectToVisible:visibleRect animated:NO];
    
    [UIView commitAnimations];
}


////////////////////////////////////////////////////////////
-(void)setFrame:(CGRect)frame {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (super.frame.size.width != frame.size.width || super.frame.size.height != frame.size.height) {
        
        super.frame = frame;
        
        [self updateContentPossitions];
    }
    else
        super.frame = frame;
}


////////////////////////////////////////////////////////////
-(float)elementWidthForHeight:(float)height elementSize:(CGSize)elementSize{
    
    if (elementSize.height == 0)
        return 90.0;
    
    return elementSize.width*height/elementSize.height;
}


////////////////////////////////////////////////////////////
- (void)animationEnded {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if  (delegate)
        [delegate barAnimationEnded];
}


////////////////////////////////////////////////////////////
-(void)updateContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    _scroll.frame = self.bounds;
    
    for (NSInteger i = [_scroll.subviews count] - 1; i >= 0; i--)
        [((UIView *)(_scroll.subviews)[i]) removeFromSuperview];
    
    
    CGSize elemSize = _buttonSize;
    if (elemSize.height <= 0)
        elemSize = CGSizeMake(_buttonSize.width, self.frame.size.height);
    
    
    _scroll.contentSize = CGSizeMake(0, 0);
    if (_buttons.count == 0)
        return;
    
    
    NSInteger currentPossition = 0;
    if (_centerFirstAndLast) {
        
        float firstElemWidth = 0;
        if (elemSize.width <= 0)
            firstElemWidth = [self elementWidthForHeight:elemSize.height elementSize:((UIView *)_buttons[0]).frame.size];
        else
            firstElemWidth = elemSize.width;
        
        int startEndPadding = (self.frame.size.width - firstElemWidth)/2;
        currentPossition = startEndPadding < _offset ? _offset : startEndPadding;
    }
    else
        currentPossition += _offset;
    
    
    for (BarButton *button in _buttons) {
        
        [_scroll addSubview:button];
        
        float curentElemWidth = 0;
        if (elemSize.width <= 0)
            curentElemWidth = [self elementWidthForHeight:elemSize.height elementSize:button.frame.size];
        else
            curentElemWidth = elemSize.width;
        
        button.frame = CGRectMake(currentPossition,
                                  (_scroll.frame.size.height - elemSize.height)/2,
                                  curentElemWidth,
                                  elemSize.height);
        
        currentPossition += button.frame.size.width + ([_buttons lastObject] == button ? 0 : _offset);
    }
    
    if (_centerFirstAndLast) {
        
        float lastElemWidth = 0;
        if (elemSize.width <= 0)
            lastElemWidth = [self elementWidthForHeight:elemSize.height elementSize:((UIView *)[_buttons lastObject]).frame.size];
        else
            lastElemWidth = elemSize.width;
        
        int startEndPadding = (self.frame.size.width - lastElemWidth)/2;
        currentPossition += startEndPadding < _offset ? _offset : startEndPadding;
    }
    else
        currentPossition += _offset;
    
    
    _scroll.contentSize = CGSizeMake(currentPossition, _scroll.frame.size.height);
    
    gradient.frame = self.bounds;
}

-(BarButton *)barViewAtIndex:(NSInteger)index {
    BarButton *retValue = nil;
    
    if (index >= 0 && index < _buttons.count) {
        retValue = _buttons[index];
    }
    
    return retValue;
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_buttons release];
    [_scroll release];
    [gradient release];
    
    [super dealloc];
}

@end
