//
//  MagazinePage.h
//  Lessons
//
//  Created by  on 1/17/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MagazinePage : UIView {
    
    CGRect _containerFrame;
    UIView *_contentView;
    CGSize _originalSize;

    float _maxScale;
    
    float _scale;
    
    BOOL _canZoom;
    
    BOOL _visible;
    
    BOOL _isCurrent;
}

@property (nonatomic, assign) float maxScale;
@property (nonatomic, assign) float scale;
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, assign) BOOL isCurrent;
@property (nonatomic, readonly) UIView *contentView;

-(CGSize)moveTo:(CGSize)move;
-(void)scaleOn:(float)scale;
-(BOOL)canZoom:(int)sign;

-(void)updateContentPossitions;
-(void)checkContentPossitions;

-(UIImage*)imageRepresentation;

@end
