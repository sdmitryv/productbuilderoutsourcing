//
//  MagazineView.m
//  Lessons
//
//  Created by  on 1/17/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "MagazineView.h"


@interface MagazineView(Private) 
    
    -(void)moveToCoordinates:(NSInteger)sign;
    -(void)oneClick;
    -(void)movePageTo:(int)sign;
@end



@implementation MagazineView

@synthesize delegate, 
            pages = _pages, 
            pageSize = _pageSize, 
            pagesPadding = _pagesPadding, 
            currentIndex = _currentIndex,
            currentPage,
            currentPageFrame;

////////////////////////////////////////////////////////////
- (id)init {

    self = [super init];
    if (self) {
#ifdef ShowLog
        NSLog(@" %s", __FUNCTION__);
#endif
        
        _pages = [[NSMutableArray alloc] init];

        _currentIndex = -1;

        animationEnded = YES;
        
        self.clipsToBounds = YES;
        
    }
    return self;
}


////////////////////////////////////////////////////////////
-(void)setPages:(NSMutableArray *)pages {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_pages removeAllObjects];
    [_pages addObjectsFromArray:pages];
    
    if (pages.count == 0)
        _currentIndex = 0;
    else if (_currentIndex >= [pages count])
        _currentIndex = [pages count] - 1;
    
    if (rightPage)  { rightPage.visible = NO;   [rightPage removeFromSuperview];  rightPage = nil; }
    if (leftPage)   { leftPage.visible = NO;    [leftPage removeFromSuperview];   leftPage = nil; }
    if (currentPage){ 
        
        currentPage.isCurrent = NO;        
        currentPage.visible = NO; 
        [currentPage removeFromSuperview];
        currentPage = nil; 
    }
    
    [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)setPageSize:(CGSize)pageSize {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BOOL isChanged = (pageSize.width != _pageSize.width || pageSize.height != _pageSize.height);
    
    _pageSize = pageSize;
    
    if (isChanged)    
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)setPagesPadding:(NSInteger)pagesPadding {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BOOL isChanged = pagesPadding != _pagesPadding;
    
    _pagesPadding = pagesPadding;
    
    if (isChanged)
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)setFrame:(CGRect)frame {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BOOL isChanged = (super.frame.size.width != frame.size.width || super.frame.size.height != frame.size.height);
    
    super.frame = frame; 

    if (isChanged)
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)setCurrentIndex:(NSInteger)currentIndex {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (delegate)
        [delegate magazinAnimationBegined];
    
    BOOL isChanged = _currentIndex != currentIndex;
    
    _currentIndex = currentIndex;
    
    if (isChanged)
        [self updateContentPossitions];
    
    if (delegate)
        [delegate magazinAnimationEnded];
}


////////////////////////////////////////////////////////////
-(CGRect)currentPageFrame {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (currentPage)
        return currentPage.frame;
    
    return CGRectZero;
}


////////////////////////////////////////////////////////////
-(void)movePageTo:(int)sign {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (delegate)
        [delegate magazinAnimationBegined];
    
    _priorIndex = _currentIndex;
    _currentIndex += sign;
    
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        
    animationEnded = NO;
    
    [self moveToCoordinates:sign];
    } completion:^(BOOL finished) {
        [self swipingEnded];
    }];
}


////////////////////////////////////////////////////////////
-(void)nextPage {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif

    if (_currentIndex < _pages.count - 1)
        [self movePageTo:1];
}


////////////////////////////////////////////////////////////
-(void)priorPage {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (_currentIndex > 0)
        [self movePageTo:-1];        
}


////////////////////////////////////////////////////////////
-(void)updateContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    BOOL needRemoveAll = NO;
    if (_pages.count == 0 || _currentIndex < 0 || _currentIndex >= _pages.count)
        needRemoveAll = YES;

    if ((needRemoveAll || currentPage != _pages[_currentIndex]) && currentPage) {

        currentPage.isCurrent = NO;
        currentPage.visible = NO;
        [currentPage removeFromSuperview];
        currentPage = nil;
    }
        
    if ((needRemoveAll || _currentIndex == 0 || leftPage != _pages[_currentIndex - 1]) && leftPage) {

        leftPage.visible = NO;
        [leftPage removeFromSuperview];
        leftPage = nil;
    }

    if ((needRemoveAll || _currentIndex == _pages.count - 1 || rightPage != _pages[_currentIndex + 1]) && rightPage) {
        rightPage.visible = NO;
        [rightPage removeFromSuperview];
        rightPage = nil;
    }


    if (needRemoveAll) {
     
        self.userInteractionEnabled = NO;
        return;
    }
    
    self.userInteractionEnabled = YES;


    if (!currentPage) {
     
        currentPage = _pages[_currentIndex];
        [self addSubview:currentPage];
    }
    currentPage.frame = CGRectMake((self.frame.size.width - _pageSize.width)/2, 
                                   (self.frame.size.height - _pageSize.height)/2, 
                                   _pageSize.width, 
                                   _pageSize.height);
    currentPage.visible = YES;
    currentPage.isCurrent = YES;


    if (_currentIndex > 0) {
        
        if (!leftPage) {
            
            leftPage = _pages[_currentIndex - 1];
            [self addSubview:leftPage];
        }
        
        leftPage.frame = CGRectMake(currentPage.frame.origin.x - _pagesPadding - _pageSize.width, 
                                    (self.frame.size.height - _pageSize.height)/2, 
                                    _pageSize.width, 
                                    _pageSize.height);
        leftPage.visible = YES;
        if (leftPage.scale != 1) { leftPage.scale = 1; [leftPage updateContentPossitions]; }
    }


    if (_currentIndex < _pages.count - 1) {
        
        if (!rightPage) {
            
            rightPage = _pages[_currentIndex + 1];
            [self addSubview:rightPage];
        }
        
        rightPage.frame = CGRectMake(currentPage.frame.origin.x + _pagesPadding + _pageSize.width, 
                                     (self.frame.size.height - _pageSize.height)/2, 
                                     _pageSize.width, 
                                     _pageSize.height);
        rightPage.visible = YES;
        
        if (rightPage.scale != 1) { rightPage.scale = 1; [rightPage updateContentPossitions]; }
    }
}


////////////////////////////////////////////////////////////
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
	if (swiping || !animationEnded || isStoped) 
        return;
	
	isMoving = NO;
	
	startDistance = 0.0;
	NSInteger touchesCount = [[event allTouches] count];
	
	if (touchesCount == 1) {
		
        swiping = YES;
		
        swipeStartX = [[[event allTouches] allObjects][0] locationInView:self].x;
		swipeStartY = [[[event allTouches] allObjects][0] locationInView:self].y;
	}
	else if (touchesCount == 2) {

		swiping = YES;
		
		CGPoint touch0 = [[[event allTouches] allObjects][0] locationInView:self];
		CGPoint touch1 = [[[event allTouches] allObjects][1] locationInView:self];
		startDistance = sqrt((touch0.x - touch1.x) * (touch0.x - touch1.x) + (touch0.y - touch1.y) * (touch0.y - touch1.y));
	}
}


////////////////////////////////////////////////////////////
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event { 
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
	if (!swiping) 
        return;

    isMoving = YES;
	
	NSInteger touchesCount = [[event allTouches] count];
	
	if (touchesCount == 1) {
		
		isMoving = YES;
		startDistance = 0.0;
		
		if (swipeStartX == 0.0) { 
		
            swipeStartX = [[[event allTouches] allObjects][0] locationInView:self].x;
			swipeStartY = [[[event allTouches] allObjects][0] locationInView:self].y;
			return;
		}
		
        CGFloat swipeDistanceX = swipeStartX - [[[event allTouches] allObjects][0] locationInView:self].x, 
                swipeDistanceY = swipeStartY - [[[event allTouches] allObjects][0] locationInView:self].y;
                
        swipeStartX = [[[event allTouches] allObjects][0] locationInView:self].x;
        swipeStartY = [[[event allTouches] allObjects][0] locationInView:self].y;
        
        
        CGSize moveSize = [currentPage moveTo:CGSizeMake(swipeDistanceX, swipeDistanceY)];

        currentPage.frame = CGRectMake(currentPage.frame.origin.x - moveSize.width, 
                                       currentPage.frame.origin.y,
                                       currentPage.frame.size.width,
                                       currentPage.frame.size.height);
        
        if (leftPage)
            leftPage.frame = CGRectMake(leftPage.frame.origin.x - moveSize.width, 
                                        leftPage.frame.origin.y,
                                        leftPage.frame.size.width,
                                        leftPage.frame.size.height);
        if (rightPage)
            rightPage.frame = CGRectMake(rightPage.frame.origin.x - moveSize.width, 
                                         rightPage.frame.origin.y,
                                         rightPage.frame.size.width,
                                         rightPage.frame.size.height);
	}
	else if (touchesCount == 2) {
		
        isMoving = YES;
		
		CGPoint touch0 = [[[event allTouches] allObjects][0] locationInView:self];
		CGPoint touch1 = [[[event allTouches] allObjects][1] locationInView:self];
		
        swipeStartX = 0;
        swipeStartY = 0;

        float newDistance = sqrt((touch0.x-touch1.x)*(touch0.x-touch1.x) + (touch0.y-touch1.y)*(touch0.y-touch1.y));
                    
        if (startDistance == 0.0) 
            startDistance = newDistance;
        
        [currentPage scaleOn:(newDistance - startDistance)/sqrt(_pageSize.width*_pageSize.width + _pageSize.height*_pageSize.height)];
        
        startDistance = newDistance;
	}
}


////////////////////////////////////////////////////////////
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
	if (!swiping) 
        return;
	
    NSInteger touchesCount = [[event allTouches] count];
	if (touchesCount == 1 && !isMoving) {
        
        [self oneClick];
        return;
	}
    
    _priorIndex = _currentIndex;
    
    animationEnded = NO;
    NSUInteger count = [_pages count];
	CGFloat swipeDistance = currentPage.frame.origin.x - (self.frame.size.width - _pageSize.width)/2;

    if (delegate)
        [delegate magazinAnimationBegined];

    
    if (_currentIndex > 0 && swipeDistance > self.frame.size.width/3) {
        
        leftPage.isCurrent = YES;
		_currentIndex--;
    }
	else if (_currentIndex < count - 1 && swipeDistance < -self.frame.size.width/3){
        
        rightPage.isCurrent = YES;
        _currentIndex++;
    }
    
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self moveToCoordinates:_currentIndex - _priorIndex];
        
        if (leftPage) [leftPage checkContentPossitions];
        if (currentPage) [currentPage checkContentPossitions];
        if (rightPage) [rightPage checkContentPossitions];
        swiping = NO;
    } completion:^(BOOL finished) {
        if (_priorIndex != _currentIndex)
            [self swipingEnded];
        else
            [self animationEnded];

    }];
}


////////////////////////////////////////////////////////////
-(void)animationEnded {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (delegate)
        [delegate magazinAnimationEnded];
    
	animationEnded = YES;
}


////////////////////////////////////////////////////////////
-(void)swipingEnded {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
	[self animationEnded];
    
    if (_priorIndex > _currentIndex) {
        
        [rightPage removeFromSuperview];
        rightPage.visible = NO;
        rightPage = currentPage;
        currentPage.isCurrent = NO;
        currentPage = leftPage;
        currentPage.isCurrent = YES;
        leftPage = nil;
    }
    else if (_priorIndex < _currentIndex) {
        
        [leftPage removeFromSuperview];
        leftPage.visible = NO;
        leftPage = currentPage;
        currentPage.isCurrent = NO;
        currentPage = rightPage;
        currentPage.isCurrent = YES;
        rightPage = nil;
    }
    
    [self updateContentPossitions];
	
	if (delegate)
		[self.delegate magazinPageChanged:_currentIndex];
}


////////////////////////////////////////////////////////////
-(void)oneClick{
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
	
	animationEnded = YES;
    
    if (delegate)
        [delegate magazinOneClick];
}


////////////////////////////////////////////////////////////
-(void)moveToCoordinates:(NSInteger)sign {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (sign > 0) {
        
        rightPage.frame = CGRectMake((self.frame.size.width - _pageSize.width)/2, 
                                     (self.frame.size.height - _pageSize.height)/2, 
                                     _pageSize.width, 
                                     _pageSize.height);

        currentPage.frame = CGRectMake(rightPage.frame.origin.x - _pagesPadding - _pageSize.width, 
                                       (self.frame.size.height - _pageSize.height)/2, 
                                       _pageSize.width, 
                                       _pageSize.height);
        
        leftPage.frame = CGRectMake(currentPage.frame.origin.x - _pagesPadding - _pageSize.width, 
                                    (self.frame.size.height - _pageSize.height)/2, 
                                    _pageSize.width, 
                                    _pageSize.height);
    }
    else if (sign < 0) {
        
        leftPage.frame = CGRectMake((self.frame.size.width - _pageSize.width)/2, 
                                     (self.frame.size.height - _pageSize.height)/2, 
                                     _pageSize.width, 
                                     _pageSize.height);
        
        currentPage.frame = CGRectMake(leftPage.frame.origin.x + _pagesPadding + _pageSize.width, 
                                       (self.frame.size.height - _pageSize.height)/2, 
                                       _pageSize.width, 
                                       _pageSize.height);
        
        rightPage.frame = CGRectMake(currentPage.frame.origin.x + _pagesPadding + _pageSize.width, 
                                    (self.frame.size.height - _pageSize.height)/2, 
                                    _pageSize.width, 
                                    _pageSize.height);
    }
    else 
        [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [_pages release];
    
    [super dealloc];
}

@end
