//
//  MagazineAVVideoPage.m
//  ProductBuilder
//
//  Created by dev1 on 5/22/12.
//  Copyright (c) 2012 Cloudworks. All rights reserved.
//

#import "MagazineAVVideoPage.h"

@implementation MagazineAVVideoPage

@synthesize filePath = _filePath, theMovie;

////////////////////////////////////////////////////////////
- (id)init {
    
    self = [super init];
    if (self) {
        
        _canZoom = NO;
        self.backgroundColor = [UIColor blackColor]; 
    }
    return self;
}


////////////////////////////////////////////////////////////
-(void)setFilePath:(NSString *)filePath {
    
    if (_contentView) {
        
        [_contentView removeFromSuperview];
        [_contentView release];
        _contentView = nil;
    }
    
    if (_filePath)
        [_filePath release];
    
    _filePath = [filePath retain];
    
    [self updateContentPossitions];
}

////////////////////////////////////////////////////////////
-(void)setFileServerPath:(NSString *)fileServerPath {
    
    if (_contentView) {
        
        [_contentView removeFromSuperview];
        [_contentView release];
        _contentView = nil;
    }
    
    if (_fileServerPath)
        [_fileServerPath release];
    
    _fileServerPath = [fileServerPath retain];
    
    [self updateContentPossitions];
}


////////////////////////////////////////////////////////////
-(void)setIsCurrent:(BOOL)isCurrent {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (_isCurrent == isCurrent)
        return;
    
    _isCurrent = isCurrent;
    
    
    if (_isCurrent && _visible && !theMovie) {
        
        //theMovie.playbackState != MPMoviePlaybackStatePlaying
        
        if (!_filePath && !_fileServerPath)
            return;

        theMovie = [[CWMovieViewController alloc] init];
        theMovie.supportsPseudoFullScreen = self.supportsPseudoFullScreen;
        theMovie.shouldAutoplay = YES;
        theMovie.view.frame = self.bounds;
        theMovie.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:theMovie.view];
        _contentView = [theMovie.view retain];
        if (_filePath)
            [theMovie setURL:[NSURL fileURLWithPath:_filePath]];
        else if (_fileServerPath)
            [theMovie setURL:[NSURL URLWithString:_fileServerPath]];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlaybackComplete:)
                                                     name:CWMoviePlayerPlaybackDidFinishNotification
                                                   object:theMovie];
    }
    
    else if (!_isCurrent && theMovie) {
        
        [theMovie stop];
        
        [_contentView removeFromSuperview];
        [_contentView release];
        _contentView = nil;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:CWMoviePlayerPlaybackDidFinishNotification
                                                      object:theMovie];
        [theMovie release];
        theMovie = nil;
    }
}


////////////////////////////////////////////////////////////
-(void)updateContentPossitions {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    if (!_filePath || !_fileServerPath || !_visible)
        return;
    
    _containerFrame = self.bounds;
    if (_contentView)
        _contentView.frame = _containerFrame;
}


////////////////////////////////////////////////////////////
- (void)moviePlaybackComplete:(NSNotification *)notification {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    CWMovieViewController *movie = [notification object];
    
	if (movie.fullscreen)
        movie.fullscreen = NO;
}


////////////////////////////////////////////////////////////
-(UIImage*)imageRepresentation {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    UIImage *videoImage = nil;
    /*    if (theMovie)
     videoImage = [theMovie thumbnailImageAtTime:theMovie.currentPlaybackTime timeOption:MPMovieTimeOptionExact];
     
     
     UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.frame.size.width, self.frame.size.height), 
     YES, 
     [UIScreen mainScreen].scale);
     [self.layer renderInContext:UIGraphicsGetCurrentContext()];
     UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     
     CGSize bgImageSize = bgImage == nil ? CGSizeZero : bgImage.size;
     const float colorMasking[6] = {0, 0, 0, 0, 0, 0};
     
     UIGraphicsBeginImageContext(bgImageSize);
     CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, bgImageSize.height);
     CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
     
     CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), 130/255, 130/255, 130/255, 1);
     CGContextFillRect (UIGraphicsGetCurrentContext(), CGRectMake (0, 0, bgImageSize.width, bgImageSize.height));
     
     if (videoImage && videoImage.size.width != 0 && videoImage.size.height != 0) {
     
     CGSize newVideoSize = videoImage.size;
     
     if (bgImageSize.height/newVideoSize.height > bgImageSize.width/newVideoSize.width)
     newVideoSize = CGSizeMake(bgImageSize.width, newVideoSize.height*bgImageSize.width/newVideoSize.width);
     else
     newVideoSize = CGSizeMake(newVideoSize.width*bgImageSize.height/newVideoSize.height, bgImageSize.height);
     
     CGContextDrawImage(UIGraphicsGetCurrentContext(), 
     CGRectMake((bgImageSize.width - newVideoSize.width)/2, (bgImageSize.height - newVideoSize.height)/2, newVideoSize.width, newVideoSize.height),
     videoImage.CGImage);
     }
     
     CGContextSetRGBFillColor (UIGraphicsGetCurrentContext(), 0, 0, 0, 100.0/255.0);
     CGContextFillRect (UIGraphicsGetCurrentContext(), CGRectMake (0, 0, bgImageSize.width, 44));    
     
     if (bgImage) {
     
     CGImageRef maskedImageRef = CGImageCreateWithMaskingColors(bgImage.CGImage, colorMasking);    
     CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, bgImage.size.width, bgImage.size.height), maskedImageRef);
     CGImageRelease(maskedImageRef);
     }
     
     UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     return newImage;
     
     */
    return videoImage;
}


////////////////////////////////////////////////////////////
-(void)dealloc {
#ifdef ShowLog
    NSLog(@" %s", __FUNCTION__);
#endif
    
    [theMovie stop];
    [theMovie release];
    [_filePath release];
    [_fileServerPath release];
    
    [super dealloc];
}



@end
