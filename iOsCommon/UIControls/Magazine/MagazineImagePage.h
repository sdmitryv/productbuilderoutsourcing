//
//  MagazineImagePage.h
//  
//
//  Created by  on 1/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagazinePage.h"

typedef NS_ENUM(NSUInteger, ScaleType) {
    scaleAuto = 0, 
    scaleByPageWidth = 1, 
    scaleByPageWidthOrOriginal  = 2,
    scaleByWidth = 3, };


@interface MagazineImagePage : MagazinePage {

    NSString *_filePath;
    UIImage *_image;
    ScaleType scaleType;
    float _widthForScale;
}

@property (nonatomic, retain) NSString *filePath; 
@property (nonatomic, retain) UIImage *image; 
@property (nonatomic, assign) ScaleType scaleType; 
@property (nonatomic, assign) float widthForScale;

@end
